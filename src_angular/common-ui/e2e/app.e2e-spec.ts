import { BluebamboCommonUiPage } from './app.po';

describe('bluebambo-common-ui App', () => {
  let page: BluebamboCommonUiPage;

  beforeEach(() => {
    page = new BluebamboCommonUiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
