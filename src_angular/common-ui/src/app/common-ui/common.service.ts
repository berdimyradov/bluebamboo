import { Injectable } from '@angular/core';

@Injectable()
export class CommonService {
  private _uniqId = 0;

  constructor() { }

  public get uniqId(): number {
    return this._uniqId++;
  }
}
