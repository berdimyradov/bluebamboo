/**
    * Created by aleksay on 24.05.2017.
    * Project: tmp_booking
    */
import {Component, ViewEncapsulation} from "@angular/core";
@Component({
  selector: "slide-panel",
  templateUrl: './slide-panel.html',
  styleUrls: ['./slide-panel.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SlidePanelComponent {
    private open: boolean = true;

    public toggle(){
        this.open = !this.open;
    }
}
