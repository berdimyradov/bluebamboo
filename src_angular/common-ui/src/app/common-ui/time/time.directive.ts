import {Directive, Output, EventEmitter} from '@angular/core';
import {NgModel} from '@angular/forms';

@Directive({
  selector: '[ngModel][time]',
  host: {
    '(ngModelChange)': 'onInputChange($event)',
    '(keydown.backspace)':'onInputChange($event.target.value, true)'
  }
})
export class TimeDirective {
  constructor(public model: NgModel) {}

  @Output() rawChange:EventEmitter<string> = new EventEmitter<string>();

  onInputChange(event: any, backspace: any = null) {
    // remove all mask characters (keep only numeric)
    let newVal = (event || "").replace(/\D/g, ''),
      rawValue = newVal;

    // special handling of backspace necessary otherwise
    // deleting of non-numeric characters is not recognized
    // this laves room for improvment for example if you delete in the
    // middle of the string
    if(backspace) {
      newVal = newVal.substring(0, newVal.length - 1);
    }

    // don't show braces for empty value
    if(newVal.length == 0) {
      newVal = '';
    }
    if (newVal.length > 4) {
      newVal = newVal.substring(0, 4);
    }
    // don't show braces for empty groups at the end
    if (newVal.length == 0){
    }
    else if(newVal.length <= 2) {
      newVal = newVal.replace(/^(\d{0,2})/, '$1');
    } else {
      newVal = newVal.replace(/^(\d{0,2})(\d{0,2})/, '$1:$2');
    }
    // set the new value
    this.model.valueAccessor.writeValue(newVal);
    this.rawChange.emit(rawValue)
  }
}
