import { TestBed, inject } from '@angular/core/testing';

import { DynamicTypeBuilderService } from './dynamic-type-builder.service';

describe('DynamicTypeBuilderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DynamicTypeBuilderService]
    });
  });

  it('should be created', inject([DynamicTypeBuilderService], (service: DynamicTypeBuilderService) => {
    expect(service).toBeTruthy();
  }));
});
