import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { ArrayDataProvider } from '../types';
import { BbGridComponent } from '../bb-grid.component';

@Component({
  selector: 'bb-grid-data-source-array',
  template: ''
})
export class BbGridDataSourceArrayComponent implements OnInit, OnChanges {
  @Input() data: any[] = [];

  private provider: ArrayDataProvider = new ArrayDataProvider([]);

  constructor(private grid: BbGridComponent) { }

  ngOnInit() {
    this.provider.setNewArray(this.data);
    this.grid.setDataProvider(this.provider);
  }

  ngOnChanges(): void {
    this.provider.setNewArray(this.data);
  }
}
