import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BbGridDataSourceArrayComponent } from './bb-grid-data-source-array.component';

describe('BbGridDataSourceArrayComponent', () => {
  let component: BbGridDataSourceArrayComponent;
  let fixture: ComponentFixture<BbGridDataSourceArrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BbGridDataSourceArrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BbGridDataSourceArrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
