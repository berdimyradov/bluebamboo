import { Component, Input, OnInit } from '@angular/core';
import { BbGridComponent } from '../bb-grid.component';
import { GridColumn } from '../types';

@Component({
  selector: 'bb-grid-column',
  template: ''
})
export class BbGridColumnComponent implements OnInit {
  @Input() private title: string;
  @Input() private width: string;
  @Input() private field: string;
  @Input('not-sort-able') private notSortAble: boolean;
  @Input('pipe') private pipe: string;
  @Input('cell-template') private template: string;

  constructor(private grid: BbGridComponent) {
  }

  ngOnInit() {
    const column: GridColumn = new GridColumn();

    column.title = this.title;
    column.width = this.width;
    column.field = this.field;
    column.pipe = this.pipe;
    column.template = this.template;
    column.notSortAble = this.notSortAble;

    if (!column.field) {
      column.notSortAble = true;
    }

    this.grid.addColumn(column);
  }
}
