import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BbGridColumnComponent } from './bb-grid-column.component';

describe('BbGridColumnComponent', () => {
  let component: BbGridColumnComponent;
  let fixture: ComponentFixture<BbGridColumnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BbGridColumnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BbGridColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
