import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BbGridComponent } from './bb-grid.component';

describe('BbGridComponent', () => {
  let component: BbGridComponent;
  let fixture: ComponentFixture<BbGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BbGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BbGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
