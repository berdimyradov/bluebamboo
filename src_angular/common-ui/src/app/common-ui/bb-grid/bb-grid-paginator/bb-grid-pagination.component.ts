import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {BbGridComponent} from "../bb-grid.component";

@Component({
  selector: 'bb-grid-pagination',
  templateUrl: './bb-grid-pagination.component.html',
  styleUrls: ['./bb-grid-pagination.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BbGridPaginationComponent implements OnInit {
  public get page(): number {
    return this.grid.dataProvider ? this.grid.dataProvider.page : 0;
  }
  public get pageCount(): number {
    return this.grid.dataProvider ? this.grid.dataProvider.pageCount : 0;
  }
  public pageList: number[];

  constructor(private grid: BbGridComponent) { }

  ngOnInit() {
    this.grid.dataProvider.onPageChanged.subscribe( () => this.buildPagination() );
    this.grid.dataProvider.onDataChanged.subscribe( () => this.buildPagination() );
    this.buildPagination();
  }

  public setPage(p: number) {
    this.grid.dataProvider.page = p;
  }

  public setPrevPage() {
    this.grid.dataProvider.page = this.page - 1;
  }

  public setNextPage() {
    this.grid.dataProvider.page = this.page + 1;
  }

  private buildPagination(){
    const viewPagesOnSide = 2;
    const res: number[] = [];
    if (this.grid.dataProvider) {
      const page = this.grid.dataProvider.page;
      const countPage = this.grid.dataProvider.pageCount;
      let start = page + viewPagesOnSide >= countPage ? countPage - 2*viewPagesOnSide - 1 : page - viewPagesOnSide;
      start = start < 0 ? 0 : start;
      for(let i = start; i < Math.min(countPage, start + viewPagesOnSide + 3); i++){
        res.push(i);
      }
    }
    this.pageList = res;
  }
}
