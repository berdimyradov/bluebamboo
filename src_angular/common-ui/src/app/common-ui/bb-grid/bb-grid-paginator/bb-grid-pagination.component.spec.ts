import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BbGridPaginationComponent } from './bb-grid-pagination.component';

describe('BbGridPaginationComponent', () => {
  let component: BbGridPaginationComponent;
  let fixture: ComponentFixture<BbGridPaginationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BbGridPaginationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BbGridPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
