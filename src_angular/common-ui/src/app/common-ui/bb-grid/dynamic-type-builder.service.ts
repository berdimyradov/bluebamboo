import {Compiler, Component, ComponentFactory, Injectable, Input, NgModule} from '@angular/core';
import {JitCompilerFactory} from '@angular/compiler';
import {IGrid} from './types';
import {BbGridComponent} from './bb-grid.component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {CommonUiModule} from '../common-ui.module';

@Injectable()
export class DynamicTypeBuilderService {
  private compiler: Compiler = new JitCompilerFactory([{useDebug: false, useJit: true}]).createCompiler();

  constructor() {}

  // this object is singleton - so we can use this as a cache
  private _cacheOfFactories: {[templateKey: string]: ComponentFactory<IGrid>} = {};

  public createComponentFactory(template: string, modules: any[] = []): Promise<ComponentFactory<IGrid>> {
    let factory = this._cacheOfFactories[template];

    if (factory) {
      console.log('Module and Type are returned from cache');
      return new Promise((resolve) => resolve(factory));
    }

    // unknown template ... let's create a Type for it
    const type   = this.createNewComponent(template);
    const module = this.createComponentModule(type, modules);

    return new Promise((resolve) => {
      this.compiler
        .compileModuleAndAllComponentsAsync(module)
        .then((moduleWithFactories) => {
          factory = moduleWithFactories.componentFactories.find((it: any) => it.componentType == type ? true : false);
          this._cacheOfFactories[template] = factory;
          resolve(factory);
        });
    });
  }

  protected createNewComponent (template: string) {
    @Component({
      selector: 'bb-grid-dynamic-component',
      template: template,
    })
    class BBGridDynamicComponent  implements IGrid { @Input()  public grid: BbGridComponent; };
    // a component for this particular template
    return BBGridDynamicComponent;
  }

  protected createComponentModule (componentType: any, modules: any[] = []) {
    @NgModule({
      imports: [ CommonModule, FormsModule, CommonUiModule].concat(modules),
      declarations: [ componentType ],
    })
    class RuntimeComponentModule { };
    // a module for just this Type
    return RuntimeComponentModule;
  }
}
