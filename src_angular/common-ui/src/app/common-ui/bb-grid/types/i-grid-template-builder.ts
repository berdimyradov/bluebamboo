import {GridColumn} from "./grid-column";
export interface IGridTemplateBuilder {
  getTemplate(columns: GridColumn[]): string;
}
