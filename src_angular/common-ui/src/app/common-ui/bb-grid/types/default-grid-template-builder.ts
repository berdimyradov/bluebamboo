import { IGridTemplateBuilder, GridColumn } from './';

export class DefaultGridTemplateBuilder implements IGridTemplateBuilder{
  public getTemplate(columns: GridColumn[]): string {
    return this.buildGrid(columns);
  }

  private buildHeaderCell(column: GridColumn, pos: number): string {
    return `<th
      (click)="grid.setSortOrder(${pos})"
      style="${(column.width ? `width: ${column.width};` : '')}"
      [class.sort-asc]="grid.sortColumnIdx == ${pos} && grid.sortColumnOrder == 'asc'"
      [class.sort-desc]="grid.sortColumnIdx == ${pos} && grid.sortColumnOrder == 'desc'">
        ${column.title}
      </th>`;
  }

  private buildHeader(columns: GridColumn[]): string {
    let res = '<thead><tr>';

    res += columns.map((column: GridColumn, pos: number) => this.buildHeaderCell(column, pos)).join('');
    res += '</tr></thead>';

    return res;
  }

  private buildDateCell(column: GridColumn): string {
    let res = '<td>';

    if (column.template) {
      res += `${column.template}`;
    } else  if (column.field) {
      res += '{{row.' + column.field + (column.pipe ? '|' + column.pipe : '') + '}}';
    }

    res += '</td>';

    return res;
  }

  private buildData(columns: GridColumn[]): string {
    let res = '<tbody><ng-template ngFor let-row [ngForOf]="grid.data">' +
      '<tr (click)="grid.onRowClick.emit(row)" [class.error_message]="row.missing_fields_invoice && row.missing_fields_invoice.length">';

    res += columns.map((column: GridColumn) => this.buildDateCell(column)).join('');

    res += '</tr>';
    res += '<tr class="error_message" *ngIf="row.missing_fields_invoice && row.missing_fields_invoice.length">';
    res += '<td colspan="5"><ul>';
    res += '<li>{{("PAGE.MISSING-FIELDS.HEADER")|translate}}</li>';
    res += '<li *ngFor="let err of row.missing_fields_invoice">{{("PAGE.MISSING-FIELDS."+ err)|translate}}</li></ul><td></tr>';
    res += '</ng-template></tbody>';

    return res;
  }
  private buildGrid(columns: GridColumn[]): string {
    let res = '<table class="table table-striped table-hover">';

    res += this.buildHeader(columns);
    res += this.buildData(columns);
    res += '</table>';

    return res;
  }
}
