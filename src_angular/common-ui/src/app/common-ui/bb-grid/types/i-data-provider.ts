import {Observable} from "rxjs/Observable";
import {EventEmitter} from "@angular/core";
export interface IDataProvider {
  pageCount: number;
  pageSize: number;
  page: number;
  sortField: string;
  sortOrder: string;
  onDataChanged: EventEmitter<void>;
  onPageChanged: EventEmitter<void>;
  onSortChanged: EventEmitter<void>;

  getData(): Observable<any>;
  setSort(field: string, order: string)
}
