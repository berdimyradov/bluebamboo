import { IDataProvider } from './i-data-provider';
import { Observable } from 'rxjs/Observable';
import { EventEmitter } from '@angular/core';
import * as _ from 'lodash';

export class ArrayDataProvider implements IDataProvider {
  private _pageSize = 10;
  private _pageCount = 0;
  private _page = 0;
  private _sortField: string;
  private _sortOrder: string;
  private _data: any[];
  private data: any[];

  public get pageCount(): number {
    return this._pageCount;
  };

  public get pageSize(): number {
    return this._pageSize;
  }

  public set pageSize(v: number) {
    this._pageSize = v;
  }

  public get page(): number {
    return this._page;
  }

  public set page(v: number) {
    if (v >= this.pageCount) {
      v = this.pageCount - 1;
    }

    if (v < 0) {
      v = 0;
    }

    if (this._page !== v) {
      this._page = v;

      this.onPageChanged.emit();
      this.onDataChanged.emit();
    }
  }
  public get sortField(): string {
    return this._sortField;
  }

  public get sortOrder(): string {
    return this._sortOrder;
  }

  public onDataChanged: EventEmitter<void> = new EventEmitter<void>();
  public onPageChanged: EventEmitter<void> = new EventEmitter<void>();
  public onSortChanged: EventEmitter<void> = new EventEmitter<void>();

  constructor(data: any[] = []) {
    this.setNewArray(data);
  }

  public getData(): Observable<any> {
    return new Observable<any>(observer => {
      const count_page = !this._data.length ? 0 : Math.floor(this._data.length / this.pageSize);

      if (count_page < this.page) {
        return observer.complete();
      }

      const pos = this.page * this.pageSize;
      const data = this._data.slice(pos, pos + this.pageSize);

      data.forEach((it: any) => observer.next(it));

      observer.complete();
    });
  }

  public setSort(field: string, order: string) {
    if (this._sortField === field && this._sortOrder === order) {
      return;
    }

    this._sortField = field;
    this._sortOrder = order;

    this.applySort();

    this.onSortChanged.emit();
    this.onDataChanged.emit();
  }

  public setFullTextFilter(term: string, fields: string[]) {
    term = (term || '').toLowerCase();

    this._data = this.data.filter((it: any) =>
      fields.some((field: string) =>
        (it[field] || '').toLowerCase().includes(term)));

    this.calcCountPage();
    this.applySort();

    if (this._page + 1 > this._pageCount) {
      this._page = 0;
    }

    this.onPageChanged.emit();
    this.onDataChanged.emit();
  }

  public setNewArray(data: any) {
    this.data = data;
    this._data = _.clone(this.data);

    this.calcCountPage();

    this.onPageChanged.emit();
    this.onDataChanged.emit();
  }

  private calcCountPage() {
    this._pageCount = Math.ceil(this._data.length / this.pageSize);
  }

  private applySort() {
    let data = _.sortBy(this._data, [this._sortField]);

    if (this._sortOrder === 'desc') {
      data = _.reverse(data);
    }

    this._data = data;
  }
}
