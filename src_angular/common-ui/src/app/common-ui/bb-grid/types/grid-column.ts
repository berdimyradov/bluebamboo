export class GridColumn {
  public title: string;
  public width: string;
  public field: string;
  public pipe: string;
  public template: string;
  public notSortAble: boolean;
}
