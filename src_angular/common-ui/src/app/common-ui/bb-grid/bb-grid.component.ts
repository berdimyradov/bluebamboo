import {
  Component, ComponentFactory, ComponentRef, EventEmitter, OnInit, Output, ViewChild, ViewContainerRef, Input,
  ViewEncapsulation
} from '@angular/core';
import { GridColumn, IDataProvider, IGrid, IGridTemplateBuilder, DefaultGridTemplateBuilder } from './types';
import { DynamicTypeBuilderService } from './dynamic-type-builder.service';

@Component({
  selector: 'bb-grid',
  templateUrl: './bb-grid.component.html',
  styleUrls: ['./bb-grid.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BbGridComponent implements OnInit {
  private static waitBeforBuild = 50;

  private buildTimeOut: any;
  private componentRef: ComponentRef<IGrid>;
  private gridBuilder: IGridTemplateBuilder;

  @Input() private modules: any[] = [];
  @Input('sort-order') public defaultSortColumnOrder = 'asc';
  @Input('sort-column') public defaultSortColumnIdx: number;

  @Output('on-row-click') public onRowClick: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('gridTarget', {read: ViewContainerRef}) private target: ViewContainerRef;

  public disabled = false;
  public dataProvider: IDataProvider;
  public columnsDef: GridColumn[] = [];
  public data: any[] = [];
  public sortColumnIdx: number = null;
  public sortColumnOrder = 'asc';

  constructor(private typeBuilder: DynamicTypeBuilderService) {
    this.gridBuilder = new DefaultGridTemplateBuilder();
  }

  ngOnInit() {
  }

  public addColumn(column: GridColumn) {
    this.columnsDef.push(column);

    this.buildTemplate();
  }

  public setDataProvider(dataProvider: IDataProvider) {
    this.dataProvider = dataProvider;

    this.dataProvider.onDataChanged.subscribe(() => this.getDataFromProvider());

    this.getDataFromProvider();

    if (this.defaultSortColumnIdx >= 0) {
      this.sortColumnOrder = this.defaultSortColumnOrder;
      this.sortColumnIdx = this.defaultSortColumnIdx;

      this.dataProvider.setSort(this.columnsDef[this.sortColumnIdx].field, this.sortColumnOrder);
    }
  }

  public setSortOrder(columnIdx: number) {
    if (this.disabled || this.columnsDef[columnIdx].notSortAble) {
      return;
    }

    if (this.sortColumnIdx !== columnIdx) {
      this.sortColumnIdx = columnIdx;
      this.sortColumnOrder = 'asc';
    } else {
      this.sortColumnOrder = this.sortColumnOrder === 'asc' ? 'desc' : 'asc';
    }

    this.dataProvider.setSort(this.columnsDef[this.sortColumnIdx].field, this.sortColumnOrder);
  }

  private getDataFromProvider() {
    this.data = [];
    this.dataProvider.getData()
      .subscribe( result => this.data.push(result) );
  }

  private buildTemplate() {
    if (this.buildTimeOut) {
      clearTimeout(this.buildTimeOut);
    }

    this.buildTimeOut = setTimeout(() => {
      if (this.componentRef) {
        this.componentRef.destroy();
      }

      // here we get a TEMPLATE with dynamic content === TODO
      const template: string = this.gridBuilder.getTemplate(this.columnsDef);
      // here we get Factory (just compiled or from cache)
      this.typeBuilder
        .createComponentFactory(template, this.modules)
        .then((factory: ComponentFactory<IGrid>) => {
          // Target will instantiate and inject component (we'll keep reference to it)
          this.componentRef = this.target.createComponent(factory);
          // let's inject @Inputs to component instance
          const component = this.componentRef.instance;

          component.grid = this;
        });
    }, BbGridComponent.waitBeforBuild);
  }
}
