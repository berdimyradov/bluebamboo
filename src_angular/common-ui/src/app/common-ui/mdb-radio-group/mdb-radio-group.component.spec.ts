import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbRadioGroupComponent } from './mdb-radio-group.component';

describe('MdbRadioGroupComponent', () => {
  let component: MdbRadioGroupComponent;
  let fixture: ComponentFixture<MdbRadioGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbRadioGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbRadioGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
