import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mdb-radio-group',
  templateUrl: './mdb-radio-group.component.html',
  styleUrls: ['./mdb-radio-group.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MdbRadioGroupComponent {
  @Input() public title = '';
  @Input() public inline = false;
}
