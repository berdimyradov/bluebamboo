import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbCardCollapseComponent } from './mdb-card-collapse.component';

describe('MdbCardCollapseComponent', () => {
  let component: MdbCardCollapseComponent;
  let fixture: ComponentFixture<MdbCardCollapseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbCardCollapseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbCardCollapseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
