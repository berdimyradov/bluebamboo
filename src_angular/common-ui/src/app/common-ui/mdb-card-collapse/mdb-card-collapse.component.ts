import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CommonService } from '../common.service';

@Component({
  selector: 'mdb-card-collapse',
  templateUrl: './mdb-card-collapse.component.html',
  styleUrls: ['./mdb-card-collapse.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MdbCardCollapseComponent implements OnInit {
  public id: string;

  constructor(
    private commonService: CommonService
  ) {}

  ngOnInit() {
    this.id = 'mdb-card-collapse-' + this.commonService.uniqId;
  }
}
