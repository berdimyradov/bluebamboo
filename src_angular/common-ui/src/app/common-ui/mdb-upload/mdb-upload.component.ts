import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { UploadService } from '../upload.service';

@Component({
  selector: 'mdb-upload',
  templateUrl: './mdb-upload.component.html',
  styleUrls: ['./mdb-upload.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MdbUploadComponent implements OnInit {
  private _photo: any;

  @Input() public accept;
  @Input() public photo: string;
  @Input('is-edit') public isEdit = true;
  @Output() public photoChange = new EventEmitter<any>();

  constructor(
    private uploadService: UploadService
  ) {}

  ngOnInit() {
    this._photo = this.uploadService.getImageByName(this.photo);
  }

  public onChange($event) {
    const [file] = $event.target.files;

    if (!file) {
      return;
    }

    this.uploadService.fixFileOrientationAndResize(file).then((fixedFile: any) => {
      const src = this.uploadService.getImageUrl(fixedFile);

      this._photo = src;

      this.photoChange.emit({
        src,
        file: fixedFile
      })
    });
  }
}
