import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbUploadComponent } from './mdb-upload.component';

describe('MdbUploadComponent', () => {
  let component: MdbUploadComponent;
  let fixture: ComponentFixture<MdbUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
