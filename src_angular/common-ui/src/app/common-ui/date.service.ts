import { Injectable } from '@angular/core';

@Injectable()
export class DateService {

  constructor() { }

  public Weekdays: string[] = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  public Months: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  public getFirstDay(date: Date =  new Date()): Date {
    let month = date.getMonth(),
      year = date.getFullYear();
    return new Date(year, month, 1, 0, 0);
  }

  public addMonth(date: Date = new Date(), step: number): Date {
    let month = date.getMonth(),
      year = date.getFullYear();
    return new Date(year, month + step, 1, 0, 0);
  }

  public dayToNumber(day: string): number {
    const dayIndex: number = this.Weekdays.indexOf(day);

    if (dayIndex === 6) {
      return 0;
    }

    return dayIndex + 1;
  }

  public numberToDay(number: number): string {
    if (number === 0) {
      return this.Weekdays[6];
    }

    return this.Weekdays[number - 1];
  }
}
