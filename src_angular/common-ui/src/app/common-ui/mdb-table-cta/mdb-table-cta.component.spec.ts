import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbTableCtaComponent } from './mdb-table-cta.component';

describe('MdbTableCtaComponent', () => {
  let component: MdbTableCtaComponent;
  let fixture: ComponentFixture<MdbTableCtaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbTableCtaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbTableCtaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
