import {
  AfterViewInit, Component, ElementRef, ViewChild, forwardRef, OnChanges, OnInit, Input,
  ViewEncapsulation
} from '@angular/core';

declare let $: any;

@Component({
  selector: 'mdb-table-cta',
  templateUrl: './mdb-table-cta.component.html',
  styleUrls: ['./mdb-table-cta.component.scss'],
  providers: [],
  encapsulation: ViewEncapsulation.None
})
export class MdbTableCtaComponent implements OnInit   {
  @Input() public title = '';
  @Input() public objects: any[] = [];
  @Input() public label: (obj: any) => string;
  @Input() public select: (obj: any) => void = null;
  @Input() public owner: any;

  constructor() {
  }

  ngOnInit(): void {
  }

  selectObject (obj: any) {
    if (this.select == null) return;
    this.select(obj);
  }

}
