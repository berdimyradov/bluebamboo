import {Component, ElementRef, forwardRef, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {FileUploader} from 'ng2-file-upload';

@Component({
  selector: 'mdb-image-upload',
  templateUrl: './mdb-image-upload.component.html',
  styleUrls: ['./mdb-image-upload.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MdbImageUploadComponent),
      multi: true
    }
  ],
  encapsulation: ViewEncapsulation.None
})
export class MdbImageUploadComponent implements OnInit, ControlValueAccessor {
  ngOnInit(): void {
  }
  @Input() public title = '';
  @Input() public accept;
  // @ViewChild('input') private fileInput: ElementRef;
  private onChange: any = () => { };
  private onTouched: any = () => { };
  private _value: Object;
  public get value(): Object {
    return this._value;
  }
  public set value(v: Object) {
    this._value = v;
    this.onChange(v);
    this.onTouched();
  }
  public url: string 

  constructor() { 
    this.url =  '/api/v1/cms/fileupload/templateimage'
    this.uploader = new FileUploader({url: this.url})
    this.uploader.autoUpload = true;
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      var res = JSON.parse(response);
      this.uploader.clearQueue();
      this.value = {
        previewUrl : `/downloads/templateimages/` + res.filename,
        fileLocation : res.imagesrc
      } 
    
    };
  }


  writeValue(obj: string): void {
    this._value = obj
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }

  public uploader:FileUploader ;
  public hasBaseDropZoneOver:boolean = false;
  public hasAnotherDropZoneOver:boolean = false;

  public onFileSelected():void {
    console.log('file selected');
    this.uploader.uploadAll()
  }

}
