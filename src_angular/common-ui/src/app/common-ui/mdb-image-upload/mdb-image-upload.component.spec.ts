import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbImageUploadComponent } from './mdb-image-upload.component';

describe('MdbImageUploadComponent', () => {
  let component: MdbImageUploadComponent;
  let fixture: ComponentFixture<MdbImageUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbImageUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbImageUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
