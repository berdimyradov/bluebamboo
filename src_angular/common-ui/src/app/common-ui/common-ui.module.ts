import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdbInputComponent } from './mdb-input/mdb-input.component';
import { MdbTableCtaComponent } from './mdb-table-cta/mdb-table-cta.component';
import { MdbTextareaComponent } from './mdb-textarea/mdb-textarea.component';
import { CommonService } from './common.service';
import { MdbCardRotateComponent } from './mdb-card-rotate/mdb-card-rotate.component';
import { EqualValidatorDirective } from './directive/equal-validator.directive';
import {ModalPlaceholderComponent} from './modal/modal-placeholder.component';
import {ModalService} from './modal/modal.service';
import {MDBCheckboxComponent} from './mdb-checkbox/mdb-checkbox.component';
import {MDBSelectComponent} from './mdb-select/mdb-select.component';
import { MdbUploadComponent } from './mdb-upload/mdb-upload.component';
import { MdbImageUploadComponent } from './mdb-image-upload/mdb-image-upload.component';
import { MdbCardComponent } from './mdb-card/mdb-card.component';
import { IsNullPipe, LeadZeroPipe, MomentDateTimePipe, MomentDatePipe, MomentTimePipe, SafePipe, InvoiceFormatTypePipe } from './pipe';
import { MdbTooltipDirective } from './directive/mdb-tooltip.directive';
import { MdbDatepickerDirective } from './directive/mdb-datepicker.directive';
import { MdbInputDatepickerComponent } from './mdb-input-datepicker/mdb-input-datepicker.component';
import { MdbSelectItemComponent } from './mdb-select/mdb-select-item/mdb-select-item.component';
import { BbGridComponent } from './bb-grid/bb-grid.component';
import { BbGridColumnComponent } from './bb-grid/bb-grid-column/bb-grid-column.component';
import { BbGridDataSourceArrayComponent } from './bb-grid/bb-grid-data-source-array/bb-grid-data-source-array.component';
import {BbGridPaginationComponent} from './bb-grid/bb-grid-paginator/bb-grid-pagination.component';
import {DynamicTypeBuilderService} from './bb-grid/dynamic-type-builder.service';
import {IndexArrayPipe} from './pipe';
import {MDBColorPickerComponent} from './mdb-color-picker/mdb-color-picker.component';
import {SlidePanelComponent} from './slide-panel/slide-panel.component';
import {DateService} from './date.service';
import { TimeDirective } from './time/time.directive';
import { MdbRadioComponent } from './mdb-radio/mdb-radio.component';
import {FileSelectDirective, FileDropDirective} from 'ng2-file-upload';
import { MdbTabsComponent } from './mdb-tabs/mdb-tabs.component';
import { MdbTabComponent } from './mdb-tabs/mdb-tab/mdb-tab.component';
import { MdbPhotoUploadComponent } from './mdb-photo-upload/mdb-photo-upload.component';
import { MdbPhotoUploadModalComponent } from './mdb-photo-upload/mdb-photo-upload-modal/mdb-photo-upload-modal.component';
import { UploadService } from './upload.service';
import { MdbCardCollapseComponent } from './mdb-card-collapse/mdb-card-collapse.component';
import { MdbRadioGroupComponent } from './mdb-radio-group/mdb-radio-group.component';
import { MdbCardPhotoCollapseComponent } from './mdb-card-photo-collapse/mdb-card-photo-collapse.component';
import { MdbDragndropComponent } from './mdb-dragndrop/mdb-dragndrop.component';
import { ShortAddressPipe } from './pipe';

@NgModule({
  imports: [
    BrowserModule, BrowserAnimationsModule, CommonModule, FormsModule
  ],
  declarations: [MdbInputComponent, MdbTableCtaComponent, MdbTextareaComponent, MdbCardRotateComponent, EqualValidatorDirective,
    ModalPlaceholderComponent, MDBCheckboxComponent, MDBSelectComponent, MdbUploadComponent, MdbCardComponent,
    IsNullPipe, InvoiceFormatTypePipe, LeadZeroPipe, MomentDateTimePipe, MomentDatePipe, MomentTimePipe, MdbTooltipDirective,
    MdbDatepickerDirective, MdbInputDatepickerComponent, MdbSelectItemComponent, MdbImageUploadComponent,
    BbGridComponent, BbGridColumnComponent, BbGridDataSourceArrayComponent, BbGridPaginationComponent, FileSelectDirective,
    IndexArrayPipe, MDBColorPickerComponent, SlidePanelComponent, TimeDirective, MdbRadioComponent, MdbTabsComponent,
    MdbTabComponent, MdbPhotoUploadComponent, MdbPhotoUploadModalComponent, SafePipe, MdbCardCollapseComponent,
    MdbRadioGroupComponent, MdbCardPhotoCollapseComponent, ShortAddressPipe,
    MdbDragndropComponent, FileDropDirective
  ],
  exports: [MdbInputComponent, MdbTableCtaComponent, MdbTextareaComponent, MdbCardRotateComponent, EqualValidatorDirective,
    ModalPlaceholderComponent, MDBCheckboxComponent, MDBSelectComponent, MdbUploadComponent, MdbCardComponent,
    IsNullPipe, InvoiceFormatTypePipe, LeadZeroPipe, MomentDateTimePipe, MomentDatePipe, MomentTimePipe, MdbTooltipDirective, FileSelectDirective,
    MdbDatepickerDirective, MdbInputDatepickerComponent, MdbSelectItemComponent, MdbImageUploadComponent,
    BbGridComponent, BbGridColumnComponent, BbGridDataSourceArrayComponent, IndexArrayPipe, MDBColorPickerComponent,
    SlidePanelComponent, TimeDirective, MdbRadioComponent, MdbTabsComponent, MdbTabComponent,
    MdbPhotoUploadComponent, MdbPhotoUploadModalComponent, SafePipe, MdbCardCollapseComponent, MdbRadioGroupComponent,
    MdbCardPhotoCollapseComponent, ShortAddressPipe,
    MdbDragndropComponent, FileDropDirective
  ],
  providers: [CommonService, ModalService, DynamicTypeBuilderService, DateService, UploadService, { provide: Window, useValue: window }]
})
export class CommonUiModule { }
