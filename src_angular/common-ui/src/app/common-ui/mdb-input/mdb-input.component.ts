import {
  AfterViewInit, Component, ElementRef, ViewChild, forwardRef, OnChanges, OnInit, Input,
  ViewEncapsulation
} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {CommonService} from '../common.service';
declare let $: any;

@Component({
  selector: 'mdb-input',
  templateUrl: './mdb-input.component.html',
  styleUrls: ['./mdb-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MdbInputComponent),
      multi: true
    }
  ],
  encapsulation: ViewEncapsulation.None
})
export class MdbInputComponent implements AfterViewInit, OnInit, OnChanges, ControlValueAccessor   {
  @ViewChild('inputElem') private inputElement: ElementRef;
  @Input() public id: string;
  @Input() public title = '';
  @Input() public icon = '';
  @Input() public type = 'text';
  @Input() public name = undefined;
  @Input() public readonly: boolean;
  private _value = '';
  private onChange: any = () => { };
  public onTouched: any = () => { };
  public get value(): string {
    return this._value;
  };
  public set value(val: string) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  };


  constructor(private commonService: CommonService) {
  }

  private updateControl() {
    setTimeout(() => {
      $(this.inputElement.nativeElement).change();
      $(this.inputElement.nativeElement).blur();
    });
  }

  ngOnInit(): void {
    this.id = 'mdb-input-' + this.commonService.uniqId;
  }

  ngAfterViewInit() {
    this.updateControl();
  }

  ngOnChanges() {
    this.updateControl();
  }

  writeValue(obj: any): void {
    if (obj !== null && obj !== undefined) {
      this._value = obj.toString();
    } else {
      this._value = '';
    }
    this.updateControl();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }
}
