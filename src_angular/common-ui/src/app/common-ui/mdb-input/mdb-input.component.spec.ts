import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbInputComponent } from './mdb-input.component';

describe('MdbInputComponent', () => {
  let component: MdbInputComponent;
  let fixture: ComponentFixture<MdbInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
