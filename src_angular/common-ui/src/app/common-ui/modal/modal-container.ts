export class ModalContainer {
    public destroy: Function;
    public componentIndex: number;

    public closeModal(): void {
        this.destroy();
    }
}
