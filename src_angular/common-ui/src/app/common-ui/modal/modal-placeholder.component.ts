import {Component, Injector, OnInit, ViewChild, ViewContainerRef, ViewEncapsulation} from "@angular/core";
import {ModalService} from "./modal.service";

@Component({
    selector: "modal-placeholder",
    template: `<div #modalplaceholder></div>`,
    styleUrls: ['./modal-placeholder.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ModalPlaceholderComponent implements OnInit {
    @ViewChild("modalplaceholder", {read: ViewContainerRef}) viewContainerRef:any;

    constructor(private modalService: ModalService, private injector: Injector) {

    }
    ngOnInit(): void {
        this.modalService.registerViewContainerRef(this.viewContainerRef);
        this.modalService.registerInjector(this.injector);
    }
}
