/**
    * Created by aleksay on 18.04.2017.
    * Project: tmp_booking
    */
import {AfterViewInit, Component, ElementRef, Input, ViewChild, forwardRef, ViewEncapsulation} from "@angular/core";
import {SelectOption} from "./types/select-option";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
declare let $:any;

@Component({
    selector: 'mdb-select',
    templateUrl: './mdb-select.component.html',
    styleUrls: ['./mdb-select.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => MDBSelectComponent),
            multi: true
        }
    ],
    encapsulation: ViewEncapsulation.None
})
export class MDBSelectComponent implements AfterViewInit, ControlValueAccessor   {
    @ViewChild('selectElem') private selectElement:ElementRef;
    private onChange: any = () => { };
    private onTouched: any = () => { };
    @Input() public options: SelectOption[] = [];
    @Input() public title: string = '';
    @Input('select-option') public select_option: string = 'Bitte wählen';
    @Input() public icon = '';
    @Input() public disabled = false;
    public value: string = null;

    constructor(){
    }

    public updateControl(){
        setTimeout(() => $(this.selectElement.nativeElement).material_select());
    }

    ngAfterViewInit(){
        $(this.selectElement.nativeElement).change(() => this.setValue($(this.selectElement.nativeElement).val()));
        this.updateControl();
    }

    ngOnChanges() {
        this.updateControl();
    }

    private setValue(v: string){
        this.value = v;
        this.onChange(v);
        this.onTouched();
    }

    writeValue(obj: any): void {
        if (obj){
            this.value = obj.toString();
        } else {
            this.value = "";
        }
        $(this.selectElement.nativeElement).val(this.value);
        this.updateControl();
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
    }
}
