import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbSelectItemComponent } from './mdb-select-item.component';

describe('MdbSelectItemComponent', () => {
  let component: MdbSelectItemComponent;
  let fixture: ComponentFixture<MdbSelectItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbSelectItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbSelectItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
