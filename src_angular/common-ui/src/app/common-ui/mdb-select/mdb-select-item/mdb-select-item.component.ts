import { Component, OnInit, Input } from '@angular/core';
import {MDBSelectComponent} from "../mdb-select.component";
import {SelectOption} from "../types/select-option";

@Component({
  selector: 'mdb-select-item',
  template: ''
})
export class MdbSelectItemComponent implements OnInit {
  @Input() private title: string;
  @Input() private code: string;
  constructor(private select: MDBSelectComponent) { }

  ngOnInit() {
    let item: SelectOption = new SelectOption();
    item.title = this.title;
    item.code = this.code;
    this.select.options.push(item);
  }

}
