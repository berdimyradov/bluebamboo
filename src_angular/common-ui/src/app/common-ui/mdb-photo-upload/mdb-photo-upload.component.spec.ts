import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbPhotoUploadComponent } from './mdb-photo-upload.component';

describe('MdbPhotoUploadComponent', () => {
  let component: MdbPhotoUploadComponent;
  let fixture: ComponentFixture<MdbPhotoUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbPhotoUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbPhotoUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
