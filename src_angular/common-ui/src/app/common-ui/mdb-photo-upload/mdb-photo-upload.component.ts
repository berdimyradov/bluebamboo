import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { UploadService } from '../upload.service';

@Component({
  selector: 'mdb-photo-upload',
  templateUrl: './mdb-photo-upload.component.html',
  styleUrls: ['./mdb-photo-upload.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MdbPhotoUploadComponent implements OnInit, OnChanges {
  @Input() public photo: string;
  @Input() public expand = true;
  @Input('is-edit') public isEdit = false;
  @Output() public photoChange = new EventEmitter<any>();

  private _photo: any;

  constructor(
    private uploadService: UploadService
  ) { }

  ngOnInit() {
    this._photo = this.uploadService.getImageUrl(this.photo);
  }

  ngOnChanges() {
    this._photo = this.uploadService.getImageUrl(this.photo);
  }

  public openModal() {
    this.uploadService.open()
      .then((file: File) => {
        if (!file) {
          return;
        }

        this.uploadService.fixFileOrientationAndResize(file).then((fixedFile: any) => {
          const src = this.uploadService.getImageUrl(fixedFile);

          this._photo = src;

          this.photoChange.emit({
            src,
            file: fixedFile
          })
        });
      });
  }
}
