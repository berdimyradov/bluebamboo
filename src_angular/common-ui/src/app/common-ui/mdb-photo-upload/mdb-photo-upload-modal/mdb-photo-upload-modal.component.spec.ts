import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbPhotoUploadModalComponent } from './mdb-photo-upload-modal.component';

describe('MdbPhotoUploadModalComponent', () => {
  let component: MdbPhotoUploadModalComponent;
  let fixture: ComponentFixture<MdbPhotoUploadModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbPhotoUploadModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbPhotoUploadModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
