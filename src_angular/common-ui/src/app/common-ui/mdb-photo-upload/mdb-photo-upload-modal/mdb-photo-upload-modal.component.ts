import { Component, OnInit } from '@angular/core';
import { Modal } from '../../modal/modal.decarator';

@Component({
  selector: 'mdb-photo-upload-modal',
  templateUrl: './mdb-photo-upload-modal.component.html',
  styleUrls: ['./mdb-photo-upload-modal.component.scss']
})
@Modal()
export class MdbPhotoUploadModalComponent implements OnInit {
  private destroy: Function;
  private closeModal: Function;
  private onClose: Function;

  constructor() { }

  ngOnInit() {
  }

  public change($event) {
    this.onClose($event.target.files[0]);
    this.close();
  }

  public close() {
    this.closeModal();
    this.destroy();
  }
}
