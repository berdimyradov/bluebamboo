import { Component, forwardRef, Input, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CommonService } from '../common.service';

declare let $: any;

@Component({
  selector: 'mdb-radio',
  templateUrl: './mdb-radio.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MdbRadioComponent),
      multi: true
    }
  ]
})
export class MdbRadioComponent implements ControlValueAccessor {
  @Input() public id = this.commonService.uniqId;
  @Input() public title = '';
  @Input('return-value') public returnValue: string = null;

  private _value = '';
  private onChange: any = () => {};
  private onTouched: any = () => {};

  public get value(): string {
    return this._value;
  };

  public set value(val: string) {
    this._value = val;

    this.onChange(val);
    this.onTouched();
  };

  constructor(
    private commonService: CommonService
  ) {}

  writeValue(obj: any): void {
    if (obj) {
      this._value = obj.toString();
    } else {
      this._value = '';
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }
}
