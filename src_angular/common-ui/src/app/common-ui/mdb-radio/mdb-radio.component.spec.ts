import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbRadioComponent } from './mdb-radio.component';

describe('MdbRadioComponent', () => {
  let component: MdbRadioComponent;
  let fixture: ComponentFixture<MdbRadioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbRadioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbRadioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
