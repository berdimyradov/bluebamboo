import { Inject, Injectable } from '@angular/core';
import { ModalService } from './modal/modal.service';
import { CommonUiModule } from './common-ui.module';
import { MdbPhotoUploadModalComponent } from './mdb-photo-upload/mdb-photo-upload-modal/mdb-photo-upload-modal.component';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';

@Injectable()
export class UploadService {
  constructor(
    @Inject(Window) private window: Window,
    private modal: ModalService,
    private http: Http
  ) { }

  public open() {
    return new Promise<File>(resolve => {
      this.modal.create(CommonUiModule, MdbPhotoUploadModalComponent, {
        onClose: resolve
      });
    });
  }

  public fixFileOrientationAndResize(file: File): Promise<any> {
    return new Promise(resolve => {
      this.getOrientation(file).then((orientation: number) => {
        this.resetOrientationAndResize(file, orientation).then((fixedFile: any) => {
          resolve(fixedFile);
        });
      });
    });
  }

  private convertFileToImage(file: File): Promise<HTMLImageElement> {
    return new Promise(resolve => {
      const img = new Image();

      img.src = this.getImageSrc(file);
      img.onload = () => resolve(img);
    });
  }

  private getOrientation(file: File): Promise<number> {
    return new Promise(resolve => {
      const reader = new FileReader();

      reader.onload = (e: any) => {
        const view = new DataView(e.target.result);

        if (view.getUint16(0, false) !== 0xFFD8) {
          resolve(-2);

          return;
        }

        const length = view.byteLength;
        let offset = 2;

        while (offset < length) {
          const marker = view.getUint16(offset, false);

          offset += 2;

          if (marker === 0xFFE1) {
            if (view.getUint32(offset += 2, false) !== 0x45786966) {
              resolve(-1);

              return;
            }

            const little = view.getUint16(offset += 6, false) === 0x4949;

            offset += view.getUint32(offset + 4, little);

            const tags = view.getUint16(offset, little);

            offset += 2;

            for (let i = 0; i < tags; i++) {
              if (view.getUint16(offset + (i * 12), little) === 0x0112) {
                resolve(view.getUint16(offset + (i * 12) + 8, little));

                return;
              }
            }
          } else if ((marker & 0xFF00) !== 0xFF00) {
            break;
          } else {
            offset += view.getUint16(offset, false);
          }
        }

        resolve(-1);
      };

      reader.readAsArrayBuffer(file);
    });
  }

  private resetOrientationAndResize(file: File, srcOrientation: number): Promise<any> {
    return new Promise(resolve => {
      this.convertFileToImage(file).then((img: HTMLImageElement) => {
        let width = img.width;
        let height = img.height;
        const canvas = this.window.document.createElement('canvas');
        const ctx = canvas.getContext('2d');
        const maxWidth = 2000;
        const maxHeight = 2000;

        if (width > height) {
          if (width > maxWidth) {
            height *= maxWidth / width;
            width = maxWidth;
          }
        } else {
          if (height > maxHeight) {
            width *= maxHeight / height;
            height = maxHeight;
          }
        }

        if (4 < srcOrientation && srcOrientation < 9) {
          canvas.width = height;
          canvas.height = width;
        } else {
          canvas.width = width;
          canvas.height = height;
        }

        switch (srcOrientation) {
          case 2: ctx.transform(-1, 0, 0, 1, width, 0); break;
          case 3: ctx.transform(-1, 0, 0, -1, width, height ); break;
          case 4: ctx.transform(1, 0, 0, -1, 0, height ); break;
          case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
          case 6: ctx.transform(0, 1, -1, 0, height , 0); break;
          case 7: ctx.transform(0, -1, -1, 0, height , width); break;
          case 8: ctx.transform(0, -1, 1, 0, 0, width); break;
          default: break;
        }

        ctx.drawImage(img, 0, 0, width, height);

        resolve(this.convertCanvasToFile(canvas, file.name, file.type, 0.8));
      });
    });
  };

  private convertCanvasToFile(canvas, name, type, quality) {
    const binStr = this.window.atob(canvas.toDataURL(type, quality).split(',')[1]);
    const len = binStr.length;
    const arr = new Uint8Array(len);

    for (let i = 0; i < len; i++) {
      arr[i] = binStr.charCodeAt(i);
    }

    const file: any = new Blob([arr], { type });

    file.name = name;

    return file;
  }

  public save(file: File): Observable<any> {
    const formData = new FormData();

    formData.append('file', file, file.name);

    return this.http.post('/api/v1/cms/fileupload/image', formData)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public getImageUrl(image: any): any {
    const { hostname, protocol } = this.window.location;

    if (image) {
      if (image instanceof Blob || image instanceof File) {
        return this.getImageSrc(image);
      } else {
        return this.getImageByName(image);
      }
    }

    return `${protocol}//${hostname}/admin/img/contact.jpg`;
  }

  public getImageByName(name) {
    const { hostname, protocol } = this.window.location;

    if (name) {
      return `${protocol}//${hostname}/api/v1/cms/filedownload/image/${name}`
    }

    return '';
  }

  public getImageSrc(image) {
    return this.window.URL.createObjectURL(image);
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;

    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);

      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    console.error(errMsg);

    return Observable.throw(errMsg);
  }
}
