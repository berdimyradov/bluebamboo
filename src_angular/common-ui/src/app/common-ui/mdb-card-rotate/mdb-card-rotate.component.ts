import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'mdb-card-rotate',
  templateUrl: './mdb-card-rotate.component.html',
  styleUrls: ['./mdb-card-rotate.component.scss']
})
export class MdbCardRotateComponent implements OnInit {
  @Input() flipped: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
