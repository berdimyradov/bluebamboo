import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbCardRotateComponent } from './mdb-card-rotate.component';

describe('MdbCardRotateComponent', () => {
  let component: MdbCardRotateComponent;
  let fixture: ComponentFixture<MdbCardRotateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbCardRotateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbCardRotateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
