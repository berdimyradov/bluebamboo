import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbCardPhotoCollapseComponent } from './mdb-card-photo-collapse.component';

describe('MdbCardPhotoCollapseComponent', () => {
  let component: MdbCardPhotoCollapseComponent;
  let fixture: ComponentFixture<MdbCardPhotoCollapseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbCardPhotoCollapseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbCardPhotoCollapseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
