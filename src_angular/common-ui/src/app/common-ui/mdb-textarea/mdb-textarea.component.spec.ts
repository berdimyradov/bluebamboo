import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbTextareaComponent } from './mdb-textarea.component';

describe('MdbTextareaComponent', () => {
  let component: MdbTextareaComponent;
  let fixture: ComponentFixture<MdbTextareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbTextareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbTextareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
