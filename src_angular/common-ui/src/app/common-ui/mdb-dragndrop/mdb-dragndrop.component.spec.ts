import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbDragndropComponent } from './mdb-dragndrop.component';

describe('MdbDragndropComponent', () => {
  let component: MdbDragndropComponent;
  let fixture: ComponentFixture<MdbDragndropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbDragndropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbDragndropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
