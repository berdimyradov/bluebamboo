import {Component, ElementRef, OnInit, ViewChild, Input, Output, EventEmitter} from '@angular/core';
import {FileItem, ParsedResponseHeaders, FileUploader} from 'ng2-file-upload/ng2-file-upload';
import {Http} from '@angular/http';

@Component({
  selector: 'mdb-dragndrop',
  templateUrl: './mdb-dragndrop.component.html',
  styleUrls: ['./mdb-dragndrop.component.css']
})
export class MdbDragndropComponent implements OnInit {
  public uploader: FileUploader;
  @Input() url: string;
  @Input() accept: string = "text/plain, text/csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel";
  @ViewChild('fileInput') inputEl: ElementRef;
  @Output() onSuccess = new EventEmitter();
  @Output() onError = new EventEmitter();

  constructor(private http: Http) { }

  ngOnInit() {
    this.uploader = new FileUploader({url: this.url});

    this.uploader.onSuccessItem = (item: FileItem, response: string, status: number, headers: ParsedResponseHeaders) => {
      console.log('onSuccessItem');
      this.onSuccess.emit(response);
    };

    this.uploader.onErrorItem = (item: FileItem, response: string, status: number, headers: ParsedResponseHeaders) => {
      console.error('onErrorItem');
      this.onError.emit(response);
    };

    this.uploader.onCompleteAll = () => {
      console.log('upload complete all');
    };
  }

  onFileSelected(event: Event) {
    console.log('onFileSelected triggered', event);
    this.uploader.uploadAll();
  }

  dropped(fileList: Event) {
    console.log('on dropped triggered', fileList);
    this.uploader.uploadAll();
  }

}
