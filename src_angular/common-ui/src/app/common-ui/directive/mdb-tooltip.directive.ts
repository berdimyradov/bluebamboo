import {Directive, ElementRef, Input, OnDestroy, OnChanges, AfterViewInit, Inject} from '@angular/core';
declare let $:any;

@Directive({
  selector: '[mdbTooltip]'
})
export class MdbTooltipDirective implements AfterViewInit, OnChanges, OnDestroy {
  @Input() public show: boolean;
  @Input() public timeout = 10000;

  private firstRun = true;
  private timer: number;

  constructor(
    @Inject(Window) private window: Window,
    private element: ElementRef,
  ) {}

  ngAfterViewInit() {
    $(this.element.nativeElement).tooltip();
  }

  ngOnChanges() {
    if (this.firstRun || typeof this.show === 'undefined') {
      this.firstRun = false;

      return;
    }

    if (this.show) {
      $(this.element.nativeElement).tooltip('show');

      this.timer = this.window.setTimeout(() => {
        this.hide();
      }, this.timeout);
    } else {
      this.hide();
    }
  }

  ngOnDestroy() {
    this.cancelTimer();

    $(this.element.nativeElement).tooltip('dispose');
  }

  private hide() {
    this.cancelTimer();

    $(this.element.nativeElement).tooltip('hide');
  }

  private cancelTimer() {
    this.timer && this.window.clearTimeout(this.timer);
  }
}
