import { Directive, ElementRef, OnInit } from '@angular/core'
import { NgModel } from '@angular/forms';
import * as moment from 'moment';
declare let $:any;

@Directive({
  selector: '[ngModel][mdbDatepicker]'
})
export class MdbDatepickerDirective implements OnInit{

  constructor(private element: ElementRef, private model: NgModel) {
  }

  ngOnInit() {
    $(this.element.nativeElement).pickadate( {
      format: 'dd.mm.yyyy',
      formatSubmit: 'yyyy-mm-dd',
      onSet: (context) => {
        this.model.update.emit(moment(context.select).format('YYYY-MM-DD'));
      }
    });
  }

}
