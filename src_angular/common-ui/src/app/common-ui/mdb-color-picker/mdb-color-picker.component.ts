/**
    * Created by aleksay on 29.05.2017.
    * Project: tmp_booking
    */
import {AfterViewInit, Component, ElementRef, ViewChild, forwardRef, Input, ViewEncapsulation} from "@angular/core";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {CommonService} from "../common.service";
declare let $:any;

@Component({
  selector: 'mdb-color-picker',
  templateUrl: './mdb-color-picker.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MDBColorPickerComponent),
      multi: true
    }
  ],
  encapsulation: ViewEncapsulation.None
})
export class MDBColorPickerComponent implements AfterViewInit, ControlValueAccessor   {
    @ViewChild('inputElement') private inputElement:ElementRef;
    @Input('title') public title:string = "Select color";
    private _value: string = null;
    public id: string;
    private onChange: any = () => { };
    private onTouched: any = () => { };
    public get value(): string {
        return this._value;
    };
    public set value(val: string) {
        this._value = val;
        this.onChange(val);
        this.onTouched();
    };

    constructor(private commonService: CommonService){
        this.id = "color-picker-"+commonService.uniqId.toString();
    }

    private updateControl(){
        setTimeout(() => {
            $(this.inputElement.nativeElement).change();
            $(this.inputElement.nativeElement).blur();
        });
    }

    ngAfterViewInit(){
        this.updateControl();
        setTimeout(() => {
            $(this.inputElement.nativeElement).change(() => {
                let v = $(this.inputElement.nativeElement).val();
                this.value = v;
            });
            $(this.inputElement.nativeElement.parentElement).colorpicker();
        });
    }

    ngOnChanges() {
        this.updateControl();
    }

    writeValue(obj: string): void {
        this._value = obj;
        this.updateControl();
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
    }
}
