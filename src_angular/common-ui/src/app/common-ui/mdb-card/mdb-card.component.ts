import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'mdb-card',
  templateUrl: './mdb-card.component.html',
  styleUrls: ['./mdb-card.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MdbCardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
