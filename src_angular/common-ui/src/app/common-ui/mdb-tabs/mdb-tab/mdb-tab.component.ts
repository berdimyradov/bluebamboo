import { Component, Input } from '@angular/core';

@Component({
  selector: 'mdb-tab',
  templateUrl: './mdb-tab.component.html',
  styleUrls: ['./mdb-tab.component.scss']
})
export class MdbTabComponent {
  @Input('tabTitle') title: string;
  @Input() active = false;
}
