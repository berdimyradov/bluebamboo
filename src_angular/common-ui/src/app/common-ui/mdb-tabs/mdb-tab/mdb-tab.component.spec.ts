import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbTabComponent } from './mdb-tab.component';

describe('MdbTabComponent', () => {
  let component: MdbTabComponent;
  let fixture: ComponentFixture<MdbTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
