import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbTabsComponent } from './mdb-tabs.component';

describe('MdbTabsComponent', () => {
  let component: MdbTabsComponent;
  let fixture: ComponentFixture<MdbTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
