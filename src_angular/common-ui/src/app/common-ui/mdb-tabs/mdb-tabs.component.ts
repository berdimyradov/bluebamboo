import { Component, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { MdbTabComponent } from './mdb-tab/mdb-tab.component';

@Component({
  selector: 'mdb-tabs',
  templateUrl: './mdb-tabs.component.html',
  styleUrls: ['./mdb-tabs.component.scss']
})
export class MdbTabsComponent implements AfterContentInit {
  @ContentChildren(MdbTabComponent) tabs: QueryList<MdbTabComponent>;

  // contentChildren are set
  ngAfterContentInit() {
    // get all active tabs
    let activeTabs = this.tabs.filter((tab: MdbTabComponent) => tab.active);

    // if there is no active tab set, activate the first
    if (!activeTabs.length) {
      this.selectTab(this.tabs.first);
    }
  }

  selectTab(tab: MdbTabComponent){
    // deactivate all tabs
    this.tabs.toArray().forEach(tab => tab.active = false);

    // activate the tab the user has clicked on.
    tab.active = true;
  }
}
