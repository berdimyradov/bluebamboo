import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdbInputDatepickerComponent } from './mdb-input-datepicker.component';

describe('MdbInputDatepickerComponent', () => {
  let component: MdbInputDatepickerComponent;
  let fixture: ComponentFixture<MdbInputDatepickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdbInputDatepickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdbInputDatepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
