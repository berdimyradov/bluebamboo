import { Pipe, PipeTransform } from '@angular/core';
/*
 * Pipe for formatting payment method in
 * invoices component
*/
@Pipe({ name: 'invoiceFormatType' })
export class InvoiceFormatTypePipe implements PipeTransform {
  transform(value: number): string {
    let formatted = '';
    switch (+value) {
      case 1:
        formatted ='PAGE.INVOICEREQUESTS.TYPE.CREATE';
        break;
      case 2:
        formatted = 'PAGE.INVOICEREQUESTS.TYPE.PREPRINT';
        break;
      case 3:
        formatted ='PAGE.INVOICEREQUESTS.TYPE.REMIND';
        break;
      default:
        formatted = 'unknown';
    }
    return formatted;
  }
}

