import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'leadZero'
})
export class LeadZeroPipe implements PipeTransform {

  transform(value: any, length: number): string {
    if (value === undefined || value === null || value === ''){
      return value;
    }
    let s = value.toString();
    for (let i = s.length; i < length; i++) {
      s = '0' + s;
    }
    return s;
  }

}
