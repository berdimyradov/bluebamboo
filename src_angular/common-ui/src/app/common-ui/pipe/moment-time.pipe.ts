import {Pipe} from '@angular/core';
import {MomentDateTimePipe} from './moment-date-time.pipe';

@Pipe({
  name: 'momentTime'
})
export class MomentTimePipe extends MomentDateTimePipe {
  transform(value: any, format = 'HH:mm'): any {
    return super.transform(value, format);
  }
}
