import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shortAddress'
})
export class ShortAddressPipe implements PipeTransform {
  transform(model = { street: '', zip: '', city: '' }): string {
    const { street, zip, city } = model;
    let address = '';

    if (street) {
      address += street + '\n';
    }

    if (zip) {
      address += zip;
    }

    if (city) {
      if (zip) {
        address += ' ';
      }

      address += city;
    }

    return address;
  }
}
