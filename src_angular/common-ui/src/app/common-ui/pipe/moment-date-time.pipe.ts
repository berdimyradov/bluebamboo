import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment/moment';

@Pipe({
  name: 'momentDateTime'
})
export class MomentDateTimePipe implements PipeTransform {
  transform(value: any, format: string = 'DD.MM.YYYY HH:mm'): any {
    if (value === undefined || value === null || value === '') {
      return '';
    }
    const momentDate = moment(value);
    return momentDate.format(format);
  }
}
