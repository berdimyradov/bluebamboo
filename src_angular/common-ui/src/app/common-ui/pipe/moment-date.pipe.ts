import { Pipe } from '@angular/core';
import {MomentDateTimePipe} from './moment-date-time.pipe';

@Pipe({
  name: 'momentDate'
})
export class MomentDatePipe extends MomentDateTimePipe {
  transform(value: any, format: string = 'DD.MM.YYYY'): any {
    return super.transform(value, format);
  }
}
