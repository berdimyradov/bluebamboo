/**
    * Created by aleksay on 13.04.2017.
    * Project: tmp_booking
    */
import {Pipe, PipeTransform} from "@angular/core";

@Pipe({name: 'indexArray'})
export class IndexArrayPipe implements PipeTransform {
    transform(length: number) : any {
        let res = [];
        for (let i = 0; i < length; i++) {
            res.push(i);
        }
        return res;
    }
}