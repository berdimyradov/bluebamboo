import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isNull'
})
export class IsNullPipe implements PipeTransform {

  transform(value: any, nullValue: any): any {
    if (value === undefined || value === null || value === '') {
      return nullValue;
    }
    return value;
  }

}
