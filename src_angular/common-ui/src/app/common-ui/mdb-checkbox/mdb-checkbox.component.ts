/**
    * Created by aleksay on 26.04.2017.
    * Project: tmp_booking
    */
import {
  AfterViewInit, Component, ElementRef, ViewChild, forwardRef, ViewEncapsulation, OnChanges,
  Input, Output, EventEmitter
} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {CommonService} from '../common.service';
declare let $: any;

@Component({
  selector: 'mdb-checkbox',
  templateUrl: './mdb-checkbox.component.html',
  styleUrls: ['./mdb-checkbox.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MDBCheckboxComponent),
      multi: true
    }
  ],
  encapsulation: ViewEncapsulation.None
})
export class MDBCheckboxComponent implements AfterViewInit, OnChanges, ControlValueAccessor {
  @Input() public icon = '';
  @Input() public title = '';
  @Input() public readonly: boolean;
  @Input() public indeterminate = false;
  @Output() public onCheckBoxClick = new EventEmitter<Event>();
  @ViewChild('inputElem') private inputElement: ElementRef;

  public id: string;

  private _value = false;
  private onChange: any = () => {
  };
  private onTouched: any = () => {
  };

  public get value(): boolean {
    return this._value;
  }

  public set value(val: boolean) {
    this._value = val;

    this.onChange(val);
    this.onTouched();
  }

  constructor(private el: ElementRef, private commonService: CommonService) {
    if (el.nativeElement.attributes.id) {
      this.id = el.nativeElement.attributes.id.value
    } else {
      this.id = 'mdb-checkbox-' + this.commonService.uniqId;
    }
  }

  private updateControl() {
    setTimeout(() => {
      $(this.inputElement.nativeElement).change().blur();
    });
  }

  public click($event: Event): void {
    this.onCheckBoxClick.emit($event);
  }

  ngAfterViewInit() {
    this.updateControl();
  }

  ngOnChanges() {
    this.updateControl();
  }

  writeValue(obj: boolean): void {
    this._value = obj || false;

    this.updateControl();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }
}
