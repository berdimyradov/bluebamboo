import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'common-card-page',
  templateUrl: './common-card-page.component.html',
  styleUrls: ['./common-card-page.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CommonCardPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
