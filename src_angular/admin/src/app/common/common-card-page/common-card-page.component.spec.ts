import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonCardPageComponent } from './common-card-page.component';

describe('CommonCardPageComponent', () => {
  let component: CommonCardPageComponent;
  let fixture: ComponentFixture<CommonCardPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonCardPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonCardPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
