import { Component, OnInit, OnChanges, AfterViewInit } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Input, Output } from '@angular/core';
import { SimpleChanges } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Renderer } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { trigger, transition, style, animate, state } from '@angular/animations';
import { opened, fade, prepare } from './animation-states/index';
import { HostBinding } from '@angular/core'
import {
  openedToStartTransition,
  openedTransition,
  fadeTransition,
  fadeOutTransition
} from './animation-transitions/index';

@Component({
  selector: 'app-ui-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('animationSequence', [
      opened,
      fade,
      prepare,
      openedToStartTransition,
      openedTransition,
      fadeTransition,
      fadeOutTransition
    ])
  ]
})

export class SnackbarComponent implements OnInit, OnChanges, AfterViewInit {
  @Input('autoHide') autoHide;
  @Input('heading') heading: string;
  @Input('display') display = false;
  @Input('style') style: {};
  @Input('classNames') classNames: {};
  @Input('eventData') eventData: any;
  @Input('temper') temper = 'info';

  @HostBinding('class.notDisplayed') private isNotDisplayed: boolean;
  @HostBinding('class.blockDisplayed') private isBlockDisplayed: boolean;

  @Output('animationStatusChange')
  animationStatusChange: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('animationElement') animationElement;
  @ViewChild('container') container;
  @ViewChild('header') header;

  public animationBinding = 'closed';

  constructor(private renderer: Renderer) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    if (this.style) {
      this.__setStyleProps();
    }
  }

  ngOnChanges(model: SimpleChanges) {
    // /**
    //  *  First launch with undefined display
    //  */

    const displayed = model.display || false;

    if (displayed && !model.display.previousValue) {
      this.isNotDisplayed = true;
      this.setAnimationState('prepare');
    }
    /**
     * @Input('Display') changed to true
     */
    if (displayed &&
        model.display.currentValue &&
        model.display.previousValue !== undefined) {
      this.displaySnackBar();
    }
    /**
     * @Input('Display') changed to false
     */
    if (displayed && model.display.currentValue === false
      && model.display.previousValue !== undefined) {
      this.hideSnackBar();
    }
  }

  /**
   *
   * @param started
   */
  public onAnimationProgressChange(started: boolean) {
    if (!started) {
       this.animationBinding === 'fade' ? this.animationStatusChange.emit(this.eventData) : null;
    }
  }


  private setAnimationState(state: string): void {
    this.animationBinding = state;
  }

  public closeElement() {
    this.animationBinding = 'fade';
  }

  public setAnimationStateAndCloseElement(state: string, params: any) {
    if (params.delay && params.delay > 0) {
      setTimeout(() => {
        this.animationBinding = 'fade';
        setTimeout(this.isNotDisplayed = true, 2000);
      }, params.delay);
    } else {
      this.animationBinding = state;
    }
  }

  private __setStyleProps(): void {
    if (this.style) {
      const elementsToStyle = Object.keys(this.style);

      elementsToStyle.forEach((tplRef) => {
        const elemContainer = this[tplRef];
        const cssProps = this.style[tplRef];

        if (elemContainer) {
          this.__setCssPropsToElement(elemContainer.nativeElement, cssProps);
        }
      });
    }
  }

  private __setCssPropsToElement(el, cssProps): void {
    const propNames = Object.keys(cssProps);
    propNames.forEach((prop) => {
      const propValues = cssProps[prop];
      const keys = Object.keys(propValues);
      const cssKey = keys[0];
      const cssVal = propValues[keys[0]];
      this.renderer.setElementStyle(el, cssKey, cssVal);
    });
  }

  public getTemper(temper: string) {
    return this.temper === temper;
  }

  public displaySnackBar() {
    this.isBlockDisplayed = true;
    this.setAnimationState('prepare');
    this.setAnimationState('opened');
    this.setAnimationStateAndCloseElement('fade', this.autoHide);
  }

  public hideSnackBar() {
    this.animationBinding = 'fade';
  }

  public toggle() {
    this.animationBinding = this.animationBinding === 'closed' ? 'opened' : 'fade';
  }

}
