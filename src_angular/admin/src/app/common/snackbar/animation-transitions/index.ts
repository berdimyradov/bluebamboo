import {transition, style, animate } from '@angular/animations';
export const openedToStartTransition = transition('opened => onStartPosition', animate('400ms ease-in-out'));
export const openedTransition = transition('* => opened', animate('400ms ease-in-out'));
export const fadeTransition = transition('* => fade', animate('300ms ease-in-out'));
export const fadeOutTransition = transition('* => fadeOut', [
                                style({ opacity: 0 }),
                                animate(1000, style({ opacity: 0 }))
                              ]);

