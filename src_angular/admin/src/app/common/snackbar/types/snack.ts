export interface AutoHideInterface {
  hide: boolean;
  delay: number;
};

export interface SnackAnimationEvent {
  type: any;
};

export class SnackState {
  public display: boolean;
  private message: string;
  private header: string;
  private evendData: SnackAnimationEvent;
  private autoHide: AutoHideInterface;
  private temper = 'info';

  constructor(autohide: AutoHideInterface, display: boolean, message: string, header: string) {
    this.setAutohide(autohide);
    this.setMessage(message);
    this.setHeader(header);
    this.display = display;
  }

  public getEvent(): SnackAnimationEvent {
    return this.evendData;
  }

  public setEvent(event: SnackAnimationEvent) {
    this.evendData = event
  }

  public setMessage(message: string): void {
    this.message = message;
  }

  public getMessage(): string {
    return this.message;
  }

  public getHeader(): string {
    return this.header;
  }

  public setHeader(header: string): void {
    this.header = header;
  }

  public getAutohide(): AutoHideInterface {
    return this.autoHide;
  }

  private setAutohide(autoHide: AutoHideInterface) {
    this.autoHide = autoHide;
  }

  public setTemper(temper: string) {
    this.temper = temper;
  }

  public getTemper(): string {
    return this.temper;
  }
}
