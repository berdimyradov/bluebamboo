import { style, state } from '@angular/animations';

export const prepare = state('prepare', style({
  transform: 'translate3d(0, -100%, 0)'
}));

export const opened = state('opened', style({
  transform: 'translate3d(0, 0, 0)'
}));

export const fade = state('fade', style({
  opacity: 0
}));
