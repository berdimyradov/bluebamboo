import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation,
  ContentChildren,
  ViewChild,
  QueryList,
  AfterViewInit
} from '@angular/core';
import { NgModel, NgForm } from '@angular/forms';

@Component({
  selector: 'common-form',
  templateUrl: './common-form.component.html',
  styleUrls: ['./common-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CommonFormComponent implements OnInit, AfterViewInit {
  @ContentChildren(NgModel) public models: QueryList<NgModel>;
  @ViewChild(NgForm) public form: NgForm;
  @Input('can-delete') canDelete = true;
  @Input('formname') formname: string;
  /*
   * Decorators for handling save/delete/back from parent component
   */
  @Output() public back: EventEmitter<void> = new EventEmitter<void>();
  @Output() public delete: EventEmitter<void> = new EventEmitter<void>();
  @Output() public save: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  public ngAfterViewInit(): void {
    const ngContentModels = this.models.toArray();
    ngContentModels.forEach((model) => {
      this.form.addControl(model);
    });
  }

  public executeBack() {
    this.back.emit();
  }

  public executeDelete() {
    this.delete.emit();
  }

  public executeSave() {
    this.save.emit();
  }

}
