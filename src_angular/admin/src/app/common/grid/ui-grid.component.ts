import { PagerService } from './pager/pager-service';
import {
  AfterViewInit,
  Component,
  ContentChild,
  Input,
  OnInit,
  TemplateRef } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import {SortablePipe} from "./pipes/sortable-pipe";

@Component({
  selector: 'app-ui-grid',
  templateUrl: './ui-grid.component.html',
  styleUrls: ['./ui-grid.component.css']
})
export class UiGridComponent implements OnInit, AfterViewInit {

  public SortBy = '';
  public Direction = 1;

  public allData = [];
  public spliceOfData = [];
  public pager = {
    startIndex: null,
    endIndex: null
  };
  public filterWord = '';

  private itemsLength: number;
  private selectedPage =1;

  @Input() withControlls = true;
  @Input() headersOfTable = [];
  @Input() withSorting = true;
  @Input() withSearch: boolean;
  @Input() elementsToDisplay = 10;

  @ContentChild('controlls')
  controllsTemplate: TemplateRef<any>;

  public RowDeleted$ = new Subject<any>();
  public PageIndexChanged$ = new Subject<any>();

  constructor(private pagerService: PagerService, private sortablePipe: SortablePipe) {

  }

  ngOnInit() {
  }

  ngAfterViewInit() {}

  public LoadData(_data: any) {
    this.allData = _data;
    this.setPage(1);
  }

  public setPage(page: number) {
    const dataToPager = this.filterOriginalData();
    const len = dataToPager.length;
    this.pager = this.pagerService.getPager(len, page, this.elementsToDisplay);
    this.selectedPage = page;
    this.spliceOfData = dataToPager.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  public saveLength(len: number) {
    this.itemsLength = len;
  }

  public reloadChunk() {
    this.setPage(1);
    this.Sort(this.SortBy + '', this.Direction * 1);
  }

  private elContainsString(row: any, searchString :string): boolean {
    let result = false;
    const values = (<any>Object).values(row);

    if(values) {
      values.forEach(item => {
        if (item && item.toString()
            .toLowerCase()
            .indexOf(searchString) > -1) {
          result = true;
        }
      });
    }

    return result;
  }

  public filterOriginalData() {
    const searchWord = this.filterWord.toLowerCase();

    if(!this.filterWord) { return this.allData; }
    const filteredArray = this.allData.filter((el) => this.elContainsString(el, searchWord));

    if (!filteredArray || filteredArray.length < 1) {
      return this.allData;
    } else {
      return filteredArray;
    }
  }

  public Sort(key: string, dir: number) {
    this.SortBy = key;
    this.Direction = dir;
    this.allData = this.sortablePipe
      .transform(this.allData,
        [
          this.SortBy,
          this.Direction
        ]);
    this.setPage(this.selectedPage);
  }
}

