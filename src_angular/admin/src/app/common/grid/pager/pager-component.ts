import {
  Component,
  OnInit,
  Output,
  Input,
  EventEmitter,
  ViewEncapsulation
} from '@angular/core';

@Component({
  selector: 'app-ui-pager',
  templateUrl: './pager-component.html',
  encapsulation: ViewEncapsulation.None
})

export class PagerComponent implements OnInit {
  @Output()
  onChangedPage: EventEmitter<number> = new EventEmitter<number>();

  @Input() pager;
  @Input() showFirstLink = true;
  @Input() showLastLink = true;
  constructor() { }

  ngOnInit() { }

  public setPage(page: number): void {
    this.onChangedPage.emit(page);
  }
}
