export interface UiGridHeaders {
  colName: string;
  displayName: string;
  sortable: boolean;
}
