import { Pipe, PipeTransform, forwardRef, Inject } from '@angular/core';
import { UiGridComponent } from '../ui-grid.component';

@Pipe({
  name: 'dump'
})
export class DumpPipe implements PipeTransform {
  private app;
  constructor( @Inject(forwardRef(() => UiGridComponent)) app: UiGridComponent) {
    this.app = app;
  }
  transform(array: Array<string>, args: string): Array<string> {
    this.app.saveLength(array.length);
    return array;
  }
}
