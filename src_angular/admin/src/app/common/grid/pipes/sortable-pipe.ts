import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortable',
  pure: false
})
export class SortablePipe implements PipeTransform {

  private rxp  = new RegExp(/(0[1-9]|[12][0-9]|3[01])\.(0[1-9]|1[012])\.(19|20)\d\d/);

  private compareAsStrings(str1: string, str2: string) {
    let res = 0;
    if (str1.toLowerCase() > str2.toLowerCase()) {
      res = 1;
    }
    if (str2.toLowerCase() > str1.toLowerCase()) {
      res = -1;
    }
    return res;
  }

  private compareAsNumbers(number1: number, number2: number) {
    let res = 0;

    if (number1 > number2) {
      res = 1;
    }
    if (number2 > number1) {
      res = -1;
    }

    return res;
  }

  private compareAsDates(date1, date2) {
    const time1 = new Date(date1.split('.').reverse().join('-'));
    const time2 = new Date(date2.split('.').reverse().join('-'));

    let res = 0;

    if (time1 > time2) {
      res = 1;
    }

    if(time2 > time1) {
      res = -1;
    }
    return res;
  }

  private getValueType(val: any) :string {
    if (typeof val === 'number' || isFinite(val)) {
      return 'number';
    }

    if(typeof val === 'string' && !this.rxp.test(val)) {
      return 'string';
    }

    if (typeof val === 'string' && this.rxp.test(val)) {
      return 'string-date';
    }
  }

  private compareValues(a: any, b: any){

    const valueType = this.getValueType(a);
    let res;

    switch (valueType){
      case 'number':
        res = this.compareAsNumbers(a,b);
        break;
      case 'string':
        res = this.compareAsStrings(a,b);
        break;
      case 'string-date':
        res = this.compareAsDates(a,b);
        break;
      default:
        res = this.compareAsNumbers(a, b);
    }

    return res;
  }

  transform(array: any[], [SortBy, Dir]) {
    array.sort((a: any, b: any) => {
      let compareResult = this.compareValues(a[SortBy], b[SortBy]);

      if (compareResult === 1 ) {
        return 1 * Dir;
      }
      if (compareResult === -1 ) {
        return -1 * Dir;
      }
      return 0;
    });

    return array;
  }
}
