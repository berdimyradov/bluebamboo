import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'filterable',
  pure: true
})
export class FilterablePipe implements PipeTransform {
  transform(array: any[], [Filter]) {
    if (Filter.toString().length < 1) {
      return array;
    }
    const filteredAray = array.filter((el) => {
      let result = false;
      const values = (<any>Object).values(el);
      values.forEach(element => {
        if (element.toString().indexOf(Filter) > -1) {
          result = true;
        }
      });
      return result;
    });
    return filteredAray;
  }
}
