import { SnackbarComponent } from './snackbar/snackbar.component';
import { StarsCComponent } from './stars/stars.component';
import { TimeSlotsComponent } from './time-slots/time-slots.component';
import { CalendarComponent } from './calendar/calendar.component';
import { PagerService } from './grid/pager/pager-service';
import { UiGridComponent } from './grid/ui-grid.component';
import { PagerComponent } from './grid/pager/pager-component';
import { SortablePipe } from './grid/pipes/sortable-pipe';
import { FilterablePipe } from './grid/pipes/filterable-pipe';
import { DumpPipe } from './grid/pipes/dump.pipe';
import { FakeWindowComponent } from './fake-window/fake-window.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { CommonPageComponent } from './common-page/common-page.component';
import { CommonListComponent } from './common-list/common-list.component';
import { CommonListItemComponent } from './common-list/common-list-item/common-list-item.component';
import { CommonFormComponent } from './common-form/common-form.component';
import {DataStorageService} from "./services/data-storage/data-storage.service";
import { DialogService } from './dialog/dialog.service';
import { ConfirmDialogComponent } from './dialog/confirm-dialog/confirm-dialog.component';
import { CommonUiModule } from '@bluebamboo/common-ui';
import { CommonSelectedComponent } from './common-selected/common-selected.component';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { CommonMenuPageComponent } from './common-menu-page/common-menu-page.component';
import { CommonMenuItemComponent } from './common-menu-page/common-menu-item/common-menu-item.component';
import { CommonCardPageComponent } from './common-card-page/common-card-page.component';
import { locale as en } from './resources/en';
import { locale as de } from './resources/de';
import { EmployeeService } from './services/employee.service';
import { EmployeesResolver } from './resolvers/employees-resolver';
import { EmployeeResolver } from './resolvers/employee-resolver';
import { ActiveEmployeesResolver } from './resolvers/active-employees-resolver';
import {UtilityService} from "./services/utility/utility.service";

import { UploadService, SelectOption } from '@bluebamboo/common-ui';

/*Preloade/Spinner Component*/
import { PreloaderComponent } from './preloader/preloader-component';
import { PreLoaderService } from './preloader/preloader.service';

/* Pipes */
import { AddressPipe } from './pipes/address.pipe';
import { ValuePrefixPipe } from './pipes/value-prefix.pipe';
import { SafeUrlPipe } from './pipes';
import { InvoiceFormatTypePipe } from './pipes';

/* Faked window*/
import { FakeWindowService } from 'app/common/fake-window/service/fake-window.service';

import { DateService } from './services/date.service';
/*Stars rating */
import { StarsComponent } from './stars/stars-component';
import { TimeMask } from 'app/common/time/time.directive';



import { CommonCustomerCardComponent } from './common-customer-card/common-customer-card.component';
import {ModalWindowComponent} from "./modal-window/modal-window.component";
// import { WithOrderPipe } from "./grid/pipes/with-order.pipe";


@NgModule({
  imports: [
    BrowserModule, RouterModule, FormsModule, TranslateModule, BrowserAnimationsModule, CommonUiModule
  ],
  declarations: [
    CommonPageComponent,
    TimeMask,
    TimeSlotsComponent,
    CommonListComponent,
    CommonListItemComponent,
    CommonFormComponent,
    ConfirmDialogComponent,
    CommonSelectedComponent,
    SnackbarComponent,
    CommonMenuPageComponent,
    CommonMenuItemComponent,
    CommonCardPageComponent,
    CalendarComponent,
    ModalWindowComponent,
    PreloaderComponent,
    StarsComponent,
    StarsCComponent,
    AddressPipe,
    ValuePrefixPipe,
    SafeUrlPipe,
    CommonCustomerCardComponent,
    DumpPipe,
    // WithOrderPipe,
    FilterablePipe,
    SortablePipe,
    PagerComponent,
    UiGridComponent,
    FakeWindowComponent,
    InvoiceFormatTypePipe,
  ],
  exports: [
    CommonPageComponent,
    TimeSlotsComponent,
    CommonListComponent,
    CommonFormComponent,
    CommonSelectedComponent,
    SnackbarComponent,
    CommonMenuPageComponent,
    CommonCardPageComponent,
    CommonCustomerCardComponent,
    CalendarComponent,
    ModalWindowComponent,
    ReactiveFormsModule,
    PreloaderComponent,
    StarsComponent,
    StarsCComponent,
    AddressPipe,
    ValuePrefixPipe,
    DumpPipe,
    // WithOrderPipe,
    FilterablePipe,
    SortablePipe,
    SafeUrlPipe,
    PagerComponent,
    UiGridComponent,
    InvoiceFormatTypePipe,
    FakeWindowComponent
  ],
  providers: [
    DataStorageService,
    DialogService,
    EmployeeService,
    EmployeesResolver,
    ActiveEmployeesResolver,
    EmployeeResolver,
    PreLoaderService,
    UploadService,
    DateService,
    FakeWindowService,
    PagerService,
    AddressPipe,
    ValuePrefixPipe,
    DumpPipe,
    // WithOrderPipe,
    FilterablePipe,
    SortablePipe,
    ValuePrefixPipe,
    UtilityService
  ]
})
export class AdminCommonModule {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
  }
}
