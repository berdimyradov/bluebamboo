/**
    * Created by aleksay on 03.05.2017.
    * Project: tmp_booking
    */
import { Injectable }               from '@angular/core';

@Injectable()
export class DateService {
    public Weekdays: string[] = ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So'];
    public Months: string[] = ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'];

    public getFirstDay(date: Date =  new Date()): Date {
        let month = date.getMonth(),
            year = date.getFullYear();
        return new Date(year, month, 1, 0, 0);
    }

    public addMonth(date: Date = new Date(), step: number): Date {
        let month = date.getMonth(),
            year = date.getFullYear();
        return new Date(year, month + step, 1, 0, 0);
    }
}