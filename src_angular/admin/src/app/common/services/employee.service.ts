import { Injectable } from '@angular/core';
import {Http, RequestOptions, Response, URLSearchParams} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Employee} from '../types'

@Injectable()
export class EmployeeService {

  constructor(private http: Http) { }

  private getLocations(fields: string[]): Observable<Employee[]> {
    let requestOptions: RequestOptions = new RequestOptions();
    requestOptions.params = new URLSearchParams();
    requestOptions.params.set('_fields', fields.join(','));
    return this.http.get('/api/v1/org/employees', requestOptions)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  private getActiveEmployees(fields: string[]): Observable<Employee[]> {
    let requestOptions: RequestOptions = new RequestOptions();
    requestOptions.params = new URLSearchParams();
    requestOptions.params.set('_fields', fields.join(','));
    return this.http.get('/api/v1/org/employees/active', requestOptions)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public getList(): Observable<Employee[]> {
    return this.getLocations(['id', 'name', 'firstname']);
  }

  public getListActive(fields: string[] = []): Observable<Employee[]> {
    return this.getActiveEmployees(fields);
  }

  public get(id: number): Observable<any> {
    return this.http.get('/api/v1/org/employees/'+id)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public save(employee:any): Observable<void> {
    if (employee.id) {
      return this.http.put('/api/v1/org/employees/'+ employee.id, employee)
        .map((res: Response) => res.json())
        .catch(this.handleError);
    }
    return this.http.post('/api/v1/org/employees/', employee)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public delete(employee: any): Observable<boolean> {
    return this.http.delete('/api/v1/org/employees/' + employee.id)
      .map(() => {return true;})
      .catch(() => Observable.of(false));
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
