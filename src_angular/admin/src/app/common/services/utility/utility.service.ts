import { Injectable } from '@angular/core';
import {SelectOption} from '@bluebamboo/common-ui';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UtilityService {

  constructor() { }

  public static handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  // If you insert option to mdb-select component by using this method, re-rendering happens automaticly
  // see: https://stackoverflow.com/questions/43223582/why-angular-2-ngonchanges-not-responding-to-input-array-push
  public static pushSelectOption(array: SelectOption[], option: SelectOption): SelectOption[] {
    array.push(option);
    return array.slice();
  }

}
