import { Injectable } from '@angular/core';

@Injectable()
export class DataStorageService {
  storageArray: any[] = [];

  constructor() { }

  setItem(key, data) {
    this.storageArray[key] = data;
  }

  getItem(key) {
    return this.storageArray[key];
  }

}
