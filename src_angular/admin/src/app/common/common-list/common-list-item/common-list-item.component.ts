import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {CommonListItem} from '../types/';

@Component({
  selector: 'common-list-item',
  templateUrl: './common-list-item.component.html',
  styleUrls: ['./common-list-item.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CommonListItemComponent implements OnInit {
  @Input() public item: CommonListItem;
  @Output() public action: EventEmitter<CommonListItem> = new EventEmitter<CommonListItem>();

  constructor() { }

  ngOnInit() {
  }

  public click() {
    this.action.emit(this.item);
  }
}
