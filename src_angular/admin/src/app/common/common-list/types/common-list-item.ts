export class CommonListItem {
  public id: number;
  public title: string;
  public description: string;
}
