import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {CommonListItem} from "./types/common-list-item";
import {ActivatedRoute, Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {animate, style, trigger, transition, group} from "@angular/animations";

@Component({
  selector: 'common-list',
  templateUrl: './common-list.component.html',
  styleUrls: ['./common-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('itemAnim', [
      transition(':enter', [
        style({transform: 'translateX(-100%)'}),
        animate(350)
      ]),
      transition(':leave', [
        group([
          animate('0.2s ease', style({
            transform: 'translate(150px,25px)'
          })),
          animate('0.5s 0.2s ease', style({
            opacity: 0
          }))
        ])
      ])
    ])
  ]
})
export class CommonListComponent implements OnInit {
  public list: CommonListItem[] = [];
  @Output() public edit: EventEmitter<CommonListItem> = new EventEmitter<CommonListItem>();
  @Output() public add: EventEmitter<CommonListItem> = new EventEmitter<CommonListItem>();

  @Input() public withoutCreateButton = false;
  @Input('get-list') public getList: any = () => Observable.of([]);

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    if (this.getList) {
      this.getList().subscribe(
        result =>  this.list = result
      )
    }
  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public executeAction(item: CommonListItem) {
    this.edit.emit(item);
  }

  public executeAdd() {
    this.add.emit();
  }
}
