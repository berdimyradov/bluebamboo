import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity } from "../animations/";
import { Router } from "@angular/router";
import { LocalizeRouterService } from "localize-router";

import { UserService } from "../../user.service";
import { HelpService } from 'app/common/help/help.service';

@Component({
  selector: 'common-page',
  templateUrl: './common-page.component.html',
  styleUrls: ['./common-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})

export class CommonPageComponent implements OnInit {
  private _title: string = '';
  private _leftSide: boolean;
  private appInfo: string = '';

  @ViewChild('logoElement') private logoElement: ElementRef;
  @ViewChild('titleElement') private titleElement: ElementRef;

  @Output('click-logout') private clickLogout: EventEmitter<void> = new EventEmitter<void>();

  public get title(): string {
    return this._title;
  }
  public set title(value: string) {
    this.setTitle(value);
  }

  constructor(
    private router: Router,
    private localize: LocalizeRouterService,
    private userService: UserService,
    protected help: HelpService,
  ) {
    this._leftSide = false;
    userService.getAppInfo().subscribe(
      info => this.appInfo = info
    );
  }

  ngOnInit() {
  }


  public toggle() {
    this.help.toggle();
  }


  public home() {
    this.router.navigate([ this.localize.translateRoute('/admin') ]);
  }
  public logout() {
    this.clickLogout.emit();
  }

  private setTitle(title: string){
    title = title || ' ';
    let
      leftSide = title == "" || title == ' ' ? false : true,
      show = this._title == '' && title != '' ? true : false,
      swap =  title != '' && !show ? true : false;
    if (this._leftSide != leftSide) {
      this._leftSide = leftSide;
      if (leftSide) {
        this.logoElement.nativeElement.classList.add('logo-left');
        this.logoElement.nativeElement.classList.remove('logo-center');
      } else {
        this.logoElement.nativeElement.classList.add('logo-center');
        this.logoElement.nativeElement.classList.remove('logo-left');
      }
    }
    if (show){
      this._title = title;
      this.titleElement.nativeElement.classList.add('title-show');
      this.titleElement.nativeElement.classList.remove('title-hide');
      this.titleElement.nativeElement.classList.remove('title-swap');
      return;
    }
    if (!show && !swap) {
      this.titleElement.nativeElement.classList.add('title-hide');
      this.titleElement.nativeElement.classList.remove('title-show');
      this.titleElement.nativeElement.classList.remove('title-swap');
      setTimeout(() => {this._title = '';}, 250);
      return;
    }
    if (swap) {
      this.titleElement.nativeElement.classList.add('title-show');
      this.titleElement.nativeElement.classList.remove('title-swap');
      setTimeout(() => this.titleElement.nativeElement.classList.add('title-swap'), 1);
      setTimeout(() => {this._title = title}, 250);
    }
  }
}
