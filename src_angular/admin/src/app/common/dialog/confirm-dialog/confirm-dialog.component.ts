import { Component, OnInit } from '@angular/core';
import { Modal } from '@bluebamboo/common-ui';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
@Modal()
export class ConfirmDialogComponent implements OnInit {
  private destroy: Function;
  private closeModal: Function;
  private onClose: Function;

  public text_yes: string;
  public text_no: string;
  public text: string;

  constructor() { }

  ngOnInit() {
  }

  public yes() {
    this.onClose(true);
    this.closeModal();
    this.destroy();

  }

  public no() {
    this.onClose(false);
    this.closeModal();
    this.destroy();
  }
}
