import { Injectable } from '@angular/core';
import { ModalService } from '@bluebamboo/common-ui';
import { AdminCommonModule } from '../common.module';
import { ConfirmDialogComponent } from "./confirm-dialog/confirm-dialog.component";

@Injectable()
export class DialogService {

  constructor(
    private modal : ModalService
  ) { }

  public confirm(text: string, yes: string = undefined, no: string = undefined): Promise<Boolean> {
    return new Promise<Boolean>( (resolve) => {
      this.modal.create(AdminCommonModule, ConfirmDialogComponent, {
        text: text,
        text_yes: yes || 'COMPONENT.DIALOG.CONFIRM.YES',
        text_no: no || 'COMPONENT.DIALOG.CONFIRM.NO',
        onClose: (res: boolean) => {
          resolve(res);
        }
      })
    });
  }
}
