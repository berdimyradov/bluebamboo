export class Location {
  public id: number;
  public title: string;
  public description: string;
  public client_id: number;
  public country: string;
  public zip: string;
  public state: string;
  public city: string;
  public street: string;
  public email: string;
  public phone: string;
  public employees: any[] = [];
  public rooms: any[] = [];

  public parse(data: any): Location {
    const names: string[] = ['id', 'title', 'description', 'client_id', 'country', 'zip', 'state', 'city', 'street', 'email', 'phone'];
    names.forEach((name) => this[name] = data[name] || this[name]);
    return this;
  }
}
