export interface Employee {
  id: number;
  name: string;
  firstname: string;
}
