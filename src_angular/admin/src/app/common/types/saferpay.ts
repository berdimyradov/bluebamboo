class SPRequestHeader {
  public SpecVersion = "1.8";
  public CustomerId = "242713";
  public RequestId = "abc";
  public RetryIndicator = 0;
}

class SPAmount {
  public Value = "103";
  public CurrencyCode = "CHF";
}

class SPPayment {
  public Amount = new SPAmount();
}

class SPPayer {
  public LanguageCode = "de";
}

class SPReturnUrls {
  public Success = "http://my.blue-bamboo.eu/de/home?p=success";
  public Fail = "http://my.blue-bamboo.eu/de/home?p=fail";
}

class SPStyling {
  public CssUrl = "";
}

export class SaferpayTrxInitRequest {
  public RequestHeader = new SPRequestHeader();
  public TerminalId = "17878280";
  public Payment = new SPPayment();
  public Payer = new SPPayer();
  public ReturnUrls = new SPReturnUrls();
  public Styling = new SPStyling();
}

export class SaferpayTrxInitResponse2 {
  public url: string;

  public parse(data: any): SaferpayTrxInitResponse2 {
    this.url = data.url;
    return this;
  }
}

export class SaferpayTrxInitResponse {
  public response_header_spec_version: string;
  public response_header_req_id: string;
  public token: string;
  public expiration: string;
  public liabilityShift: string;
  public redirectRequired: boolean;
  public redirect_url: string;
  public redirect_payment_means_required: boolean;

  public parse(data: any): SaferpayTrxInitResponse {
    this.response_header_spec_version = data.ResponseHeader.SpecVersion;
    this.response_header_req_id = data.ResponseHeader.RequestId;
    this.token = data.Token;
    this.expiration = data.Expiration;
    this.liabilityShift = data.LiabilityShift;
    this.redirectRequired = data.RedirectRequired;
    this.redirect_url = data.Redirect.RedirectUrl;
    this.redirect_payment_means_required = data.Redirect.PaymentMeansRequired;
    return this;
  }
}

