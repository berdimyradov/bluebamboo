export class Organization {
  public id: number;
  public client_id: number;
  public name: string;
  public logo: string;
  public phone: string;
  public email: string;

  public country: string;
  public state: string;
  public city: string;
  public zip: string;
  public street: string;
  public address_line: string;

  public bankaccount_bank: string;
  public bankaccount_bic: string;
  public bankaccount_iban: string;
  public bankaccount_owner: string;

  public cost_reminder_first: number;
  public cost_reminder_second: number;

  public mwst: boolean;
  public mwst_nr: string;

  public payment_term: number;
  public payment_term_reminder_first: number;
  public payment_term_reminder_second: number;

  public parse(data: any): Organization {
    Object.assign(this, data);
    /* const names: string[] = ['id', 'client_id', 'name', 'logo', 'phone', 'email', 'country', 'state', 'city', 'street', 'zip', 'address_line',
      'bankaccount_bank', 'bankaccount_bic', 'bankaccount_iban', 'bankaccount_owner', 'cost_reminder_first', 'cost_reminder_second',
      'mwst', 'mwst_nr', 'payment_term', 'payment_term_reminder_first', 'payment_term_reminder_second'];
    names.forEach((name) => {
      if(data[name] !== null && data[name] !== undefined) {
        this[name] = data[name]
      }
    }); */
    return this;
  }
}
