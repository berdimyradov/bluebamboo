export class InvoicePosition {
  public id: number;
  public price: number;
  public quantity: number;
  public tariff: number;
  public tariffposition_nr: string;
  public tariffposition_title: string;
  public title: string;
  public type: string;
  public bookingpart_id: number;
  public bookingpart: any;
  public client_id: number;
  public date: string;
  public duration: string;
  public invoice_id: number;
  public mwst: any;
  public get total_price(): number { return this.price; }
  public get price_per_part(): number { return this.price / this.quantity; }

  public parse(data: any) : InvoicePosition {
    const names: string[] = ['id','price','quantity','tariff','tariffposition_nr','tariffposition_title','title',
      'type','bookingpart_id','bookingpart','client_id','date','duration','invoice_id','mwst'];
    names.forEach((name) => this[name] = data[name] || this[name]);
    return this;
  }
}
