export class Customer {
  public id: number;
  public type: string;
  public firstname: string;
  public name: string;
  public fullName: string;
  public address: string;
  public address_you: boolean;
  public foto: string;
  public address_line: string;
  public street: string;
  public zip: string;
  public city: string;
  public state: string;
  public country: string;
  public phone_home: string;
  public phone_mobile: string;
  public phone_work: string;
  public email: string;
  public email_second: string;
  public email_primary: string;
  public birth_day: string;
  public birth_month: string;
  public birth_year: string;
  public insurance_id: number;
  public known_from: string;
  public comment: string;
  public client_id: number;
  public nr: string;
  public gender: string;
  public insurance_nr: string;
  public guardian_id: number;
  public caregiver_id: number;
  public dossier_id: number;
  public guardian: Customer;
  public caregiver: Customer;

  public parse(data: any): Customer {
    const names: string[] = [
      'id',
      'type',
      'firstname',
      'name',
      'address',
      'address_you',
      'foto',
      'address_line',
      'street',
      'zip',
      'city',
      'state',
      'country',
      'phone_home',
      'phone_mobile',
      'phone_work',
      'email',
      'email_second',
      'email_primary',
      'birth_day',
      'birth_month',
      'birth_year',
      'insurance_id',
      'known_from',
      'comment',
      'client_id',
      'nr',
      'gender',
      'insurance_nr',
      'guardian_id',
      'caregiver_id',
      'dossier_id'
    ];

    names.forEach((name: string) => this[name] = data[name]);

    if (data.guardian) {
      this.guardian = new Customer().parse(data.guardian);
    }

    if (data.caregiver) {
      this.caregiver = new Customer().parse(data.caregiver);
    }

    this.fullName = this.firstname + ' ' + this.name;

    return this;
  }
}
