export class InvoiceRequest {
  public id: number;
  public createdAt: string;
  public file: string;
  public client_id: number;

  public parse(data: any): InvoiceRequest {
    const names: string[] = ['id','createdAt','file','client_ud'];
    names.forEach((name) => this[name] = data[name] || this[name]);
    return this;
  }
}
