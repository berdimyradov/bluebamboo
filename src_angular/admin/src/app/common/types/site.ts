export class Site {
  public id: number;
  public name: string;
  public description: string;
  public domains: string;
  public default_page: string;

  public parse(data: any): Site {
    const names: string[] = ['id', 'name', 'description', 'domains', 'default_page'];
    names.forEach((name) => this[name] = data[name]);
    return this;
  }
}
