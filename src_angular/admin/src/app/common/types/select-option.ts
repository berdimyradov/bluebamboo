/**
    * Created by aleksay on 18.04.2017.
    * Project: tmp_booking
    */
export class SelectOption {
    public title: string;
    public code: string;

    constructor(data: any = undefined){
        this.title = (data ? data.title : "");
        this.code = (data ? data.code : "");
    }
}