import { Location } from './location';

export class Room {
  public id: number;
  public client_id: number;
  public title: string;
  public description: string;
  public area: number;
  public active_from: Date;
  public active_to: Date;
  public location: Location = new Location();
  public location_id: number;
  public bookings: any[] = [];
  public products: any[] = [];

  public parse(data: any): Room {
    const names: string[] = ['id', 'title', 'description', 'client_id', 'area', 'active_from', 'active_to', 'location_id'];
    names.forEach((name) => this[name] = data[name]);
    if (data.location) {
      this.location = new Location();
      this.location.parse(data.location);
    }
    this.products = data.products;
    return this;
  }
}
