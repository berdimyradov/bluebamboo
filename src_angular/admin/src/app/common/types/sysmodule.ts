class SysModuleBooking {
  public price: number;
  public state: string;
  public discount: string;
  public order_active: boolean;

  public parse(data: any): SysModuleBooking {
    const names: string[] = ['price', 'state', 'discount', 'order_active'];
    names.forEach((name) => this[name] = data[name]);
    return this;
  }

}

export class SysModule {
  public id: number;
  public validFrom: Date;
  public validTo: Date;
  public code: string;
  public label: string;
  public description: string;
  public img: string;
  public availability: string;
  public sortix: number;
  public platform_id: number;
  public booking: SysModuleBooking;
  public visible: boolean;

  public parse(data: any): SysModule {
    const names: string[] = ['id', 'validFrom', 'validTo', 'code', 'label', 'description', 'img', 'availability', 'sortix', 'platform_id', 'visible'];
    names.forEach((name) => this[name] = data[name]);
    this.booking = new SysModuleBooking();
    this.booking.parse(data.booking);
    return this;
  }
}
