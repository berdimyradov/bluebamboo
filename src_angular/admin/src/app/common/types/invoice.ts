import {Organization, Customer, InvoiceRequest} from './';
import {InvoicePosition} from "./invoice-position";

export class Invoice {
  public id: number;
  public number: string;
  public date: string;
  public date_reminder_1: string;
  public date_reminder_2: string;
  public file: string;
  public value: number;
  public customer_name: string;
  public comment: string;
  public status: string;
  public organization_id: number;
  public invoicerequest_id: number;
  public customer_id: number;
  public client_id: number;
  public file_reminder_1: string;
  public file_reminder_2: string;
  public file_copy_insurance: boolean;
  public file_copy_archive: boolean;

  public organization: Organization;
  public invoicerequest: InvoiceRequest;
  public customer: Customer;
  public bookings: any[];
  public invoicepositions: InvoicePosition[] = [];
  public payments: any[];
  public get positions_total_price(): number {

    return this.invoicepositions.length > 0 ? this.invoicepositions.reduce((prev: number, curent: InvoicePosition) => prev + curent.total_price, 0) : 0;
  }

  public parse(data: any) : Invoice {
    const names: string[] = ['id','number','date','date_reminder_1','date_reminder_2','file','value','customer_name',
      'comment','status','organization_id','invoicerequest_id','customer_id','client_id','file_reminder_1',
      'file_reminder_2','file_copy_insurance','file_copy_archive'];
    names.forEach((name) => this[name] = data[name] || this[name]);
    if (data.organization) {
      this.organization = (new Organization()).parse(data.organization);
    }
    if (data.invoicerequest) {
      this.invoicerequest = (new InvoiceRequest()).parse(data.invoicerequest);
    }
    if (data.customer) {
      this.customer = (new Customer()).parse(data.customer);
    }
    if (data.invoicepositions) {
      data.invoicepositions.forEach((it: any) => this.invoicepositions.push( (new InvoicePosition()).parse(it) ));
    }
    this.payments = data.payments;
    return this;
  }
}
