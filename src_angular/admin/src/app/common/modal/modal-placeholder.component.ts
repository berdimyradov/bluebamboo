/**
    * Created by aleksay on 14.04.2017.
    * Project: tmp_booking
    */
import {Component, Injector, OnInit, ViewChild, ViewContainerRef} from "@angular/core";
import {ModalService} from "./modal.service";

@Component({
    selector: "modal-placeholder",
    template: `<div #modalplaceholder></div>`
})
export class ModalPlaceholderComponent implements OnInit {
    @ViewChild("modalplaceholder", {read: ViewContainerRef}) viewContainerRef:any;

    constructor(private modalService: ModalService, private injector: Injector) {

    }
    ngOnInit(): void {
        this.modalService.registerViewContainerRef(this.viewContainerRef);
        this.modalService.registerInjector(this.injector);
    }
}
