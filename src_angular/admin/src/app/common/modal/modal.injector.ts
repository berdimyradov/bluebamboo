/**
    * Created by aleksay on 14.04.2017.
    * Project: tmp_booking
    */
import {ModalContainer} from "./modal-container";

export function Modal() {
    return function (target:any) {
        Object.assign(target.prototype,  ModalContainer.prototype);
    };
}