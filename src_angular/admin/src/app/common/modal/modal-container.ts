/**
    * Created by aleksay on 14.04.2017.
    * Project: tmp_booking
    */
export class ModalContainer {
    public destroy: Function;
    public componentIndex: number;

    public closeModal(): void {
        this.destroy();
    }
}
