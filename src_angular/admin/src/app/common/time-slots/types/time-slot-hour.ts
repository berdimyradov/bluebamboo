/**
    * Created by aleksay on 20.04.2017.
    * Project: tmp_booking
    */
export class TimeSlotHour{
    public minutes: number[] = [];

    constructor(public hour: number){}

    public fill(step: number, offset: number = 0): number{
        do{
            this.minutes.push(offset);
            offset += step;
        } while(offset < 60);
        offset -= 60;
        return offset;
    }
}