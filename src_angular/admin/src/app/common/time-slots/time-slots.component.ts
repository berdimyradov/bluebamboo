/**
    * Created by aleksay on 20.04.2017.
    * Project: tmp_booking
    */
import { Component, forwardRef, Input, ViewEncapsulation, OnInit, OnChanges } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TimeSlotHour } from './types/time-slot-hour';

@Component({
  selector: 'time-slots',
  templateUrl: './time-slots.html',
  styleUrls: ['./time-slots.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TimeSlotsComponent),
      multi: true
    }
  ]
})
export class TimeSlotsComponent implements ControlValueAccessor, OnInit, OnChanges {
  @Input('timeData') public timeData:any;
  @Input('cols') public cols: number;

  public hours: TimeSlotHour[] = [];
  public selectedHour: number = null;
  public selectedMinutes: number = null;

  public selectedTime;

  private onChange: any = () => { };
  private onTouched: any = () => { };

  ngOnInit() {
    this.init();
  }

  ngOnChanges() {
    this.init();
  }

  private getTimePair(time: string): any {
    if (!time) {
      return null;
    }
    let t = time.split(':');
    return {
      hour: parseInt(t[0]),
      minute: parseInt(t[1])
    };
  }

  private leadZero(value: number, length: number): string {
    let res: string = value.toString();
    for (let i = res.length; i < length; i++) {
      res = "0" + res;
    }
    return res;
  }

  private init() {
    
  }

  public select(slot) {
    if(slot.available) {
      this.selectedTime = slot.time;
      this.onChange(slot);
      this.onTouched();
    }
  }

  writeValue(obj: string): void {
    if (obj) {
      let t = obj.split(':');
      this.selectedHour = parseInt(t[0]);
      this.selectedMinutes = parseInt(t[1]);
    } else {
      this.selectedHour = null;
      this.selectedMinutes = null;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
