import { Component, Input, OnInit } from '@angular/core';
import { Customer } from '../types/customer';

@Component({
  selector: 'common-customer-card',
  templateUrl: './common-customer-card.component.html',
  styleUrls: ['./common-customer-card.component.scss']
})
export class CommonCustomerCardComponent {
  @Input() public customer: Customer;
}
