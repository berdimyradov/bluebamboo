import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonCustomerCardComponent } from './common-customer-card.component';

describe('CommonCustomerCardComponent', () => {
  let component: CommonCustomerCardComponent;
  let fixture: ComponentFixture<CommonCustomerCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonCustomerCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonCustomerCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
