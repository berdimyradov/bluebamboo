import {animate, state, style, transition, trigger} from "@angular/animations";

export const routeAnimationOpacity = trigger('routeAnimationOpacity', [
  state('*', style({
    opacity: 1
  })),
  transition('void => *', [
    style(
      {
        opacity: 0,
        display: 'none'
      }),
    animate(1000)
  ]),
  transition('* => void', style({opacity: 0, display: 'none'}))
]);
