export class HelpContext {

  public level: number;
//  public mwst: boolean;
//  public mwst_nr: string;


  public parse(data: any): HelpContext {
//    Object.assign(this, data);

    if (!data) return this;

    this.level = data.level;

    return this;
  }
}

export class HelpEntry {

  public area: string;
  public question: string;
  public answer: string;


  public parse(data: any): HelpEntry {
    if (!data) return this;

    Object.assign(this, data);

    return this;
  }
}
