import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Http, Response } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';

import { HelpContext, HelpEntry } from './help.types';
import { FrameworkService, GenericResultModel } from 'app/framework';

@Injectable()
export class HelpService extends FrameworkService {

  private url = new ReplaySubject(1);
  private activeSubject = new ReplaySubject(1);
  private active = true;
  private area = '';

  constructor (
    private localStorageService: LocalStorageService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    http: Http
  ) {
    super(http);

    router.events.filter(e => e instanceof NavigationEnd).subscribe(e => {
      this.area = (e as NavigationEnd).url;
      this.url.next(this.area);
    });

    this.activeSubject.next(this.active);

  }

  public getURL (): ReplaySubject<string> {
    return this.url;
  }

  public getHelpIsActive (): ReplaySubject<boolean> {
    return this.activeSubject;
  }

  public toggle () {
    this.active = !this.active;
    this.activeSubject.next(this.active);
  }

  // -------------------------------------------------------------------------------------
  // get help level


//      this.localStorageService.set("bb_personal_invite_subject", this.inviteMessage.subject);


  public getHelpContext (): Observable<HelpContext> {
    let info: {'level': 1};
    if (true) return Observable.of((new HelpContext()).parse(info));
/*
    return this.http.get('/api/v1/auth/state')
      .map((response: Response) => new LoginInfoModel(response.json()))
      .catch(() => (new Observable<LoginInfoModel>(observable => observable.next(null))))
*/
  }

  // -------------------------------------------------------------------------------------
  // get help entries

  public getHelpEntries(): Observable<HelpEntry[]> {
    return this.http.get('/api/v1/platform/helpentries/-' + this.area)
      .map((res: Response) => res.json().map(item => new HelpEntry().parse(item)))
      .catch(this.handleError);
  }

}
