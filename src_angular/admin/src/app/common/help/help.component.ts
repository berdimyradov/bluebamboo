import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonListItem } from "app/common";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { GenericComponent } from "app/framework";
import { UserService } from "app/user.service";

import { HelpEntry } from 'app/common/help/help.types';
import { HelpService } from 'app/common/help/help.service';

@Component({
	selector:		    'help',
	templateUrl:	  './help.component.html',
	styleUrls:		  ['./help.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			      {'[@routeAnimationOpacity]': 'true'},
	animations:		  [routeAnimationOpacity]
})

export class HelpComponent extends GenericComponent {

  public helpEntries: HelpEntry[];
  public context: string;
  public showHelp = true;

	constructor(
    	protected help: HelpService,
    	protected router: Router
    )
	{
	  super(help);
	}

	ngOnInit() {
	  super.ngOnInit();

    this.help.getHelpIsActive().subscribe(
      res => this.showHelp = res
    );

    this.help.getURL().subscribe(
      res => {
        this.context = res;
    	  this.help.getHelpEntries().subscribe(
	        (res) => {
	          this.helpEntries = res;
	        }
	      );
      }
    );


	}

  public toggle() {
    this.showHelp = !this.showHelp;
  }

  public save() {
  }

}
