/**
    * Created by aleksay on 13.04.2017.
    * Project: tmp_booking
    */
import {Component, Input} from '@angular/core';

@Component({
    selector: 'stars',
    templateUrl: './stars.html'
})
export class StarsCComponent {
    @Input()
    count: number = 0;
}
