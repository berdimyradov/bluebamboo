import { Component, OnInit, EventEmitter, Input, Output, } from '@angular/core';
import { NgFor, NgClass } from '@angular/common';

@Component({
  selector:     'app-ui-stars',
  templateUrl:  './stars-component.html',
  styleUrls:    ['./stars-component.scss']
})

export class StarsComponent implements OnInit {
//  public range: Array<number> = [1, 2, 3, 4, 5,6,7,8,9,10];
  public range: Array<number> = [1, 2, 3, 4, 5];

  @Input('rate') rate: number;
  @Input('rated') rated: string;
  @Input('unrated') unrated: string;

  @Output('updatedRate') updatedRate: EventEmitter<any> = new EventEmitter()

  constructor() { }

  ngOnInit() { }

  public ratingClass (ix) {
    if (ix >= this.rate) {
      return "ratingElement unrated fa " + this.unrated;
    }
    else {
      return "ratingElement rated fa " + this.rated;
    }
  }

  update(value) {
    this.rate = value;
    this.updatedRate.next(value);
  }
}
