import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {EmployeeService} from "../services/employee.service";

@Injectable()
export class EmployeeResolver implements Resolve<any> {
  constructor (private employee : EmployeeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any | Observable<any> | Promise<any> {
    const id: string = route.paramMap.get('id');

    if (id === 'add') {
      return Observable.of({
        email_primary: 1,
        locations: [],
        products: []
      });
    }

    return this.employee.get(parseInt(id));
  }
}
