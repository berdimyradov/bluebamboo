import {Injectable} from "@angular/core";
import {Employee} from '../types';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {EmployeeService} from "../services/employee.service";

@Injectable()
export class EmployeesResolver implements Resolve<Employee[]> {
  constructor (private employee : EmployeeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Employee[] | Observable<Employee[]> | Promise<Employee[]> {
    return this.employee.getList();
  }

}
