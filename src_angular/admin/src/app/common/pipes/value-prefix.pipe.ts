import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'valuePrefix'
})
export class ValuePrefixPipe implements PipeTransform {

  transform(value: any, unit: string): any {
    if (value) {
      return `${value} ${unit}`;
    }

    return '';
  }
}
