import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'address'
})
export class AddressPipe implements PipeTransform {
  transform(model = { address_line: '', street: '', zip: '', city: '', state: '', country: '' }): string {
    const { address_line, street, zip, city, state, country } = model;
    let address = '';

    if (!address_line && !street && !zip && !city && !state && !country) {
      return address;
    }

    if (address_line) {
      address += address_line + '\r\n';
    }

    if (street) {
      address += street;
    }

    address += '\r\n';

    if (zip || city) {
      if (zip) {
        address += zip + ' ';
      }

      if (city) {
        address += city;
      }
    }

    if (country || state) {
      address += '\r\n';

      if (state) {
        address += state;
        address += ' ';
      }

      if (country) {
        address += country;
      }
    }

    return address;
  }
}
