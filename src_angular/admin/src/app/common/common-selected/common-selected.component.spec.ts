import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonSelectedComponent } from './common-selected.component';

describe('CommonSelectedComponent', () => {
  let component: CommonSelectedComponent;
  let fixture: ComponentFixture<CommonSelectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonSelectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
