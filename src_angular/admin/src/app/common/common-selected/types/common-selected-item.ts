export class CommonSelectedItem {
  public get value(): boolean {
    return this.isSelect(this.id);
  }
  public set value(v: boolean) {
    this.select(this.id, v);
  }

  constructor (public id: string, public title: string,
    public errorMessages: string[], private select: Function, private isSelect: Function) {}
}
