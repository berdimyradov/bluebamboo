import { Component, forwardRef, Input, OnChanges, OnInit, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CommonSelectedItem } from './types';

@Component({
  selector: 'common-selected',
  templateUrl: './common-selected.component.html',
  styleUrls: ['./common-selected.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CommonSelectedComponent),
      multi: true
    }
  ],
  encapsulation: ViewEncapsulation.None
})
export class CommonSelectedComponent implements OnInit, OnChanges, ControlValueAccessor {
  public isCollapse = true;
  public list: CommonSelectedItem[] = [];
  private _value: any[] = [];

  @Input('selectFirst') selectFirst: boolean;
  @Input('headerName') public headerName: string;
  @Input('title') public title: string;
  @Input('list') private dictionaryList: any[];
  @Input('key-field') private keyField = 'id';
  @Input('text-field') private textField = 'title';
  @Input('search-field') private searchField = 'id';
  @Input('error-messages-field') private errorMessagesField = 'missing_fields_invoice';

  private onChange: any = () => { };
  private onTouched: any = () => { };

  public get isAllSelected(): boolean {
    return !!this.list.length && this.list.every((item: CommonSelectedItem) => item.value);
  }

  public set isAllSelected(v: boolean) {
    this.list.forEach((item: CommonSelectedItem) => {
      if (item.errorMessages && !!item.errorMessages.length) {
        item.value = false;
      } else {
        item.value = v;
      }
    });
  }


  public get isAllReadOnly(): boolean {
    return this.list.every((item: CommonSelectedItem) => item.errorMessages && !!item.errorMessages.length);
  }

  public get indeterminate(): boolean {
    return this.list.some((item: CommonSelectedItem) => item.value) && !this.isAllSelected;
  }

  public onAllClick($event) {
    if (this.list.some((item: CommonSelectedItem) => item.errorMessages && !!item.errorMessages.length)) {
      $event.preventDefault();

      this.isAllSelected = !this.indeterminate;
    }
  }

  public setAll(val: boolean){
      this.isAllSelected = val;
  }

  ngOnInit() {
    if (this.selectFirst && this.list.length === 1) {
      setTimeout(() => {
        this.list[0].value = true;
      });
    } else {
      this.setAll(false);
    }
  }

  ngOnChanges() {
    this.list = this.dictionaryList.map((item: any) =>
      new CommonSelectedItem(
        item[this.keyField],
        item[this.textField],
        item[this.errorMessagesField],
        (id: string, state: boolean) => {
          const result: any = this._value.find((it: any) => it[this.searchField] == id);

          if (state) {
            if (result === undefined) {
              const el = {};

              el[this.searchField] = id;

              this._value.push(el);
            }
          } else {
            if (result !== undefined) {
              const idx = this._value.indexOf(result);

              this._value.splice(idx, 1);
            }
          }
        },
        (id: string) => this._value.find((it: any) => it[this.searchField] == id) !== undefined
      )
    );
  }

  public toggle() {
    this.isCollapse = !this.isCollapse;
  }

  writeValue(obj: any[]): void {
    this._value = obj || [];
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }
}
