import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-ui-fake-window',
  templateUrl: './fake-window.component.html',
  styleUrls: ['./fake-window.component.css'],
})

  export class FakeWindowComponent implements OnInit {
  private __fakedUrl: string;
  @Input('fakeUrl') fakeUrl: string;

  ngOnInit() {
  }

  constructor(private sanitizer: DomSanitizer) {}
}
