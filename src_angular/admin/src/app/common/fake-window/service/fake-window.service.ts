import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

function _window(): any {
  return window;
}
@Injectable()
export class FakeWindowService {
  public urlData: Subject<any> = new Subject();

  url(value: string) {
    this.urlData.next(value);
  }

  get nativeWindow(): any {
    return _window();
  }
}
