/**
    * Created by aleksay on 18.04.2017.
    * Project: tmp_booking
    */
    import { Component, forwardRef, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';
    import { CalendarMonth } from './types/calendar-month';
    import { CalendarWeekday } from './types/calendar-weekday';
    import { CalendarDay } from './types/calendar-day';
    import { CalendarDayState } from './types/calendar-day-state';
    import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
    import { DateService } from '../services/date.service';
    import { ViewEncapsulation } from '@angular/core';

    @Component({
      selector: 'calendar',
      styleUrls: ['./calendar.scss'],
      templateUrl: './calendar.html',
      encapsulation: ViewEncapsulation.None,
      providers: [
        {
          provide: NG_VALUE_ACCESSOR,
          useExisting: forwardRef(() => CalendarComponent),
          multi: true
        }
      ]
    })
    export class CalendarComponent implements ControlValueAccessor, OnChanges, OnInit {
      public _selectedDay: CalendarDay = null;
      private _months: string[];
      private _weekdays: string[];

      @Input('first-day-of-week') public firstDayOfWeek: string = 'Mon';
      @Input('min-date') public minDate: Date = new Date(1970, 1, 1, 0, 0);
      @Input('max-date') public maxDate: Date = new Date(2900, 1, 1, 0, 0);

      @Output()
      monthChanged = new EventEmitter();

      public weekdays: CalendarWeekday[] = [];
      public currentMonth: CalendarMonth = null;
      public get selectedDay(): CalendarDay { return this._selectedDay; };
      public set selectedDay(value: CalendarDay) { this.setValue(value); };

      @Input('get-day-state')
      public getDayState: Function = (): CalendarDayState => { return new CalendarDayState() };

      private onChange: any = () => { };
      private onTouched: any = () => { };

      constructor(private dateService: DateService) {
        this._weekdays = dateService.Weekdays;
        this._months = dateService.Months;
      }

      ngOnInit() {
        this.init();
      }

      ngOnChanges() {
        this.init();
      }

      private init() {
        this.buildWeekdays();
        let currentDate = (
          this.minDate.getDate() == 1 && this.minDate.getMonth() == 1
            && this.minDate.getFullYear() == 1970 ? new Date() : this.minDate);

        this.currentMonth = new CalendarMonth(this._months,
          (day: number, month: number, year: number) => {
            let date: Date = new Date(year, month, day, 0, 0),
              time = date.getTime();

            if (this.minDate.getTime() > time || this.maxDate.getTime() < time) {
              let state: CalendarDayState = new CalendarDayState();
              state.isEnabled = false;
              return state;
            }

            return this.getDayState(day, month, year);
          },
          currentDate, this.minDate, this.maxDate);
      }

      private buildWeekdays() {
        let weekdays: CalendarWeekday[] = [],
          operShift: boolean = true;
        for (let i = this._weekdays.length - 1; i >= 0; i--) {
          let wDay: CalendarWeekday = new CalendarWeekday(this._weekdays[i], i + 1, i > 4);
          if (operShift) {
            weekdays.unshift(wDay);
          } else {
            weekdays.push(wDay);
          }
          if (wDay.day.toUpperCase() == this.firstDayOfWeek.toUpperCase()) {
            operShift = false;
          }
        }
        this.weekdays = weekdays;
      }

      private setValue(v: CalendarDay) {
        if (!v && !this._selectedDay) {
          return;
        }
        if (v && this._selectedDay && (v.day == this._selectedDay.day && v.month == this._selectedDay.month && v.year == this._selectedDay.year)) {
          return;
        }
        this._selectedDay = v;
        this.onChange(v ? new Date(v.year, v.month, v.day) : null);
        this.onTouched();
      }

      public nextMonth() {
        this.currentMonth.next();
        this.monthChanged.emit(
          {
            month: this.currentMonth.index + 1,
            year: this.currentMonth.year
          }
        );
      }

      public prevMonth() {
        this.currentMonth.prev();
        this.monthChanged.emit(
          {
            month: this.currentMonth.index + 1,
            year: this.currentMonth.year
          }
        );
      }

      public select(day: CalendarDay) {
        if (day && day.state.isEnabled) {
          this.selectedDay = day;
        }
      }

      writeValue(obj: Date): void {
        if (obj) {
          this._selectedDay = new CalendarDay(obj.getDate(), obj.getMonth(), obj.getFullYear(), obj.getDay() == 0 || obj.getDay() == 6, this.getDayState)
        } else {
          this._selectedDay = null;
        }
      }

      registerOnChange(fn: any): void {
        this.onChange = fn;
      }

      registerOnTouched(fn: any): void {
        this.onTouched = fn;
      }

      setDisabledState(isDisabled: boolean): void {
      }
    }
