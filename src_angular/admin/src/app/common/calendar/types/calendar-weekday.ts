/**
    * Created by aleksay on 19.04.2017.
    * Project: tmp_booking
    */
export class CalendarWeekday{
    constructor(public day:string, public index:number, public isWeekend: boolean = false){}
}
