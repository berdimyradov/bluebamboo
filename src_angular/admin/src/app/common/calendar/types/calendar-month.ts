/**
    * Created by aleksay on 19.04.2017.
    * Project: tmp_booking
    */
import {CalendarDay} from "./calendar-day";

export class CalendarMonth{
    public index: number;
    public year: number;
    public get month():string {
        return this.monthsNames[this.index];
    }
    public weeks: CalendarDay[][] = [];
    public prevMonthEnabled: boolean = true;
    public nextMonthEnabled: boolean = true;

    constructor(private monthsNames: string[], private getDayState: Function, current: Date = new Date(), private minDate: Date, private maxDate: Date){
        this.index = current.getMonth();
        this.year = current.getFullYear();
        this.buildDays();
        this.calculateButtonEnabled();
    }

    private buildDays(){
        let wDays: CalendarDay[] = [],
            startIndex = (new Date(this.year, this.index, 1, 0 , 0)).getDay();

        for(let i:number = 1; i < (startIndex == 0 ? 7 : startIndex); i++){
            wDays.push(null);
        }

        for(let i: number = 1; i <= 31; i++){
            let d: Date = new Date(this.year, this.index, i, 0 , 0),
                wd = d.getDay();
            if (d.getMonth() != this.index){
                continue;
            }
            wDays.push(new CalendarDay(i, this.index, this.year, wd === 0 || wd === 6, this.getDayState));
        }

        for(let i: number = wDays.length; i < 6*7; i++){
            wDays.push(null);
        }

        let weeks: CalendarDay[][] = [];
        for(let i: number = 0; i < 6; i++){
            weeks.push(wDays.splice(0,7));
        }
        this.weeks = weeks;
    }

    private compareDate(d1: Date, d2: Date): string{
        let res:string = '==';
        if ((d1.getFullYear() > d2.getFullYear()) ||
            (d1.getFullYear() == d2.getFullYear() && d1.getMonth() > d2.getMonth())
        ){
            return ">";
        }
        if ((d2.getFullYear() > d1.getFullYear()) ||
            (d2.getFullYear() == d1.getFullYear() && d2.getMonth() > d1.getMonth())
        ){
            return "<";
        }
    }
    private dateIsCorrect(date: Date):boolean{
        return this.compareDate(this.minDate, date) != '>' && this.compareDate(this.maxDate, date) != '<';
    }

    private calculateButtonEnabled(){
        this.prevMonthEnabled = this.dateIsCorrect(new Date(this.year, this.index - 1, 1, 0, 0, 0, 0));
        this.nextMonthEnabled = this.dateIsCorrect(new Date(this.year, this.index + 1, 1, 0, 0, 0, 0));
    }

    private move(step:number){
        let d: Date = new Date(this.year, this.index + step, 1, 0 , 0);
        // if (!this.dateIsCorrect(d)){
        //     return;
        // }
        this.year = d.getFullYear();
        this.index = d.getMonth();
        this.buildDays();
        this.calculateButtonEnabled();
    }

    public next(){
        this.move(1);
    }

    public prev(){
        this.move(-1);
    }
}
