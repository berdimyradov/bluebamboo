/**
    * Created by aleksay on 19.04.2017.
    * Project: tmp_booking
    */
import {CalendarDayState} from "./calendar-day-state";
export class CalendarDay {
    public get state(): CalendarDayState{
        return this.getDayState(this.day, this.month, this.year);
    }

    constructor(public day: number, public month: number, public year: number, public isWeekend: boolean, private getDayState: Function){}

    public isSame(day: CalendarDay):boolean{
        if (!day){
            return false;
        }
        return this.day == day.day && this.month == day.month && this.year == day.year;
    }
}