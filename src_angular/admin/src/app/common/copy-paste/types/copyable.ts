/**
    * Created by aleksay on 04.05.2017.
    * Project: tmp_booking
    */
export interface Copyable {
    copyFrom(src: Copyable):void
}