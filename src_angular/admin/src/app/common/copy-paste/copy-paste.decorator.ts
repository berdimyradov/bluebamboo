/**
    * Created by aleksay on 27.04.2017.
    * Project: tmp_booking
    */
import {CopyPasteContainer} from "./copy-paste-container";

export function CopyPaste() {
    return function (target:any) {
        Object.assign(target.prototype,  CopyPasteContainer.prototype);
    };
}