/**
    * Created by aleksay on 27.04.2017.
    * Project: tmp_booking
    */
import {Copyable} from "./types/copyable";
export class CopyPasteContainer{
    private copyObj: any;

    public pasteItem(src: Copyable, dst: Copyable){
        dst.copyFrom(src);
    }

    public canPaste(): boolean{
        return this.copyObj ? true : false;
    }

    public getCopyObj(): any{
        return this.copyObj;
    }

    public copy(obj: any){
        this.copyObj = obj;
    }

    public paste(...args:any[]){
        this.pasteItem.apply(this, [this.copyObj, ...args]);
    }
}
