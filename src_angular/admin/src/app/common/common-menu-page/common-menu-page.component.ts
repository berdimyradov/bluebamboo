import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {CommonMenuItem} from "./types/";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'admin-common-menu-page',
  templateUrl: './common-menu-page.component.html',
  styleUrls: ['./common-menu-page.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CommonMenuPageComponent implements OnInit {
  @Input() public list: CommonMenuItem[] = [];
  @Input('resource-path') public resourcePath: string;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public executeAction(item: CommonMenuItem) {
    if (item.implement) {
//      console.log("common-menu-page.executeAction: " + item.code);
//      console.log("active route: " + this.activeRoute.toString());
      this.router.navigate(['./'+item.code], {relativeTo: this.activeRoute}).then((res) => {
        console.log("navigate res = " + res);
      });
    }
  }
}
