export class CommonMenuItem {
  public code: string;
  public icon: string;
  public implement: boolean = false;

  public parse( data: any ): CommonMenuItem {
    this.code = data.code;
    this.icon = data.icon;
    this.implement = data.implement;
    return this;
  }
}

