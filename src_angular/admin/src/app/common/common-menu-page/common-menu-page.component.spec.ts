import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonMenuPageComponent } from './common-menu-page.component';

describe('CommonMenuPageComponent', () => {
  let component: CommonMenuPageComponent;
  let fixture: ComponentFixture<CommonMenuPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonMenuPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonMenuPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
