import {Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {CommonMenuItem} from "../types";

@Component({
  selector: 'admin-common-menu-item',
  templateUrl: './common-menu-item.component.html',
  styleUrls: ['./common-menu-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CommonMenuItemComponent implements OnInit {
  @HostBinding('class.col-xs-6')private classXs6 = true;
  @HostBinding('class.col-md-3')private classMd3 = true;
  @HostBinding('class.not-implemented')private classNotImplemented = true;
  @Input() public item: CommonMenuItem;
  @Input('resource-path') public resourcePath: string;
  @Output() public action: EventEmitter<CommonMenuItem> = new EventEmitter<CommonMenuItem>();

  constructor() { }

  ngOnInit() {
    this.classNotImplemented = !this.item.implement;
  }

  public click() {
    this.action.emit(this.item);
  }
}
