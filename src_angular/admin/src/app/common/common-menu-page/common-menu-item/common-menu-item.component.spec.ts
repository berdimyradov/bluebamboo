import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonMenuItemComponent } from './common-menu-item.component';

describe('CommonMenuItemComponent', () => {
  let component: CommonMenuItemComponent;
  let fixture: ComponentFixture<CommonMenuItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonMenuItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonMenuItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
