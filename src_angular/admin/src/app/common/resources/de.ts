export const locale = {
  "ENUM": {
    "INVOICE_STATE": {
      "ope": {"TITLE": "Offen", "DESCRIPTION": "Open, not yet payed invoice"},
      "re1": {"TITLE": "1. Mahnung", "DESCRIPTION": "Already first reminder sent"},
      "re2": {"TITLE": "2. Mahnung", "DESCRIPTION": "Already second reminder sent"},
      "can": {"TITLE": "Storniert", "DESCRIPTION": "Canceled Invoice"},
      "pay": {"TITLE": "Bezahlt", "DESCRIPTION": "Fully payed invoice"},
      "dep": {"TITLE": "Angezahlt", "DESCRIPTION": "Already a deposit made by customer (not yet implemented)"},
      "dr1": {"TITLE": "1. Mahnung", "DESCRIPTION": "First reminder sent and already deposited"},
      "dr2": {"TITLE": "2. Mahnung", "DESCRIPTION": "Second reminder send and already deposited"}
    }
  },
  "COMPONENT": {
    "COMMON-LIST": {
      "BACK": "Zurück",
	    "ADD": "Neu anlegen"
    },
    "COMMON-FORM": {
      "BACK": "Zurück",
      "SAVE": "Speichern",
      "DELETE": "Löschen",
      "EDIT": "Bearbeiten"
    },
    "COMMON-MENU-PAGE": {
      "BACK": "Zurück"
    },
    "COMMON-MENU-ITEM": {
      "ACTION": "Anzeigen"
    },
    "COMMON-SELECTED": {
      "ALL": "Alle"
    },
    "DIALOG": {
      "CONFIRM": {
        "YES": "Ja",
        "NO": "Nein"
      }
    },
    "COMMON-PAGE": {
      "USER": {
        "LOGOUT": "Abmelden"
      }
    },
    "COMMON-CUSTOMER-CARD": {
      "ADDRESS": "Adresse",
      "EMAIL": "E-Mail",
      "PHONE": "Telefon"
    },
    "MDB-UPLOAD": {
      "IMAGE": "Bild",
      "NO-IMAGE": "Noch kein Bild.",
      "BUTTON-TEXT": "Ändern",
      "PLACEHOLDER-TEXT": "Upload your file"
    },
    "GRID": {
      "PAGER": {
        "FIRST": "Erste",
        "LAST": "Letzte"
      }
    }
  }
};
