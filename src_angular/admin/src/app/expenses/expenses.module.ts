import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CommonUiModule } from '@bluebamboo/common-ui';
import { AdminCommonModule } from '../common';

import { locale as en } from './resources/en';
import { locale as de } from './resources/de';

import { ExpensesService }                from 'app/expenses/services/expenses.service';

import { ExpensesTypeItemComponent }      from 'app/expenses/expenses/expenses-add/expenses-type-item/expenses-type-item.component';

import { ExpensesComponent }              from 'app/expenses/expenses/expenses.component';
import { ExpensesListComponent }          from 'app/expenses/expenses/expenses-list/expenses-list.component';
import { ExpensesAddComponent }           from 'app/expenses/expenses/expenses-add/expenses-add.component';
import { ExpensesAddFormComponent }       from 'app/expenses/expenses/expenses-add/expenses-add-form/expenses-add-form.component';

import { ExpensesCashBoxComponent }       from 'app/expenses/expenses/expenses-cashbox/expenses-cashbox.component';
import { ExpensesCashBoxFormComponent }   from 'app/expenses/expenses/expenses-cashbox/expenses-cashbox-form/expenses-cashbox-form.component';
import { ExpensesCashBoxResolver }        from 'app/expenses/resolvers/expenses-cashbox.resolver';

import { ExpensesBankAccountComponent }       from 'app/expenses/expenses/expenses-bankaccount/expenses-bankaccount.component';
import { ExpensesBankAccountFormComponent }   from 'app/expenses/expenses/expenses-bankaccount/expenses-bankaccount-form/expenses-bankaccount-form.component';
import { ExpensesBankAccountResolver }        from 'app/expenses/resolvers/expenses-bankaccount.resolver';

import { ExpensesTypeComponent }          from 'app/expenses/expenses/expenses-type/expenses-type.component';
import { ExpensesTypeFormComponent }      from 'app/expenses/expenses/expenses-type/expenses-type-form/expenses-type-form.component';
import { ExpensesTypeResolver }           from 'app/expenses/resolvers/expenses-type.resolver';

@NgModule({
  imports: [CommonModule, CommonUiModule, AdminCommonModule, FormsModule, BrowserAnimationsModule, TranslateModule, RouterModule],
  declarations: [
    ExpensesTypeItemComponent,

    ExpensesComponent,
    ExpensesListComponent,
    ExpensesAddComponent,
    ExpensesAddFormComponent,

    ExpensesCashBoxComponent,
    ExpensesCashBoxFormComponent,

    ExpensesBankAccountComponent,
    ExpensesBankAccountFormComponent,

    ExpensesTypeComponent,
    ExpensesTypeFormComponent

  ],

  providers: [

    ExpensesService,
    ExpensesCashBoxResolver,
    ExpensesBankAccountResolver,
    ExpensesTypeResolver

  ]
})

export class ExpensesModule {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
  }
}
