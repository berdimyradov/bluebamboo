export class ExpenseDoc {
  public id: number;
  public path: string;
  public notes: string;

  public parse( data: any ): ExpenseDoc {
    this.id         = data.id         || this.id;
    this.path       = data.path       || this.path;
    this.notes      = data.notes      || this.notes;
    return this;
  }
}
