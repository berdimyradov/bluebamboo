export class Expense {
  public id: number;
  public name: string;
  public place: string;
  public amount: number;
  public distance: string;
  public participants: string;
  public notes: string;
  public type_id: number;

  public parse( data: any ): Expense {
    this.id           = data.id           || this.id;
    this.name         = data.name         || this.name;
    this.place        = data.place        || this.place;
    this.amount       = data.amount       || this.amount;
    this.distance     = data.distance     || this.distance;
    this.participants = data.participants || this.participants;
    this.notes        = data.notes        || this.notes;
    this.type_id      = data.type_id      || this.type_id;
    return this;
  }
}
