export class AccountCode {
  public id: number;
  public country: string;
  public name: string;
  public notes: string;
  public platform_id: number;

  public parse( data: any ): AccountCode {
    this.id           = data.id           || this.id;
    this.country         = data.country         || this.country;
    this.name        = data.name        || this.name;
    this.notes       = data.notes       || this.notes;
    this.platform_id     = data.platform_id     || this.platform_id;
    return this;
  }
}

