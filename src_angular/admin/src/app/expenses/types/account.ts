export class Account {
  public id: number;
  public name: string;
  public section: string;
  public maingroup: string;
  public subgroup: string;
  public parent: number;
  public number: string;
  public opening: number;
  public debit: number;
  public credit: number;
  public balance: number;
  public notes: number;
  public accountscodes_id: number;

  public parse( data: any ): Account {
    this.id           = data.id           || this.id;
    this.name         = data.name         || this.name;
    this.section        = data.section        || this.section;
    this.maingroup       = data.maingroup       || this.maingroup;
    this.subgroup     = data.subgroup     || this.subgroup;
    this.parent = data.parent || this.parent;
    this.number        = data.number        || this.number;
    this.opening      = data.opening      || this.opening;
    this.debit      = data.debit      || this.debit;
    this.credit      = data.credit      || this.credit;
    this.balance      = data.balance      || this.balance;
    this.notes      = data.notes      || this.notes;
    this.accountscodes_id      = data.accountscodes_id      || this.accountscodes_id;
    return this;
  }
}

