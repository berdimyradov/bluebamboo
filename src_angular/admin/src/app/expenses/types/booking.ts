export class Booking {
  public id: number;
  public book_date: Date;
  public val_date: Date;
  public doc_date: Date;
  public doc_number: string;
  public description: string;
  public acc_debit_id: number;
  public acc_credit_id: number;
  public amount: number;
  public amount_foreign: number;
  public currency: string;

  public parse( data: any ): Booking {
    this.id           = data.id           || this.id;
    this.book_date         = data.book_date         || this.book_date;
    this.val_date        = data.val_date        || this.val_date;
    this.doc_date       = data.doc_date       || this.doc_date;
    this.doc_number     = data.doc_number     || this.doc_number;
    this.description = data.description || this.description;
    this.description        = data.description        || this.description;
    this.acc_debit_id      = data.acc_debit_id      || this.acc_debit_id;
    this.acc_credit_id      = data.acc_credit_id      || this.acc_credit_id;
    this.amount      = data.amount      || this.amount;
    this.amount_foreign      = data.amount_foreign      || this.amount_foreign;
    this.currency      = data.currency      || this.currency;
    return this;
  }
}

