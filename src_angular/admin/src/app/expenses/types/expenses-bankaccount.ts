export class ExpensesBankAccount {
  public id: number;
  public name: string;
  public notes: string;
  public account_id: number;

  public parse( data: any ): ExpensesBankAccount {
    this.id         = data.id         || this.id;
    this.name       = data.name       || this.name;
    this.notes      = data.notes      || this.notes;
    this.account_id = data.account_id || this.account_id;
    return this;
  }
}


