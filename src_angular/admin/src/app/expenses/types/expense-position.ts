export class ExpensePosition {
  public id: number;
  public name: string;
  public place: string;
  public amount: number;
  public personal: boolean;
  public notes: string;
  public account_id: number;

  public parse( data: any ): ExpensePosition {
    this.id         = data.id         || this.id;
    this.name       = data.name       || this.name;
    this.place      = data.place      || this.place;
    this.amount     = data.amount     || this.amount;
    this.personal   = data.personal   || this.personal;
    this.account_id = data.account_id || this.account_id;
    this.notes      = data.notes      || this.notes;
    return this;
  }
}
