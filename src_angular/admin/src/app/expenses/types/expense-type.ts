export class ExpenseType {
  public id: number;
  public name: string;
  public icon: string;
  public notes: string;

  public parse( data: any ): ExpenseType {
    this.id         = data.id         || this.id;
    this.name       = data.name       || this.name;
    this.icon       = data.icon       || this.icon;
    this.notes      = data.notes      || this.notes;
    return this;
  }
}


