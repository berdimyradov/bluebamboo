import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions} from "@angular/http";
import { Observable } from "rxjs/Observable";

import { FrameworkService, GenericResultModel } from 'app/framework/';

import { ExpensesCashBox }      from "app/expenses/types/expenses-cashbox";
import { ExpensesBankAccount }  from "app/expenses/types/expenses-bankaccount";
import { ExpenseType }          from "app/expenses/types/expense-type";
import { ExpenseDoc }           from "app/expenses/types/expense-doc";
import { ExpensePosition }      from "app/expenses/types/expense-position";
import { Expense }              from "app/expenses/types/expense";

@Injectable()

export class ExpensesService extends FrameworkService {

  constructor (http: Http) { super(http); }

  // -------------------------------------------------------------------------------------
  // Cash Boxes

  public getCashBoxes(): Observable<ExpensesCashBox[]> {
    return this.http.get('/api/v1/accounting/cashboxes')
      .map((res: Response) => res.json().map((it: any) => new ExpensesCashBox().parse(it)))
      .catch(this.handleError);
  }

	public getCashBox(id: number): Observable<ExpensesCashBox> {
		return this.http.get('/api/v1/accounting/cashboxes/' + id)
			.map((res: Response) => new ExpensesCashBox().parse(res.json()))
			.catch(this.handleError);
	}

  public saveCashBox(obj: ExpensesCashBox): Observable<ExpensesCashBox> {
    const res: Observable<Response> = obj.id
      ? this.http.put('/api/v1/accounting/cashboxes/' + obj.id, obj)
      : this.http.post('/api/v1/accounting/cashboxes/', obj);
    return res.map((res: Response) => new ExpensesCashBox().parse(res.json()))
      .catch(this.handleError);
  }

  public deleteCashBox(id: number): Observable<void> {
    return this.http.delete('/api/v1/accounting/cashboxes/' + id)
      .map(() => {return;})
      .catch(this.handleError);
  }

  // -------------------------------------------------------------------------------------
  // Bank Accounts

  public getBankAccounts(): Observable<ExpensesBankAccount[]> {
    return this.http.get('/api/v1/accounting/bankaccounts')
      .map((res: Response) => res.json().map((it: any) => new ExpensesBankAccount().parse(it)))
      .catch(this.handleError);
  }

	public getBankAccount(id: number): Observable<ExpensesBankAccount> {
		return this.http.get('/api/v1/accounting/bankaccounts/' + id)
			.map((res: Response) => new ExpensesBankAccount().parse(res.json()))
			.catch(this.handleError);
	}

  public saveBankAccount(obj: ExpensesBankAccount): Observable<ExpensesBankAccount> {
    const res: Observable<Response> = obj.id
      ? this.http.put('/api/v1/accounting/bankaccounts/' + obj.id, obj)
      : this.http.post('/api/v1/accounting/bankaccounts/', obj);
    return res.map((res: Response) => new ExpensesBankAccount().parse(res.json()))
      .catch(this.handleError);
  }

  public deleteBankAccount(id: number): Observable<void> {
    return this.http.delete('/api/v1/accounting/bankaccounts/' + id)
      .map(() => {return;})
      .catch(this.handleError);
  }
  // -------------------------------------------------------------------------------------
  // Expense Type

  public getExpenseTypes(): Observable<ExpenseType[]> {
    return this.http.get('/api/v1/accounting/exptypes')
      .map((res: Response) => res.json().map((it: any) => new ExpenseType().parse(it)))
      .catch(this.handleError);
  }

	public getExpenseType(id: number): Observable<ExpenseType> {
		return this.http.get('/api/v1/accounting/exptypes/' + id)
			.map((res: Response) => new ExpenseType().parse(res.json()))
			.catch(this.handleError);
	}

  public saveExpenseType(obj: ExpenseType): Observable<ExpenseType> {
    const res: Observable<Response> = obj.id
      ? this.http.put('/api/v1/accounting/exptypes/' + obj.id, obj)
      : this.http.post('/api/v1/accounting/exptypes/', obj);
    return res.map((res: Response) => new ExpenseType().parse(res.json()))
      .catch(this.handleError);
  }

  public deleteExpenseType(id: number): Observable<void> {
    return this.http.delete('/api/v1/accounting/exptypes/' + id)
      .map(() => {return;})
      .catch(this.handleError);
  }

  // -------------------------------------------------------------------------------------
  // Expense Doc

  public getExpenseDocs(): Observable<ExpenseDoc[]> {
    return this.http.get('/api/v1/accounting/expdocs')
      .map((res: Response) => res.json().map((it: any) => new ExpenseDoc().parse(it)))
      .catch(this.handleError);
  }

	public getExpenseDoc(id: number): Observable<ExpenseDoc> {
		return this.http.get('/api/v1/accounting/expdocs/' + id)
			.map((res: Response) => new ExpenseDoc().parse(res.json()))
			.catch(this.handleError);
	}

  public saveExpenseDoc(obj: ExpenseDoc): Observable<ExpenseDoc> {
    const res: Observable<Response> = obj.id
      ? this.http.put('/api/v1/accounting/expdocs/' + obj.id, obj)
      : this.http.post('/api/v1/accounting/expdocs/', obj);
    return res.map((res: Response) => new ExpenseDoc().parse(res.json()))
      .catch(this.handleError);
  }

  public deleteExpenseDoc(id: number): Observable<void> {
    return this.http.delete('/api/v1/accounting/expdocs/' + id)
      .map(() => {return;})
      .catch(this.handleError);
  }

  // -------------------------------------------------------------------------------------
  // Expense Position

  public getExpensePositions(): Observable<ExpensePosition[]> {
    return this.http.get('/api/v1/accounting/exppositions')
      .map((res: Response) => res.json().map((it: any) => new ExpensePosition().parse(it)))
      .catch(this.handleError);
  }

	public getExpensePosition(id: number): Observable<ExpensePosition> {
		return this.http.get('/api/v1/accounting/exppositions/' + id)
			.map((res: Response) => new ExpensePosition().parse(res.json()))
			.catch(this.handleError);
	}

  public saveExpensePosition(obj: ExpensePosition): Observable<ExpensePosition> {
    const res: Observable<Response> = obj.id
      ? this.http.put('/api/v1/accounting/exppositions/' + obj.id, obj)
      : this.http.post('/api/v1/accounting/exppositions/', obj);
    return res.map((res: Response) => new ExpensePosition().parse(res.json()))
      .catch(this.handleError);
  }

  public deleteExpensePosition(id: number): Observable<void> {
    return this.http.delete('/api/v1/accounting/exppositions/' + id)
      .map(() => {return;})
      .catch(this.handleError);
  }

  // -------------------------------------------------------------------------------------
  // Expense

  public getExpenses(): Observable<Expense[]> {
    return this.http.get('/api/v1/accounting/expenses')
      .map((res: Response) => res.json().map((it: any) => new Expense().parse(it)))
      .catch(this.handleError);
  }

	public getExpense(id: number): Observable<Expense> {
		return this.http.get('/api/v1/accounting/expenses/' + id)
			.map((res: Response) => new Expense().parse(res.json()))
			.catch(this.handleError);
	}

  public saveExpense(obj: Expense): Observable<Expense> {
    const res: Observable<Response> = obj.id
      ? this.http.put('/api/v1/accounting/expenses/' + obj.id, obj)
      : this.http.post('/api/v1/accounting/expenses/', obj);
    return res.map((res: Response) => new Expense().parse(res.json()))
      .catch(this.handleError);
  }

  public deleteExpense(id: number): Observable<void> {
    return this.http.delete('/api/v1/accounting/expenses/' + id)
      .map(() => {return;})
      .catch(this.handleError);
  }

}
