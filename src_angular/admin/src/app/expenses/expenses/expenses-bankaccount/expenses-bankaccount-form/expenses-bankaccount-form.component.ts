import { Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ExpensesBankAccount } from "app/expenses/types/expenses-bankaccount";
import { ExpensesService } from 'app/expenses/services/expenses.service';

@Component({
  selector:       'expenses-bankaccount-form',
  templateUrl:    './expenses-bankaccount-form.component.html',
  styleUrls:      ['./expenses-bankaccount-form.component.scss'],
  encapsulation:  ViewEncapsulation.None
})

export class ExpensesBankAccountFormComponent implements OnInit {

  public item = new ExpensesBankAccount();
  public sentRequest = false;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private service : ExpensesService
  ) { }

  ngOnInit() {
    this.item = this.activeRoute.snapshot.data.item;
  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public canSave () {
    return !this.sentRequest;
  }

  public save() {
    if (this.sentRequest) return;
    this.sentRequest = true;

//  console.log("save support request: " + JSON.stringify(this.supportMessage));

    this.service.saveBankAccount(this.item).subscribe(
      (res) => { this.router.navigate(['../../'], { relativeTo: this.activeRoute }); }
    );

  }

}

