import { Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ExpensesCashBox } from "app/expenses/types/expenses-cashbox";
import { ExpensesService } from 'app/expenses/services/expenses.service';

@Component({
  selector:       'expenses-cashbox-form',
  templateUrl:    './expenses-cashbox-form.component.html',
  styleUrls:      ['./expenses-cashbox-form.component.scss'],
  encapsulation:  ViewEncapsulation.None
})

export class ExpensesCashBoxFormComponent implements OnInit {

  public item = new ExpensesCashBox();
  public sentRequest = false;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private service : ExpensesService
  ) { }

  ngOnInit() {
    this.item = this.activeRoute.snapshot.data.item;
  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public canSave () {
    return !this.sentRequest;
  }

  public save() {
    if (this.sentRequest) return;
    this.sentRequest = true;

//  console.log("save support request: " + JSON.stringify(this.supportMessage));

    this.service.saveCashBox(this.item).subscribe(
      (res) => { this.router.navigate(['../../'], { relativeTo: this.activeRoute }); }
    );

  }

}

