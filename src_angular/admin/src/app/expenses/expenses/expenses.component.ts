import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonMenuItem } from "../../common";

import { ExpensesService } from "../services/expenses.service";

@Component({
	selector:		    'app-expenses',
	templateUrl:	  './expenses.component.html',
	styleUrls:		  ['./expenses.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			      {'[@routeAnimationOpacity]': 'true'},
	animations:		  [routeAnimationOpacity]
})

export class ExpensesComponent implements OnInit {

	public modules: CommonMenuItem[] = [

			new CommonMenuItem().parse({
				code: 'add',
				icon: 'fa-key',
				implement: true
			}),

			new CommonMenuItem().parse({
				code: 'list',
				icon: 'fa-key',
				implement: true
			}),

			new CommonMenuItem().parse({
				code: 'types',
				icon: 'fa-key',
				implement: true
			}),

			new CommonMenuItem().parse({
				code: 'cashboxes',
				icon: 'fa-key',
				implement: true
			}),

			new CommonMenuItem().parse({
				code: 'bankaccounts',
				icon: 'fa-key',
				implement: true
			}),

	];

	constructor(
	  private service: ExpensesService,
	  private commonPage: CommonPageComponent
	)
	  {}

	ngOnInit() {
		this.commonPage.title = 'PAGE.EXPENSES.DESKTOP.PAGE_TITLE';
		/*
		this.service.getListModules().subscribe(
			result => this.modules = result
		);
		*/
	}
}
