import { Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Expense }          from "app/expenses/types/expense";
import { ExpenseType }      from "app/expenses/types/expense-type";
import { ExpensesService }  from "app/expenses/services/expenses.service";

@Component({
  selector:       'expenses-add-form',
  templateUrl:    './expenses-add-form.component.html',
  styleUrls:      ['./expenses-add-form.component.scss'],
  encapsulation:  ViewEncapsulation.None
})

export class ExpensesAddFormComponent implements OnInit {

  public expType: ExpenseType = null;
  public item = new Expense();
  public sentRequest = false;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
	  private service: ExpensesService,
  ) { }

  ngOnInit() {
    this.expType = this.activeRoute.snapshot.data.etype;
    this.item.type_id = this.expType.id;
  }

  public back() {
    this.router.navigate(['../../add'], {relativeTo: this.activeRoute});
  }

  public canSave () {
    return !this.sentRequest;
  }

  public save() {
    if (this.sentRequest) return;
    this.sentRequest = true;

    console.log("save expense: " + JSON.stringify(this.item));

    this.service.saveExpense(this.item).subscribe(
      (res) => {
        this.router.navigate(['../../'], { relativeTo: this.activeRoute });
      }
    );

  }

}

