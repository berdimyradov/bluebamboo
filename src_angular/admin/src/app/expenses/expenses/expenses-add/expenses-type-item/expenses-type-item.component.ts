import { Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';

import { ExpenseType } from "app/expenses/types/expense-type";

@Component({
  selector:       'expenses-type-item',
  templateUrl:    './expenses-type-item.component.html',
  styleUrls:      ['./expenses-type-item.component.scss'],
  encapsulation:  ViewEncapsulation.None
})

export class ExpensesTypeItemComponent implements OnInit {

  @HostBinding('class.col-xs-6')private classXs6 = true;
  @HostBinding('class.col-md-3')private classMd3 = true;
  @Input() public item: ExpenseType;
  @Input('resource-path') public resourcePath: string;
  @Output() public action: EventEmitter<ExpenseType> = new EventEmitter<ExpenseType>();

  constructor() { }

  ngOnInit() {
  }

  public click() {
    this.action.emit(this.item);
  }
}
