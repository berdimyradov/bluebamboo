import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent } from "app/common";
import { ActivatedRoute, Router } from '@angular/router';

import { ExpenseType }      from "app/expenses/types/expense-type";
import { ExpensesService }  from "app/expenses/services/expenses.service";

@Component({
	selector:		    'app-expenses-add',
	templateUrl:	  './expenses-add.component.html',
	styleUrls:		  ['./expenses-add.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			      {'[@routeAnimationOpacity]': 'true'},
	animations:		  [routeAnimationOpacity]
})

export class ExpensesAddComponent implements OnInit {

	public types: ExpenseType[] = [];

	constructor(
	  private service: ExpensesService,
    private router: Router,
    private activeRoute: ActivatedRoute,
	  private commonPage: CommonPageComponent
	)
	  {}

	ngOnInit() {
		this.commonPage.title = 'PAGE.EXPENSES_ADD.DESKTOP.PAGE_TITLE';

		this.service.getExpenseTypes().subscribe(
			result => this.types = result
		);

	}

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public executeAction(item: ExpenseType) {
      this.router.navigate(['../form/' + item.id], {relativeTo: this.activeRoute}).then((res) => {
        console.log("navigate res = " + res);
      });
  }




}
