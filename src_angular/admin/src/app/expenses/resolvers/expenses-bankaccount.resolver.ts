import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";

import { ExpensesService } from 'app/expenses/services/expenses.service';
import { ExpensesBankAccount } from "app/expenses/types/expenses-bankaccount";

@Injectable()

export class ExpensesBankAccountResolver implements Resolve<ExpensesBankAccount> {

  constructor (private service : ExpensesService) {}

  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): ExpensesBankAccount | Observable<ExpensesBankAccount> | Promise<ExpensesBankAccount> {
    const id: string = route.paramMap.get('id');
    if (id === 'add') return Observable.of(new ExpensesBankAccount());
    return this.service.getBankAccount(parseInt(id));
  }

}
