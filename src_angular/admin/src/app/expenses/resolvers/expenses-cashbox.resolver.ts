import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";

import { ExpensesService } from 'app/expenses/services/expenses.service';
import { ExpensesCashBox } from "app/expenses/types/expenses-cashbox";

@Injectable()

export class ExpensesCashBoxResolver implements Resolve<ExpensesCashBox> {

  constructor (private service : ExpensesService) {}

  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): ExpensesCashBox | Observable<ExpensesCashBox> | Promise<ExpensesCashBox> {
    const id: string = route.paramMap.get('id');
    if (id === 'add') return Observable.of(new ExpensesCashBox());
    return this.service.getCashBox(parseInt(id));
  }

}
