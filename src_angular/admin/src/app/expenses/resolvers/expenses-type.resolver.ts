import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";

import { ExpensesService }  from 'app/expenses/services/expenses.service';
import { ExpenseType }      from "app/expenses/types/expense-type";

@Injectable()

export class ExpensesTypeResolver implements Resolve<ExpenseType> {

  constructor (private service : ExpensesService) {}

  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): ExpenseType | Observable<ExpenseType> | Promise<ExpenseType> {
    const id: string = route.paramMap.get('id');
    if (id === 'add') return Observable.of(new ExpenseType());
    return this.service.getExpenseType(parseInt(id));
  }

}
