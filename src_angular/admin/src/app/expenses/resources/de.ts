export const locale = {
    "EXPENSES": {
      "CTA": "weiter",
      "DELETE-TITLE": "Löschbestätigung",
      "DELETE-MESSAGE": "Wollen Sie diesen Eintrag wirklich löschen?",
      "DELETE-YES": "Ja, löschen",
      "DELETE-NO": "Nein, nicht löschen"
    },
    "PAGE": {
        "EXPENSES": {
            "DESKTOP": {
              "PAGE_TITLE": "Ausgaben (erhaltene Rechnungen, Spesen etc.)",
              "ITEMS": {
                  "add": {
                      "TITLE": "Erfassen"
                  },
                  "list": {
                      "TITLE": "Anzeigen und bearbeiten"
                  },
                  "types": {
                      "TITLE": "Typen verwalten"
                  },
                  "cashboxes": {
                      "TITLE": "Kassa/Kassen verwalten"
                  },
                  "bankaccounts": {
                      "TITLE": "Bankkonto/konti verwalten"
                  }
              }
            }
        },
        "EXPENSES_ADD": {
            "DESKTOP": {
              "PAGE_TITLE": "Ausgaben und Spesen erfassen",
              "ITEMS": {
                  "dine": {
                      "TITLE": "Essen"
                  },
                  "train": {
                      "TITLE": "Zugfahrt"
                  },
                  "car": {
                      "TITLE": "Autofahrt"
                  },
                  "phone": {
                      "TITLE": "Telefon / Mobile"
                  },
                  "internet": {
                      "TITLE": "Internet"
                  },
                  "other": {
                      "TITLE": "Allgemein"
                  }
              }
            }
        }
    }
}
