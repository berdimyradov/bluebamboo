import { Routes } from "@angular/router";

import { ExpensesComponent }              from 'app/expenses/expenses/expenses.component';
import { ExpensesListComponent }          from 'app/expenses/expenses/expenses-list/expenses-list.component';
import { ExpensesAddComponent }           from 'app/expenses/expenses/expenses-add/expenses-add.component';
import { ExpensesAddFormComponent }       from 'app/expenses/expenses/expenses-add/expenses-add-form/expenses-add-form.component';

import { ExpensesCashBoxComponent }       from 'app/expenses/expenses/expenses-cashbox/expenses-cashbox.component';
import { ExpensesCashBoxFormComponent }   from 'app/expenses/expenses/expenses-cashbox/expenses-cashbox-form/expenses-cashbox-form.component';
import { ExpensesCashBoxResolver }        from 'app/expenses/resolvers/expenses-cashbox.resolver';

import { ExpensesBankAccountComponent }       from 'app/expenses/expenses/expenses-bankaccount/expenses-bankaccount.component';
import { ExpensesBankAccountFormComponent }   from 'app/expenses/expenses/expenses-bankaccount/expenses-bankaccount-form/expenses-bankaccount-form.component';
import { ExpensesBankAccountResolver }        from 'app/expenses/resolvers/expenses-bankaccount.resolver';

import { ExpensesTypeComponent }          from 'app/expenses/expenses/expenses-type/expenses-type.component';
import { ExpensesTypeFormComponent }      from 'app/expenses/expenses/expenses-type/expenses-type-form/expenses-type-form.component';
import { ExpensesTypeResolver }           from 'app/expenses/resolvers/expenses-type.resolver';

export const expensesRoutes : Routes = [

// expenses desktop ----------------------------------------------------------------------
	{
		path: "expenses",
    pathMatch: 'full',
		component: ExpensesComponent
	},

// list and add expenses -----------------------------------------------------------------
	{
		path: "expenses/add",
    pathMatch: 'full',
		component: ExpensesAddComponent
	},
	{
		path: "expenses/form/:id",
    pathMatch: 'full',
		component: ExpensesAddFormComponent,
    resolve: {
      etype: ExpensesTypeResolver
		}
	},
	{
		path: "expenses/list",
    pathMatch: 'full',
		component: ExpensesListComponent
	},

// list and add cashboxes ----------------------------------------------------------------
	{
		path: "expenses/cashboxes",
    pathMatch: 'full',
		component: ExpensesCashBoxComponent
	},
	{
		path: "expenses/cashboxes/form/:id",
    pathMatch: 'full',
		component: ExpensesCashBoxFormComponent,
    resolve: {
      item: ExpensesCashBoxResolver
		}
	},

// list and add bankaccounts -------------------------------------------------------------
	{
		path: "expenses/bankaccounts",
    pathMatch: 'full',
		component: ExpensesBankAccountComponent
	},
	{
		path: "expenses/bankaccounts/form/:id",
    pathMatch: 'full',
		component: ExpensesBankAccountFormComponent,
    resolve: {
      item: ExpensesBankAccountResolver
		}
	},

// list and add expense types ------------------------------------------------------------
	{
		path: "expenses/types",
    pathMatch: 'full',
		component: ExpensesTypeComponent
	},
	{
		path: "expenses/types/form/:id",
    pathMatch: 'full',
		component: ExpensesTypeFormComponent,
    resolve: {
      item: ExpensesTypeResolver
		}
	}

];

