export class LoginInfoModel {
  public loggedOn: boolean = false;

  constructor(data: any = null) {
    if (data) {
      this.loggedOn = data.loggedOn;
    }
  }
}
