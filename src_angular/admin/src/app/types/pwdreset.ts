export class PwdResetModel {
  public code: string;
  public password1: string;
  public password2: string;
}
