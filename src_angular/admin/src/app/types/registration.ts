export class RegistrationModel {
  public email: string;
  public password: string;
  public again: string;
  public icode: string;
  public token: string;
}
