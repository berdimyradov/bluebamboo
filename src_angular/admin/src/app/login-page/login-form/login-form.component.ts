import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { LoginModel } from '../../types';
import { UserService } from '../../user.service';
import { Router } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';
import { DesktopModule } from "../../desktop/desktop-page/types/desktop-module";

@Component({
  selector: 'admin-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginFormComponent implements OnInit {
  @Output() public flip: EventEmitter<string> = new EventEmitter<string>();
  public model: LoginModel = new LoginModel();
  public processing: boolean = false;
  public message: string = "";

  constructor(
    private userService: UserService,
    private router: Router,
    private localize: LocalizeRouterService
  ) { }

  ngOnInit() {
  /*
    let $: any;
    $('#login_form_email').trigger('focus');
  */
  }

  private setupUserLane(userId) {
  /*
    (<any>window).userlaneSettings = {
        app_id: 31859,
        user: {
          id: "" + userId
        }
    };
    let userlaneTag = document.createElement('script');
    userlaneTag.type = 'text/javascript';
    userlaneTag.src = '//cdn.userlane.com/userlane.js';
    document.body.appendChild(userlaneTag);
  */
  }

  public login() {
    this.processing = true;
    this.userService.login(this.model)
      .subscribe(
        result => {
         this.processing = false;
          if (result.isSuccess) {
            this.setupUserLane(1);
            this.router.navigate([ this.localize.translateRoute('/admin') ]);
          }
          else {
            this.message = JSON.parse(result.error).message;
          }
        }
      )
  }

  public registration() {
    console.log("emit register");
    this.flip.emit("register");
  }
  public reqpassword() {
    console.log("emit request_password_reset");
    this.flip.emit("request_password_reset");
  }
}
