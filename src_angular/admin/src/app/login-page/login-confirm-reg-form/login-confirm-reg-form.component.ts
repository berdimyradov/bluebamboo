import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ConfirmRegistrationModel } from '../../types';
import { UserService } from '../../user.service';
import { Router } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';

@Component({
  selector: 'admin-login-confirm-reg-form',
  templateUrl: './login-confirm-reg-form.component.html',
  styleUrls: ['./login-confirm-reg-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginConfirmRegistrationFormComponent implements OnInit {
  @Output() public flip: EventEmitter<string> = new EventEmitter<string>();
  public model: ConfirmRegistrationModel = new ConfirmRegistrationModel();
  public subpage: number = 0; // 0 = form, 1 = thank you page
  public processing: boolean = false;
  public message: string = "";

  constructor(
    private userService: UserService,
    private router: Router,
    private localize: LocalizeRouterService
  ) { }

  ngOnInit() {}

  public confirm() {
    this.processing = true;
    this.message = "";
    this.userService.confirmRegistration(this.model).subscribe(
      result => {
        if (result.isSuccess) {
          this.subpage = 1;
        } else {
          this.processing = false;
          let m = JSON.parse(result.error);
          this.message = m.message;
        }
      }
    );
  }

  public login() {
    this.flip.emit("login");
  }
}
