import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginConfirmRegistrationFormComponent } from './login-form.component';

describe('LoginConfirmRegistrationFormComponent', () => {
  let component: LoginConfirmRegistrationFormComponent;
  let fixture: ComponentFixture<LoginConfirmRegistrationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginConfirmRegistrationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginConfirmRegistrationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
