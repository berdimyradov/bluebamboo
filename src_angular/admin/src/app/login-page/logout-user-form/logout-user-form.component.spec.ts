import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoutUserFormComponent } from './logout-user-form.component';

describe('LogoutUserFormComponent', () => {
  let component: LogoutUserFormComponent;
  let fixture: ComponentFixture<LogoutUserFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoutUserFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutUserFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
