import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'admin-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginPageComponent implements OnInit {
  public view: string = "login";
  public flipped: boolean = false;
  public key: string = "";
  public token: string = "";

  constructor(
    private route: ActivatedRoute,
  ) {
    let view: string = this.route.snapshot.data['view'];
    if (view === "password_reset") {
      this.key = this.route.snapshot.params.key;
    }
    if (view === "invitation") {
      view = "register";
    }
    this.setMode(view);
  }

  ngOnInit() {
  }

  public setMode(view: string) {
    this.flipped = false;
    this.flipped = this.flipped || view == "auto_logout";
    this.flipped = this.flipped || view == "user_logout";
    this.flipped = this.flipped || view == "password_reset";
    this.flipped = this.flipped || view == "register";
    this.flipped = this.flipped || view == "request_password_reset";
    this.view = view;
  }
}
