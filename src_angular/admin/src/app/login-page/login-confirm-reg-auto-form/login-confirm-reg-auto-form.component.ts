import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ConfirmRegistrationModel } from '../../types';
import { UserService } from '../../user.service';
import { Router, ActivatedRoute, ActivatedRouteSnapshot, ParamMap } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';
import { DesktopModule } from "../../desktop/desktop-page/types/desktop-module";

@Component({
  selector: 'admin-login-confirm-reg-auto-form',
  templateUrl: './login-confirm-reg-auto-form.component.html',
  styleUrls: ['./login-confirm-reg-auto-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginConfirmRegistrationAutoFormComponent implements OnInit {
  @Output() public flip: EventEmitter<string> = new EventEmitter<string>();
  public model: ConfirmRegistrationModel = new ConfirmRegistrationModel();
  public subpage: number = 0; // 0 = form, 1 = thank you page
  public processing: boolean = false;
  public message: string = "";

  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private localize: LocalizeRouterService
  ) {

    this.model.code = this.route.snapshot.params.code;
    console.log("auto confirm: " + this.model.code);
    this.confirm();
  }

  ngOnInit() {
  }

  public confirm() {
    this.processing = true;
    this.message = "";
    this.userService.confirmRegistration(this.model).subscribe(
      result => {
        if (result.isSuccess) {
          this.subpage = 1;
        } else {
          this.processing = false;
          let m = JSON.parse(result.error);
          this.message = m.message;
        }
      }
    );
  }

  public login() {
    this.flip.emit("login");
  }
}
