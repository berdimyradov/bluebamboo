import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPwdResetFormComponent } from './login-form.component';

describe('LoginPwdResetFormComponent', () => {
  let component: LoginPwdResetFormComponent;
  let fixture: ComponentFixture<LoginPwdResetFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginPwdResetFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPwdResetFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
