import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { PwdResetModel } from '../../types';
import { UserService } from '../../user.service';
import { Router, ActivatedRoute, ActivatedRouteSnapshot, ParamMap } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';
import { DesktopModule } from "../../desktop/desktop-page/types/desktop-module";

@Component({
  selector: 'admin-login-pwdreset-form',
  templateUrl: './login-pwdreset-form.component.html',
  styleUrls: ['./login-pwdreset-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginPwdResetFormComponent implements OnInit {
  @Output() public flip: EventEmitter<string> = new EventEmitter<string>();
  public model: PwdResetModel = new PwdResetModel();
  public subpage: number = 0; // 0 = form, 1 = thank you page
  public processing: boolean = false;
  public message: string = "";

  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private localize: LocalizeRouterService
  ) {
    this.model.code = this.route.snapshot.params.key;
  }

  ngOnInit() {
  }

  public reset() {
    this.processing = true;
    this.message = "";
    this.userService.resetPassword(this.model).subscribe(
      result => {
        if (result.isSuccess) {
          this.subpage = 1;
        } else {
          this.processing = false;
          let m = JSON.parse(result.error);
          this.message = m.message;
        }
      }
    );
  }

  public login() {
    this.flip.emit("login");
  }
}
