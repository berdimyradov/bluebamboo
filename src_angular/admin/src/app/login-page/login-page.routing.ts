import { Routes } from "@angular/router";
import { CanActivateSecurity } from "../can-activate-security";
import { LoginPageComponent } from "./";

export const authenticationRoutes : Routes = [
  { path: 'admin/login', pathMatch: 'full', component: LoginPageComponent, canActivate: [CanActivateSecurity], data: {view: 'login'} },

  { path: 'admin/invitation/:token', pathMatch: 'full', component: LoginPageComponent, canActivate: [CanActivateSecurity], data: {view: 'invitation'} },

  { path: 'admin/user_logout', pathMatch: 'full', component: LoginPageComponent, canActivate: [CanActivateSecurity], data: {view: 'user_logout'} },
  { path: 'admin/auto_logout', pathMatch: 'full', component: LoginPageComponent, canActivate: [CanActivateSecurity], data: {view: 'auto_logout'} },
  { path: 'admin/pass_reset/:key', pathMatch: 'full', component: LoginPageComponent, canActivate: [CanActivateSecurity], data: {view: 'password_reset'} },
  { path: 'admin/confirm_registration_auto/:code', pathMatch: 'full', component: LoginPageComponent, canActivate: [CanActivateSecurity], data: {view: 'confirm_registration_auto'} },
  { path: 'admin/confirm_registration', pathMatch: 'full', component: LoginPageComponent, canActivate: [CanActivateSecurity], data: {view: 'confirm_registration'} }
];
