import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { PwdRequestModel } from '../../types';
import { UserService } from '../../user.service';
import { Router } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';
import { DesktopModule } from "../../desktop/desktop-page/types/desktop-module";

@Component({
  selector: 'admin-login-pwdrequest-form',
  templateUrl: './login-pwdrequest-form.component.html',
  styleUrls: ['./login-pwdrequest-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginPwdRequestFormComponent implements OnInit {
  @Output() public flip: EventEmitter<string> = new EventEmitter<string>();
  public model: PwdRequestModel = new PwdRequestModel();
  public subpage: number = 0; // 0 = form, 1 = thank you page
  public processing: boolean = false;
  public message: string = "";

  constructor(
    private userService: UserService,
    private router: Router,
    private localize: LocalizeRouterService
  ) { }

  ngOnInit() {
  }

  public request() {
    this.processing = true;
    this.message = "";
    this.userService.requestPassword(this.model).subscribe(
      result => {
        if (result.isSuccess) {
          this.subpage = 1;
        } else {
          this.processing = false;
          let m = JSON.parse(result.error);
          this.message = m.message;
        }
      }
    );
  }

  public registration() {
    this.flip.emit("register");
  }

  public login() {
    this.flip.emit("login");
  }
}
