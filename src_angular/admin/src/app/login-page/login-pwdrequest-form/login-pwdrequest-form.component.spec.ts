import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPwdRequestFormComponent } from './login-form.component';

describe('LoginPwdRequestFormComponent', () => {
  let component: LoginPwdRequestFormComponent;
  let fixture: ComponentFixture<LoginPwdRequestFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginPwdRequestFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPwdRequestFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
