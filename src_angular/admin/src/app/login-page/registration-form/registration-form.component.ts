import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { RegistrationModel } from '../../types';
import { UserService } from "../../user.service";
import { ActivatedRoute, ActivatedRouteSnapshot, ParamMap } from '@angular/router';

@Component({
  selector: 'admin-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss']
})
export class RegistrationFormComponent implements OnInit {
  @Output() public flip: EventEmitter<string> = new EventEmitter<string>();
  public model: RegistrationModel = new RegistrationModel();
  public subpage: number = 0; // 0 = form, 1 = thank you page
  public processing: boolean = false;
  public message: string = "";

  constructor(
    private route: ActivatedRoute,
    private user: UserService
  ) {
    let view: string = this.route.snapshot.data['view'];
    if (view === "invitation") {
      this.model.token = this.route.snapshot.params.token;
    }
  }

  ngOnInit() {}

  public register() {
    this.processing = true;
    this.message = "";
    this.user.register(this.model).subscribe(
      result => {
        if (result.isSuccess) {
          this.subpage = 1;
        } else {
          this.processing = false;
          let m = JSON.parse(result.error);
          this.message = m.message;
        }
      }
    );
  }

  public login() {
    this.flip.emit("login");
  }
}
