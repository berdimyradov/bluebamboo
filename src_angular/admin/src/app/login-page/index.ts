import { LoginPageComponent, LogoutAutoFormComponent, LogoutUserFormComponent, LoginPwdResetFormComponent, LoginPwdRequestFormComponent } from '.';
import { LoginFormComponent } from './login-form/login-form.component';
import { LoginInfoFormComponent } from './login-info-form/login-info-form.component';
import { LoginConfirmRegistrationFormComponent } from './login-confirm-reg-form/login-confirm-reg-form.component';
import { LoginConfirmRegistrationAutoFormComponent } from './login-confirm-reg-auto-form/login-confirm-reg-auto-form.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';

export * from './login-page.component';
export * from './logout-auto-form/logout-auto-form.component';
export * from './logout-user-form/logout-user-form.component';
export * from './login-pwdreset-form/login-pwdreset-form.component';
export * from './login-pwdrequest-form/login-pwdrequest-form.component';

export const loginPageDeclarations : any[] = [
  LoginFormComponent, LoginInfoFormComponent, LoginPageComponent,
  LogoutAutoFormComponent, LogoutUserFormComponent,
  LoginConfirmRegistrationAutoFormComponent, LoginConfirmRegistrationFormComponent,
  LoginPwdResetFormComponent, LoginPwdRequestFormComponent,
  RegistrationFormComponent
];

export const loginPageImports : any[] = [
];

export const loginPageProviders : any[] = [
];
