import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'admin-login-info-form',
  templateUrl: './login-info-form.component.html',
  styleUrls: ['./login-info-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginInfoFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
