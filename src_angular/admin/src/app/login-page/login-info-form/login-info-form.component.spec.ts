import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginInfoFormComponent } from './login-info-form.component';

describe('LoginInfoFormComponent', () => {
  let component: LoginInfoFormComponent;
  let fixture: ComponentFixture<LoginInfoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginInfoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginInfoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
