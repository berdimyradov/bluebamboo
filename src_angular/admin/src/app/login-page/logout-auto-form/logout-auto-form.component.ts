import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';
import { DesktopModule } from "../../desktop/desktop-page/types/desktop-module";

@Component({
  selector: 'admin-logout-auto-form',
  templateUrl: './logout-auto-form.component.html',
  styleUrls: ['./logout-auto-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LogoutAutoFormComponent implements OnInit {
  @Output() public flip: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    private router: Router,
    private localize: LocalizeRouterService
  ) { }

  ngOnInit() {
  }

  public login() {
    this.flip.emit("login");
  }
}
