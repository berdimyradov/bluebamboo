import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoutAutoFormComponent } from './logout-auto-form.component';

describe('LogoutAutoFormComponent', () => {
  let component: LogoutAutoFormComponent;
  let fixture: ComponentFixture<LogoutAutoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoutAutoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutAutoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
