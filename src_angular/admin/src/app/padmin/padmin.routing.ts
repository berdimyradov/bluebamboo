import { Routes } from "@angular/router";
import { PAdminPageComponent } from "./padmin-page/padmin-page.component";

import { SysMessageResolver }         from "./padmin-page/sysmessages/resolvers/sysmessage.resolver";
import { SysMessagesListComponent }   from "./padmin-page/sysmessages/sysmessages-list.component";
import { SysMessageFormComponent }    from "./padmin-page/sysmessages/sysmessage-form/sysmessage-form.component";

export const padminRoutes : Routes = [
  { path: 'padmin', pathMatch: 'full', component: PAdminPageComponent },

  { path: 'padmin/sysmessages', pathMatch: 'full', component: SysMessagesListComponent },
  {
    path: 'padmin/sysmessages/:id',
    pathMatch: 'full',
    component: SysMessageFormComponent,
    resolve: {
      obj: SysMessageResolver
    }
  }/*,
  { path: 'configuration/location', pathMatch: 'full', component: LocationConfigurationComponent },
  {
    path: 'configuration/location/:id',
    pathMatch: 'full',
    component: LocationFormComponent,
    resolve: {
      location: LocationResolver
    }
  },
  { path: 'configuration/insurance', pathMatch: 'full', component: InsuranceConfigurationComponent },
  {
    path: 'configuration/insurance/:id',
    pathMatch: 'full',
    component: InsuranceFormComponent,
    resolve: {
      insurance: InsuranceResolver
    }
  },
  {
    path: 'configuration/organization',
    pathMatch: 'full',
    component: OrganizationFormComponent,
    resolve: {
      organization: OrganizationResolver
    }
  },
  {
    path: 'configuration/report',
    component: ReportConfigurationComponent,
    children: [
      {
        path: 'base',
        pathMatch: "full",
        component: ReportBaseListComponent
      },
      {
        path: ':id',
        pathMatch: "full",
        component: ReportFormComponent,
        resolve: {
          report: ReportResolver
        }
      }
    ]
  },
  {
    path: 'configuration/service',
    component: ServiceConfigurationComponent
  },
  {
    path: 'configuration/service/:id',
    component: ProductFormComponent,
    resolve: {
      product: ProductResolver,
      positions: TariffPositionsResolver,
      organization: OrganizationResolver,
      rooms: RoomsResolver,
      employees: EmployeesResolver
    }
  },
  {
    path: 'configuration/employee',
    component: EmployeeConfigurationComponent
  },
  {
    path: 'configuration/employee/:id',
    component: EmployeeFormComponent,
    resolve: {
      employee: EmployeeResolver,
      locations: LocationListResolver,
      products: ProductListResolver
    }
  },
  {
    path: 'configuration/email-templates',
    pathMatch: 'full',
    component: EmailTemplatesComponent,
    resolve: {
      // emailTemplates: EmailTemplateResolver
    }
  },
  {
    path: 'configuration/hourly-rates',
    component: HourlyRatesConfigurationComponent,
    resolve: {
      insuranceList: InsuranceListResolver,
      tariffPositionsList: TariffPositionsResolver
    }
  }
  */
];
