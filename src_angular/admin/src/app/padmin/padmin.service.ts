import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { CommonMenuItem } from "../common";

@Injectable()
export class PAdminService {
  private _modules: CommonMenuItem[] = [
    new CommonMenuItem().parse({
      code: 'sysmessages',
      icon: 'fa-envelope-open-o',
      implement: true
    })
  ];

  constructor() { }

  public getListModules(): Observable<CommonMenuItem[]> {
    return (new Observable<CommonMenuItem[]>(observable => observable.next(this._modules)));
  }
}
