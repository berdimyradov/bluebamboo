export const locale = {
  "PAGE": {
    "PADMIN": {
      "TITLE": "Plattform Administration",
      "BACK": "Zurück",
      "ITEMS": {
        "ACTION": "Anzeigen",
        "organization": {
          "TITLE": "Organisation"
        },
        "location": {
          "TITLE": "Standorte"
        },
        "room": {
          "TITLE": "Räume"
        },
        "employee": {
          "TITLE": "Mitarbeiter"
        },
        "service": {
          "TITLE": "Leistungen"
        },
        "insurance": {
          "TITLE": "Versicherung"
        },
        "report": {
          "TITLE": "Berichte"
        },
        "hourly-rates": {
          "TITLE": "KK-Maximal- stundensätze"
        }
      }
    }
  }
};
