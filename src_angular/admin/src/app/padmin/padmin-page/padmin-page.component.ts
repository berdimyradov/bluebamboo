import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CommonMenuItem } from "../../common";
import { CommonPageComponent, routeAnimationOpacity } from "../../common/";

import { PAdminService } from "../padmin.service";

@Component({
  selector:       'admin-padmin-page',
  templateUrl:    './padmin-page.component.html',
  styleUrls: [    './padmin-page.component.scss'],
  encapsulation:  ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class PAdminPageComponent implements OnInit {
  public modules: CommonMenuItem[] = [];

  constructor(
    private padminService: PAdminService,
    private commonPage: CommonPageComponent
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.PADMIN.TITLE';
    this.padminService.getListModules()
      .subscribe(
        result => this.modules = result
      );
  }
}
