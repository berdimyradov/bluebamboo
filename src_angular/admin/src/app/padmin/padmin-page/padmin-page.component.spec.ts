import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PAdminPageComponent } from './padmin-page.component';

describe('PAdminPageComponent', () => {
  let component: PAdminPageComponent;
  let fixture: ComponentFixture<PAdminPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PAdminPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PAdminPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
