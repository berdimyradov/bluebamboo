import { Injectable }             from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable }             from "rxjs/Observable";

import { SysMessage }             from '../types';
import { SysMessagesService }     from "../services";

@Injectable()

export class SysMessageResolver implements Resolve<SysMessage> {
  constructor ( private service : SysMessagesService ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): SysMessage | Observable<SysMessage> | Promise<SysMessage> {
    const id: string = route.paramMap.get('id');

    if (id === 'add') {
      return Observable.of(new SysMessage());
    }

    return this.service.get(parseInt(id));
  }
}
