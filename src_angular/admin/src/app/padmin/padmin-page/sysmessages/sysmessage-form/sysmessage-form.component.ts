import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

import { routeAnimationOpacity, CommonPageComponent, DialogService } from "app/common";

import { CRUDFormComponent }    from 'app/framework';

import { SysMessagesService }   from '../services';
import { SysMessage }           from '../types';


@Component({
	selector:				'admin-sysmessage-form',
	templateUrl:		'./sysmessage-form.component.html',
	styleUrls:			['./sysmessage-form.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:						{'[@routeAnimationOpacity]': 'true'},
	animations:			[ routeAnimationOpacity ]
})

export class SysMessageFormComponent extends CRUDFormComponent<SysMessagesService, SysMessage> {
	public object: SysMessage;

	constructor(
		protected commonPage: CommonPageComponent,
		protected router: Router,
		protected activeRoute: ActivatedRoute,
		protected service: SysMessagesService,
		protected dialog: DialogService
	) { super( commonPage, router, activeRoute, service, dialog ); }

	protected getLabelKeys () {
		return {
			pageTitle:  'PAGE.SYSMESSAGE-EDIT.TITLE',
			delText:    'PAGE.REPORT-CONFIGURATION.CONFIRM-DELETE',
			delYes:     'PAGE.REPORT-CONFIGURATION.CONFIRM-YES',
			delNo:      'PAGE.REPORT-CONFIGURATION.CONFIRM-NO'
		};
	}

}
