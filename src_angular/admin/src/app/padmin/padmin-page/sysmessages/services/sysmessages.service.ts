import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { FrameworkService, GenericResultModel, CRUDService } from 'app/framework';

import { SysMessage } from '../types';

@Injectable()

export class SysMessagesService extends CRUDService<SysMessage> {
  constructor (http: Http) { super(http); }

	public get(id: number): Observable<SysMessage> {
		return this.http.get('/api/v1/platform/messages/' + id)
			.map((res: Response) => new SysMessage(res.json()))
			.catch(this.handleError);
	}

  public getList (): Observable<SysMessage[]> {
    return this.http.get(`/api/v1/platform/messages`)
      .map((res: Response) => res.json().map(item => new SysMessage(item)))
      .catch(this.handleError);
  }

  public save (obj: SysMessage): Observable<SysMessage> {
    const res: Observable<Response> = obj.id
      ? this.http.put('/api/v1/platform/messages/' + obj.id, obj)
      : this.http.post('/api/v1/platform/messages/', obj);

    return res.map((res: Response) => new SysMessage(res.json()))
              .catch(this.handleError);
  }

  public delete(id: number): Observable<void> {
    return this.http.delete('/api/v1/platform/messages/' + id.toString())
      .map(() => null)
      .catch(this.handleError);
  }

}
