import { TestBed, inject } from '@angular/core/testing';

import { SysMessagesService } from './sysmessages.service';

describe('SysMessagesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ SysMessagesService ]
    });
  });

  it('should be created', inject([ SysMessagesService ], (service: SysMessagesService) => {
    expect(service).toBeTruthy();
  }));
});
