import { CRUDObject } from 'app/framework';

export class SysMessage implements CRUDObject {
  public id:        number;
  public title:     string;
  public message:   string;
  public modal:     boolean;

  constructor(data: any = {}) {
    const names: string[] = ['id', 'title', 'message', 'modal'];
    names.forEach(name => this[name] = data[name] || this[name]);
  }
}
