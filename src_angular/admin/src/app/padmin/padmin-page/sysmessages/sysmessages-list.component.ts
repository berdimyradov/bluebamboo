import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { TranslateModule } from "@ngx-translate/core";

import { CommonPageComponent, routeAnimationOpacity, DialogService } from 'app/common';
import { BbGridComponent, ArrayDataProvider } from "@bluebamboo/common-ui";

import { CRUDGridComponent } from 'app/framework';

import { SysMessagesService } from './services';
import { SysMessage } from './types';

@Component({
	selector:				'admin-sysmessages-list',
	templateUrl:		'./sysmessages-list.component.html',
	styleUrls:			['./sysmessages-list.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:						{'[@routeAnimationOpacity]': 'true'},
	animations:			[routeAnimationOpacity]
})

export class SysMessagesListComponent extends CRUDGridComponent<SysMessagesService, SysMessage> {

	constructor(
		protected commonPage: CommonPageComponent,
		protected router: Router,
		protected activeRoute: ActivatedRoute,
		protected dialogService: DialogService,
		protected service: SysMessagesService
	) { super(commonPage, router, activeRoute, dialogService, service); }


	protected getLabelKeys () {
		return {
			pageTitle:  'PAGE.SYSMESSAGES-LIST.TITLE',
			delText:    'PAGE.REPORT-CONFIGURATION.CONFIRM-DELETE',
			delYes:     'PAGE.REPORT-CONFIGURATION.CONFIRM-YES',
			delNo:      'PAGE.REPORT-CONFIGURATION.CONFIRM-NO'
		};
	}

}
