import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SysMessagesListComponent } from './sysmessages-list.component';

describe('SysMessagesListComponent', () => {
  let component: SysMessagesListComponent;
  let fixture: ComponentFixture<SysMessagesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SysMessagesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SysMessagesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
