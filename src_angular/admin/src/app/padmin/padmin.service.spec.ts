import { TestBed, inject } from '@angular/core/testing';

import { PAdminService } from './padmin.service';

describe('PAdminService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ PAdminService ]
    });
  });

  it('should be created', inject([ PAdminService ], (service: PAdminService) => {
    expect(service).toBeTruthy();
  }));
});
