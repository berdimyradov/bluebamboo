import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { AdminCommonModule } from '../common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { CommonUiModule } from '@bluebamboo/common-ui';

import { PAdminPageComponent } from './padmin-page/padmin-page.component';
import { PAdminService } from './padmin.service';

import { SysMessageResolver } from './padmin-page/sysmessages/resolvers';
import { SysMessagesService } from './padmin-page/sysmessages/services';
import { SysMessagesListComponent } from './padmin-page/sysmessages/sysmessages-list.component';
import { SysMessageFormComponent } from './padmin-page/sysmessages/sysmessage-form/sysmessage-form.component';

import { locale as en } from './resources/en';
import { locale as de } from './resources/de';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    CommonModule,
    TranslateModule,
    CommonUiModule,
    AdminCommonModule
  ],
  declarations: [
    PAdminPageComponent,
    SysMessagesListComponent, SysMessageFormComponent
  ],
  providers: [
    PAdminService,
    SysMessagesService,
    SysMessageResolver
  ]
})

export class PAdminModule {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
  }
}
