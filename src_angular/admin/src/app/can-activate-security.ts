import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {UserService} from "./user.service";
import {LocalizeRouterService} from "localize-router";

@Injectable()
export class CanActivateSecurity implements CanActivate {
  constructor(private router: Router,
              private localize: LocalizeRouterService,
              private user: UserService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return new Promise<Boolean>((resolve) => {
      this.user.getLoginInfo()
        .subscribe(
          result => {
            if (result.loggedOn && route.routeConfig.path === 'admin/login') {
              this.router.navigate([this.localize.translateRoute('/admin')]);
              return resolve(false);
            }
            if (!result.loggedOn) {
              let publicPath = false;
              publicPath = publicPath || route.routeConfig.path === 'admin/login';
              publicPath = publicPath || route.routeConfig.path === 'admin/invitation/:token';
              publicPath = publicPath || route.routeConfig.path === 'admin/user_logout';
              publicPath = publicPath || route.routeConfig.path === 'admin/auto_logout';
              publicPath = publicPath || route.routeConfig.path === 'admin/pass_reset/:key';
              publicPath = publicPath || route.routeConfig.path === 'admin/confirm_registration';
              publicPath = publicPath || route.routeConfig.path === 'admin/confirm_registration_auto/:code';

              if (!publicPath) {
                this.router.navigate([this.localize.translateRoute('/admin/login')]);
                return resolve(false);
              }
            }
            resolve(true);
          }
        );
    });
  }
}
