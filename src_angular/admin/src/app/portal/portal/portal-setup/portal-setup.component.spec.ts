import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortalSetupComponent } from './portal-setup/portal-setup.component';

describe('PortalSetupComponent', () => {
  let component: PortalSetupComponent;
  let fixture: ComponentFixture<PortalSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortalSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortalSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
