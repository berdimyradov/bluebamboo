import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms';
import { routeAnimationOpacity, CommonPageComponent, CommonListItem, DialogService } from "../../../common";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { TranslateModule } from "@ngx-translate/core";
import { LocalStorageService } from 'angular-2-local-storage';

import { PortalService } from "../../services/portal.service";
import { TemplateSite } from '../../services/portal.types';

import { SelectOption } from '@bluebamboo/common-ui';


@Component({
	selector:		    'app-portal-setup',
	templateUrl:	  './portal-setup.component.html',
	styleUrls:		  ['./portal-setup.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			      {'[@routeAnimationOpacity]': 'true'},
	animations:		  [routeAnimationOpacity]
})

export class PortalSetupComponent implements OnInit {
  @ViewChild('bbForm') public form: NgForm;

	private templates: TemplateSite[] = [];
	public templateOptions: SelectOption[] = [];
	public templateId: number = -1;

	constructor(
    	private commonPage: CommonPageComponent,
    	private router: Router,
    	private activeRoute: ActivatedRoute,
    	private service: PortalService,
    	private localStorageService: LocalStorageService,
			private dialogService: DialogService
    )
	{}

	ngOnInit() {
		this.commonPage.title = 'Neuen Web Auftritt anlegen';
		this.refresh();
		/*
		const nameControl = this.form.get('name');
  nameControl.valueChanges.forEach(
    (value: string) => this.nameChangeLog.push(value)
  );
	*/
	}

	private updateTemplateSelectionDropDown () {
		this.templateOptions = [];
		this.templateId = -1;
				let o: SelectOption = new SelectOption();
				o.code = "" + -1;
				o.title = "TestTest";
				this.templateOptions.push(o);
		this.templates.forEach(template => {
				let o: SelectOption = new SelectOption();
				o.code = "" + template.id;
				o.title = template.title;
				this.templateOptions.push(o);
			}
		);
	}

  private refresh() {
    this.service.getTemplates().subscribe(
    	result => {
    		this.templates = result;
    		this.updateTemplateSelectionDropDown();
    	}
    );
  }
/*
  public selectSite(site:Site) {
    this.service.setActiveSite(site.id)
      .subscribe(
        result => {
          console.log("set active site result: " + result);
          this.localStorageService.set("gsc_cms_site_id", site.id);
          window.location.href = site.default_page;
        }
      );
  }

  public duplicateSite(site:Site) {
    this.service.duplicateSite(site.id)
      .subscribe(
        result => {
		      this.refresh();
        }
      );
  }

  public createSite () {
    let ids: number[] = [4441];
    this.service.setupSite(ids)
      .subscribe(
        result => {
          console.log("setup site: " + result);
		      this.refresh();
        }
      );
  }
*/
  public back() {
    this.router.navigate(['../myportal'], {relativeTo: this.activeRoute});
	}

}
