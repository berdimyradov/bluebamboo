import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortalSetupEmptyComponent } from './portal-setup/portal-setup-empty.component';

describe('PortalSetupEmptyComponent', () => {
  let component: PortalSetupEmptyComponent;
  let fixture: ComponentFixture<PortalSetupEmptyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortalSetupEmptyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortalSetupEmptyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
