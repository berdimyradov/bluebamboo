import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms';
import { routeAnimationOpacity, CommonPageComponent, CommonListItem, DialogService } from "../../../common";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { TranslateModule } from "@ngx-translate/core";
import { LocalStorageService } from 'angular-2-local-storage';

import { PortalService } from "../../services/portal.service";
import { TemplateSite, EmptySite } from '../../services/portal.types';

import { SelectOption } from '@bluebamboo/common-ui';


@Component({
	selector:		    'app-portal-setup-empty',
	templateUrl:	  './portal-setup-empty.component.html',
	styleUrls:		  ['./portal-setup-empty.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			      {'[@routeAnimationOpacity]': 'true'},
	animations:		  [routeAnimationOpacity]
})

export class PortalSetupEmptyComponent implements OnInit {
  @ViewChild('bbForm') public form: NgForm;
  public model: EmptySite = new EmptySite();

	constructor(
    	private commonPage: CommonPageComponent,
    	private router: Router,
    	private activeRoute: ActivatedRoute,
    	private service: PortalService,
    	private localStorageService: LocalStorageService,
			private dialogService: DialogService
    )
	{}

	ngOnInit() {
	  this.model.title = "Mein Web-Auftritt";
		this.commonPage.title = 'Neuen leeren Web Auftritt anlegen';
		/*
		this.refresh();
		const nameControl = this.form.get('name');
    nameControl.valueChanges.forEach(
    (value: string) => this.nameChangeLog.push(value)
  );
	*/
	}

  public createSite () {
    let ids: number[] = [4441];
    this.service.setupSite(ids)
      .subscribe(
        result => {
          console.log("setup site: " + result);
//		      this.refresh();
        }
      );
  }

  public createEmptySite () {
    this.service.setupEmptySite(this.model)
      .subscribe(
        result => {
          console.log("setup empty site: " + result);
//		      this.refresh();
        }
      );
  }

  public back() {
    this.router.navigate(['../myportal'], {relativeTo: this.activeRoute});
	}

}
