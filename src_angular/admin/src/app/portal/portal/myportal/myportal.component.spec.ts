import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPortalComponent } from './myportal/myportal.component';

describe('MyPortalComponent', () => {
  let component: MyPortalComponent;
  let fixture: ComponentFixture<MyPortalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPortalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPortalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
