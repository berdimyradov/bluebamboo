import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonListItem, DialogService } from "../../../common";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { TranslateModule } from "@ngx-translate/core";
import { LocalStorageService } from 'angular-2-local-storage';

import { BbGridComponent, ArrayDataProvider } from "@bluebamboo/common-ui";

import { PortalService } from "../../services/portal.service";
import { Site } from "../../../common";

@Component({
	selector:		    'app-myportal',
	templateUrl:	  './myportal.component.html',
	styleUrls:		  ['./myportal.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			      {'[@routeAnimationOpacity]': 'true'},
	animations:		  [routeAnimationOpacity]
})

export class MyPortalComponent implements OnInit {
  @ViewChild('grid') private grid: BbGridComponent;

  public list: Site[] = [];
  public hasSites: boolean = true;

  public gridModules: any [] = [TranslateModule];
  public templateAction: string = '<a class="btn-floating btn-sm white btn-command" (click)="row.edit($event)" data-toggle="tooltip" data-placement="top" title="{{\'PAGE.REPORT-CONFIGURATION.ACTIONS.EDIT\'|translate}}"><i class="fa fa-pencil"></i></a><a class="btn-floating btn-sm white btn-command" (click)="row.duplicate($event)" data-toggle="tooltip" data-placement="top" title="{{\'PAGE.REPORT-CONFIGURATION.ACTIONS.DUPLICATE\'|translate}}"><i class="fa fa-clone"></i></a><a class="btn-floating btn-sm white btn-command" (click)="row.delete($event)" data-toggle="tooltip" data-placement="top" title="{{\'PAGE.REPORT-CONFIGURATION.ACTIONS.DELETE\'|translate}}"><i class="fa fa-trash"></i></a>';
  public templateActive: string = '<mdb-checkbox [ngModel]="row.active" readonly="true"></mdb-checkbox>';

	constructor(
    	private commonPage: CommonPageComponent,
    	private router: Router,
    	private activeRoute: ActivatedRoute,
    	private service: PortalService,
    	private localStorageService: LocalStorageService,
			private dialogService: DialogService
    )
	{}

	ngOnInit() {
		this.commonPage.title = 'Web Auftritt(e)';
		this.refresh();
	}

  private refresh() {
    this.service.getSites().subscribe( result => {
      this.list = result.map(it => this.initRow(it));
      this.hasSites = this.list.length > 0;
    });
  }

  private initRow(row: any): any {
    row.edit = ($event) => {
      $event.stopPropagation();
      this.selectSite(row as Site);
    };
    row.duplicate = ($event) => {
      $event.stopPropagation();
      this.duplicateSite(row as Site);
    };
    row.delete = ($event) => {
      $event.stopPropagation();
      this.dialogService.confirm('PAGE.REPORT-CONFIGURATION.CONFIRM-DELETE','PAGE.REPORT-CONFIGURATION.CONFIRM-YES','PAGE.REPORT-CONFIGURATION.CONFIRM-NO')
        .then((res: Boolean) => {
          if (res) {
            //this.reportService.delete(row.id)
            //  .subscribe(() => this.refresh());
          }
        });

    };
    return row;
  }

  public selectSite(site:Site) {
    this.service.setActiveSite(site.id)
      .subscribe(
        result => {
          console.log("set active site result: " + result);
          this.localStorageService.set("gsc_cms_site_id", site.id);
          window.location.href = site.default_page;
        }
      );
  }

  public duplicateSite(site:Site) {
    this.service.duplicateSite(site.id)
      .subscribe(
        result => {
		      this.refresh();
        }
      );
  }

  public createEmptySite () {
    this.router.navigate(['../setup-empty'], {relativeTo: this.activeRoute});
  }

  public createSite () {

    this.router.navigate(['../setup'], {relativeTo: this.activeRoute});

    return;
/*
    let ids: number[] = [4441];
    this.service.setupSite(ids)
      .subscribe(
        result => {
          console.log("setup site: " + result);
		      this.refresh();
        }
      );
*/
  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
	}

}
