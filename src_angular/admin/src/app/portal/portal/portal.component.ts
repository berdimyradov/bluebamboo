import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonMenuItem } from "../../common";
import { PortalService } from "../services/portal.service";

@Component({
	selector:		'app-portal',
	templateUrl:	'./portal.component.html',
	styleUrls:		['./portal.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			{'[@routeAnimationOpacity]': 'true'},
	animations:		[routeAnimationOpacity]
})

export class PortalComponent implements OnInit {

	public modules: CommonMenuItem[] = [

			new CommonMenuItem().parse({
				code: 'myportal',
				icon: 'fa-globe',
				implement: true
			})
	];

	constructor(private service: PortalService, private commonPage: CommonPageComponent) {}

	ngOnInit() {
		this.commonPage.title = 'Web Auftritt';
		/*
		this.service.getListModules().subscribe(
			result => this.modules = result
		);
		*/
	}
}
