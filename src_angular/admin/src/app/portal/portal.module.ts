import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CommonUiModule } from '@bluebamboo/common-ui';
import { AdminCommonModule } from '../common';

import { LocalStorageModule } from 'angular-2-local-storage';

import { locale as en } from './resources/en';
import { locale as de } from './resources/de';

import { PortalService } from './services/portal.service';

import { PortalComponent } from './portal/portal.component';
import { MyPortalComponent } from './portal/myportal/myportal.component';
import { PortalSetupComponent } from './portal/portal-setup/portal-setup.component';
import { PortalSetupEmptyComponent } from './portal/portal-setup-empty/portal-setup-empty.component';

@NgModule({
  imports: [
    CommonModule, CommonUiModule, AdminCommonModule, FormsModule, BrowserAnimationsModule, TranslateModule, RouterModule,
    LocalStorageModule.withConfig({
      prefix: 'ls',
      storageType: 'localStorage'
    })
  ],
  declarations: [PortalComponent, MyPortalComponent, PortalSetupComponent, PortalSetupEmptyComponent],
  providers: [PortalService]
})

export class PortalModule {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
  }
 }
