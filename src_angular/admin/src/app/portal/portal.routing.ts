import { Routes } from "@angular/router";

import { PortalComponent } from './portal/portal.component';
import { MyPortalComponent } from './portal/myportal/myportal.component';
import { PortalSetupComponent } from './portal/portal-setup/portal-setup.component';
import { PortalSetupEmptyComponent } from './portal/portal-setup-empty/portal-setup-empty.component';

export const portalRoutes : Routes = [
	{
		path: "portal",
		component: PortalComponent,
	},
	{
		path: "portal/myportal",
		component: MyPortalComponent,
	},
	{
		path: "portal/setup",
		component: PortalSetupComponent,
	},
	{
		path: "portal/setup-empty",
		component: PortalSetupEmptyComponent,
	},
];

