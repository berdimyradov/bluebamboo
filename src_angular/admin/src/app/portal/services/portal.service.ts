import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, RequestOptions} from "@angular/http";
import { Observable } from "rxjs/Observable";

import { Site } from "../../common";

import { TemplateSite, EmptySite } from './portal.types';

@Injectable()
export class PortalService {

  constructor(private http: Http) { }

  public getSites(fields = ['id', 'name', 'description', 'domains', 'default_page']): Observable<Site[]> {
    let requestOptions: RequestOptions = new RequestOptions();
    requestOptions.params = new URLSearchParams();
    requestOptions.params.set('_fields', fields.join(','));
    return this.http.get('/api/v1/cms/sites', requestOptions)
      .map((res: Response) => res.json().map((it: any) => new Site().parse(it)))
      .catch(this.handleError);
  }

  public setActiveSite (id: number): Observable<string> {
    return this.http.put('/api/v1/cms/sites/active/' + id, {})
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public duplicateSite (id: number): Observable<string> {
    return this.http.post('/api/v1/cms/sites/duplicate/' + id, {})
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public setupSite (ids: number[]): Observable<string> {
    return this.http.put('/api/v1/cms/sitesetup', ids)
      .map((res: Response) => res.json())
      .catch(this.handleError);

  }

  public setupEmptySite (data: EmptySite): Observable<string> {
    return this.http.put('/api/v1/cms/sitesetupempty', data)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public getTemplates(): Observable<TemplateSite[]> {
    return this.http.get('/api/v1/cms/templatecategories/select')
      .map((res: Response) => res.json().map((it: any) => new TemplateSite().parse(it)))
      .catch(this.handleError);
  }


  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
