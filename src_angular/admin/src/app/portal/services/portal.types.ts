export class EmptySite {
  public title: string = "";
  public desc: string = "";
  public url: string = "";

  public parse(data: any): EmptySite {
    const names: string[] = ['title', 'desc', 'url'];
    names.forEach((name) => this[name] = data[name]);
    return this;
  }
}

export class TemplatePage {
  public id: number;
  public title: string;
  public url: string;

  public parse(data: any): TemplatePage {
    const names: string[] = ['id', 'title', 'url'];
    names.forEach((name) => this[name] = data[name]);
    return this;
  }
}

export class TemplateCategory {
  public id: number;
  public title: string;
  public pages: TemplatePage[] = [];

  public parse(data: any): TemplateCategory {
    const names: string[] = ['id', 'title'];
    names.forEach((name) => this[name] = data[name]);
    if (data.pages) {
      data.pages.forEach((page) => this.pages.push(new TemplatePage().parse(page)));
    }
    return this;
  }
}

export class TemplateSite {
  public id: number;
  public title: string;
  public categories: TemplateCategory[] = [];

  public parse(data: any): TemplateSite {
    const names: string[] = ['id', 'title'];
    names.forEach((name) => this[name] = data[name]);
    if (data.categories) {
      data.categories.forEach((category) => this.categories.push(new TemplateCategory().parse(category)));
    }
    return this;
  }
}
