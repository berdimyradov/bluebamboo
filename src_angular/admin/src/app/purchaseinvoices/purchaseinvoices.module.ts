import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CommonUiModule } from '@bluebamboo/common-ui';
import { AdminCommonModule } from '../common';

import { locale as en } from './resources/en';
import { locale as de } from './resources/de';

import { PurchaseInvoicesService } from './services/purchaseinvoices.service';

import { PurchaseInvoicesComponent } from './purchaseinvoices/purchaseinvoices.component';

@NgModule({
  imports: [CommonModule, CommonUiModule, AdminCommonModule, FormsModule, BrowserAnimationsModule, TranslateModule, RouterModule],
  declarations: [
    PurchaseInvoicesComponent
  ],
  providers: [PurchaseInvoicesService]
})

export class PurchaseInvoicesModule {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
  }
}
