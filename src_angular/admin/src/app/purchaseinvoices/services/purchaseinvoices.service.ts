import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions} from "@angular/http";
import { Observable } from "rxjs/Observable";

import { FrameworkService, GenericResultModel } from 'app/framework/';

import { Room } from "app/common";

@Injectable()

export class PurchaseInvoicesService extends FrameworkService {

  constructor (http: Http) { super(http); }

  // -------------------------------------------------------------------------------------
  // rooms

  public getRooms(fields = ['id', 'title', 'description']): Observable<Room[]> {
    let requestOptions: RequestOptions = new RequestOptions();
    requestOptions.params = new URLSearchParams();
    requestOptions.params.set('_fields', fields.join(','));
    return this.http.get('/api/v1/loc/rooms', requestOptions)
      .map((res: Response) => res.json().map((it: any) => new Room().parse(it)))
      .catch(this.handleError);
  }

	public getRoom(id: number): Observable<Room> {
		return this.http.get('/api/v1/loc/rooms/' + id)
			.map((res: Response) => new Room().parse(res.json()))
			.catch(this.handleError);
	}

  public saveRoom(obj: Room): Observable<Room> {
    //(room.products || []).forEach((p: any) => {p.room_id = room.id;});
    const res: Observable<Response> = obj.id
      ? this.http.put('/api/v1/loc/rooms/' + obj.id, obj)
      : this.http.post('/api/v1/loc/rooms/', obj);
    return res.map((res: Response) => new Room().parse(res.json()))
      .catch(this.handleError);
  }

  public deleteRoom(obj: Room): Observable<void> {
    return this.http.delete('/api/v1/loc/rooms/' + obj.id)
      .map(() => {return;})
      .catch(this.handleError);
  }

}
