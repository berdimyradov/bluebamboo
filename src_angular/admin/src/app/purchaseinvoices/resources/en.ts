export const locale = {
    "PAGE": {
        "Expenses": {
            "ITEMS": {
                "myprofile": {
                    "TITLE": "Mein Profil"
                },
                "myrooms": {
                    "TITLE": "Meine R\u00e4ume"
                },
                "mypassword": {
                    "TITLE": ""
                },
                "mymodules": {
                    "TITLE": ""
                },
                "myinvoices": {
                    "TITLE": ""
                }
            }
        }
    }
}
