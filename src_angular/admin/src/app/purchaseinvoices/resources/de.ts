export const locale = {
    "COMPONENT": {
      "PURCHASEINVOICES": {
        "COMMON": {
          "ORDER": "Verbindlich bestellen",
          "LABEL_AGB": "AGB <a target='_blank' href='https://bluebamboo.blob.core.windows.net/public/bluebamboo-agb-v1-0-1.pdf'>(lesen)</a>",
          "LABEL_LICENCE": "Nutzungsbestimmungen  <a target='_blank' href='https://bluebamboo.blob.core.windows.net/public/bluebamboo-nutzungsbedingungen-v1-0-1.pdf'>(lesen)</a>",
          "READ_AGB": "den Allgemeinen Geschäftsbedingungen zustimmen",
          "READ_LICENCE": "den Nutzungsbestimmungen zustimmen",
          "PAYMENT_SCHEME": "Zahlungsweise",
          "PAYMENT_SCHEME_HF": "Haljährlich (105.- CHF)",
          "PAYMENT_SCHEME_FF": "Jährlich (199.- CHF)"
        }
      }
    },
    "PAGE": {
        "PURCHASEINVOICES": {
            "DESKTOP": {
              "PAGE_TITLE": "Erhaltene Rechnungen",
              "ITEMS": {
                  "password": {
                      "TITLE": "Passwort ändern"
                  },
                  "modules": {
                      "TITLE": "Meine Module"
                  },
                  "invoices": {
                      "TITLE": "Meine Rechnungen und Gutschriften"
                  }
              }
            },
            "PASSWORD": {
              "TITLE": "Ändern Sie hier Ihr Passwort"
            },
            "MODULES" : {
              "PAGE_TITLE": "Ihre Module"

            },
            "MODULE-ADMIN" : {
              "PAGE_TITLE": "Modul 'Administration'"

            },
            "MODULE-WEB" : {
              "PAGE_TITLE": "Modul 'Webseite'"

            }
        }
    }
}
