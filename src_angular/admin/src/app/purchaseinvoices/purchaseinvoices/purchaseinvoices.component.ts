import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonMenuItem } from "../../common";
import { PurchaseInvoicesService } from "../services/purchaseinvoices.service";

@Component({
	selector:		    'app-purchaseinvoices',
	templateUrl:	  './purchaseinvoices.component.html',
	styleUrls:		  ['./purchaseinvoices.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			      {'[@routeAnimationOpacity]': 'true'},
	animations:		  [routeAnimationOpacity]
})

export class PurchaseInvoicesComponent implements OnInit {

	public modules: CommonMenuItem[] = [

			new CommonMenuItem().parse({
				code: 'add',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'list',
				icon: 'fa-key',
				implement: true
			}),
	];

	constructor(
	  private service: PurchaseInvoicesService,
	  private commonPage: CommonPageComponent
	)
	  {}

	ngOnInit() {
		this.commonPage.title = 'PAGE.PURCHASEINVOICES.DESKTOP.PAGE_TITLE';
		/*
		this.service.getListModules().subscribe(
			result => this.modules = result
		);
		*/
	}
}
