import { Routes } from "@angular/router";

import { PurchaseInvoicesComponent } from './purchaseinvoices/purchaseinvoices.component';

export const purchaseinvoicesRoutes : Routes = [
	{
		path: "purchaseinvoices",
		component: PurchaseInvoicesComponent,
	}
];

