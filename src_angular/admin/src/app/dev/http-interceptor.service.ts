import { Injectable } from '@angular/core';
import { ConnectionBackend, RequestOptions, Request, RequestOptionsArgs, Response, Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class HttpInterceptorService extends Http {
  private urlTransform: Function;
  private devMode: Boolean;
  private skipUrl(url) {
    const tmp = url.split('.');
    if (tmp[tmp.length - 1] === 'json') {
      console.debug(`skip url:` + url);
      return true;
    }
    return false;
  }

  constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
    super(backend, defaultOptions);
    const port = document.location.port.toString();
    if (port === '4200') {
      this.devMode = true;
      const prefix = document.location.protocol + '//' + document.location.hostname;
      console.warn('client run on development mode');
      this.urlTransform = (url: string): string => {
        if (this.skipUrl(url)) {
          return url;
        }
        return  url;
      }
    } else {
      this.devMode = false;
      this.urlTransform = (url: string): string => url;
    }
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    if (typeof url === 'string') {
      url = this.urlTransform(url);
    } else {
      url.url = this.urlTransform(url.url);
      if (this.devMode) {
        url.withCredentials = true;
      }
    }
    return super.request(url, options);
  }
}
