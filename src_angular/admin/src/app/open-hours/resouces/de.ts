export const locale = {
  "PAGE": {
    'DESKTOP': {
      'SYSTEM-LINKS': {
        'BTN_SUPPORT': "Support / Hilfe",
        'BTN_PDF_MANUAL': "PDF-Anleitung",
        'BTN_PDF_PRICING': "Preise und Module",
        'BTN_VIDEOS': "Erklär-Videos",
        'BTN_FEEDBACK': "Ihr Feedback bringt uns weiter",
        'BTN_RECOMMEND': "Weiterempfehlen & Geld sparen",
        'BTN_MY_MODULES': "Meine Module",
        'MESSAGES': {
          'TITLE': "Mitteilungen anzeigen"
        },
        'INVITE': {
          'TITLE': "Weiterempfehlen"
        },
        'SUPPORT': {
          'TITLE': "Support anfordern"
        },
        'FEEDBACK': {
          'FEEDBACK-MESSAGE': 'Feedback',
          'RATING-MESSAGE': '* Rate Us',
          'TITLE': 'Feedback title'
        }
      },
      'SUPPORT': {
        'TITLE': "Support anfordern",
        'NAME': 'Name *',
        'E-MAIL': 'E-Mail *',
        'PHONE-NUMBER': 'Telefonnummer *',
        'ADDITIONAL-INFO': 'Zusatzinformationen (z.B. Wunschzeit für Rückruf etc.)',
        'COMMENT': 'Kommentar *',
        'LEVEL_URGENT': 'Dringend',
        'LEVEL_NORMAL': 'Normal',
        'LEVEL_NONURGENT': 'Nicht dringend'
      },
      'SUPPORT_THANKS': {
        'TITLE': "Support angefordert",
        'HEADER': 'Danke',
        'MESSAGE': 'Danke für Ihre Support-Anfrage, wir bearbeiten diese so schnell wie möglich.',
        'BACK': 'Zurück',
      },
      'SYSTEM-MESSAGES': {
        'TRIAL-PERIOD': {
          'DAYS': "Sie haben noch {{days}} Tage für Ihren kostenfreien Test zur Verfügung.",
          'TEXT': "Diese Tage werden Ihnen gutgeschrieben, falls Sie sich heute definitv anmelden.",
          'PAY': "zur definitiven anmeldung"
        },
        'MESSAGE': {
          'SKIP': "später lesen",
          'READ': "als gelesen markieren",
          'CLOSE': "schliessen"
        }
      },
      "MESSAGES": {
        "TITLE": "Mitteilungen anzeigen",
        "NO_MESSAGES": "Sie haben keine Mitteilungen",
        "NO_UNREAD_MESSAGES": "Sie haben keine ungelesenen Mitteilungen",
        "BACK": "Zurück"
      }
    },
    "FEEDBACK": {
      "TITLE": "Feedback",
      "FEEDBACK-MESSAGE": "Wir sind dankbar für konstruktiven Feedback, Wünsche, Anregungen und einfach auch dann, wenn es Ihnen gut gefällt.<br>Herzlichen Dank!",
      "RATING-MESSAGE": "Wie bewerten Sie unsere Lösung?",
      "THANKS-MESSAGE": {
        "PAGE_TITLE": "Danke!",
        "TITLE_5": "5/Danke für Ihr Feedback!",
        "TITLE_4": "4/Danke für Ihr Feedback!",
        "TITLE_3": "3/Danke für Ihr Feedback!",
        "TITLE_2": "2/Danke für Ihr Feedback!",
        "TITLE_1": "1/Danke für Ihr Feedback!",
        "TEXT_5": "5/Wir hoffen, dass Ihnen die bluebamboo Lösung gute Dienste leistet und danken Ihnen für Ihre Rückmeldung!.",
        "TEXT_4": "4/Wir hoffen, dass Ihnen die bluebamboo Lösung gute Dienste leistet und danken Ihnen für Ihre Rückmeldung!.",
        "TEXT_3": "3/Wir hoffen, dass Ihnen die bluebamboo Lösung gute Dienste leistet und danken Ihnen für Ihre Rückmeldung!.",
        "TEXT_2": "2/Wir hoffen, dass Ihnen die bluebamboo Lösung gute Dienste leistet und danken Ihnen für Ihre Rückmeldung!.",
        "TEXT_1": "1/Wir hoffen, dass Ihnen die bluebamboo Lösung gute Dienste leistet und danken Ihnen für Ihre Rückmeldung!.",
        "BTN_RECOMMEND": "Zur Weiterempfehlungs-Seite"
      }
    },
    "INVITE": {
      "PAGE_TITLE": "Weiterempfehlung",
      "INFO": "<h3>Win-Win: wir sparen, Sie profitieren!</h3><p>Persönliche Empfehlungen sind die beste Werbung.<br>Wir senken damit unsere Aufwände um KundInnen zu finden und können Ihnen damit mehr für weniger Kosten bieten und Sie bekommen regelmässig eine Vergünstigung.<br>Detaillierte Informationen finden Sie hier: <a target='_blank' href='https://www.bluebamboo.ch/de/weiterempfehlung'>Detailinformationen zum Win-Win-Weiterempfehlungs-Programm von bluebamboo.</a></p>",
      "SENDER": "Ihre E-Mail Adresse",
      "NAME": "Name und Vorname",
      "SUBJECT": "Betreffzeile des Einladungs-E-Mails",
      "SUBJECT_DEFAULT": "Einladung zum Testen der Lösung von bluebamboo",
      "EMAIL": "E-Mail Adresse der Empfängerin, des Empfängers",
      "EMAIL_DEFAULT": "vorname.nachname@domain.ch",
      "MESSAGE": "Mitteilung an die Empfängerin, den Empfänger",
      "MESSAGE_DEFAULT": "Liebe Name,\n\nich verwende die Lösung von bluebamboo für meine 590er Rechnungen.\nSchau Dir die Lösung doch auch an, der Test ist während 30 Tagen unverbindlich und kostenfrei.\n\nHerzliche Grüsse,\n\nName",
      "THANKS-MESSAGE": {
        "PAGE_TITLE": "Danke!",
        "TITLE": "Danke herzlich für Ihre Weiterempfehlung!",
        "TEXT": "<p>Sobald die eingeladene Person Kundin, Kunde bei bluebamboo wird, bekommen Sie regelmässig eine entsprechende Gutschrift.<br>Detaillierte Informationen finden Sie hier: <a target='_blank' href='https://www.bluebamboo.ch/de/weiterempfehlung'>Detailinformationen zum Win-Win-Weiterempfehlungs-Programm von bluebamboo.</a></p><p>Herzlichst Ihr bluebamboo Team",
        "BNT_NEW_INVITE": "Nächste Person einladen."
      }
    }
  }
};
