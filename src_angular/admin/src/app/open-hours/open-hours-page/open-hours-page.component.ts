import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CommonMenuItem } from '../../common/common-menu-page/types/common-menu-item';
import { CommonPageComponent, routeAnimationOpacity } from '../../common/';
import { OpenHoursService } from '../services/open-hours.service';

@Component({
  selector: 'app-open-hours-page',
  templateUrl: 'open-hours-page.component.html',
  encapsulation: ViewEncapsulation.None,
  host: { '[@routeAnimationOpacity]': 'true'},
  animations: [
    routeAnimationOpacity
  ]
})

export class OpenHoursPageComponent implements OnInit {
  public modules: CommonMenuItem[] = [];

  constructor(
    private commonPage: CommonPageComponent,
    private openHours: OpenHoursService
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.BOOKING.TITLE';
    this.openHours.getListModules()
      .subscribe(
        result => this.modules = result
      );
  }
}
