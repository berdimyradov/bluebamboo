import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { AdminCommonModule } from '../common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { CommonUiModule } from '@bluebamboo/common-ui';

import { locale as en } from './resouces/en';
import { locale as de } from './resouces/de';

import { OpenHoursPageComponent } from 'app/open-hours/open-hours-page/open-hours-page.component';
import { OpenHoursService } from './services/open-hours.service';


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    CommonModule,
    TranslateModule,
    CommonUiModule,
    AdminCommonModule
  ],
  declarations: [
    OpenHoursPageComponent
  ],
  exports: [
    OpenHoursPageComponent
  ],
  providers: [
    OpenHoursService
  ]
})
export class OpenHoursModule {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
  }
}
