import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CommonMenuItem } from '../../common/common-menu-page/types/common-menu-item';

@Injectable()
export class OpenHoursService {
  private _modules: CommonMenuItem[] = [
    new CommonMenuItem().parse({
      code: 'open_hours',
      icon: 'fa-clock-o',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'employee',
      icon: 'fa-user',
      implement: true
    })
  ];

  public getListModules(): Observable<CommonMenuItem[]> {
    return (new Observable<CommonMenuItem[]>(observable => observable.next(this._modules)));
  }
}
