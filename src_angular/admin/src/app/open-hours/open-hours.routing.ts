import { Routes } from '@angular/router';
import { OpenHoursPageComponent } from 'app/open-hours/open-hours-page/open-hours-page.component';

export const openHoursRoutes: Routes = [
  {
    path: 'openHours',
    pathMatch: 'full',
    component: OpenHoursPageComponent
  },
  {
    path: 'openHours/open_hours',
    pathMatch: 'full',
    redirectTo: '/admin/booking/open_hours'
  },
  {
    path: 'openHours/employee',
    pathMatch: 'full',
    redirectTo: '/admin/booking/employee'
  },
];
