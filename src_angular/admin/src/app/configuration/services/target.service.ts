import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from "rxjs/Observable";
import {TargetRow} from "../models/target";
import {UtilityService} from "../../common/services/utility/utility.service";

@Injectable()
export class TargetService {

  constructor(private http: Http) {
  }

  public getAllTargetRows(): Observable<TargetRow[]> {
    return this.http.get('/api/v1/data/targets/all')
      .map((res: Response) => (res.json() as any[]).map((it: any) => (new TargetRow()).parse(it)))
      .catch(UtilityService.handleError);
  }

  public getTargetRow(id): Observable<TargetRow> {
    return this.http.get(`/api/v1/data/targets/${id}`)
      .map((res: Response) => Object.assign(new TargetRow, res.json()))
      .catch(UtilityService.handleError);
  }

}