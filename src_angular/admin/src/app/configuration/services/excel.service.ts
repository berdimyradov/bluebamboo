import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Http, Response} from "@angular/http";
import {InvoiceExcelRow} from "../../invoice/invoice-page/types/invoice-excel-row";
import {UtilityService} from "../../common/services/utility/utility.service";

@Injectable()
export class ExcelService  {
  constructor(private http: Http) { }

  public getGridData(): Observable<InvoiceExcelRow[]> {
    return this.http.get('/api/v1/pay/excel/all')
      .map((res: Response) => (res.json() as any[]).map((it:any) => (new InvoiceExcelRow()).parse(it)) )
      .catch(UtilityService.handleError);
  }
}
