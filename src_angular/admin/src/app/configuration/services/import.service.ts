import {Injectable} from '@angular/core';
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {ImportRow} from "../../invoice/invoice-page/types/import-row";
import {UtilityService} from "../../common/services/utility/utility.service";
import {RecordRow} from "../models/record";
import {Customer} from "../../common/types";

@Injectable()
export class ImportService {

  constructor(private http: Http) {
  }

  public getGridData(): Observable<ImportRow[]> {
    return this.http.get('/api/v1/data/imports/all')
      .map((res: Response) => (res.json() as any[]).map((it: any) => (new ImportRow()).parse(it)))
      .catch(UtilityService.handleError);
  }

  public getImportRow(id): Observable<ImportRow> {
    return this.http.get(`/api/v1/data/imports/${id}`)
      .map((res: Response) => Object.assign(new ImportRow(), res.json()))
      .catch(UtilityService.handleError);
  }

  public getRecordsByImportId(id): Observable<ImportRow> {
    return this.http.get(`/api/v1/data/imports/${id}/records`)
      .map((res: Response) => Object.assign(new ImportRow(), res.json()))
      .catch(UtilityService.handleError);
  }

  public saveImport(importRow: ImportRow): Observable<any> {
    return this.http.put(`/api/v1/data/imports/${importRow.id}`, importRow)
      .map((res: Response) => Object.assign(new ImportRow(), res.json()))
      .catch(UtilityService.handleError);
  }

  public importRecordsToCustomer(data: any): Observable<Array<Customer>> {
    return this.http.post(`/api/v1/data/records/import`, data)
      .map((res: Response) => (res.json() as any[]).map((it: any) => (new Customer()).parse(it)))
      .catch(UtilityService.handleError)
  }

}
