import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {UtilityService} from "../../common/services/utility/utility.service";
import {RecordRow} from "../models/record";

@Injectable()
export class RecordService {

  constructor(private http: Http) { }

  // @todo FIX or Remove maybe
  // public getRecordsByImportId(id): Observable<RecordRow> {
  //   return this.http.get(`/api/v1/data/records/${id}`)
  //     .map((res: Response) => Object.assign(new RecordRow(), res.json()))
  //     .catch(UtilityService.handleError);
  // }

}
