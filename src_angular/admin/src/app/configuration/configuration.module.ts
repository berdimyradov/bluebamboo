import {EmailTemplatesComponent} from './configuration-page/email-templates/email-templates.component';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {AdminCommonModule} from '../common';
import {ConfigurationPageComponent} from './configuration-page/configuration-page.component';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {CommonUiModule} from '@bluebamboo/common-ui';
import {ConfigurationService} from './configuration.service';
import {
  LocationConfigurationService, InsuranceConfigurationService, RoomConfigurationService, ProductConfigurationService,
  OrganizationConfigurationService, CustomerConfigurationService, EmailTemplateConfigurationService
} from './configuration-page/services/';
import {
  RoomResolver,
  LocationResolver,
  InsuranceResolver,
  LocationListResolver,
  ProductListResolver,
  OrganizationResolver,
  InsuranceListResolver, CustomerResolver, CaseResolver, CaseTypeListResolver, CaseLawListResolver,
  CaseDiagnosisTypeResolver, CaseTherapyTypeListResolver, CasePaymentTypeListResolver, CaseReferralTypeResolver,
  DossierResolver, BookingReportResolver, BookingResolver, EmailTemplateListResolver
} from './configuration-page/resolvers/';
import {RoomConfigurationComponent} from './configuration-page/room-configuration/room-configuration.component';
import {RoomFormComponent} from './configuration-page/room-configuration/room-form/room-form.component';
import {LocationConfigurationComponent} from './configuration-page/location-configuration/location-configuration.component';
import {InsuranceConfigurationComponent} from './configuration-page/insurance-configuartion/insurance-configuration.component';
import {InsuranceFormComponent} from './configuration-page/insurance-configuartion/insurance-form/insurance-form.component';
import {LocationFormComponent} from './configuration-page/location-configuration/location-form/location-form.component';
import {OrganizationFormComponent} from './configuration-page/organization-form/organization-form.component';
import {ReportConfigurationComponent} from './configuration-page/report-configuration/report-configuration.component';
import {ReportConfigurationService} from './configuration-page/services';
import {ReportTemplateVarsService} from './configuration-page/services/report-template-vars.service';
import {ReportFormComponent} from './configuration-page/report-configuration/report-form/report-form.component';
import {ReportResolver} from './configuration-page/resolvers/report-resolver';
import {RouterModule} from '@angular/router';
import {SharedModule} from 'app/shared/shared.module';
import {ReportBorderStyleDirective} from './configuration-page/report-configuration/report-form/page/element/directives/report-borderstyle';
import {ReportDesignerElementComponent} from './configuration-page/report-configuration/report-form/page/element/report-designer-element.component';
import {ReportDesignerPageComponent} from './configuration-page/report-configuration/report-form/page/report-designer-page.component';
import {ReportDesignerTextAlignEditorComponent} from './configuration-page/report-configuration/report-form/property-editor/text-align/report-designer-text-align-editor.component';
import {ReportDesignerFontSizeEditorComponent} from './configuration-page/report-configuration/report-form/property-editor/font-size/report-designer-font-size-editor.component';
import {ReportDesignerFontListEditorComponent} from './configuration-page/report-configuration/report-form/property-editor/font-list/report-designer-font-list-editor.component';
import {ReportDesignerGridActionEditorComponent} from './configuration-page/report-configuration/report-form/property-editor/grid-action/report-designer-grid-action-editor.compontent';
import {ReportDesignerGridSectionEditorComponent} from './configuration-page/report-configuration/report-form/property-editor/grid/section/report-designer-grid-section-editor.component';
import {ReportDesignerGridEditorComponent} from './configuration-page/report-configuration/report-form/property-editor/grid/report-designer-grid-editor';
import {ReportDesignerCommonEditorComponent} from './configuration-page/report-configuration/report-form/property-editor/common/report-designer-common-editor.component';
import {ReportDesignerLocationEditorComponent} from './configuration-page/report-configuration/report-form/property-editor/location/report-designer-location-editor.component';
import {ReportDesignerBorderStyleEditorComponent} from './configuration-page/report-configuration/report-form/property-editor/border-style/report-designer-border-style-editor.component';
import {ReportDesignerFontStyleEditorComponent} from './configuration-page/report-configuration/report-form/property-editor/font-style/report-designer-font-style-editor';
import {ReportDesignerPropertyEditorComponent} from './configuration-page/report-configuration/report-form/property-editor/report-designer-property-editor.component';
import {ReportFontStyleDirective} from './configuration-page/report-configuration/report-form/page/element/directives/report-fontstyle';
import {ReportBaseListComponent} from './configuration-page/report-configuration/report-base-list/report-base-list.component';
import {locale as en} from './resources/en';
import {locale as de} from './resources/de';
import {ServiceConfigurationComponent} from './configuration-page/service-configuration/service-configuration.component';
import {ServiceConfigurationService} from './configuration-page/services/service-configuration.service';
import {ProductFormComponent} from './configuration-page/service-configuration/product-form/product-form.component';
import {ProductResolver} from './configuration-page/resolvers/product-resolver';
import {TariffPositionsResolver} from './configuration-page/resolvers/tariff-positions-resolver';
import {RoomsResolver} from './configuration-page/resolvers/rooms-resolver';
import {EmployeeConfigurationComponent} from './configuration-page/employee-configuration/employee-configuration.component';
import {EmployeeFormComponent} from './configuration-page/employee-configuration/employee-form/employee-form.component';
import {ValidatorsModule} from 'ngx-validators';
import {FroalaEditorModule, FroalaViewModule} from 'angular-froala-wysiwyg';
import {HourlyRatesConfigurationComponent} from './configuration-page/hourly-rates-configuration/hourly-rates-configuration.component';
import {ImportDataConfigurationComponent} from './configuration-page/import-data-configuration/import-data-configuration.component';
import {ImportFilePageComponent} from './configuration-page/import-data-configuration/import-file-page/import-file-page.component';
import {ExcelService} from './services/excel.service';
import {ImportService} from './services/import.service';
import {MappingPageComponent} from './configuration-page/import-data-configuration/mapping-page/mapping-page.component';
import {EditPageComponent} from './configuration-page/import-data-configuration/edit-page/edit-page.component';
import {TargetService} from './services/target.service';
import {RecordService} from './services/record.service';
import {CustomerConfigurationComponent} from './configuration-page/customer-configuration/customer-configuration.component';
import {CustomerFormComponent} from './configuration-page/customer-configuration/customer-form/customer-form.component';
import {CaseFormComponent} from './configuration-page/customer-configuration/case-form/case-form.component';
import {DossierFormComponent} from './configuration-page/customer-configuration/dossier-form/dossier-form.component';
import {BookingReportFormComponent} from './configuration-page/customer-configuration/booking-report-form/booking-report-form.component';
import {BookingReportCaseFormComponent} from './configuration-page/customer-configuration/booking-report-case-form/booking-report-case-form.component';
import {BookingCardComponent} from './configuration-page/customer-configuration/booking-card/booking-card.component';
import {BookingCasesComponent} from './configuration-page/customer-configuration/booking-cases/booking-cases.component';

@NgModule({
  imports: [
    BrowserModule, BrowserAnimationsModule, FormsModule, CommonModule, TranslateModule,
    CommonUiModule, AdminCommonModule, RouterModule, ValidatorsModule, SharedModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
  ],
  declarations: [ConfigurationPageComponent, RoomConfigurationComponent,
    RoomFormComponent, LocationConfigurationComponent, InsuranceConfigurationComponent, InsuranceFormComponent,
    LocationFormComponent, OrganizationFormComponent, ReportConfigurationComponent, ReportFormComponent,

    ReportDesignerPageComponent, ReportDesignerElementComponent,
    ReportFontStyleDirective, ReportBorderStyleDirective, ReportDesignerPropertyEditorComponent,
    ReportDesignerFontStyleEditorComponent, ReportDesignerBorderStyleEditorComponent, ReportDesignerLocationEditorComponent,
    ReportDesignerCommonEditorComponent, ReportDesignerGridEditorComponent, ReportDesignerGridSectionEditorComponent,
    ReportDesignerGridActionEditorComponent, ReportDesignerFontListEditorComponent, ReportDesignerFontSizeEditorComponent,
    ReportDesignerTextAlignEditorComponent, ReportBaseListComponent, ServiceConfigurationComponent,
    ProductFormComponent, EmployeeConfigurationComponent, EmployeeFormComponent, EmailTemplatesComponent,
    HourlyRatesConfigurationComponent, CustomerConfigurationComponent, CustomerFormComponent, CaseFormComponent,
    DossierFormComponent, BookingReportFormComponent, BookingReportCaseFormComponent, BookingCardComponent, BookingCasesComponent,
    ImportDataConfigurationComponent, ImportFilePageComponent, MappingPageComponent, EditPageComponent
  ],
  exports: [
    CustomerFormComponent, ProductFormComponent, EmployeeConfigurationComponent, EmployeeFormComponent, EmailTemplatesComponent,
    HourlyRatesConfigurationComponent, ImportDataConfigurationComponent, ImportFilePageComponent, MappingPageComponent, EditPageComponent
  ],
  providers: [ConfigurationService, RoomConfigurationService, RoomResolver, LocationConfigurationService,
    InsuranceConfigurationService, InsuranceResolver, LocationResolver, ProductConfigurationService, ProductListResolver,
    LocationListResolver, OrganizationResolver, OrganizationConfigurationService, ReportConfigurationService,
    ReportResolver, ReportTemplateVarsService, ServiceConfigurationService, ProductResolver, TariffPositionsResolver, RoomsResolver,
    InsuranceListResolver, CustomerConfigurationService, CustomerResolver, CaseResolver, CaseTypeListResolver,
    CasePaymentTypeListResolver, CaseTherapyTypeListResolver, CaseDiagnosisTypeResolver, CaseLawListResolver,
    CaseReferralTypeResolver, DossierResolver, BookingReportResolver, BookingResolver, BookingReportResolver,
    ReportResolver, ServiceConfigurationService, ProductResolver, TariffPositionsResolver, RoomsResolver,
    InsuranceListResolver, EmailTemplateConfigurationService, EmailTemplateListResolver,
    ExcelService, ImportService, TargetService, RecordService
  ]
})

export class ConfigurationModule {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
  }
}
