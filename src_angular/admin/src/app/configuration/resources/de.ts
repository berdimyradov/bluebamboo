export const locale = {
  "PAGE": {
    "CONFIGURATION": {
      "TITLE": "Konfiguration",
      "BACK": "Zurück",
      "ITEMS": {
        "ACTION": "Anzeigen",
        "organization": {
          "TITLE": "Organisation"
        },
        "location": {
          "TITLE": "Standorte"
        },
        "room": {
          "TITLE": "Räume"
        },
        "employee": {
          "TITLE": "Mitarbeiter"
        },
        "service": {
          "TITLE": "Leistungen"
        },
        "insurance": {
          "TITLE": "Versicherung"
        },
        "report": {
          "TITLE": "PDF-Vorlagen"
        },
        "hourly-rates": {
          "TITLE": "KK-Maximal- stundensätze"
        },
        "customer": {
          "TITLE": "Kunden"
        },
        "email-templates": {
          "TITLE": "E-mail vorlagen"
        },
        "import-data": {
          "TITLE": "Import data"
        }
      }
    },
    "ROOM-CONFIGURATION": {
      "TITLE": "Räume",
      "BACK": "Zurück"
    },
    "ROOM-FORM": {
      "TITLE": {
        "EDIT": "Raum bearbeiten",
        "NEW": "Raum hinzufügen",
        "VIEW": "Raum"
      },
      "BACK": "Zurück",
      "SAVE": "Speichern",
      "DELETE": "Löschen",
      "FORM": {
        "NAME": "Name",
        "SERVICE": "Leistungen, die in dem Raum erbracht werden können",
        "DESCRIPTION": "Beschreibung",
        "AREA": "Grundfläche (in m²)",
        "LOCATION": "Standort",
        "LOCATION-EMPTY": "Noch kein Standort gewählt"
      },
      "CONFIRM-DELETE": "Den Raum wirklich löschen?",
      "CONFIRM-NO": "Abbrechen",
      "CONFIRM-YES": "Löschen"
    },
    "LOCATION-CONFIGURATION": {
      "TITLE": "Standorte"
    },
    "LOCATION-FORM": {
      "TITLE": {
        "EDIT": "Standort bearbeiten",
        "NEW": "Standort hinzufügen",
        "VIEW": "Standort"
      },
      "FORM": {
        "NAME": "Name",
        "DESCRIPTION": "Beschreibung",
        "STREET": "Straße & Nr.",
        "ZIP": "PLZ",
        "CITY": "Ort",
        "STATE": "Kanton",
        "COUNTRY": "Land",
        "EMAIL": "E-Mail",
        "PHONE": "Telefon",
        "ADDRESS": "Adresse",
        "LOCATION": "Standort",
        "TAKE-ORG-BUTTON": "Adressdaten der Organisation übernehmen"
	  },
	  "CONFIRM-DELETE": "Den Standort wirklich löschen?",
	  "CONFIRM-NO": "Abbrechen",
	  "CONFIRM-YES": "Löschen"
    },
    "INSURANCE-CONFIGURATION": {
      "TITLE": "Krankenkassen"
    },
    "INSURANCE-FORM": {
      "TITLE": {
        "EDIT": "Krankenkassendaten bearbeiten",
        "NEW": "Krankenkasse hinzufügen",
        "VIEW": "Krankenkasse"
      },
      "CONFIRM-DELETE": "Die Krankenkasse wirklich löschen?",
      "CONFIRM-NO": "Abbrechen",
      "CONFIRM-YES": "Löschen",
      "FORM": {
        "NAME": "Name"
      }
    },
    "FAQ": {
      "TITLE": "FAQ",
      "BACK": "Zurück"
    },
    "ORGANIZATION-FORM": {
      "TITLE": "Organisation",
      "FORM": {
        "NAME": "Name",
        "ADDRESS_LINE": "Zweite Adresszeile",
        "STREET": "Strasse & Nr.",
        "ZIP": "Postleitzahl",
        "CITY": "Ort",
        "STATE": "Kanton",
        "COUNTRY": "Land",
        "PHONE": "Telefon",
        "EMAIL": "E-Mail",
        "BANKACCOUNT": {
          "IBAN": "IBAN",
          "BIC": "BIC",
          "BANK": "Bank",
          "OWNER": "Kontoinhaber"
        },
        "MWST": "MWST-Pflichtig?",
        "MWST_NR": "MWST Nr.",
        "MWST-ALERT": "Achtung! Bitte prüfen Sie, ob Sie für alle Teilleistungen ihrer Leistungen die passenden MwSt-Sätze definiert haben und passen Sie diese ggf. an!",
        "PAYMENT": {
          "TERM": "Zahlungsfrist für Rechnungen (in Tagen)",
          "REMINDER_FIRST": "Zahlungsfrist erste Mahnung (in Tagen)",
          "REMINDER_SECOND": "Zahlungsfrist zweite Mahnung (in Tagen)"
        },
        "COST": {
          "REMINDER_FIRST": "Kosten erste Mahnung (in CHF)",
          "REMINDER_SECOND": "Kosten zweite Mahnung (in CHF)"
        }
      }
    },
    "REPORT-CONFIGURATION": {
      "TITLE": "PDF-Vorlagen",
      "BACK": "Zurück",
      "ADD": "Hinzufügen",
      "SEARCH": "Suchen",
      "CONFIRM-DELETE": "Diese Vorlage wirklich löschen?",
      "CONFIRM-NO": "Abbrechen",
      "CONFIRM-YES": "Löschen",
      "GRID": {
        "NAME": "Name",
        "GROUP": "Gruppe",
        "ROLE": "Rolle",
        "DESCRIPTION": "Beschreibung",
        "ACTION": "Aktion",
        "ACTIVE": "Aktiv"
      },
      "FORM": {
        "APPLY": "Speichern",
        "CANCEL": "Abbrechen",
        "ACTIVE": "Aktiv?"
      },
      "ACTIONS": {
        "EDIT": "Bearbeiten",
        "SETTINGS": "Einstellungen",
        "DUPLICATE": "Duplizieren",
        "DELETE": "Löschen"
      },
      "NAME": "Name",
      "STATE": "Aktiv",
      "TITLE_SELECT_ROLE": "Rolle"
    },
    "REPORT-FORM": {
      "TITLE": "Vorlagendesigner",
      "BACK": "Zurück",
      "SAVE": "Speichern",
      "PDF": "Preview-PDF",
      "ADD_ELEMENT": "Element hinzufügen",
      "NO_EDIT_ON_MOBILE": "Bearbeitung von PDF-Vorlagen ist auf Smartphones und Tablets nicht möglich. Bitte nutzen Sie einen Desktop-PC.",
      "COMMAND": {
        "STRING": "Zeichenkette",
        "RECT": "Rechteck",
        "HL": "Horizontale Linie",
        "DATAMATRIX": "Datamatrix-Code",
        "IMAGE": "Bild"
      },
      "PROPERTIES": {
        "LOC_TITLE": "Position",
        "COMMON_TITLE": "Allgemein",
        "BORDER-STYLE": "Rahmenstil",
        "BORDER-WIDTH": "Breite",
        "COLOR": "Farbe",
        "BGCOLOR": "Hintergrundfarbe",
        "WIDTH": "Breite",
        "URL": "URL",
        "CONTENT": "Inhalt",
        "COLSPAN": "Verbundene Spalten",
        "FONT-STYLE": "Zeichensatz Stil",
        "LINE-HEIGHT": "Zeilenhöhe",
        "LETTER-SPACING": "Buchstabenabstand",
        "BOLD": "Fett",
        "X": "X",
        "Y": "Y",
        "KEEP_RATIO":"Proportionen beibehalten",
        "HEIGHT": "Höhe",
        "LENGTH": "Länge",
        "THICKNESS": "Dicke",
        "GOTO_PROP": "Gehe zu Raster Eigenschaft",
        "GRID_ADD_COLUMN_LEFT": "Spalte linkerhand einfügen",
        "GRID_ADD_COLUMN_RIGHT": "Spalte rechterhand einfügen",
        "GRID_REMOVE_COLUMN": "Spalte entfernen",
        "GRID_REMOVE_DATA_COLUMN":  "Spalte entfernen",
        "GRID_COLUMN_COUNT": "Anzahl Spalten",
        "GRID_COMMON": "Raster allgemein",
        "GRID_HEADER": "Raster Kopfbereich",
        "GRID_FOOTER": "Raster Fussbereich",
        "VAR_GROUP_SELECT": "Variablen Gruppe:",
        "VAR_PROP_SELECT": "Variable",
        "VAR_PROP_ADD": "Zum Text hinzufügen",
        "VAR_PROP_DESC_HEADER": "Beschreibung:"
      },
      "FORM": {
        "TITLE": "PDF-Vorlagen"
      }
    },
    "REPORT-BASE-LIST": {
      "TITLE": "Vorlage wählen",
      "ACTION": "Auswählen",
      "BACK": "Zurück"
    },
    "PRODUCT-FORM": {
      "TITLE":  {
        "EDIT": "Leistung bearbeiten",
        "NEW": "Leistung hinzufügen",
        "VIEW": "Leistung"
      },
      "FORM": {
        "NAME": "Titel der Leistung",
        "BREAK_MIN": "Pause nach Behandlung (in min.)",
        "BREAK": "Pause nach Behandlung",
        "PARTS": {
          "FORM-TITLE": "Teilleistung {{pos}}",
          "TITLE": "Titel",
          "TARIFF": "Tarif",
          "TARIFF-POSITION": "Tarifposition",
          "DURATION":  "Dauer (in min.)",
          "PRICE": "Preis (in CHF)",
          "PRICE-5MIN": "{{price}} CHF pro 5 min.",
          "MWST": "MwSt",
          "SELECT_MWST": "MwSt wählen",
          "MIN": "min.",
          "SERVICE-TITLE": "Teilleistungen"
        },
        "ACTIVE": "Leistung aktiv?",
        "BOOKING": "Leistung verfügbar für Online Booking?",
        "ROOM": "Räume, in denen die Leistung erbracht werden kann",
        "EMPLOYEE": "Mitarbeiter, die die Leistung erbringen können",
        "DESCRIPTION": "Beschreibung",
        "ADD-PART": "Teilleistung hinzufügen",
		    "COLOR": "Kalenderfarbe",
        "DURATION": "Dauer",
        "MIN": "min.",
        "PRICE": "Preis",
        "CHF": "CHF",
        "IS_ACTIVE": {
          "TITLE": "Aktive Leistung?",
          "YES": "Die Leistung ist aktiv.",
          "NO": "Die Leistung ist aktuell inaktiv."
        },
        "IS_AVAILABLE": {
          "TITLE": "Verfügbar für Online Booking?",
          "YES": "Ja",
          "NO": "Nein"
        },
        "NO_ROOMS": "Diese Leistung kann in keinem Raum erbracht werden!",
        "NO_EMPLOYEES": "Diese Leistung kann von keinem Mitarbeiter erbracht werden!"
      },
      "CONFIRM-DELETE": "Die Leistung wirklich löschen?",
      "CONFIRM-NO": "Abbrechen",
      "CONFIRM-YES": "Löschen"
    },
    "PRODUCT-LIST": {
      "TITLE": "Leistungen"
    },
    "EMPLOYEE-LIST": {
      "TITLE": "Mitarbeiter"
    },
    "EMPLOYEE-CONFIGURATION": {
      "TITLE": "Mitarbeiter",
      "ITEMS": {
        "add": {
          "TITLE": "Mitarbeiter erfassen"
        },
        "list": {
          "TITLE": "Mitarbeiter anzeigen"
        }
      }
    },
    "EMPLOYEE-FORM": {
      "TITLE":  {
        "EDIT": "Mitarbeiter bearbeiten",
        "NEW": "Mitarbeiter hinzufügen",
        "VIEW": "Mitarbeiter"
      },
      "FORM": {
        "ADDRESS": "Adresse",
        "SALUTATION": "Anrede",
        "NAME": "Nachname",
        "FIRST-NAME": "Vorname",
        "GENDER": "Geschlecht",
        "GENDER-MAN": "männlich",
        "GENDER-WOMAN": "weiblich",
        "BIRTH": "Geburtstag",
        "BIRTH_DAY": "Tag",
        "BIRTH_MONTH": "Monat",
        "BIRTH_YEAR": "Jahr",
        "ADDRESS-LINE": "Zweite Adresszeile",
        "STREET": "Strasse & Nr.",
        "ZIP": "Postleitzahl",
        "CITY": "Ort",
        "STATE": "Kanton",
        "COUNTRY": "Land",
        "PHONE": {
          "PRIVATE": "Telefon Privat",
          "MOBILE": "Telefon Mobil",
          "WORK": "Telefon Arbeit"
        },
        "EMAIL": {
          "PRIMARY": "Primär",
          "PRIVATE": "E-Mail privat",
          "SECOND": "E-Mail geschäftlich"
        },
        "AHV": "AHV-Nr.",
        "ZSR": "ZSR-Nr.",
        "EGK": "EGK-Nr.",
        "LOCATION": "Standorte, an denen der Mitarbeiter arbeitet",
        "PRODUCT": "Leistungen, die der Mitarbeiter erbringen kann",
        "EXPERIENCE": "Ausbildungen",
        "SPECIALTY": "Spezialgebiete",
        "CAREER": "Werdegang",
        "ACTIVE": "Mitarbeiter ist aktiv.",
        "HASTARIFF590": "Mitarbeiter verrechnet nach Tarif 590",
        "MIN": "min.",
        "CHF": "CHF",
        "PRODUCTS": "Der Mitarbeiter kann folgende Leistungen erbringen",
        "NO_PRODUCTS": "Keine Leistungen ausgewählt!",
        "LOCATIONS": "Der Mitarbeiter arbeitet an folgenden Standorten",
        "NO_LOCATIONS": "Keine Standorte ausgewählt!",
        "IS_ACTIVE": {
          "TITLE": "Ist der Mitarbeiter aktiv?",
          "YES": "Ja",
          "NO": "Nein"
        },
        "IS_TARIFF590": {
          "TITLE": "Verrechnet nach Tarif 590?",
          "YES": "Ja",
          "NO": "Nein"
        }
      },
      "CONFIRM-DELETE": "Den Mitarbeiter wirklich löschen?",
      "CONFIRM-NO": "Abbrechen",
      "CONFIRM-YES": "Löschen"
    },
    "SERVICE-CONFIGURATION": {
      "TITLE": "Leistungen",
      "ITEMS": {
        "add": {
          "TITLE": "Leistungen hinzufügen"
        },
        "list": {
          "TITLE": "Leistungen anzeigen"
        }
      }
    },
    "HOURLY-RATES-CONFIGURATION": {
      "TITLE": "KK-Maximalstundensätze",
      "FORM": {
        "INSURANCE": "Krankenkasse",
        "ENABLE": "Angepasste maximale Stundensätze nutzen",
        "SAME-RATE": "Benutze denselben maximalen Stundensatz für alle Tarifpositionen",
        "ADD-PART": "Weiteren Stundensatz hinzufügen",
        "PARTS": {
          "TARIFF-POSITION": "Tarifposition",
          "RATE": "Maximaler Stundensatz",
          "INCLUDE-MWST": "Inclusive MWST"
        }
      }
    },
    "CUSTOMER-CONFIGURATION": {
      "TITLE": "Kunden",
      "BACK": "Zurück",
      "NAME": "Name",
      "FIRST-NAME": "Vorname",
      "CITY": "Ort",
      "SEARCH": "Suchen",
      "ADD": "Neu anlegen"
    },
    "CUSTOMER-FORM": {
      "TITLE": {
        "VIEW": "Kunde",
        "EDIT": "Kunden bearbeiten",
        "NEW": "Kunden erfassen"
      },
      "FORM": {
        "ADDRESS": "Adresse",
        "NAME": "Nachname",
        "FIRST-NAME": "Vorname",
        "GENDER": "Geschlecht",
        "GENDER-MAN": "männlich",
        "GENDER-WOMAN": "weiblich",
        "BIRTH": "Geburtstag",
        "BIRTH_DAY": "Tag",
        "BIRTH_MONTH": "Monat",
        "BIRTH_YEAR": "Jahr",
        "ADDRESS-LINE": "Zweite Adresszeile",
        "STREET": "Strasse & Nr.",
        "ZIP": "Postleitzahl",
        "CITY": "Ort",
        "STATE": "Kanton",
        "COUNTRY": "Land",
        "INSURANCE": "Krankenkasse",
        "KNOWN-FROM": "Bekannt durch",
        "COMMENT": "Bemerkungen",
        "GUARDIAN": "Vormund",
        "CAREGIVER": "Bezugsperson / Betreuer",
        "DOSSIER": "Klientendossier",
        "ADD-CASE": "Fall eröffnen",
        "PHONE": {
          "PRIVATE": "Telefon Privat",
          "MOBILE": "Telefon Mobil",
          "WORK": "Telefon Arbeit"
        },
        "EMAIL": {
          "PRIMARY": "Primär",
          "PRIVATE": "E-Mail privat",
          "SECOND": "E-Mail geschäftlich"
        },
        "SALUTATION": {
          "TITLE": "Anrede",
          "DU": "Du",
          "SIE": "Sie",
          "QUESTION": "Anrede per Du?"
        }
      },
      "CONFIRM-DELETE": "Den Kunden wirklich löschen?",
      "CONFIRM-NO": "Abbrechen",
      "CONFIRM-YES": "Löschen"
    },
    "CASE-FORM": {
      "FORM": {
        "TITLE": "Falltitel *",
        "TYPE": "Behandlungsgrund",
        "ACCIDENT-DATE": "Unfalldatum",
        "PAYMENT-TYPE": "Vergütungsart",
        "LAW": "Gesetz",
        "THERAPY-TYPE": "Therapie",
        "DIAGNOSIS-TYPE": "Diagnose Textformat",
        "DIAGNOSIS-TEXT": "Diagnose",
        "COMMENT": "Notizen",
        "COMMENT-INVOICE": "Rechnungsbemerkung",
        "IS-OPEN": "Fall offen?",
        "IS-CURRENT": "Als aktuellen Fall setzen?",
        "BOOKINGS": "Behandlungen",
        "NO-BOOKINGS": "Noch keine Behandlungen für diesen Fall vorhanden.",
        "REFERRAL": {
          "TITLE": "Zuweiser",
          "TYPE": "Art",
          "NAME": "Name",
          "ZSR": "ZSR-Nr.",
          "GLN": "GLN-Nr."
        }
      }
    },
    "DOSSIER-FORM": {
      "TITLE": "Klientendossier",
      "FORM": {
        "MEDICAL-HISTORY": "Medizinische Vorgeschichte",
        "MEDICINES": "Medikamente",
        "ALLERGIES": "Allergien",
        "CASES": "Fälle",
        "NO-CASES": "Noch keine Fälle angelegt.",
        "ADD-CASE": "Neuen Fall eröffnen",
        "BOOKINGS": "Unzugeordnete Behandlungen",
        "NO-BOOKINGS": "Alle Behandlungen sind Fällen zugewiesen."
      }
    },
    "BOOKING-REPORT-FORM": {
      "TITLE": {
        "VIEW": "Bericht",
        "NEW": "Bericht schreiben",
        "EDIT": "Bericht bearbeiten"
      },
      "FORM": {
        "TREATMENT": "Behandlung",
        "TEXT": "Behandlungsbericht",
        "COMMENT": "Interner Kommentar",
        "CASE": "Fall"
      }
    },
    "BOOKING-REPORT-CASE-FORM": {
      "TITLE": {
        "NEW": "Bericht schreiben",
        "EDIT": "Bericht bearbeiten"
      },
      "FORM": {
        "TREATMENT": "Behandlung",
        "TEXT": "Behandlungsbericht",
        "COMMENT": "Interner Kommentar",
        "CASE": "Fall",
        "CASE-CHOOSE": "Fall wählen",
      }
    },
    "BOOKING-CARD": {
      "TITLE": "Behandlung",
      "ROOM": "Raum",
      "ADDRESS": "Standort",
      "EMPLOYEE": "Mitarbeiter",
      "DATE": "Datum",
      "START-TIME": "Zeit"
    },
    "BOOKING-CASES": {
      "TITLE": "Fälle"
    },
    "EMAIL-TEMPLATES-CONFIGURATION": {
      "TITLE": "E-mail vorlagen",
      "SAVE": "Speichern",
      "BACK": "Zurück",
      "RESET": "Zurücksetzen",
      "TEMPLATE-SELECTOR": "Vorlagen",
      'SUBJECT': 'Subject'
    }
  }
};
