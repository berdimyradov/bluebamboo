import {Routes} from '@angular/router';
import {ConfigurationPageComponent} from './configuration-page/configuration-page.component';
import {RoomConfigurationComponent} from './configuration-page/room-configuration/room-configuration.component';
import {RoomFormComponent} from './configuration-page/room-configuration/room-form/room-form.component';
import {RoomResolver} from './configuration-page/resolvers';
import {LocationConfigurationComponent} from './configuration-page/location-configuration/location-configuration.component';
import {InsuranceConfigurationComponent} from './configuration-page/insurance-configuartion/insurance-configuration.component';
import {InsuranceResolver} from './configuration-page/resolvers';
import {InsuranceFormComponent} from './configuration-page/insurance-configuartion/insurance-form/insurance-form.component';
import {LocationFormComponent} from './configuration-page/location-configuration/location-form/location-form.component';
import {LocationResolver} from './configuration-page/resolvers';
import {ProductListResolver} from './configuration-page/resolvers';
import {LocationListResolver} from './configuration-page/resolvers';
import {OrganizationFormComponent} from './configuration-page/organization-form/organization-form.component';
import {OrganizationResolver} from './configuration-page/resolvers';
import {ReportConfigurationComponent} from './configuration-page/report-configuration/report-configuration.component';
import {ReportFormComponent} from './configuration-page/report-configuration/report-form/report-form.component';
import {ReportResolver} from './configuration-page/resolvers/report-resolver';
import {ReportBaseListComponent} from './configuration-page/report-configuration/report-base-list/report-base-list.component';
import {ServiceConfigurationComponent} from './configuration-page/service-configuration/service-configuration.component';
import {ProductResolver} from './configuration-page/resolvers/product-resolver';
import {ProductFormComponent} from './configuration-page/service-configuration/product-form/product-form.component';
import {TariffPositionsResolver} from './configuration-page/resolvers/tariff-positions-resolver';
import {RoomsResolver} from './configuration-page/resolvers/rooms-resolver';
import {EmployeesResolver, EmployeeResolver, ActiveEmployeesResolver} from '../common';
import {EmployeeConfigurationComponent} from './configuration-page/employee-configuration/employee-configuration.component';
import {EmployeeFormComponent} from './configuration-page/employee-configuration/employee-form/employee-form.component';
import { EmailTemplatesComponent } from './configuration-page/email-templates/email-templates.component';
import { CustomerConfigurationComponent } from './configuration-page/customer-configuration/customer-configuration.component';
import { CustomerFormComponent } from './configuration-page/customer-configuration/customer-form/customer-form.component';
import { CustomerResolver } from './configuration-page/resolvers';
import { CaseResolver } from './configuration-page/resolvers';
import { CaseFormComponent } from './configuration-page/customer-configuration/case-form/case-form.component';
import { CaseTypeListResolver } from './configuration-page/resolvers';
import { CaseLawListResolver } from './configuration-page/resolvers';
import { CasePaymentTypeListResolver } from './configuration-page/resolvers';
import { CaseTherapyTypeListResolver } from './configuration-page/resolvers';
import { CaseDiagnosisTypeResolver } from './configuration-page/resolvers';
import { CaseReferralTypeResolver } from './configuration-page/resolvers';
import { DossierResolver } from './configuration-page/resolvers';
import { DossierFormComponent } from './configuration-page/customer-configuration/dossier-form/dossier-form.component';
import { BookingReportFormComponent } from './configuration-page/customer-configuration/booking-report-form/booking-report-form.component';
import { BookingReportResolver } from './configuration-page/resolvers';
import { BookingResolver } from './configuration-page/resolvers';
import { BookingReportCaseFormComponent } from './configuration-page/customer-configuration/booking-report-case-form/booking-report-case-form.component';
import { BookingCasesComponent } from './configuration-page/customer-configuration/booking-cases/booking-cases.component';
import {HourlyRatesConfigurationComponent} from './configuration-page/hourly-rates-configuration/hourly-rates-configuration.component';
import {InsuranceListResolver} from './configuration-page/resolvers';
import {EmailTemplateListResolver} from './configuration-page/resolvers';
import {ImportDataConfigurationComponent} from './configuration-page/import-data-configuration/import-data-configuration.component';
import {ImportFilePageComponent} from './configuration-page/import-data-configuration/import-file-page/import-file-page.component';
import {MappingPageComponent} from './configuration-page/import-data-configuration/mapping-page/mapping-page.component';
import {EditPageComponent} from './configuration-page/import-data-configuration/edit-page/edit-page.component';

export const configurationRoutes: Routes = [
  {path: 'configuration', pathMatch: 'full', component: ConfigurationPageComponent},
  {path: 'configuration/room', pathMatch: 'full', component: RoomConfigurationComponent},
  {
    path: 'configuration/room/:id',
    pathMatch: 'full',
    component: RoomFormComponent,
    resolve: {
      room: RoomResolver,
      productList: ProductListResolver,
      locationList: LocationListResolver
    }
  },
  { path: 'configuration/location', pathMatch: 'full', component: LocationConfigurationComponent },
  {
    path: 'configuration/location/:id',
    pathMatch: 'full',
    component: LocationFormComponent,
    resolve: {
      location: LocationResolver
    }
  },
  { path: 'configuration/insurance', pathMatch: 'full', component: InsuranceConfigurationComponent },
  {
    path: 'configuration/insurance/:id',
    pathMatch: 'full',
    component: InsuranceFormComponent,
    resolve: {
      insurance: InsuranceResolver
    }
  },
  {
    path: 'configuration/organization',
    pathMatch: 'full',
    component: OrganizationFormComponent,
    resolve: {
      organization: OrganizationResolver
    }
  },
  {
    path: 'configuration/report',
    component: ReportConfigurationComponent,
    children: [
      {
        path: 'base',
        pathMatch: 'full',
        component: ReportBaseListComponent
      },
      {
        path: ':id',
        pathMatch: 'full',
        component: ReportFormComponent,
        resolve: {
          report: ReportResolver
        }
      }
    ]
  },
  {
    path: 'configuration/service',
    component: ServiceConfigurationComponent
  },
  {
    path: 'configuration/service/:id',
    component: ProductFormComponent,
    resolve: {
      product: ProductResolver,
      positions: TariffPositionsResolver,
      organization: OrganizationResolver,
      rooms: RoomsResolver,
      employees: EmployeesResolver
    }
  },
  {
    path: 'configuration/employee',
    component: EmployeeConfigurationComponent
  },
  {
    path: 'configuration/employee/:id',
    component: EmployeeFormComponent,
    resolve: {
      employee: EmployeeResolver,
      locations: LocationListResolver,
      products: ProductListResolver
    }
  },
  // {
  //   path: 'configuration/email-templates',
  //   pathMatch: 'full',
  //   component: EmailTemplatesComponent,
  //   resolve: {
  //     emailTemplates: EmailTemplateResolver
  //   }
  // },
  {
    path: 'configuration/hourly-rates',
    component: HourlyRatesConfigurationComponent,
    resolve: {
      insuranceList: InsuranceListResolver,
      tariffPositionsList: TariffPositionsResolver
    }
  },
  {
    path: 'configuration/customer',
    pathMatch: 'full',
    component: CustomerConfigurationComponent
  },
  {
    path: 'configuration/customer/:id',
    component: CustomerFormComponent,
    resolve: {
      customer: CustomerResolver,
      insurances: InsuranceListResolver
    },
    children: [
      {
        path: 'choose',
        pathMatch: 'full',
        component: CustomerConfigurationComponent,
        data: {
          mode: 'choose'
        }
      }
    ]
  },
  {
    path: 'configuration/customer/:id/case/:caseId',
    pathMatch: 'full',
    component: CaseFormComponent,
    resolve: {
      _case: CaseResolver,
      customer: CustomerResolver,
      typeList: CaseTypeListResolver,
      lawList: CaseLawListResolver,
      paymentTypeList: CasePaymentTypeListResolver,
      therapyTypeList: CaseTherapyTypeListResolver,
      diagnosisTypeList: CaseDiagnosisTypeResolver,
      referralTypeList: CaseReferralTypeResolver
    }
  },
  {
    path: 'configuration/customer/:id/booking/:bookingId/report/:reportId',
    pathMatch: 'full',
    component: BookingReportFormComponent,
    resolve: {
      report: BookingReportResolver,
      booking: BookingResolver
    }
  },
  {
    path: 'configuration/customer/:id/booking/:bookingId/report-case',
    component: BookingReportCaseFormComponent,
    resolve: {
      booking: BookingResolver
    },
    children: [
      {
        path: 'choose',
        pathMatch: 'full',
        component: BookingCasesComponent
      }
    ]
  },
  {
    path: 'configuration/customer/:id/dossier',
    pathMatch: 'full',
    component: DossierFormComponent,
    resolve: {
      dossier: DossierResolver
    }
  },
  {
    path: 'configuration/import-data',
    component: ImportDataConfigurationComponent
  },
  {
    path: 'configuration/import-data/import',
    component: ImportFilePageComponent
  },
  {
    path: 'configuration/import-data/mapping/:importId',
    component: MappingPageComponent
  },
  {
    path: 'configuration/import-data/edit/:importId',
    component: EditPageComponent
  }
];
