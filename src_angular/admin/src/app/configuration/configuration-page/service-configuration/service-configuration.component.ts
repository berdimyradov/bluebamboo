import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {routeAnimationOpacity, CommonPageComponent, CommonMenuItem} from "../../../common";
import {CommonListItem} from "../../../common/common-list/types/common-list-item";
import {Observable} from "rxjs/Observable";
import {ActivatedRoute, Router} from "@angular/router";
import {ProductConfigurationService} from "../services/product-configuration.service";

@Component({
  selector: 'admin-service-configuration',
  templateUrl: './service-configuration.component.html',
  styleUrls: ['./service-configuration.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class ServiceConfigurationComponent implements OnInit {
  public modules: CommonMenuItem[] = [];

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private productConfiguration: ProductConfigurationService
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.SERVICE-CONFIGURATION.TITLE';
  }

  public getList = ():Observable<CommonListItem[]> => {
    return new Observable<CommonListItem[]>(observer => {
      this.productConfiguration.getList().subscribe(
        result => {
          observer.next(result.map(it => {
            const t: CommonListItem = new CommonListItem();

            t.id = it.id;
            t.title = it.title;
            t.description = `${this.formatTime(it.duration)} - ${this.formatTime(it.breakAfter)} Pause - CHF ${it.price}`;

            return t;
          }));

          observer.complete();
        }
      );
    });
  };

  public edit(product: CommonListItem) {
    this.router.navigate(['./' + product.id], {relativeTo: this.activeRoute});
  }

  public add() {
    this.router.navigate(['./add'], {relativeTo: this.activeRoute});
  }

  private formatTime(duration): string {
    if (typeof duration === 'undefined') {
      return '0m';
    }

    const h: number = Math.floor(duration / 60);
    const m: number = duration % 60;

    return (h > 0 ? h + 'h ' : '') + m + 'm';
  }
}
