import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CommonPageComponent, DialogService, Employee, Room, routeAnimationOpacity} from "../../../../common";
import {ProductConfigurationService} from "../../services/product-configuration.service";
import {SelectOption} from "@bluebamboo/common-ui";
import {TranslateService} from '@ngx-translate/core';
import {ValuePrefixPipe} from '../../../../common/pipes';

@Component({
  selector: 'admin-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class ProductFormComponent implements OnInit {
  private mwstEnable: boolean;
  private roomsNormalize = {};
  private employeesNormalize = {};
  public product: any;
  public tariffPositions: SelectOption[];
  public rooms: Room[];
  public employees: Employee[];
  public selectedEmployees: any[] = [];
  public selectedRooms: any[] = [];
  public mwstGroups: SelectOption[] = [];
  public isView: boolean;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private productService: ProductConfigurationService,
    private dialog: DialogService,
    private translate: TranslateService,
    private valuePrefix: ValuePrefixPipe,
  ) { }

  ngOnInit() {
    this.product = this.activeRoute.snapshot.data.product;
    this.tariffPositions = this.activeRoute.snapshot.data.positions.map(it => {
      const t = new SelectOption();

      t.code = it.id;
      t.title= `${it.nr} - ${it.title}`;

      return t;
    });
    this.mwstEnable = !!Number(this.activeRoute.snapshot.data.organization.mwst);
    this.rooms = this.activeRoute.snapshot.data.rooms;
    this.roomsNormalize = this.activeRoute.snapshot.data.rooms.reduce((result: any, cur: Room) => {
      result[cur.id] = cur;

      return result;
    }, {});
    this.employees = this.activeRoute.snapshot.data.employees
      .map(it => Object.assign({title: `${it.firstname} ${it.name}`}, it));
    this.employeesNormalize = this.activeRoute.snapshot.data.employees.reduce((result: any, cur: Employee) => {
      result[cur.id] = cur;

      return result;
    }, {});
    this.selectedRooms = this.product.rooms.map(it => ({id: it.room_id}));
    this.selectedEmployees = this.product.employees.map(it => ({id: it.employee_id}));

    this.isView = !!this.product.id;
    this.mwstGroups = this.productService.getMWSTGroups();

    if(this.isView) {
      this.commonPage.title = 'PAGE.PRODUCT-FORM.TITLE.VIEW';
    } else {
      this.commonPage.title = 'PAGE.PRODUCT-FORM.TITLE.' + (this.product.id ? 'EDIT' : 'NEW');
    }

  }

  public back(force = false) {
    if (force || this.isView || !this.product.id) {
      this.router.navigate(['../'], {relativeTo: this.activeRoute});
    } else {
      if (this.product.id) {
        this.isView = true;
      }
    }
  }

  public save() {
    this.product.rooms = this.selectedRooms.map(
      it => {
        return {room_id: it.id, product_id: this.product.id}
      });
    this.product.employees = this.selectedEmployees.map(
      it => {
        return {employee_id: it.id, product_id: this.product.id}
      });
    this.productService.save(this.product)
      .subscribe(
        result => this.back()
      );
  }

  public delete() {
    this.dialog.confirm('PAGE.PRODUCT-FORM.CONFIRM-DELETE','PAGE.PRODUCT-FORM.CONFIRM-YES','PAGE.PRODUCT-FORM.CONFIRM-NO')
      .then((res: Boolean) => {
        if (res) {
          this.productService.delete(this.product).subscribe(
            result => this.back(true)
          );
        }
      });
  }

  public addPart() {
    this.product.productparts.push({product_id: this.product.id});
  }

  public removePart(pos: number) {
    this.product.productparts.splice(pos, 1);
  }

  public calculatePriceFor5Min(price: number, duration: number) {
    let ret = 0;

    if (!price || !duration) {
      return ret.toFixed(2);
    }

    if (duration == 0) {
      return ret.toFixed(2);
    }

    ret = 5 * price / duration;

    return ret.toFixed(2);
  }

  public getProductTariffText(tariff: string): string {
    const result = this.tariffPositions.find((item: SelectOption) => item.code === tariff);

    return result ? result.title : '';
  }

  public getMWSTGroupText(mwstGroupId: string): string {
    const result = this.mwstGroups.find((item: SelectOption) => item.code === mwstGroupId);

    return result ? result.title : '';
  }

  public getPricePartText(price, duration) {

//  console.log("price part: " + (typeof price));

    price = Number(price) || 0;
    duration = Number(duration) || 0;

    return this.valuePrefix.transform(price.toFixed(2), this.translate.instant('PAGE.PRODUCT-FORM.FORM.CHF')) +
      ' (' +
      this.translate.instant(
        'PAGE.PRODUCT-FORM.FORM.PARTS.PRICE-5MIN',
        { price: this.calculatePriceFor5Min(price, duration)}
      ) +
      ')';
  }

  public get viewRooms() {
    return this.selectedRooms.map(item => this.roomsNormalize[item.id]);
  }

  public get viewEmployees() {
    return this.selectedEmployees.map(item => this.employeesNormalize[item.id]);
  }
}
