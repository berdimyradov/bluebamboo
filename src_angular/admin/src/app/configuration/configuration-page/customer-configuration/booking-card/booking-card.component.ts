import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'booking-card',
  templateUrl: './booking-card.component.html',
  styleUrls: ['./booking-card.component.scss']
})
export class BookingCardComponent {
  @Input() public booking: any;
}
