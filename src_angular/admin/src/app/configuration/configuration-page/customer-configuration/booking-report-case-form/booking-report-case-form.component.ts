import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UploadService } from '@bluebamboo/common-ui';
import { CustomerConfigurationService } from '../../services';
import { BookingReport, Case } from '../../types';
import { CommonPageComponent } from '../../../../common';

@Component({
  selector: 'app-booking-report-case-form',
  templateUrl: './booking-report-case-form.component.html',
  styleUrls: ['./booking-report-case-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BookingReportCaseFormComponent implements OnInit {
  private image: any;

  public showMainScreen = true;
  public report: BookingReport;
  public booking: any;
  public _case: Case;

  constructor(
    private commonPage: CommonPageComponent,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private uploadService: UploadService,
    private customerConfigurationService: CustomerConfigurationService
  ) { }

  ngOnInit() {
    this.booking = this.activeRoute.snapshot.data.booking;
    this.report = new BookingReport(this.booking.report || {});

    if (!this.report.booking) {
      this.report.booking = this.booking;
    }

    this.commonPage.title = 'PAGE.BOOKING-REPORT-CASE-FORM.TITLE.' + (this.booking.report_id ? 'EDIT' : 'NEW');

    this.customerConfigurationService.tempCase.subscribe(res => {
      this._case = res;
    });
  }

  public save() {
    if (this.image) {
      this.uploadService.save(this.image.file)
        .subscribe((result: any) => {
          this.image = null;
          this.report.image = result.filename;

          this.saveBooking();
        });
    } else {
      this.saveBooking();
    }
  }

  private saveBooking() {
    if (this._case) {
      this.booking.case = this._case;
      this.booking.case_id = this._case.id;

      this.customerConfigurationService.saveBooking(this.booking)
        .subscribe(
          () => this.saveReport()
        );
    } else {
      this.saveReport();
    }
  }

  private saveReport() {
    this.customerConfigurationService.saveReport(this.report)
      .subscribe(
        () => this.back()
      );
  }

  public onImageChange($event) {
    this.image = $event;
  }

  public back() {
    this.router.navigate(['../../../dossier'], {relativeTo: this.activeRoute});
  }

  public onActivate() {
    this.showMainScreen = false;
  }

  public onDeactivate() {
    this.showMainScreen = true;
  }

  public chooseCase() {
    this.router.navigate(['./choose'], {relativeTo: this.activeRoute});
  }

  public get caseText() {
    return this.booking.case ? this.booking.case.title : (this._case ? this._case.title : '');
  }
}
