import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingReportCaseFormComponent } from './booking-report-case-form.component';

describe('BookingReportCaseFormComponent', () => {
  let component: BookingReportCaseFormComponent;
  let fixture: ComponentFixture<BookingReportCaseFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingReportCaseFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingReportCaseFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
