import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ArrayDataProvider, BbGridComponent } from '@bluebamboo/common-ui';
import { CustomerConfigurationService } from '../services/customer-configuration.service';
import { routeAnimationOpacity, CommonPageComponent, Customer } from '../../../common';


@Component({
  selector: 'app-customer-configuration',
  templateUrl: './customer-configuration.component.html',
  styleUrls: ['./customer-configuration.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class CustomerConfigurationComponent implements OnInit {
  private _searchTimeOut: any;
  private _search = '';

  public isChooseMode: boolean;
  public list: Customer[] = [];
  public gridModules: any[] = [TranslateModule];
  public bookingId: number;
  public gridControlsTemplate = `
  <a class="btn-floating btn-sm white btn-command"
      data-toggle="tooltip" data-placement="top"
      title="{{\'PAGE.REPORT-CONFIGURATION.ACTIONS.EDIT\'|translate}}">
      <i class="fa fa-magic">
      </i>
    </a>`

  public get search(): string {
    return this._search;
  }

  public set search(v: string) {
    this._search = v;

    this.startSearch(v);
  }

  @ViewChild('grid') private grid: BbGridComponent;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private customerConfigurationService: CustomerConfigurationService,
    private _location: Location
  ) { }

  ngOnInit() {
    this.bookingId = this.activeRoute.snapshot.params.bid;
    this.commonPage.title = 'PAGE.CUSTOMER-CONFIGURATION.TITLE';
    this.isChooseMode = this.activeRoute.snapshot.data.mode === 'choose';

    this.getList();
  }

  private getList() {
    this.customerConfigurationService.getList().subscribe(
      result => {
        this.list = result;

        this.startSearch(this._search);
      }
    );
  }

  private startSearch(term: string) {
    if (this._searchTimeOut) {
      clearTimeout(this._searchTimeOut);
    }
    this._searchTimeOut = setTimeout(() => {
      (this.grid.dataProvider as ArrayDataProvider).setFullTextFilter(term, ['name', 'firstname', 'city']);
    }, 300);
  }

  public add() {
    this.router.navigate(['./add'], { relativeTo: this.activeRoute });
  }

  public back() {
    if (this.bookingId) {
      this.router.navigate(['../../booking/unhandled-bookings/' + this.bookingId],
        { relativeTo: this.activeRoute });
      return;
    }

    this.router.navigate(['../'], { relativeTo: this.activeRoute });
  }

  public gridRowClick(row: Customer) {
    if (this.bookingId) {
      this.router.navigate(['../../booking/unhandled-bookings/' + this.bookingId],
        { relativeTo: this.activeRoute });

     setTimeout(() => {
        this.customerConfigurationService
          .tempCustomer
          .next(new Customer().parse(row))
     }, 1000)
      return false;
    }
    if (this.isChooseMode) {
      this.customerConfigurationService.tempCustomer.next(new Customer().parse(row));
      this._location.back();
    } else {
      this.router.navigate(['./' + row.id], { relativeTo: this.activeRoute });
    }
  }
}
