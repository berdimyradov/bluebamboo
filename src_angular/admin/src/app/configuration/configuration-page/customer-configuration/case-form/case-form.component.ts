import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { SelectOption, UploadService } from '@bluebamboo/common-ui';
import { CustomerConfigurationService } from '../../services';
import { Case } from '../../types';
import { Customer, CommonPageComponent } from '../../../../common';

@Component({
  selector: 'app-case-form',
  templateUrl: './case-form.component.html',
  styleUrls: ['./case-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CaseFormComponent implements OnInit {
  private image: any;

  public _case: Case;
  public isView: boolean;
  public typeList: SelectOption[];
  public paymentTypeList: SelectOption[];
  public therapyTypeList: SelectOption[];
  public referralTypeList: SelectOption[];
  public diagnosisTypeList: SelectOption[];
  public lawList: SelectOption[];

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private uploadService: UploadService,
    private customerConfigurationService: CustomerConfigurationService,
    private _location: Location
  ) { }

  ngOnInit() {
    this._case = this.activeRoute.snapshot.data._case;
    this.isView = !!this._case.id;

    this.typeList = this.activeRoute.snapshot.data.typeList.map(item => new SelectOption({
      code: item.id.toString(),
      title: item.label
    }));

    this.paymentTypeList = this.activeRoute.snapshot.data.paymentTypeList.map(item => new SelectOption({
      code: item.id.toString(),
      title: item.label
    }));

    this.therapyTypeList = this.activeRoute.snapshot.data.therapyTypeList.map(item => new SelectOption({
      code: item.id.toString(),
      title: item.label
    }));

    this.referralTypeList = this.activeRoute.snapshot.data.referralTypeList.map(item => new SelectOption({
      code: item.id.toString(),
      title: item.label
    }));

    this.diagnosisTypeList = this.activeRoute.snapshot.data.diagnosisTypeList.map(item => new SelectOption({
      code: item.id.toString(),
      title: item.label
    }));

    this.lawList = this.activeRoute.snapshot.data.lawList.map(item => new SelectOption({
      code: item.id.toString(),
      title: item.label
    }));

    if (!this._case.id) {
      this._case.customer = new Customer().parse(this.activeRoute.snapshot.data.customer);
    }

    this._case.customer_id = this._case.customer.id;

    if (this.isView) {
      this.commonPage.title = 'PAGE.CASE-FORM.TITLE.VIEW';
    } else {
      this.commonPage.title = 'PAGE.CASE-FORM.TITLE.' + (this._case.id ? 'EDIT' : 'NEW');
    }
  }

  public save() {
    if (this.image) {
      this.uploadService.save(this.image.file)
        .subscribe((result: any) => {
          this.image = null;
          this._case.image = result.filename;

          this.saveCase();
        });
    } else {
      this.saveCase();
    }
  }

  private saveCase() {
    this.customerConfigurationService.saveCase(this._case)
      .subscribe(
        () => this.back()
      );
  }

  public onImageChange($event) {
    this.image = $event;
  }

  public back() {
    if (this.isView || !this._case.id) {
      this._location.back();
    } else {
      if (this._case.id) {
        this.isView = true;
      }
    }
  }

  public get typeText(): string {
    const result = this.activeRoute.snapshot.data.typeList.find(item => item.id === this._case.type);

    return result ? result.label : '';
  }

  public get paymentTypeText(): string {
    const result = this.activeRoute.snapshot.data.paymentTypeList.find(item => item.id === this._case.payment_type);

    return result ? result.label : '';
  }

  public get lawText(): string {
    const result = this.activeRoute.snapshot.data.lawList.find(item => item.id === this._case.law);

    return result ? result.label : '';
  }

  public get diagnosisTypeText(): string {
    const result = this.activeRoute.snapshot.data.diagnosisTypeList.find(item => item.id === this._case.diagnosis_type);

    return result ? `(${result.label})` : '';
  }

  public get referralTypeText(): string {
    const result = this.activeRoute.snapshot.data.referralTypeList.find(item => item.id === this._case.referral_type);

    return result ? result.label : '';
  }

  public goToReport(bookingId: number, reportId: number) {
    let param: string;

    if (reportId) {
      param = String(reportId);
    } else {
      param = 'add';
    }

    this.router.navigate([`../../booking/${bookingId}/report/${param}`], {relativeTo: this.activeRoute});
  }
}
