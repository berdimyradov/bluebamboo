import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UploadService } from '@bluebamboo/common-ui';
import { CustomerConfigurationService } from '../../services';
import { Dossier } from '../../types';
import { CommonPageComponent } from '../../../../common';

@Component({
  selector: 'app-dossier-form',
  templateUrl: './dossier-form.component.html',
  styleUrls: ['./dossier-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DossierFormComponent implements OnInit {
  private image: any;

  public dossier: Dossier;
  public isView = true;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private uploadService: UploadService,
    private customerConfigurationService: CustomerConfigurationService
  ) { }

  ngOnInit() {
    this.dossier = this.activeRoute.snapshot.data.dossier;
    this.commonPage.title = 'PAGE.DOSSIER-FORM.TITLE';
  }

  public save() {
    if (this.image) {
      this.uploadService.save(this.image.file)
        .subscribe((result: any) => {
          this.image = null;
          this.dossier.image = result.filename;

          this.saveDossier();
        });
    } else {
      this.saveDossier();
    }
  }

  private saveDossier() {
    this.customerConfigurationService.saveDossier(this.dossier)
      .subscribe(
        () => this.back()
      );
  }

  public onImageChange($event) {
    this.image = $event;
  }

  public back() {
    if (this.isView) {
      this.router.navigate(['../'], { relativeTo: this.activeRoute });
    } else {
      this.isView = true;
    }
  }

  public goToReport(bookingId: number) {
    this.router.navigate([`../booking/${bookingId}/report-case`], {relativeTo: this.activeRoute});
  }
}
