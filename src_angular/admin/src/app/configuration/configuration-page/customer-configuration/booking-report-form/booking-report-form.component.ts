import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { UploadService } from '@bluebamboo/common-ui';
import { CustomerConfigurationService } from '../../services';
import { BookingReport } from '../../types';
import { CommonPageComponent } from '../../../../common';

@Component({
  selector: 'app-booking-report-form',
  templateUrl: './booking-report-form.component.html',
  styleUrls: ['./booking-report-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BookingReportFormComponent implements OnInit {
  private image: any;

  public report: BookingReport;
  public booking: any;
  public isView: boolean;

  constructor(
    private commonPage: CommonPageComponent,
    private activeRoute: ActivatedRoute,
    private uploadService: UploadService,
    private customerConfigurationService: CustomerConfigurationService,
    private _location: Location
  ) { }

  ngOnInit() {
    this.report = this.activeRoute.snapshot.data.report;
    this.booking = this.activeRoute.snapshot.data.booking;

    this.isView = !!this.report.id;

    if (!this.report.booking) {
      this.report.booking = this.booking;
    }

    if (this.isView) {
      this.commonPage.title = 'PAGE.BOOKING-REPORT-FORM.TITLE.VIEW';
    } else {
      this.commonPage.title = 'PAGE.BOOKING-REPORT-FORM.TITLE.' + (this.report.id ? 'EDIT' : 'NEW');
    }
  }

  public save() {
    if (this.image) {
      this.uploadService.save(this.image.file)
        .subscribe((result: any) => {
          this.image = null;
          this.report.image = result.filename;

          this.saveReport();
        });
    } else {
      this.saveReport();
    }
  }


  private saveReport() {
    this.customerConfigurationService.saveReport(this.report)
      .subscribe(
        () => this.back()
      );
  }

  public onImageChange($event) {
    this.image = $event;
  }

  public back() {
    if (this.isView || !this.report.id) {
      this._location.back();
    } else {
      if (this.report.id) {
        this.isView = true;
      }
    }
  }
}
