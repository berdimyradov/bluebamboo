import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingReportFormComponent } from './booking-report-form.component';

describe('BookingReportFormComponent', () => {
  let component: BookingReportFormComponent;
  let fixture: ComponentFixture<BookingReportFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingReportFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingReportFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
