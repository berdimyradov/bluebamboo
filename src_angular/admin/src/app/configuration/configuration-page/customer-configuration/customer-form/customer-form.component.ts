import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UploadService, SelectOption } from '@bluebamboo/common-ui';
import { CustomerConfigurationService } from '../../services';
import { Insurance } from '../../types';
import { CommonPageComponent, DialogService, Customer } from '../../../../common';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CustomerFormComponent implements OnInit {
  private _selectField = '';

  public showMainScreen = true;
  public photo: any;
  public customer: Customer;
  public insurances: Insurance[] = [];
  public insuranceList: SelectOption[] = [];
  public isView: boolean;
  public mode: string;

  private bookingId: number;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private customerConfigurationService: CustomerConfigurationService,
    private dialog: DialogService,
    private uploadService: UploadService
  ) { }

  ngOnInit() {
    this.mode = this.activeRoute.snapshot.data.mode;
    this.customer = this.activeRoute.snapshot.data.customer;
    this.insurances = this.activeRoute.snapshot.data.insurances;
    this.isView = !!this.customer.id;
    this.bookingId = this.activeRoute.snapshot.params.bid;
    this.insuranceList = this.insurances.map((item: Insurance) => new SelectOption({
      code: item.id.toString(),
      title: item.name
    }));

    if (this.isView) {
      this.commonPage.title = 'PAGE.CUSTOMER-FORM.TITLE.VIEW';
    } else {
      this.commonPage.title = 'PAGE.CUSTOMER-FORM.TITLE.' + (this.customer.id ? 'EDIT' : 'NEW');
    }

    this.customerConfigurationService.tempCustomer.subscribe((customer: Customer) => {
      if (this._selectField && this.customer.id !== customer.id) {
        this.customer[this._selectField] = customer;
        this.customer[this._selectField + '_id'] = customer.id;
      }
    });
  }

  public back() {
    if (this.bookingId) {
      this.router
      .navigate(['./../../../booking/unhandled-bookings/' + this.bookingId],
                         { relativeTo: this.activeRoute });
      return;
    }

    if (this.isView || !this.customer.id) {
      this.router.navigate(['../'], { relativeTo: this.activeRoute });
    } else {
      if (this.customer.id) {
        this.isView = true;
      }
    }
  }

  public save() {
    if (this.photo) {
      this.uploadService.save(this.photo.file)
        .subscribe((result: any) => {
          this.photo = null;
          this.customer.foto = result.filename;

          this.saveCustomer();
        });
    } else {
      this.saveCustomer();
    }
  }

  private saveCustomer() {
    if (typeof this.customer.id === 'undefined') {
      this.customerConfigurationService.saveNew(this.customer)
      .subscribe((data) => {
        if (this.bookingId) {
          setTimeout(() => this.customerConfigurationService.tempCustomer.next(data), 1000)
        }

        this.router.navigate(['./../../../booking/unhandled-bookings/' + this.bookingId],
                             { relativeTo: this.activeRoute });
        return true;
      });
    } else {
      this.customerConfigurationService.save(this.customer)
        .subscribe( () => this.back());
    }
  }

  public onPhotoChange($event) {
    this.photo = $event;
  }

  public delete() {
    this.dialog
      .confirm(
      'PAGE.CUSTOMER-FORM.CONFIRM-DELETE',
      'PAGE.CUSTOMER-FORM.CONFIRM-YES',
      'PAGE.CUSTOMER-FORM.CONFIRM-NO'
      )
      .then((res: Boolean) => {
        if (res) {
          this.customerConfigurationService.delete(this.customer).subscribe(
            () => this.back()
          );
        }
      });
  }

  public get genderText() {
    if (this.customer.gender === 'male') {
      return 'PAGE.CUSTOMER-FORM.FORM.GENDER-MAN';
    } else if (this.customer.gender === 'female') {
      return 'PAGE.CUSTOMER-FORM.FORM.GENDER-WOMAN';
    }

    return '';
  }

  public get birthdayText() {
    const { birth_day, birth_month, birth_year } = this.customer;
    const result = [];

    if (birth_day && birth_month) {
      result.push(birth_day, birth_month);

      if (birth_year) {
        result.push(birth_year);
      }
    }

    return result.join('.');
  }

  public get insuranceText(): string {
    const result = this.insurances.find((item: Insurance) => item.id === this.customer.insurance_id);

    return result ? result.name : '';
  }

  public get salutationText(): string {
    switch (this.customer.address_you) {
      case true:
        return 'PAGE.CUSTOMER-FORM.FORM.SALUTATION.DU';
      case false:
        return 'PAGE.CUSTOMER-FORM.FORM.SALUTATION.SIE';
      default:
        return '';
    }
  }

  public chooseCustomer(field) {
    this._selectField = field;

    this.router.navigate(['./choose'], { relativeTo: this.activeRoute });
  }

  public removeModelField(field) {
    this.customer[field] = null;
    this.customer[field + '_id'] = null;
  }

  public onActivate() {
    this.showMainScreen = false;
  }

  public onDeactivate() {
    this.showMainScreen = true;
  }
}
