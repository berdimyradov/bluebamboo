import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerConfigurationComponent } from './customer-configuration.component';

describe('CustomerConfigurationComponent', () => {
  let component: CustomerConfigurationComponent;
  let fixture: ComponentFixture<CustomerConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
