import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingCasesComponent } from './booking-cases.component';

describe('BookingCasesComponent', () => {
  let component: BookingCasesComponent;
  let fixture: ComponentFixture<BookingCasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingCasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingCasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
