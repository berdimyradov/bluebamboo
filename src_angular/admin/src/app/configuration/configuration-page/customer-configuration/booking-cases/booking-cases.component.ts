import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Case } from '../../types';
import { CustomerConfigurationService } from '../../services';
import { CommonPageComponent, CommonListItem } from '../../../../common';

@Component({
  selector: 'app-booking-cases',
  templateUrl: './booking-cases.component.html',
  styleUrls: ['./booking-cases.component.scss']
})
export class BookingCasesComponent implements OnInit {
  constructor(
    private commonPage: CommonPageComponent,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private customerConfigurationService: CustomerConfigurationService
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.BOOKING-CASES.TITLE';
  }

  public getList = (): Observable<CommonListItem[]> => {
    return new Observable<CommonListItem[]>(observer => {
      const customerId = Number(this.activeRoute.parent.snapshot.paramMap.get('id'));

      this.customerConfigurationService.getCaseList(customerId).subscribe(
        result => {
          observer.next(result);
          observer.complete();
        }
      );
    });
  };

  public back() {
    this.router.navigate(['../'], { relativeTo: this.activeRoute });
  }

  public add() {
    this.router.navigate(['../../../../case/add'], {relativeTo: this.activeRoute});
  }

  public choose(row: CommonListItem) {
    this.customerConfigurationService.tempCase.next(new Case(row));
    this.back();
  }
}
