import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ConfigurationService} from "../configuration.service";
import {CommonMenuItem} from "../../common";
import {CommonPageComponent, routeAnimationOpacity} from "../../common/";

@Component({
  selector: 'admin-configuration-page',
  templateUrl: './configuration-page.component.html',
  styleUrls: ['./configuration-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class ConfigurationPageComponent implements OnInit {
  public modules: CommonMenuItem[] = [];

  constructor(
    private configurationService: ConfigurationService,
    private commonPage: CommonPageComponent
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.CONFIGURATION.TITLE';
    this.configurationService.getListModules()
      .subscribe(
        result => {
          this.modules = result;
        }
      );
  }
}
