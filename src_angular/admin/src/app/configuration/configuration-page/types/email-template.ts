import {EmailTemplateRole} from './email-template-role';

export class EmailTemplate {
  public id: number;
  public role_id: number;
  public template: string;
  public vars: any;
  public templatebase: any;
  public role: EmailTemplateRole;
  public subject: string;

  constructor(data: any) {
    this.id = data.id;
    this.role_id = data.role_id;
    this.template = data.template;
    this.vars = data.vars;
    this.subject = data.subject;
    this.templatebase = data.templatebase;
    this.role = new EmailTemplateRole(data.role);
  }
}
