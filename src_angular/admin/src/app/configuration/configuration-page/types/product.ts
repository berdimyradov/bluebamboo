export class Product {
  public id: number;
  public title: string;
  public price: number;
  public duration: number;

  public parse(data: any): Product {
    const names: string[] = ['id', 'title', 'price', 'duration'];
    names.forEach((name) => this[name] = data[name]);
    return this;
  }
}
