import { Customer } from '../../../common';

export class Case {
  public id: number;
  public title: string;
  public type: number;
  public therapy_type: number;
  public payment_type: number;
  public law: number;
  public diagnosis_type: number;
  public diagnosis_text: string;
  public comment: string;
  public comment_invoice: string;
  public referral_type: number;
  public referral_name: string;
  public referral_gln: string;
  public referral_zsr: string;
  public accident_date: string;
  public image: string;
  public isOpen: boolean;
  public isCurrent: boolean;
  public customer_id: number;
  public customer: Customer;
  public bookings: any[];
  public reports: any[];

  constructor(data = {}) {
    Object.assign(this, data);

    if (this.customer) {
      this.customer = new Customer().parse(this.customer);
    }
  }
}
