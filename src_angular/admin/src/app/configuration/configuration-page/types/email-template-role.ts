export class EmailTemplateRole {
  public id: number;
  public title: string;

  constructor(data = {}) {
    const names: string[] = ['id', 'title'];

    names.forEach(name => this[name] = data[name]);
  }
}
