export class HourlyRate {
  public tariffposition_id: number;
  public insurance_id: number;
  public max_pph: number;
  public includeMWST: boolean;

  constructor(data) {
    this.tariffposition_id = data.tariffposition_id || null;
    this.insurance_id = data.insurance_id;
    this.max_pph = data.max_pph;
    this.includeMWST = typeof data.includeMWST === 'boolean' ? data.includeMWST : true;
  }
}
