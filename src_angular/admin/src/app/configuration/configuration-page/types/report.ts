import {ReportSchema} from "../report-configuration/report-form/types/report-schema";
import {ReportData} from "../report-configuration/report-form/types/report-data";
import {ReportInfo} from "./";

export class Report {
  public id: number;
  public title: string;
  public group: string;
  public description: string;
  public active: boolean;
  public editable: boolean;
  public role_id: number;
  public group_id: number;
  public standard: string;
  public template: any;
  public schema: ReportSchema;
  public data: ReportData;
  public info: ReportInfo;
  public isNew: boolean;

  public roles: Object;

  public parse(data: any): Report {
    const names: string[] = [
      'id',
      'title',
      'group',
      'description',
      'schema',
      'data',
      'active',
      'editable',
      'role_id',
      'group_id',
      'standard',
      'template',
      'roles'
    ];
    names.forEach((name) => this[name] = data[name] || this[name]);
    return this;
  }
}
