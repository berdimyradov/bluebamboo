export class Insurance {
  public id: number;
  public name: string;
  public client_id: number;

  public parse(data: any): Insurance {
    this.id = data.id || this.id;
    this.name = data.name || this.name;
    this.client_id = data.client_id || this.client_id;
    return this;
  }
}
