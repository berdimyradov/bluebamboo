import { Case } from '../types';
import { Customer } from '../../../common';

export class Dossier {
  public id: number;
  public medical_history: string;
  public medicines: string;
  public allergies: string;
  public image: string;
  public customer: Customer;
  public bookings: any[];
  public cases: Case[];

  constructor(data = {}) {
    Object.assign(this, data);

    if (this.customer) {
      this.customer = new Customer().parse(this.customer);
    }

    if (this.cases) {
      this.cases = this.cases.map(item => new Case(item));
    }
  }
}
