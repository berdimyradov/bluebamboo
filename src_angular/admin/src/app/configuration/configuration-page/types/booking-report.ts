export class BookingReport {
  public id: number;
  public text: string;
  public comment: string;
  public image: number;
  public booking: any;

  constructor(data = {}) {
    Object.assign(this, data);
  }
}
