import {ReportSchema} from "../report-configuration/report-form/types/report-schema";

export class ReportInfo {
  template: ReportSchema;
  description: string;
  editable: boolean;
  group_id: number;
  group: any;
  id: number;
  role: any;
  role_id: number;
  title: string;
  thumbnail: string;
  standard: string;
  active: boolean;

  public parse(data: any): ReportInfo {
    const names: string[] = ['id','title','description','editable','group_id', 'group', 'role_id', 'role', 'thumbnail', 'standard', 'active'];
    names.forEach((name) => this[name] = data[name] === undefined ? this[name] : data[name]);
    if (data.template) {
      this.template = new ReportSchema();
      this.template.parse(data.template);
    }
    return this;
  }
}
