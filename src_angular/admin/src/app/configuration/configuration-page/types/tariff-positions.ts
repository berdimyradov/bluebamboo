export interface TariffPositions {
  id: number;
  nr: number;
  title: string;
}
