import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Room} from '../../../common';
import {Http, Response, URLSearchParams, RequestOptions} from "@angular/http";

@Injectable()
export class RoomConfigurationService {

  constructor( private http: Http ) { }

  private getLocations(fields: string[]): Observable<Room[]> {
    let requestOptions: RequestOptions = new RequestOptions();
    requestOptions.params = new URLSearchParams();
    requestOptions.params.set('_fields', fields.join(','));
    return this.http.get('/api/v1/loc/rooms', requestOptions)
      .map((res: Response) => res.json().map((it: any) => new Room().parse(it)))
      .catch(this.handleError);
  }

  public getList(): Observable<Room[]> {
    return this.getLocations(['id', 'title', 'description'])
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  public get(id: number): Observable<Room> {
    return this.http.get('/api/v1/loc/rooms/' + id)
      .map((res: Response) => new Room().parse(res.json()))
      .catch(this.handleError);
  }

  public save(room: Room): Observable<Room> {
    (room.products || []).forEach((p: any) => {p.room_id = room.id;});
    const res: Observable<Response> = room.id
      ? this.http.put('/api/v1/loc/rooms/' + room.id, room)
      : this.http.post('/api/v1/loc/rooms/', room);
    return res.map((res: Response) => new Room().parse(res.json()))
      .catch(this.handleError);
  }

  public delete(room: Room): Observable<void> {
    return this.http.delete('/api/v1/loc/rooms/' + room.id)
      .map(() => {return;})
      .catch(this.handleError);
  }
}
