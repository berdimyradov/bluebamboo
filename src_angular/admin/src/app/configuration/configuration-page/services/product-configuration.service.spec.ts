import { TestBed, inject } from '@angular/core/testing';

import { ProductConfigurationService } from './product-configuration.service';

describe('ProductConfigurationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductConfigurationService]
    });
  });

  it('should be created', inject([ProductConfigurationService], (service: ProductConfigurationService) => {
    expect(service).toBeTruthy();
  }));
});
