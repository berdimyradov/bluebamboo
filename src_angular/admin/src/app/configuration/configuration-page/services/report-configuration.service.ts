import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Report} from "../types";
import {Http, Response} from "@angular/http";
import {ReportInfo} from "../types/report-info";

@Injectable()
export class ReportConfigurationService {

  constructor(private http: Http) { }

  public getRoles(groupId: number) {
    return this.http.get(`api/v1/pdf/templates/groups/${groupId}`)
    .map((res: Response) => res.json())
    .catch(this.handleError);
  }

  public getList(): Observable<any[]> {
    return this.http.get('/api/v1/pdf/templates')
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public getBaseList(): Observable<any[]> {
    return this.http.get('/api/v1/pdf/templates/bases')
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public getBase(id: number): Observable<ReportInfo> {
    return this.http.get('/api/v1/pdf/templates/bases/' + id)
      .map((res: Response) => (new ReportInfo()).parse(res.json()))
      .catch(this.handleError);
  }

  public getPreviewData(): Observable<any> {
    return this.http.get('/api/v1/pdf/templates/previewcontent')
      .map((res: Response) => res.json().content_json)
      .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  public get(id: number): Observable<ReportInfo> {
    return this.http.get('/api/v1/pdf/templates/'+id.toString())
      .map((res: Response) => (new ReportInfo()).parse(res.json()))
      .catch(this.handleError);
  }

  public delete(id: number): Observable<void> {
    return this.http.delete('/api/v1/pdf/templates/'+id.toString())
      .map(() => null)
      .catch(this.handleError);
  }

  public new(info: ReportInfo, template: any): Observable<void> {
    return this.http.post('/api/v1/pdf/templates', {
      title: info.title,
      active: info.active,
      editable: info.editable,
      role_id: info.role_id,
      group_id: info.group_id,
      description: info.description,
	  standard: info.standard,
      template: template
    })
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public update(report: Report) {
    const data: any = {
      title: report.title,
      active: report.active,
      editable: report.editable,
      role_id: report.role_id,
      group_id: report.group_id,
      description: report.description,
	    standard: report.standard,
      template: report.template
    };
    return this.http.put('/api/v1/pdf/templates/' + report.id, data)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public preivewPdf(data: any): Observable<any> {
	return new Observable<any>(observable => {
		this.http.post('/api/v1/pdf/templates/previewpdf', data).subscribe(
			data => {
		    	let res = data.json();
				// let url: string = res.path;
				observable.next(res);
		  	},
			error => observable.error(error)
		);
      //.map((res: Response) => res.json())
      //.catch(this.handleError);
  	});
  }

  public duplicate(report_id: number): Observable<void> {
    return new Observable<void>(observable => {
      this.get(report_id).subscribe(
        info => {
          info.active = false;
          info.title += ' - Copy';
          this.new(info, info.template.json()).subscribe(
            () => {
              observable.next(null);
              observable.complete();
            },
                error => observable.error(error)
          )
        },
        error => observable.error(error)
      )
    });
  }
}
