import { TestBed, inject } from '@angular/core/testing';

import { RoomConfigurationService } from './room-configuration.service';

describe('RoomConfigurationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RoomConfigurationService]
    });
  });

  it('should be created', inject([RoomConfigurationService], (service: RoomConfigurationService) => {
    expect(service).toBeTruthy();
  }));
});
