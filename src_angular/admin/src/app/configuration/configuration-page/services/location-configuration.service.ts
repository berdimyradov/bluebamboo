import { Injectable } from '@angular/core';
import {Http, Response, URLSearchParams, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Location} from '../../../common';

@Injectable()
export class LocationConfigurationService {
  constructor(private http: Http) { }

  private getLocations(fields: string[]): Observable<Location[]> {
    let requestOptions: RequestOptions = new RequestOptions();
    requestOptions.params = new URLSearchParams();
    requestOptions.params.set('_fields', fields.join(','));
    return this.http.get('/api/v1/loc/locations', requestOptions)
      .map((res: Response) => res.json().map((it: any) => new Location().parse(it)))
      .catch(this.handleError);
  }

  public getList(): Observable<Location[]> {
    return this.getLocations(['id', 'title', 'description', 'zip', 'city', 'street'])
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  public get(id: number): Observable<Location> {
    return this.http.get('/api/v1/loc/locations/' + id)
      .map((res: Response) => new Location().parse(res.json()))
      .catch(this.handleError);
  }

  public save(location: Location): Observable<Location> {
    const res: Observable<Response> = location.id
      ? this.http.put('/api/v1/loc/locations/' + location.id, location)
      : this.http.post('/api/v1/loc/locations/', location);
    return res.map((res: Response) => new Location().parse(res.json()))
      .catch(this.handleError);
  }

  public delete(location: Location): Observable<void> {
    return this.http.delete('/api/v1/loc/locations/' + location.id)
      .map(() => {return;})
      .catch(this.handleError);
  }
}
