import { TestBed, inject } from '@angular/core/testing';

import { OrganizationConfigurationService } from './organization-configuration.service';

describe('OrganizationConfigurationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrganizationConfigurationService]
    });
  });

  it('should be created', inject([OrganizationConfigurationService], (service: OrganizationConfigurationService) => {
    expect(service).toBeTruthy();
  }));
});
