import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Insurance } from '../types';
import { HourlyRate } from "../types/hourly-rate";

@Injectable()
export class InsuranceConfigurationService {
  constructor(private http: Http) { }

  public getList(): Observable<Insurance[]> {
    return this.http.get('/api/v1/insu/insurances')
      .map((res: Response) => res.json().map((it: any) => new Insurance().parse(it)))
      .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  public get(id: number): Observable<Insurance> {
    return this.http.get('/api/v1/insu/insurances/' + id)
      .map((res: Response) => new Insurance().parse(res.json()))
      .catch(this.handleError);
  }

  public save(insurance: Insurance): Observable<Insurance> {
    const res: Observable<Response> = insurance.id
            ? this.http.put('/api/v1/insu/insurances/' + insurance.id, insurance)
            : this.http.post('/api/v1/insu/insurances/', insurance);
    return res.map((res: Response) => new Insurance().parse(res.json()))
      .catch(this.handleError);
  }

  public delete(insurance: Insurance): Observable<void> {
    return this.http.delete('/api/v1/insu/insurances/' + insurance.id)
      .map(() => {return;})
      .catch(this.handleError);
  }

  public getHourlyRates(insuranceId: number) {
    return this.http.get('/api/v1/insu/hourlyrates/insurance/' + insuranceId)
      .map((res: Response) => res.json().map(item => new HourlyRate(item)))
      .catch(this.handleError);
  }

  public saveHourlyRates(id: number, data: object[]) {
    return this.http.post('/api/v1/insu/hourlyrates/insurance/' + id, data)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }
}
