import { Injectable } from '@angular/core';
import { Http, URLSearchParams, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Case, Dossier, BookingReport } from '../types';
import { Customer } from '../../../common';
import { FrameworkService } from '../../../framework/service.framework';
import 'rxjs/add/operator/debounceTime';

@Injectable()
export class CustomerConfigurationService extends FrameworkService {
  public tempCustomer: Subject<Customer> = new Subject();
  public tempCase: Subject<Case> = new Subject();

  constructor(public http: Http) {
    super(http);
  }

  private getCustomers(fields: string[]): Observable<Customer[]> {
    const requestOptions: RequestOptions = new RequestOptions();

    requestOptions.params = new URLSearchParams();
    requestOptions.params.set('_fields', fields.join(','));

    return this.http.get('/api/v1/cust/customers', requestOptions)
      .map((res: Response) => res.json().map((it: any) => new Customer().parse(it)))
      .catch(this.handleError);
  }

  public getList(): Observable<Customer[]> {
    return this.getCustomers(['id', 'name', 'firstname', 'city'])
  }

  public get(id: number): Observable<Customer> {
    return this.http.get(`/api/v1/cust/customers/${id}`)
      .map((res: Response) => new Customer().parse(res.json()))
      .catch(this.handleError);
  }

  public getTempSubscription() {
    return this.tempCustomer.asObservable();
  }

  public save(customer: Customer): Observable<Customer> {
    return this.http.put(`/api/v1/cust/customers/${customer.id}`, customer)
      .map((res: Response) => new Customer().parse(res.json()))
      .catch(this.handleError);
  }

  public saveNew(customer: Customer): Observable<Customer> {
    return this.http.post(`/api/v1/cust/customers`, customer)
      .map((res: Response) => new Customer().parse(res.json()))
      .catch(this.handleError);
  }

  public delete(customer: Customer): Observable<void> {
    return this.http.delete(`/api/v1/cust/customers/${customer.id}`)
      .map((res: Response) => true)
      .catch(this.handleError);
  }

  public getCase(id: number): Observable<Case> {
    return this.http.get(`/api/v1/cust/cases/${id}`)
      .map((res: Response) => new Case(res.json()))
      .catch(this.handleError);
  }

  public getDossier(id: number): Observable<Dossier> {
    return this.http.get(`api/v1/cust/dossiers/customer/${id}`)
      .map((res: Response) => new Dossier(res.json()))
      .catch(this.handleError);
  }

  public getReport(id: number): Observable<BookingReport> {
    return this.http.get(`api/v1/cust/reports/${id}`)
      .map((res: Response) => new BookingReport(res.json()))
      .catch(this.handleError);
  }

  public getBooking(id: number): Observable<any> {
    return this.http.get(`api/v1/booking/bookings/${id}`)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public getCaseTypeList(): Observable<any> {
    return Observable.of([
      {
        id: 1,
        label: 'Krankheit'
      }, {
        id: 2,
        label: 'Unfall'
      }, {
        id: 3,
        label: 'Prävention'
      }, {
        id: 4,
        label: 'Schwangerschaft'
      }, {
        id: 5,
        label: 'Geburtsgebrechen'
      }, {
        id: 6,
        label: 'Sonstige'
      }
    ]);
  }

  public getCaseTherapyTypeList(): Observable<any> {
    return Observable.of([
      {
        id: 1,
        label: 'Einzeltherapie'
      }, {
        id: 2,
        label: 'Gruppentherapie'
      }
    ]);
  }

  public getCaseDiagnosisTypeList(): Observable<any> {
    return Observable.of([
      {
        id: 1,
        label: 'Text'
      }, {
        id: 2,
        label: 'ICD-10'
      }, {
        id: 3,
        label: 'ICPC'
      }, {
        id: 4,
        label: 'Tessiner Code'
      }
    ]);
  }

  public getCaseLawList(): Observable<any> {
    return Observable.of([
      {
        id: 1,
        label: 'KVG - Kranken-Versicherungs-Gesetz'
      }, {
        id: 2,
        label: 'UVG - Unfall-Versicherungs-Gesetz'
      }, {
        id: 3,
        label: 'IVG - Bundesgesetz über die Invalidenversicherung'
      }, {
        id: 4,
        label: 'MVG - Bundesgesetz über die Militärversicherung'
      }, {
        id: 5,
        label: 'VVG - Versicherungs-Vertrags-Gesetz'
      }, {
        id: 6,
        label: 'ORG'
      }
    ]);
  }

  public getCasePaymentTypeList(): Observable<any> {
    return Observable.of([
      {
        id: 1,
        label: 'TG - Tiers Garant / Rückerstattung durch Versicherer'
      }, {
        id: 2,
        label: 'TP - Tiers Payant / Direktzahlung durch Versicherer'
      }
    ]);
  }

  public getCaseReferralTypeList(): Observable<any> {
    return Observable.of([
      {
        id: 1,
        label: 'Arzt'
      }, {
        id: 2,
        label: 'Therapeut'
      }, {
        id: 3,
        label: 'Psychologe'
      }, {
        id: 4,
        label: 'Psychiater'
      }, {
        id: 5,
        label: 'Sonstige'
      }
    ]);
  }

  public saveCase(_case: Case): Observable<Case> {
    let response;

    if (_case.id) {
      response = this.http.put(`/api/v1/cust/cases/${_case.id}`, _case);
    } else {
      response = this.http.post(`/api/v1/cust/cases`, _case);
    }

    return response
      .map((res: Response) => new Case(res.json()))
      .catch(this.handleError);
  }

  public saveDossier(dossier: Dossier): Observable<Dossier> {
    return this.http.put(`/api/v1/cust/dossiers/${dossier.id}`, dossier)
      .map((res: Response) => new Dossier(res.json()))
      .catch(this.handleError);
  }

  public saveReport(report: BookingReport): Observable<BookingReport> {
    let response;

    if (report.id) {
      response = this.http.put(`/api/v1/cust/reports/${report.id}`, report);
    } else {
      response = this.http.post(`/api/v1/cust/reports`, report);
    }

    return response
      .map((res: Response) => new BookingReport(res.json()))
      .catch(this.handleError);
  }

  public getCaseList(customerId: number): Observable<any> {
    const requestOptions: RequestOptions = new RequestOptions();

    requestOptions.params = new URLSearchParams();
    requestOptions.params.set('customer_id', String(customerId));

    return this.http.get('/api/v1/cust/cases', requestOptions)
      .map((res: Response) => res.json().map(item => new Case(item)))
      .catch(this.handleError);
  }

  public saveBooking(booking: any): Observable<Dossier> {
    return this.http.put(`/api/v1/booking/bookings/${booking.id}`, booking)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }
}
