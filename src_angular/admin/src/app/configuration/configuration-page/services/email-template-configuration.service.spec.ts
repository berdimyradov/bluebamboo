import { TestBed, inject } from '@angular/core/testing';

import { EmailTemplateConfigurationService } from './email-template-configuration.service';

describe('EmailTemplateConfigurationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmailTemplateConfigurationService]
    });
  });

  it('should be created', inject([EmailTemplateConfigurationService], (service: EmailTemplateConfigurationService) => {
    expect(service).toBeTruthy();
  }));
});
