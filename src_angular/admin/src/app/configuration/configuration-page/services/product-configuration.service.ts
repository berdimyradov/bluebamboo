import {Injectable} from '@angular/core';
import {Http, Response, URLSearchParams, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Product, TariffPositions} from '../types';
import {SelectOption} from "@bluebamboo/common-ui";

@Injectable()
export class ProductConfigurationService {
  constructor(private http: Http) {
  }

  private getProducts(fields: string[]): Observable<Product[]> {
    let requestOptions: RequestOptions = new RequestOptions();
    requestOptions.params = new URLSearchParams();
    requestOptions.params.set('_fields', fields.join(','));
    return this.http.get('/api/v1/prod/products', requestOptions)
      .map((res: Response) => res.json().map((it: any) => new Product().parse(it)))
      .catch(this.handleError);
  }

  public getSelectList(): Observable<Product[]> {
    return this.getProducts(['id', 'title', 'price', 'duration'])
  }

  public getList(): Observable<any[]> {
    return this.http.get('/api/v1/prod/products')
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public get(id: number): Observable<any[]> {
    return this.http.get('/api/v1/prod/products/' + id)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public getTariffPositions(): Observable<TariffPositions[]> {
    return this.http.get('/api/v1/prod/tariffpositions')
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public getMWSTGroups(): SelectOption[] {
    return [
      new SelectOption({
        code: 0,
        title: '0 %'
      }),
      new SelectOption({
        code: 1,
        title: '2.5 %'
      }),
      new SelectOption({
        code: 2,
        title: '8 %'
      })
    ];
  }

  public save(product: any): Observable<void> {
    product.duration = product.productparts.reduce((prev, cur) => parseInt(prev || 0) + parseInt(cur.duration), 0);
    product.price = product.productparts.reduce((prev, cur) => parseFloat(prev || 0) + parseFloat(cur.price), 0);

    if (product.id) {
      return this.http.put('/api/v1/prod/products/' + product.id, product)
        .map((res: Response) => res.json())
        .catch(this.handleError);
    }

    return this.http.post('/api/v1/prod/products', product)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public delete(product: any): Observable<boolean> {
    return this.http.delete('/api/v1/prod/products/' + product.id)
      .map(() => {
        return true;
      })
      .catch(() => Observable.of(false));
  }

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
