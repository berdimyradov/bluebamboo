import { TestBed, inject } from '@angular/core/testing';

import { InsuranceConfigurationService } from './insurance-configuration.service';

describe('InsuranceConfigurationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InsuranceConfigurationService]
    });
  });

  it('should be created', inject([InsuranceConfigurationService], (service: InsuranceConfigurationService) => {
    expect(service).toBeTruthy();
  }));
});
