import { TestBed, inject } from '@angular/core/testing';

import { ServiceConfigurationService } from './service-configuration.service';

describe('ServiceConfigurationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServiceConfigurationService]
    });
  });

  it('should be created', inject([ServiceConfigurationService], (service: ServiceConfigurationService) => {
    expect(service).toBeTruthy();
  }));
});
