import { TestBed, inject } from '@angular/core/testing';

import { LocationConfigurationService } from './location-configuration.service';

describe('LocationConfigurationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocationConfigurationService]
    });
  });

  it('should be created', inject([LocationConfigurationService], (service: LocationConfigurationService) => {
    expect(service).toBeTruthy();
  }));
});
