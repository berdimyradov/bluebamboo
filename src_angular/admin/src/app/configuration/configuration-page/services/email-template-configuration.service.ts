import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { EmailTemplate, EmailTemplateRole } from '../types';

@Injectable()
export class EmailTemplateConfigurationService {
  constructor(
    private http: Http
  ) {}

  public getRoleList(): Observable<EmailTemplateRole[]> {
    return this.http.get('/api/v1/mailtemplates/roles')
      .map((res: Response) => res.json().map((it: any) => new EmailTemplateRole(it)))
      .catch(this.handleError);
  }

  public getTemplateByRoleId(id: number): Observable<EmailTemplate> {
    return this.http.get(`/api/v1/mailtemplates/role/${id}`)
      .map((res: Response) => new EmailTemplate(res.json()))
      .catch(this.handleError);
  }

  public saveTemplate(template: any): Observable<void> {
    return this.http.put('/api/v1/mailtemplates/templates/' + template.id, template)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    let errMsg: string;

    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);

      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    console.error(errMsg);

    return Observable.throw(errMsg);
  }
}
