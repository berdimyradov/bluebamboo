import { Injectable } from '@angular/core';
import {CommonMenuItem} from "../../../common";
import {Observable} from "rxjs/Observable";

@Injectable()
export class ServiceConfigurationService {
  private _modules: CommonMenuItem[] = [
    new CommonMenuItem().parse({
      code: 'add',
      icon: 'fa-plus',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'list',
      icon: 'fa-shopping-basket',
      implement: true
    })
  ];

  constructor() { }

  public getListModules(): Observable<CommonMenuItem[]> {
    return (new Observable<CommonMenuItem[]>(observable => observable.next(this._modules)));
  }
}
