import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { Organization } from "../../../common";

@Injectable()
export class OrganizationConfigurationService {

  constructor(private http: Http) { }

  public get(): Observable<Organization> {
    return this.http.get('/api/v1/org/organization/')
      .map((res: Response) => new Organization().parse(res.json()))
      .catch(this.handleError);
  }

  public save(organization: Organization): Observable<Organization> {
    return this.http.put('/api/v1/org/organization/'+organization.id, organization)
      .map((res: Response) => new Organization().parse(res.json()))
      .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
