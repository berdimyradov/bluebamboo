import { TestBed, inject } from '@angular/core/testing';

import { CustomerConfigurationService } from './customer-configuration.service';

describe('CustomerConfigurationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerConfigurationService]
    });
  });

  it('should be created', inject([CustomerConfigurationService], (service: CustomerConfigurationService) => {
    expect(service).toBeTruthy();
  }));
});
