import { SelectOption } from '@bluebamboo/common-ui';
import { Injectable } from '@angular/core';

@Injectable()
export class ReportTemplateVarsService {
  private data: SelectOption[] = [];
  private stab;
  private groups = [];
  private vars = [];

  public init(templateVars): void {
    this.stab = templateVars;
    this.data['groupNames'] = [];
    this.groups['Global'] = [];
    this.groups['dynamicTable'] = [];
    this.stab.forEach((group) => {
      if (group.type === 'Global') {
        this.groups['Global'].push(
          new SelectOption({ code: group.id, title: group.groupname }));
      } else {
        this.groups['dynamicTable'].push(
          new SelectOption({ code: group.id, title: group.groupname }));
      }
    });

  }

  public getElementsOfGroup(name) {
    const group = this.stab.filter((g) => g.id === name);
    const elements = [];
    group[0].elements.forEach((el) => {
      elements.push(new SelectOption({ code: el.value, title: el.title }));
    });
    return elements;
  }

  public getGroupsByElement(el) {
    let result;
    if (el.type === 'dynamicTable') {
      const newGroup = this.groups['dynamicTable'].filter(g => g.code === el.rowcontent);
      result = [...this.groups['Global'], ...newGroup];
    } else {
      result = this.groups['Global'];
    }
    return result;
  }

  public getTemplateVar(mode, groupName, property): any {
    const group = this.stab.find((g) => g.id === groupName);
    const templateVar = group.elements.find((el) => el.value === property);
    return templateVar;
  }

}
