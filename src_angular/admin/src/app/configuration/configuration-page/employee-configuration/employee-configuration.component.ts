import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {routeAnimationOpacity, CommonPageComponent, CommonListItem} from "../../../common";
import {Observable} from "rxjs/Observable";
import {ActivatedRoute, Router} from "@angular/router";
import {EmployeeService} from "../../../common/services/employee.service";

@Component({
  selector: 'admin-employee-configuration',
  templateUrl: './employee-configuration.component.html',
  styleUrls: ['./employee-configuration.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class EmployeeConfigurationComponent implements OnInit {
  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private employee: EmployeeService
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.EMPLOYEE-CONFIGURATION.TITLE';
  }

  public getList = ():Observable<CommonListItem[]> => {
    return new Observable<CommonListItem[]>(observer => {
      this.employee.getList().subscribe(
        result => {
          observer.next(result.map(it => {
            const t: CommonListItem = new CommonListItem();

            t.id = it.id;
            t.title = `${it.firstname} ${it.name}`;

            return t;
          }));

          observer.complete();
        }
      );
    });
  };

  public add() {
    this.router.navigate(['./add'], {relativeTo: this.activeRoute});
  }

  public edit(employee: CommonListItem) {
    this.router.navigate(['./' + employee.id], {relativeTo: this.activeRoute});
  }
}
