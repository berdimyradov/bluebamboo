import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {CommonPageComponent,DialogService,EmployeeService, Location, routeAnimationOpacity} from "../../../../common";
import {ActivatedRoute, Router} from "@angular/router";
import {Product} from "../../types";
import {UploadService} from '@bluebamboo/common-ui';

@Component({
  selector: 'admin-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class EmployeeFormComponent implements OnInit {
  private locationsNormalize = {};
  private productsNormalize = {};
  public photo: any;
  public employee: any;
  public products: Product[] = [];
  public locations: Location[] = [];
  public selectedProducts: any[] = [];
  public selectedLocations: any[] = [];
  public isView: boolean;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private employeeService: EmployeeService,
    private dialog: DialogService,
    private uploadService: UploadService
  ) { }

  ngOnInit() {
    this.products = this.activeRoute.snapshot.data.products;
    this.locations = this.activeRoute.snapshot.data.locations;
    this.employee = this.activeRoute.snapshot.data.employee;
    this.commonPage.title = 'PAGE.EMPLOYEE-FORM.TITLE.' + (this.employee.id ? 'EDIT' : 'NEW');
    this.isView = !!this.employee.id;

    if (this.employee.gender === false){
      this.employee.gender = 'female'
    }else if (this.employee.gender === true){
      this.employee.gender = 'male'
    }

    this.selectedLocations = this.employee.locations.map(it => {
      return {id: it.location_id};
    });
    this.selectedProducts = this.employee.products.map(it => {
      return {id: it.product_id};
    });
    this.locationsNormalize = this.activeRoute.snapshot.data.locations.reduce((result: any, cur: Location) => {
      result[cur.id] = cur;

      return result;
    }, {});
    this.productsNormalize = this.activeRoute.snapshot.data.products.reduce((result: any, cur: Product) => {
      result[cur.id] = cur;

      return result;
    }, {});
  }

  public back() {
    if (this.isView || !this.employee.id) {
      this.router.navigate(['../'], {relativeTo: this.activeRoute});
    } else {
      if (this.employee.id) {
        this.isView = true;
      }
    }
  }

  public save() {
    if (this.employee.gender === 'male'){
      this.employee.gender = true
    } else if (this.employee.gender === 'female'){
      this.employee.gender = false
    } else{
      this.employee.gender = null
    }
    this.employee.locations = this.selectedLocations.map(
      it => {
        return {location_id: it.id, employee_id: this.employee.id}
      });
    this.employee.products = this.selectedProducts.map(
      it => {
        return {product_id: it.id, employee_id: this.employee.id}
      });

    if (this.photo) {
      this.uploadService.save(this.photo.file)
        .subscribe((result: any) => {
          this.photo = null;
          this.employee.foto = result.filename;

          this.saveEmp();
        });
    } else {
      this.saveEmp();
    }
  }

  private saveEmp() {
    this.employeeService.save(this.employee)
      .subscribe(
        () => this.back()
      );
  }

  public onPhotoChange($event) {
    this.photo = $event;
  }

  public delete() {
    this.dialog.confirm('PAGE.EMPLOYEE-FORM.CONFIRM-DELETE','PAGE.EMPLOYEE-FORM.CONFIRM-YES','PAGE.EMPLOYEE-FORM.CONFIRM-NO')
      .then((res: Boolean) => {
        if (res) {
          this.employeeService.delete(this.employee).subscribe(
            result => this.back()
          );
        }
      });
  }

  public get genderText() {
    if (this.employee.gender === 'male') {
      return 'PAGE.EMPLOYEE-FORM.FORM.GENDER-MAN';
    } else if (this.employee.gender === 'female') {
      return 'PAGE.EMPLOYEE-FORM.FORM.GENDER-WOMAN';
    }

    return '';
  }

  public get birthdayText() {
    const { birth_day, birth_month, birth_year } = this.employee;
    const result = [];

    if (birth_day && birth_month) {
      result.push(birth_day, birth_month);

      if (birth_year) {
        result.push(birth_year);
      }
    }

    return result.join('.');
  }

  public get viewLocations() {
    return this.selectedLocations.map(item => this.locationsNormalize[item.id]);
  }

  public get viewProducts() {
    return this.selectedProducts.map(item => this.productsNormalize[item.id]);
  }
}
