import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from "@angular/router";
import {ImportRow} from "../../../invoice/invoice-page/types/import-row";
import {CommonPageComponent} from "../../../common/common-page/common-page.component";
import {TranslateModule} from "@ngx-translate/core";
import {ImportService} from "../../services/import.service";

@Component({
  selector: 'admin-import-data-configuration',
  templateUrl: './import-data-configuration.component.html',
  styleUrls: ['./import-data-configuration.component.scss']
})
export class ImportDataConfigurationComponent implements OnInit {
  public isLoading: boolean;
  public list: ImportRow[] = [];
  public gridModules: any[] = [TranslateModule];

  constructor(private commonPage: CommonPageComponent,
              private _location: Location,
              private router: Router,
              private activeRoute: ActivatedRoute,
              private importsService: ImportService) {

  }

  ngOnInit() {
    this.commonPage.title = 'PAGE.INVOICE-PAID.TITLE';
    this.refresh();
  }

  private refresh() {
    this.isLoading = true;

    this.importsService.getGridData()
      .subscribe(
        result => {
          console.dir(result);
          // result.forEach((item: InvoiceCamtRow) => this.initRow(item));
          this.list = result;
          // this.startSearch(this._search, 0);
        },
        error => {
          console.error(error);
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  goToImportFilePage() {
    this.router.navigate(['./import'], {relativeTo: this.activeRoute});
  }

  back() {
    this._location.back();
  }
}
