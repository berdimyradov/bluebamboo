import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ImportService} from "../../../services/import.service";
import {ImportRow} from "../../../../invoice/invoice-page/types/import-row";
import {RecordRow} from "../../../models/record";
import {ModalWindowComponent} from "../../../../common/modal-window/modal-window.component";
import {UiGridComponent} from "../../../../common/grid/ui-grid.component";
import {UtilityService} from "../../../../common/services/utility/utility.service";
import {SnackbarComponent} from "../../../../common/snackbar/snackbar.component";

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditPageComponent implements OnInit {
  @ViewChild(ModalWindowComponent) public popup: ModalWindowComponent;
  private importItem: ImportRow;
  public isImportAll = false;
  public hasHeader = false;
  public recordsKey = [
    'col01', 'col02', 'col03', 'col04', 'col05', 'col06', 'col07', 'col08', 'col09', 'col10',
    'col11', 'col12', 'col13', 'col14', 'col15', 'col16', 'col17', 'col18', 'col19', 'col20',
  ];
  public recordsMappedKeys: string[] = [];
  private recordRowCached: RecordRow;

  @ViewChild(UiGridComponent) public UiGridComponent: UiGridComponent;
  public nickNamesForRows: Array<{
    colName: string,
    displayName: string,
    sortable: boolean
  }>;
  @ViewChild(SnackbarComponent) public snackBar: SnackbarComponent;
  public snackState = {
    autoHide: {hide: true, delay: 3000},
    display: false,
    message: '',
    header: '',
    temper: 'sucess',
    eventData: {type: ''}
  };

  constructor(public router: Router,
              private activeRoute: ActivatedRoute,
              private importService: ImportService) {
  }

  ngOnInit() {
    this.nickNamesForRows = [
      // {colName: '', displayName: 'Actions', sortable: false},
      {colName: 'id', displayName: 'ID', sortable: true},
      {colName: 'import', displayName: 'Import', sortable: true},
      {colName: 'imported', displayName: 'Imported', sortable: true},
    ];

    let importId = parseInt(this.activeRoute.snapshot.paramMap.get('importId'));

    this.getImport(importId);
  }

  private getImport(importId: number): void {

    this.importService.getImportRow(importId)
      .subscribe(
        result => {
          this.importItem = result;
          this.retrieveMappedColumnsFromImport();
          this.loadGridWithData();
        },
        error => {
          console.error(error);
        }
      );
  }

  public importToCustomer() {
    const data = {
      import_id: this.importItem.id,
      record_ids: []
    };

    this.importItem.records
      .filter((record: RecordRow) => (record.import == true))
      .forEach((record: RecordRow) => data.record_ids.push(record.id));

    this.importService.importRecordsToCustomer(data)
      .subscribe(
        customers => {
          data.record_ids.forEach((recordId: number) => {
            const importedRecord = this.importItem.records.find((record: RecordRow) => record.id === recordId);
            importedRecord.import = false;
            importedRecord.imported = true;
            importedRecord.duplication = true;

            this.updateRecordRowData(importedRecord);

            this.snackState.header = 'Import if finished';
            this.snackState.message = 'Record is successfully imported';
            this.snackState.temper = 'sucess';
            this.snackBar.displaySnackBar();
          });
        },
        error => UtilityService.handleError(error)
      );
  }

  public onHasHeaderChange(event: Event) {
    this.hasHeader = !this.hasHeader;

    if (this.hasHeader) {
      this.recordRowCached = this.importItem.records[0];
      this.importItem.records.shift();

      this.UiGridComponent.allData.splice(this.UiGridComponent.allData.findIndex(data => data.id === this.recordRowCached.id), 1);
      this.UiGridComponent.reloadChunk();
    } else if (this.recordRowCached) {
      this.importItem.records.unshift(this.recordRowCached);

      this.UiGridComponent.allData.unshift(this.mapRecordToUiGridDatum(this.recordRowCached));
      this.UiGridComponent.reloadChunk();
    }
  }

  public onImportAllChange(event: Event) {
    this.isImportAll = !this.isImportAll;

    this.importItem.records
      .filter(record => record.duplication == false)
      .forEach(record => {
        record.import = this.isImportAll;
        this.updateRecordRowData(record);
      });
  }

  public onRecordImportChange(data) {
    data.import = !data.import;
    this.importItem.records.find(record => record.id === data.id).import = data.import;
  }

  private retrieveMappedColumnsFromImport() {
    this.recordsKey.forEach(key => {
      if (this.importItem[key + '_target_field']) {
        this.recordsMappedKeys.push(key);
      }
    })
  }

  private loadGridWithData(): void {
    // Populate nickNamesForRows with column keys
    this.recordsMappedKeys.forEach(key => this.nickNamesForRows.push({
      colName: key,
      displayName: this.importItem.target.fields.find(field => field.id === this.importItem[key + "_target_field"]).label,
      sortable: true
    }));

    // map records to ui grid data -> Load ui-grid with data
    const loadData = this.importItem.records.map(record => this.mapRecordToUiGridDatum(record));
    console.log('loaddata', loadData);
    this.UiGridComponent.LoadData(loadData);
  }

  private mapRecordToUiGridDatum(record: RecordRow) {
    const mappedObj = {
      id: record.id,
      import: (record.import == true),
      imported: (record.imported == true),
      duplication: (record.duplication == true)
    };
    this.recordsMappedKeys.forEach(key => mappedObj[key] = record[key]);

    return mappedObj;
  }

  private updateRecordRowData(record: RecordRow) {
    const recordRowData = this.UiGridComponent.allData.find(r => r.id === record.id);
    recordRowData.import = (record.import == true);
    recordRowData.imported = (record.imported == true);
    recordRowData.duplication = (record.duplication == true);
    this.recordsMappedKeys.forEach(key => recordRowData[key] = record[key]);
  }
}
