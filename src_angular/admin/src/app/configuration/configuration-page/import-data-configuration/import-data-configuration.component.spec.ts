import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportDataConfigurationComponent } from './import-data-configuration.component';

describe('ImportDataConfigurationComponent', () => {
  let component: ImportDataConfigurationComponent;
  let fixture: ComponentFixture<ImportDataConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportDataConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportDataConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
