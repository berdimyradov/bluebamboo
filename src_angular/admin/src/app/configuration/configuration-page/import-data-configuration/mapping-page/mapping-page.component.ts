import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TargetRow} from "../../../models/target";
import {TargetService} from "../../../services/target.service";
import {ImportService} from "../../../services/import.service";
import {ImportRow} from "../../../../invoice/invoice-page/types/import-row";
import {Field} from '../../../models/field';
import {RecordService} from "app/configuration/services/record.service";
import {SelectOption} from '@bluebamboo/common-ui';
import {UtilityService} from '../../../../common/services/utility/utility.service';
import {SnackbarComponent} from '../../../../common/snackbar/snackbar.component';

@Component({
  selector: 'app-mapping-page',
  templateUrl: './mapping-page.component.html',
  styleUrls: ['./mapping-page.component.scss']
})
export class MappingPageComponent implements OnInit {
  private target: TargetRow;
  private importItem: ImportRow;
  public recordsKey = [
    'col01', 'col02', 'col03', 'col04', 'col05', 'col06', 'col07', 'col08', 'col09', 'col10',
    'col11', 'col12', 'col13', 'col14', 'col15', 'col16', 'col17', 'col18', 'col19', 'col20',
  ];
  public selectOptions: SelectOption[] = [
    new SelectOption({"title": "None", "code": null})
  ];
  @ViewChild(SnackbarComponent) public snackBar: SnackbarComponent;
  public snackState = {
    autoHide: {hide: true, delay: 3000},
    display: false,
    message: '',
    header: '',
    temper: 'danger',
    eventData: {type: ''}
  };

  constructor(public router: Router,
              private activeRoute: ActivatedRoute,
              private targetService: TargetService,
              private importService: ImportService,
              private recordService: RecordService) {
  }

  ngOnInit() {
    let importId = parseInt(this.activeRoute.snapshot.paramMap.get('importId'));
    this.getImport(importId);
  }

  public getImport(importId: number): void {
    this.importService.getImportRow(importId)
      .subscribe(
        result => {
          this.importItem = result;
          this.selectOptions.push(...this.mapFieldsToSelectOptions(this.importItem.target.fields));
        },
        error => {
          console.error(error);
        }
      );
  }

  public saveMappings() {
    try {
      this.recordsKey.forEach(key => this.importItem[key + '_target_field'] = parseInt(this.importItem[key + '_target_field']) || null);
      this.validateFieldMapping();

      this.importService.saveImport(this.importItem)
        .finally(() => console.log('import is saved'))
        .subscribe(
          response => {
            console.log('success', response);
            this.router.navigate(['../../edit/' + this.importItem.id], {relativeTo: this.activeRoute})
          },
          error => UtilityService.handleError(error)
        );
    } catch (error) {
      this.snackState.header = 'Error!';
      this.snackState.message = error.message;
      this.snackBar.displaySnackBar();
    }
  }

  public getTarget(targetId: number): void {
    this.targetService.getTargetRow(targetId)
      .subscribe(
        result => {
          this.target = result;
        },
        error => {
          console.error(error);
        }
      );
  }

  private mapFieldsToSelectOptions(fields: Field[]): SelectOption[] {
    return fields.map(field => new SelectOption({"title": field.label, "code": field.id}));
  }

  private validateFieldMapping() {
    console.dir(this.importItem);

    // Check if `Firstname` and `Lastname` are mapped
    let isFirstNameMapped = false;
    let isLastNameMapped = false;
    this.recordsKey.forEach(key => {
      if (this.importItem[key + '_target_field'] === 1) {     // 1 -> id of field with label Firstname
        isFirstNameMapped = true;
      } else if (this.importItem[key + '_target_field'] === 2) {    // 2 -> id of field with label Firstname
        isLastNameMapped = true;
      }
    });
    if (!(isFirstNameMapped && isLastNameMapped)) {
      throw new Error('Minimum required Mapped fields: Firstname, Lastname are not provided!');
    }

    // Check if there is a duplicates of mapped fields
    const targetFieldIds = [];
    const duplicates = [];
    this.recordsKey.forEach(key => {
      if (this.importItem[key + '_target_field'])
        targetFieldIds.push(this.importItem[key + '_target_field']);
    });
    duplicates.push(...this.findDuplicates(targetFieldIds));
    if (duplicates.length) {
      const duplicatedFields = duplicates.map(id => this.importItem.target.fields.find(field => field.id === id));
      let duplicatedFieldsStr;
      duplicatedFields.forEach((field, index) => {
        if (index === 0) {
          duplicatedFieldsStr = field.label;
        } else {
          duplicatedFieldsStr += ', ' + field.label;
        }
      });
      throw new Error(`Duplicates of [${duplicatedFieldsStr}] fields-mapping is detected! Each field can be mapped once!`);
    }
  }

  private findDuplicates(data) {

    let result = [];

    data.forEach(function (element, index) {

      // Find if there is a duplicate or not
      if (data.indexOf(element, index + 1) > -1) {

        // Find if the element is already in the result array or not
        if (result.indexOf(element) === -1) {
          result.push(element);
        }
      }
    });

    return result;
  }

}
