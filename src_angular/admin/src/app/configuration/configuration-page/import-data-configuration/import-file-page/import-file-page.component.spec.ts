import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportFilePageComponent } from './import-file-page.component';

describe('ImportFilePageComponent', () => {
  let component: ImportFilePageComponent;
  let fixture: ComponentFixture<ImportFilePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportFilePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportFilePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
