import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from "@angular/router";
import {SelectOption} from '@bluebamboo/common-ui';
import {DataStorageService} from "../../../../common/services/data-storage/data-storage.service";
import {TargetService} from "../../../services/target.service";
import {TargetRow} from "../../../models/target";
import {UtilityService} from "../../../../common/services/utility/utility.service";
import {ImportRow} from "../../../../invoice/invoice-page/types/import-row";

@Component({
  selector: 'admin-import-file-page',
  templateUrl: './import-file-page.component.html',
  styleUrls: ['./import-file-page.component.scss']
})
export class ImportFilePageComponent implements OnInit {
  uploadUrl: string = '/api/v1/data/upload';
  public isLoading: boolean;
  public targets: TargetRow[] = [];
  public selectedTarget = 1;
  public selectOptions: SelectOption[] = [];

  constructor(private _location: Location,
              private router: Router,
              protected activeRoute: ActivatedRoute,
              private targetService: TargetService) {
  }

  ngOnInit() {
    this.getAllTargets();
  }

  getAllTargets() {
    this.isLoading = true;

    this.targetService.getAllTargetRows()
      .finally(() => this.isLoading = false)
      .subscribe(
        result => {
          // console.dir(result);
          this.targets = result;
          this.targets.forEach(target => this.selectOptions.push(new SelectOption({
            "title": target.label,
            "code": target.id
          })));
        },
        error => console.error(error)
      );
  }

  onFileUpload(response: any) {
    const resObj = <ImportRow>JSON.parse(response);
    this.router.navigate(['../mapping/' + resObj.id], {relativeTo: this.activeRoute});
  }

  onFileUploadFail(error: any) {
    console.error(error);
    alert("Error!" + error);
  }

  public back() {
    this._location.back();
  }
}
