import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {routeAnimationOpacity, CommonPageComponent, DialogService} from "../../../../common";
import {Insurance} from '../../types';
import {ActivatedRoute, Router} from "@angular/router";
import {InsuranceConfigurationService} from "../../services";

@Component({
  selector: 'app-insurance-form',
  templateUrl: './insurance-form.component.html',
  styleUrls: ['./insurance-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class InsuranceFormComponent implements OnInit {
  public insurance: Insurance;
  public isView: boolean;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private insuranceService: InsuranceConfigurationService,
    private dialog: DialogService
  ) { }

  ngOnInit() {
    this.insurance = this.activeRoute.snapshot.data.insurance;
    this.commonPage.title = 'PAGE.INSURANCE-FORM.TITLE.' + (this.insurance.id ? 'EDIT' : 'NEW');
    this.isView = !!this.insurance.id;
  }

  public back(force = false) {
    if (force || this.isView || !this.insurance.id) {
      this.router.navigate(['../'], {relativeTo: this.activeRoute});
    } else {
      if (this.insurance.id) {
        this.isView = true;
      }
    }
  }

  public save() {
    this.insuranceService.save(this.insurance)
      .subscribe(
        result => this.back()
      );
  }

  public delete() {
    this.dialog.confirm('PAGE.INSURANCE-FORM.CONFIRM-DELETE','PAGE.INSURANCE-FORM.CONFIRM-YES','PAGE.INSURANCE-FORM.CONFIRM-NO')
      .then((res: Boolean) => {
        if (res) {
          this.insuranceService.delete(this.insurance).subscribe(
            result => this.back(true)
          );
        }
      });
  }
}
