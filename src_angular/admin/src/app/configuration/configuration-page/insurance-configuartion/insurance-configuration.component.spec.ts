import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsuranceConfiguartionComponent } from './insurance-configuration.component';

describe('InsuranceConfiguartionComponent', () => {
  let component: InsuranceConfiguartionComponent;
  let fixture: ComponentFixture<InsuranceConfiguartionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsuranceConfiguartionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuranceConfiguartionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
