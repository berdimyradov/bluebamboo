import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {InsuranceConfigurationService} from "../services";
import {ActivatedRoute, Router} from "@angular/router";
import {CommonPageComponent} from "app/common";
import {CommonListItem} from "../../../common";
import {Observable} from "rxjs/Observable";
import {Insurance} from '../types/insurance';

@Component({
  selector: 'app-insurance-configuration',
  templateUrl: './insurance-configuration.component.html',
  styleUrls: ['./insurance-configuration.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InsuranceConfigurationComponent implements OnInit {

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private insuranceService: InsuranceConfigurationService
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.INSURANCE-CONFIGURATION.TITLE';
  }

  public getList = ():Observable<CommonListItem[]> => {
    return new Observable<CommonListItem[]>(observer => {
      this.insuranceService.getList().subscribe(
        result => {
          observer.next(result.map((it: Insurance) => {
            let item: CommonListItem = new CommonListItem();
            item.id = it.id;
            item.title = it.name;
            return item;
          }));
          observer.complete();
        }
      )
    });
  };

  public edit(insurance: CommonListItem) {
    this.router.navigate(['./' + insurance.id], {relativeTo: this.activeRoute});
  }

  public add() {
    this.router.navigate(['./add'], { relativeTo: this.activeRoute });
  }
}
