import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationConfigurationnComponent } from './organization-form.component';

describe('OrganizationConfigurationnComponent', () => {
  let component: OrganizationConfigurationnComponent;
  let fixture: ComponentFixture<OrganizationConfigurationnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizationConfigurationnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationConfigurationnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
