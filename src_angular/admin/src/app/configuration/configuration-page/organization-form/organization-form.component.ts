import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { OrganizationConfigurationService } from '../services';
import { CommonPageComponent, Organization } from '../../../common';
import { ActivatedRoute, Router } from '@angular/router';
import { routeAnimationOpacity } from '../../../common/animations/route-animation-opacity';
import { UploadService } from '@bluebamboo/common-ui';

@Component({
  selector: 'admin-organization-configuration',
  templateUrl: './organization-form.component.html',
  styleUrls: ['./organization-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class OrganizationFormComponent implements OnInit {
  public organization: Organization;
  public isMWSTRequired: boolean;
  public logo: any;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private organizationService: OrganizationConfigurationService,
    private uploadService: UploadService
  ) { }

  ngOnInit() {
    this.organization = this.activeRoute.snapshot.data.organization;
    this.commonPage.title = 'PAGE.ORGANIZATION-FORM.TITLE';
    this.isMWSTRequired = !!Number(this.organization.mwst);
  }

  public back() {
    this.router.navigate(['../'], { relativeTo: this.activeRoute });
  }

  public save() {
    if (this.logo) {
      this.uploadService.save(this.logo.file)
        .subscribe((result: any) => {
          this.logo = null;
          this.organization.logo = result.filename;

          this.saveOrg();
        });
    } else {
      this.saveOrg();
    }
  }

  private saveOrg() {
    this.organizationService.save(this.organization)
      .subscribe(
        () => this.back()
      );
  }

  public onLogoChange($event) {
    this.logo = $event;
  }

  public onMWSTChanged(e) {
    this.isMWSTRequired = Boolean(e.value);
    this.organization.mwst = e.value;
  }
}


