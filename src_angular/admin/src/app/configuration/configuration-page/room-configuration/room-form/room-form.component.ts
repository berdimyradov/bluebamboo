import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {routeAnimationOpacity, CommonPageComponent, DialogService, Room, Location} from "../../../../common";
import {Product} from '../../types';
import {RoomConfigurationService} from "../../services";
import {SelectOption} from '@bluebamboo/common-ui';

@Component({
  selector: 'admin-room-form',
  templateUrl: './room-form.component.html',
  styleUrls: ['./room-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class RoomFormComponent implements OnInit {
  public room: Room;
  public productList: Product[];
  public locationList: SelectOption[];
  public isView: boolean;
  public locationTitle = '';

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private roomService: RoomConfigurationService,
    private dialog: DialogService
  ) { }

  ngOnInit() {
    this.room = this.activeRoute.snapshot.data.room;
    this.productList = this.activeRoute.snapshot.data.productList;
    const locationList = [];
    this.activeRoute.snapshot.data.locationList.forEach((loc: Location) => locationList.push(new SelectOption({
      code: loc.id.toString(),
      title: loc.title
    })));
    this.locationList = locationList;
    if (this.locationList.length === 1) {
      this.room.location_id = +this.locationList[0].code;
    }
    this.commonPage.title = 'PAGE.ROOM-FORM.TITLE.' + (this.room.id ? "EDIT" : "NEW");
    this.isView = !!this.room.id;

    if (this.room.location_id) {
      this.locationTitle = this.activeRoute.snapshot.data.locationList
        .find((item: Location) => item.id === this.room.location_id).title;
    }
  }

  public back(force = false) {
    if (force || this.isView || !this.room.id) {
      this.router.navigate(['../'], {relativeTo: this.activeRoute});
    } else {
      if (this.room.id) {
        this.isView = true;
      }
    }
  }

  public save() {
    this.roomService.save(this.room)
      .subscribe(
        result => this.back()
      );
  }

  public delete() {
    this.dialog.confirm('PAGE.ROOM-FORM.CONFIRM-DELETE','PAGE.ROOM-FORM.CONFIRM-YES','PAGE.ROOM-FORM.CONFIRM-NO')
      .then((res: Boolean) => {
        if (res) {
          this.roomService.delete(this.room).subscribe(
            result => this.back(true)
          );
        }
      });
  }
}
