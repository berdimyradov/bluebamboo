import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomConfigurationComponent } from './room-configuration.component';

describe('RoomConfigurationComponent', () => {
  let component: RoomConfigurationComponent;
  let fixture: ComponentFixture<RoomConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
