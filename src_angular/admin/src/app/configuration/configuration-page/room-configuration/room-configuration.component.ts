import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {CommonPageComponent, routeAnimationOpacity, CommonListItem} from '../../../common';
import {ActivatedRoute, Router} from '@angular/router';
import {RoomConfigurationService} from '../services';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'admin-room-configuration',
  templateUrl: './room-configuration.component.html',
  styleUrls: ['./room-configuration.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class RoomConfigurationComponent implements OnInit {
  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private roomConfiguration: RoomConfigurationService
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.ROOM-CONFIGURATION.TITLE';
  }

  public getList = ():Observable<CommonListItem[]> => {
    return new Observable<CommonListItem[]>(observer => {
      this.roomConfiguration.getList().subscribe(
        result => {
          observer.next(result);
          observer.complete();
        }
      );
    });
  };

  public edit(room: CommonListItem) {
    this.router.navigate(['./' + room.id], {relativeTo: this.activeRoute});
  }

  public add() {
    this.router.navigate(['./add'], { relativeTo: this.activeRoute });
  }
}
