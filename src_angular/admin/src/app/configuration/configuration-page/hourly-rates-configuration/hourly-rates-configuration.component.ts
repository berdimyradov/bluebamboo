import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectOption } from '@bluebamboo/common-ui';
import { routeAnimationOpacity, CommonPageComponent } from "../../../common";
import { Insurance } from "../types/insurance";
import { InsuranceConfigurationService } from '../services/insurance-configuration.service';
import { HourlyRate } from "../types/hourly-rate";
import { TariffPositions } from "../types/tariff-positions";

@Component({
  selector: 'hourly-rates-configuration',
  templateUrl: './hourly-rates-configuration.component.html',
  styleUrls: ['./hourly-rates-configuration.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class HourlyRatesConfigurationComponent implements OnInit {
  public insuranceList: SelectOption[] = [];
  public insuranceId: string;
  public hourlyRates: HourlyRate[] = [];
  public tariffPositionsList: TariffPositions[] = [];
  public enableHourlyRates: boolean;
  public useTheSameHourlyRate: boolean;
  private cache = {};

  constructor(
    private commonPage: CommonPageComponent,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private insuranceConfigurationService: InsuranceConfigurationService
  ) { }

  ngOnInit() {
    this.insuranceList = this.activeRoute.snapshot.data.insuranceList.map((item: Insurance) => new SelectOption({
      code: item.id.toString(),
      title: item.name
    }));
    this.tariffPositionsList = this.activeRoute.snapshot.data.tariffPositionsList.map(item => {
      const t = new SelectOption();

      t.code = item.id;
      t.title = `${item.nr} - ${item.title}`;

      return t;
    });

    this.insuranceId = this.insuranceList.length ? this.insuranceList[0].code : null;
    this.commonPage.title = 'PAGE.HOURLY-RATES-CONFIGURATION.TITLE';

    this.getHourlyRates();
  }

  public getHourlyRates() {
    if (!this.insuranceId) {
      return;
    }

    this.insuranceConfigurationService.getHourlyRates(Number(this.insuranceId))
      .subscribe(
        result => this.setHourlyRates(result)
      );
  }

  public onInsuranceChange($event) {
    this.insuranceId = $event;
    this.cache = {};

    this.getHourlyRates();
  }

  public onUseTheSameHourlyRateChange($event) {
    if (this.useTheSameHourlyRate !== $event) {
      this.useTheSameHourlyRate = $event;
      this.cache[Number(!this.useTheSameHourlyRate)] = this.hourlyRates;
    }

    const index = Number(this.useTheSameHourlyRate);

    if (this.cache[index]) {
      this.hourlyRates = [ ...this.cache[index] ];
    } else {
      this.hourlyRates = [this.newItem()];
    }
  }

  public onEnableHourlyRatesChange($event) {
    this.enableHourlyRates = $event;

    if (this.enableHourlyRates) {
      this.onUseTheSameHourlyRateChange(this.useTheSameHourlyRate);
    } else {
      this.cache[Number(this.useTheSameHourlyRate)] = this.hourlyRates;
      this.hourlyRates = [];
    }
  }

  public addPart() {
    this.hourlyRates.push(this.newItem());
  }

  public removePart(pos: number) {
    this.hourlyRates.splice(pos, 1);
  }

  private setHourlyRates(data) {
    this.hourlyRates = data;
    this.enableHourlyRates = !!this.hourlyRates.length;

    if (this.hourlyRates.length > 1) {
      this.useTheSameHourlyRate = false;
    } else if (this.hourlyRates.length === 1) {
      this.useTheSameHourlyRate = !this.hourlyRates[0].tariffposition_id;
    } else {
      this.useTheSameHourlyRate = true;
    }

    if (this.enableHourlyRates) {
      this.cache[Number(this.useTheSameHourlyRate)] = this.hourlyRates;
    }
  }

  public back() {
    this.router.navigate(['../'], { relativeTo: this.activeRoute });
  }

  public get disableSave() {
    return this.enableHourlyRates &&
      (
        this.useTheSameHourlyRate && !this.hourlyRates[0].max_pph ||
        !this.useTheSameHourlyRate &&
          this.hourlyRates.some((item: HourlyRate) => !item.max_pph || !item.tariffposition_id)
      );
  }

  public save() {
   this.insuranceConfigurationService.saveHourlyRates(Number(this.insuranceId), this.hourlyRates)
      .subscribe(
        result => {
          this.cache = {};
        }
      );
  }

  private newItem() {
    return new HourlyRate({
      insurance_id: this.insuranceId,
    });
  }
}
