import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HourlyRatesConfigurationComponent } from './hourly-rates-configuration.component';

describe('HourlyRatesConfigurationComponent', () => {
  let component: HourlyRatesConfigurationComponent;
  let fixture: ComponentFixture<HourlyRatesConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HourlyRatesConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HourlyRatesConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
