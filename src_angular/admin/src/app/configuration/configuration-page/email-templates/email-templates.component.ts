import { SnackbarComponent } from '../../../common/snackbar/snackbar.component';
import { SnackState } from '../../../common/snackbar/types/snack';
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { SelectOption } from '@bluebamboo/common-ui';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonPageComponent } from '../../../common';
import { EmailTemplateRole, EmailTemplate } from '../types';
import { EmailTemplateConfigurationService } from '../services';

const BUTTONS = ['bold', 'italic', 'formatBlock', 'templateVarsDropdown'];

@Component({
  selector: 'app-email-templates',
  templateUrl: './email-templates.component.html',
  styleUrls: ['./email-templates.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EmailTemplatesComponent implements OnInit {
  config: Object = {
    charCounterCount: false,
    toolbarButtons: BUTTONS,
    toolbarButtonsMD: BUTTONS,
    toolbarButtonsSM: BUTTONS,
    toolbarButtonsXS: BUTTONS,
    getOptions: () => this.options
  };
  public roleId: string;
  public emailTemplatesOptions: SelectOption[] = [];
  public template: EmailTemplate;
  public snackState = new SnackState({hide: true, delay: 3000},
    false,
    'Email template',
    'Email template is saved');

    @ViewChild(SnackbarComponent)
    snackBar: SnackbarComponent;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private emailTemplateConfigurationService: EmailTemplateConfigurationService
  ) {}

  ngOnInit() {
    this.commonPage.title = 'PAGE.EMAIL-TEMPLATES-CONFIGURATION.TITLE';
    this.emailTemplatesOptions = this.activeRoute.snapshot.data.emailTemplates
      .map((item: EmailTemplateRole) => new SelectOption({ code: item.id, title: item.title }));

  }

  public onRoleChange(id: number) {
    this.emailTemplateConfigurationService.getTemplateByRoleId(id).subscribe(
      result => {
        this.template = result;
      }
    )
  }

  public onEditorChange(text) {
    this.template.template = text;
  }

  public reset() {
    const { template } = this.template.templatebase;

    this.template.template = template;
    this.template.subject = this.template.templatebase.subject;
  }

  public save() {
    const { template, role_id, id, subject } = this.template;
    this.emailTemplateConfigurationService.saveTemplate({ template, role_id, id, subject }).subscribe(
      result => {
         this.snackBar.displaySnackBar();
      }
    )
  }

  public get options() {
    return this.template ? this.template.vars : [];
  }

  public get editorText(): string {
    return this.template ? this.template.template : '';
  }

  public back() {
    this.router.navigate(['../'], { relativeTo: this.activeRoute });
  }
}
