/**
    * Created by aleksay on 15.05.2017.
    * Project: tmp_booking
    */
import {ReportElement} from "./report-element";
import {ReportSchema} from "./report-schema";
import {ElementRef} from "@angular/core";
import {ReportElementPropertyEditor} from "./report-element-property-editor";
import {ReportFontStyle} from "./report-fontstyle";
import {ReportBorderStyle} from "./report-borderstyle";

export class ReportElementString extends ReportElement{
    public static typeElement = "string";
    public static editor: ReportElementPropertyEditor = new ReportElementPropertyEditor(["font", "location", "location.width", "common", "common.content", "border"]);

    public content: string;
    public fontstyle: ReportFontStyle;
    public width: number;
    public textalign: string;
    public borderstyle: ReportBorderStyle;

    constructor(reportSchema: ReportSchema){
        super(reportSchema);
        this.type = "string";
    }

    public init(): ReportElementString {
        this.parse({
            location: {
                position: "fixed",
                x: 10,
                y: 10
            },
            content: "Text",
            fontstyle: "default",
            width: 0,
            textalign: "left",
        });
        this.borderstyle = new ReportBorderStyle(this.reportSchema);
        this.borderstyle.width = 0;
        this.borderstyle.color = "#000000";
        return this;
    }

    public parse(data:any):ReportElementString{
        super.parse(data);
        this.content = data.content;
        this.fontstyle = this.getFontStyle(data.fontstyle);
        this.borderstyle = this.getBorderStyle(data.borderstyle);
        this.width = Number(data.width) || 0;
        this.textalign = data.textalign;
        return this;
    }

    public draw(el:ElementRef){
        super.draw(el);
        super.drawFontStyle(this.fontstyle, this.element);
        super.drawBorderStyle(this.borderstyle, el);
        this.element.nativeElement.innerHTML = this.fillData().replace(/\n/g,"<br>");
        this.element.nativeElement.style.width = this.width ?  this.width + "mm" : undefined;
        this.element.nativeElement.style.textAlign = this.textalign || "left";
    }

    private fillData():string{
        return this.transformContent(this.content);
    }

    public json(): any {
      let res: any = super.json();
      res.content = this.content;
      res.width = Number(this.width) || undefined;
      res.textalign = this.textalign || undefined;
      res.fontstyle = this.fontstyle.json();
      res.borderstyle = this.borderstyle.json();
      return res;
    }
}
