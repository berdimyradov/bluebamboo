/**
    * Created by aleksay on 15.05.2017.
    * Project: tmp_booking
    */
import {ReportElement} from "./report-element";
import {ReportSchema} from "./report-schema";
import {ElementRef} from "@angular/core";
import {ReportElementPropertyEditor} from "./report-element-property-editor";
import {ReportBorderStyle} from "./report-borderstyle";

export class ReportElementRect extends ReportElement{
    public static typeElement = "rect";
    public static editor: ReportElementPropertyEditor = new ReportElementPropertyEditor(["border", "location", "location.width", "location.height"]);

    public borderstyle: ReportBorderStyle;
    public width: number;
    public height: number;

    constructor(reportSchema: ReportSchema){
        super(reportSchema);
        this.type = "rect";
    }

    public init(): ReportElementRect {
        this.parse({
            location: {
                position: "fixed",
                x: 10,
                y: 10
            },
            width: 20,
            height: 20
        });
        this.borderstyle = new ReportBorderStyle(this.reportSchema);
        this.borderstyle.width = 0.2;
        this.borderstyle.color = "#000000";
        return this;
    }

    public parse(data:any):ReportElementRect{
        super.parse(data);
        this.borderstyle = this.getBorderStyle(data.borderstyle);
        this.width = Number(data.width) || 0;
        this.height = Number(data.height) || 0;
        return this;
    }

    public draw(el:ElementRef){
        super.draw(el);
        super.drawBorderStyle(this.borderstyle, el);
        this.element.nativeElement.style.height = this.height + "mm";
        this.element.nativeElement.style.width = this.width + "mm";
    }

    public json(): any {
      let res: any = super.json();
      res.width = Number(this.width) || 0;
      res.height = Number(this.height) || 0;
      if(res.width == 0 && res.height == 0) { return null; }
      res.borderstyle = this.borderstyle.json();
      return res;
    }
}
