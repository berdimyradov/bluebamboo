/**
    * Created by aleksay on 30.05.2017.
    * Project: tmp_booking
    */
import {Component, forwardRef, Input}   from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {SelectOption} from '@bluebamboo/common-ui';

@Component({
    selector: 'report-designer-font-list-editor',
    templateUrl: './report-designer-font-list-editor.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ReportDesignerFontListEditorComponent),
            multi: true
        }
    ]
})
export class ReportDesignerFontListEditorComponent implements ControlValueAccessor{
    private _value: string;
    private onChange: any = () => { };
    private onTouched: any = () => { };
    // TODO: font family
    @Input('title') public title = 'Schriftart';
    public options: SelectOption[] = [];
    public get value(): string {
        return this._value;
    }
    public set value(v: string) {
        this._value = v;
        this.onChange(v);
        this.onTouched();
    }

    constructor() {
        const options = [
            {code: '', title: 'None'},
            {code: 'helvetica', title: 'Helvetica'},
            {code: 'courier', title: 'Courier'},
            {code: 'times', title: 'Times New Roman'}
        ];
        options.forEach((option) => {
            let o = new SelectOption();
            o.title = option.title;
            o.code = option.code;
            this.options.push(o);
        });
    }
    writeValue(obj: any): void {
        if (obj){
            this._value = obj.toString();
        } else {
            this.value = "";
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
    }
}
