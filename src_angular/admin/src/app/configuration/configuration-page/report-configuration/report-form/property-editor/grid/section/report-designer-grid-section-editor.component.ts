/**
    * Created by aleksay on 24.05.2017.
    * Project: tmp_booking
    */
import {Component, Input}   from '@angular/core';
import {ReportElementPropertyEditor} from "../../../types/report-element-property-editor";
import {ReportTableHeader} from "../../../types/report-table-header";

@Component({
    selector: 'report-designer-grid-section-editor',
    templateUrl: './report-designer-grid-section-editor.html'
})
export class ReportDesignerGridSectionEditorComponent{
    @Input('section') private section: ReportTableHeader;
    @Input('show') private show: ReportElementPropertyEditor;
}
