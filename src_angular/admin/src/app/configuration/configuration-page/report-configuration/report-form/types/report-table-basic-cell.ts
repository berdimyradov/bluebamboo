import {ReportSchema} from "./report-schema";
import {ReportFontStyle} from "./report-fontstyle";
import {ReportBaseElement} from "./report-base-element";
import {ReportElementDynamicTable} from "./report-element-dynamictable";

export class ReportTableBasicCell extends ReportBaseElement{
    public textalign: string;
    public fontstyle: ReportFontStyle;
    public colspan: number = 1;
    public width: number = undefined;

    constructor (reportSchema: ReportSchema, protected table: ReportElementDynamicTable, public refreshEvent: Function = () => {}){
        super(reportSchema);
    }

    public parse(data: any, fontstyle: ReportFontStyle = null): ReportTableBasicCell {
        this.textalign = data.textalign || 'right';
        this.colspan =  Number(data.colspan) || 1;
        this.fontstyle = this.reportSchema.getFontStyle(data.fontstyle) || fontstyle;
        if (!this.fontstyle){
            this.fontstyle = this.reportSchema.getFontStyle('default');
        }
        this.fontstyle = this.fontstyle.copy();
        return this;
    }

    public getTable(): ReportElementDynamicTable{
        return this.table;
    }

    public redraw(){
        this.refreshEvent();
        super.redraw();
    }

    public check(propertyName: string): boolean {
        return false;
    }

    public json(): any {
      return {
        textalign: this.textalign,
        fontstyle: this.fontstyle.json(),
        colspan: this.colspan !== 1 ? Number(this.colspan) : undefined
      }
    }
}
