/**
    * Created by aleksay on 30.05.2017.
    * Project: tmp_booking
    */
import {Component, forwardRef, Input}   from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {SelectOption} from '@bluebamboo/common-ui';
@Component({
    selector: 'report-designer-text-align-editor',
    templateUrl: './report-designer-text-align-editor.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ReportDesignerTextAlignEditorComponent),
            multi: true
        }
    ]
})
export class ReportDesignerTextAlignEditorComponent implements ControlValueAccessor{
    private _value: string;
    private onChange: any = () => { };
    private onTouched: any = () => { };
    // TODO Text Align MLS
    @Input('title') public title = 'Text Ausrichtung';
    public options: SelectOption[] = [];
    public get value(): string {
        return this._value;
    }
    public set value(v: string) {
        this._value = v;
        this.onChange(v);
        this.onTouched();
    }

    constructor() {
        // TODO: Standard, Left, Right, Center MLS
        const options = [
            {code: '', title: 'Standard'},
            {code: 'left', title: 'Linksbündig'},
            {code: 'right', title: 'Rechtsbündig'},
            {code: 'center', title: 'Zentriert'}
        ];
        options.forEach((option) => {
            let o = new SelectOption();
            o.title = option.title;
            o.code = option.code;
            this.options.push(o);
        });
    }
    writeValue(obj: any): void {
        if (obj){
            this._value = obj.toString();
        } else {
            this.value = "";
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
    }
}
