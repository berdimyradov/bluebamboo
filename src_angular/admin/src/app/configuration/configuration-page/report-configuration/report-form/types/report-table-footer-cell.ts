/**
    * Created by aleksay on 26.05.2017.
    * Project: tmp_booking
    */
import {ReportElementPropertyEditor} from "./report-element-property-editor";
import {ReportSchema} from "./report-schema";
import {ReportTableBasicCell} from "./report-table-basic-cell";
import {ReportElementDynamicTable} from "./report-element-dynamictable";
import {ReportFontStyle} from "./report-fontstyle";
export class ReportTableFooterCell extends ReportTableBasicCell{
    public static editor: ReportElementPropertyEditor = new ReportElementPropertyEditor(["font", "common", "common.value", "common.colspan", "grid-action", 'grid-action.insert-data-column-left', 'grid-action.insert-data-column-right','grid-action.remove-data-column']);
    public value: string = undefined;

    constructor (reportSchema: ReportSchema, table: ReportElementDynamicTable, refresh: Function){
        super(reportSchema, table, refresh);
    }

    public parse(data: any, fontstyle: ReportFontStyle): ReportTableFooterCell {
        super.parse(data, fontstyle);
        if (data.value !== undefined) {
            this.value = data.value;
        }
        return this;
    }

    public redraw(){
        this.refreshEvent();
        super.redraw();
    }

    public check(propertyName: string): boolean {
        return this.table.colcount > this.table.footer.count();
    }

  public json(): any {
    let res: any = super.json();
    res.value = this.value;
    return res;
  }
}
