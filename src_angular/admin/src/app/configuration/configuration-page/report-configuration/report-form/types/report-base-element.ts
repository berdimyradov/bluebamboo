/**
    * Created by aleksay on 25.05.2017.
    * Project: tmp_booking
    */
import {ReportSchema} from "./report-schema";
import {ElementRef} from "@angular/core";
import {ReportBorderStyle} from "./report-borderstyle";
import {ReportFontStyle} from "./report-fontstyle";

export class ReportBaseElement{
    public reportSchema: ReportSchema;

    constructor(_reportSchema: ReportSchema){
      Object.defineProperties(this, {
        reportSchema: {
          get: () => _reportSchema,
          set: (v:ReportSchema) => _reportSchema = v
        }
      });
    }

    public parse(data:any):ReportBaseElement{
        return this;
    }

    public draw(el:ElementRef = null){
    }

    public redraw(){
        this.draw();
    }

    public refresh(){}

    public getFontStyle(name: any, notUseDefault: boolean = false){
        if (typeof name !== "object") {
            let res = this.reportSchema.getFontStyle(name || (notUseDefault ? null : "default")) || new ReportFontStyle(this.reportSchema);
            return res.copy();
        }
        return new ReportFontStyle(this.reportSchema, name);
    }

    public getBorderStyle(name: any, notUseDefault: boolean = false){
        if (typeof name == "string") {
            let res = this.reportSchema.getBorderStyle(name || (notUseDefault ? null : "default")) || new ReportBorderStyle(this.reportSchema);
            return res.copy();
        }
        return new ReportBorderStyle(this.reportSchema, name);
    }
}
