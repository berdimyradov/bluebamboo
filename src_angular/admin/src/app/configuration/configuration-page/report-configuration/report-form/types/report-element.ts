/**
    * Created by aleksay on 15.05.2017.
    * Project: tmp_booking
    */
import {ReportLocation} from "./report-location";
import {ReportSchema} from "./report-schema";
import {ElementRef} from "@angular/core";
import {ReportBorderStyle} from "./report-borderstyle";
import {ReportFontStyle} from "./report-fontstyle";
import {ReportBaseElement} from "./report-base-element";

export class ReportElement extends ReportBaseElement{
    public name: string;
    public type: string;
    public location: ReportLocation;
    public element: ElementRef;

    constructor(reportSchema: ReportSchema){
        super(reportSchema);
        let e: ElementRef;
        Object.defineProperties(this, {
          element: {
            get: () => e,
            set: (v:ElementRef) => e = v
          }
        });
    }

    public parse(data:any):ReportElement{
        this.name = data.name;
        this.location = (new ReportLocation()).parse(data.location);
        return this;
    }

    public draw(el:ElementRef = null){
        if (el) {
            this.element = el;
        }
        this.element.nativeElement.style.position = this.location.position == "fixed" ? "absolute" : "static";
        if (this.location.x !== undefined){
            this.element.nativeElement.style.left = this.location.x + "mm";
        }
        if (this.location.y !== undefined){
            this.element.nativeElement.style.top = this.location.y + "mm";
        }
    }

    public redraw(){
        this.draw();
    }

    public init(){

    }

    public refreshPostion(){
        if (this.location.x !== undefined){
            this.element.nativeElement.style.left = this.location.x + "mm";
        }
        if (this.location.y !== undefined){
            this.element.nativeElement.style.top = this.location.y + "mm";
        }
    }

    protected drawBorderStyle(data: ReportBorderStyle, el:ElementRef){
        if (data && data.width){
            this.element.nativeElement.style.border = "solid " + data.width + "mm " + data.color;
        }
    }

    protected drawFontStyle(data: ReportFontStyle, el:ElementRef){
        if (data){
            this.element.nativeElement.style.fontSize = data.size + "pt";
            this.element.nativeElement.style.fontFamily = data.face;
            this.element.nativeElement.style.color = data.color;
            this.element.nativeElement.style.lineHeight = (data.size*data.lineheight) + "pt";
            this.element.nativeElement.style.fontWeight = data.bold ? "bold" : "normal";
            this.element.nativeElement.style.backgroundColor = data.backgroundcolor ? data.backgroundcolor : "transparent";
            //el.nativeElement.style.letterSpacing = result.letterspacing + "mm";
        }
    }

    protected transformContent(content:string):string{
        let result: string = content || "", m, fields:string[] = [];
        const regex = /(%\w+?%)/gm;
        while ((m = regex.exec(result)) !== null) {
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }

            m.forEach((match) => {
                if (fields.indexOf(match) < 0){
                    fields.push(match);
                }
            });
        }
        fields.forEach((field) => result = result.replace(new RegExp(field, 'gm'), this.reportSchema.getValue(field)));
        return result;
    }

    public refresh(){

    }

    public json(): any {
      return {
        name: this.name,
        type: this.type,
        location: this.location.json()
      };
    }
}
