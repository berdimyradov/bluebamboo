import {ReportElementPropertyEditor} from "./report-element-property-editor";
/**
    * Created by aleksay on 16.05.2017.
    * Project: tmp_booking
    */
declare let $:any;
import {ReportElement} from "./report-element";
import {ReportSchema} from "./report-schema";
import {ElementRef} from "@angular/core";

export class ReportElementDataMatrix extends ReportElement {
    public static typeElement = "datamatrix";
    public static editor: ReportElementPropertyEditor = new ReportElementPropertyEditor(["location", "location.width", "location.height", "common", "common.content"]);

    public content: string;
    public width: number;
    public height: number;

    constructor(reportSchema: ReportSchema){
        super(reportSchema);
        this.type = "datamatrix";
    }

    public init(): ReportElementDataMatrix {
        this.parse({
            location: {
                position: "fixed",
                x: 10,
                y: 10
            },
            width: 20,
            height: 20
        });
        this.content = "Text";
        return this;
    }

    public parse(data:any):ReportElementDataMatrix{
        super.parse(data);
        this.content = data.content;
        this.width = Number(data.width) || 0;
        this.height = Number(data.height) || 0;
        return this;
    }

    public draw(el:ElementRef){
        super.draw(el);
        let settings = {
            output: "bmp",
            bgColor: "#FFFFFF",
            color: "#000000",
            barWidth: this.width + "mm",
            barHeight: this.height + "mm",
            moduleSize: 2,
            posX: 0,
            posY: 0,
            addQuietZone: 0
        };
        $(this.element.nativeElement).barcode(this.transformContent(this.content), 'datamatrix', settings);
        this.element.nativeElement.style.overflow = "hidden";
    }

    public json(): any {
      let res: any = super.json();
      res.content = this.content;
      if(res.content == "Text") { return null; }
      res.width = Number(this.width) || 0;
      res.height = Number(this.height) || 0;
      if(res.width == 0 || res.height == 0) { return null; }
      return res;
    }
}
