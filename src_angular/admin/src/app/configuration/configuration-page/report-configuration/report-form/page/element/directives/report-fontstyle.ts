/**
    * Created by aleksay on 16.05.2017.
    * Project: tmp_booking
    */
import {Directive, ElementRef, Input} from "@angular/core";
import {ReportFontStyle} from "../../../types/report-fontstyle";
import {ReportSchema} from "../../../types/report-schema";

@Directive({ selector: '[report-fontstyle]' })
export class ReportFontStyleDirective{
    @Input('report-schema')  private reportSchema: ReportSchema;
    @Input('report-fontstyle')  private style: ReportFontStyle;
    private prev: string = "";

    constructor(private el: ElementRef) {

    }

    ngDoCheck() {
        if(this.prev != (this.style || "").toString()) {
            this.apply();
            this.prev = (this.style || "").toString()
        }
    }

    private apply(){
        if (this.style) {
            if (this.style.isDefined) {
                this.el.nativeElement.style.fontSize = this.style.size ? this.style.size + "pt" : "";
                this.el.nativeElement.style.fontFamily = this.style.face || "";
                this.el.nativeElement.style.color = this.style.color;
                this.el.nativeElement.style.lineHeight = (this.style.size * this.style.lineheight) + "pt";
                this.el.nativeElement.style.fontWeight = this.style.bold ? "bold" : "normal";
                this.el.nativeElement.style.backgroundColor = this.style.backgroundcolor ? this.style.backgroundcolor : "transparent";
            } else {
                this.el.nativeElement.style.fontSize = "";
                this.el.nativeElement.style.fontFamily = "";
                this.el.nativeElement.style.color = "";
                this.el.nativeElement.style.lineHeight = "";
                this.el.nativeElement.style.fontWeight = "";
                this.el.nativeElement.style.backgroundColor = "transparent";
            }
        } else {
            this.el.nativeElement.style.fontSize = "";
            this.el.nativeElement.style.fontFamily = "";
            this.el.nativeElement.style.color = "";
            this.el.nativeElement.style.lineHeight = "";
            this.el.nativeElement.style.fontWeight = "";
            this.el.nativeElement.style.backgroundColor = "transparent";

        }
    }
}