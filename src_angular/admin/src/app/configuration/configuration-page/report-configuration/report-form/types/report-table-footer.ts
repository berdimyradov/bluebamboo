/**
 * Created by aleksey on 02.06.2017.
 */
import {ReportTableHeader} from "./report-table-header";
import {ReportTableFooterCell} from "./report-table-footer-cell";
import {ReportElementDynamicTable} from "./report-element-dynamictable";
import {ReportSchema} from "./report-schema";

export class ReportTableFooter extends ReportTableHeader {

    constructor (reportSchema: ReportSchema, table: ReportElementDynamicTable, type: string){
        super(reportSchema, table, type);
    }

    public addColumn(pos: number) {
        let cell: ReportTableFooterCell = new ReportTableFooterCell(this.reportSchema, this.table, this.cells[0].refreshEvent);
        cell.parse({
            textalign: "left",
            colspan: 1,
            value: "Column"
        }, this.fontstyle);
        cell.width = 20;
        this.cells.splice(pos, 0, cell);
    }

    public removeColumn(pos: number) {
        this.cells.splice(pos, 1);
    }

    public count(): number {
        let res: number = 0;
        this.cells.forEach((cell: ReportTableFooterCell) => res = res + cell.colspan);
        return res;
    }
}