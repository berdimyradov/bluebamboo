/**
    * Created by aleksay on 22.05.2017.
    * Project: tmp_booking
    */
export class ReportElementPropertyEditor{
    public get showFont(): boolean{
        return this.hasProperty('font');
    }

    public get showCommon(): boolean{
        return this.hasProperty('common');
    }

    public get showBorder(): boolean{
        return this.hasProperty('border');
    }

    public get showLocation(): boolean{
        return this.hasProperty('location');
    }

    public get showGrid(): boolean{
        return this.hasProperty('grid');
    }

    public get showGridAction(): boolean{
        return this.hasProperty('grid-action');
    }

    public showProperty(section: string, property: string): boolean{
        return this.hasProperty(`${section}.${property}`);
    }

    constructor(private properties: string[] = []){

    }

    private hasProperty(name: string): boolean{
        return this.properties.indexOf(name) >= 0;
    }
}
