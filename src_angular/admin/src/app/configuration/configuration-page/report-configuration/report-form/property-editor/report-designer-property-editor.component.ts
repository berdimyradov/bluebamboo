/**
    * Created by aleksay on 19.05.2017.
    * Project: tmp_booking
    */
import {Component, Input, OnInit} from '@angular/core';
import {ReportElement} from '../types/report-element';
import {ReportElementPropertyEditor} from '../types/report-element-property-editor';
import {ReportDesignerPageComponent} from '../page/report-designer-page.component';
import { ReportConfigurationService } from '../../../services/report-configuration.service';

@Component({
    selector: 'report-designer-property-editor',
    templateUrl: './report-designer-property-editor.html'
})
export class ReportDesignerPropertyEditorComponent implements OnInit {
    @Input('elements') private elements: ReportElement[] = [];
    @Input('page') private page: ReportDesignerPageComponent;
    @Input('readonly') private readonly  = false;

    public templateVars = [];

    constructor(private reportConfigService: ReportConfigurationService) {
    }
    ngOnInit() {
      this.reportConfigService.getPreviewData().subscribe( (previewData) => {
        this.templateVars = previewData.valueDescriptions;
      })
    }
    public get element(): ReportElement{
        return (this.elements.length > 0 ? this.elements[0] : null);
    }

    public get show(): ReportElementPropertyEditor{
        if (this.elements.length == 0){
            return new ReportElementPropertyEditor();
        }
        return this.elements[0].constructor["editor"];
    }
}
