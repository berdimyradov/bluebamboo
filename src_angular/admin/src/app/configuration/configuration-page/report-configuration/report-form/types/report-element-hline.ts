/**
    * Created by aleksay on 22.05.2017.
    * Project: tmp_booking
    */
import {ReportElement} from "./report-element";
import {ReportSchema} from "./report-schema";
import {ElementRef} from "@angular/core";
import {ReportElementPropertyEditor} from "./report-element-property-editor";

export class ReportElementHLine extends ReportElement{
    public static typeElement = "hline";
    public static editor: ReportElementPropertyEditor = new ReportElementPropertyEditor(['location', 'location.thickness', 'location.length']);

    public width: number;
    public length: number;

    constructor(reportSchema: ReportSchema){
        super(reportSchema);
        this.type = "hline";
    }

    public init(): ReportElementHLine {
        this.parse({
            location: {
                position: "fixed",
                x: 10,
                y: 10
            },
            width: 0.2,
            length: 20
        });
        return this;
    }

    public parse(data:any):ReportElementHLine{
        super.parse(data);
        this.width = Number(data.width) || 0;
        this.length = Number(data.length) || 0;
        return this;
    }

    public draw(el:ElementRef){
        super.draw(el);
        this.element.nativeElement.style.backgroundColor = "#000";
        this.element.nativeElement.style.height = Math.ceil(this.width*4)+ "px";
        this.element.nativeElement.style.width = this.length + "mm";
    }

    public json(): any {
      let res: any = super.json();
      res.width = Number(this.width) || 0;
      res.length = Number(this.length) || 0;
      if(res.width == 0 || res.length == 0) { return null; }
      return res;
    }
}
