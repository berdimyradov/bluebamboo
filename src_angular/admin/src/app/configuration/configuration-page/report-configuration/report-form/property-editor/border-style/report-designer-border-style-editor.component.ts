/**
    * Created by aleksay on 19.05.2017.
    * Project: tmp_booking
    */
import {Component, Input}   from '@angular/core';
import {ReportElement} from "../../types/report-element";
import {ReportElementPropertyEditor} from "../../types/report-element-property-editor";
import {ReportElementEditor} from "../decorator/report-element-editor.decorator";
import {ReportDesignerPageComponent} from "../../page/report-designer-page.component";

@Component({
    selector: 'report-designer-border-style-editor',
    templateUrl: './report-designer-border-style-editor.html'
})
export class ReportDesignerBorderStyleEditorComponent{
    @Input('element') private element: ReportElement;
    @Input('show') private show: ReportElementPropertyEditor;
    @Input('page') private page: ReportDesignerPageComponent;

    @ReportElementEditor('borderstyle.color') private color: string;
    @ReportElementEditor('borderstyle.width') private width: number;
}
