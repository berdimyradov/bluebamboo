/**
    * Created by aleksay on 30.05.2017.
    * Project: tmp_booking
    */
import {Component, forwardRef, Input}   from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {SelectOption} from '@bluebamboo/common-ui';

@Component({
    selector: 'report-designer-font-size-editor',
    templateUrl: './report-designer-font-size-editor.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ReportDesignerFontSizeEditorComponent),
            multi: true
        }
    ]
})
export class ReportDesignerFontSizeEditorComponent implements ControlValueAccessor{
    private _value: string;
    private onChange: any = () => { };
    private onTouched: any = () => { };
    // TODO: size
    @Input('title') public title = 'Schriftgrösse';
    public options: SelectOption[] = [];
    public get value(): string {
        return this._value;
    }
    public set value(v: string) {
        this._value = v;
        this.onChange(v);
        this.onTouched();
    }

    constructor() {
        const options = [
            {code: '', title: 'None'},
            {code: '5', title: '5'},
            {code: '6', title: '6'},
            {code: '7', title: '7'},
            {code: '8', title: '8'},
            {code: '9', title: '9'},
            {code: '10', title: '10'},
            {code: '11', title: '11'},
            {code: '12', title: '12'},
            {code: '13', title: '13'},
            {code: '14', title: '14'},
            {code: '15', title: '15'},
            {code: '16', title: '16'},
            {code: '17', title: '17'},
            {code: '18', title: '18'},
            {code: '19', title: '19'},
            {code: '20', title: '20'},
            {code: '21', title: '21'},
            {code: '22', title: '22'},
            {code: '23', title: '23'},
            {code: '24', title: '24'},
            {code: '25', title: '25'},
            {code: '26', title: '26'},
            {code: '27', title: '27'},
            {code: '28', title: '28'},
            {code: '29', title: '29'},
            {code: '30', title: '30'}
        ];
        options.forEach((option) => {
            let o = new SelectOption();
            o.title = option.title;
            o.code = option.code;
            this.options.push(o);
        });
    }
    writeValue(obj: any): void {
        if (obj){
            this._value = obj.toString();
        } else {
            this.value = "";
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
    }
}
