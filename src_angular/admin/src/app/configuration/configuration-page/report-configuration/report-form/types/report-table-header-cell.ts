/**
    * Created by aleksay on 16.05.2017.
    * Project: tmp_booking
    */
import {ReportElementPropertyEditor} from "./report-element-property-editor";
import {ReportSchema} from "./report-schema";
import {ReportTableBasicCell} from "./report-table-basic-cell";
import {ReportElementDynamicTable} from "./report-element-dynamictable";
import {ReportFontStyle} from "./report-fontstyle";

export class ReportTableHeaderCell extends ReportTableBasicCell{
    public static editor: ReportElementPropertyEditor = new ReportElementPropertyEditor(["font", "common", "common.value", "common.width", "grid-action", 'grid-action.insert-column-left', 'grid-action.insert-column-right', 'grid-action.remove']);
    public value: string = undefined;

    constructor (reportSchema: ReportSchema, table: ReportElementDynamicTable, refresh: Function){
        super(reportSchema, table, refresh);
    }

    public parse(data: any, fontstyle: ReportFontStyle): ReportTableHeaderCell {
        super.parse(data, fontstyle);
        if (data.value !== undefined) {
            this.value = data.value;
        }
        return this;
    }

    public json(): any {
      let res: any = super.json();
      res.value = this.value;
      return res;
    }
}
