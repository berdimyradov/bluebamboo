/**
 * Created by aleksay on 16.05.2017.
 */
import {Directive, ElementRef, Input} from "@angular/core";
import {ReportSchema} from "../../../types/report-schema";
import {ReportBorderStyle} from "../../../types/report-borderstyle";

@Directive({ selector: '[report-borderstyle]' })
export class ReportBorderStyleDirective{
    @Input('report-schema')  private reportSchema: ReportSchema;
    @Input('report-borderstyle')  private style: ReportBorderStyle;
    private prev: string = "";

    constructor(private el: ElementRef) {
    }

    ngDoCheck() {
        if(this.prev != (this.style || "").toString()) {
            this.apply();
        }
    }
    private apply(){
        if (this.style && this.style.width){
            this.el.nativeElement.style.border = "solid " + this.style.width + "mm " + this.style.color;
        } else {
            this.el.nativeElement.style.border = undefined;
        }
    }
}