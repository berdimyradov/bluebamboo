import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CommonPageComponent} from "../../../../common/common-page/common-page.component";
import {Report} from "../../types";
import {ReportDesignerPageComponent} from "./page/report-designer-page.component";
import {ReportDesignerPropertyEditorComponent} from "./property-editor/report-designer-property-editor.component";
import {ReportElement} from "./types/report-element";
import {ReportElementImage} from "./types/report-element-image";
import {ReportElementDataMatrix} from "./types/report-element-datamatrix";
import {ReportElementHLine} from "./types/report-element-hline";
import {ReportElementRect} from "./types/report-element-rect";
import {ReportElementString} from "./types/report-element-string";
import {ReportConfigurationService} from "../../services/report-configuration.service";
import {ReportInfo} from "../../types/report-info";

@Component({
  selector: 'admin-report-form',
  templateUrl: './report-form.component.html',
  styleUrls: ['./report-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ReportFormComponent implements OnInit {
  public report: Report;
  private elements: ReportElement[] = [];
  @ViewChild("property") public property: ReportDesignerPropertyEditorComponent;
  @ViewChild("page") public page: ReportDesignerPageComponent;
  
  public menuOpen: boolean = false;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private reportService: ReportConfigurationService
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.REPORT-FORM.TITLE';
    this.report = this.activeRoute.snapshot.data.report;
  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public setSelectedElement(elements: ReportElement[]){
    this.elements = elements;
  }

  public openMenu() {
    this.menuOpen = true;
  }

  private createElement(type: any){
    let element: ReportElement = (new type(this.report.schema)).init();
    this.report.schema.addElement(element);
    this.page.selectElement(element);
    this.menuOpen = false;
  }

  public createStringElement() {
    this.createElement(ReportElementString);
  }

  public createRectElement() {
    this.createElement(ReportElementRect);
  }

  public createHLineElement() {
    this.createElement(ReportElementHLine);
  }

  public createDataMatrixElement() {
    this.createElement(ReportElementDataMatrix);
  }

  public createImageElement() {
    this.createElement(ReportElementImage);
  }

  public get isCanRemove(): boolean {
    let res = false;
    if (this.elements && this.elements.length > 0){
      res = true;
    }
    return res;
  }

  public removeElement() {
    this.elements.forEach((el: ReportElement) => this.report.schema.removeElement(el) );
    this.page.selectElement(null);
  }

  public save() {
    let data: any = this.report.schema.json();
    if (this.report.isNew) {
      this.reportService.new(this.report.info, data)
        .subscribe(
          result => this.back()
        );
    } else {
      const report: Report = new Report();
      report.id = this.report.info.id;
      report.title = this.report.info.title;
      report.active = this.report.info.active;
      report.group_id = this.report.info.group_id;
      report.role_id = this.report.info.role_id;
      report.editable = this.report.info.editable;
      report.description = this.report.info.description;
	    report.standard = this.report.info.standard;
      report.template = data;
      this.reportService.update(report)
        .subscribe(
          result => this.back()
        );
    }
  }

  public pdf() {
	let data: Report = new Report();
	data.role_id = this.report.info.role_id;
	data.group_id = this.report.info.group_id;
	data.template = this.report.schema.json();

	let win = window.open();

    // let data: any = this.report.schema.json();
    this.reportService.preivewPdf(data)
      .subscribe(result => {
		  win.location.href = result.path;
	  });
  }

  public settings() {

  }
}
