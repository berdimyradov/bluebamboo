/**
    * Created by aleksay on 15.05.2017.
    * Project: tmp_booking
    */
export class ReportLocation{
    public x: number;
    public y: number;
    public position: string;

    public parse(data:any):ReportLocation{
        this.x = Number(data.x) || 0;
        this.y = Number(data.y) || 0;
        this.position = data.position;
        return this;
    }

    public json(): any {
      return {
        x: Number(this.x) || 0,
        y: Number(this.y) || 0,
        position: this.position
      }
    }
}
