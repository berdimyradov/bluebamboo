/**
    * Created by aleksay on 25.05.2017.
    * Project: tmp_booking
    */
import {Component, Input}   from '@angular/core';
import {ReportElementPropertyEditor} from "../../types/report-element-property-editor";
import {ReportTableBasicCell} from "../../types/report-table-basic-cell";
import {ReportDesignerPageComponent} from "../../page/report-designer-page.component";

@Component({
    selector: 'report-designer-grid-action-editor',
    templateUrl: './report-designer-grid-action-editor.html'
})
export class ReportDesignerGridActionEditorComponent{
    @Input('element') private element: ReportTableBasicCell;
    @Input('show') private show: ReportElementPropertyEditor;
    @Input('page') private page: ReportDesignerPageComponent;

    public goToGridProperty(){
        let table = this.element.getTable();
        this.page.selectElement(table);
    }

    public addColumnOnLeftSide() {
        let table = this.element.getTable();
        table.addColumnOnLeftSide(this.element);
    }

    public addColumnOnRightSide() {
        let table = this.element.getTable();
        table.addColumnOnRightSide(this.element);
    }

    public addDataColumnOnLeftSide() {
        let table = this.element.getTable();
        let type = this.element.constructor.name == "ReportTableFooterCell" ? "footer" : "data";
        table.addDataColumnOnLeftSide(this.element, type);
    }

    public addDataColumnOnRightSide() {
        let table = this.element.getTable();
        let type = this.element.constructor.name == "ReportTableFooterCell" ? "footer" : "data";
        table.addDataColumnOnRightSide(this.element, type);
    }

    public removeColumn() {
        let table = this.element.getTable();
        table.removeColumn(this.element);
    }
    public removeDataColumn() {
        let type = this.element.constructor.name == "ReportTableFooterCell" ? "footer" : "data";
        let table = this.element.getTable();
        table.removeDataColumn(this.element, type);
    }
}
