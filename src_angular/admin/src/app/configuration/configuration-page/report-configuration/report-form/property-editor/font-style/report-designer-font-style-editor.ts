/**
    * Created by aleksay on 19.05.2017.
    * Project: tmp_booking
    */
import {Component, Input}   from '@angular/core';
import {ReportElementPropertyEditor} from "../../types/report-element-property-editor";
import {ReportElement} from "../../types/report-element";
import {ReportElementEditor} from "../decorator/report-element-editor.decorator";
import {ReportDesignerPageComponent} from "../../page/report-designer-page.component";

@Component({
    selector: 'report-designer-font-style-editor',
    templateUrl: './report-designer-font-style-editor.html'
})
export class ReportDesignerFontStyleEditorComponent{
    @Input('element') private element: ReportElement;
    @Input('show') private show: ReportElementPropertyEditor;
    @Input('page') private page: ReportDesignerPageComponent;

    @ReportElementEditor('fontstyle.face') private face: string;
    @ReportElementEditor('fontstyle.size') private size: number;
    @ReportElementEditor('fontstyle.color') private color: string;
    @ReportElementEditor('fontstyle.backgroundcolor') private backgroundcolor: string;
    @ReportElementEditor('fontstyle.lineheight') private lineheight: number;
    @ReportElementEditor('fontstyle.letterspacing') private letterspacing: number;
    @ReportElementEditor('fontstyle.bold') private bold: boolean;
    @ReportElementEditor() private textalign: string;
}
