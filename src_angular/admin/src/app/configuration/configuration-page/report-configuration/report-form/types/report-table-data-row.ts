/**
    * Created by aleksay on 25.05.2017.
    * Project: tmp_booking
    */
import {ReportBaseElement} from "./report-base-element";
import {ReportSchema} from "./report-schema";
import {ReportTableDataCell} from "./report-table-data-cell";
import {ReportFontStyle} from "./report-fontstyle";
import {ReportBorderStyle} from "./report-borderstyle";
import {ReportElementDynamicTable} from "./report-element-dynamictable";

export class ReportTableDataRow extends ReportBaseElement{
    public cells: ReportTableDataCell[] = [];
    public fontstyle: ReportFontStyle;
    public borderstyle: ReportBorderStyle;

    constructor(reportSchema: ReportSchema, private table: ReportElementDynamicTable){
        super(reportSchema);
    }

    public parse(data:any):ReportTableDataRow{
        this.fontstyle = this.getFontStyle(data.fontstyle, true);
        this.borderstyle = this.getBorderStyle(data.borderstyle, true);
        let cells: ReportTableDataCell[] = [];
        data.cells.forEach((cell:any) => {
            cells.push(new ReportTableDataCell(this.reportSchema, this.table).parse(cell));
        });
        this.cells = cells;
        return this;
    }

    public addColumn(pos: number) {
        console.log(pos);
        let cell: ReportTableDataCell = new ReportTableDataCell(this.reportSchema, this.table);
        cell.parse({ textalign: "cemter", colspan: 1});
        this.cells.splice(pos, 0, cell);
    }

    public json() {
      return {
        fontstyle: this.fontstyle.json(),
        borderstyle: this.borderstyle.json(),
        cells: this.cells.map( (it: ReportTableDataCell) => it.json())
      };
    }
}
