/**
    * Created by aleksay on 19.05.2017.
    * Project: tmp_booking
    */
import { Component, Input } from '@angular/core';
import { ReportElement } from '../../types/report-element';
import { ReportElementPropertyEditor } from '../../types/report-element-property-editor';
import { ReportElementEditor } from '../decorator/report-element-editor.decorator';
import { ReportDesignerPageComponent } from '../../page/report-designer-page.component';
import { SelectOption } from '@bluebamboo/common-ui';
import { OnInit, OnChanges} from '@angular/core';
import { NgFor } from '@angular/common';

import { ReportTemplateVarsService } from '../../../../services/report-template-vars.service';
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'report-designer-common-editor',
  templateUrl: './report-designer-common-editor.html',
  styleUrls: ['./report-designer-common-editor.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ReportDesignerCommonEditorComponent implements OnInit, OnChanges {
  @Input('element') private element: ReportElement;
  @Input('show') private show: ReportElementPropertyEditor;
  @Input('page') private page: ReportDesignerPageComponent;
  @Input('templateVars') private templateVars;

  @ReportElementEditor() private content: string;
  @ReportElementEditor() private upload: string;
  @ReportElementEditor() private url: string;
  @ReportElementEditor() public value: string;
  @ReportElementEditor() private type: string;
  @ReportElementEditor() private colspan: number;
  @ReportElementEditor() private width: number;

  public mode= 'global';
  public data: SelectOption[] = [];

  public selectedGroup: string;
  public selectedProperty: string;
  public templateVar = null;

  public groups;
  public elements;

  private el = {
    type: null,
    rowcontent: null
  };

  constructor(private templateVarsService: ReportTemplateVarsService) {}

  ngOnInit() {
  }

  ngOnChanges() {
    this.templateVar = null;
    this.setElementType();
    this.mode = this.element.element ? 'global' : 'dynamicTable';
    this.templateVarsService.init(this.templateVars);
    this.groups = this.templateVarsService.getGroupsByElement(this.el);
  }

  changedGroup() {
    this.elements = this.templateVarsService.getElementsOfGroup(this.selectedGroup);
    this.selectedProperty = this.elements[0].code;
    this.templateVar = this.templateVarsService
    .getTemplateVar(this.mode, this.selectedGroup, this.selectedProperty);
  }

  public changedPropSelect() {
    this.templateVar = this.templateVarsService
    .getTemplateVar(this.el, this.selectedGroup, this.selectedProperty);
  }

  public onAddToElement() {
    if (this.mode === 'dynamicTable') {
      this.value += '\n'  + this.templateVar.value;
      this.templateVar = null;
    } else {
      this.content += this.templateVar.value;
      this.templateVar = null;
    }
  }

  private setElementType() {
    this.el.type = this.element['table'] ? 'dynamicTable' : 'Global';
    this.el.rowcontent = this.element['table'] ? this.element['table']['content']['rowcontent'] : null;
  }

}
