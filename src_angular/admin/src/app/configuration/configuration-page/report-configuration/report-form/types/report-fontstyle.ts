import {ReportSchema} from "./report-schema";
export class ReportFontStyle{
    public name: string;
    public face: string;
    public size: number;
    public color: string;
    public backgroundcolor: string;
    public lineheight: number;
    public letterspacing: number;
    public bold: boolean = false;
    public reportSchema: ReportSchema;

    private propertyIsDefined(val: any){
        return val !== undefined && val !== null;
    }

    public get isDefined(){
        return this.propertyIsDefined(this.face) && this.propertyIsDefined(this.size);
    }

    constructor(_reportSchema: ReportSchema, data: any = null){
      Object.defineProperties(this, {
        reportSchema: {
          get: () => _reportSchema,
          set: (v:ReportSchema) => _reportSchema = v
        }
      });
        if (data){
            this.name = data.name;
            this.face = data.face;
            this.size = Number(data.size) || undefined;
            this.color = data.color;
            this.lineheight = Number(data.lineheight) || undefined;
            this.letterspacing = Number(data.letterspacing) || undefined;
            this.backgroundcolor = data.backgroundcolor;
            this.bold = data.bold || undefined;
        }
    }

    public copy() : ReportFontStyle{
        let res: ReportFontStyle = new ReportFontStyle(this.reportSchema);
        res.name = this.name;
        res.face = this.face;
        res.size = this.size || undefined;
        res.color = this.color;
        res.backgroundcolor = this.backgroundcolor;
        res.lineheight = this.lineheight || undefined;
        res.letterspacing = this.letterspacing || undefined;
        res.bold = this.bold || false;
        return res;
    }

    public toString():string{
        return `${this.face}-${this.size}-${this.color}-${this.lineheight}-${this.letterspacing}-${this.backgroundcolor}-${this.bold}`;
    }

    public json(): any {
      if (this.name) {
        return {
          name: this.name,
          face: this.face,
          size: Number(this.size) || undefined,
          color: this.color || undefined,
          backgroundcolor: this.backgroundcolor || undefined,
          lineheight: Number(this.lineheight) || undefined,
          letterspacing: Number(this.letterspacing) || undefined,
          bold: this.bold || undefined
        }
      }
      return undefined;
    }
}
