/**
    * Created by aleksay on 15.05.2017.
    * Project: tmp_booking
    */
import {ReportSchema} from "./report-schema";
export class ReportBorderStyle {
  public name: string;
  public width: number;
  public color: string;
  public reportSchema: ReportSchema;

  private propertyIsDefined(val: any) {
    return val !== undefined && val !== null;
  }

  public get isDefined() {
    return this.propertyIsDefined(this.width) && this.propertyIsDefined(this.color);
  }

  constructor(_reportSchema: ReportSchema, data: any = null) {
    Object.defineProperties(this, {
      reportSchema: {
        get: () => _reportSchema,
        set: (v:ReportSchema) => _reportSchema = v
      }
    });
    if (data) {
      this.name = data.name;
      this.width = Number(data.width);
      this.color = data.color;
    }
  }

  public copy(): ReportBorderStyle {
    let res: ReportBorderStyle = new ReportBorderStyle(this.reportSchema);
    res.name = this.name;
    res.width = Number(this.width) || 0;
    res.color = this.color;
    return res;
  }

  public toString(): string {
    return `${this.width}-${this.color}`;
  }

  public json(): any {
    return {
      name: this.name,
      width: Number(this.width) || undefined,
      color: this.color || undefined
    }
  }
}
