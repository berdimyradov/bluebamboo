/**
    * Created by aleksay on 16.05.2017.
    * Project: tmp_booking
    */
export class ReportData{
    public type: string;
    public values: any;
    public documentSettings: any;
    public other: any;
    public dynamicTableContent:any;

    public parse(data:any): ReportData{
        this.type = data.type;
        this.values  = data.values;
        this.documentSettings = data.documentSettings;
        this.dynamicTableContent = data.dynamicTableContent;
        return this;
    }
}
