/**
 * Created by aleksay on 25.05.2017.
 */
import { ReportElementPropertyEditor } from './report-element-property-editor';
import { ReportSchema } from './report-schema';
import { ReportTableBasicCell } from './report-table-basic-cell';
import { ReportElementDynamicTable } from './report-element-dynamictable';

export class ReportTableDataCell extends ReportTableBasicCell {
  public static editor: ReportElementPropertyEditor = new ReportElementPropertyEditor([
    'font',
    'common',
    'common.value',
    'grid-action',
    'grid-action.insert-data-column-left',
    'grid-action.insert-data-column-right',
    'grid-action.remove-data-column'
  ]);
  private _value = undefined;
  private _key = undefined;
  public type: string = undefined;
  public value: string;

  constructor(
    reportSchema: ReportSchema,
    table: ReportElementDynamicTable) {
    super(reportSchema, table);
  }

  public parse(data: any): ReportTableDataCell {
    super.parse(data);
    if (data.type !== undefined) {
      this.type = data.type;
    }
    if (data.value) {
      this.value = data.value;
    }
    return this;
  }

  public check(propertyName: string): boolean {
    return this.table.colcount > this.table.content.count();
  }

  public getValue(data: any, globalVars: any): any {

    let result = this.value, m, fields = [];
    const regex = /([%#]\w+?[%#])/gm;

    if (this._value !== undefined && this._key === this.value) {
        return this._value;
      }

    while ((m = regex.exec(result)) !== null) {
      if (m.index === regex.lastIndex) {
        regex.lastIndex++;
      }

      m.forEach((match) => {
        if (fields.indexOf(match) < 0) {
          fields.push(match);
        }
      });
    }

    fields.forEach((field, index) => {
      const strippedOFTags = field.replace(/[%#]/g, '');
      const parsedValue = data[strippedOFTags] || globalVars[strippedOFTags];
      result = result.replace(new RegExp(field, 'gm'), parsedValue);
    });
    return result;
  }

  public json(): any {
    let res: any = super.json();
    res.value = this.value;
    res.type = this.type;
    return res;
  }
}
