/**
    * Created by aleksay on 25.05.2017.
    * Project: tmp_booking
    */
import {ReportFontStyle} from "./report-fontstyle";
import {ReportBorderStyle} from "./report-borderstyle";
import {ReportBaseElement} from "./report-base-element";
import {ReportSchema} from "./report-schema";
import {ReportTableDataRow} from "./report-table-data-row";
import {ReportElementDynamicTable} from "./report-element-dynamictable";
import {ReportTableBasicCell} from "./report-table-basic-cell";
import {ReportTableDataCell} from "./report-table-data-cell";

export class ReportTableDataSchema extends ReportBaseElement{
    public rows: ReportTableDataRow[] = [];
    public fontstyle: ReportFontStyle;
    public borderstyle : ReportBorderStyle;

    constructor(reportSchema: ReportSchema, private table: ReportElementDynamicTable){
        super(reportSchema);
    }

    public parse(data:any): ReportTableDataSchema{
        this.fontstyle = this.getFontStyle(data.fontstyle, true);
        this.borderstyle = this.getBorderStyle(data.borderstyle, true);
        let rows: ReportTableDataRow[] = [];
        data.rows.forEach((row:any) => {
            rows.push(new ReportTableDataRow(this.reportSchema, this.table).parse(row));
        });
        this.rows = rows;
        return this;
    }

    public count(): number {
        let c: number = 0;
        this.rows[0].cells.forEach((cell: ReportTableBasicCell) => c = c + cell.colspan);
        return c;
    }

    public addColumn(pos: number) {
        console.log(pos);
        let cell: ReportTableDataCell = new ReportTableDataCell(this.reportSchema, this.table);
        cell.parse({ textalign: "left", colspan: 1});
        this.rows[0].cells.splice(pos, 0, cell);
    }
}
