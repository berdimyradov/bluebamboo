/**
    * Created by aleksay on 15.05.2017.
    * Project: tmp_booking
    */
import {ReportData}             from "./report-data";
import {ReportFontStyle}        from "./report-fontstyle";
import {ReportBorderStyle}      from "./report-borderstyle";
import {ReportElement}          from "./report-element";
import {ReportElementRect}      from "./report-element-rect";
import {ReportElementString}    from "./report-element-string";
import {ReportElementDataMatrix} from "./report-element-datamatrix";
import {ReportElementDynamicTable} from "./report-element-dynamictable";
import {ReportElementImage} from "./report-element-image";
import {ReportElementHLine} from "./report-element-hline";

export class ReportSchema {
  private data: ReportData;
  public type: string;
  public fontstyles: ReportFontStyle[] = [];
  public borderstyles: ReportBorderStyle[] = [];
  public elements: ReportElement[] = [];
  public extentData: any;

  private createElement(data: any): ReportElement {
    let classes: any[] = [ReportElementRect, ReportElementString, ReportElementDataMatrix, ReportElementDynamicTable,
      ReportElementHLine, ReportElementImage];
    let el: ReportElement = null;

    classes.forEach((cl: any) => {
      if (cl.typeElement == data.type) {
        el = new cl(this);
        el.parse(data);
      }
    });
    return el;
  }

  public parse(obj: any): ReportSchema {
    this.type = obj.type;
    this.fontstyles = [];
    obj.fontstyles.forEach((style: any) => this.fontstyles.push(new ReportFontStyle(this, style)));
    this.borderstyles = [];
    obj.borderstyles.forEach((border: any) => this.borderstyles.push(new ReportBorderStyle(this, border)));
    this.extentData = {};
    Object.keys(obj).forEach((field) => {
      if (field == 'type' || field == 'fontstyles' || field == 'borderstyles' || field == 'elements') {
        return;
      }
      this.extentData[field] = obj[field];
    });
    this.elements = [];
    obj.elements.forEach((element: any) => this.elements.push(this.createElement(element)));
    return this;
  }

  public getBorderStyle(name: any, returnNotNull: boolean = false) {
    let result: ReportBorderStyle = null;
    if (typeof name !== 'string') {
      return new ReportBorderStyle(this, name);
    }
    this.borderstyles.forEach((border: ReportBorderStyle) => result = border.name == name ? border : result);
    if (returnNotNull) {
      result = result || new ReportBorderStyle(this);
    }
    return result;
  }

  public getFontStyle(name: any, returnNotNull: boolean = false) {
    let result: ReportFontStyle = null;
    if (typeof name !== 'string') {
      return new ReportFontStyle(this, name);
    }
    this.fontstyles.forEach((font: ReportFontStyle) => result = font.name == name ? font : result);
    if (returnNotNull) {
      result = result || new ReportFontStyle(this);
    }
    return result;
  }

  public getValue(field: string) {
    if (this.data && this.data.values) {
      return this.data.values[field.replace(/%/g, '')];
    }
    return '';
  }

  public getTableData(field: string): any[] {
    if (this.data && this.data.dynamicTableContent) {
      return this.data.dynamicTableContent[field.replace(/%/g, '')];
    }
    return [];
  }

  public setData(data: ReportData) {
    this.data = data;
    this.refreshElement();
  }

  private refreshElement() {
    this.elements.forEach((element) => element.refresh());
  }

  public addElement(element: ReportElement) {
    this.elements.push(element);
  }

  public removeElement(element: ReportElement) {
    this.elements.splice(this.elements.indexOf(element), 1);
  }

  public json(): any {
    let data: any = JSON.parse(JSON.stringify(this.extentData));
    data.type = this.type;
    data.borderstyles = JSON.parse(JSON.stringify(this.borderstyles));
    data.fontstyles = JSON.parse(JSON.stringify(this.fontstyles));
    data.elements = this.elements.reduce((ret: ReportElement[], e: ReportElement) => {
      let json = e.json();
      if(json) {
        ret.push(json); 
      }
      return ret;
    }, []);
    return data;
  }
}
