import {ReportFontStyle} from "./report-fontstyle";
import {ReportBorderStyle} from "./report-borderstyle";
import {ReportElement} from "./report-element";
import {ReportSchema} from "./report-schema";
import {ReportElementDynamicTable} from "./report-element-dynamictable";
import {ReportTableDataRow} from "./report-table-data-row";
import {ReportTableBasicCell} from "./report-table-basic-cell";
/**
    * Created by aleksay on 16.05.2017.
    * Project: tmp_booking
    */
export class ReportTableContent {
  public rowcontent: string;
  public rows: ReportTableDataRow[] = [];
  public fontstyle: ReportFontStyle;
  public borderstyle: ReportBorderStyle;

  constructor (private reportSchema: ReportSchema, private table: ReportElementDynamicTable) {}

  public parse(data: any, element: ReportElement): ReportTableContent {
    this.rows = data.rows.map((r:any) => (new ReportTableDataRow(this.reportSchema, this.table)).parse(r));
    this.rowcontent = data.rowcontent;
    this.fontstyle = element.getFontStyle(data.fontstyle);
    this.borderstyle = element.getBorderStyle(data.borderstyle);
    return this;
  }

  public json(): any {
    return {
      rowcontent: this.rowcontent,
      rows: this.rows.map( (it: ReportTableDataRow) => it.json()),
      fontstyle: this.fontstyle.json(),
      borderstyle: this.borderstyle.json()
    }
  }

  public count(): number {
    let c: number = 0;
    this.rows[0].cells.forEach((cell: ReportTableBasicCell) => c = c + cell.colspan);
    return c;
  }
}
