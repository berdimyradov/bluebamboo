/**
    * Created by aleksay on 16.05.2017.
    * Project: tmp_booking
    */
import {ReportTableHeaderCell} from "./report-table-header-cell";
import {ReportFontStyle} from "./report-fontstyle";
import {ReportBorderStyle} from "./report-borderstyle";
import {ReportBaseElement} from "./report-base-element";
import {ReportSchema} from "./report-schema";
import {ReportElementDynamicTable} from "./report-element-dynamictable";
import {ReportTableFooterCell} from "./report-table-footer-cell";

export class ReportTableHeader extends ReportBaseElement{
    public show: boolean;
    public fontstyle: ReportFontStyle;
    public borderstyle: ReportBorderStyle;
    public cells: ReportTableHeaderCell[] = [];

    constructor (reportSchema: ReportSchema, protected table: ReportElementDynamicTable, protected type: string){
        super(reportSchema);
    }

    public parse(data: any, refresh: Function = () => {}): ReportTableHeader{
        this.show = data.show;
        this.fontstyle =  this.reportSchema.getFontStyle(data.fontstyle, true);
        this.borderstyle = this.reportSchema.getBorderStyle(data.borderstyle, true);
        this.cells = data.cells.map((cell:any) => (new (this.type == "header" ? ReportTableHeaderCell : ReportTableFooterCell)(this.reportSchema, this.table, refresh)).parse(cell, this.fontstyle));
        return this;
    }

    public addColumn(pos: number) {
        let cell: ReportTableHeaderCell = new ReportTableHeaderCell(this.reportSchema, this.table, this.cells[0].refreshEvent);
        cell.parse({
            textalign: "left",
            colspan: 1,
            value: "Column"
        }, this.fontstyle);
        cell.width = 20;
        this.cells.splice(pos, 0, cell);
    }

    public json(): any {
      return {
        show: this.show,
        fontstyle: this.fontstyle.json(),
        borderstyle: this.borderstyle.json(),
        cells: this.cells.map((it: ReportTableHeaderCell) => it.json())
      };
    }
}
