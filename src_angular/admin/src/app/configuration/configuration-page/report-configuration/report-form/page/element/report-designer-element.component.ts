 /**
    * Created by aleksay on 15.05.2017.
    * Project: tmp_booking
    */
import {
  AfterViewInit, Component,
  ElementRef, Input, HostListener,
  HostBinding, Output, EventEmitter, ViewEncapsulation
}                       from '@angular/core';
import {ReportElement}                  from "../../types/report-element";
import {ReportData}                     from "../../types/report-data";
 import {ReportDesignerPageComponent} from "../report-designer-page.component";

@Component({
  selector: 'report-designer-element',
  templateUrl: './report-designer-element.html',
  styleUrls: ['./report-designer-element.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ReportDesignerElementComponent implements AfterViewInit{
    @Input('element') private element: ReportElement;
    @Input('data') private data: ReportData;
    @Input('is-selected') private isSelected: boolean = false;
    @Output('on-select') private onSelect = new EventEmitter<ReportElement>();
    @Input('page') private page: ReportDesignerPageComponent;

    constructor(private el: ElementRef){

    }

    ngOnChanges(){
        this.update();
    }

    ngAfterViewInit(): void {
        this.update();
    }

    @HostListener('click')
    public click(){
        this.onSelect.emit(this.element);
    }

    @HostBinding("class") get getClass(){
        return this.isSelected ? "report-designer-element-selected" : "";
    }

    private update(){
        if (!this.element){
            return;
        }
        this.element.draw(this.el);
    }

    private select($event: any, type: string, element: any){
        this.page.selectElement(element);
        $event.stopPropagation();
    }
}
