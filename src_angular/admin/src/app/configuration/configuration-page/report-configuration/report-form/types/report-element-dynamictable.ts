/**
    * Created by aleksay on 16.05.2017.
    * Project: tmp_booking
    */
import { ReportElement } from "./report-element";
import { ReportSchema } from "./report-schema";
import { ElementRef } from "@angular/core";
import { ReportTableContent } from "./report-table-content";
import { ReportTableHeader } from "./report-table-header";
import { ReportTableRow } from "./report-table-row";
import { ReportElementPropertyEditor } from "./report-element-property-editor";
import { ReportTableDataSchema } from "./report-table-data-schema";
import { ReportTableHeaderCell } from "./report-table-header-cell";
import { ReportTableBasicCell } from "./report-table-basic-cell";
import { ReportTableDataCell } from "./report-table-data-cell";
import { ReportTableFooterCell } from "./report-table-footer-cell";
import { ReportTableFooter } from "./report-table-footer";
import { ReportTableDataRow } from "./report-table-data-row";

export class ReportElementDynamicTable extends ReportElement {
    public static typeElement = "dynamictable";
    public static editor: ReportElementPropertyEditor = new ReportElementPropertyEditor(["location", "grid"]);

    private _columnWidth: number[] = [];
    private _rows: ReportTableRow[] = [];

    public get columnWidth(): number[] {
        return this._columnWidth;
    }

    public get rows(): ReportTableRow[] {
        return this._rows;
    }

    public colcount: number;
    public colwidth: string;
    public content: ReportTableContent = null;
    public header: ReportTableHeader = null;
    public footer: ReportTableFooter = null;

    constructor(reportSchema: ReportSchema) {
        super(reportSchema);
        this.type = "dynamictable";
    }

    public parse(data: any): ReportElementDynamicTable {
        super.parse(data);
        this.colcount = Number(data.colcount);
        this.colwidth = data.colwidth;
        this.content = new ReportTableContent(this.reportSchema, this);
        this.content.parse(data.content, this);
        if (data.header) {
            this.header = new ReportTableHeader(this.reportSchema, this, 'header');
            this.header.parse(data.header, () => {
                this.updateColumns();
            });
        }
        if (data.footer) {
            this.footer = new ReportTableFooter(this.reportSchema, this, 'footer');
            this.footer.parse(data.footer, () => {
                this.updateColumns();
            });
        }
        this.updateColumnWidth();
        return this;
    }

    public draw(el: ElementRef) {
        super.draw(el);
    }

    public getCellContent(cell: ReportTableHeaderCell): string {
        let content = this.transformContent(cell.value);
        return content;
    }

    private parseRowSchema(data: any): ReportTableDataSchema {
        let result: ReportTableDataSchema = new ReportTableDataSchema(this.reportSchema, this);
        return result.parse(data);
    }

    private setCellWidthForRow(cells: ReportTableHeaderCell[]) {
        let w: number[] = [].concat(this.columnWidth);
        cells.forEach((cell: any) => {
            cell.width = 0;
            for (let i = 0; i < (cell.colspan || 1); i++) {
                cell.width = Number(cell.width + w.shift());
            }
        });
    }

    private updateColumnWidth() {
        let data: string[] = this.colwidth.split(",");
        this._columnWidth = data.map((w: string) => Number(w));
        this.content.rows.forEach((row: any) => {
            this.setCellWidthForRow(row.cells);
        });
        if (this.header) {
            this.setCellWidthForRow(this.header.cells);
        }
        if (this.footer) {
            this.setCellWidthForRow(this.footer.cells);
        }
    }

    private buildDataRows() {
        let data = this.reportSchema.getTableData(this.content.rowcontent);
        let result: ReportTableRow[] = [];
        let rowInd = 0;
        data.forEach((d) => {
            let row: ReportTableRow = new ReportTableRow();
            row.data = d;
            row.schema = this.content.rows[rowInd];
            result.push(row);
            rowInd++;
            if (rowInd >= this.content.rows.length) {
                rowInd = 0;
            }
        });
        this._rows = result;
    }

    private updateColumns() {
        this.colwidth = this.header.cells.map((el: ReportTableHeaderCell) => el.width).join(',');
        this.colcount = this.header.cells.length;
        this.updateColumnWidth();
    }

    public refresh() {
        super.refresh();
        this.buildDataRows();
    }

    public addColumnOnLeftSide(cell: ReportTableBasicCell) {
        let pos = this.header.cells.indexOf(cell as ReportTableHeaderCell);
        this.header.addColumn(pos);
        this.updateColumns();
    }

    public addColumnOnRightSide(cell: ReportTableBasicCell) {
        let pos = this.header.cells.indexOf(cell as ReportTableHeaderCell);
        this.header.addColumn(pos + 1);
        this.updateColumns();
    }

    public addDataColumnOnLeftSide(cell: ReportTableBasicCell, type: string) {
        let pos = (type == 'data'
            ? this.content.rows[0].cells.indexOf(cell as ReportTableDataCell)
            : this.footer.cells.indexOf(cell as ReportTableFooterCell)
        );
        let target = type === 'data' ? this.content.rows[0] : this.footer;
        target.addColumn(pos);
        this.updateColumns();
    }

    public addDataColumnOnRightSide(cell: ReportTableBasicCell, type: string) {
        let pos = (type == 'data'
            ? this.content.rows[0].cells.indexOf(cell as ReportTableDataCell)
            : this.footer.cells.indexOf(cell as ReportTableFooterCell)
        );
        let target = type === 'data' ? this.content.rows[0] : this.footer;
        target.addColumn(pos + 1);
        this.updateColumns();
    }

    private removeCell(pos: number, cells: ReportTableBasicCell[]) {
        let current = 0;
        for (let i = 0, c = cells.length; i < c; i++) {
            let c: ReportTableBasicCell = cells[i];
            if (current <= pos && pos <= (current + c.colspan)) {
                if (c.colspan > 1) {
                    c.colspan = c.colspan - 1;
                } else {
                    cells.splice(i, 1);
                }
                return;
            }
        }
    }

    public removeColumn(cell: ReportTableBasicCell) {
        let pos = this.header.cells.indexOf(cell as ReportTableHeaderCell);
        this.header.cells.splice(pos, 1);
        if (this.footer) {
            this.removeCell(pos, this.footer.cells)
        }
        // this.content.rows.forEach((row:ReportTableDataRow) => this.removeCell(pos, row.cells));
        this.updateColumns();
        this.refresh();
    }

    public removeDataColumn(cell: ReportTableBasicCell, type: string) {
        if (type === 'data') {
            const pos = this.content.rows[0].cells.indexOf(cell as ReportTableDataCell);
            this.content.rows[0].cells.splice(pos, 1);
        } else {
            const pos = this.footer.cells.indexOf(cell as ReportTableFooterCell);
            this.footer.removeColumn(pos);
        }
        this.updateColumns();
        this.refresh();
    }

    public json(): any {
        let res: any = super.json();
        res.colcount = Number(this.colcount);
        res.colwidth = this.colwidth;
        res.header = this.header ? this.header.json() : undefined;
        res.content = this.content.json();
        res.footer = this.footer ? this.footer.json() : undefined;
        return res;
    }
}
