/**
    * Created by aleksay on 22.05.2017.
    * Project: tmp_booking
    */
import {ReportElement} from "./report-element";
import {ReportSchema} from "./report-schema";
import {ElementRef} from "@angular/core";
import {ReportElementPropertyEditor} from "./report-element-property-editor";

export class ReportElementImage extends ReportElement{
    private _width: number;
    private _height: number;
    private _aspectRatio: number = 1;
    private _url: string;
    
    public static typeElement = "image";
    public static editor: ReportElementPropertyEditor = new ReportElementPropertyEditor(["location", "location.width", "location.height", "location.keepRatio", "common", "common.url", "common.upload"]);

    public get previewUrl(): string {
        if (!this._url){
            return '/admin/img/upload_picture.png';
        }
        // if it is (http ….) just use this url as preview
        // else extract filename part from `bluebamboo/client/171/images/171_59a7df6311855hjp.png` and prefix with `/downloads/templateimages/`
        if (this._url.indexOf('http')===0){
            return this._url
        }

        let parts = this._url.split('/')
        let filename = parts[parts.length-1];
        return '/downloads/templateimages/' + filename;
    };
    public get url(): string {
        return this._url;
    };
    public set url(u: string) {
        this._url = u;
        this._recalculateRatio(this.previewUrl);
    };
    public get upload(): any {
        return this.url;
    };
    public set upload(r: any) {
        this.url = r.fileLocation; 
        this._recalculateRatio(this.previewUrl, ()=>{
            this.width = this.width; // force recalculation of height 
            this.redraw() // force rerender
        });
    };

    public get width(): number {
        return this._width;
    };
    public set width(w: number) {
        this._width = Number(w);
        if (this.keepRatio){
            this._height = Number(w / this._aspectRatio);
        }
    };
    public get height(): number {
        return this._height;
    };
    public set height(h: number) {
        this._height = Number(h);
        if (this.keepRatio){
            this._width = Number(h * this._aspectRatio);
        }
    };
    public keepRatio = true;

    constructor(reportSchema: ReportSchema){
        super(reportSchema);
        this.type = "image";
    }

    public init(): ReportElementImage {
        this.parse({
            location: {
                position: "fixed",
                x: 10,
                y: 10
            },
            width: 20,
            height: 20
        });
        return this;
    }

    public parse(data:any):ReportElementImage {
        super.parse(data);
        this.url = data.url;
        this._width = Number(data.width) || 0;
        this._height = Number(data.height) || 0;
        
        return this;
    }

    public draw(el:ElementRef){
        super.draw(el);
        this.element.nativeElement.style.height = this.height + "mm";
        this.element.nativeElement.style.width = this.width + "mm";
        this.element.nativeElement.style.backgroundImage = 'url(' + this.previewUrl + ')';
        this.element.nativeElement.style.backgroundRepeat = 'no-repeat';
        this.element.nativeElement.style.backgroundSize = '100% 100%';
    }

    public json(): any {
      let res: any = super.json();
      if(!this.url) { return null; }
      res.url = this.url;
      res.width = Number(this.width) || 0;
      res.height = Number(this.height) || 0;
      if(res.width == 0 || res.height == 0) { return null; }
      return res;
    }

    private _recalculateRatio(url, cb?){
        var actualImage = new Image();
        actualImage.src = url;
        actualImage.onload = ()=>{
            this._aspectRatio = actualImage.width /actualImage.height;
            if (!this._aspectRatio){
                this._aspectRatio = 1;
            }
            if (cb){
                cb();
            }
        }
    }
}
