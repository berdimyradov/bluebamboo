/**
 * Created by aleksay on 15.05.2017.
 */
import {
  Component, Input, OnChanges,
  Output, EventEmitter, HostListener, ElementRef, ViewChild, AfterViewInit, ViewEncapsulation,
  HostBinding, OnInit
} from '@angular/core';
import { ReportSchema } from '../types/report-schema';
import { ReportData } from '../types/report-data';
import { ReportElement } from '../types/report-element';
import { ReportBaseElement } from '../types/report-base-element';

@Component({
  selector: 'report-designer-page',
  templateUrl: './report-designer-page.html',
  styleUrls: ['./report-designer-page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ReportDesignerPageComponent implements OnChanges, AfterViewInit, OnInit {
  @ViewChild('sizeElement') private sizeElement: ElementRef;
  @Input('schema') private schema: ReportSchema;
  @Input('data') private data: ReportData;
  @Output('on-selected') private onSelected = new EventEmitter<ReportBaseElement[]>();
  @HostBinding('class.z-depth-1') private classZDepth1 = true;

  private selected: ReportBaseElement[] = [];
  private px2mm = 1;
  private get page(): ReportDesignerPageComponent { return this };

  // region Events
  private state = 'none';
  private startPos: any;
  private rootElement;
  private rootPosition = {};

  constructor(private rootElementLink: ElementRef) { }

  ngOnInit(): void {
    if (this.schema && this.data) {
      this.schema.setData(this.data);
    }
  }

  ngOnChanges(): void {
    if (this.schema && this.data) {
      this.schema.setData(this.data);
    }
  }

  ngAfterViewInit() {
    if(this.sizeElement.nativeElement.getClientRects()[0]) {
      this.px2mm = this.sizeElement.nativeElement.getClientRects()[0].height || 1;
    }
    this.rootElement = this.rootElementLink.nativeElement;
    this.sizeElement.nativeElement.remove();
  }

  @HostListener('mousedown', ['$event']) private mouseDown(e: any) {
    if (e.srcElement === this.rootElement) {
      this.selected = [];
      return;
    }
    if (this.selected.length === 0) { return; }

    this.startPos = {
      x: e.clientX,
      y: e.clientY,
      location: []
    };
    this.selected.forEach((el: ReportElement) => {
      this.startPos.location.push(el.location ? {
        x: Number(el.location.x) || 0,
        y: Number(el.location.y) || 0
      } : null);
    });
  }

  @HostListener('mousemove', ['$event']) private mouseMove(e: any) {
    if (this.selected.length === 0 || !this.startPos) { return; }
    this.state = 'move';
    const deltaX = e.clientX - this.startPos.x;
    const deltaY = e.clientY - this.startPos.y;
    this.selected.forEach((el: ReportElement, pos) => {
      if (el.location) {
        el.location.x = this.startPos.location[pos].x + (deltaX / this.px2mm);
        el.location.y = this.startPos.location[pos].y + (deltaY / this.px2mm);
        el.refreshPostion();
      }
    });
  }

  @HostListener('mouseup', ['$event']) private mouseUp(e: any) {
    setTimeout(() => {
      this.state = 'none';
      this.startPos = undefined;
    });
  }

  public selectElement(el: ReportBaseElement) {
    if (this.state !== 'none') { return; }
    // const i = this.selected.indexOf(el);
    //if (i >= 0) {
    //  this.selected.splice(i, 1);
    //} else {
    this.selected = [];
    if (el) {
      this.selected.push(el);
    }
    //}
    this.onSelected.emit(this.selected)
  }

  public elementIsSelected(el: ReportBaseElement) {
    return this.selected.indexOf(el) >= 0;
  }
}
