/**
    * Created by aleksay on 23.05.2017.
    * Project: tmp_booking
    */
import {ReportElement} from "../../types/report-element";
export function ReportElementEditor(prefix: string = '') {
    let path = prefix.split('.'), keyField = path.pop();

    function getMainObject(obj: any){
        if (prefix == ''){
            return obj;
        }
        let res: any = obj;
        path.forEach((key) => {
            res = (res || {})[key];
        });
        return res;
    }

    return function (target: any, key: string) {
        function getKey(){
            return prefix == '' ? key : keyField;
        }

        Object.defineProperty(target, key, {
            configurable: false,
            get: function() {
                let el: ReportElement = this.element;
                return el ? getMainObject(el)[getKey()] : undefined;
            },
            set: function(v) {
                let el: ReportElement = this.element;
                if (el) {
                    getMainObject(el)[getKey()] = v;
                    el.redraw();
                }
            }
        });
    };
}