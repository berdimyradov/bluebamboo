/**
    * Created by aleksay on 19.05.2017.
    * Project: tmp_booking
    */
import {Component, Input}   from '@angular/core';
import {ReportElementPropertyEditor} from "../../types/report-element-property-editor";
import {ReportElement} from "../../types/report-element";
import {ReportElementEditor} from "../decorator/report-element-editor.decorator";
import {ReportDesignerPageComponent} from "../../page/report-designer-page.component";

@Component({
    selector: 'report-designer-location-editor',
    templateUrl: './report-designer-location-editor.html'
})
export class ReportDesignerLocationEditorComponent{
    @Input('element') private element: ReportElement;
    @Input('show') private show: ReportElementPropertyEditor;
    @Input('page') private page: ReportDesignerPageComponent;

    @ReportElementEditor('location.x') private x: number;
    @ReportElementEditor('location.y') private y: number;
    @ReportElementEditor() private width: number;
    @ReportElementEditor() private height: number;
    @ReportElementEditor() private length: number;
    @ReportElementEditor('width') private thickness: number;
    @ReportElementEditor('keepRatio') private keepRatio: boolean;
}
