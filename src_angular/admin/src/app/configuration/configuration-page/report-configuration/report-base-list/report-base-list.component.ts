import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CommonPageComponent} from "../../../../common";
import {ReportConfigurationService} from "../../services/report-configuration.service";

@Component({
  selector: 'admin-report-base-list',
  templateUrl: './report-base-list.component.html',
  styleUrls: ['./report-base-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ReportBaseListComponent implements OnInit {
  public list: any[] = [];

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private reportService: ReportConfigurationService
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.REPORT-BASE-LIST.TITLE';
    this.reportService.getBaseList().subscribe( result => this.list = result.map(it => this.initRow(it)) );
  }

  private initRow(row: any): any {
    row.click = () => {
      this.router.navigate(['../-'+row.id], {relativeTo: this.activeRoute});
    };
    return row;
  }

  public back() {
    this.router.navigate(['..'], { relativeTo: this.activeRoute });
  }
}
