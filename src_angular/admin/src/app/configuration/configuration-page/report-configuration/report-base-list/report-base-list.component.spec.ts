import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportBaseListComponent } from './report-base-list.component';

describe('ReportBaseListComponent', () => {
  let component: ReportBaseListComponent;
  let fixture: ComponentFixture<ReportBaseListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportBaseListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportBaseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
