import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";

import { BbGridComponent, ArrayDataProvider } from "@bluebamboo/common-ui";

import { CommonPageComponent, routeAnimationOpacity, DialogService } from "app/common";
import { ReportConfigurationService } from "../services/report-configuration.service";
import { Report } from "../types";

@Component({
  selector: 'admin-report-configuration',
  templateUrl: './report-configuration.component.html',
  styleUrls: ['./report-configuration.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class ReportConfigurationComponent implements OnInit {
  private static SearchTimeOut: number = 300;
  private _search: string;
  private _searchTimeOut: any;
  @ViewChild('grid') public grid: BbGridComponent;

  public gridModules: any [] = [TranslateModule];
  public templateAction: string = `
                                  <a class="btn-floating btn-sm white btn-command"
                                      (click)="row.edit($event)" data-toggle="tooltip" data-placement="top"
                                      title="{{\'PAGE.REPORT-CONFIGURATION.ACTIONS.EDIT\'|translate}}">
                                      <i class="fa"
                                        [ngClass]="{'fa-pencil': row.editable, 'fa-search-plus': !row.editable}">
                                      </i>
                                    </a>
                                    <a class="btn-floating btn-sm white btn-command"
                                    (click)="row.settings($event)"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="{{\'PAGE.REPORT-CONFIGURATION.ACTIONS.SETTINGS\'|translate}}">
                                    <i class="fa fa-cog"></i></a><a class="btn-floating btn-sm white btn-command"
                                    (click)="row.duplicate($event)"
                                    data-toggle="tooltip" data-placement="top"
                                    title="{{\'PAGE.REPORT-CONFIGURATION.ACTIONS.DUPLICATE\'|translate}}">
                                    <i class="fa fa-clone"></i></a>
                                    <a class="btn-floating btn-sm white btn-command"
                                    (click)="row.delete($event)" data-toggle="tooltip"
                                    data-placement="top" title="{{\'PAGE.REPORT-CONFIGURATION.ACTIONS.DELETE\'|translate}}">
                                    <i class="fa fa-trash"></i></a>`;
  public templateActive: string = '<mdb-checkbox [ngModel]="row.active" readonly="true"></mdb-checkbox>';
  public showMainScreen: boolean = true;
  public list: Report[] =[];
  public editRow: Report;
  public get search(): string {
    return this._search;
  }
  public set search(v: string) {
    this._search = v;
    this.startSearch(v);
  }
  public reportRow: Element;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private reportService: ReportConfigurationService,
    private dialogService: DialogService
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.REPORT-CONFIGURATION.TITLE';
    this.refresh();
  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public gridRowClick(row: Report) {
  }

  public onActivate(){
    this.showMainScreen = false;
  }

  public onDeactivate() {
    this.showMainScreen = true;
    this.commonPage.title = 'PAGE.REPORT-CONFIGURATION.TITLE';
    this.refresh();
  }

  public apply() {
    this.showRows();
    this.reportService.update(this.editRow)
      .subscribe(
        () => {
          this.editRow = null;
          this.refresh();
        }
      )
  }

  public cancel() {
    this.showRows();
    this.editRow = null;
  }

  public executeAdd() {
    this.router.navigate(['./base'], { relativeTo: this.activeRoute });
  }

  private refresh() {
    this.reportService.getList().subscribe( result => {
      this.list = result.map(it => this.initRow(it));
      this.startSearch(this._search, 0);
    });
  }

  private initRow(row: any): any {
    row.edit = ($event) => {
      $event.stopPropagation();
      this.router.navigate(['./' + row.id], {relativeTo: this.activeRoute});
    };
    row.settings = ($event) => {
      $event.stopPropagation();
      const tempRow = JSON.parse(JSON.stringify(row));
      const rowGroupId = tempRow.group_id;
      this.reportService.getRoles(rowGroupId).subscribe( (roles) => {
        tempRow.roles = roles;
        this.editRow = (new Report()).parse(tempRow);
        this.hideRows($event.target);
      });
    };
    row.duplicate = ($event) => {
      $event.stopPropagation();
      this.reportService.duplicate(row.id)
        .subscribe(() =>this.refresh())
    };
    row.delete = ($event) => {
      $event.stopPropagation();
      this.dialogService.confirm('PAGE.REPORT-CONFIGURATION.CONFIRM-DELETE','PAGE.REPORT-CONFIGURATION.CONFIRM-YES','PAGE.REPORT-CONFIGURATION.CONFIRM-NO')
        .then((res: Boolean) => {
          if (res) {
            this.reportService.delete(row.id)
              .subscribe(() =>this.refresh());
          }
        });

    };
    return row;
  }

  private hideRows(buttonElement: Element) {
    //this.invoicePaid = new InvoicePaid();
    this.reportRow = buttonElement.parentElement.parentElement.parentElement;
    this.reportRow.classList.add('selected-row');
    this.grid.disabled = true;
  }

  private showRows() {
    this.reportRow.classList.remove('selected-row');
    this.reportRow = null;
    this.grid.disabled = false;
  }

  private startSearch(term: string, wait: number = ReportConfigurationComponent.SearchTimeOut) {
    if (this._searchTimeOut) {
      clearTimeout(this._searchTimeOut);
    }
    let ctrl = this;
    this._searchTimeOut = setTimeout(() => {
      if(!ctrl.grid) { return; }
      (ctrl.grid.dataProvider as ArrayDataProvider).setFullTextFilter(term, ['title', 'description']);
    }, wait);
  }
}
