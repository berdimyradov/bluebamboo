import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BookingReport } from '../types';
import { CustomerConfigurationService } from '../services';

@Injectable()
export class BookingReportResolver implements Resolve<BookingReport> {
  constructor(
    private customerConfigurationService: CustomerConfigurationService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<BookingReport> {
    const id: string = route.paramMap.get('reportId');

    if (id === 'add') {
      return Observable.of(new BookingReport());
    }

    return this.customerConfigurationService.getReport(Number(id));
  }
}
