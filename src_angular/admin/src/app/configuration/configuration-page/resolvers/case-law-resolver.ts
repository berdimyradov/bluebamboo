import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CustomerConfigurationService } from '../services';

@Injectable()
export class CaseLawListResolver implements Resolve<any[]> {
  constructor(private customerConfigurationService: CustomerConfigurationService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any[]> {
    return this.customerConfigurationService.getCaseLawList();
  }
}
