import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CustomerConfigurationService } from '../services';
import { Customer } from '../../../common/types/customer';

@Injectable()
export class CustomerResolver implements Resolve<Customer> {
  constructor(
    private customerConfigurationService: CustomerConfigurationService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Customer> {
    const id: string = route.paramMap.get('id');

    if (id === 'add') {
      return Observable.of(new Customer());
    }

    return this.customerConfigurationService.get(Number(id));
  }
}
