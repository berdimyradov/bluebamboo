import {Injectable} from '@angular/core';
import {Location} from '../../../common';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {LocationConfigurationService} from '../services';

@Injectable()
export class LocationResolver implements Resolve<Location> {
  constructor (private locationConfiguration : LocationConfigurationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Location | Observable<Location> | Promise<Location> {
    const id: string = route.paramMap.get('id');

    if (id === 'add') {
      return Observable.of(new Location());
    }

    return this.locationConfiguration.get(parseInt(id));
  }
}
