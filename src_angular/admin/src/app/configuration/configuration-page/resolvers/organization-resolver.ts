import {Injectable} from '@angular/core';
import {Organization} from '../../../common';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {OrganizationConfigurationService} from '../services';

@Injectable()
export class OrganizationResolver implements Resolve<Organization> {
  constructor (private organizationConfiguration: OrganizationConfigurationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Organization | Observable<Organization> | Promise<Organization> {
    return this.organizationConfiguration.get();
  }
}
