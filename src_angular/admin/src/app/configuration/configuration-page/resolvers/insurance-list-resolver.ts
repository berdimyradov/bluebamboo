import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Insurance } from '../types';
import { InsuranceConfigurationService } from '../services';

@Injectable()
export class InsuranceListResolver implements Resolve<Insurance[]> {
  constructor(private insuranceConfiguration: InsuranceConfigurationService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Insurance[]> {
    return this.insuranceConfiguration.getList();
  }
}
