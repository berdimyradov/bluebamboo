import {Injectable} from "@angular/core";
import {Room} from '../../../common';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {RoomConfigurationService} from "../services";

@Injectable()
export class RoomResolver implements Resolve<Room> {
  constructor (private roomConfiguration : RoomConfigurationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Room | Observable<Room> | Promise<Room> {
    const id: string = route.paramMap.get('id');

    if (id === 'add') {
      return Observable.of(new Room());
    }

    return this.roomConfiguration.get(parseInt(id));
  }
}
