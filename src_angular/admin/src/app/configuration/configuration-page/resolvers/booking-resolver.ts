import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CustomerConfigurationService } from '../services';

@Injectable()
export class BookingResolver implements Resolve<any> {
  constructor(
    private customerConfigurationService: CustomerConfigurationService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const id: string = route.paramMap.get('bookingId');

    return this.customerConfigurationService.getBooking(Number(id));
  }
}
