import { Injectable } from '@angular/core';;
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { EmailTemplateConfigurationService } from '../services';
import { EmailTemplateRole } from '../types';

@Injectable()
export class EmailTemplateListResolver implements Resolve<EmailTemplateRole[]> {
  constructor (private emailTemplateConfigurationService: EmailTemplateConfigurationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<EmailTemplateRole[]> {
    return this.emailTemplateConfigurationService.getRoleList();
  }
}
