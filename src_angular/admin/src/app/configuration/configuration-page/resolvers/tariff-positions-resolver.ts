import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {ProductConfigurationService} from "../services";
import {TariffPositions} from "../types";

@Injectable()
export class TariffPositionsResolver implements Resolve<TariffPositions[]> {
  constructor (private productConfiguration : ProductConfigurationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): TariffPositions[] | Observable<TariffPositions[]> | Promise<TariffPositions[]> {
    return this.productConfiguration.getTariffPositions();
  }

}

