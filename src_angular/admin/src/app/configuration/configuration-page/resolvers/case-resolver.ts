import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Case } from '../types';
import { CustomerConfigurationService } from '../services';

@Injectable()
export class CaseResolver implements Resolve<Case> {
  constructor(
    private customerConfigurationService: CustomerConfigurationService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Case> {
    const id: string = route.paramMap.get('caseId');

    if (id === 'add') {
      return Observable.of(new Case({
        type: 1,
        therapy_type: 1,
        payment_type: 1,
        diagnosis_type: 1,
        law: 5,
        isOpen: true,
        isCurrent: true
      }));
    }

    return this.customerConfigurationService.getCase(Number(id));
  }
}
