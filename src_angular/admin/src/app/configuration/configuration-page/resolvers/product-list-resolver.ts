import {Injectable} from "@angular/core";
import {Product} from '../types';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {ProductConfigurationService} from "../services";

@Injectable()
export class ProductListResolver implements Resolve<Product[]> {
  constructor (private productConfiguration : ProductConfigurationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Product[] | Observable<Product[]> | Promise<Product[]> {
    return this.productConfiguration.getSelectList();
  }

}

