import {Injectable} from '@angular/core';
import {Report} from '../types';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {ReportConfigurationService} from "../services";

@Injectable()
export class ReportResolver implements Resolve<Report> {
  constructor (private reportConfiguration : ReportConfigurationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Report | Observable<Report> | Promise<Report> {
    let id: number = parseInt(route.paramMap.get('id'));
    return new Promise((resolve, reject) => {
      this.reportConfiguration.getPreviewData().subscribe( data => {
        if (id < 0) {
          this.reportConfiguration.getBase(-1*id).subscribe(info => {
            const report: Report = new Report();
            report.data = data;
            report.schema = info.template;
            report.info = info;
            report.isNew = id >= 0 ? false : true;
            resolve(report);
          }, error => reject(error))
        } else {
          this.reportConfiguration.get(id).subscribe(info => {
            const report: Report = new Report();
            report.data = data;
            report.schema = info.template;
            report.info = info;
            report.isNew = id >= 0 ? false : true;
            resolve(report);
          }, error => reject(error))
        }
      }, error => reject(error));
    });

  }

}
