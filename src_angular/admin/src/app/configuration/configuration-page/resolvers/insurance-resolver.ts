import {Injectable} from "@angular/core";
import {Insurance} from '../types';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {InsuranceConfigurationService} from "../services";

@Injectable()
export class InsuranceResolver implements Resolve<Insurance> {
  constructor (private insuranceConfiguration : InsuranceConfigurationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Insurance | Observable<Insurance> | Promise<Insurance> {
    const id: string = route.paramMap.get('id');

    if (id === 'add') {
      return Observable.of(new Insurance());
    }

    return this.insuranceConfiguration.get(parseInt(id));
  }
}
