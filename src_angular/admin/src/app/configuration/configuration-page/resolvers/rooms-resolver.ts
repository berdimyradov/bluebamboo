import {Injectable} from "@angular/core";
import {Room} from '../../../common';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {RoomConfigurationService} from "../services";

@Injectable()
export class RoomsResolver implements Resolve<Room[]> {
  constructor (private roomConfiguration : RoomConfigurationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Room[] | Observable<Room[]> | Promise<Room[]> {
    return this.roomConfiguration.getList();
  }

}
