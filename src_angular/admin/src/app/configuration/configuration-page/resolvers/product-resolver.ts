import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {ProductConfigurationService} from "../services";

@Injectable()
export class ProductResolver implements Resolve<any> {
  constructor (private productConfiguration : ProductConfigurationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any | Observable<any> | Promise<any> {
    const id: string = route.paramMap.get('id');

    if (id === 'add') {
      return Observable.of({
        productparts: [{}],
        rooms: [],
        employees: [],
        active: true,
        online_booking_available: false,        
        type: 10
      });
    }

    return this.productConfiguration.get(parseInt(id));
  }
}

