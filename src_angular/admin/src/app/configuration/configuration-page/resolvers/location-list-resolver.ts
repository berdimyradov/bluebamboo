import {Injectable} from '@angular/core';
import {Location} from '../../../common';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {LocationConfigurationService} from '../services';

@Injectable()
export class LocationListResolver implements Resolve<Location[]> {
  constructor (private locationConfiguration : LocationConfigurationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Location[] | Observable<Location[]> | Promise<Location[]> {
    return this.locationConfiguration.getList();
  }

}
