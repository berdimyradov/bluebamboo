import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CustomerConfigurationService } from '../services';
import { Dossier } from '../types';

@Injectable()
export class DossierResolver implements Resolve<Dossier> {
  constructor(
    private customerConfigurationService: CustomerConfigurationService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Dossier> {
    const id: string = route.paramMap.get('id');

    return this.customerConfigurationService.getDossier(Number(id));
  }
}
