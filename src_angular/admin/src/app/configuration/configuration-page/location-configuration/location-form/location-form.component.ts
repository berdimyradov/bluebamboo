import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {routeAnimationOpacity, Location} from "../../../../common";
import {ActivatedRoute, Router} from "@angular/router";
import {CommonPageComponent, DialogService} from "../../../../common";
import {LocationConfigurationService} from "../../services";
import {OrganizationConfigurationService} from '../../services';

@Component({
  selector: 'admin-location-form',
  templateUrl: './location-form.component.html',
  styleUrls: ['./location-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class LocationFormComponent implements OnInit {
  public location: Location;
  public isView: boolean;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private dialog: DialogService,
    private locationService: LocationConfigurationService,
    private organizationService: OrganizationConfigurationService
  ) { }

  ngOnInit() {
    this.location = this.activeRoute.snapshot.data.location;
    this.commonPage.title = 'PAGE.LOCATION-FORM.TITLE.' + (this.location.id ? 'EDIT' : 'NEW');
    this.isView = !!this.location.id;
  }

  public onFillLocationData() {
    this.organizationService.get().subscribe( (data) => {
      this.location.zip = data.zip;
      this.location.street = data.street;
      this.location.city = data.city;
      this.location.country = data.country;
      this.location.street = data.street;
    })
  }
  public back(force = false) {
    if (force || this.isView || !this.location.id) {
      this.router.navigate(['../'], {relativeTo: this.activeRoute});
    } else {
      if (this.location.id) {
        this.isView = true;
      }
    }
  }

  public save() {
    this.locationService.save(this.location)
      .subscribe(
        result => this.back()
      );
  }

  public delete() {
    this.dialog.confirm('PAGE.LOCATION-FORM.CONFIRM-DELETE', 'PAGE.LOCATION-FORM.CONFIRM-YES', 'PAGE.LOCATION-FORM.CONFIRM-NO')
      .then((res: Boolean) => {
        if (res) {
          this.locationService.delete(this.location).subscribe(
            result => this.back(true)
          );
        }
      });
  }
}
