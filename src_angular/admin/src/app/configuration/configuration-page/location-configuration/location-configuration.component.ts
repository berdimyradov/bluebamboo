import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {CommonPageComponent, routeAnimationOpacity, CommonListItem, Location} from '../../../common';
import {ActivatedRoute, Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {LocationConfigurationService} from "../services";

@Component({
  selector: 'app-location-configuration',
  templateUrl: './location-configuration.component.html',
  styleUrls: ['./location-configuration.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class LocationConfigurationComponent implements OnInit {
  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private locationService: LocationConfigurationService
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.LOCATION-CONFIGURATION.TITLE';
  }

  public getList = ():Observable<CommonListItem[]> => {
    return new Observable<CommonListItem[]>(observer => {
      this.locationService.getList().subscribe(
        result => {
          observer.next(result.map((it: Location) => {
            let item: CommonListItem = new CommonListItem();
            item.id = it.id;
            item.title = it.title;
            item.description = it.description;
            return item;
          }));
          observer.complete();
        }
      )
    });
  };

  public edit(location: CommonListItem) {
    this.router.navigate(['./' + location.id], {relativeTo: this.activeRoute});
  }

  public add() {
    this.router.navigate(['./add'], { relativeTo: this.activeRoute });
  }
}
