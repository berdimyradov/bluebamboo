import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {CommonMenuItem} from "../common";

@Injectable()
export class ConfigurationService {
  private _modules: CommonMenuItem[] = [
    new CommonMenuItem().parse({
      code: 'organization',
      icon: 'fa-vcard-o',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'location',
      icon: 'fa-map-marker',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'room',
      icon: 'fa-cube',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'employee',
      icon: 'fa-users',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'service',
      icon: 'fa-shopping-basket',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'insurance',
      icon: 'fa-medkit',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'hourly-rates',
      icon: 'fa-money',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'report',
      icon: 'fa-file-pdf-o',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'email-templates',
      icon: 'fa-file-text-o',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'import-data',
      icon: 'fa-upload',
      implement: true
    })
  ];

  constructor() { }

  public getListModules(): Observable<CommonMenuItem[]> {
    return (new Observable<CommonMenuItem[]>(observable => observable.next(this._modules)));
  }
}
