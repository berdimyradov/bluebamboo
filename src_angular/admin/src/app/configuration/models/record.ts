export interface Record {
  id: number;
  import_id: number;
  client_id: number;
  import: boolean;
  imported: boolean;
  duplication: boolean;
  col01: string;
  col01_imported: string;
  col02: string;
  col02_imported: string;
  col03: string;
  col03_imported: string;
  col04: string;
  col04_imported: string;
  col05: string;
  col05_imported: string;
  col06: string;
  col06_imported: string;
  col07: string;
  col07_imported: string;
  col08: string;
  col08_imported: string;
  col09: string;
  col09_imported: string;
  col10: string;
  col10_imported: string;
  col11: string;
  col11_imported: string;
  col12: string;
  col12_imported: string;
  col13: string;
  col13_imported: string;
  col14: string;
  col14_imported: string;
  col15: string;
  col15_imported: string;
  col16: string;
  col16_imported: string;
  col17: string;
  col17_imported: string;
  col18: string;
  col18_imported: string;
  col19: string;
  col19_imported: string;
  col20: string;
  col20_imported: string;
  col01_valid: boolean;
  col02_valid: boolean;
  col03_valid: boolean;
  col04_valid: boolean;
  col05_valid: boolean;
  col06_valid: boolean;
  col07_valid: boolean;
  col08_valid: boolean;
  col09_valid: boolean;
  col10_valid: boolean;
  col11_valid: boolean;
  col12_valid: boolean;
  col13_valid: boolean;
  col14_valid: boolean;
  col15_valid: boolean;
  col16_valid: boolean;
  col17_valid: boolean;
  col18_valid: boolean;
  col19_valid: boolean;
  col20_valid: boolean;
}

export class RecordRow implements Record {
  id: number;
  import_id: number;
  client_id: number;
  import: boolean;
  imported: boolean;
  duplication: boolean;
  col01: string;
  col01_imported: string;
  col02: string;
  col02_imported: string;
  col03: string;
  col03_imported: string;
  col04: string;
  col04_imported: string;
  col05: string;
  col05_imported: string;
  col06: string;
  col06_imported: string;
  col07: string;
  col07_imported: string;
  col08: string;
  col08_imported: string;
  col09: string;
  col09_imported: string;
  col10: string;
  col10_imported: string;
  col11: string;
  col11_imported: string;
  col12: string;
  col12_imported: string;
  col13: string;
  col13_imported: string;
  col14: string;
  col14_imported: string;
  col15: string;
  col15_imported: string;
  col16: string;
  col16_imported: string;
  col17: string;
  col17_imported: string;
  col18: string;
  col18_imported: string;
  col19: string;
  col19_imported: string;
  col20: string;
  col20_imported: string;
  col01_valid: boolean;
  col02_valid: boolean;
  col03_valid: boolean;
  col04_valid: boolean;
  col05_valid: boolean;
  col06_valid: boolean;
  col07_valid: boolean;
  col08_valid: boolean;
  col09_valid: boolean;
  col10_valid: boolean;
  col11_valid: boolean;
  col12_valid: boolean;
  col13_valid: boolean;
  col14_valid: boolean;
  col15_valid: boolean;
  col16_valid: boolean;
  col17_valid: boolean;
  col18_valid: boolean;
  col19_valid: boolean;
  col20_valid: boolean;

  public parse(data: any): RecordRow {
    const names: string[] = [
        'id',
        'import_id',
        'client_id',
        'import',
        'imported',
        'duplication',
        'col01',
        'col01_imported',
        'col02',
        'col02_imported',
        'col03',
        'col03_imported',
        'col04',
        'col04_imported',
        'col05',
        'col05_imported',
        'col06',
        'col06_imported',
        'col07',
        'col07_imported',
        'col08',
        'col08_imported',
        'col09',
        'col09_imported',
        'col10',
        'col10_imported',
        'col11',
        'col11_imported',
        'col12',
        'col12_imported',
        'col13',
        'col13_imported',
        'col14',
        'col14_imported',
        'col15',
        'col15_imported',
        'col16',
        'col16_imported',
        'col17',
        'col17_imported',
        'col18',
        'col18_imported',
        'col19',
        'col19_imported',
        'col20',
        'col20_imported',
        'col01_valid',
        'col02_valid',
        'col03_valid',
        'col04_valid',
        'col05_valid',
        'col06_valid',
        'col07_valid',
        'col08_valid',
        'col09_valid',
        'col10_valid',
        'col11_valid',
        'col12_valid',
        'col13_valid',
        'col14_valid',
        'col15_valid',
        'col16_valid',
        'col17_valid',
        'col18_valid',
        'col19_valid',
        'col20_valid',
      ]
    ;

    names.forEach((name) => this[name] = data[name] || this[name]);

    return this;
  }
}