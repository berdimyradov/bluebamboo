import {FieldRow} from "./field";

export interface Target {
  id: number;
  label: string;
  code: string;
}

export class TargetRow implements Target {
  public id: number;
  public label: string;
  public code: string;
  public fields: FieldRow[];

  public parse(data: any): TargetRow {
    const names: string[] = [
      'id',
      'label',
      'code'
    ];

    names.forEach((name) => this[name] = data[name] || this[name]);

    return this;
  }
}