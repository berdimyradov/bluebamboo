import {TargetRow} from "./target";

export interface Field {
  id: number;
  label: string;
  code: string;
  type: string;
  target: TargetRow;

}

export class FieldRow implements Field {
  public id: number;
  public label: string;
  public code: string;
  public type: string;
  public target: TargetRow;

  public parse(data: any): FieldRow {
    const names: string[] = [
      'id',
      'label',
      'code',
      'type',
      'target'
    ];

    names.forEach((name) => this[name] = data[name] || this[name]);

    return this;
  }
}