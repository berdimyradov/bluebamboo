export class Accounts {
	public id: number;

	public name: string;
	public section: string;
	public maingroup: string;
	public subgroup: string;
	public parent: number;
	public number: string;
	public opening: number;
	public debit: number;
	public credit: number;
	public balance: number;
	public budget: number;
	public budget_difference: number;
	public budget_prior: number;
	public prior: number;
	public prior_difference: number;
	public period_begin: Date;
	public period_end: Date;
	public period_credit: number;
	public period_debit: number;
	public period_total: number;
	public notes: string;
	public client_id: number;
	public accountscodes_id: number;

	public parse( data: any ): Accounts {
		this.id = data.id || this.id;

		this.name = data.name || this.name;
		this.section = data.section || this.section;
		this.maingroup = data.maingroup || this.maingroup;
		this.subgroup = data.subgroup || this.subgroup;
		this.parent = data.parent || this.parent;
		this.number = data.number || this.number;
		this.opening = data.opening || this.opening;
		this.debit = data.debit || this.debit;
		this.credit = data.credit || this.credit;
		this.balance = data.balance || this.balance;
		this.budget = data.budget || this.budget;
		this.budget_difference = data.budget_difference || this.budget_difference;
		this.budget_prior = data.budget_prior || this.budget_prior;
		this.prior = data.prior || this.prior;
		this.prior_difference = data.prior_difference || this.prior_difference;
		this.period_begin = data.period_begin || this.period_begin;
		this.period_end = data.period_end || this.period_end;
		this.period_credit = data.period_credit || this.period_credit;
		this.period_debit = data.period_debit || this.period_debit;
		this.period_total = data.period_total || this.period_total;
		this.notes = data.notes || this.notes;
		this.client_id = data.client_id || this.client_id;
		this.accountscodes_id = data.accountscodes_id || this.accountscodes_id;
		return this;
	}
}
