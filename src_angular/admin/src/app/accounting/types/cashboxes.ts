export class Cashboxes {
	public id: number;

	public name: string;
	public balance: number;
	public notes: string;
	public client_id: number;
	public account_id: number;

	public parse( data: any ): Cashboxes {
		this.id = data.id || this.id;

		this.name = data.name || this.name;
		this.balance = data.balance || this.balance;
		this.notes = data.notes || this.notes;
		this.client_id = data.client_id || this.client_id;
		this.account_id = data.account_id || this.account_id;
		return this;
	}
}
