export class Currencies {
	public id: number;

	public name: string;
	public code: string;
	public client_id: number;

	public parse( data: any ): Currencies {
		this.id = data.id || this.id;

		this.name = data.name || this.name;
		this.code = data.code || this.code;
		this.client_id = data.client_id || this.client_id;
		return this;
	}
}
