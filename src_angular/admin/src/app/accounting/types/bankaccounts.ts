export class Bankaccounts {
	public id: number;

	public name: string;
	public notes: string;
	public client_id: number;
	public account_id: number;

	public parse( data: any ): Bankaccounts {
		this.id = data.id || this.id;

		this.name = data.name || this.name;
		this.notes = data.notes || this.notes;
		this.client_id = data.client_id || this.client_id;
		this.account_id = data.account_id || this.account_id;
		return this;
	}
}
