import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";

import { AccountingService } from "app/accounting/services/accounting.service";
import { Accounts } from "app/accounting/types/accounts";

@Injectable()

export class AccountsformResolver implements Resolve<Accounts> {

  constructor (private service : AccountingService) {}

  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Accounts | Observable<Accounts> | Promise<Accounts> {
    const id: string = route.paramMap.get('id');
    if (id === 'add') return Observable.of(new Accounts());
    return this.service.getAccounts(parseInt(id));
  }

}
