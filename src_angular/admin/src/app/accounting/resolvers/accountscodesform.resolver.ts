import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";

import { AccountingService } from "app/accounting/services/accounting.service";
import { Accountcodes } from "app/accounting/types/accountcodes";

@Injectable()

export class AccountscodesformResolver implements Resolve<Accountcodes> {

  constructor (private service : AccountingService) {}

  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Accountcodes | Observable<Accountcodes> | Promise<Accountcodes> {
    const id: string = route.paramMap.get('id');
    if (id === 'add') return Observable.of(new Accountcodes());
    return this.service.getAccountcodes(parseInt(id));
  }

}
