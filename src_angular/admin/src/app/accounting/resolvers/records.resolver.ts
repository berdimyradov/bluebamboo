import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";

import { AccountingService } from "app/accounting/services/accounting.service";
import { Records } from "app/accounting/types/records";

@Injectable()

export class RecordsResolver implements Resolve<Records> {

  constructor (private service : AccountingService) {}

  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Records | Observable<Records> | Promise<Records> {
    const id: string = route.paramMap.get('id');
    if (id === 'add') return Observable.of(new Records());
    return this.service.getRecords(parseInt(id));
  }

}
