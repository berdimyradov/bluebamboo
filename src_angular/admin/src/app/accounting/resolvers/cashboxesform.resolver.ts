import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";

import { AccountingService } from "app/accounting/services/accounting.service";
import { Cashboxes } from "app/accounting/types/cashboxes";

@Injectable()

export class CashboxesformResolver implements Resolve<Cashboxes> {

  constructor (private service : AccountingService) {}

  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Cashboxes | Observable<Cashboxes> | Promise<Cashboxes> {
    const id: string = route.paramMap.get('id');
    if (id === 'add') return Observable.of(new Cashboxes());
    return this.service.getCashboxes(parseInt(id));
  }

}
