import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";

import { AccountingService } from "app/accounting/services/accounting.service";
import { Bankaccounts } from "app/accounting/types/bankaccounts";

@Injectable()

export class BankaccountsformResolver implements Resolve<Bankaccounts> {

  constructor (private service : AccountingService) {}

  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Bankaccounts | Observable<Bankaccounts> | Promise<Bankaccounts> {
    const id: string = route.paramMap.get('id');
    if (id === 'add') return Observable.of(new Bankaccounts());
    return this.service.getBankaccounts(parseInt(id));
  }

}
