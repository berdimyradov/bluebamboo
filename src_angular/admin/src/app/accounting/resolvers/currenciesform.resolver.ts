import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";

import { AccountingService } from "app/accounting/services/accounting.service";
import { Currencies } from "app/accounting/types/currencies";

@Injectable()

export class CurrenciesformResolver implements Resolve<Currencies> {

  constructor (private service : AccountingService) {}

  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Currencies | Observable<Currencies> | Promise<Currencies> {
    const id: string = route.paramMap.get('id');
    if (id === 'add') return Observable.of(new Currencies());
    return this.service.getCurrencies(parseInt(id));
  }

}
