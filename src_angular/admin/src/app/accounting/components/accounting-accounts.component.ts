import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { TranslateModule } from "@ngx-translate/core";
import { ActivatedRoute, Router } from "@angular/router";

import { BbGridComponent, ArrayDataProvider } from "@bluebamboo/common-ui";
import { CommonPageComponent, routeAnimationOpacity, DialogService} from "app/common";

import { AccountingService } from "app/accounting/services/accounting.service";
import { Accounts } from "app/accounting/types/accounts";

@Component({
	selector:			'app-accounting-accounts',
	templateUrl:		'./accounting-accounts.component.html',
	styleUrls:			['./accounting-accounts.component.scss'],
	encapsulation:		ViewEncapsulation.None,
	host:				{'[@routeAnimationOpacity]': 'true'},
	animations:			[routeAnimationOpacity]
})

export class AccountingAccountsComponent implements OnInit {
  @ViewChild('grid') public grid: BbGridComponent;
  public gridModules: any [] = [TranslateModule];
  public templateAction: string = `
                                  <a class="btn-floating btn-sm white btn-command"
                                      (click)="row.edit($event)" data-toggle="tooltip" data-placement="top"
                                      title="bearbeiten">
                                      <i class="fa fa-pencil">
                                      </i>
                                    </a>

                                    <a class="btn-floating btn-sm white btn-command"
                                    (click)="row.delete($event)" data-toggle="tooltip"
                                    data-placement="top" title="löschen">
                                    <i class="fa fa-trash"></i></a>`;

  public list: Accounts [] = [];
  public editRow: Accounts;
  public itemRow: Element;

	/* -------------------------------------------------------------------------------------
	 * constructor
	 * ---------------------------------------------------------------------------------- */

	constructor(
		private router: Router,
		private activeRoute: ActivatedRoute,
		private dialogService: DialogService,
		private service: AccountingService,
		private commonPage: CommonPageComponent
	)
	{
	}

	/* -------------------------------------------------------------------------------------
	 * ngOnInit
	 * ---------------------------------------------------------------------------------- */

	ngOnInit() {
		this.commonPage.title = 'PAGE.EXPENSES.DESKTOP.PAGE_TITLE';
    	this.refresh();
	}

	/* -------------------------------------------------------------------------------------
	 * gridRowClick
	 * ---------------------------------------------------------------------------------- */

  public gridRowClick(row: Accounts) {}

	/* -------------------------------------------------------------------------------------
	 * apply
	 * ---------------------------------------------------------------------------------- */

  public apply() {
    this.showGrid();
    this.service.saveAccounts(this.editRow)
      .subscribe(
        () => {
          this.editRow = null;
          this.refresh();
        }
      )
  }

	/* -------------------------------------------------------------------------------------
	 * cancel CTA
	 * ---------------------------------------------------------------------------------- */

  public cancelCTA() {
    this.showGrid();
    this.editRow = null;
  }

	/* -------------------------------------------------------------------------------------
	 * back CTA
	 * ---------------------------------------------------------------------------------- */

  public backCTA() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

	/* -------------------------------------------------------------------------------------
	 * add entry CTA
	 * ---------------------------------------------------------------------------------- */

  public addCTA() {
    this.router.navigate(['./form/add'], { relativeTo: this.activeRoute });
  }

	/* -------------------------------------------------------------------------------------
	 * refresh from server
	 * ---------------------------------------------------------------------------------- */

  private refresh() {
    this.service.getAccountsList().subscribe(
    	result => { this.list = result.map(it => this.initRow(it)); }
    );
  }

	/* -------------------------------------------------------------------------------------
	 * show grid
	 * ---------------------------------------------------------------------------------- */

  private showGrid () {
    this.itemRow.classList.remove('selected-row');
    this.itemRow = null;
    this.grid.disabled = false;
  }

	/* -------------------------------------------------------------------------------------
	 * init row
	 * ---------------------------------------------------------------------------------- */

  private initRow(row: any): any {
    row.edit = ($event) => {
      $event.stopPropagation();
      this.router.navigate(['./form/' + row.id], {relativeTo: this.activeRoute});
    };

    row.delete = ($event) => {
      $event.stopPropagation();
      this.dialogService.confirm('EXPENSES.DELETE-MESSAGE','EXPENSES.DELETE-YES','EXPENSES.DELETE-NO')
        .then((res: Boolean) => {
          if (res) {
            this.service.deleteAccounts(row.id).subscribe(() => this.refresh());
          }
        });

    };

    return row;
  }

}
