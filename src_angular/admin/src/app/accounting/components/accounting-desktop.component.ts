import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonMenuItem } from "../../common";

@Component({
	selector:			'app-accounting-desktop',
	templateUrl:		'./accounting-desktop.component.html',
	styleUrls:			['./accounting-desktop.component.scss'],
	encapsulation:		ViewEncapsulation.None,
	host:				{'[@routeAnimationOpacity]': 'true'},
	animations:			[routeAnimationOpacity]
})

export class AccountingDesktopComponent implements OnInit {

	public modules: CommonMenuItem[] = [

			new CommonMenuItem().parse({
				code: 'records',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'balance',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'incomestatement',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'journal',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'accounts',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'openingbalance',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'balancing',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'vat',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'currencies',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'bankaccounts',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'cashboxes',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'accountcodes',
				icon: 'fa-key',
				implement: true
			}),


	];

	constructor(
	  private commonPage: CommonPageComponent
	)
	  {}

	ngOnInit() {
		this.commonPage.title = 'PAGE.ACCOUNTING.DESKTOP.PAGE_TITLE';
	}
}
