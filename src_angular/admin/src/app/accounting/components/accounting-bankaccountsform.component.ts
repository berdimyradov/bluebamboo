import { Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AccountingService } from "app/accounting/services/accounting.service";
import { Bankaccounts } from "app/accounting/types/bankaccounts";

@Component({
  selector:       'app-accounting-bankaccountsform-form',
  templateUrl:    './accounting-bankaccountsform.component.html',
  styleUrls:      ['./accounting-bankaccountsform.component.scss'],
  encapsulation:  ViewEncapsulation.None
})

export class AccountingBankaccountsformComponent implements OnInit {

  public item = new Bankaccounts();
  public sentRequest = false;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private service : AccountingService
  ) { }

  ngOnInit() {
    this.item = this.activeRoute.snapshot.data.item;
  }

  public back() {
    this.router.navigate(['../../'], {relativeTo: this.activeRoute});
  }

  public canSave () {
    return !this.sentRequest;
  }

  public save() {
//    this.item.platform_id = 1;    // TODO: make dynamic
    if (this.sentRequest) return;
    this.sentRequest = true;

//  console.log("save support request: " + JSON.stringify(this.supportMessage));

    this.service.saveBankaccounts(this.item).subscribe(
      (res) => { this.router.navigate(['../../'], { relativeTo: this.activeRoute }); }
    );

  }

}


