import { Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AccountingService } from "app/accounting/services/accounting.service";
import { Records } from "app/accounting/types/records";

@Component({
  selector:       'app-accounting-recordsform-form',
  templateUrl:    './accounting-recordsform.component.html',
  styleUrls:      ['./accounting-recordsform.component.scss'],
  encapsulation:  ViewEncapsulation.None
})

export class AccountingRecordsformComponent implements OnInit {

  public item = new Records();
  public sentRequest = false;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private service : AccountingService
  ) { }

  ngOnInit() {
    this.item = this.activeRoute.snapshot.data.item;
  }

  public back() {
    this.router.navigate(['../../'], {relativeTo: this.activeRoute});
  }

  public canSave () {
    return !this.sentRequest;
  }

  public save() {
//    this.item.platform_id = 1;    // TODO: make dynamic
    if (this.sentRequest) return;
    this.sentRequest = true;

//  console.log("save support request: " + JSON.stringify(this.supportMessage));

    this.service.saveRecords(this.item).subscribe(
      (res) => { this.router.navigate(['../../'], { relativeTo: this.activeRoute }); }
    );

  }

}


