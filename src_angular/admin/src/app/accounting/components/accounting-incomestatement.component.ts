import { Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AccountingService } from "app/accounting/services/accounting.service";

@Component({
  selector:       'app-accounting-incomestatement-form',
  templateUrl:    './accounting-incomestatement.component.html',
  styleUrls:      ['./accounting-incomestatement.component.scss'],
  encapsulation:  ViewEncapsulation.None
})

export class AccountingIncomestatementComponent implements OnInit {

	/* -------------------------------------------------------------------------------------
	 * constructor
	 * ---------------------------------------------------------------------------------- */

	constructor(
    	private router: Router,
    	private activeRoute: ActivatedRoute,
    	private service : AccountingService
  	) { }

	/* -------------------------------------------------------------------------------------
	 * ngOnInit
	 * ---------------------------------------------------------------------------------- */
	
	ngOnInit() {
  	}

	/* -------------------------------------------------------------------------------------
	 * back
	 * ---------------------------------------------------------------------------------- */
	public back() {
		this.router.navigate(['../../'], {relativeTo: this.activeRoute});
	}

}
