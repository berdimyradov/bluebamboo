import { Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AccountingService } from "app/accounting/services/accounting.service";
import { Currencies } from "app/accounting/types/currencies";

@Component({
  selector:       'app-accounting-currenciesform-form',
  templateUrl:    './accounting-currenciesform.component.html',
  styleUrls:      ['./accounting-currenciesform.component.scss'],
  encapsulation:  ViewEncapsulation.None
})

export class AccountingCurrenciesformComponent implements OnInit {

  public item = new Currencies();
  public sentRequest = false;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private service : AccountingService
  ) { }

  ngOnInit() {
    this.item = this.activeRoute.snapshot.data.item;
  }

  public back() {
    this.router.navigate(['../../'], {relativeTo: this.activeRoute});
  }

  public canSave () {
    return !this.sentRequest;
  }

  public save() {
//    this.item.platform_id = 1;    // TODO: make dynamic
    if (this.sentRequest) return;
    this.sentRequest = true;

//  console.log("save support request: " + JSON.stringify(this.supportMessage));

    this.service.saveCurrencies(this.item).subscribe(
      (res) => { this.router.navigate(['../../'], { relativeTo: this.activeRoute }); }
    );

  }

}


