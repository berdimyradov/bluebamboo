import { Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AccountingService } from "app/accounting/services/accounting.service";
import { Accounts } from "app/accounting/types/accounts";

@Component({
  selector:       'app-accounting-accountsform-form',
  templateUrl:    './accounting-accountsform.component.html',
  styleUrls:      ['./accounting-accountsform.component.scss'],
  encapsulation:  ViewEncapsulation.None
})

export class AccountingAccountsformComponent implements OnInit {

  public item = new Accounts();
  public sentRequest = false;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private service : AccountingService
  ) { }

  ngOnInit() {
    this.item = this.activeRoute.snapshot.data.item;
  }

  public back() {
    this.router.navigate(['../../'], {relativeTo: this.activeRoute});
  }

  public canSave () {
    return !this.sentRequest;
  }

  public save() {
//    this.item.platform_id = 1;    // TODO: make dynamic
    if (this.sentRequest) return;
    this.sentRequest = true;

//  console.log("save support request: " + JSON.stringify(this.supportMessage));

    this.service.saveAccounts(this.item).subscribe(
      (res) => { this.router.navigate(['../../'], { relativeTo: this.activeRoute }); }
    );

  }

}


