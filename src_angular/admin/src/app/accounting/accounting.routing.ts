import { Routes } from "@angular/router";

import { AccountscodesformResolver } from 'app/accounting/resolvers/accountscodesform.resolver';
import { CurrenciesformResolver } from 'app/accounting/resolvers/currenciesform.resolver';
import { RecordsformResolver } from 'app/accounting/resolvers/recordsform.resolver';
import { AccountsformResolver } from 'app/accounting/resolvers/accountsform.resolver';
import { BankaccountsformResolver } from 'app/accounting/resolvers/bankaccountsform.resolver';
import { CashboxesformResolver } from 'app/accounting/resolvers/cashboxesform.resolver'

import { AccountingAccountscodesComponent } from 'app/accounting/components/accounting-accountscodes.component';
import { AccountingAccountscodesformComponent } from 'app/accounting/components/accounting-accountscodesform.component';
import { AccountingCurrenciesComponent } from 'app/accounting/components/accounting-currencies.component';
import { AccountingCurrenciesformComponent } from 'app/accounting/components/accounting-currenciesform.component';
import { AccountingRecordsComponent } from 'app/accounting/components/accounting-records.component';
import { AccountingRecordsformComponent } from 'app/accounting/components/accounting-recordsform.component';
import { AccountingAccountsComponent } from 'app/accounting/components/accounting-accounts.component';
import { AccountingAccountsformComponent } from 'app/accounting/components/accounting-accountsform.component';
import { AccountingBankaccountsComponent } from 'app/accounting/components/accounting-bankaccounts.component';
import { AccountingBankaccountsformComponent } from 'app/accounting/components/accounting-bankaccountsform.component';
import { AccountingCashboxesComponent } from 'app/accounting/components/accounting-cashboxes.component';
import { AccountingCashboxesformComponent } from 'app/accounting/components/accounting-cashboxesform.component';
import { AccountingBalanceComponent } from 'app/accounting/components/accounting-balance.component';
import { AccountingIncomestatementComponent } from 'app/accounting/components/accounting-incomestatement.component';
import { AccountingJournalComponent } from 'app/accounting/components/accounting-journal.component';
import { AccountingDesktopComponent } from 'app/accounting/components/accounting-desktop.component';

export const accountingRoutes : Routes = [

	{
		path: "accounting/accountcodes",
		pathMatch: 'full',
		component: AccountingAccountscodesComponent,
	},
	{
		path: "accounting/accountcodes/form/:id",
		pathMatch: 'full',
		component: AccountingAccountscodesformComponent,
		resolve: {
			item: AccountscodesformResolver
		}
	},
	{
		path: "accounting/currencies",
		pathMatch: 'full',
		component: AccountingCurrenciesComponent,
	},
	{
		path: "accounting/currencies/form/:id",
		pathMatch: 'full',
		component: AccountingCurrenciesformComponent,
		resolve: {
			item: CurrenciesformResolver
		}
	},
	{
		path: "accounting/records",
		pathMatch: 'full',
		component: AccountingRecordsComponent,
	},
	{
		path: "accounting/records/form/:id",
		pathMatch: 'full',
		component: AccountingRecordsformComponent,
		resolve: {
			item: RecordsformResolver
		}
	},
	{
		path: "accounting/accounts",
		pathMatch: 'full',
		component: AccountingAccountsComponent,
	},
	{
		path: "accounting/accounts/form/:id",
		pathMatch: 'full',
		component: AccountingAccountsformComponent,
		resolve: {
			item: AccountsformResolver
		}
	},
	{
		path: "accounting/bankaccounts",
		pathMatch: 'full',
		component: AccountingBankaccountsComponent,
	},
	{
		path: "accounting/bankaccounts/form/:id",
		pathMatch: 'full',
		component: AccountingBankaccountsformComponent,
		resolve: {
			item: BankaccountsformResolver
		}
	},
	{
		path: "accounting/cashboxes",
		pathMatch: 'full',
		component: AccountingCashboxesComponent,
	},
	{
		path: "accounting/cashboxes/form/:id",
		pathMatch: 'full',
		component: AccountingCashboxesformComponent,
		resolve: {
			item: CashboxesformResolver
		}
	},
	{
		path: "accounting/balance",
		pathMatch: 'full',
		component: AccountingBalanceComponent,
	},
	{
		path: "accounting/incomestatement",
		pathMatch: 'full',
		component: AccountingIncomestatementComponent,
	},
	{
		path: "accounting/journal",
		pathMatch: 'full',
		component: AccountingJournalComponent,
	},
	{
		path: "accounting",
		pathMatch: 'full',
		component: AccountingDesktopComponent,
	}

];
