export const locale = 
{
    "PAGE": {
        "DESKTOP": {
            "MODULE": {
                "ITEMS": {
                    "accounting": {
                        "TITLE": "Accounting",
                        "DESCRIPTION": "Accounting, Profit & Loss Statement etc."
                    }
                }
            }
        },
        "ACCOUNTING": {
            "ACCOUNTSCODES": {
                "PAGE_TITLE": "Account Codes"
            },
            "ACCOUNTSCODESFORM": {
                "PAGE_TITLE": "Account"
            },
            "CURRENCIES": {
                "PAGE_TITLE": "Currencies"
            },
            "CURRENCIESFORM": {
                "PAGE_TITLE": "Currency"
            },
            "RECORDS": {
                "PAGE_TITLE": "Accounting Records"
            },
            "RECORDSFORM": {
                "PAGE_TITLE": "Accounting Record"
            },
            "ACCOUNTS": {
                "PAGE_TITLE": "Accounts"
            },
            "ACCOUNTSFORM": {
                "PAGE_TITLE": "Account"
            },
            "BANKACCOUNTS": {
                "PAGE_TITLE": "Bank Accounts"
            },
            "BANKACCOUNTSFORM": {
                "PAGE_TITLE": "Bank Account"
            },
            "CASHBOXES": {
                "PAGE_TITLE": "Cash Boxes"
            },
            "CASHBOXESFORM": {
                "PAGE_TITLE": "Cash Box"
            },
            "BALANCE": {
                "PAGE_TITLE": "Balance"
            },
            "INCOMESTATEMENT": {
                "PAGE_TITLE": "Income Statement"
            },
            "JOURNAL": {
                "PAGE_TITLE": "Journal"
            },
            "DESKTOP": {
                "ITEMS": {
                    "records": {
                        "TITLE": "Accounting Records"
                    },
                    "balance": {
                        "TITLE": "Balance"
                    },
                    "incomestatement": {
                        "TITLE": "Income Statement"
                    },
                    "journal": {
                        "TITLE": "Journal"
                    },
                    "accounts": {
                        "TITLE": "accounts"
                    },
                    "openingbalance": {
                        "TITLE": "Opening Balance"
                    },
                    "balancing": {
                        "TITLE": "Balancing"
                    },
                    "vat": {
                        "TITLE": "vat"
                    },
                    "currencies": {
                        "TITLE": "Currencies"
                    },
                    "bankaccounts": {
                        "TITLE": "Bank Accounts"
                    },
                    "cashboxes": {
                        "TITLE": "Cash Boxes"
                    },
                    "accountcodes": {
                        "TITLE": "Account Codes"
                    }
                },
                "PAGE_TITLE": "Accounting"
            }
        }
    }
};