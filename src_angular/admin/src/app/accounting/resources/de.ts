export const locale = 
{
    "PAGE": {
        "DESKTOP": {
            "MODULE": {
                "ITEMS": {
                    "accounting": {
                        "TITLE": "Buchhaltung",
                        "DESCRIPTION": "Buchhaltung, Bilanz, Erfolgsrechnung usw."
                    }
                }
            }
        },
        "ACCOUNTING": {
            "ACCOUNTSCODES": {
                "PAGE_TITLE": "Kontenpl\u00e4ne"
            },
            "ACCOUNTSCODESFORM": {
                "PAGE_TITLE": "Kontenplan"
            },
            "CURRENCIES": {
                "PAGE_TITLE": "W\u00e4hrungen"
            },
            "CURRENCIESFORM": {
                "PAGE_TITLE": "W\u00e4hrung"
            },
            "RECORDS": {
                "PAGE_TITLE": "Buchungen"
            },
            "RECORDSFORM": {
                "PAGE_TITLE": "Buchung"
            },
            "ACCOUNTS": {
                "PAGE_TITLE": "Konti"
            },
            "ACCOUNTSFORM": {
                "PAGE_TITLE": "Konto"
            },
            "BANKACCOUNTS": {
                "PAGE_TITLE": "Bank Konti"
            },
            "BANKACCOUNTSFORM": {
                "PAGE_TITLE": "Bank Konto"
            },
            "CASHBOXES": {
                "PAGE_TITLE": "Kassen"
            },
            "CASHBOXESFORM": {
                "PAGE_TITLE": "Kasse"
            },
            "BALANCE": {
                "PAGE_TITLE": "Bilanz"
            },
            "INCOMESTATEMENT": {
                "PAGE_TITLE": "Erfolgsrechnung"
            },
            "JOURNAL": {
                "PAGE_TITLE": "Buchungsjournal"
            },
            "DESKTOP": {
                "ITEMS": {
                    "records": {
                        "TITLE": "Buchungen"
                    },
                    "balance": {
                        "TITLE": "Bilanz"
                    },
                    "incomestatement": {
                        "TITLE": "Erfolgsrechnung"
                    },
                    "journal": {
                        "TITLE": "Buchungsjournal"
                    },
                    "accounts": {
                        "TITLE": "Konten"
                    },
                    "openingbalance": {
                        "TITLE": "Er\u00f6ffnungsbilanz"
                    },
                    "balancing": {
                        "TITLE": "Abschluss"
                    },
                    "vat": {
                        "TITLE": "Mehrtwertsteuer"
                    },
                    "currencies": {
                        "TITLE": "W\u00e4hrungen"
                    },
                    "bankaccounts": {
                        "TITLE": "Bank Konti"
                    },
                    "cashboxes": {
                        "TITLE": "Kassen"
                    },
                    "accountcodes": {
                        "TITLE": "Kontenplan"
                    }
                },
                "PAGE_TITLE": "Buchhaltung"
            }
        }
    }
};