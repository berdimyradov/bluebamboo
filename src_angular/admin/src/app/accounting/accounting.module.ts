
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CommonUiModule } from '@bluebamboo/common-ui';
import { AdminCommonModule } from '../common';

import { locale as en } from './resources/en';
import { locale as de } from './resources/de';

import { AccountingService }                    from 'app/accounting/services/accounting.service';


import { AccountscodesformResolver } from 'app/accounting/resolvers/accountscodesform.resolver';
import { CurrenciesformResolver } from 'app/accounting/resolvers/currenciesform.resolver';
import { RecordsformResolver } from 'app/accounting/resolvers/recordsform.resolver';
import { AccountsformResolver } from 'app/accounting/resolvers/accountsform.resolver';
import { BankaccountsformResolver } from 'app/accounting/resolvers/bankaccountsform.resolver';
import { CashboxesformResolver } from 'app/accounting/resolvers/cashboxesform.resolver'


import { AccountingAccountscodesComponent } from 'app/accounting/components/accounting-accountscodes.component';
import { AccountingAccountscodesformComponent } from 'app/accounting/components/accounting-accountscodesform.component';
import { AccountingCurrenciesComponent } from 'app/accounting/components/accounting-currencies.component';
import { AccountingCurrenciesformComponent } from 'app/accounting/components/accounting-currenciesform.component';
import { AccountingRecordsComponent } from 'app/accounting/components/accounting-records.component';
import { AccountingRecordsformComponent } from 'app/accounting/components/accounting-recordsform.component';
import { AccountingAccountsComponent } from 'app/accounting/components/accounting-accounts.component';
import { AccountingAccountsformComponent } from 'app/accounting/components/accounting-accountsform.component';
import { AccountingBankaccountsComponent } from 'app/accounting/components/accounting-bankaccounts.component';
import { AccountingBankaccountsformComponent } from 'app/accounting/components/accounting-bankaccountsform.component';
import { AccountingCashboxesComponent } from 'app/accounting/components/accounting-cashboxes.component';
import { AccountingCashboxesformComponent } from 'app/accounting/components/accounting-cashboxesform.component';
import { AccountingBalanceComponent } from 'app/accounting/components/accounting-balance.component';
import { AccountingIncomestatementComponent } from 'app/accounting/components/accounting-incomestatement.component';
import { AccountingJournalComponent } from 'app/accounting/components/accounting-journal.component';
import { AccountingDesktopComponent } from 'app/accounting/components/accounting-desktop.component';

@NgModule({
  imports: [CommonModule, CommonUiModule, AdminCommonModule, FormsModule, BrowserAnimationsModule, TranslateModule, RouterModule],
  declarations: [


		AccountingAccountscodesComponent,
		AccountingAccountscodesformComponent,
		AccountingCurrenciesComponent,
		AccountingCurrenciesformComponent,
		AccountingRecordsComponent,
		AccountingRecordsformComponent,
		AccountingAccountsComponent,
		AccountingAccountsformComponent,
		AccountingBankaccountsComponent,
		AccountingBankaccountsformComponent,
		AccountingCashboxesComponent,
		AccountingCashboxesformComponent,
		AccountingBalanceComponent,
		AccountingIncomestatementComponent,
		AccountingJournalComponent,
		AccountingDesktopComponent

  ],
  providers: [


	AccountingService,
	AccountscodesformResolver,
	CurrenciesformResolver,
	RecordsformResolver,
	AccountsformResolver,
	BankaccountsformResolver,
	CashboxesformResolver

  ]
})

export class AccountingModule {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
  }
}
