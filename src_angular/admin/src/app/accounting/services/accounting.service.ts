import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions} from "@angular/http";
import { Observable } from "rxjs/Observable";

import { FrameworkService, GenericResultModel } from 'app/framework/';

import { Accountcodes } from "app/accounting/types/accountcodes";
import { Accounts } from "app/accounting/types/accounts";
import { Currencies } from "app/accounting/types/currencies";
import { Records } from "app/accounting/types/records";
import { Bankaccounts } from "app/accounting/types/bankaccounts";
import { Cashboxes } from "app/accounting/types/cashboxes";

@Injectable()

export class AccountingService extends FrameworkService {

  constructor (http: Http) { super(http); }

		// -------------------------------------------------------------------------------------
		// Accountcodes

		public getAccountcodesList(): Observable<Accountcodes []> {
			return this.http.get('/api/v1/accounting/acccodes')
				.map((res: Response) => res.json().map((it: any) => new Accountcodes().parse(it)))
				.catch(this.handleError);
		}

		public getAccountcodes(id: number): Observable<Accountcodes> {
			return this.http.get('/api/v1/accounting/acccodes/' + id)
				.map((res: Response) => new Accountcodes().parse(res.json()))
				.catch(this.handleError);
		}

		public saveAccountcodes(obj: Accountcodes): Observable<Accountcodes> {
			const res: Observable<Response> = obj.id
				? this.http.put('/api/v1/accounting/acccodes/' + obj.id, obj)
				: this.http.post('/api/v1/accounting/acccodes/', obj);
			return res
					.map((res: Response) => new Accountcodes().parse(res.json()))
					.catch(this.handleError);
		}

		public deleteAccountcodes(id: number): Observable<void> {
			return this.http.delete('/api/v1/accounting/acccodes/' + id)
						.map(() => {return;})
						.catch(this.handleError);
		}
		// -------------------------------------------------------------------------------------
		// Accounts

		public getAccountsList(): Observable<Accounts []> {
			return this.http.get('/api/v1/accounting/accounts')
				.map((res: Response) => res.json().map((it: any) => new Accounts().parse(it)))
				.catch(this.handleError);
		}

		public getAccounts(id: number): Observable<Accounts> {
			return this.http.get('/api/v1/accounting/accounts/' + id)
				.map((res: Response) => new Accounts().parse(res.json()))
				.catch(this.handleError);
		}

		public saveAccounts(obj: Accounts): Observable<Accounts> {
			const res: Observable<Response> = obj.id
				? this.http.put('/api/v1/accounting/accounts/' + obj.id, obj)
				: this.http.post('/api/v1/accounting/accounts/', obj);
			return res
					.map((res: Response) => new Accounts().parse(res.json()))
					.catch(this.handleError);
		}

		public deleteAccounts(id: number): Observable<void> {
			return this.http.delete('/api/v1/accounting/accounts/' + id)
						.map(() => {return;})
						.catch(this.handleError);
		}
		// -------------------------------------------------------------------------------------
		// Currencies

		public getCurrenciesList(): Observable<Currencies []> {
			return this.http.get('/api/v1/accounting/currencies')
				.map((res: Response) => res.json().map((it: any) => new Currencies().parse(it)))
				.catch(this.handleError);
		}

		public getCurrencies(id: number): Observable<Currencies> {
			return this.http.get('/api/v1/accounting/currencies/' + id)
				.map((res: Response) => new Currencies().parse(res.json()))
				.catch(this.handleError);
		}

		public saveCurrencies(obj: Currencies): Observable<Currencies> {
			const res: Observable<Response> = obj.id
				? this.http.put('/api/v1/accounting/currencies/' + obj.id, obj)
				: this.http.post('/api/v1/accounting/currencies/', obj);
			return res
					.map((res: Response) => new Currencies().parse(res.json()))
					.catch(this.handleError);
		}

		public deleteCurrencies(id: number): Observable<void> {
			return this.http.delete('/api/v1/accounting/currencies/' + id)
						.map(() => {return;})
						.catch(this.handleError);
		}
		// -------------------------------------------------------------------------------------
		// Records

		public getRecordsList(): Observable<Records []> {
			return this.http.get('/api/v1/accounting/records')
				.map((res: Response) => res.json().map((it: any) => new Records().parse(it)))
				.catch(this.handleError);
		}

		public getRecords(id: number): Observable<Records> {
			return this.http.get('/api/v1/accounting/records/' + id)
				.map((res: Response) => new Records().parse(res.json()))
				.catch(this.handleError);
		}

		public saveRecords(obj: Records): Observable<Records> {
			const res: Observable<Response> = obj.id
				? this.http.put('/api/v1/accounting/records/' + obj.id, obj)
				: this.http.post('/api/v1/accounting/records/', obj);
			return res
					.map((res: Response) => new Records().parse(res.json()))
					.catch(this.handleError);
		}

		public deleteRecords(id: number): Observable<void> {
			return this.http.delete('/api/v1/accounting/records/' + id)
						.map(() => {return;})
						.catch(this.handleError);
		}
		// -------------------------------------------------------------------------------------
		// Bankaccounts

		public getBankaccountsList(): Observable<Bankaccounts []> {
			return this.http.get('/api/v1/accounting/bankaccounts')
				.map((res: Response) => res.json().map((it: any) => new Bankaccounts().parse(it)))
				.catch(this.handleError);
		}

		public getBankaccounts(id: number): Observable<Bankaccounts> {
			return this.http.get('/api/v1/accounting/bankaccounts/' + id)
				.map((res: Response) => new Bankaccounts().parse(res.json()))
				.catch(this.handleError);
		}

		public saveBankaccounts(obj: Bankaccounts): Observable<Bankaccounts> {
			const res: Observable<Response> = obj.id
				? this.http.put('/api/v1/accounting/bankaccounts/' + obj.id, obj)
				: this.http.post('/api/v1/accounting/bankaccounts/', obj);
			return res
					.map((res: Response) => new Bankaccounts().parse(res.json()))
					.catch(this.handleError);
		}

		public deleteBankaccounts(id: number): Observable<void> {
			return this.http.delete('/api/v1/accounting/bankaccounts/' + id)
						.map(() => {return;})
						.catch(this.handleError);
		}
		// -------------------------------------------------------------------------------------
		// Cashboxes

		public getCashboxesList(): Observable<Cashboxes []> {
			return this.http.get('/api/v1/accounting/cashboxes')
				.map((res: Response) => res.json().map((it: any) => new Cashboxes().parse(it)))
				.catch(this.handleError);
		}

		public getCashboxes(id: number): Observable<Cashboxes> {
			return this.http.get('/api/v1/accounting/cashboxes/' + id)
				.map((res: Response) => new Cashboxes().parse(res.json()))
				.catch(this.handleError);
		}

		public saveCashboxes(obj: Cashboxes): Observable<Cashboxes> {
			const res: Observable<Response> = obj.id
				? this.http.put('/api/v1/accounting/cashboxes/' + obj.id, obj)
				: this.http.post('/api/v1/accounting/cashboxes/', obj);
			return res
					.map((res: Response) => new Cashboxes().parse(res.json()))
					.catch(this.handleError);
		}

		public deleteCashboxes(id: number): Observable<void> {
			return this.http.delete('/api/v1/accounting/cashboxes/' + id)
						.map(() => {return;})
						.catch(this.handleError);
		}

		public getBalance(): Observable<GenericResultModel> {
			return this.getRequest('/api/v1/reports/balance');
		}

		public getIncomeStatement(): Observable<GenericResultModel> {
			return this.getRequest('/api/v1/reports/incomestatement');
		}

		public getJournal(): Observable<GenericResultModel> {
			return this.getRequest('/api/v1/reports/journal');
		}

}
