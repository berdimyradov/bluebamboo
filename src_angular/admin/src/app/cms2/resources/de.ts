export const locale = 
{
    "PAGE": {
        "DESKTOP": {
            "MODULE": {
                "ITEMS": {
                    "cms2": {
                        "TITLE": "Web-Inhalte",
                        "DESCRIPTION": "Texte, Bilder, Testimonials, Fragen und Antworten usw."
                    }
                }
            }
        },
        "CMS2": {
            "DESKTOP": {
                "ITEMS": {
                    "testimonials": {
                        "TITLE": "Testimonials"
                    },
                    "faqs": {
                        "TITLE": "FAQ"
                    },
                    "news": {
                        "TITLE": "News"
                    },
                    "articles": {
                        "TITLE": "Artikel"
                    },
                    "mikee": {
                        "TITLE": "Mikee"
                    }
                },
                "PAGE_TITLE": "Web-Inhalte"
            },
            "MIKEE": {
                "PAGE_TITLE": "Mikee"
            },
            "TESTIMONIALS": {
                "PAGE_TITLE": "Kundenstimmen"
            }
        }
    }
};