export const locale = 
{
    "PAGE": {
        "DESKTOP": {
            "MODULE": {
                "ITEMS": {
                    "cms2": {
                        "TITLE": "Web-Content",
                        "DESCRIPTION": "Texts, Inages, Testimonials, Q&A etc."
                    }
                }
            }
        },
        "CMS2": {
            "DESKTOP": {
                "ITEMS": {
                    "testimonials": {
                        "TITLE": "Testimonials"
                    },
                    "faqs": {
                        "TITLE": "FAQ"
                    },
                    "news": {
                        "TITLE": "News"
                    },
                    "articles": {
                        "TITLE": "Articles"
                    },
                    "mikee": {
                        "TITLE": "Mikee"
                    }
                },
                "PAGE_TITLE": "Web-Content"
            },
            "MIKEE": {
                "PAGE_TITLE": "Mikee"
            },
            "TESTIMONIALS": {
                "PAGE_TITLE": "Testimonials"
            }
        }
    }
};