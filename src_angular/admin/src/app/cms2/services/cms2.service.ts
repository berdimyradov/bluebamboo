import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions} from "@angular/http";
import { Observable } from "rxjs/Observable";

import { FrameworkService, GenericResultModel } from 'app/framework/';

import { Testimonial } from "app/cms2/types/testimonial";

@Injectable()

export class Cms2Service extends FrameworkService {

  constructor (http: Http) { super(http); }

	// -------------------------------------------------------------------------------------
	// Testimonial

	public getTestimonialList(): Observable<Testimonial []> {
		return this.http.get('/api/v1/cms2/testimonials')
			.map((res: Response) => res.json().map((it: any) => new Testimonial().parse(it)))
			.catch(this.handleError);
	}

	public getTestimonial(id: number): Observable<Testimonial> {
		return this.http.get('/api/v1/cms2/testimonials/' + id)
			.map((res: Response) => new Testimonial().parse(res.json()))
			.catch(this.handleError);
	}

	public saveTestimonial(obj: Testimonial): Observable<Testimonial> {
		const res: Observable<Response> = obj.id
			? this.http.put('/api/v1/cms2/testimonials/' + obj.id, obj)
			: this.http.post('/api/v1/cms2/testimonials/', obj);
		return res
				.map((res: Response) => new Testimonial().parse(res.json()))
				.catch(this.handleError);
	}

	public deleteTestimonial(id: number): Observable<void> {
		return this.http.delete('/api/v1/cms2/testimonials/' + id)
					.map(() => {return;})
					.catch(this.handleError);
	}

}
