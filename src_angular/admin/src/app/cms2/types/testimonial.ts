export class Testimonial {
	public id: number;

	public country: string;
	public name: string;
	public notes: string;
	public platform_id: number;
	public platform_id2: number;
	public country2: string;

	public parse( data: any ): Testimonial {
		this.id = data.id || this.id;

		this.country = data.country || this.country;
		this.name = data.name || this.name;
		this.notes = data.notes || this.notes;
		this.platform_id = data.platform_id || this.platform_id;
		this.platform_id2 = data.platform_id2 || this.platform_id2;
		this.country2 = data.country2 || this.country2;
		return this;
	}
}
