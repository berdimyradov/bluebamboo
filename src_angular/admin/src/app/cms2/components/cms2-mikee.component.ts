import { Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Cms2Service } from "app/cms2/services/cms2.service";

@Component({
  selector:       'app-cms2-mikee-form',
  templateUrl:    './cms2-mikee.component.html',
  styleUrls:      ['./cms2-mikee.component.scss'],
  encapsulation:  ViewEncapsulation.None
})

export class Cms2MikeeComponent implements OnInit {

	/* -------------------------------------------------------------------------------------
	 * constructor
	 * ---------------------------------------------------------------------------------- */

	constructor(
    	private router: Router,
    	private activeRoute: ActivatedRoute,
    	private service : Cms2Service
  	) { }

	/* -------------------------------------------------------------------------------------
	 * ngOnInit
	 * ---------------------------------------------------------------------------------- */
	
	ngOnInit() {
  	}

	/* -------------------------------------------------------------------------------------
	 * back
	 * ---------------------------------------------------------------------------------- */
	public back() {
		this.router.navigate(['../../'], {relativeTo: this.activeRoute});
	}

}
