import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonMenuItem } from "../../common";

@Component({
	selector:			'app-cms2-desktop',
	templateUrl:		'./cms2-desktop.component.html',
	styleUrls:			['./cms2-desktop.component.scss'],
	encapsulation:		ViewEncapsulation.None,
	host:				{'[@routeAnimationOpacity]': 'true'},
	animations:			[routeAnimationOpacity]
})

export class Cms2DesktopComponent implements OnInit {

	public modules: CommonMenuItem[] = [

			new CommonMenuItem().parse({
				code: 'testimonials',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'faqs',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'news',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'articles',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'mikee',
				icon: 'fa-key',
				implement: true
			}),


	];

	constructor(
	  private commonPage: CommonPageComponent
	)
	  {}

	ngOnInit() {
		this.commonPage.title = 'PAGE.CMS2.DESKTOP.PAGE_TITLE';
	}
}
