import { Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Cms2Service } from "app/cms2/services/cms2.service";
import { Testimonial } from "app/cms2/types/testimonial";

@Component({
  selector:       'app-cms2-testimonialform-form',
  templateUrl:    './cms2-testimonialform.component.html',
  styleUrls:      ['./cms2-testimonialform.component.scss'],
  encapsulation:  ViewEncapsulation.None
})

export class Cms2TestimonialformComponent implements OnInit {

  public item = new Testimonial();
  public sentRequest = false;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private service : Cms2Service
  ) { }

  ngOnInit() {
    this.item = this.activeRoute.snapshot.data.item;
  }

  public back() {
    this.router.navigate(['../../'], {relativeTo: this.activeRoute});
  }

  public canSave () {
    return !this.sentRequest;
  }

  public save() {
    this.item.platform_id = 1;    // TODO: make dynamic
    if (this.sentRequest) return;
    this.sentRequest = true;

//  console.log("save support request: " + JSON.stringify(this.supportMessage));

    this.service.saveTestimonial(this.item).subscribe(
      (res) => { this.router.navigate(['../../'], { relativeTo: this.activeRoute }); }
    );

  }

}


