import { Routes } from "@angular/router";

import { TestimonialformResolver } from 'app/cms2/resolvers/testimonialform.resolver'

import { Cms2DesktopComponent } from 'app/cms2/components/cms2-desktop.component';
import { Cms2MikeeComponent } from 'app/cms2/components/cms2-mikee.component';
import { Cms2TestimonialsComponent } from 'app/cms2/components/cms2-testimonials.component';
import { Cms2TestimonialformComponent } from 'app/cms2/components/cms2-testimonialform.component';

export const cms2Routes : Routes = [

	{
		path: "cms2",
		pathMatch: 'full',
		component: Cms2DesktopComponent,
	},
	{
		path: "cms2/mikee",
		pathMatch: 'full',
		component: Cms2MikeeComponent,
	},
	{
		path: "cms2/testimonials",
		pathMatch: 'full',
		component: Cms2TestimonialsComponent,
	},
	{
		path: "cms2/testimonials/form/:id",
		pathMatch: 'full',
		component: Cms2TestimonialformComponent,
		resolve: {
			item: TestimonialformResolver
		}
	}

];
