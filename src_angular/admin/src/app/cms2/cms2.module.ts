
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CommonUiModule } from '@bluebamboo/common-ui';
import { AdminCommonModule } from '../common';

import { locale as en } from './resources/en';
import { locale as de } from './resources/de';

import { Cms2Service }                    from 'app/cms2/services/cms2.service';


import { TestimonialformResolver } from 'app/cms2/resolvers/testimonialform.resolver'


import { Cms2DesktopComponent } from 'app/cms2/components/cms2-desktop.component';
import { Cms2MikeeComponent } from 'app/cms2/components/cms2-mikee.component';
import { Cms2TestimonialsComponent } from 'app/cms2/components/cms2-testimonials.component';
import { Cms2TestimonialformComponent } from 'app/cms2/components/cms2-testimonialform.component';

@NgModule({
  imports: [CommonModule, CommonUiModule, AdminCommonModule, FormsModule, BrowserAnimationsModule, TranslateModule, RouterModule],
  declarations: [


		Cms2DesktopComponent,
		Cms2MikeeComponent,
		Cms2TestimonialsComponent,
		Cms2TestimonialformComponent

  ],
  providers: [


	Cms2Service,
	TestimonialformResolver

  ]
})

export class Cms2Module {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
  }
}
