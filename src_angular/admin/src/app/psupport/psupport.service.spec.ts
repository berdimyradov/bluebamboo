import { TestBed, inject } from '@angular/core/testing';

import { PSupportService } from './psupport.service';

describe('PSupportService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ PSupportService ]
    });
  });

  it('should be created', inject([ PSupportService ], (service: PSupportService) => {
    expect(service).toBeTruthy();
  }));
});
