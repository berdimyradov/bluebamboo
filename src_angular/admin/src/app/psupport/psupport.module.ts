import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { AdminCommonModule } from '../common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { CommonUiModule } from '@bluebamboo/common-ui';

import { PSupportPageComponent } from './psupport-page/psupport-page.component';
import { PSupportService } from './psupport.service';
import { locale as en } from './resources/en';
import { locale as de } from './resources/de';

@NgModule({
  imports: [
    BrowserModule, BrowserAnimationsModule, FormsModule, CommonModule, TranslateModule,
    CommonUiModule, AdminCommonModule
  ],
  declarations: [
   PSupportPageComponent,
  ],
  providers: [
    PSupportService
  ]
})
export class PSupportModule {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
  }
}
