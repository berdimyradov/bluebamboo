import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PSupportPageComponent } from './psupport-page.component';

describe('PSupportPageComponent', () => {
  let component: PSupportPageComponent;
  let fixture: ComponentFixture<PSupportPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PSupportPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PSupportPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
