import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CommonMenuItem } from "../../common";
import { CommonPageComponent, routeAnimationOpacity } from "../../common/";

import { PSupportService } from "../psupport.service";

@Component({
  selector:       'admin-psupport-page',
  templateUrl:    './psupport-page.component.html',
  styleUrls: [    './psupport-page.component.scss'],
  encapsulation:  ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class PSupportPageComponent implements OnInit {
  public modules: CommonMenuItem[] = [];

  constructor(
    private psupportService: PSupportService,
    private commonPage: CommonPageComponent
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.PSUPPORT.TITLE';
    this.psupportService.getListModules()
      .subscribe(
        result => this.modules = result
      );
  }
}
