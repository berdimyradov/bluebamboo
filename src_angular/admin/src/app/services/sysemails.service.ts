import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import { DatePipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment/moment';


@Injectable()
export class SysEmailsService {

  constructor(private http: Http, private datePipe: DatePipe, private translate: TranslateService) { }

  private parseType(status: number): string {
    switch (status) {
      case 200:
        return this.translate.instant('SYSMAILS.TYPES.INVOICE');
      case 201:
        return this.translate.instant('SYSMAILS.TYPES.INVOICE_REMINDER_1');
      case 202:
        return this.translate.instant('SYSMAILS.TYPES.INVOICE_REMINDER_2');
      case 300:
        return this.translate.instant('SYSMAILS.TYPES.ONLINEBOOKING_CONFIRMATION_CUSTOMER');
      case 301:
        return this.translate.instant('SYSMAILS.TYPES.ONLINEBOOKING_APPROVAL_CUSTOMER');
      case 302:
        return this.translate.instant('SYSMAILS.TYPES.ONLINEBOOKING_DECLINATION_CUSTOMER');
      case 303:
        return this.translate.instant('SYSMAILS.TYPES.ONLINEBOOKING_CANCELLATION_CUSTOMER');
      case 304:
        return this.translate.instant('SYSMAILS.TYPES.ONLINEBOOKING_REMINDER_CUSTOMER');
      case 310:
        return this.translate.instant('SYSMAILS.TYPES.ONLINEBOOKING_CONFIRMATION_EMPLOYEE');
      case 311:
        return this.translate.instant('SYSMAILS.TYPES.ONLINEBOOKING_APPROVAL_EMPLOYEE');
      case 313:
        return this.translate.instant('SYSMAILS.TYPES.ONLINEBOOKING_CANCELLATION_EMPLOYEE');
    }
  }

  private parseState(char: string): string {
    switch (char) {
      case 'o':
        return this.translate.instant('SYSMAILS.STATES.OPEN');
      case 's':
        return this.translate.instant('SYSMAILS.STATES.SENT');
      case 't':
        return this.translate.instant('SYSMAILS.STATES.TRIED');
      case 'e':
        return this.translate.instant('SYSMAILS.STATES.ERROR');
      default:
        return 'Unknown';
    }
  }

  private parseLastSendAtempt(attempt: any): string {
    if (! attempt) {
      return '';
    } else {
      return this.datePipe.transform(attempt, 'dd.MM.yyyy, hh:mm');
    }
  }

  private parseTo(to: any) {
    return Object.keys(to)[0];
  }

  private parseEmails(rawData) {
    const dataForGrid = [];
    rawData.forEach(element => {
      dataForGrid.push({
        createdAt: this.datePipe.transform(element.createdAt, 'dd.MM.yyyy'),
        state: this.parseState(element.state),
        last_send_attempt: this.parseLastSendAtempt(element.last_send_attempt),
        type: this.parseType(element.type),
        to: this.parseTo(element.to),
        item_id: element.item_id,
        subject: element.subject
      });
    });
    return dataForGrid;
  }

  public getInvoiceEmails(): Observable<any> {
    return this.http.get('/api/v1/invo/emails')
      .map((res: Response) => {
        return this.parseEmails(res.json());
      })
      .catch(this.handleError);
  }

  public getOnlinebookingEmails(): Observable<any> {
    return this.http.get('/api/v1/onlinebooking/emails')
      .map((res: Response) => {
        return this.parseEmails(res.json());
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
