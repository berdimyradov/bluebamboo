import {Component, ViewEncapsulation, OnInit, ChangeDetectorRef} from '@angular/core';
import { PreLoaderService } from './common/preloader/preloader.service';
import { FakeWindowService } from './common/fake-window/service/fake-window.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  title = 'app';

  public preLoaderActive = 0;
  public urlToDisplay = '';


  constructor(
    private preloader: PreLoaderService,
    private fakeWindow: FakeWindowService,
    private cdr: ChangeDetectorRef) {

  }

  ngOnInit() {
    this.preloader.loaderStatus
      .subscribe((val: boolean) => {
        val ? this.preLoaderActive++ : setTimeout(() => this.preLoaderActive--, 1000);
        this.cdr.detectChanges();
      });

    this.fakeWindow.urlData
      .subscribe((val: string) => {
        this.urlToDisplay = val;
      });
  }
}
