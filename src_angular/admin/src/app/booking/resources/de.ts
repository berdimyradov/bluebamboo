export const locale = {
  "COMMON": {
    "BOOKING": {
      "GRID": {
        "OPEN": "Offen",
        "CLOSED": "Geschlossen",
        "DAY": "Tag",
        "TIME": "Zeit(en)",
        "TIME_REASON": "Zeit(en) / Grund",
        "COPY": "Kopieren",
        "PASTE": "Einfügen",
        "AVAILABLE": "Anwesend",
        "AWAY": "Abwesend",
        "STANDARD": "Standard",
        "USER_DEFINED": "Spezifisch",
        "NAME": "Name",
        "DATE": "Datum"
      }
    }
  },
  "PAGE": {
    "BOOKING": {
      "TITLE": "Online Buchungen",
      "ITEMS": {
        "open_hours": {
          "TITLE": "Öffnungszeiten"
        },
        "employee": {
          "TITLE": "Mitarbeiter Arbeitszeiten"
        },
        "settings": {
          "TITLE": "Einstellungen"
        },
        "unhandled-bookings": {
          "TITLE": "Offene Onlinebuchungen"
        },
        'booking-emails': {
          'TITLE': 'Onlinebuchungs E-Mails'
        }
      },
      "WEEKLY": "Generelle Planung",
      "DAILY": "Tägliche Abweichungen",
    },
    "BOOKING-OPEN-HOURS": {
      "TITLE": "Öffnungszeiten",
      "LOCATION": "Standort wählen",
      "SAVE": "Speichern",
      "BACK": "Zurück",
    },
    "BOOKING-EMPLOYEE-PLAN": {
      "BACK": "Zurück",
      "EMPLOYEE": "Mitarbeiter wählen",
      "SAVE": "Speichern",
      "TITLE": "Mitarbeiter Arbeitszeiten"
    },
    "BOOKING-EMPLOYEE-PLAN-WEEKLY": {
      "COPY": "Von Öffnungszeiten übernehmen"
    },
    "BOOKING-EMPLOYEE-REASON": {
      "EMPTY": "Grund wählen",
      "HOLIDAY": "Urlaub",
      "SICKNESS": "Krankheit",
      "EDUCATION": "Fortbildung",
      "OTHER": "Anderer",
      "PUBLIC_HOLIDAY": "Gesetzlicher Feiertag"
    },
    "BOOKING-HOLIDAYS": {
      "YEAR": "Jahr",
      "SAVE": "Speichern",
      "BACK": "Zurück"
    },
    "BOOKING-SETTINGS": {
      "TITLE": "Einstellungen",
      "FORM": {
        "TIME-PERIODS": "Termintaktung (in Min.)",
        "MIN-TIME-BEFORE": "Wie viele Stunden muss eine Buchung mindestens in der Zukunft liegen?",
        "MAX-TIME-BEFORE": "Wie viele Tage darf eine Buchung maximal in der Zukunf liegen?",
        "CANCEL-TIME": "Bis zu vieviele Stunden vor dem Termin ist eine Stornierung möglich?",
        "AUTO-CONFIRMATION": "Automatische Terminbestätigung nach der Buchung (in Stunden nach Buchungszeitpunkt)?",
        "REMINDER-EMAIL": "Dem Kunden eine Erinnerungs Mail vor dem Termin schicken? (Tage vor Termin)",
        "NOTIFY-EMPLOYEE": "Bestätigungsmail nach Buchung an Mitarbeiter senden?",
        "EMAIL": "Absender E-Mail Adresse",
        "MUST_ACCEPT": "Kunde muss Terms of Use bestätigen?",
        "TERMS-OF-USE": "Terms of Use",
        "INCLUDE-CODE": "Einbindungs-Code",
        "DEACTIVATED": "Deaktiviert",
        "IMMEDIATELY": "Sofort",
        "EMAIL_USE_EXISTING": "Organisations E-Mail benutzen",
        "EMAIL_USE_OTHER": "Spezifische Absender-Adresse eingeben",
        "EMAIL_OTHER": "Spezifische Absender E-Mail"
      }
    },
    "BOOKING-EMPLOYEE-WORK-TIME": {
      "ONLINE-BOOKING-AVAILABLE": "Verfügbar für Online Buchungen"
    },
    "UNHANDLED-BOOKINGS": {
      "TITLE": "Onlinebuchungen ToDo",
      "BOOKED-AT": "Gebucht am",
      "TIME-PREFIX": "Uhr",
      "UNHANDLED-BOOKING": {
        "TITLE": "Onlinebuchung zuweisen / bestätigen",
        "CUSTOMER": {
          "ASSIGN": "Diesem Kunden zuweisen",
          "MATCHES": "Mögliche passende Kunden in der Datenbank",
          "SELECT-EXISTING": "Kunden aus Datenbank wählen",
          "RESELECT-EXISTING": "Anderem Kunden zuweisen",
          "ADD": "Neuen Kunden erstellen und zuweisen",
        },
        "BOOKING": {
          "APPROVE": "Buchung bestätigen",
          "DECLINE": "Buchung ablehnen"
        },
        "BOOKING-INFO": {
          "TITLE": "Buchungsinformationen",
          "DATE": "Datum",
          "TIME": "Zeit",
          "SERVICE": "Leistung",
          "EMPLOYEE": "Mitarbeiter",
          "LOCATION": "Standort",
          "ROOM": "Raum",
          'STATUS': {
            'TITLE': 'Status',
            'OPE': 'Offen',
            'OPA': 'Offen und einem Kunden zugewiesen',
            'CAN': 'Abgelehnt',
            'CAA': 'Abgelehnt und einem Kunden zugewiesen',
            'APP' : 'Bestätigt',
            'APA' : 'Bestätigt und einem Kunden zugewiesen'
          }
        },
        "CUSTOMER-INFO": {
          "TITLE": "Eingegebene Kundeninformationen",
          "FIRSTNAME": "Vorname",
          "NAME": "Nachname",
          "EMAIL": "E-Mail",
          "PHONE": "Telefon",
          "COMMENT": "Kommentar"
        },
        "CUSTOMER-CARD": {
          "TITLE": "Zugewiesener Kunde"
        }
      },
      "MESSAGES": {
        "ASSIGN_IMPOSSIBLE": {
          "TITLE": "Kundenzuweisung nicht möglich!",
          "MESSAGE": "Der Onlinebuchung kann kein Kunde zugewiesen werden, weil sie bereits abgelehnt ist."
        },
        "ASSIGN_SUCCESSFUL": {
          "TITLE": "Kundenzuweisung erfolgreich!",
          "MESSAGE": "Der Kunde wurde erfolgreich der Onlinebuchung zugewiesen."
        },
        "ASSIGN_FAILED": {
          "TITLE": "Kundenzuweisung gescheitert!",
          "MESSAGE": "Bei der Kundenzuweisung ist leider ein Fehler aufgetreten."
        },
        "APPROVE_IMPOSSIBLE": {
          "TITLE": "Bestätigung nicht möglich!",
          "MESSAGE": "Die Onlinebuchung ist bereits bestätigt oder abgelehnt."
        },
        "APPROVE_SUCCESSFUL": {
          "TITLE": "Bestätigung erfolgreich!",
          "MESSAGE": "Die Onlinebuchung wurde bestätigt und eine E-Mail an den Kunden geschickt."
        },
        "APPROVE_FAILED": {
          "TITLE": "Bestätigung gescheitert!",
          "MESSAGE": "Bei der Bestätigung der Onlinebuchung ist leider ein Fehler aufgetreten."
        },
        "DECLINE_IMPOSSIBLE": {
          "TITLE": "Ablehnung nicht möglich!",
          "MESSAGE": "Die Onlinebuchung kann nicht abgelehnt werden, da sie bereits abgelehnt oder bestätigt ist."
        },
        "DECLINE_SUCCESSFUL": {
          "TITLE": "Ablehnung erfolgreich!",
          "MESSAGE": "Die Onlinebuchung wurde erfolgreich abgelehnt und eine E-Mail an den Kunden geschickt."
        },
        "DECLINE_FAILED": {
          "TITLE": "Ablehnung gescheitert!",
          "MESSAGE": "Bei der Ablehnung der Onlinebuchung ist leider ein Fehler aufgetreten."
        },
      }
    },
    "BOOKING_MAILS": {
      "ACTIONS": {
        "SHOW": "Onlinebuchung anzeigen"
      }
    }
  }
};
