import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {CommonMenuItem} from '../common/common-menu-page/types/common-menu-item';

@Injectable()
export class BookingService {
  private _modules: CommonMenuItem[] = [
    new CommonMenuItem().parse({
      code: 'unhandled-bookings',
      icon: 'fa-calendar',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'booking-emails',
      icon: 'fa-envelope',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'settings',
      icon: 'fa-wrench',
      implement: true
    })
  ];

  public getListModules(): Observable<CommonMenuItem[]> {
    return (new Observable<CommonMenuItem[]>(observable => observable.next(this._modules)));
  }
}
