export class Location {
  public id: string;
  public title: string;

  constructor(data: any) {
    if (data) {
      this.id = data.id;
      this.title = data.title;
    }
  }
}
