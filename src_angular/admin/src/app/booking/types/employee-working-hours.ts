import { TimeSettings } from './time-settings';
import { Copyable } from '../../common';

export class EmployeeWorkingHours implements Copyable {
  public get isOpen(): boolean {
    return this._isOpen;
  }

  public set isOpen(val: boolean) {
    if (val === this._isOpen) {
      return;
    }

    if (val) {
      this.time.push(new TimeSettings());
    } else {
      this.time = [];
    }

    this._isOpen = val;
  }

  constructor(private _isOpen: boolean = true, public day: string, public time: TimeSettings[] = []) {
  }

  public copyFrom(src: EmployeeWorkingHours) {
    this.isOpen = src.isOpen;
    this.time = JSON.parse(JSON.stringify(src.time));
  }
}
