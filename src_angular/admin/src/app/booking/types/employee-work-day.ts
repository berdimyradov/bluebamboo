import { TimeSettings } from './time-settings';
import { Copyable } from '../../common';

export class EmployeeWorkday implements Copyable {
  public get isOpen(): boolean {
    return false;
  }

  public get mode(): string {
    return this._mode;
  }

  public set mode(val: string) {
    if (val === this._mode) {
      return;
    }

    if (val === 'user_defined' && !this.customTime.length) {
      this.customTime.push(new TimeSettings());
    }

    if (val === 'user_defined') {
      this.reason = null;
    }

    if (val === 'standard') {
      this.customTime = [];
      this.reason = null;
    }

    this._mode = val;
  }

  public get timeIsReadOnly() {
    return this._mode !== 'user_defined';
  }

  public get timeIsShow() {
    return this._mode !== 'away';
  }

  public get time() {
    return this._mode === 'standard' ? this.standardTime : this.customTime;
  }

  constructor(
    private _mode = 'standard',
    public date: number,
    public day: string,
    public standardTime: TimeSettings[] = [],
    public customTime: TimeSettings[] = [],
    public reason: string = ''
  ) {}

  public copyFrom(src: EmployeeWorkday) {
    this.mode = src.mode;
    this.customTime = JSON.parse(JSON.stringify(src.customTime));
    this.reason = src.reason;
  }
}
