/**
    * Created by aleksay on 04.05.2017.
    * Project: tmp_booking
    */
import {TimeSettings}   from "./";
import {Copyable}       from "../../common";

export class HolidaysWork implements Copyable{
    public get isOpen(): boolean {
        return this._isOpen;
    }
    public set isOpen(val: boolean){
        if (val == this._isOpen){
            return;
        }
        if (val){
            this.time.push(new TimeSettings())
        } else {
            this.time = [];
        }
        this._isOpen = val;
    }

    constructor (private _isOpen: boolean = true, public title: string, public day: string, public time: TimeSettings[] = []){}

    public copyFrom(src: HolidaysWork){
        this.isOpen = src.isOpen;
        this.time = JSON.parse(JSON.stringify(src.time));
    }
}
