import { Employee } from '../types';
import { Room, Location } from '../../common';
import { Product } from '../../configuration/configuration-page/types';

export class OnlineBooking {
  public id: number;
  public bookedAt: string;
  public firstname: string;
  public name: string;
  public email: string;
  public phone: string;
  public comment: string;
  public title: string;
  public date: string;
  public startTime: string;
  public endTime: string;
  public duration: number;
  public breakAfter: number;
  public price: string;
  public status: string;
  public cancelledAt: string;
  public cancel_reason: number;
  public product_id: number;
  public employee_id: number;
  public room_id: number;
  public booking_id: number;
  public customer_id: number;
  public location_id: number;
  public employee: Employee;
  public product: Product;
  public room: Room;
  public booking: any;
  public customer: any;
  public location: Location;
  public customer_match: any;

  constructor(data) {
    Object.assign(this, data);

    if (this.employee) {
      this.employee = new Employee(this.employee);
    }

    if (this.room) {
      this.room = new Room().parse(this.room);
    }

    if (this.product) {
      this.product = new Product().parse(this.product);
    }

    if (this.location) {
      this.location = new Location().parse(this.location);
    }
  }
}
