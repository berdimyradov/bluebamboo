import { Location } from './location';

export class Employee {
  public id: string;
  public name: string;
  public firstname: string;
  public locations: Location[];

  constructor(data: any) {
    if (data) {
      this.id = data.id;
      this.name = data.name;
      this.firstname = data.firstname;
      this.locations = data.locations;
    }
  }

  public get fullName() {
    return this.firstname + ' ' + this.name;
  }
}
