export class BookingSettings {
  public time_period: string;
  public min_time_from_now: number;
  public max_time_from_now: number;
  public max_cancel_before: number;
  public auto_confirm: string;
  public email_sender: string;
  public employee_notification: boolean;
  public send_reminder_mail: string;
  public must_accept: boolean;
  public terms_of_use: string;
  public include_code: string;

  constructor(data: any) {
    if (data) {
      this.time_period = data.time_period === null ? '' : String(data.time_period);
      this.min_time_from_now = data.min_time_from_now;
      this.max_time_from_now = data.max_time_from_now;
      this.max_cancel_before = data.max_cancel_before;
      this.auto_confirm = data.auto_confirm === null ? '' : String(data.auto_confirm);
      this.email_sender = data.email_sender;
      this.employee_notification = data.employee_notification;
      this.send_reminder_mail = data.send_reminder_mail === null ? '' : String(data.send_reminder_mail);
      this.must_accept = data.must_accept;
      this.terms_of_use = data.terms_of_use;
      this.include_code = data.include_code;
    }
  }
}
