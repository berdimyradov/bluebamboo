export class TimeSettings {
  constructor(public start: string = null, public end: string = null, public online_booking_available: boolean = null) {}
}
