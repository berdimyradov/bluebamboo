import {
    CustomerConfigurationComponent,
} from '../configuration/configuration-page/customer-configuration/customer-configuration.component';
import { InsuranceListResolver } from '../configuration/configuration-page/resolvers/insurance-list-resolver';
import { Routes } from '@angular/router';
import { BookingPageComponent } from './booking-page/booking-page.component';
import { BookingEmployeePlanComponent } from './booking-page/employee-plan/employee-plan.component';
import { BookingOpenHoursComponent } from './booking-page/open-hours/open-hours.component';
import { BookingSettingsComponent } from './booking-page/booking-settings/booking-settings.component';
import { BookingSettingsResolver } from './booking-page/resolvers/booking-settings-resolver';
import { UnhandledBookingsComponent } from './booking-page/unhandled-bookings/unhandled-bookings.component';
import { UnhandledBookingComponent } from './booking-page/unhandled-bookings/unhandled-booking/unhandled-booking.component';
import { UnhandledBookingResolver } from './booking-page/resolvers';
import { BookingNewClientResolver } from 'app/booking/booking-page/resolvers/booking-new-client.resolver';
import { OnlineBookingsEmailsComponent } from './booking-page/booking-emails/online-bookins-emails.component';

export const bookingRoutes: Routes = [
  {
    path: 'booking',
    pathMatch: 'full',
    component: BookingPageComponent
  },
  {
    path: 'booking/open_hours',
    pathMatch: 'full',
    component: BookingOpenHoursComponent
  },
  {
    path: 'booking/employee',
    pathMatch: 'full',
    component: BookingEmployeePlanComponent
  },
  {
    path: 'booking/settings',
    pathMatch: 'full',
    component: BookingSettingsComponent,
    resolve: {settings: BookingSettingsResolver}
  },
  {
    path: 'booking/unhandled-bookings',
    pathMatch: 'full',
    component: UnhandledBookingsComponent
  },
  {
    path: 'booking/unhandled-bookings/:id',
    pathMatch: 'full',
    component: UnhandledBookingComponent,
    resolve: {
      booking: UnhandledBookingResolver
    }
  },
  {
    path: 'booking/booking-emails',
    component: OnlineBookingsEmailsComponent,
    pathMatch: 'full'
  }
];
