import { ConfigurationModule } from '../configuration';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookingPageComponent } from './booking-page/booking-page.component';
import { RouterModule } from '@angular/router';
import { AdminCommonModule } from '../common';
import { CommonUiModule } from '@bluebamboo/common-ui';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { locale as en } from './resources/en';
import { locale as de } from './resources/de';
import { BookingService } from './booking.service';
import { EmployeePlanService } from './booking-page/services';
import { BookingOpenHoursComponent } from './booking-page/open-hours/open-hours.component';
import { BookingEmployeePlanComponent } from './booking-page/employee-plan/employee-plan.component';
import { EmployeePlanWeeklyComponent } from './booking-page/employee-plan/employee-plan-weekly/employee-plan-weekly.component';
import { EmployeePlanDailyComponent } from './booking-page/employee-plan/employee-plan-daily/employee-plan-daily.component';
import { BookingEmployeeReasonComponent } from './booking-page/employee-plan/employee-plan-daily/booking-employee-reason/booking-employee-reason.component';
import { BookingPeriodSelectorComponent } from './booking-page/employee-plan/employee-plan-daily/booking-period-selector/booking-period-selector.component';
import { LocationPlanWeeklyComponent } from './booking-page/open-hours/location-plan-weekly/location-plan-weekly.component';
import { LocationPlanDailyComponent } from './booking-page/open-hours/location-plan-daily/location-plan-daily.component';
import { BookingLocationReasonComponent } from './booking-page/open-hours/location-plan-daily/booking-location-reason/booking-location-reason.component';
import { LocationPlanService } from './booking-page/services/location-plan.service';
import { BookingSettingsComponent } from './booking-page/booking-settings/booking-settings.component';
import { BookingSettingsService } from './booking-page/services/settings.service';
import { BookingSettingsResolver } from './booking-page/resolvers/booking-settings-resolver';
import { LocationWorkTimeComponent } from './booking-page/open-hours/location-work-time/location-work-time.component';
import { EmployeeWorkTimeComponent } from './booking-page/employee-plan/employee-work-time/employee-work-time.component';
import { UnhandledBookingsComponent } from './booking-page/unhandled-bookings/unhandled-bookings.component';
import { OnlineBookingService } from './booking-page/services/online-booking.service';
import { UnhandledBookingComponent } from './booking-page/unhandled-bookings/unhandled-booking/unhandled-booking.component';
import { UnhandledBookingResolver } from './booking-page/resolvers/unhandled-booking-resolver';
//import { NewCustomerComponent } from 'app/booking/booking-page/unhandled-bookings/unhandled-booking/new-customer/new-customer.component';
import { BookingNewClientResolver } from 'app/booking/booking-page/resolvers/booking-new-client.resolver';
import { OnlineBookingsEmailsComponent } from './booking-page/booking-emails/online-bookins-emails.component';
import { SysEmailsService } from '../services/sysemails.service';
@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    CommonModule,
    TranslateModule,
    CommonUiModule,
    AdminCommonModule,
    RouterModule,
    ConfigurationModule
  ],
  providers: [
    BookingService,
    EmployeePlanService,
    LocationPlanService,
    BookingSettingsService,
    BookingSettingsResolver,
    OnlineBookingService,
    UnhandledBookingResolver,
    BookingNewClientResolver,
    SysEmailsService
  ],
  declarations: [
    // NewCustomerComponent,
    BookingPageComponent,
    BookingOpenHoursComponent,
    BookingEmployeePlanComponent,
    EmployeePlanWeeklyComponent,
    EmployeePlanDailyComponent,
    BookingEmployeeReasonComponent,
    BookingPeriodSelectorComponent,
    LocationPlanWeeklyComponent,
    LocationPlanDailyComponent,
    BookingLocationReasonComponent,
    BookingSettingsComponent,
    LocationWorkTimeComponent,
    EmployeeWorkTimeComponent,
    UnhandledBookingsComponent,
    UnhandledBookingComponent,
    OnlineBookingsEmailsComponent
  ]
})
export class BookingModule {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
  }
}
