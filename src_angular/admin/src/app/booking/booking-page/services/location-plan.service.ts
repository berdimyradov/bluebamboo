import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Location } from '../../types/location';

@Injectable()
export class LocationPlanService {
  constructor(private http: Http) { }

  public getLocations(): Observable<Location[]> {
    return this.http.get('/api/v1/onlinebooking/locations')
      .map((res: Response) => res.json().map(item => new Location(item)))
      .catch(this.handleError);
  }

  public getWeeklyTimes(locationId: string): Observable<any[]> {
    return this.http.get(`/api/v1/onlinebooking/locations/${locationId}/weektimes`)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public setWeeklyTimes(locationId: string, data: any[]): Observable<void> {
    return this.http.post(`/api/v1/onlinebooking/locations/${locationId}/weektimes`, data)
      .map((res: Response) => true)
      .catch(this.handleError);
  }

  public getMonthlyTimes(locationId: string, year: string, month: string): Observable<any[]> {
    return this.http.get(`/api/v1/onlinebooking/locations/${locationId}/monthtimes/${year}/${month}`)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public setMonthlyTimes(locationId: string, year: string, month: string, data: any[]): Observable<void> {
    return this.http.post(`/api/v1/onlinebooking/locations/${locationId}/monthtimes/${year}/${month}`, data)
      .map((res: Response) => true)
      .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    let errMsg: string;

    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);

      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    console.error(errMsg);

    return Observable.throw(errMsg);
  }
}
