import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Customer } from '../../../common';
import { FrameworkService } from '../../../framework/service.framework';
import { first } from 'rxjs/operator/first';
import { OnlineBooking } from '../../types/online-booking';
@Injectable()
export class OnlineBookingService extends FrameworkService {
  constructor(public http: Http) {
    super(http);
  }

  public getList(): Observable<OnlineBooking[]> {
    return this.http.get('/api/v1/onlinebooking/bookings/open')
      .map((res: Response) => {
        return res.json().map((it: any) => new OnlineBooking(it))
      })
      .catch(this.handleError)
  }

  public get(id: number): Observable<OnlineBooking> {
      return this.http.get(`/api/v1/onlinebooking/bookings/${id}`)
      .map((res: Response) => new OnlineBooking(res.json()))
      .catch(this.handleError);
  }

  public assignCustomerToBooking(bookingId: number, clientId: number): Observable<any> {

    return this.http.put(`api/v1/onlinebooking/bookings/${bookingId}/assign/${clientId}`, null)
           .map( response => response.json())
           .catch(this.handleError)
  }

  public declineBooking(bookingId: number): Observable<any> {
    return this.http.put(`api/v1/onlinebooking/bookings/${bookingId}/decline`, {})
           .map( response => response.json())
           .catch(this.handleError)
  }

  public approveBooking(bookingId: number): Observable<any> {
    return this.http.put(`api/v1/onlinebooking/bookings/${bookingId}/accept`, null)
           .map( response => response.json())
           .catch(this.handleError)
  }

  public save(booking: OnlineBooking): Observable<void> {
    return this.http.put(`/api/v1/onlinebooking/${booking.id}`, booking)
      .map((res: Response) => new OnlineBooking(res.json()))
      .catch(this.handleError);
  }
}
