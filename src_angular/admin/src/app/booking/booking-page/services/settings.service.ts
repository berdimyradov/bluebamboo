import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BookingSettings } from '../../types/settings';

@Injectable()
export class BookingSettingsService {
  constructor(private http: Http) { }

  getSettings(): Observable<BookingSettings> {
    return this.http.get(`/api/v1/settings/onlinebooking`)
      .map((res: Response) => new BookingSettings(res.json()))
      .catch(this.handleError);
  }

  saveSettings(settings: BookingSettings) {
    return this.http.put(`/api/v1/settings/onlinebooking`, settings)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    let errMsg: string;

    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);

      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    console.error(errMsg);

    return Observable.throw(errMsg);
  }
}
