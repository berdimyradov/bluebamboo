import { TestBed, inject } from '@angular/core/testing';

import { BookingSettingsService } from './settings.service';

describe('BookingSettingsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BookingSettingsService]
    });
  });

  it('should be created', inject([BookingSettingsService], (service: BookingSettingsService) => {
    expect(service).toBeTruthy();
  }));
});
