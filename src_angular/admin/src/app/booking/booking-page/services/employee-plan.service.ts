import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Employee } from '../../types/employee';

@Injectable()
export class EmployeePlanService {

  constructor(private http: Http) { }

  public getEmployees(): Observable<Employee[]> {
    return this.http.get('/api/v1/onlinebooking/employees')
      .map((res: Response) => res.json().map(item => new Employee(item)))
      .catch(this.handleError);
  }

  public getWeeklyTimes(employeeId: string): Observable<any[]> {
    return this.http.get(`/api/v1/onlinebooking/employees/${employeeId}/weektimes`)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public setWeeklyTimes(employeeId: string, data: any[]): Observable<void> {
    return this.http.post(`/api/v1/onlinebooking/employees/${employeeId}/weektimes`, data)
      .map((res: Response) => true)
      .catch(this.handleError);
  }

  public getMonthlyTimes(employeeId: string, year: string, month: string): Observable<any[]> {
    return this.http.get(`/api/v1/onlinebooking/employees/${employeeId}/monthtimes/${year}/${month}`)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public setMonthlyTimes(employeeId: string, year: string, month: string, data: any[]): Observable<void> {
    return this.http.post(`/api/v1/onlinebooking/employees/${employeeId}/monthtimes/${year}/${month}`, data)
      .map((res: Response) => true)
      .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    let errMsg: string;

    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);

      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    console.error(errMsg);

    return Observable.throw(errMsg);
  }
}
