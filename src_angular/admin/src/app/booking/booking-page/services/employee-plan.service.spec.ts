import { TestBed, inject } from '@angular/core/testing';

import { EmployeePlanService } from './employee-plan.service';

describe('EmployeePlanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmployeePlanService]
    });
  });

  it('should be created', inject([EmployeePlanService], (service: EmployeePlanService) => {
    expect(service).toBeTruthy();
  }));
});
