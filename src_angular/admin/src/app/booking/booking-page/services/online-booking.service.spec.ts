import { TestBed, inject } from '@angular/core/testing';

import { OnlineBookingService } from './online-booking.service';

describe('OnlineBookingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OnlineBookingService]
    });
  });

  it('should be created', inject([OnlineBookingService], (service: OnlineBookingService) => {
    expect(service).toBeTruthy();
  }));
});
