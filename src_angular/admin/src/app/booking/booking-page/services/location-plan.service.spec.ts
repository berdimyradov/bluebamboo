import { TestBed, inject } from '@angular/core/testing';

import { LocationPlanService } from './location-plan.service';

describe('LocationPlanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocationPlanService]
    });
  });

  it('should be created', inject([LocationPlanService], (service: LocationPlanService) => {
    expect(service).toBeTruthy();
  }));
});
