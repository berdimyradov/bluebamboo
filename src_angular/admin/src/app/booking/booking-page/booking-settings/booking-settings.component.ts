import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CopyPaste, CommonPageComponent } from '../../../common';
import { BookingSettings } from '../../types/settings';
import { BookingSettingsService } from '../services/settings.service';
import { AutoHideInterface, SnackState } from '../../../common/snackbar/types/snack';
import { SnackbarComponent } from '../../../common/snackbar/snackbar.component';

@Component({
  selector: 'booking-settings',
  templateUrl: './booking-settings.component.html',
  styleUrls: ['./booking-settings.component.scss'],
  encapsulation: ViewEncapsulation.None
})
@CopyPaste()
export class BookingSettingsComponent implements OnInit {
  public useEmail: string;
  public settings: BookingSettings;
  public snackState = new SnackState({hide: true, delay: 3000},
                                      false,
                                      'Settings are saved',
                                      'Saving settings');
  @ViewChild(SnackbarComponent) snackBar: SnackbarComponent;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private bookingSettingsService: BookingSettingsService
  ) {}

  ngOnInit() {
    this.commonPage.title = 'PAGE.BOOKING-SETTINGS.TITLE';
    this.settings = this.activeRoute.snapshot.data.settings;
    this.useEmail = this.settings.email_sender ? 'other' : 'existing';
  }

  public save() {
    if (this.useEmail === 'existing') {
      this.settings.email_sender = '';
    }

    this.bookingSettingsService
        .saveSettings(this.settings)
        .subscribe(() => {
          this.snackBar.displaySnackBar();
        });
  }

  public back() {
    this.router.navigate(['../'], { relativeTo: this.activeRoute });
  }

}
