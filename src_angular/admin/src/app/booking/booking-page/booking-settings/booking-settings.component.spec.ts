import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingSettingsComponent } from './booking-settings.component';

describe('BookingSettingsComponent', () => {
  let component: BookingSettingsComponent;
  let fixture: ComponentFixture<BookingSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
