import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { OnlineBookingService } from '../services';
import { OnlineBooking } from '../../types';

@Injectable()
export class UnhandledBookingResolver implements Resolve<OnlineBooking> {
  constructor (private onlineBookingService: OnlineBookingService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<OnlineBooking> {
    const id: string = route.paramMap.get('id');

    return this.onlineBookingService.get(Number(id));
  }
}
