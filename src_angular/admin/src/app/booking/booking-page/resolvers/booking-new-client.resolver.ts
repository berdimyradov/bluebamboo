import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Customer } from '../../../common/types/customer';

@Injectable()
export class BookingNewClientResolver implements Resolve<Customer> {
  constructor() {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Customer> {
    const id: string = route.paramMap.get('id');

    if (id === 'add' || id === 'new') {
      return Observable.of(new Customer());
    }
  }
}
