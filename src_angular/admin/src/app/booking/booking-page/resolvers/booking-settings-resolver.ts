import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BookingSettingsService } from '../services/settings.service';
import { BookingSettings } from '../../types/settings';

@Injectable()
export class BookingSettingsResolver implements Resolve<BookingSettings> {
  constructor (private bookingSettingsService: BookingSettingsService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): BookingSettings | Observable<BookingSettings> | Promise<BookingSettings> {
    return this.bookingSettingsService.getSettings();
  }
}
