import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { DateService, SelectOption } from '@bluebamboo/common-ui';
import { EmployeePlanService, LocationPlanService } from '../../services';
import { EmployeeWorkingHours, Location } from '../../../types';
import { CopyPaste } from '../../../../common';
import { TimeSettings } from '../../../types/time-settings';

import * as moment from 'moment';

@Component({
  selector: 'booking-employee-plan-weekly',
  templateUrl: './employee-plan-weekly.component.html',
  styleUrls: ['./employee-plan-weekly.component.scss']
})
@CopyPaste()
export class EmployeePlanWeeklyComponent implements OnInit, OnChanges {
  @Input('employee-id') public employeeId: string;
  @Input('locations') private _locations: Location[] = [];

  @Output() public onInit: EventEmitter<any> = new EventEmitter<any>();

  public locationId: string;
  public days: EmployeeWorkingHours[] = [];
  public locations: SelectOption[] = [];

  constructor(
    private dateService: DateService,
    private employeePlanService: EmployeePlanService,
    private locationPlanService: LocationPlanService
  ) {}

  ngOnInit() {
    this.onInit.emit(this);
  }

  ngOnChanges(): void {
    if (this.employeeId) {
      this.getWeeklyTimes();
    }

    this.locations = this._locations.map((item: Location) => ({
      code: item.id,
      title: item.title
    }));
    this.locationId = this.locations.length > 0 ? this.locations[0].code : null;
  }

  private initDays() {
    const days: EmployeeWorkingHours[] = [];

    this.dateService.Weekdays.forEach((day: string) => days.push(new EmployeeWorkingHours(false, day)));
    this.days = days;
  }

  private getWeeklyTimes() {
    this.initDays();
    this.employeePlanService.getWeeklyTimes(this.employeeId)
      .subscribe(
        result => this.setDays(result),
        error => this.errorHandler(error)
      );
  }

  private setDays(days) {
    const data = {};

    days
      .forEach((day: any) => {
        const start = moment.parseZone(day.start).format('HH:mm');
        const end = moment.parseZone(day.end).format('HH:mm');
        const dayString = this.dateService.numberToDay(day.weekday);

        data[dayString] = data[dayString] || [];

        data[dayString].push({
          start,
          end,
          online_booking_available: day.online_booking_available
        });
      });

    Object
      .keys(data)
      .forEach(key => {
        const dayToFind = this.days.find((day: EmployeeWorkingHours) => day.day === key);

        dayToFind.isOpen = true;
        dayToFind.time = data[key];
      });
  }

  public copyFromOpeningHours() {
    this.initDays();
    this.locationPlanService.getWeeklyTimes(this.locationId)
      .subscribe(
        result => this.setDays(result),
        error => this.errorHandler(error)
      );
  }

  public save() {
    const data = this.days.map((day: EmployeeWorkingHours) => (
      day.time
        .filter((item: TimeSettings) => item.start && item.end)
        .map((item: TimeSettings) => ({
          type: 3,
          available: day.isOpen,
          weekday: this.dateService.dayToNumber(day.day),
          start: item.start,
          wholeday: false,
          end: item.end,
          online_booking_available: item.online_booking_available,
          employee_id: this.employeeId
        }))
    ));

    this.employeePlanService
      .setWeeklyTimes(this.employeeId, [].concat.apply([], data))
      .subscribe(
        () => {},
        error => this.errorHandler(error)
      );
  }

  private errorHandler(error: any) {
  }
}
