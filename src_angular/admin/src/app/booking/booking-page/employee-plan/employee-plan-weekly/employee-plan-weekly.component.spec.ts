import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeePlanWeeklyComponent } from './employee-plan-weekly.component';

describe('EmployeePlanWeeklyComponent', () => {
  let component: EmployeePlanWeeklyComponent;
  let fixture: ComponentFixture<EmployeePlanWeeklyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeePlanWeeklyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeePlanWeeklyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
