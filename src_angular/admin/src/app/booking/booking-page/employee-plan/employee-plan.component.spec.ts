import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeePlanComponent } from './employee-plan.component';

describe('EmployeePlanComponent', () => {
  let component: EmployeePlanComponent;
  let fixture: ComponentFixture<EmployeePlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeePlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeePlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
