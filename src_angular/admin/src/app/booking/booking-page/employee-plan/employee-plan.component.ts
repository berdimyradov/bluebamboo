import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectOption } from '@bluebamboo/common-ui';
import { CommonPageComponent } from '../../../common';
import { EmployeePlanService } from '../services/employee-plan.service';
import { Employee, Location } from '../../types';

@Component({
  selector: 'booking-employee-plan',
  templateUrl: './employee-plan.component.html',
  styleUrls: ['./employee-plan.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BookingEmployeePlanComponent implements OnInit {
  private _data: Employee[] = [];
  public page: any;
  public employees: SelectOption[] = [];
  public employeeId: string;

  constructor(private commonPage: CommonPageComponent,
              private router: Router,
              private activeRoute: ActivatedRoute,
              private employeePlanService: EmployeePlanService) {
  }

  ngOnInit() {
    this.commonPage.title = 'PAGE.BOOKING-EMPLOYEE-PLAN.TITLE';
    this.employeePlanService.getEmployees()
      .subscribe(
        result => this.transformEmployees(result),
        error => this.errorHandler(error)
      );
  }

  public back() {
    this.router.navigate(['../../openHours'], {relativeTo: this.activeRoute});
  }

  private transformEmployees(data: Employee[]) {
    const res: SelectOption[] = [];

    this._data = data;

    data.forEach((d: Employee) => {
      const option: SelectOption = new SelectOption();

      option.title = d.fullName;
      option.code = d.id;

      res.push(option);
    });

    this.employees = res;
    this.employeeId = res.length > 0 ? res[0].code : null;
  }

  private errorHandler(error: any) {
  }

  public setActivePage(page) {
    this.page = page;
  }

  public get locations(): Location[] {
    if (!this.employeeId) {
      return [];
    }

    return this._data.find((item: Employee) => item.id == this.employeeId).locations;
  }
}
