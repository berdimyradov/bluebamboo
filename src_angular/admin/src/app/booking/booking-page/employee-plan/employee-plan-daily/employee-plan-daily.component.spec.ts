import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeePlanDailyComponent } from './employee-plan-daily.component';

describe('EmployeePlanDailyComponent', () => {
  let component: EmployeePlanDailyComponent;
  let fixture: ComponentFixture<EmployeePlanDailyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeePlanDailyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeePlanDailyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
