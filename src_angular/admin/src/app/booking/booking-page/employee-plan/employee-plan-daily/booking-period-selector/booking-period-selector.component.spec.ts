import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingPeriodSelectorComponent } from './booking-period-selector.component';

describe('BookingPeriodSelectorComponent', () => {
  let component: BookingPeriodSelectorComponent;
  let fixture: ComponentFixture<BookingPeriodSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingPeriodSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingPeriodSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
