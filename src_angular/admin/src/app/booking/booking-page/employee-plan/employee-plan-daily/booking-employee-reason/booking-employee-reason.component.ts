import {Component, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'booking-employee-reason',
  templateUrl: './booking-employee-reason.component.html',
  styleUrls: ['./booking-employee-reason.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => BookingEmployeeReasonComponent),
      multi: true
    }
  ]
})
export class BookingEmployeeReasonComponent implements ControlValueAccessor {
  private _value: string = "";
  private onChange: any = () => { };
  private onTouched: any = () => { };

  public get value(): string {
    return this._value;
  };
  public set value(val: string) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  };

  writeValue(obj: any): void {
    if (obj){
      this._value = obj.toString();
    } else {
      this._value = "";
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }
}
