import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { DateService } from '@bluebamboo/common-ui';
import { EmployeePlanService } from '../../services';
import { EmployeeWorkday, TimeSettings } from '../../../types';
import { CopyPaste } from '../../../../common';

import * as moment from 'moment';

@Component({
  selector: 'booking-employee-plan-daily',
  templateUrl: './employee-plan-daily.component.html',
  styleUrls: ['./employee-plan-daily.component.scss']
})
@CopyPaste()
export class EmployeePlanDailyComponent implements OnInit, OnChanges {
  @Input('employee-id') public employeeId: string;
  @Output() public onInit: EventEmitter<any> = new EventEmitter<any>();

  private _selectedPeriod: Date;

  public get selectedPeriod(): Date {
    return this._selectedPeriod;
  }

  public set selectedPeriod(v: Date) {
    this._selectedPeriod = v;

    this.getMonthlyTimes();
  }

  public days: EmployeeWorkday[] = [];

  constructor(
    private dateService: DateService,
    private employeePlanService: EmployeePlanService
  ) {
    this._selectedPeriod = this.dateService.getFirstDay();
  }

  ngOnInit() {
    this.onInit.emit(this);
  }

  ngOnChanges(): void {
    if (this.employeeId) {
      this.getMonthlyTimes();
    }
  }

  private initDays() {
    const year: number = (this._selectedPeriod).getFullYear();
    const month: number = (this._selectedPeriod).getMonth();
    const days: EmployeeWorkday[] = [];

    let day = 1;
    let date: Date = new Date(year, month, day, 0, 0);

    do {
      days.push(new EmployeeWorkday('standard', day, this.dateService.Weekdays[moment(date).isoWeekday() - 1]));

      day++;

      date = new Date(year, month, day, 0, 0);
    } while (date.getMonth() == month);

    this.days = days;
  }

  private getMonthlyTimes() {
    const year: string = String(this._selectedPeriod.getFullYear());
    const month: string = this.addLeadingZero(this._selectedPeriod.getMonth() + 1);

    this.initDays();
    this.employeePlanService.getMonthlyTimes(this.employeeId, year, month)
      .subscribe(
        result => this.setDays(result),
        error => this.errorHandler(error)
      );
  }

  private setDays(days) {
    const data = {};

    days
      .forEach((date: any) => {
        const dateString = moment.parseZone(date.date).format('D');
        const start = date.start ? moment.parseZone(date.start).format('HH:mm') : null;
        const end = date.end ? moment.parseZone(date.end).format('HH:mm') : null;

        data[dateString] = data[dateString] || {
          customTime: [],
          mode: date.wholeday ? 'away' : 'user_defined',
          reason: date.reason
        };

        if (start && end) {
          data[dateString].customTime.push({
            start,
            end,
            online_booking_available: date.online_booking_available
          });
        }
      });

    Object
      .keys(data)
      .forEach(key => {
        const day = this.days.find((day: EmployeeWorkday) => String(day.date) === key);
        const {mode, customTime, reason} = data[key];

        Object.assign(day, {
          mode,
          customTime,
          reason
        });
      });
  }

  public save() {
    const year: string = String(this._selectedPeriod.getFullYear());
    const month: string = this.addLeadingZero(this._selectedPeriod.getMonth() + 1);

    const data = this.days
      .filter((day: EmployeeWorkday) => ['user_defined', 'away'].includes(day.mode))
      .map((day: EmployeeWorkday) => {
        const date = [year, month, this.addLeadingZero(day.date)].join('-');

        if (day.mode === 'user_defined') {
          return day.customTime
            .filter((item: TimeSettings) => item.start && item.end)
            .map((item: TimeSettings) => ({
              date,
              type: 4,
              available: true,
              wholeday: false,
              start: item.start,
              end: item.end,
              online_booking_available: item.online_booking_available,
              employee_id: this.employeeId
            }));
        }

        return [{
          date,
          type: 4,
          available: false,
          wholeday: true,
          reason: day.reason,
          employee_id: this.employeeId
        }];
      });

    this.employeePlanService
      .setMonthlyTimes(this.employeeId, year, month, [].concat.apply([], data))
      .subscribe(
        () => {},
        error => this.errorHandler(error)
      );
  }

  private errorHandler(error: any) {
  }

  private addLeadingZero(str: string | number): string {
    str = String(str);

    return (str.length === 1 ? '0' : '') + str;
  }
}
