import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingEmployeeReasonComponent } from './booking-employee-reason.component';

describe('BookingEmployeeReasonComponent', () => {
  let component: BookingEmployeeReasonComponent;
  let fixture: ComponentFixture<BookingEmployeeReasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingEmployeeReasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingEmployeeReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
