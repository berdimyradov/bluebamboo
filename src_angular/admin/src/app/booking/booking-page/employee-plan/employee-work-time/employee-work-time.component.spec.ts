import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeWorkTimeComponent } from './employee-work-time.component';

describe('EmployeeWorkTimeComponent', () => {
  let component: EmployeeWorkTimeComponent;
  let fixture: ComponentFixture<EmployeeWorkTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeWorkTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeWorkTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
