import { Component, forwardRef, Input, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TimeSettings } from '../../../types';

@Component({
  selector: 'booking-employee-work-time',
  templateUrl: './employee-work-time.component.html',
  styleUrls: ['./employee-work-time.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => EmployeeWorkTimeComponent),
      multi: true
    }
  ]
})
export class EmployeeWorkTimeComponent implements ControlValueAccessor {
  @Input('readonly') public isReadonly = false;
  @Input('max') public _maxItems = 3;

  private _list: TimeSettings[] = [];
  private onChange: any = () => {};
  private onTouched: any = () => {};

  public get list(): TimeSettings[] {
    return this._list;
  }

  public set list(list: TimeSettings[]) {
    this._list = list;
    this.onChange(this._list);
    this.onTouched();
  }

  public get canAdd(): boolean {
    return this._list.length < this._maxItems;
  }

  public get canRemove(): boolean {
    return this._list.length > 1;
  }

  public add(): void {
    this._list.push(new TimeSettings());
    this.onTouched();
  }

  public remove(pos: number = this._list.length - 1): void {
    this._list.splice(pos, pos + 1);
    this.onTouched();
  }

  writeValue(obj: TimeSettings[]): void {
    this._list = obj || [];
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }
}
