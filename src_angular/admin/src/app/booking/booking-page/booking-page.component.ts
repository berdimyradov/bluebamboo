import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {CommonPageComponent, routeAnimationOpacity} from "../../common/";
import {CommonMenuItem} from "../../common/common-menu-page/types/common-menu-item";
import {BookingService} from "../booking.service";

@Component({
  selector: 'admin-booking-page',
  templateUrl: './booking-page.component.html',
  styleUrls: ['./booking-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class BookingPageComponent implements OnInit {
  public modules: CommonMenuItem[] = [];

  constructor(
    private bookingService: BookingService,
    private commonPage: CommonPageComponent
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.BOOKING.TITLE';
    this.bookingService.getListModules()
      .subscribe(
        result => this.modules = result
      );
  }}
