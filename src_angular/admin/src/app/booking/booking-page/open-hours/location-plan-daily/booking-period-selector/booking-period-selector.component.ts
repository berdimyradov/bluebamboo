import {Component, forwardRef, OnInit, ViewEncapsulation} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {DateService} from '@bluebamboo/common-ui';

@Component({
  selector: 'booking-period-selector',
  templateUrl: './booking-period-selector.component.html',
  styleUrls: ['./booking-period-selector.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => BookingPeriodSelectorComponent),
      multi: true
    }
  ]
})
export class BookingPeriodSelectorComponent implements ControlValueAccessor {
  private _value: Date;
  private onChange: any = () => { };
  private onTouched: any = () => { };

  public get value(): Date {
    return this._value;
  };
  public set value(val: Date) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  };
  public get title(){
    if (!this._value){
      return "";
    }
    let month = this.dateService.Months[this._value.getMonth()];
    return month + ", " + this._value.getFullYear();
  }

  constructor (private dateService: DateService){}

  public next(){
    this.value = this.dateService.addMonth(this.value, 1);
  }

  public prev(){
    this.value = this.dateService.addMonth(this.value, -1);
  }

  writeValue(obj: Date): void {
    this._value = obj
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }
}
