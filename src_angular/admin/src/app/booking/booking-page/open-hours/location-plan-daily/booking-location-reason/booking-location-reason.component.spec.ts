import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingLocationReasonComponent } from './booking-location-reason.component';

describe('BookingEmployeeReasonComponent', () => {
  let component: BookingLocationReasonComponent;
  let fixture: ComponentFixture<BookingLocationReasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingLocationReasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingLocationReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
