import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CopyPaste} from '../../../../common';
import {EmployeeWorkday, TimeSettings} from '../../../types';
import {DateService} from '@bluebamboo/common-ui';
import {LocationPlanService} from '../../services';

import * as moment from 'moment';

@Component({
  selector: 'booking-location-plan-daily',
  templateUrl: './location-plan-daily.component.html',
  styleUrls: ['./location-plan-daily.component.scss']
})
@CopyPaste()
export class LocationPlanDailyComponent implements OnInit {
  private _selectedPeriod: Date;
  public get selectedPeriod(): Date {
    return this._selectedPeriod;
  }
  public set selectedPeriod(v: Date) {
    this._selectedPeriod = v;

    this.getMonthlyTimes();
  }
  public days: EmployeeWorkday[] = [];
  @Input('location-id') public locationId: string;
  @Output() public onInit: EventEmitter<any> = new EventEmitter<any>();
  @Output() public onSave: EventEmitter<any> = new EventEmitter<any>();

  constructor (private dateService: DateService, private locationPlanService: LocationPlanService) {
    this._selectedPeriod = this.dateService.getFirstDay();
  }

  ngOnInit() {
    this.onInit.emit(this);
  }

  ngOnChanges(): void {
    if (this.locationId) {
      this.getMonthlyTimes();
    }
  }

  private initDays() {
    let year: number = (this._selectedPeriod).getFullYear(),
      month: number = (this._selectedPeriod).getMonth(),
      day = 1,
      days: EmployeeWorkday[] = [];

    let date: Date = new Date(year, month, day, 0, 0);

    do {
      days.push(new EmployeeWorkday('standard', day, this.dateService.Weekdays[moment(date).isoWeekday() - 1]));

      day++;

      date = new Date(year, month, day, 0, 0);
    } while (date.getMonth() == month);

    this.days = days;
  }

  private getMonthlyTimes() {
    const year: string = String(this._selectedPeriod.getFullYear());
    const month: string = this.addLeadingZero(this._selectedPeriod.getMonth() + 1);

    this.initDays();
    this.locationPlanService.getMonthlyTimes(this.locationId, year, month)
      .subscribe(
        result => this.setDays(result),
        error => this.errorHandler(error)
      );
  }

  private setDays(days) {
    const data = {};

    days
      .forEach((date: any) => {
        const dateString = moment.parseZone(date.date).format('D');
        const start = date.start ? moment.parseZone(date.start).format('HH:mm') : null;
        const end = date.end ? moment.parseZone(date.end).format('HH:mm') : null;

        data[dateString] = data[dateString] || {
          customTime: [],
          mode: date.wholeday ? 'away' : 'user_defined',
          reason: date.reason
        };

        if (start && end) {
          data[dateString].customTime.push({ start, end });
        }
      });

    for (let item in data) {
      const day = this.days.find((day: EmployeeWorkday) => String(day.date) === item);
      const { mode, customTime, reason } = data[item];

      Object.assign(day, {
        mode,
        customTime,
        reason
      });
    }
  }

  public save() {
    const year: string = String(this._selectedPeriod.getFullYear());
    const month: string = this.addLeadingZero(this._selectedPeriod.getMonth() + 1);

    const data = this.days
      .filter((day: EmployeeWorkday) => ['user_defined', 'away'].includes(day.mode))
      .map((day: EmployeeWorkday) => {
        const date = [year, month, this.addLeadingZero(day.date)].join('-');

        if (day.mode === 'user_defined') {
          return day.customTime
            .filter((item: TimeSettings) => item.start && item.end)
            .map((item: TimeSettings) => ({
              date,
              type: 2,
              available: true,
              wholeday: false,
              start: item.start,
              end: item.end,
              location_id: this.locationId
            }));
        }

        return [{
          date,
          type: 2,
          available: false,
          wholeday: true,
          reason: day.reason,
          location_id: this.locationId
        }];
      });

    this.locationPlanService
      .setMonthlyTimes(this.locationId, year, month, [].concat.apply([], data))
      .subscribe(
        result => {
          if (result) {
            this.onSave.emit({sucessfull: true, type: 'daily'})
          }
        },
        error => this.errorHandler(error)
      );
  }

  private errorHandler(error: any){}

  private addLeadingZero(str: string | number): string {
    str = String(str);

    return (str.length === 1 ? '0' : '') + str;
  }
}
