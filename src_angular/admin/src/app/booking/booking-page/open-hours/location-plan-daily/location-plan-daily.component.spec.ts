import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationPlanDailyComponent } from './location-plan-daily.component';

describe('LocationPlanDailyComponent', () => {
  let component: LocationPlanDailyComponent;
  let fixture: ComponentFixture<LocationPlanDailyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationPlanDailyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationPlanDailyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
