import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationPlanWeeklyComponent } from './location-plan-weekly.component';

describe('LocationPlanWeeklyComponent', () => {
  let component: LocationPlanWeeklyComponent;
  let fixture: ComponentFixture<LocationPlanWeeklyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationPlanWeeklyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationPlanWeeklyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
