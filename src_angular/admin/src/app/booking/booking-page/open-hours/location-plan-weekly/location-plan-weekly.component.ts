import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CopyPaste} from "../../../../common";
import {DateService} from '@bluebamboo/common-ui';
import {LocationPlanService} from "../../services";
import {LocationOpeningHours} from "../../../types";
import {TimeSettings} from "../../../types/time-settings";

import * as moment from 'moment';

@Component({
  selector: 'booking-location-plan-weekly',
  templateUrl: './location-plan-weekly.component.html',
  styleUrls: ['./location-plan-weekly.component.scss']
})
@CopyPaste()
export class LocationPlanWeeklyComponent implements OnInit {
  public days: LocationOpeningHours[] = [];
  @Input('location-id') public locationId: string;
  @Output() public onInit: EventEmitter<any> = new EventEmitter<any>();
  @Output() public onSave: EventEmitter<any> = new EventEmitter<any>();

  constructor(private dateService: DateService, private locationPlanService: LocationPlanService) {}

  ngOnInit() {
    this.onInit.emit(this);
  }

  ngOnChanges(): void {
    if (this.locationId) {
      this.getWeeklyTimes();
    }
  }

  private initDays() {
    let days: LocationOpeningHours[] = [];

    this.dateService.Weekdays.forEach((day:string) => days.push(new LocationOpeningHours(false, day)));
    this.days = days;
  }

  private getWeeklyTimes() {
    this.initDays();
    this.locationPlanService.getWeeklyTimes(this.locationId)
      .subscribe(
        result => this.setDays(result),
        error => this.errorHandler(error)
      );
  }

  private setDays(days) {
    const data = {};

    days
      .forEach((day: any) => {
        const start = moment.parseZone(day.start).format('HH:mm');
        const end = moment.parseZone(day.end).format('HH:mm');
        const dayString = this.dateService.numberToDay(day.weekday);

        data[dayString] = data[dayString] || [];

        data[dayString].push({ start, end });
      });

    for (let item in data) {
      const day = this.days.find((day: LocationOpeningHours) => day.day === item);

      day.isOpen = true;
      day.time = data[item];
    }
  }

  public save() {
    const data = this.days.map((day: LocationOpeningHours) => (
      day.time
        .filter((item: TimeSettings) => item.start && item.end)
        .map((item: TimeSettings) => ({
          type: 1,
          available: day.isOpen,
          weekday: this.dateService.dayToNumber(day.day),
          start: item.start,
          wholeday: false,
          end: item.end,
          location_id: this.locationId
        }))
    ));

    this.locationPlanService
      .setWeeklyTimes(this.locationId, [].concat.apply([], data))
      .subscribe(
        result => {
          this.onSave.emit({sucessfull: true, type: 'weekly'});
        },
        error => this.errorHandler(error)
      );
  }

  private errorHandler(error: any){}
}
