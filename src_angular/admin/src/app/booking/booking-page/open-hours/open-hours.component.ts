import {Component, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SelectOption} from '@bluebamboo/common-ui';
import {LocationPlanService} from '../services/location-plan.service';
import {CopyPaste, CommonPageComponent} from '../../../common';
import {LocationOpeningHours, Location} from '../../types';
import { AutoHideInterface, SnackState } from '../../../common/snackbar/types/snack';
import { SnackbarComponent } from '../../../common/snackbar/snackbar.component';

@Component({
  selector: 'booking-open-hours',
  templateUrl: './open-hours.component.html',
  styleUrls: ['./open-hours.component.scss'],
  encapsulation: ViewEncapsulation.None
})
@CopyPaste()
export class BookingOpenHoursComponent implements OnInit {
  public page: any;
  public locations: SelectOption[] = [];
  public locationId: string;
  public days: LocationOpeningHours[] = [];

  public snackState = new SnackState({hide: true, delay: 3000}, false, '', '');

  @ViewChild(SnackbarComponent)
  snackBar: SnackbarComponent;

  constructor(
    private locationPlanService: LocationPlanService,
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute
    ) {}

  ngOnInit() {
    this.commonPage.title = 'PAGE.BOOKING-OPEN-HOURS.TITLE';
    this.locationPlanService.getLocations()
      .subscribe(
        result => this.transformLocations(result),
        error => this.errorHandler(error)
      );
  }

  public back() {
    this.router.navigate(['../../openHours'], {relativeTo: this.activeRoute});
  }

  private transformLocations(data: Location[]) {
    const res: SelectOption[] = [];

    data.forEach((d: Location) => {
      const option: SelectOption = new SelectOption();

      option.title = d.title;
      option.code = d.id;

      res.push(option);
    });

    this.locations = res;
    this.locationId = res.length > 0 ? res[0].code : null;
  }

  private errorHandler(error: any) {}

  public setActivePage(page) {
    this.page = page;
  }

  public onTimeSave(event) {
    const message = `Saved ${event.type} open hours`;
    this.snackState.setMessage(message);
    this.snackState.setHeader('Saved time');
    this.snackBar.displaySnackBar();
  }
}
