import { CommonPageComponent } from '../../../common/common-page/common-page.component';
import { SysEmailsService } from '../../../services';
import {
  Component,
  OnInit,
  ViewChild,
  ViewEncapsulation } from '@angular/core';
import { UiGridComponent } from '../../../common/grid/ui-grid.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-online-bookins-emils',
  templateUrl: 'online-bookins-emails.component.html',
  styleUrls: ['./online-bookins-emails.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class OnlineBookingsEmailsComponent implements OnInit {
  @ViewChild(UiGridComponent)
  public UiGridComponent: UiGridComponent;

  public nickNamesForRows: Array<{
    colName: string,
    displayName: string,
    sortable: boolean
  }>;
  constructor(private emailsService: SysEmailsService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private commonPage: CommonPageComponent) { }

  ngOnInit() {

    this.commonPage.title = 'Onlinebuchungs E-Mails';

    this.nickNamesForRows = [
      { colName: 'createdAt', displayName: 'Erstellt', sortable: true },
      { colName: 'state', displayName: 'Status', sortable: true },
      { colName: 'last_send_attempt', displayName: 'Letzter Sendeversuch', sortable: true },
      { colName: 'to', displayName: 'Empfänger', sortable: true },
      { colName: 'type', displayName: 'Art', sortable: true }
    ];

    this.emailsService.getOnlinebookingEmails()
      .subscribe((data) => {
        this.UiGridComponent.LoadData(data);
      });

  }

  public emailClick(row) {
    this.router.navigate([`../unhandled-bookings/${row.item_id}`], {relativeTo: this.activeRoute});
  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }
}
