import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';
import { OnlineBookingService } from '../services';
import { OnlineBooking } from '../../types/online-booking';
import { CommonPageComponent, routeAnimationOpacity, CommonListItem } from '../../../common';

import * as moment from 'moment/moment';

@Component({
  selector: 'app-unhandled-bookings',
  templateUrl: './unhandled-bookings.component.html',
  styleUrls: ['./unhandled-bookings.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class UnhandledBookingsComponent implements OnInit {
  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private onlineBookingService: OnlineBookingService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.commonPage.title = 'PAGE.UNHANDLED-BOOKINGS.TITLE';
  }

  public getList = (): Observable<CommonListItem[]> => {
    return new Observable<CommonListItem[]>(observer => {
      this.onlineBookingService.getList().subscribe(
        result => {
          observer.next(result.map((it: OnlineBooking) => {
            const t: CommonListItem = new CommonListItem();

            t.id = it.id;
            t.title = this.getItemTitle(it);
            t.description = this.getItemDescription(it);

            return t;
          }));

          observer.complete();
        }
      );
    });
  };

  private getItemTitle(item: OnlineBooking) {
    return moment(item.date).format('DD.MM.YYYY') + ' ' +
      moment(item.startTime).format('HH:mm') + ' - ' +
      moment(item.endTime).format('HH:mm') + ': ' +
      item.firstname + ' ' + item.name;
  }

  private getItemDescription(item: OnlineBooking) {
     return item.title + ' (' + this.translate.instant('PAGE.UNHANDLED-BOOKINGS.BOOKED-AT') + ' ' +
       moment(item.bookedAt).format('DD.MM.YYYY, HH:mm') + ' ' +
       this.translate.instant('PAGE.UNHANDLED-BOOKINGS.TIME-PREFIX')  + ')';
  }

  public edit(booking: CommonListItem) {
    this.router.navigate(['./' + booking.id], {relativeTo: this.activeRoute});
  }
}
