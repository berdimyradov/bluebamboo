import { Location } from '../../../../front/booking/type/location';
import { SnackbarComponent } from '../../../../common/snackbar/snackbar.component';
import { Component, OnInit, OnDestroy, ViewEncapsulation, ChangeDetectorRef, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OnlineBookingService } from '../../services';
import { routeAnimationOpacity, CommonPageComponent } from '../../../../common';
import { OnlineBooking } from '../../../types/online-booking';
import { CustomerConfigurationService } from '../../../../configuration/configuration-page/services/customer-configuration.service';
import { ViewChild } from '@angular/core';
import { Location as loc} from '@angular/common';
import { Customer } from '../../../../common';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-unhandled-booking',
  templateUrl: './unhandled-booking.component.html',
  styleUrls: ['./unhandled-booking.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class UnhandledBookingComponent implements OnInit, OnDestroy {
  public booking: OnlineBooking;
  public customerMatchList: any[] = [];
  public displaySnackBar = false;
  public snackState = {
    autoHide: { hide: true, delay: 3000 },
    display: false,
    message: '',
    header: '',
    eventData: { type: '' }
  };
  public bookingStatus;
  private subscriptionToNewCustomer;

  @ViewChild(SnackbarComponent)
  snackBar: SnackbarComponent;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private onlineBookingService: OnlineBookingService,
    private customerConfigurationService: CustomerConfigurationService,
    private loc: loc,
    private zone: NgZone,
    private translate: TranslateService) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.UNHANDLED-BOOKINGS.UNHANDLED-BOOKING.TITLE';
    this.booking = this.activeRoute.snapshot.data.booking;

    this.subscriptionToNewCustomer = this.customerConfigurationService
      .getTempSubscription().subscribe((customer) => {
        this.assignToCustomer(customer.id);
        // this.booking.customer = customer;
      })

  }

  ngOnDestroy() {
    // console.info('unsubscribing */* ');
    //this.subscriptionToNewCustomer.unsubscribe();
  }

  public assignToCustomer(id: number) {
    if (this.booking.status === 'can') {
      this.snackState.header = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.ASSIGN_IMPOSSIBLE.TITLE');
      this.snackState.message = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.ASSIGN_IMPOSSIBLE.MESSAGE');
      this.snackBar.displaySnackBar();
      return;
    }

    this.onlineBookingService
      .assignCustomerToBooking(this.booking.id, id)
      .subscribe(res => {
        this.booking.status = res.status;
        this.booking.customer = new Customer().parse(res.customer);
        
        this.snackState.eventData.type = 'newuser';
        this.snackState.header = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.ASSIGN_SUCCESSFUL.TITLE');
        this.snackState.message = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.ASSIGN_SUCCESSFUL.MESSAGE');
        this.snackBar.displaySnackBar();
      }, err => {
        this.snackState.header = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.ASSIGN_FAILED.TITLE');
        this.snackState.message = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.ASSIGN_FAILED.MESSAGE');
        this.snackBar.displaySnackBar();
      })
  }

  public selectExistingCustomer() {
    this.router.navigate(['../../../configuration/customer',
      { bid: this.booking.id }
    ], { relativeTo: this.activeRoute });
  }

  public addNewCustomer() {
    this.router.navigate(['../../../configuration/customer/add',
      { bid: this.booking.id }
    ], { relativeTo: this.activeRoute });
  }

  public approveBooking() {
    if (this.booking.status !== 'ope' && this.booking.status !== 'opa') {
      this.snackState.header = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.APPROVE_IMPOSSIBLE.TITLE');
      this.snackState.message = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.APPROVE_IMPOSSIBLE.MESSAGE');
      this.snackBar.displaySnackBar();
      return;
    }

    this.onlineBookingService.approveBooking(this.booking.id)
      .subscribe((resp) => {
        if (resp.id) {
          this.booking.status = resp.status || 'app';
          if(this.booking.status === 'apa') {
            this.snackState.eventData.type = 'approved_and_assigned';
          }
          this.snackState.header = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.APPROVE_SUCCESSFUL.TITLE');
          this.snackState.message = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.APPROVE_SUCCESSFUL.MESSAGE');
          this.snackBar.displaySnackBar();
        }
      },
      (err) => {
        this.snackState.header = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.APPROVE_FAILED.TITLE');
        this.snackState.message = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.APPROVE_FAILED.MESSAGE');
        this.snackBar.displaySnackBar();
      });
  }

  public declineBooking() {
    if (this.booking.status !== 'ope' && this.booking.status !== 'opa') {
      this.snackState.header = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.DECLINE_IMPOSSIBLE.TITLE');
      this.snackState.message = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.DECLINE_IMPOSSIBLE.MESSAGE');
      this.snackBar.displaySnackBar();
      return;
    }

    this.onlineBookingService.declineBooking(this.booking.id)
      .subscribe((res) => {
        if (res === 'ok') {
          this.snackState.eventData.type = 'delete';
          this.snackState.header = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.DECLINE_SUCCESSFUL.TITLE');
          this.snackState.message = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.DECLINE_SUCCESSFUL.MESSAGE');
          this.snackBar.displaySnackBar();
        }
      },
      (err) => {
        this.snackState.header = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.DECLINE_FAILED.TITLE');
        this.snackState.message = this.translate.instant('PAGE.UNHANDLED-BOOKINGS.MESSAGES.DECLINE_FAILED.MESSAGE');
        this.snackBar.displaySnackBar();
      });
  }

  public get statusText(): string {
    switch (this.booking.status) {
      case 'ope':
        return 'PAGE.UNHANDLED-BOOKINGS.UNHANDLED-BOOKING.BOOKING-INFO.STATUS.OPE';
      case 'opa':
        return 'PAGE.UNHANDLED-BOOKINGS.UNHANDLED-BOOKING.BOOKING-INFO.STATUS.OPA';
      case 'can':
        return 'PAGE.UNHANDLED-BOOKINGS.UNHANDLED-BOOKING.BOOKING-INFO.STATUS.CAN'
      case 'caa':
        return 'PAGE.UNHANDLED-BOOKINGS.UNHANDLED-BOOKING.BOOKING-INFO.STATUS.CAA'
      case 'app':
        return 'PAGE.UNHANDLED-BOOKINGS.UNHANDLED-BOOKING.BOOKING-INFO.STATUS.APP'
      case 'apa':
        return 'PAGE.UNHANDLED-BOOKINGS.UNHANDLED-BOOKING.BOOKING-INFO.STATUS.APA'
    }
    // if (this.booking.status === 'ope') {
    //   return 'PAGE.UNHANDLED-BOOKINGS.UNHANDLED-BOOKING.BOOKING-INFO.STATUS.OPEN';
    // } else if (this.booking.status === 'app') {
    //   return 'PAGE.UNHANDLED-BOOKINGS.UNHANDLED-BOOKING.BOOKING-INFO.STATUS.APPROVED';
    // }

    return '';
  }

  public goBackWithState() {
    this.snackState.eventData = null;
    this.back();
  }

  public animationStatusChange($event) {
    if (!$event) {
      return;
    }

    switch ($event.type) {
      case 'delete':
        this.goBackWithState();
        break;
      case 'approved_and_assigned':
        this.goBackWithState();
        break;
      case 'newuser':
        break;
    }

  }

  public back() {
    this.router.navigate(['../'], { relativeTo: this.activeRoute });
  }
}
