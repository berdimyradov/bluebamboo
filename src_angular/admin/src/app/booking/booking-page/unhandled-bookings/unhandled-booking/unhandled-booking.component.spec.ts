import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnhandledBookingComponent } from './unhandled-booking.component';

describe('UnhandledBookingComponent', () => {
  let component: UnhandledBookingComponent;
  let fixture: ComponentFixture<UnhandledBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnhandledBookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnhandledBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
