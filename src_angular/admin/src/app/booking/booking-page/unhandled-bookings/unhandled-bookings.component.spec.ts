import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnhandledBookingsComponent } from './unhandled-bookings.component';

describe('UnhandledBookingsComponent', () => {
  let component: UnhandledBookingsComponent;
  let fixture: ComponentFixture<UnhandledBookingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnhandledBookingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnhandledBookingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
