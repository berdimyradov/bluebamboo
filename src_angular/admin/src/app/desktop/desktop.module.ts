import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonUiModule } from '@bluebamboo/common-ui';

import { TranslateModule, TranslateService } from '@ngx-translate/core';

import { locale as en } from './resources/en';
import { locale as de } from './resources/de';
import { SystemMessagesComponent } from './desktop-page/system-messages/system-messages.component';
import { SystemLinksComponent } from './desktop-page/system-links/system-links.component';
import { MessagesComponent } from './desktop-page/messages/messages.component';

import { InviteComponent } from './desktop-page/invite/invite.component';
import { NextInviteComponent } from './desktop-page/invite/next-invite/next-invite.component';

import { SupportComponent } from './desktop-page/support/support.component';
import { SupportMessageComponent } from './desktop-page/support/message/support-message.component';
import { FeedbackComponent } from './desktop-page/feedback/feedback.component';
import { FeedbackMessageComponent } from './desktop-page/feedback/message/feedback-message.component'

import {
  FeedbackService, SupportService, MessagesService, InviteService, SystemLinksService
} from './desktop-page/services';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { AdminCommonModule } from '../common/common.module';
import { RouterModule } from '@angular/router';
import { ValidatorsModule } from 'ngx-validators';
import { SupportResolver, MessageListResolver, TrialPeriodResolver } from './desktop-page/resolvers';
import { MessageItemComponent } from './desktop-page/messages/message-item/message-item.component';

@NgModule({
  imports: [
    CommonModule, CommonUiModule, TranslateModule, BrowserModule, BrowserAnimationsModule, FormsModule,
    AdminCommonModule, RouterModule, ValidatorsModule
  ],
  declarations: [
                  SystemMessagesComponent, SystemLinksComponent, MessagesComponent,
                  FeedbackComponent, FeedbackMessageComponent, InviteComponent,
                  NextInviteComponent, SupportComponent, SupportMessageComponent,
                  MessageItemComponent
                ],
  providers: [FeedbackService, SupportService, MessagesService, InviteService, SystemLinksService, MessagesService,
    SupportResolver, MessageListResolver, TrialPeriodResolver],
  exports: [SystemMessagesComponent, SystemLinksComponent]
})
export class DesktopModule {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
  }
}
