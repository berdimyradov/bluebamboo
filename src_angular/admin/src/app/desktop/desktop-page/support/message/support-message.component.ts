import { Component, OnInit } from '@angular/core';
import { CommonPageComponent } from '../../../../common';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'admin-support-message',
  templateUrl: './support-message.component.html',
  styleUrls: ['./support-message.scss']
})
export class SupportMessageComponent implements OnInit {

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.DESKTOP.SUPPORT_THANKS.TITLE';
  }

  public back() {
    this.router.navigate(['../../'], {relativeTo: this.activeRoute});
  }

}
