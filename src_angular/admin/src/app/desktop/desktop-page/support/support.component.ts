import { Component, OnInit } from '@angular/core';
import { CommonPageComponent } from '../../../common';
import { ActivatedRoute, Router } from '@angular/router';

import { OrganizationConfigurationService } from '../../../configuration/configuration-page/services/organization-configuration.service';
import { SupportService } from '../services/support.service';
import { SupportMessage } from '../services/types/support-message';

@Component({
  selector: 'admin-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})

export class SupportComponent implements OnInit {
  public supportMessage = new SupportMessage();
  public sentRequest: boolean = false;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private userOrgService: OrganizationConfigurationService,
    private service: SupportService ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.DESKTOP.SUPPORT.TITLE';
    this.supportMessage.phone = this.activeRoute.snapshot.data.organization.phone;
    this.supportMessage.email = this.activeRoute.snapshot.data.organization.email;
  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public canSave () {
    return !this.sentRequest;
  }

  public save() {
    if (this.sentRequest) return;
      this.sentRequest = true;
//    console.log("save support request: " + JSON.stringify(this.supportMessage));
    this.service.requestSupport(this.supportMessage).subscribe(
      (res) => {
        this.router.navigate(['./message'], { relativeTo: this.activeRoute });
      }
    );
  }

}
