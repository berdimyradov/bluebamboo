import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import { Observable} from 'rxjs/Observable';
import { MessagesService } from '../services';

@Injectable()
export class TrialPeriodResolver implements Resolve<any> {
  constructor (private messagesService: MessagesService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any | Observable<any> | Promise<any> {
    return this.messagesService.getTrialPeriod();
  }
}
