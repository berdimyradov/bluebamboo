import { Injectable} from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import { OrganizationConfigurationService } from '../../../configuration/configuration-page/services/organization-configuration.service';
import { Observable} from 'rxjs/Observable';

@Injectable()
export class SupportResolver implements Resolve<any> {
  constructor (private orgService: OrganizationConfigurationService ) {}

  resolve() {
    return this.orgService.get();
  }
}
