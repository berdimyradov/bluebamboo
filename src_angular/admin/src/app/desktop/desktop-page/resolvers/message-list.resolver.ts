import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import { Observable} from 'rxjs/Observable';
import { Message} from '../types';
import { MessagesService } from '../services';

@Injectable()
export class MessageListResolver implements Resolve<Message[]> {
  constructor (private messagesService: MessagesService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Message[]> {
    return this.messagesService.getMessages();
  }
}

