import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import { Observable} from 'rxjs/Observable';
import { Message} from '../types';
import { MessagesService } from '../services';

@Injectable()
export class MessageUnreadListResolver implements Resolve<Message[]> {
  constructor (private messagesService: MessagesService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Message[] | Observable<Message[]> | Promise<Message[]> {
    return this.messagesService.getUnreadMessages();
  }
}

