import { Component, OnInit } from '@angular/core';
import { CommonPageComponent} from '../../../common';
import { ActivatedRoute, Router } from '@angular/router';
import { FeedbackService } from '../services/feedback.service';
import { FeedbackMessage} from '../services/types/feedback-message';

@Component({
  selector: 'admin-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {
  public feedbackMessage: FeedbackMessage;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private feedbackService: FeedbackService
  ) { }

  ngOnInit() {
    this.feedbackMessage = new FeedbackMessage();
    this.feedbackMessage.type = 1;
    this.feedbackMessage.rating = 4;
    this.commonPage.title = 'PAGE.FEEDBACK.TITLE';
  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public save() {
    this.feedbackService.sendFeedback(this.feedbackMessage).subscribe(
      (res) => this.router.navigate(['./message'], {relativeTo: this.activeRoute})
    );
  }

  public onRate(value) {
      this.feedbackMessage.rating = value;
  }

}
