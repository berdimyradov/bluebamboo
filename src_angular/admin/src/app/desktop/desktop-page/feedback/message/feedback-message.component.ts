import { Component, OnInit } from '@angular/core';
import { CommonPageComponent } from '../../../../common';
import { ActivatedRoute, Router } from '@angular/router';
import { FeedbackService } from '../../services/feedback.service';

@Component({
  selector:     'admin-feedback-message',
  templateUrl:  './feedback-message.component.html',
  styleUrls:    ['./feedback-message.component.scss']
})

export class FeedbackMessageComponent implements OnInit {
  public stars: number = 5;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private feedbackService: FeedbackService
  ) {
    this.stars = feedbackService.stars;
  }

  ngOnInit() {
    this.commonPage.title = 'PAGE.FEEDBACK.THANKS-MESSAGE.PAGE_TITLE';
  }

  public invite () {
    this.router.navigate(['../../invite'], {relativeTo: this.activeRoute});
  }

  public back () {
    this.router.navigate(['../../'], {relativeTo: this.activeRoute});
  }

}
