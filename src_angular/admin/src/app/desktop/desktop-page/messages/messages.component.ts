import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Message } from '../types';
import {CommonPageComponent} from "../../../common/common-page/common-page.component";

@Component({
  selector: 'admin-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MessagesComponent implements OnInit {
  private _messages: Message[] = [];
  public status = 'unread';

  constructor(
    private activeRoute: ActivatedRoute,
    private commonPage: CommonPageComponent,
  ) { }

  ngOnInit() {
    this._messages = this.activeRoute.snapshot.data.messages;
    this.commonPage.title = 'PAGE.DESKTOP.MESSAGES.TITLE';
  }

  public get messages() {
    if (this.status === 'all') {
      return this._messages;
    }

    return this._messages.filter((item: Message) => !item.read);
  }

  public onMessageRead(message) {
    const mes: Message = this._messages.find((item: Message) => item.id === message.id);

    mes.read = true;
  }
}
