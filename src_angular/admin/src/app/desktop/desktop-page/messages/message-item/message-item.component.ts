import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { MessagesService } from '../../services';
import { Message } from '../../types';

@Component({
  selector: 'message-item',
  templateUrl: './message-item.component.html',
  styleUrls: ['./message-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MessageItemComponent {
  @Input() public message: Message;
  @Input() public canSkip = true;
  @Output() public onMessageSkip = new EventEmitter<Message>();
  @Output() public onMessageRead = new EventEmitter<Message>();
  @Output() public onMessageClose = new EventEmitter<Message>();

  constructor(
    private messagesService: MessagesService
  ) { }

  public skipMessage() {
    this.onMessageSkip.emit(this.message);
  }

  public closeMessage() {
    this.messagesService.markMessageAsRead(this.message).subscribe(
      () => {
        this.onMessageClose.emit(this.message);
      }
    )
  }

  public markAsRead() {
    this.messagesService.markMessageAsRead(this.message).subscribe(
      () => {
        this.onMessageRead.emit(this.message);
      }
    )
  }
}
