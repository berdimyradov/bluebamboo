export class Message {
  public id: number;
  public title: string;
  public readAt: string;
  public message: string;
  public modal: boolean;
  public read: boolean;

  constructor(data: any) {
    this.id = data.id;
    this.title = data.title;
    this.readAt = data.readAt;
    this.message = data.message;
    this.modal = data.modal;
    this.read = false;
  }
}
