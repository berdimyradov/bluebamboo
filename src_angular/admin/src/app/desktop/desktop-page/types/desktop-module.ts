export class DesktopModule {
  public code: string;
  public external: boolean;
  public icon: string;
  public color: string;
  public title: string;
  public description: string;

  public parse(data: any): DesktopModule {
    this.code = data.code;
    this.external = data.external;
    this.icon = data.icon;
    this.color = data.color;
//    this.title = data.title;
//    this.description = data.description;
    return this;
  }
}
