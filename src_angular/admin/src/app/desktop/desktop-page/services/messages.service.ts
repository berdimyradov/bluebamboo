import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Message } from '../types';

import { FrameworkService, GenericResultModel } from 'app/framework/';

@Injectable()

export class MessagesService extends FrameworkService {
  constructor (http: Http) { super(http); }

  getMessages(): Observable<Message[]> {
    return this.http.get(`/api/v1/platform/clientmessages`)
      .map((res: Response) => res.json().map(item => new Message(item)))
      .catch(this.handleError);
  }

  getUnreadMessages(): Observable<Message[]> {
    return this.http.get(`/api/v1/platform/clientmessages/unread`)
      .map((res: Response) => res.json().map(item => new Message(item)))
      .catch(this.handleError);
  }

  markMessageAsRead(message: Message): Observable<Message> {
    return this.http.put(`/api/v1/platform/clientmessages/${message.id}/read`, message)
      .map((res: Response) => new Message(res.json()))
      .catch(this.handleError);
  }

  getTrialPeriod(): Observable<any> {
    return (new Observable<any>(observable => {
      observable.next({ days: 10 });
      observable.complete();
    }));

    /*return this.http.get(`/api/v1/org/organization/trial`)
      .map((res: Response) => res.json())
      .catch(this.handleError);*/
  }

}
