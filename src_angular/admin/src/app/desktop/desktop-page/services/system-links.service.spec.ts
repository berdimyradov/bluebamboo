import { TestBed, inject } from '@angular/core/testing';

import { SystemLinksService } from './system-links.service';

describe('SystemLinksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SystemLinksService]
    });
  });

  it('should be created', inject([SystemLinksService], (service: SystemLinksService) => {
    expect(service).toBeTruthy();
  }));
});
