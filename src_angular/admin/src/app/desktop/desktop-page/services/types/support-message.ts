export class SupportMessage {
  public name: string;
  public email: string;
  public phone: string;
  public urgency = 'normal';
  public comment: string;
  public info: number;

  public parse(data: any): SupportMessage {
    const names: string[] = ['name', 'email', 'phone', 'urgency', 'comment', 'additionalInfo'];
    names.forEach((name) => this[name] = data[name] || '');
    return this;
  }

}
