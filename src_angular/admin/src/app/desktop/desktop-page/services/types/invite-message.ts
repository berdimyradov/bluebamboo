export class InviteMessage {

  public id: number;
  public created: number;
  public sender: string;
  public receiver: string;
  public name: string;
  public subject: string;
  public message: string;
  public clicked: boolean;
  public registered: boolean;
  public accepted: boolean;
  public client_id: number;

  public parse(data: any): InviteMessage {
    const names: string[] = ['id', 'created', 'sender', 'receiver', 'name', 'subject', 'message', 'clicked', 'registered', 'accepted', 'client_id'];
    names.forEach((name) => this[name] = data[name] || '');
    return this;
  }

}
