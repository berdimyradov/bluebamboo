export class FeedbackMessage {
  public comment: string;
  public type: number;
  public rating: number;

  public parse(data: any): FeedbackMessage {
    const names: string[] = ['comment', 'type', 'rating'];
    names.forEach((name) => this[name] = data[name] || '');
    return this;
  }

}
