import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { FeedbackMessage } from './types/feedback-message';

import { FrameworkService, GenericResultModel } from 'app/framework/';

@Injectable()

export class FeedbackService extends FrameworkService {

  public stars = 5;

  constructor (http: Http) { super(http); }

  // -------------------------------------------------------------------------------------
  // send invitation

  public sendFeedback(model: FeedbackMessage): Observable<GenericResultModel> {
    this.stars = model.rating;
    console.log("SEND FEEDBACK; DAM IT: " + JSON.stringify(model));
    return this.postRequest(model, '/api/v1/platform/feedbacks');
  }

  // -------------------------------------------------------------------------------------
  // get invitations

  public getFeedbacks(): Observable<FeedbackMessage[]> {
    return this.http.get(`/api/v1/platform/feedbacks?_fields=type,rating,comment,created`)
      .map((res: Response) => res.json().map(item => new FeedbackMessage().parse(item)))
      .catch(this.handleError);
  }

}
