import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SupportMessage } from './types/support-message';

import { FrameworkService, GenericResultModel } from 'app/framework/';

@Injectable()

export class SupportService extends FrameworkService {
  constructor (http: Http) { super(http); }

  // -------------------------------------------------------------------------------------
  // requestSupport

  public requestSupport(model: SupportMessage): Observable<GenericResultModel> {
    return this.postRequest(model, '/api/v1/platform/systickets');
  }

}
