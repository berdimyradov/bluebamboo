import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { FrameworkService, GenericResultModel } from 'app/framework/';
import { InviteMessage } from './types/invite-message';

@Injectable()

export class InviteService extends FrameworkService {
  constructor (http: Http) { super(http); }

  // -------------------------------------------------------------------------------------
  // send invitation

  public sendInvitation(model: InviteMessage): Observable<GenericResultModel> {
    return this.postRequest(model, '/api/v1/platform/recommendations');
  }

  // -------------------------------------------------------------------------------------
  // get invitations

  public getInvitations(): Observable<InviteMessage[]> {
    return this.http.get(`/api/v1/platform/recommendations?_fields=receiver,created`)
      .map((res: Response) => res.json().map(item => new InviteMessage().parse(item)))
      .catch(this.handleError);
  }

}
