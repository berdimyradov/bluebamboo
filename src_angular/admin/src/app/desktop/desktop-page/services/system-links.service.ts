import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SystemLinksService {
  private _links1 = [
    {
      code: 'messages'
    },
    {
      code: 'invite'
    },
    {
      code: 'support'
    },
    {
      code: 'feedback'
    }
  ];

  private _links = [
    {
      code: 'support'
    }
  ];

  public getListLinks(): Observable<any[]> {
    return (new Observable<any[]>(observable => observable.next(this._links)));
  }
}
