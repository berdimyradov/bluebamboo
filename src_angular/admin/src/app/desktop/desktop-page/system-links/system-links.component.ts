import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SystemLinksService } from '../services/system-links.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'admin-system-links',
  templateUrl: './system-links.component.html',
  styleUrls: ['./system-links.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SystemLinksComponent implements OnInit {
  public links = [];
  public showMenu: boolean = false;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private systemLinksService: SystemLinksService
  ) { }

  ngOnInit() {
    this.systemLinksService.getListLinks().subscribe(
      result => {
        this.links = result;
      }
    )
  }

  public toggleMenu () {
    this.showMenu = !this.showMenu;
  }

  public showPdfManual () {
    window.open("https://bluebamboo.blob.core.windows.net/public/bluebamboo-die-anleitung.pdf", "_blank");
  }

  public showPdfPreisblatt () {
    window.open("https://bluebamboo.blob.core.windows.net/public/bluebamboo-preisblatt.pdf", "_blank");
  }

  public showVideosPage () {
    window.open("https://www.bluebamboo.ch/de/videos", "_blank");
  }

  public showSupportPage() {
      this.router.navigate(['../support'], {relativeTo: this.activeRoute});
  }

  public showFeedbackPage() {
      this.router.navigate(['../feedback'], {relativeTo: this.activeRoute});
  }

  public showRecommendPage() {
      this.router.navigate(['../invite'], {relativeTo: this.activeRoute});
  }

  public showMyModulesPage() {
      this.router.navigate(['../myaccount/modules'], {relativeTo: this.activeRoute});
  }

}
