import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemLinksComponent } from './system-links.component';

describe('SystemLinksComponent', () => {
  let component: SystemLinksComponent;
  let fixture: ComponentFixture<SystemLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
