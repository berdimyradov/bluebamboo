import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesktopModuleComponent } from './desktop-module.component';

describe('DesktopModuleComponent', () => {
  let component: DesktopModuleComponent;
  let fixture: ComponentFixture<DesktopModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesktopModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesktopModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
