import {Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {DesktopModule} from '../types';

@Component({
  selector: 'admin-desktop-module',
  templateUrl: './desktop-module.component.html',
  styleUrls: ['./desktop-module.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DesktopModuleComponent implements OnInit {
  @HostBinding('class.ccol-xs-6')private classXs6 = true;
  @HostBinding('class.ccol-md-3')private classMd3 = true;
  @Input() public module: DesktopModule;
  @Output() public action: EventEmitter<DesktopModule> = new EventEmitter<DesktopModule>();

  constructor() { }

  ngOnInit() {
  }

  public click() {
    this.action.emit(this.module);
  }
}
