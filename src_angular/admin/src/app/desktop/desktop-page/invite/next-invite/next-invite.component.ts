import { Component, OnInit } from '@angular/core';
import { CommonPageComponent } from '../../../../common';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector:     'admin-invite-message',
  templateUrl:  './next-invite.component.html',
  styleUrls:    ['./next-invite.component.scss']
})

export class NextInviteComponent implements OnInit {
  public message: string;
  public messageText: string;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.INVITE.THANKS-MESSAGE.PAGE_TITLE';
    this.message = 'Thank you for inviting friend!';
    this.messageText = 'Maybe You want to invite one more person?';
  }

  public back() {
    this.router.navigate(['../../'], {relativeTo: this.activeRoute});
  }

}
