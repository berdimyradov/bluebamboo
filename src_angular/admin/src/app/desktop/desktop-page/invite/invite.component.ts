import { Component, OnInit } from '@angular/core';
import { CommonPageComponent } from '../../../common';
import { ActivatedRoute, Router } from '@angular/router';
import { InviteService } from '../services/invite.service';
import { InviteMessage } from '../services/types/invite-message';
import { LocalStorageService } from 'angular-2-local-storage';
import { TranslateService } from '@ngx-translate/core';
import { OrganizationConfigurationService } from '../../../configuration/configuration-page/services/organization-configuration.service';

@Component({
  selector:     'admin-invite',
  templateUrl:  './invite.component.html',
  styleUrls:    ['./invite.component.scss']
})

export class InviteComponent implements OnInit {

  public invitations = [];

  public inviteMessage: InviteMessage = new InviteMessage();

  private _myMail: string;
  public myMailError = false;
  public myMailMessage = "";

  private _otherMail: string;
  public otherMailError = false;
  public otherMailMessage = "";

  private _inviteMessageBody: string = "";
  private _inviteMessageAddress: string = "";

  public get inviteMessageBody() :string {
    return this._inviteMessageBody;
  }
  public set inviteMessageBody(body: string) {
    this._inviteMessageBody = body;
    this.inviteMessage.message = this.inviteMessageAddress + "\n\n" + this.inviteMessageBody;
  }

  public get inviteMessageAddress() :string {
    return this._inviteMessageAddress;
  }
  public set inviteMessageAddress(address: string) {
    this._inviteMessageAddress = address;
    this.inviteMessage.message = this.inviteMessageAddress + "\n\n" + this.inviteMessageBody;
  }

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private userOrgService: OrganizationConfigurationService,
    private inviteService: InviteService,
   	private localStorageService: LocalStorageService,
    private translate: TranslateService
  ) { }

  isMail (val: string) : boolean {
    const pureEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pureEmail.test(val);
  }

  get myMail(): string {
    return this._myMail;
  }

  set myMail(val: string) {
    this.myMailError = false;
    this.myMailMessage = "";
    if (!val) {
      this.myMailError = true;
      this.myMailMessage = "Ihre Absender-E-Mail muss eingegeben sein.";
    }
    else if (!this.isMail(val)) {
      this.myMailError = true;
      this.myMailMessage = "Die E-Mail Adresse ist noch nicht vollständig.";
    }
    this._myMail = val;
  }

  get otherMail(): string {
    return this._otherMail;
  }

  set otherMail(val: string) {
    this.otherMailError = false;
    this.otherMailMessage = "";
    if (!val) {
      this.otherMailError = true;
      this.otherMailMessage = "Die E-Mail der einzuladenden Person muss eingegeben sein.";
    }
    else if (!this.isMail(val)) {
      this.otherMailError = true;
      this.otherMailMessage = "Die E-Mail Adresse ist noch nicht vollständig.";
    }
    this._otherMail = val;
  }

  ngOnInit() {
    this.commonPage.title = 'PAGE.INVITE.PAGE_TITLE';
    this.inviteMessage.sender = this.activeRoute.snapshot.data.organization.email;
    this._myMail = this.activeRoute.snapshot.data.organization.email;

    // see https://github.com/ngx-translate/core
    this.translate.get('PAGE.INVITE.MESSAGE_ADDRESS_DEFAULT').subscribe((res: string) => {
      this.inviteMessageAddress = res;
    });
    this.translate.get('PAGE.INVITE.MESSAGE_DEFAULT').subscribe((res: string) => {
      this.inviteMessageBody = res;
    });
    this.translate.get('PAGE.INVITE.SUBJECT_DEFAULT').subscribe((res: string) => {
      this.inviteMessage.subject = res;
    });
    this.translate.get('PAGE.INVITE.EMAIL_DEFAULT').subscribe((res: string) => {
      this.inviteMessage.receiver = res;
    });

    let sub = this.localStorageService.get("bb_personal_invite_subject");
    let txt = this.localStorageService.get("bb_personal_invite_text");
    if (sub) this.inviteMessage.subject = sub as string;
    if (txt) this.inviteMessageBody = txt as string;

    this.inviteService.getInvitations().subscribe(
      (res) => this.invitations = res
    );

  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public invite() {
    this.inviteMessage.sender = this._myMail;
    this.inviteMessage.receiver = this._otherMail;

    this.localStorageService.set("bb_personal_invite_subject", this.inviteMessage.subject);
    this.localStorageService.set("bb_personal_invite_text", this.inviteMessageBody);
    this.inviteService.sendInvitation(this.inviteMessage).subscribe(
      (res) => {
//        console.log("result: " + JSON.stringify(res));
        this.router.navigate(['./nextinvite'], {relativeTo: this.activeRoute});
      }
    );
  }

}
