import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DesktopService } from '../desktop.service';
import { DesktopModule } from './types/desktop-module';
import { ActivatedRoute, Router } from "@angular/router";
import { CommonPageComponent, routeAnimationOpacity } from "../../common";
import { Message } from './types';
import { Observable } from 'rxjs/Observable';
import { FakeWindowService } from '../../common/fake-window/service/fake-window.service';

import { MessagesService } from './services/messages.service';

@Component({
  selector: 'admin-desktop-page',
  templateUrl: './desktop-page.component.html',
  styleUrls: ['./desktop-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})

export class DesktopPageComponent implements OnInit {
  public modules: DesktopModule[] = [];
  public messages: Message[] = [];
  public trialPeriod: any = {};
  public hasModalMessage: boolean = false;
  private fakeWindow;

  constructor(
    private desktopService: DesktopService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private commonPage: CommonPageComponent,
    private fakeWindowService: FakeWindowService,
    private messagesService: MessagesService
  ) { }

  ngOnInit() {
    this.fakeWindow = this.fakeWindowService.nativeWindow;

    this.fakeWindow['gotoDesktop'] = () => {
      this.fakeWindowService.url(null);
    }
    this.commonPage.title = '';
    this.desktopService.getListModules()
      .subscribe(
        result => {

          this.modules = result;
        }
      );
    this.messages = this.activeRoute.snapshot.data.messages;
    this.trialPeriod = this.activeRoute.snapshot.data.trialPeriod;
    this.checkForModalMessage();
  }

  private checkForModalMessage () {
    this.hasModalMessage = false;
    this.messages.forEach (el => {
      if (el.modal) this.hasModalMessage = true;
    });
  }

  public executeAction(module: DesktopModule) {
    const fakeUrl = window.location + '/' + module.code;
    if (module.external) {
      this.fakeWindowService.url(fakeUrl);
    } else {
      this.router.navigate(['./' + module.code], {relativeTo: this.activeRoute});
    }
  }

  public closeMessage(message: Message) {
    this.messagesService.getUnreadMessages().subscribe(
      (messages) => {
        this.messages = messages;
        this.checkForModalMessage();
      }
    );
  }
}
