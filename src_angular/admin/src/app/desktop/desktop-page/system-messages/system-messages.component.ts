import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { Message } from '../types';

@Component({
  selector: 'admin-system-messages',
  templateUrl: './system-messages.component.html',
  styleUrls: ['./system-messages.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SystemMessagesComponent {
  @Input()  public messages: Message[] = [];
  @Input()  public trialPeriod: any;
  @Output() public onMessageClose = new EventEmitter<Message>();

  public pay() {

  }

  public closeMessage (message: Message) {
    this.onMessageClose.emit(message);
  }

  public removeMessage(message: Message) {
    this.messages = this.messages.filter((item: Message) => item.id !== message.id);
  }
}
