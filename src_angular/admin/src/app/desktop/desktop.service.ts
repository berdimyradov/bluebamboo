import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from "@angular/http";

import { DesktopModule } from './desktop-page/types'

@Injectable()
export class DesktopService {
  private _modules: DesktopModule[] = [
    new DesktopModule().parse({
      code: 'calendar',
      external: true,
      icon: 'fa-calendar',
      color: 'red-text'
    }),
    new DesktopModule().parse({
      code: 'customers',
      external: true,
      icon: 'fa-address-book',
      color: 'pink-text'
    }),
    new DesktopModule().parse({
      code: 'invoice',
      external: false,
      icon: 'fa-calculator',
      color: 'purple-text'
    }),
    /*new DesktopModule().parse({
      code: 'booking',
      external: false,
      icon: 'fa-shopping-basket',
      color: 'purple-text'
    }),*/
    new DesktopModule().parse({
      code: 'sites',
      external: true,
      icon: 'fa-tv',
      color: 'deep-purple-text'
    }),
    new DesktopModule().parse({
      code: 'myaccount',
      external: false,
      icon: 'fa-users',
      color: 'blue-text'
    }),
    new DesktopModule().parse({
      code: 'configuration',
      external: false,
      icon: 'fa-wrench',
      color: 'green-text'
    }),
    new DesktopModule().parse({
      code: 'email-templates',
      external: false,
      icon: 'fa-envelope',
      color: 'green-text'
    })
    /*new DesktopModule().parse({
      code: 'settings',
      external: true,
      icon: 'fa-gear',
      color: 'light-green-text'
    })*//*,
    new DesktopModule().parse({
      code: 'faq',
      external: false,
      icon: 'fa-gear',
      color: 'light-blue-text'
    })*/
  ];

  constructor(private http: Http) {}

  public getListModules(): Observable<DesktopModule[]> {

    return this.http.get('/api/v1/desktop/desktop')
      .map((res: Response) => (res.json() as any[]).map((it:any) => (new DesktopModule()).parse(it)) )
      .catch(this.handleError);

//    return (new Observable<DesktopModule[]>(observable => observable.next(this._modules)));
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
