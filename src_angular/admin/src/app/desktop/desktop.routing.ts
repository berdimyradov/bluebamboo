import { Routes } from '@angular/router';

import { MessagesComponent } from './desktop-page/messages/messages.component';

import { InviteComponent } from './desktop-page/invite/invite.component';
import { NextInviteComponent } from './desktop-page/invite/next-invite/next-invite.component';

/* Support Component*/
import { SupportComponent } from './desktop-page/support/support.component';
import { SupportMessageComponent } from './desktop-page/support/message/support-message.component';

/* FeedBack Component */
import { FeedbackComponent } from './desktop-page/feedback/feedback.component';
import { FeedbackMessageComponent } from './desktop-page/feedback/message/feedback-message.component';

import { SupportResolver, MessageListResolver } from './desktop-page/resolvers';

export const desktopRoutes: Routes = [
  {
    path: 'feedback',
    pathMatch: 'full',
    component: FeedbackComponent
  },
  {
    path: 'feedback/message',
    pathMatch: 'full',
    component: FeedbackMessageComponent
  },
  {
    path: 'messages',
    pathMatch: 'full',
    component: MessagesComponent,
    resolve: { messages: MessageListResolver }
  },
  {
    path: 'invite',
    pathMatch: 'full',
    component: InviteComponent,
    resolve: { organization: SupportResolver}
  },
  {
    path: 'invite/nextinvite',
    pathMatch: 'full',
    component: NextInviteComponent
  },
  {
    path: 'support',
    pathMatch: 'full',
    component: SupportComponent,
    resolve: { organization: SupportResolver}
  },
  {
    path: 'support/message',
    pathMatch: 'full',
    component: SupportMessageComponent
  },

];
