export const locale = {
  "PAGE": {
    "LOGIN": {
      "LOGIN-FORM": {
        "TITLE": "Anmelden",
        "EMAIL": "Ihre E-Mail",
        "PASSWORD": "Ihr Passwort",
        "LOGIN": "Anmelden",
        "REGISTRATION": "Registration",
        "PASSWORD_RESET_REQ": "Passwort neu setzen"
      },
      "LOGIN-INFO-FORM": {
        "TITLE": "Melden Sie sich hier an!",
        "DESCRIPTION": "Bluebamboo - Ihr Partner für mehr Kunden und effizientere Administration - damit Sie sich auf das konzentrieren können, was Sie wohl am besten und liebsten tun!",
        "INFORMATION": "Weitere Informationen"
      },
      "REGISTRATION-FORM": {
        "TITLE": "Registration",
        "REGISTRATION": "Registration",
        "LOGIN": "Abbrechen",
        "EMAIL": "E-Mail",
        "PASSWORD": "Passwort",
        "PASSWORD-AGAIN": "Passwort Wiederholung",
        "CODE": "Code"
      },
      "LOGIN-PWDRESET-FORM": {
        "TITLE": "Passwort neu setzen",
        "RESET": "Setzen"
      },
      "LOGIN-PWDREQUEST-FORM": {
        "TITLE": "Link zur Rücksetzung des Passwort",
        "REQUEST": "Anfordern",
        "BACK": "Zurück"
      },
      "LOGOUT-AUTO-FORM": {
        "TITLE": "Automatische Abmeldung",
        "TEXT": "Sie wurden sicherheitshalber automatisch abgemeldet. Bitte melden Sie sich neu an.",
        "RELOGIN": "Wieder anmelden"
      },
      "LOGOUT-USER-FORM": {
        "TITLE": "Abmeldung",
        "TEXT": "Sie haben sich erfolgreich abgemeldet. Bitte melden Sie sich neu an.",
        "RELOGIN": "Wieder anmelden"
      }
    },
    "DESKTOP": {
      "MODULE": {
        "ACTION": "anzeigen",
        "ITEMS": {
            "calendar": {
              "TITLE": "Termine",
              "DESCRIPTION": "Verwalten Sie hier Ihre Termine - geschäftlich und privat"
            },
            "customers": {
              "TITLE": "Kunden",
              "DESCRIPTION": "Interessenten, Kunden und private Kontakte verwalten"
            },
            "invoice": {
              "TITLE": "Rechnungen und Mahnungen",
              "DESCRIPTION": "Rechnungen, Mahnungen, einzeln und gebündelt für Ihre Buchhaltung"
            },
            "expenses": {
              "TITLE": "Ausgaben",
              "DESCRIPTION": "Erhaltene Rechnungen, Ausgaben und Spesen Erfassung, Kassen usw."
            },
            "purchaseinvoices": {
              "TITLE": "Erhaltene Rechnungen",
              "DESCRIPTION": "Erhaltene Rechnungen erfassen und bezahlen"
            },
            "accounting": {
              "TITLE": "Buchhaltung",
              "DESCRIPTION": "Buchhaltung einfach führen"
            },
            "portal": {
              "TITLE": "Web Auftritt",
              "DESCRIPTION": "Web Auftritt, Bilder, Schriften usw."
            },
            "sites": {
              "TITLE": "Web-Auftritt",
              "DESCRIPTION": "Web-Auftritt pflegen"
            },
            "openHours": {
              "TITLE": "Arbeits- und Öffnungszeiten",
              "DESCRIPTION": "Mitarbeiter Arbeitszeiten und Standort Öffnungszeiten verwalten"
            },
            "configuration": {
              "TITLE": "Konfiguration",
              "DESCRIPTION": "Verwalten Sie hier alle Organisationsdaten"
            },
            "settings": {
              "TITLE": "Einstellungen",
              "DESCRIPTION": "Systemeinstellungen vornehmen"
            },
            "booking": {
              "TITLE": "Online Buchungen",
              "DESCRIPTION": "Einstellungen für das Online Buchungs Modul vornehmen"
            },
            "myaccount": {
              "TITLE": "Mein Account",
              "DESCRIPTION": "Passwort, gebuchte Module, Weiterempfehlung usw."
            },
            "padmin": {
              "TITLE": "Plattform Admin",
              "DESCRIPTION": "Funktion für Plattform-Administratoren"
            },
            "psupport": {
              "TITLE": "Support",
              "DESCRIPTION": "Supporter-Funktionen"
            }
          }
      }
    }
  },
  "SYSMAILS": {
    "TYPES": {
      "INVOICE": "Rechnung",
      "INVOICE_REMINDER_1": "Erste Mahnung",
      "INVOICE_REMINDER_2": "Zweite Mahnung",
      "ONLINEBOOKING_CONFIRMATION_CUSTOMER": "Bestätigung Anfrage Kunde",
      "ONLINEBOOKING_APPROVAL_CUSTOMER": "Information Bestätigung Kunde",
      "ONLINEBOOKING_DECLINATION_CUSTOMER": "Information Ablehnung Kunde",
      "ONLINEBOOKING_CANCELLATION_CUSTOMER": "Bestätigung Stornierung Kunde",
      "ONLINEBOOKING_REMINDER_CUSTOMER": "Terminerinnerung Kunde",
      "ONLINEBOOKING_CONFIRMATION_EMPLOYEE": "Information Anfrage Mitarbeiter",
      "ONLINEBOOKING_APPROVAL_EMPLOYEE": "Information Auto-Bestätigung Mitarbeiter",
      "ONLINEBOOKING_CANCELLATION_EMPLOYEE": "Information Stornierung Mitarbeiter"
    },
    "STATES": {
      "OPEN": "Offen",
      "SENT": "Versandt",
      "TRIED": "Versucht",
      "ERROR": "Fehler"
    }
  },
  "FOOTER": "© 2017 by bluebamboo GmbH, Schweiz / all rights reserved"
}
