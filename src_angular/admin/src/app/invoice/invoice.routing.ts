import { InvoiceRequestsComponent } from './invoice-page/invoice-requests/invoice-requests.component';
import {Routes} from '@angular/router';
import {InvoicePageComponent} from './invoice-page/invoice-page.component';
import {InvoiceCreateComponent} from './invoice-page/invoice-create/invoice-create.component';
import {InvoiceListComponent} from './invoice-page/invoice-list/invoice-list.component';
import {InvoiceAbleResolver, InvoiceResolver} from './invoice-page/resolver';
import {InvoiceCreateBookingComponent} from './invoice-page/invoice-create/invoice-create-booking/invoice-create-booking.component';
import {InvoiceCreateReminderComponent} from './invoice-page/invoice-create/invoice-create-reminder/invoice-create-reminder.component';
import {InvoiceCreateFinishComponent} from './invoice-page/invoice-create/invoice-create-finish/invoice-create-finish.component';
import {InvoiceViewComponent} from './invoice-page/invoice-view/invoice-view.component';
import {InvoicePaidListComponent} from './invoice-page/invoice-paid-list/invoice-paid-list.component';
import {InvoiceReminderListComponent} from './invoice-page/invoice-reminder-list/invoice-reminder-list.component';
import {InvoiceCancelListComponent} from './invoice-page/invoice-cancel-list/invoice-cancel-list.component';
import {InvoicePrePrintListComponent} from './invoice-page/invoice-pre-print-list/invoice-pre-print-list.component';
import {InvoicePrePrintDownloadComponent} from './invoice-page/invoice-pre-print-download/invoice-pre-print-download.component';

import {InvoiceResultReminderPreviewComponent} from './invoice-page/invoice-preview-reminder/invoice-preview-reminder-result.component';
import { InvoiceEmailsComponent } from './invoice-page/invoice-emails/invoice-email.component';

import {CamtUploadComponent} from "./camt-upload/camt-upload.component";
import {CamtListComponent} from "./camt-list/camt-list.component";
export const invoiceRoutes: Routes = [
  { path: 'invoice', pathMatch: 'full', component: InvoicePageComponent },
  {
    path: 'invoice/create',
    component: InvoiceCreateComponent,
    resolve: {
      list: InvoiceAbleResolver
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: InvoiceCreateBookingComponent
      },
      {
        path: 'reminder',
        pathMatch: 'full',
        component: InvoiceCreateReminderComponent
      },
      {
        path: 'done',
        pathMatch: 'full',
        component: InvoiceCreateFinishComponent
      }
    ]
  },
  {
    path: 'invoice/invoicerequests',
    component: InvoiceRequestsComponent,
    children: [ ],
  },
  {
    path: 'invoice/list',
    component: InvoiceListComponent,
    children: [
      {
        path: ':id',
        pathMatch: 'full',
        component: InvoiceViewComponent,
        resolve: {
          invoice: InvoiceResolver
        }
      },
      {
        path: ':id/paid',
        pathMatch: 'full',
        component: InvoicePaidListComponent,
        resolve: {
          invoice: InvoiceResolver
        }
      },
      {
        path: ':id/reminder',
        pathMatch: 'full',
        component: InvoiceReminderListComponent,
        resolve: {
          invoice: InvoiceResolver
        }
      },
      {
        path: ':id/cancel',
        pathMatch: 'full',
        component: InvoiceCancelListComponent,
        resolve: {
          invoice: InvoiceResolver
        }
      }
    ]
  },
  {
    path: 'invoice/paid',
    component: InvoicePaidListComponent,
    pathMatch: 'full'
  },
  {
    path: 'invoice/reminder',
    component: InvoiceReminderListComponent,
    pathMatch: 'full'
  },
  {
    path: 'invoice/cancel',
    component: InvoiceCancelListComponent,
    pathMatch: 'full'
  },
  {
    path: 'invoice/pre-print-download/:filename',
    component: InvoicePrePrintDownloadComponent,
    pathMatch: 'full'
  },
  {
    path: 'invoice/reminder/result',
    pathMatch: 'full',
    component: InvoiceResultReminderPreviewComponent,
  },
  {
    path: 'invoice/pre-print',
    component: InvoicePrePrintListComponent,
    pathMatch: 'full'
  },
  {
    path: 'invoice/invoice-emails',
    component: InvoiceEmailsComponent,
    pathMatch: 'full'
  },
  {
    path: 'invoice/camt-upload',
    component: CamtUploadComponent,
    pathMatch: 'full'
  },
  {
    path: 'invoice/camt-list',
    component: CamtListComponent,
    pathMatch: 'full'
  }
];
