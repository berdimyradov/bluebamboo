import { SysEmailsService } from '../services/sysemails.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonUiModule } from '@bluebamboo/common-ui';
import { InvoicePageComponent } from './invoice-page/invoice-page.component';
import { InvoiceService } from './invoice.service';
import { AdminCommonModule } from '../common';
import { InvoiceCreateComponent } from './invoice-page/invoice-create/invoice-create.component';
import { InvoiceListComponent } from './invoice-page/invoice-list/invoice-list.component';
import { InvoiceRequestsComponent } from './invoice-page/invoice-requests/invoice-requests.component';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {InvoiceAbleResolver, InvoiceResolver} from './invoice-page/resolver';
import { InvoiceCreateBookingComponent } from './invoice-page/invoice-create/invoice-create-booking/invoice-create-booking.component';
import { InvoiceCreateReminderComponent } from './invoice-page/invoice-create/invoice-create-reminder/invoice-create-reminder.component';
import { InvoiceCreateFinishComponent } from './invoice-page/invoice-create/invoice-create-finish/invoice-create-finish.component';
import { InvoiceViewComponent } from './invoice-page/invoice-view/invoice-view.component';
import { InvoicePaidListComponent } from './invoice-page/invoice-paid-list/invoice-paid-list.component';
import { InvoiceReminderListComponent } from './invoice-page/invoice-reminder-list/invoice-reminder-list.component';
import { InvoiceCancelListComponent } from './invoice-page/invoice-cancel-list/invoice-cancel-list.component';
import { InvoicePrePrintListComponent } from './invoice-page/invoice-pre-print-list/invoice-pre-print-list.component';
import { InvoicePrePrintDownloadComponent } from './invoice-page/invoice-pre-print-download/invoice-pre-print-download.component';
import { InvoiceResultReminderPreviewComponent } from './invoice-page/invoice-preview-reminder/invoice-preview-reminder-result.component';

import { locale as en } from './resources/en';
import { locale as de } from './resources/de';

import { PaymentPipe } from './invoice-page/pipes/invoice-page-payment.pipe';
import { DatePipe } from '@angular/common';
import { InvoiceEmailsComponent } from './invoice-page/invoice-emails/invoice-email.component';

import { CamtUploadComponent } from './camt-upload/camt-upload.component';
import { CamtListComponent } from './camt-list/camt-list.component';
import {CamtService} from "./camt.service";

@NgModule({
  imports: [
    CommonModule,
    CommonUiModule,
    AdminCommonModule,
    FormsModule,
    BrowserAnimationsModule,
    RouterModule,
    TranslateModule
  ],
  declarations: [
    InvoicePageComponent,
    InvoiceRequestsComponent,
    InvoiceCreateComponent,
    InvoiceListComponent,
    InvoiceCreateBookingComponent,
    InvoiceCreateReminderComponent,
    InvoiceCreateFinishComponent,
    InvoiceViewComponent,
    InvoicePaidListComponent,
    InvoiceReminderListComponent,
    InvoiceCancelListComponent,
    InvoicePrePrintListComponent,
    InvoicePrePrintListComponent,
    InvoicePrePrintDownloadComponent,
    InvoiceResultReminderPreviewComponent,
    InvoiceEmailsComponent,
    PaymentPipe,
    InvoicePageComponent, InvoiceCreateComponent, InvoiceListComponent, InvoiceCreateBookingComponent,
    InvoiceCreateReminderComponent, InvoiceCreateFinishComponent,
    InvoiceViewComponent, InvoicePaidListComponent, InvoiceReminderListComponent, InvoiceCancelListComponent,
    InvoicePrePrintListComponent, InvoicePrePrintListComponent, InvoicePrePrintDownloadComponent,
    InvoiceResultReminderPreviewComponent, PaymentPipe, CamtUploadComponent, CamtListComponent
  ],
  providers: [
    InvoiceService,
    InvoiceAbleResolver,
    InvoiceResolver,
    SysEmailsService,
    DatePipe,
    CamtService
  ],
  exports: [PaymentPipe]
})
export class InvoiceModule {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
  }
}
