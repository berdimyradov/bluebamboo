import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CamtListComponent } from './camt-list.component';

describe('CamtListComponent', () => {
  let component: CamtListComponent;
  let fixture: ComponentFixture<CamtListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CamtListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CamtListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
