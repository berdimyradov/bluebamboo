import {Component, OnInit, ViewChild} from '@angular/core';
import {Location} from '@angular/common';
import {BbGridComponent} from "../../../../../common-ui/src/app/common-ui/bb-grid/bb-grid.component";
import {ArrayDataProvider} from "../../../../../common-ui/src/app/common-ui/bb-grid/types/array-data-provider";
import {CommonPageComponent} from "../../common/common-page/common-page.component";
import {CamtService} from "../camt.service";
import {InvoiceCamtRow} from "../invoice-page/types/invoice-camt-row";
import {TranslateModule} from "@ngx-translate/core";

@Component({
  selector: 'admin-camt-list',
  templateUrl: './camt-list.component.html',
  styleUrls: ['./camt-list.component.scss']
})
export class CamtListComponent implements OnInit {
  private static SearchTimeOut = 300;
  private _search: string;
  private _searchTimeOut: any;
  @ViewChild('grid') private grid: BbGridComponent;

  public list: InvoiceCamtRow[] = [];
  public gridModules: any[] = [TranslateModule];
  public get search(): string {
    return this._search;
  }

  public set search(v: string) {
    this._search = v;
    this.startSearch(v);
  }

  constructor(private commonPage: CommonPageComponent,
              private camtService: CamtService,
              private _location: Location) {
  }

  ngOnInit() {
    this.commonPage.title = 'PAGE.INVOICE-PAID.TITLE';
    this.refresh();
  }

  private refresh() {
    this.camtService.getGridData()
      .subscribe(
        result => {
          console.dir(result);
          // result.forEach((item: InvoiceCamtRow) => this.initRow(item));
          this.list = result;
          this.startSearch(this._search, 0);
        }
      );
  }

  public back() {
    this._location.back();
  }

  private startSearch(term: string, wait: number = CamtListComponent.SearchTimeOut) {
    if (this._searchTimeOut) {
      clearTimeout(this._searchTimeOut);
    }
    this._searchTimeOut = setTimeout(() => {
      (this.grid.dataProvider as ArrayDataProvider).setFullTextFilter(term, ['number', 'customer_name', 'customer_firstname']);
    }, wait);
  }
}
