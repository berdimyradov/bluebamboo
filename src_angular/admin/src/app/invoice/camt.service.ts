import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Http, Response} from "@angular/http";
import {InvoiceCamtRow} from "./invoice-page/types/invoice-camt-row";

@Injectable()
export class CamtService {
  constructor(private http: Http) { }

  public getGridData(): Observable<InvoiceCamtRow[]> {
    return this.http.get('/api/v1/pay/camt/all')
      .map((res: Response) => (res.json() as any[]).map((it:any) => (new InvoiceCamtRow()).parse(it)) )
      .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
