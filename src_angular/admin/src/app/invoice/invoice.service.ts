import { Injectable } from '@angular/core';
import { CommonMenuItem, Invoice } from "../common/";
import { Observable } from "rxjs/Observable";
import { InvoiceAble, InvoiceCreate, InvoiceCreateResponse, InvoicePaid } from "./invoice-page/types";
import { Http, Response } from "@angular/http";
import { InvoiceGridRow } from "./invoice-page/types/invoice-grid-row";
import { InvoiceReminder } from "./invoice-page/types/invoice-reminder";
import * as moment from 'moment';

@Injectable()
export class InvoiceService {
  private _modules: CommonMenuItem[] = [
    new CommonMenuItem().parse({
      code: 'create',
      icon: 'fa-file-text-o',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'list',
      icon: 'fa-files-o',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'invoicerequests',
      icon: 'fa-files-o',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'paid',
      icon: 'fa-money',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'reminder',
      icon: 'fa-clock-o',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'cancel',
      icon: 'fa-times',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'pre-print',
      icon: 'fa-print',
      implement: true
    }),
    new CommonMenuItem().parse({
      code: 'invoice-emails',
      icon: 'fa-envelope',
      implement: true
    }),
      new CommonMenuItem().parse({
          code: 'camt-upload',
          icon: 'fa-upload',
          implement: true
      }),
      new CommonMenuItem().parse({
          code: 'camt-list',
          icon: 'fa-list',
          implement: true
      })
  ];

  constructor(private http: Http) { }

  public getListModules(): Observable<CommonMenuItem[]> {
    return (new Observable<CommonMenuItem[]>(observable => observable.next(this._modules)));
  }

  public getListOfInvoiceAble(): Observable<InvoiceAble[]> {
    return this.http.get('/api/v1/booking/bookings/invoiceable')
      .map((res: Response) => (res.json() as any[]).map((it: any) => (new InvoiceAble()).parse(it)))
      .catch(this.handleError);
  }

  public getCompundInvoices(): Observable<any[]> {
    return this.http.get('/api/v1/invo/invoicerequests')
    .map((res: Response) => (res.json() ))
    .catch(this.handleError);
  }

  public createInvoices(invoiceAbles: InvoiceAble[], printPrivCopy: boolean): Observable<InvoiceCreateResponse> {
    return this.http.post('/api/v1/invo/invoicerequests', new InvoiceCreate(invoiceAbles, printPrivCopy))
      .map((res: Response) => (new InvoiceCreateResponse()).parse(res.json()))
      .catch(this.handleError);
  }

  public getGridData(): Observable<InvoiceGridRow[]> {
    return this.http.get('/api/v1/invo/invoices/all')
      .map((res: Response) => (res.json() as any[]).map((it: any) => (new InvoiceGridRow()).parse(it)))
      .catch(this.handleError);
  }

  public getPaidGridData(): Observable<InvoiceGridRow[]> {
    return this.http.get('/api/v1/invo/invoices/unpaid')
      .map((res: Response) => (res.json() as any[]).map((it: any) => (new InvoiceGridRow()).parse(it)))
      .catch(this.handleError);
  }

  public getReminderGridData(): Observable<InvoiceGridRow[]> {
    return this.http.get('/api/v1/invo/invoices/overdue')
      .map((res: Response) => (res.json() as any[]).map((it: any) => (new InvoiceGridRow()).parse(it)))
      .catch(this.handleError);
  }

  public getCancelGridData(): Observable<InvoiceGridRow[]> {
    return this.http.get('/api/v1/invo/invoices/uncanceled')
      .map((res: Response) => (res.json() as any[]).map((it: any) => (new InvoiceGridRow()).parse(it)))
      .catch(this.handleError);
  }

  public getPrePrintGridData(): Observable<InvoiceGridRow[][]> {
    return this.http.get('/api/v1/booking/bookings/invoiceable/tomtod')
      .map((res: Response) => {
        let data = res.json();
        let result = [];
        result.push((data.today || []).map((it: any) => (new InvoiceGridRow()).parse(it)));
        result.push((data.tomorrow || []).map((it: any) => (new InvoiceGridRow()).parse(it)));
        return result;
      })
      .catch(this.handleError);
  }

  public setPrePrint(rows: any[]): Observable<void> {
    return this.http.post('/api/v1/invo/invoicerequests/preprint', rows)
      .map((res) => (new InvoiceCreateResponse()).parse(res.json()))
      .catch(this.handleError);
  }

  public get(id: number): Observable<Invoice> {
    return this.http.get('/api/v1/invo/invoices/' + id.toString())
      .map((res: Response) => (new Invoice()).parse(res.json()))
      .catch(this.handleError);
  }

  public setReminderForInvoice(invoices: InvoiceReminder[]): Observable<any> {
    return this.http.post('/api/v1/invo/invoicerequests/reminder', {
      invoices: invoices, createdAt: moment().format('YYYY-MM-DD HH:mm:ss')
    })
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public cancelInvoice(invoice: Invoice, comment: string): Observable<any> {
    return this.http.put('/api/v1/invo/invoices/actions/storno/' + invoice.id, {
      comment: comment
    })
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public setPaidInvoice(invoice: InvoicePaid): Observable<any> {
    return this.http.put('/api/v1/invo/invoices/actions/pay/' + invoice.id, invoice)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public resendInvoice(invoice: Invoice): Observable<any> {
    return this.http.put('/api/v1/invo/invoices/actions/resend/' + invoice.id, invoice)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
