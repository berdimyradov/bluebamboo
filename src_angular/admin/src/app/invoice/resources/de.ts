export const locale = {
  'PAGE': {
    'INVOICEREQUESTS': {
      'TITLE': 'Rechnungsläufe',
      'GRID':{
        'CREATED_AT': 'Datum',
        'DATE': 'Anzahl',
        'TYPE': 'Typ',
        'ACTION': 'Aktionen',
        'ACTIONS': {
          'ACTIONTITLE': 'Aktionen',
          'PREVIEWPDF': 'Rechnungslauf Druck PDF downloaden'
        }
      },
      'TYPE': {
        'CREATE': 'Rechnung',
        'PREPRINT': 'Vordruck',
        'REMIND': 'Mahnung'
      }
    },
    'INVOICE': {
      'TITLE': 'Rechnungen und Mahnungen',
      'ITEMS': {
        'create': {
          'TITLE': 'Erstellen'
        },
        'invoicerequests':{
          'TITLE': 'Rechnungsläufe'
        },
        'list': {
          'TITLE': 'Anzeigen'
        },
        'paid': {
          'TITLE': 'Bezahlungen erfassen'
        },
        'reminder': {
          'TITLE': 'Mahnen'
        },
        "pre-print": {
          "TITLE": "Vordruck"
        },
        'invoice-emails': {
          'TITLE': 'Rechnungs E-Mails'
        },
        'cancel': {
          'TITLE': 'Stornieren'
        },
        "camt-upload": {
          "TITLE": "Camt upload"
        },
        "camt-list": {
          "TITLE": "List Camt"
        }
      }
    },
    'INVOICE-CREATE': {
      'TITLE': 'Rechnungen Erstellen'
    },
    'INVOICE-CREATE-BOOKING': {
      'SELECT-ALL': 'Alle wählen',
      'DESELECT-ALL': 'Alle abwählen',
      'CANCEL': 'Abbrechen',
      'APPLY': 'Weiter',
      'SERVICES': '{{name}}, {{count}} von {{count_total}} Terminen, {{sum}}',
      'TOTAL': 'Total diese Rechnungsstellung: {{sum}}'
    },
    'INVOICE-CREATE-REMINDER': {
      'BACK': 'Zurück',
      'CANCEL': 'Abbrechen',
      'APPLY': 'Rechnungen erstellen',
      'SERVICES': '{{name}}, {{count}} von {{count_total}} Terminen, {{sum}}',
      'SEND_EMAIL': 'Rechnung per Mail',
      'PRINT': 'Rechnung drucken',
      'PRINT_PRIV_COPY': 'Archivkopien drucken'
    },
    'INVOICE-CREATE-FINISH': {
      'TEXT': 'Die Rechnungen wurden erfolgreich erstellt und verschickt!',
      'DOWNLOAD_PDF': 'PDF Datei für Druck downloaden',
      'OK': 'OK'
    },
    'INVOICE-LIST': {
      'TITLE': 'Anzeigen',
      'BACK': 'Zurück',
      'SEARCH': 'Suchen',
      'GRID': {
        'DATE': 'Datum',
        'NAME': 'Name',
        'FIRST_NAME': 'Vorname',
        'INVOICE_NUM': 'Rechnungs Nr.',
        'AMOUNT': 'Betrag',
        'STATUS': 'Status',
        'ACTION': 'Aktion'
      },
      'ACTIONS': {
        'PAID': 'Rechnungen auf bezahlt setzen',
        'REMINDER': 'Rechnungen mahnen',
        'CANCEL': 'Rechnungen stornieren'
      }
    },
    "INVOICE-VIEW": {
      "BACK": "Zurück",
      "TITLE": "Rechnungen anzeigen",
      "CONFIRM-RESEND": "Die Rechnung noch einmal per E-Mail versenden?",
      "CONFIRM-YES": "OK",
      "CONFIRM-NO": "Abbrechen",
      "FORM": {
        "TITLE": "Rechnung Nr. {{number}}",
        "DATE": "Rechnungsdatum",
        "COMMENT": "Bemerkung zur Bezahlung",
        "PAYMENT_METHOD": "Zahlungsart",
        "PAYMENT:METHOD:BAR": "Bar",
        "PAYMENT:METHOD:TRANSFER": "Überweisung",
        "PAYMENT:METHOD:KARTE": "EC-Karte",
        "PAYMENT:METHOD:KREDITKARTE": "Kreditkarte",
        "PAYMENT_DATE": "Zahlungseingang",
        "STATUS": "Status",
        "DOWNLOAD_PDF": "    PDF Rechnung",
        "FILE_REMINDER1": "    PDF 1. Mahnung",
        "FILE_REMINDER2": "    PDF 2. Mahnung",
        "FILE_COPY_INSURANCE": "    PDF Rückforderungsbeleg",
        "FILE_COPY_ARCHIVE": "    PDF Archivkopie",
        "RESEND-INVOICE": "Rechnung erneut senden",
        "CANCEL-INVOICE": "Rechnung stornieren",
        "REMIND-INVOICE": "Mahnung auslösen",
        "PAY-INVOICE": "Rechnungen auf bezahlt setzen"
      },
      "POSITIONS": {
        "TITLE": "Positionen",
        "DATE": "Datum",
        "DESCRIPTION": "Bezeichnung",
        "DURATION": "Dauer",
        "AMOUNT": "Betrag",
        "TOTAL": "<b>Total: </b> CHF "
      }
    },
    "INVOICE-PAID": {
      "TITLE": "Bezahlungen erfassen",
      "BACK": "Zurück",
      "SEARCH": "Suchen",
      "GRID": {
        "DATE": "Datum",
        "NAME": "Name",
        "FIRST_NAME": "Vorname",
        "INVOICE_NUM": "Rechnungs Nr.",
        "AMOUNT": "Betrag",
        "STATUS": "Status",
        "ACTION": "Aktion",
        "ACTIONS": {
          "PAID": "Bezahlt >"
        }
      },
      "FORM": {
        "APPLY": "Bezahlung erfassen",
        "CANCEL": "Abbrechen",
        "PAYMENT_METHOD": "Zahlungsart:",
        "PAYMENT_DATE": "Zahlungseingang:",
        "COMMENTS": "Bemerkungen:",
        "METHOD": {
          "CASH": "Bar",
          "BANK_ACCOUNT": "Überweisung",
          "CREDIT_CARD": "Kreditkarte",
          "DEBIT_CARD": "EC Karte"
        },
        "SELECT_OPTION": "Bitte wählen"
      }
    },
    "INVOICE-REMINDER": {
      "TITLE": "Rechnungen mahnen",
      "BACK": "zurück",
      "SEARCH": "Suchen",
      "RESULT": {
        "WITHPDF": "Die Mahnungen wurden erfolgreich erstellt und ggf. per E-Mail versandt. Zum Drucken der Mahnungen, für die die Option 'Druck' gewählt wurde, laden Sie bitte die PDF Datei herunter:",
        "WITHOUTPDF": "Die Mahnungen wurden erfolgreich erstellt und per E-Mail versandt.",
        "LINK": "PDF Datei für Druck downloaden"
      },
      "GRID": {
        "DATE": "Datum",
        "NAME": "Name",
        "FIRST_NAME": "Vorname",
        "INVOICE_NUM": "Rechnungs Nr.",
        "AMOUNT": "Betrag",
        "STATUS": "Status",
        "ACTION": "Aktion",
        "CHECKBOX": {
          "REMIND": "Erinnern",
          "PRINT": "Druck",
          "EMAIL": "eMail"
        }
      },
      "FORM": {
        "TEXT": "Alle markierten Rechnungen mahnen",
        "APPLY": "Mahnen",
        "CANCEL": "Abbrechen"
      }
    },
    "INVOICE-CANCEL": {
      "TITLE": "Rechnungen stornieren",
      "BACK": "Zurück",
      "SEARCH": "Suchen",
      "GRID": {
        "DATE": "Datum",
        "NAME": "Name",
        "FIRST_NAME": "Vorname",
        "INVOICE_NUM": "Rechnungs Nr.",
        "AMOUNT": "Betrag",
        "STATUS": "Status",
        "ACTION": "Aktion",
        "ACTIONS": {
          "CANCEL": "Stornieren"
        }
      },
      "FORM": {
        "DESCRIPTION": "Stornierungsgrund / Bemerkungen",
        "EMAIL": "Storno per Mail senden ",
        "PRINT": "Storno drucken",
        "APPLY": "Stornieren",
        "CANCEL": "Abbrechen"
      },
      "POSITIONS": {
        "TITLE": "Positionen",
        "DATE": "Datum",
        "DESCRIPTION": "Bezeichnung",
        "DURATION": "Dauer",
        "AMOUNT": "Betrag",
        "TOTAL": "<b>Total: </b> CHF "
      }
    },
    "INVOICE-PRE-PRINT": {
      "TITLE": "Vordruck",
      "DESCRIPTION": "Sie möchten die Rechnungen gleich bei der Behandlung abgeben? Hier können Sie noch nicht erbrachte Leistungen verrechnen.",
      "BACK": "Zurück",
      "TODAY": "Leistungen von <b>heute</b>",
      "TOMORROW": "Leistungen von <b>morgen</b>",
      "SELECT-ALL": "Alle auswählen",
      "GRID": {
        "DATE": "Datum",
        "NAME": "Name",
        "FIRST_NAME": "Vorname",
        "INVOICE_NUM": "Rechnungs Nr.",
        "AMOUNT": "Betrag",
        "ACTION": "Drucken",
        "TITLE": "Titel",
      },
      "FORM": {
        "APPLY": "Rechnungen erstellen",
        "TEXT": "Rechnungen für alle markierten Leistungen erstellen."
      }
    },
    "INVOICE_EMAILS": {
      "ACTIONS": {
        "SHOW": "Rechnung anzeigen"
      }
    },
    "MISSING-FIELDS": {
      "HEADER": "Für die Verrechnung dieses Termins fehlen noch folgende Eingaben:",
      "CUSTOMER": "Kunde",
      "CUSTOMER.NAME": "Kunden - Nachname",
      "CUSTOMER.FIRSTNAME": "Kunden - Vorname",
      "CUSTOMER.STREET": "Kunden - Straße",
      "CUSTOMER.ZIP": "Kunden - PLZ",
      "CUSTOMER.CITY": "Kunden - Stadt",
      "CUSTOMER.BIRTHDAY": "Kunden - Geburtstag",
      "ORGANIZATION": "Organisation",
      "ORGANIZATION.NAME": "Organisation - Name",
      "ORGANIZATION.STREET": "Organisation - Straße",
      "ORGANIZATION.ZIP": "Organisation - PLZ",
      "ORGANIZATION.CITY": "Organisation - Stadt",
      "ORGANIZATION.IBAN": "Organisation - IBAN",
      "ORGANIZATION.MWST": "Organisation - ist MWST-pflichtig?",
      "ORGANIZATION.MWST_NR": "Organisation - MWST-Nr.",
      "ORGANIZATION.PAYMENT_TERM": "Organisation - Zahlungsfrist",
      "EMPLOYEE": "Mitarbeiter",
      "EMPLOYEE.FIRSTNAME": "Mitarbeiter - Vorname",
      "EMPLOYEE.NAME": "Mitarbeiter - Nachname",
      "EMPLOYEE.ZSR": "Mitarbeiter - ZSR-Nr.",
      "CASE": "Fall",
      "CASE.TYPE": "Fall - Behandlungsgrund",
      "CASE.THERAPY_TYPE": "Fall - Therapie",
      "CASE.PAYMENT_TYPE": "Fall - Vergütungsart",
      "TEMPLATE.INVOICE": "Rechnungsvorlage",
      "TEMPLATE.RECLAIM_SLIP_590": "Rechnungsvorlage Rückforderungsbeleg Tarif 590"
    }
  }
};
