import {Component, OnInit} from '@angular/core';
import {Http, RequestOptions, Headers} from "@angular/http";
import {Location} from '@angular/common';
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-camt-upload',
  templateUrl: './camt-upload.component.html',
  styleUrls: ['./camt-upload.component.scss']
})
export class CamtUploadComponent implements OnInit {
  uploadUrl: string = '/api/v1/pay/camt/upload';

  constructor(private _location: Location) {
  }

  ngOnInit() {
  }

  onFileUpload (response: any) {
    console.log('camt file uploaded', response);
    alert("Camt file was successfully uploaded!");
  }

  onFileUploadFail(error: any) {
    console.error(error);
    alert("Error!" + error);
  }

  public back() {
    this._location.back();
  }

}
