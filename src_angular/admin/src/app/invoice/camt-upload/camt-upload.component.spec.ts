import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CamtUploadComponent } from './camt-upload.component';

describe('CamtUploadComponent', () => {
  let component: CamtUploadComponent;
  let fixture: ComponentFixture<CamtUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CamtUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CamtUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
