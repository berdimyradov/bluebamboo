import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { InvoiceCreateComponent } from '../invoice-create.component';
import { InvoiceAble } from '../../types';
import { DecimalPipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { AutoHideInterface, SnackState } from '../../../../common/snackbar/types/snack';
import { SnackbarComponent } from '../../../../common/snackbar/snackbar.component';

@Component({
  selector: 'admin-invoice-create-reminder',
  templateUrl: './invoice-create-reminder.component.html',
  styleUrls: ['./invoice-create-reminder.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceCreateReminderComponent implements OnInit {
  public get list(): InvoiceAble[] {
    return this.invoiceCreate.list.filter((it: InvoiceAble) => (it.count_selected > 0 ? true : false));
  }
  public get printPrivCopy(): boolean { return this.invoiceCreate.printPrivCopy; }
  public set printPrivCopy(v: boolean) { this.invoiceCreate.printPrivCopy = v; }
  public snackState = new SnackState({hide: true, delay: 5000}, false, '', '');
  @ViewChild(SnackbarComponent) snackBar: SnackbarComponent;

  constructor(
    private invoiceCreate: InvoiceCreateComponent,
    private translate: TranslateService
  ) { }

  ngOnInit() {
  }

  public formatNumber(d: number): string {
    return (new DecimalPipe(this.translate.currentLang)).transform(d, '1.2');
  }

  public cancel() {
    this.invoiceCreate.back();
  }

  public goToBooking() {
    this.invoiceCreate.goToBooking();
  }

  public apply() {
    this.invoiceCreate.apply();
  }

  public onEmailCheckboxClick(invoice: any, val: number, eventSrc: MouseEvent): boolean {
    const email = invoice.email || invoice.second_email;
    if (!val && !email) {
      (<HTMLInputElement>eventSrc.target).checked = false;
      const message = `no email information for ${invoice.name}, ${invoice.firstname}`;
      this.snackState.setTemper('danger');
      this.snackState.setHeader(`Email is required for this option!`);
      this.snackState.setMessage(message);
      this.snackBar.displaySnackBar();
      return false;
    }
  }
}

