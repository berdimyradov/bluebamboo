import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceCreateReminderComponent } from './invoice-create-reminder.component';

describe('InvoiceCreateReminderComponent', () => {
  let component: InvoiceCreateReminderComponent;
  let fixture: ComponentFixture<InvoiceCreateReminderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceCreateReminderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceCreateReminderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
