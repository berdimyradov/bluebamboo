import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonPageComponent} from '../../../common';
import {InvoiceAble, InvoiceCreateResponse} from '../types';
import {InvoiceService} from '../../invoice.service';
import { PreLoaderService } from '../../../common/preloader/preloader.service';

@Component({
  selector: 'admin-invoice-create',
  templateUrl: './invoice-create.component.html',
  styleUrls: ['./invoice-create.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceCreateComponent implements OnInit {
  public list: InvoiceAble[] = [];
  public printPrivCopy: boolean = false;
  public result: InvoiceCreateResponse;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private invoice: InvoiceService,
    private preloader: PreLoaderService
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.INVOICE-CREATE.TITLE';
    this.list = this.activeRoute.snapshot.data.list;
  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public done() {
    this.router.navigate(['./done'], {relativeTo: this.activeRoute});
  }

  public goToReminder() {
    this.router.navigate(['./reminder'], {relativeTo: this.activeRoute});
  }

  public goToBooking() {
    this.router.navigate(['./'], {relativeTo: this.activeRoute});
  }

  public apply() {
    this.preloader.displayLoader(true);
    this.invoice.createInvoices(this.list, this.printPrivCopy)
      .subscribe(
        result => {
          this.result = result;
          this.done()
          this.preloader.displayLoader(false);
        }
      )
  }
}
