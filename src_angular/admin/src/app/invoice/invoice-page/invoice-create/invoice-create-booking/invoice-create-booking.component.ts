import {Component, OnInit, ViewEncapsulation, ViewChildren, QueryList,} from '@angular/core';
import {InvoiceCreateComponent} from "../invoice-create.component";
import {InvoiceAble} from "../../types/invoice-able";
import * as decimal from 'decimal.js';
import {TranslateService} from "@ngx-translate/core";
import {DecimalPipe} from "@angular/common";
import {CommonSelectedComponent} from "../../../../common/common-selected/common-selected.component";

@Component({
  selector: 'admin-invoice-create-booking',
  templateUrl: './invoice-create-booking.component.html',
  styleUrls: ['./invoice-create-booking.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceCreateBookingComponent implements OnInit {

  public selectedAllCheckboxes = true;
  @ViewChildren(CommonSelectedComponent) commonSelects: QueryList<CommonSelectedComponent>;

  public get list(): InvoiceAble[] {
    return this.invoiceCreate.list;
  }

  public get price(): number {
    let res = new decimal.default(0);
    this.list.forEach((it: InvoiceAble) => {
      res = res.add(it.price);
    });
    return res.toNumber() || 0;
  }

  constructor(
    private invoiceCreate: InvoiceCreateComponent,
    private translate: TranslateService
  ) { }

  ngOnInit() {
  }

  public formatNumber(d: number): string {
    return (new DecimalPipe(this.translate.currentLang)).transform(d,'1.2');
  }

  public cancel() {
    this.invoiceCreate.back();
  }

  public goToReminder() {
    this.invoiceCreate.goToReminder();
  }

  public onSelecedAllCheckboxesChange($event){
    // console.log('select all: ', this.selectedAllCheckboxes);
    // console.log(this.commonSelects);
    this.commonSelects.forEach( (select) => select.setAll(this.selectedAllCheckboxes));
  }
}
