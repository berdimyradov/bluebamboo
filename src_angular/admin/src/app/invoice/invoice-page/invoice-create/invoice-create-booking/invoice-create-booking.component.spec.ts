import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceCreateBookingComponent } from './invoice-create-booking.component';

describe('InvoiceCreateBookingComponent', () => {
  let component: InvoiceCreateBookingComponent;
  let fixture: ComponentFixture<InvoiceCreateBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceCreateBookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceCreateBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
