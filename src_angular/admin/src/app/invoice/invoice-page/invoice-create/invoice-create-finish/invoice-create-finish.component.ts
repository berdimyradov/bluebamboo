import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {InvoiceCreateComponent} from "../invoice-create.component";
import {InvoiceCreateResponse} from "../../types/invoice-create-response";

@Component({
  selector: 'app-invoice-create-finish',
  templateUrl: './invoice-create-finish.component.html',
  styleUrls: ['./invoice-create-finish.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceCreateFinishComponent implements OnInit {
  public get result() : InvoiceCreateResponse {
    return this.invoiceCreate.result;
  }

  constructor(
    private invoiceCreate: InvoiceCreateComponent
  ) { }

  ngOnInit() {
  }

  public close() {
    this.invoiceCreate.back();
  }
}
