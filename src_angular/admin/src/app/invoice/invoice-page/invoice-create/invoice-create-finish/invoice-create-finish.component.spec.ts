import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceCreateFinishComponent } from './invoice-create-finish.component';

describe('InvoiceCreateFinishComponent', () => {
  let component: InvoiceCreateFinishComponent;
  let fixture: ComponentFixture<InvoiceCreateFinishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceCreateFinishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceCreateFinishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
