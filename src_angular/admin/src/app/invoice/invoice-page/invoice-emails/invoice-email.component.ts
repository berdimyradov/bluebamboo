import { CommonPageComponent } from '../../../common/common-page/common-page.component';
import { UiGridHeaders } from '../../../common/grid/types/ui-grid.headers';
import { SysEmailsService } from '../../../services';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UiGridComponent } from '../../../common/grid/ui-grid.component';
import { ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-invoice-emails',
  templateUrl: 'invoice-emails.component.html',
  styleUrls: ['./invoice-emails.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class InvoiceEmailsComponent implements OnInit {
  @ViewChild(UiGridComponent)
  public UiGridComponent: UiGridComponent;
  public nickNamesForRows: Array<{
                  colName: string,
                  displayName: string,
                  sortable: boolean
                }>;

  constructor( private emailsService: SysEmailsService,
               private _location: Location,
               private router: Router,
               private activeRoute: ActivatedRoute,
               private commonPage: CommonPageComponent) { }

  ngOnInit() {
    this.commonPage.title = 'Rechnungs E-Mails';
    this.nickNamesForRows = [
      { colName: 'createdAt', displayName: 'Erstellt', sortable: true },
      { colName: 'state', displayName: 'Status', sortable: true },
      { colName: 'last_send_attempt', displayName: 'Letzter Sendeversuch', sortable: true },
      { colName: 'to', displayName: 'Empfänger', sortable: true },
      { colName: 'type', displayName: 'Art', sortable: true}
    ];


    this.emailsService.getInvoiceEmails()
        .subscribe((data) => {
            this.UiGridComponent.LoadData(data);
        });
  }

  public emailClick(row) {
    this.router.navigate([`../list/${row.item_id}`], {relativeTo: this.activeRoute});
  }

  // Back
  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

}
