import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicePrePrintDownloadComponent } from './invoice-pre-print-download.component';

describe('InvoicePrePrintDownloadComponent', () => {
  let component: InvoicePrePrintDownloadComponent;
  let fixture: ComponentFixture<InvoicePrePrintDownloadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicePrePrintDownloadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicePrePrintDownloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
