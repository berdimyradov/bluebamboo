import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-invoice-pre-print-download',
  templateUrl: './invoice-pre-print-download.component.html',
  styleUrls: ['./invoice-pre-print-download.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoicePrePrintDownloadComponent implements OnInit {
  public url: string;
  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
  ) {
    this.url = "/api/v1/invo/download/pdf/" +  this.activeRoute.snapshot.paramMap.get('filename');
  }

  ngOnInit() {
  }

  public close() {
    this.router.navigate(['../../pre-print'], {relativeTo: this.activeRoute});
  }
}
