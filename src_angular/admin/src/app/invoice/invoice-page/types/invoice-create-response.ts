export class InvoiceCreateResponse {
  public id: number;
  public client_id: number;
  public createAt: string;
  public file: string;
  public sentMails: boolean;

  public get url(): string {
    return '/api/v1/invo/download/' + this.file;
  }

  public get hasFile(): boolean {
    if(this.file === null || this.file === undefined || this.file === "") return false;
    return true;
  }

  public parse(data: any): InvoiceCreateResponse {
    const names: string[] = ['id','client_id','createAt','file', 'sentMails'];
    names.forEach((name) => this[name] = data[name] || this[name]);
    return this;
  }
}
