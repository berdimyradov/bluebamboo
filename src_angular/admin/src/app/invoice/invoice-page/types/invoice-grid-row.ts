export class InvoiceGridRow {
  public id: number;
  public client_id: number;
  public customer_id: number;
  public invoicerequest_id: number;
  public organization_id: number;
  public number: string;
  public price: number;
  public title: string;
  public date: string;
  public customer_name: string;
  public customer_firstname: string;
  public status: string;
  public value: number;
  public date_reminder_1: string;
  public date_reminder_2: string;
  public file: string;
  public file_copy_archive: string;
  public file_copy_insurance: string;
  public paid: Function;
  public pdfFilePreview: Function;
  public itemClick: Function;
  public reminder: Function;
  public cancel: Function;
  public isCancelAble: boolean;
  public isPaidAble: boolean;
  public isRemindAble: boolean;
  public missing_fields_invoice: string[];
  public isInvoiceable: boolean;

  public parse(data: any): InvoiceGridRow {
    const names: string[] = ['id','client_id','customer_id','invoicerequest_id','organization_id', 'customer_firstname',
      'number','date','status','value','date_reminder_1','date_reminder_2','file','file_copy_archive',
      'file_copy_insurance', 'customer_name', 'title', 'price', 'missing_fields_invoice'];
    names.forEach((name) => this[name] = data[name] || this[name]);
    if (data.customer){
      this.customer_firstname = this.customer_firstname || data.customer.firstname
      this.customer_name = this.customer_name || data.customer.name
    }

    this.isInvoiceable = data.invoiceable;
    this.isRemindAble = ['ope', 're1', 'dep', 'dr1', 'dr2'].indexOf(this.status) >= 0;
    this.isPaidAble = ['ope', 're1', 're2', 'dep', 'dr1', 'dr2'].indexOf(this.status) >= 0;
    this.isCancelAble = ['ope', 're1', 're2'].indexOf(this.status) >= 0;
    return this;
  }
}
