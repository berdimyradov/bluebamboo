export class InvoicePaid {
  public id: number;
  public payed_by: number;
  public payed_at: string;
  public value: number;
  public bankaccount_iban: string;
  public bankaccount_bic: string;
  public bankaccount_bank: string;
  public bankaccount_owner: string;
  public comment: string;
  public customer_id: number;
  public client_id: number;
}
