import * as moment from "../../../../../../../dist/platforms/bluebamboo/vendor/moment/moment";
import _date = moment.unitOfTime._date;

export class InvoiceCamtRow {
  public id: number;
  public creditor_iban: string;
  public amount: number;
  public currency: string;
  public booking_date: string;
  public value_date: string;
  public xml_tree: string;
  public created_at: string;
  public updated_at: string;

  public parse(data: any): InvoiceCamtRow {
    const names: string[] = ['id', 'creditor_iban', 'amount', 'currency', 'booking_date', 'value_date', 'xml_tree', 'created_at', 'updated_at'];

    names.forEach((name) => this[name] = data[name] || this[name]);

    return this;
  }
}
