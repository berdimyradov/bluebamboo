import { InvoiceBooking } from './invoice-booking';
import * as decimal from 'decimal.js';

export class InvoiceCases {
  private _selected: any[] = [];
  private _sendEmail: boolean = false;
  private _print: boolean = true;
  public get selected(): any[] {
    return this._selected;
  }
  public get count(): number {
    return this._selected.length;
  }
  public get price(): number {
    let res = new decimal.default(0);
    this._selected.forEach((it: any) => {
      const b = this.bookings.find((f: InvoiceBooking) => f.id == it.id ? true : false);
      res = res.add(b.price || 0);
    });
    return res.toNumber() || 0;
  }
  public get sendEmail(): boolean { return this._sendEmail; }
  public set sendEmail(v: boolean) { this._sendEmail = v; }
  public get print(): boolean { return this._print; }
  public set print(v: boolean) { this._print = v; }

  public id: number;
  public client_id: number;
  public customer_id: number;
  public payment_type: string;
  public therapy_type: string;
  public type: string;
  public title: string;
  public image: string;
  public comment: string;

  public referarral_name: string;
  public referarral_type: string;
  public referarral_gln: string;
  public referarral_zsr: string;
  public accident_date: string;

  public bookings: InvoiceBooking[] = [];

  public parse(data: any): InvoiceCases {
    const names: string[] = ['id', 'client_id', 'customer_id', 'payment_type', 'therapy_type', 'type', 'title', 'image',
      'comment', 'referarral_name', 'referarral_type', 'referarral_gln',
      'referarral_zsr', 'accident_date'];
    names.forEach((name) => this[name] = data[name] || this[name]);
    if (data.bookings) {
      this.bookings.splice(0, this.bookings.length);
      data.bookings.forEach((it: any) => this.bookings.push((new InvoiceBooking()).parse(it)));
    }

    this._selected = [];
    this.bookings.forEach((booking: InvoiceBooking) => {
      if (booking.invoiceable) {
        this._selected.push({ id: booking.id.toString() });
      }
    });

    return this;
  }
}
