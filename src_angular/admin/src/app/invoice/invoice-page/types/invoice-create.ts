import * as moment from 'moment/moment';
import {InvoiceAble, InvoiceCases} from "./";

export class InvoiceCreate {
  public createAt: string;
  public customers: any[];

  constructor (invoiceAbles: InvoiceAble[], public printPrivCopy: boolean) {
    this.createAt = moment().format('YYYY-MM-DD HH:mm:ss');
    this.customers = this.prepareData(invoiceAbles);
  }

  private prepareData(invoiceAbles: InvoiceAble[]): any[] {
    const res: any[] = [];
    invoiceAbles.forEach((invoice: InvoiceAble) => {
      const customer: any = {
        id: invoice.cases[0].bookings[0].customer_id,
        client_id: invoice.client_id,
        cases: []
      };
      invoice.cases.forEach((cas: InvoiceCases) => {
        if (cas.selected.length == 0) {
          return;
        }
        const resCase = {
          id: cas.id,
          client_id: cas.client_id,
          customer_id: cas.customer_id,
          sendmail: cas.sendEmail,
          print: cas.print,
          bookings: cas.selected.map((it: any) => parseInt(it.id))
        };
        customer.cases.push(resCase);
      });
      if (customer.cases.length > 0) {
        res.push(customer);
      }
    });
    return res;
  }
}
