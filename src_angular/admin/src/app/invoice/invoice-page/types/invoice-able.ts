import { InvoiceCases } from "./invoice-cases";
import * as decimal from 'decimal.js';

export class InvoiceAble {
  public get price(): number {
    let res = new decimal.default(0);
    this.cases.forEach((it: InvoiceCases) => {
      res = res.add(it.price);
    });
    return res.toNumber() || 0;
  }
  public get count_selected(): number {
    return this.cases.reduce((prev: number, element: InvoiceCases) => (element.selected.length + prev), 0);
  }

  public id: number;
  public type: string;
  public client_id: number;
  public comment: string;

  public firstname: string;
  public name: string;
  public gender: string;
  public photo: string;

  public birth_day: string;
  public birth_month: string;
  public birth_year: string;

  public caregiver_id: number;
  public dossier_id: number;
  public guardian_id: number;
  public insurance_id: number;
  public insurance_nr: number;
  public known_from: string;
  public nr: string;

  public address: string;
  public address_line: string;
  public address_you: number;
  public city: string;
  public country: string;
  public street: string;
  public state: string;
  public zip: string;

  public email: string;
  public email_primary: string;
  public email_second: string;

  public phone_home: string;
  public phone_mobile: string;
  public phone_work: string;


  public cases: InvoiceCases[] = [];

  public parse(data: any): InvoiceAble {
    const names: string[] = ['id', 'type', 'client_id', 'comment', 'firstname', 'name', 'gender', 'photo', 'birth_day'
      , 'birth_month', 'birth_year', 'caregiver_id', 'dossier_id', 'guardian_id', 'insurance_id', 'insurance_nr', 'known_from'
      , 'nr', 'address', 'address_line', 'address_you', 'city', 'country', 'street', 'state', 'zip', 'email', 'email_primary'
      , 'email_second', 'phone_home', 'phone_mobile', 'phone_work'];
    names.forEach((name) => this[name] = data[name] || this[name]);
    if (data.cases) {
      this.cases.splice(0, this.cases.length);
      data.cases.forEach((it: any) => this.cases.push((new InvoiceCases()).parse(it)));
    }
    return this;
  }
}
