import * as moment from "../../../../../../../dist/platforms/bluebamboo/vendor/moment/moment";
import _date = moment.unitOfTime._date;

export class InvoiceExcelRow {
  public id: number;
  public first_name: string;
  public last_name: string;
  public email: string;
  public ip_address: string;
  public gender: string;
  public street: string;
  public car: string;
  public city: string;
  public hair_color: string;
  public zip: string;
  public birthday: _date;
  public website: string;
  public avatar: string;
  public geo: string;
  public married: boolean;
  public state: string;
  public country: string;
  public user_agent: string;
  public work: string;
  public language: string;
  public created_at: _date;
  public updated_at: _date;

  public parse(data: any): InvoiceExcelRow {
    const names: string[] = [
      'id',
      'first_name',
      'last_name',
      'email',
      'ip_address',
      'gender',
      'street',
      'car',
      'city',
      'hair_color',
      'zip',
      'birthday',
      'website',
      'avatar',
      'geo',
      'married',
      'state',
      'country',
      'user_agent',
      'work',
      'language',
      'created_at',
      'updated_at'
    ];

    names.forEach((name) => this[name] = data[name] || this[name]);

    return this;
  }
}
