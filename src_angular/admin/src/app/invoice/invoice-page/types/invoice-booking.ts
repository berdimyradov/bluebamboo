import * as moment from 'moment/moment';

export class InvoiceBooking {
  private _name: string;
  public get name(): string { return this._name; }
  public id: number;
  public customer_id: number;
  public title: string;
  public date: string;
  public price: number;
  public invoiceable: boolean;
  public missing_fields_invoice: string[];

  public parse(data: any): InvoiceBooking {
    const names: string[] = ['id', 'title', 'date', 'price', 'customer_id',
     'invoiceable', 'missing_fields_invoice'];
    names.forEach((name) => this[name] = data[name] || this[name]);

    this._name = moment(this.date).format('DD.MM.YYYY') + '  ' + this.title + ' ';
    return this;
  }
}
