import * as moment from "../../../../../../../dist/platforms/bluebamboo/vendor/moment/moment";
import _date = moment.unitOfTime._date;
import {TargetRow} from "../../../configuration/models/target";
import {RecordRow} from "../../../configuration/models/record";

export class ImportRow {
  public id: number;
  public created_at: _date;
  public filename: string;
  public col01: number;
  public col02: number;
  public col03: number;
  public col04: number;
  public col05: number;
  public col06: number;
  public col07: number;
  public col08: number;
  public col09: number;
  public col10: number;
  public col11: number;
  public col12: number;
  public col13: number;
  public col14: number;
  public col15: number;
  public col16: number;
  public col17: number;
  public col18: number;
  public col19: number;
  public col20: number;
  public target: TargetRow;
  public records: RecordRow[];

  public parse(data: any): ImportRow {
    const names: string[] = [
      'id',
      'created_at',
      'filename',
      'col01',
      'col02',
      'col03',
      'col04',
      'col05',
      'col06',
      'col07',
      'col08',
      'col09',
      'col10',
      'col11',
      'col12',
      'col13',
      'col14',
      'col15',
      'col16',
      'col17',
      'col18',
      'col19',
      'col20',
      'target',
      'records'
    ];

    names.forEach((name) => this[name] = data[name] || this[name]);

    return this;
  }
}
