import { Pipe, PipeTransform } from '@angular/core';
/*
 * Pipe for formatting payment method in
 * invoices component
*/
@Pipe({ name: 'paymentFormatter' })
export class PaymentPipe implements PipeTransform {
  transform(value: number): string {
    let formatted = '';
    switch (+value) {
      case 0:
        formatted = 'PAGE.INVOICE-VIEW.FORM.PAYMENT:METHOD:BAR';
        break;
      case 1:
        formatted = 'PAGE.INVOICE-VIEW.FORM.PAYMENT:METHOD:TRANSFER';
        break;
      case 2:
        formatted = 'PAGE.INVOICE-VIEW.FORM.PAYMENT:METHOD:KARTE';
        break;
      case 3:
        formatted = 'PAGE.INVOICE-VIEW.FORM.PAYMENT:METHOD:KREDITKARTE';
        break;
      default:
        formatted = 'unknown';
    }
    return formatted;

  }
}
