import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { BbGridComponent, ArrayDataProvider } from '@bluebamboo/common-ui';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { CommonPageComponent } from '../../../common';
import { InvoiceService } from '../../invoice.service';
import { InvoiceGridRow } from '../types/invoice-grid-row';
import { Inject } from '@angular/core';

import 'rxjs/add/operator/filter';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'admin-invoice-list',
  templateUrl: './invoice-requests.component.html',
  styleUrls: ['./invoice-requests.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceRequestsComponent implements OnInit, OnDestroy {
  private static SearchTimeOut = 300;
  private _search: string;
  private _searchTimeOut: any;
  private _subscription: Subscription;
  private _invoicesSubscription: Subscription;
  @ViewChild('grid') private grid: BbGridComponent;

  public list: InvoiceGridRow[] = [];
  public showMainScreen = true;
  public gridModules: any[] = [TranslateModule];
  public templateAction = `<a *ngIf="row.file"
                               class="btn-floating btn-sm white btn-command"
                               (click)="row.pdfFilePreview($event)"
                               mdbTooltip
                               data-toggle="tooltip"
                               data-placement="top"
                               title="{{\'PAGE.INVOICEREQUESTS.GRID.ACTIONS.PREVIEWPDF\'|translate}}">
                               <i class="fa fa-file-pdf-o"></i>
                          </a>`;
  public gridData = [];

  public get search(): string {
    return this._search;
  }
  public set search(v: string) {
    this._search = v;
    this.startSearch(v);
  }

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private invoice: InvoiceService,
    private _location: Location,
    @Inject(Window) private win: Window
  ) {
  }

  ngOnInit() {
    this.commonPage.title = 'PAGE.INVOICEREQUESTS.TITLE';
    this.refresh();

    this._subscription = this.router.events
      .filter(e => e instanceof NavigationEnd)
      .subscribe(() => {
        this.refresh();
      });
  }

  public back() {
    this._location.back();
  }

  private initRow(row: InvoiceGridRow) {
    row.pdfFilePreview = ($event) => {
      const path = '/api/v1/invo/download/pdf/' + row.file;
      $event.stopPropagation();
      this.win.open(path);
      // this.router.navigate(['./' + row.id + '/paid'], {relativeTo: this.activeRoute});
    };

    row.itemClick = ($event) => {
      $event.stopPropagation();
    };
  }

  private startSearch(term: string) {
    if (this._searchTimeOut) {
      clearTimeout(this._searchTimeOut);
    }
    this._searchTimeOut = setTimeout(() => {
      (this.grid.dataProvider as ArrayDataProvider).setFullTextFilter(term, ['createdAt', 'type']);
    }, InvoiceRequestsComponent.SearchTimeOut);
  }

  public onActivate() {
    this.showMainScreen = false;
  }

  public onDeactivate() {
    this.showMainScreen = true;
  }

  private refresh() {
    this.invoice.getCompundInvoices()
      .subscribe(
        result => {
          result.forEach((item: any) => this.initRow(item));
          this.gridData = result;
          this.startSearch(this._search);
        }
      );
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
