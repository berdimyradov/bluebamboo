import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {routeAnimationOpacity, CommonPageComponent, CommonMenuItem} from "../../common";
import {InvoiceService} from "../invoice.service";

@Component({
  selector: 'app-invoice-page',
  templateUrl: './invoice-page.component.html',
  styleUrls: ['./invoice-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class InvoicePageComponent implements OnInit {
  public modules: CommonMenuItem[] = [];

  constructor(
    private invoiceService: InvoiceService,
    private commonPage: CommonPageComponent
  ) {
  }

  ngOnInit() {
    this.commonPage.title = 'PAGE.INVOICE.TITLE';
    this.invoiceService.getListModules()
      .subscribe(
        result => this.modules = result
      );

  }
}
