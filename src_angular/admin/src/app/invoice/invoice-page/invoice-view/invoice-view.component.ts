import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { CommonPageComponent, Invoice, DialogService } from '../../../common/';
import { InvoiceService } from '../../invoice.service';

@Component({
  selector: 'admin-invoice-view',
  templateUrl: './invoice-view.component.html',
  styleUrls: ['./invoice-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceViewComponent implements OnInit {
  public invoice: Invoice;
  public pdfPath: string;
  public pdfReminder1Path: string;
  public pdfReminder2Path: string;
  public pdfCopyInsurancePath: string;
  public pdfCopyArchivePath: string;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private dialog: DialogService,
    private invoiceService: InvoiceService,
    private _location: Location
  ) { }

  ngOnInit() {
    const API_PATH = '/api/v1/invo/download/';

    this.commonPage.title = 'PAGE.INVOICE-VIEW.TITLE';
    this.invoice = this.activeRoute.snapshot.data.invoice;
    this.pdfPath = API_PATH + this.invoice.file;
    this.pdfReminder1Path = API_PATH + this.invoice.file_reminder_1;
    this.pdfReminder2Path = API_PATH + this.invoice.file_reminder_2;
    this.pdfCopyInsurancePath = API_PATH + this.invoice.file_copy_insurance;
    this.pdfCopyArchivePath = API_PATH + this.invoice.file_copy_archive;
  }

  public resendInvoice() {
    this.dialog.confirm('PAGE.INVOICE-VIEW.CONFIRM-RESEND', 'PAGE.INVOICE-VIEW.CONFIRM-YES', 'PAGE.INVOICE-VIEW.CONFIRM-NO')
      .then((res: Boolean) => {
        if (res) {
          this.invoiceService.resendInvoice(this.invoice).subscribe(
            result => () => {}
          );
        }
      });
  }

  public cancelInvoice() {
    this.router.navigate(['./cancel'], {relativeTo: this.activeRoute});
  }

  public remindInvoice() {
    this.router.navigate(['./reminder'], {relativeTo: this.activeRoute});
  }

  public payInvoice() {
    this.router.navigate(['./paid'], {relativeTo: this.activeRoute});
  }

  public back() {
    this._location.back();
  }

  public get isResendActive() {
    return (this.invoice.customer.email || this.invoice.customer.email_second) && this.invoice.status !== 'can';
  }

  public get isPayActive() {
    return ['ope', 're1', 're2', 'dep', 'dr1', 'dr2'].includes(this.invoice.status);
  }

  public get isRemindActive() {
    return ['ope', 're1', 'dep', 'dr1', 'dr2'].includes(this.invoice.status);
  }

  public get isCancelActive() {
    return ['ope', 're1', 're2'].includes(this.invoice.status);
  }
}
