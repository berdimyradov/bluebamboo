import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { BbGridComponent, ArrayDataProvider } from '@bluebamboo/common-ui';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { InvoiceService } from '../../invoice.service';
import { CommonPageComponent, Invoice } from '../../../common/';
import { InvoiceGridRow, InvoiceReminder } from '../types';

@Component({
  selector: 'admin-invoice-reminder-list',
  templateUrl: './invoice-reminder-list.component.html',
  styleUrls: ['./invoice-reminder-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceReminderListComponent implements OnInit {
  private static SearchTimeOut = 300;
  private _search: string;
  private _searchTimeOut: any;
  @ViewChild('grid') private grid: BbGridComponent;

  public list: InvoiceGridRow[] = [];
  public selected: InvoiceGridRow[] = [];
  public gridModules: any[] = [TranslateModule];
  public get search(): string {
    return this._search;
  }
  public set search(v: string) {
    this._search = v;
    this.startSearch(v);
  }
  public templatePrint = '<mdb-checkbox [(ngModel)]="row.isPrint"></mdb-checkbox>';
  public templateEMail = '<mdb-checkbox [(ngModel)]="row.isEmail"></mdb-checkbox>';
  public templateState = '{{\'ENUM.INVOICE_STATE.\'+row.status+\'.TITLE\'|translate}}';
  public invoice: Invoice = null;
  public showSingle = false;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private invoiceService: InvoiceService,
    private _location: Location
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.INVOICE-REMINDER.TITLE';
    this.invoice = this.activeRoute.snapshot.data.invoice || null;
    this.showSingle = this.invoice !== null;

    if (this.showSingle) {
      const row: InvoiceGridRow = new InvoiceGridRow();

      row.id = this.invoice.id;
      row.date = this.invoice.date;
      row.number = this.invoice.number;
      row.customer_name = this.invoice.customer_name;
      row.status = this.invoice.status;
      row.value = this.invoice.value;

      this.initRow(row);
      this.list.push(row);

      return;
    }

    this.refresh();
  }

  public back() {
    this._location.back();
  }

  public apply() {
    this.invoiceService.setReminderForInvoice(this.selected.map((it: any) => {
      const res: InvoiceReminder = new InvoiceReminder();

      res.id = it.id;
      res._print = it.isPrint;
      res._sendMail = it.isEmail;

      return res;
    }))
      .subscribe(result => {
        if (this.showSingle) {
          this.router.navigate(['../../../reminder/result'],
          {
            queryParams: {
              filename: result.file,
              fromList : true
            },
              relativeTo: this.activeRoute
          });
        } else {
          this.router.navigate(['./result'], { queryParams: { filename: result.file }, relativeTo: this.activeRoute });
        }
      });
  }

  private startSearch(term: string, wait: number = InvoiceReminderListComponent.SearchTimeOut) {
    if (this._searchTimeOut) {
      clearTimeout(this._searchTimeOut);
    }
    this._searchTimeOut = setTimeout(() => {
      (this.grid.dataProvider as ArrayDataProvider).setFullTextFilter(term, ['number', 'customer_name', 'customer_firstname']);
    }, wait);
  }

  private initRow(row: any) {
    let email = false, print = false;

    const select = (v: boolean) => {
      const idx: number = this.selected.indexOf(row);

      if (v) {
        if (idx < 0) {
          this.selected.push(row);
        }

        return;
      }

      if (!row.isPrint && !row.isEmail && idx >= 0) {
        this.selected.splice(idx, 1);
      }
    };

    Object.defineProperties(row, {
      isPrint: {
        get: () => print,
        set: (v) => select(print = v)
      },
      isEmail: {
        get: () => email,
        set: (v) => select(email = v)
      },
    });
  }

  private refresh() {
    this.invoiceService.getReminderGridData()
      .subscribe(
        result => {
          result.forEach((item: InvoiceGridRow) => this.initRow(item));

          this.list = result;
          this.startSearch(this._search, 0);
        }
      );
  }
}
