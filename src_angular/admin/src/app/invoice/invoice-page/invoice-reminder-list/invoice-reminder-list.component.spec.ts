import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceReminderListComponent } from './invoice-reminder-list.component';

describe('InvoiceReminderListComponent', () => {
  let component: InvoiceReminderListComponent;
  let fixture: ComponentFixture<InvoiceReminderListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceReminderListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceReminderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
