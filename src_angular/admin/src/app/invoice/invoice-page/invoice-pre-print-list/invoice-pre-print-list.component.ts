import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { InvoiceGridRow } from '../types';
import { InvoiceService } from '../../invoice.service';
import { CommonPageComponent } from '../../../common';

@Component({
  selector: 'admin-invoice-pre-print-list',
  templateUrl: './invoice-pre-print-list.component.html',
  styleUrls: ['./invoice-pre-print-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoicePrePrintListComponent implements OnInit {
  public gridModules: any[] = [TranslateModule];
  public listToday: InvoiceGridRow[] = [];
  public listTomorrow: InvoiceGridRow[] = [];
  public selected: InvoiceGridRow [] = [];
  public templateAction = '<mdb-checkbox [(ngModel)]="row.isSelected" readonly="{{!row.isInvoiceable}}"></mdb-checkbox>';

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private invoice: InvoiceService
  ) {}

  ngOnInit() {
    this.commonPage.title = 'PAGE.INVOICE-PRE-PRINT.TITLE';
    this.refresh();
  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public generatePrePrint() {
    this.invoice.setPrePrint(this.selected.map((it: InvoiceGridRow) => ({
      id: it.id,
      customer_id: it.customer_id,
      client_id: it.client_id
    })))
      .subscribe((result: any) => {
        this.router.navigate(['../pre-print-download/', result.file], {relativeTo: this.activeRoute});
      });
  }

  private initRow(row: any): any {
    Object.defineProperties(row, {
      isSelected: {
        get: () => this.selected.indexOf(row) >= 0,
        set: (v) => {
          const i = this.selected.indexOf(row);

          if (v && i < 0) {
            this.selected.push(row);
          }

          if (!v && i >= 0) {
            this.selected.splice(i, 1);
          }
        }
      }
    });

    return row;
  }

  private refresh() {
    this.invoice.getPrePrintGridData()
      .subscribe(result => {
        this.listToday = result[0].map(it => this.initRow(it));
        this.listTomorrow = result[1].map(it => this.initRow(it));
      });
  }

  public get selectAllToday(): boolean {
    return !!this.listToday.length && this.listToday.every((item: any) => item.isSelected);
  }

  public get selectAllTomorrow(): boolean {
    return !!this.listTomorrow.length && this.listTomorrow.every((item: any) => item.isSelected);
  }

  public set selectAllToday(v: boolean) {
    this.listToday.forEach((item: any) => {
      if (item.isInvoiceable) {
        item.isSelected = v;
      }
    });
  }

  public set selectAllTomorrow(v: boolean) {
    this.listTomorrow.forEach((item: any) => {
      if (item.isInvoiceable) {
        item.isSelected = v;
      }
    });
  }

  public get isAllTomorrowReadOnly(): boolean {
    return this.listTomorrow.every((item: any) => item.isInvoiceable);
  }

  public get isAllTodayReadOnly(): boolean {
    return this.listToday.every((item: any) => item.isInvoiceable);
  }

  public get isAllTomorrowIndeterminate(): boolean {
    return this.listTomorrow.some((item: any) => item.isSelected) && !this.selectAllTomorrow;
  }

  public get isAllTodayIndeterminate(): boolean {
    return this.listToday.some((item: any) => item.isSelected) && !this.selectAllToday;
  }

  public onAllTomorrowClick($event) {
    if (this.listTomorrow.some((item: any) => !item.isInvoiceable)) {
      $event.preventDefault();

      this.selectAllTomorrow = !this.isAllTomorrowIndeterminate;
    }
  }

  public onAllTodayClick($event) {
    if (this.listToday.some((item: any) => !item.isInvoiceable)) {
      $event.preventDefault();

      this.selectAllToday = !this.isAllTodayIndeterminate;
    }
  }
}
