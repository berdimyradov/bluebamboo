import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicePrePrintListComponent } from './invoice-pre-print-list.component';

describe('InvoicePrePrintListComponent', () => {
  let component: InvoicePrePrintListComponent;
  let fixture: ComponentFixture<InvoicePrePrintListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicePrePrintListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicePrePrintListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
