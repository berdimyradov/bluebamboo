import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-invoice-preview-reminder',
  templateUrl: './invoice-preview-reminder-result.html',
  styleUrls: [],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceResultReminderPreviewComponent implements OnInit {
  public pdfURL: string;
  public emailNotification: string;
  public sub: any;
  public fromListUrl: boolean;
  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.sub = this.activeRoute
      .queryParams
      .subscribe(params => {
        if (params['filename']) {
          this.pdfURL = '/api/v1/invo/download/pdf/' + params['filename'];
        }
        if (params['fromList']) {
          this.fromListUrl = true;
        }
      });
  }

  public close() {
    if (this.fromListUrl) {
      this.router.navigate(['../../list'], { relativeTo: this.activeRoute });
    } else {
      this.router.navigate(['../'], { relativeTo: this.activeRoute });
    }
  }
}
