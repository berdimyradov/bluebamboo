import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {BbGridComponent, ArrayDataProvider} from '@bluebamboo/common-ui';
import {TranslateModule} from '@ngx-translate/core';
import {InvoiceService} from '../../invoice.service';
import {CommonPageComponent, Invoice} from '../../../common/';
import {InvoiceGridRow} from '../types';

@Component({
  selector: 'admin-invoice-cancel-list',
  templateUrl: './invoice-cancel-list.component.html',
  styleUrls: ['./invoice-cancel-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceCancelListComponent implements OnInit {
  private static SearchTimeOut = 300;
  private _search: string;
  private _searchTimeOut: any;

  public invoiceRow: Element = null;
  public invoice: Invoice = null;
  public comment: string;
  @ViewChild('grid') private grid: BbGridComponent;
  public list: InvoiceGridRow[] = [];
  public get search(): string {
    return this._search;
  }
  public set search(v: string) {
    this._search = v;
    this.startSearch(v);
  }
  public gridModules: any [] = [TranslateModule];
  public templateAction = '<button class="btn btn-default waves-effect waves-light btn-sm" (click)="row.cancel($event)">{{\'PAGE.INVOICE-CANCEL.GRID.ACTIONS.CANCEL\'|translate}}</button>';
  public templateState = '{{\'ENUM.INVOICE_STATE.\'+row.status+\'.TITLE\'|translate}}';
  public showSingle = false;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private invoiceService: InvoiceService,
    private _location: Location
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.INVOICE-CANCEL.TITLE';
    this.invoice = this.activeRoute.snapshot.data.invoice || null;
    this.showSingle = this.invoice !== null;

    if (this.showSingle) {
      const row: InvoiceGridRow = new InvoiceGridRow();

      row.id = this.invoice.id;
      row.date = this.invoice.date;
      row.number = this.invoice.number;
      row.customer_name = this.invoice.customer_name;
      row.status = this.invoice.status;
      row.value = this.invoice.value;

      this.list.push(row);

      return;
    }

    this.refresh();
  }

  public back() {
    this._location.back();
  }

  public cancel() {
    if (this.showSingle) {
      return this.back();
    }

    this.showRows();
  }

  public apply() {
    this.invoiceService.cancelInvoice(this.invoice, this.comment)
      .subscribe( () => {
        if (!this.showSingle) {
          this.refresh();
        }

        this.cancel();
      });
  }

  private startSearch(term: string, wait: number = InvoiceCancelListComponent.SearchTimeOut) {
    if (this._searchTimeOut) {
      clearTimeout(this._searchTimeOut);
    }

    this._searchTimeOut = setTimeout(() => {
      (this.grid.dataProvider as ArrayDataProvider).setFullTextFilter(term, ['number', 'customer_name', 'customer_firstname']);
    }, wait);
  }

  private initRow(row: InvoiceGridRow) {
    row.cancel = ($event) => {
      $event.stopPropagation();

      this.hideRows($event.target);
      this.invoiceService.get(row.id).subscribe( result => this.invoice = result);
    };
  }

  private hideRows(buttonElement: Element) {
    this.invoiceRow = buttonElement.parentElement.parentElement;
    this.invoiceRow.classList.add('selected-row');
    this.grid.disabled = true;
  }

  private showRows() {
    this.invoice = null;
    this.comment = '';
    this.invoiceRow.classList.remove('selected-row');
    this.invoiceRow = null;
    this.grid.disabled = false;
  }

  private refresh() {
    this.invoiceService.getCancelGridData()
      .subscribe(
        result => {
          result.forEach((item: InvoiceGridRow) => this.initRow(item));

          this.list = result;
          this.startSearch(this._search, 0);
        }
      );
  }
}
