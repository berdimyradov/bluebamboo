import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceCancelListComponent } from './invoice-cancel-list.component';

describe('InvoiceCancelListComponent', () => {
  let component: InvoiceCancelListComponent;
  let fixture: ComponentFixture<InvoiceCancelListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceCancelListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceCancelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
