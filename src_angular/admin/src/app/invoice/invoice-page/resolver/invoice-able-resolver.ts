import {Injectable} from "@angular/core";
import {InvoiceAble} from '../types';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {InvoiceService} from "../../invoice.service";

@Injectable()
export class InvoiceAbleResolver implements Resolve<InvoiceAble[]> {
  constructor (private invoice : InvoiceService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): InvoiceAble[] | Observable<InvoiceAble[]> | Promise<InvoiceAble[]> {
    return this.invoice.getListOfInvoiceAble();
  }

}
