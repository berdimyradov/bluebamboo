import {Injectable} from "@angular/core";
import {Invoice} from '../../../common';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {InvoiceService} from "../../invoice.service";

@Injectable()
export class InvoiceResolver implements Resolve<Invoice> {
  constructor (private invoice : InvoiceService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Invoice | Observable<Invoice> | Promise<Invoice> {
    const id: number = parseInt(route.paramMap.get('id'));
    return this.invoice.get(id);
  }

}
