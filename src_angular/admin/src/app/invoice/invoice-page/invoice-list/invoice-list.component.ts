import {Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {BbGridComponent, ArrayDataProvider} from '@bluebamboo/common-ui';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Location} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {CommonPageComponent} from '../../../common';
import {InvoiceService} from '../../invoice.service';
import {InvoiceGridRow} from '../types/invoice-grid-row';

import 'rxjs/add/operator/filter';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'admin-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceListComponent implements OnInit, OnDestroy {
  private static SearchTimeOut = 300;
  private _search: string;
  private _searchTimeOut: any;
  private _subscription: Subscription;
  @ViewChild('grid') private grid: BbGridComponent;

  public list: InvoiceGridRow[] = [];
  public showMainScreen = true;
  public gridModules: any[] = [TranslateModule];
  public templateAction = '<a *ngIf="row.isPaidAble" class="btn-floating btn-sm white btn-command" (click)="row.paid($event)" mdbTooltip  data-toggle="tooltip" data-placement="top" title="{{\'PAGE.INVOICE-LIST.ACTIONS.PAID\'|translate}}"><i class="fa fa-money"></i></a><a *ngIf="row.isRemindAble" class="btn-floating btn-sm white btn-command" (click)="row.reminder($event)" mdbTooltip  data-toggle="tooltip" data-placement="top" title="{{\'PAGE.INVOICE-LIST.ACTIONS.REMINDER\'|translate}}"><i class="fa fa-clock-o"></i></a><a *ngIf="row.isCancelAble" class="btn-floating btn-sm white btn-command" (click)="row.cancel($event)" mdbTooltip  data-toggle="tooltip" data-placement="top" title="{{\'PAGE.INVOICE-LIST.ACTIONS.CANCEL\'|translate}}"><i class="fa fa-times"></i></a>';
  public templateState = '{{\'ENUM.INVOICE_STATE.\'+row.status+\'.TITLE\'|translate}}';
  public get search(): string {
    return this._search;
  }
  public set search(v: string) {
    this._search = v;
    this.startSearch(v);
  }

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private invoice: InvoiceService,
    private _location: Location
  ) {
  }

  ngOnInit() {
    this.commonPage.title = 'PAGE.INVOICE-LIST.TITLE';

    this.refresh();

    this._subscription = this.router.events
      .filter(e => e instanceof NavigationEnd)
      .subscribe(() => {
        this.refresh();
      });
  }

  public back() {
    this._location.back();
  }

  public gridRowClick(row: InvoiceGridRow) {
    this.router.navigate(['./' + row.id], {relativeTo: this.activeRoute});
  }

  private initRow(row: InvoiceGridRow) {
    row.paid = ($event) => {
      $event.stopPropagation();

      this.router.navigate(['./' + row.id + '/paid'], {relativeTo: this.activeRoute});
    };
    row.reminder = ($event) => {
      $event.stopPropagation();

      this.router.navigate(['./' + row.id + '/reminder'], {relativeTo: this.activeRoute});
    };
    row.cancel = ($event) => {
      $event.stopPropagation();

      this.router.navigate(['./' + row.id + '/cancel'], {relativeTo: this.activeRoute});
    };
  }

  private startSearch(term: string) {
    if (this._searchTimeOut) {
      clearTimeout(this._searchTimeOut);
    }
    this._searchTimeOut = setTimeout(() => {
      (this.grid.dataProvider as ArrayDataProvider).setFullTextFilter(term, ['number', 'customer_name', 'customer_firstname']);
    }, InvoiceListComponent.SearchTimeOut);
  }

  public onActivate() {
    this.showMainScreen = false;
  }

  public onDeactivate() {
    this.showMainScreen = true;
  }

  private refresh() {
    this.invoice.getGridData()
      .subscribe(
        result => {
          result.forEach((item: InvoiceGridRow) => this.initRow(item));

          this.list = result;
          this.startSearch(this._search);
        }
      );
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
