import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicePaidListComponent } from './invoice-paid-list.component';

describe('InvoicePaidListComponent', () => {
  let component: InvoicePaidListComponent;
  let fixture: ComponentFixture<InvoicePaidListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicePaidListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicePaidListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
