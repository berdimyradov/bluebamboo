import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {Location} from '@angular/common';
import {BbGridComponent, ArrayDataProvider} from '@bluebamboo/common-ui';
import {TranslateModule} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {InvoiceService} from '../../invoice.service';
import {CommonPageComponent, Invoice} from '../../../common/';
import {InvoiceGridRow, InvoicePaid} from '../types';

@Component({
  selector: 'admin-invoice-paid-list',
  templateUrl: './invoice-paid-list.component.html',
  styleUrls: ['./invoice-paid-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoicePaidListComponent implements OnInit {
  private static SearchTimeOut = 300;
  private _search: string;
  private _searchTimeOut: any;
  @ViewChild('grid') private grid: BbGridComponent;

  public list: InvoiceGridRow[] = [];
  public gridModules: any[] = [TranslateModule];
  public get search(): string {
    return this._search;
  }
  public set search(v: string) {
    this._search = v;
    this.startSearch(v);
  }
  public templateAction = '<button class="btn btn-default waves-effect waves-light btn-sm" (click)="row.paid($event)">{{\'PAGE.INVOICE-PAID.GRID.ACTIONS.PAID\'|translate}}</button>';
  public templateState = '{{\'ENUM.INVOICE_STATE.\'+row.status+\'.TITLE\'|translate}}';
  public invoiceRow: Element = null;
  public invoice: Invoice = null;
  public invoiceRowData: InvoiceGridRow = null;
  public showSingle = false;
  public invoicePaid: InvoicePaid;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private invoiceService: InvoiceService,
    private _location: Location
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.INVOICE-PAID.TITLE';
    this.invoice = this.activeRoute.snapshot.data.invoice || null;
    this.showSingle = this.invoice === null ? false : true;
    if (this.showSingle) {
      let row: InvoiceGridRow = new InvoiceGridRow();
      row.id = this.invoice.id;
      row.date = this.invoice.date;
      row.number = this.invoice.number;
      row.customer_name = this.invoice.customer_name;
      row.status = this.invoice.status;
      row.value = this.invoice.value;
      this.list.push(row);
      this.invoicePaid = new InvoicePaid();
      return;
    }
    this.refresh();
  }

  public back() {
    this._location.back();
  }

  public cancel() {
    if (this.showSingle) {
      return this.back();
    }

    this.showRows();
  }

  public apply() {
    if (this.invoiceRowData) {
      this.invoicePaid.id = this.invoiceRowData.id;
      this.invoicePaid.client_id = this.invoiceRowData.client_id;
      this.invoicePaid.customer_id = this.invoiceRowData.customer_id;
    } else {
      this.invoicePaid.id = this.invoice.id;
      this.invoicePaid.client_id = this.invoice.client_id;
      this.invoicePaid.customer_id = this.invoice.customer_id;
    }
    this.invoiceService.setPaidInvoice(this.invoicePaid)
      .subscribe(
        () => {
          if (!this.showSingle) {
            this.refresh();
          }
          this.cancel();
        }
      );
  }

  private startSearch(term: string, wait: number = InvoicePaidListComponent.SearchTimeOut) {
    if (this._searchTimeOut) {
      clearTimeout(this._searchTimeOut);
    }
    this._searchTimeOut = setTimeout(() => {
      (this.grid.dataProvider as ArrayDataProvider).setFullTextFilter(term, ['number', 'customer_name', 'customer_firstname']);
    }, wait);
  }

  private initRow(row: InvoiceGridRow) {
    row.paid = ($event) => {
      $event.stopPropagation();
      this.invoiceRowData = row;
      this.hideRows($event.target);
    };
  }

  private hideRows(buttonElement: Element) {
    this.invoicePaid = new InvoicePaid();
    this.invoiceRow = buttonElement.parentElement.parentElement;
    this.invoiceRow.classList.add('selected-row');
    this.grid.disabled = true;
  }

  private showRows() {
    this.invoiceRow.classList.remove('selected-row');
    this.invoiceRow = null;
    this.grid.disabled = false;
  }

  private refresh() {
    this.invoiceService.getPaidGridData()
      .subscribe(
        result => {
          result.forEach((item: InvoiceGridRow) => this.initRow(item));
          this.list = result;
          this.startSearch(this._search, 0);
        }
      );
  }
}
