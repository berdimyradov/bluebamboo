import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import { DatePipe } from '@angular/common';


@Injectable()
export class InvoiceEmailsService {

  constructor(private http: Http, private datePipe: DatePipe) { }

  private parseType(status: number): string {
    switch (status) {
      case 200:
        return 'INVOICE';
      case 201:
        return 'INVOICE_REMINDER_1'
      case 202:
        return 'INVOICE_REMINDER_2'
    }
  }

  private parseState(char: string): string {
    switch (char) {
      case 'o':
        return 'Open';
      case 's':
        return 'Successfully sent'
      case 't':
        return 'Failed'
      case 'e':
        return 'Error'
      default:
        return 'Unknown'
    }
  }

  private parseLastSendAtempt(attempt: any): string {
    if (! attempt) {
      return '';
    } else {
      return this.datePipe.transform(attempt, 'dd.MM.yyyy, hh:mm');
    }
  }

  private parseTo(to: any) {
    return Object.keys(to)[0];
  }

  private parseEmails(rawData) {
    const dataForGrid = [];
    rawData.forEach(element => {
      dataForGrid.push({
        createdAt: this.datePipe.transform(element.createdAt, 'dd.MM.yyyy'),
        state: this.parseState(element.state),
        last_send_attempt: this.parseLastSendAtempt(element.last_send_attempt),
        type: this.parseType(element.type),
        to: this.parseTo(element.to),
        item_id: element.item_id
      });
    });
    return dataForGrid;
  }

  public getListOfEmails(): Observable<any> {
    return this.http.get('/api/v1/invo/emails')
      .map((res: Response) => {
        return this.parseEmails(res.json());
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
