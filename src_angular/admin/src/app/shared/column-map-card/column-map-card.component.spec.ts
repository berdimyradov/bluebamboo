import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnMapCardComponent } from './column-map-card.component';

describe('ColumnMapCardComponent', () => {
  let component: ColumnMapCardComponent;
  let fixture: ComponentFixture<ColumnMapCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColumnMapCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnMapCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
