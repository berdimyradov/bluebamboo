import {Component, Input, OnInit} from '@angular/core';
import {SelectOption} from '@bluebamboo/common-ui';

@Component({
  selector: 'column-map-card',
  templateUrl: './column-map-card.component.html',
  styleUrls: ['./column-map-card.component.scss']
})
export class ColumnMapCardComponent implements OnInit {
  public _title = '';
  public _content = '';
  public _selectOptions: SelectOption[] = [];
  public _value: string;

  constructor() {
  }

  ngOnInit() {
  }

  get title(): string {
    return this._title;
  }

  @Input()
  set title(value: string) {
    this._title = value;
  }

  get content(): string {
    return this._content;
  }

  @Input()
  set content(value: string) {
    this._content = value;
  }

  get selectOptions(): SelectOption[] {
    return this._selectOptions;
  }

  @Input()
  set selectOptions(value: SelectOption[]) {
    const defaultOption = new SelectOption({'title': 'None', 'code': 0});
    // this. selectedOption = defaultOption.code;

    this._selectOptions.push(defaultOption);
    this._selectOptions.push(...value);
  }

  get value(): string {
    return this._value;
  }

  @Input()
  set value(value: string) {
    this._value = value;
  }

}
