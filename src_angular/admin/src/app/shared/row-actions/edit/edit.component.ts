import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RecordRow} from "../../../configuration/models/record";

@Component({
  selector: 'bb-action-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  public _data: any;
  @Output() click: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  get data(): any {
    return this._data;
  }

  @Input() set data(value: any) {
    this._data = value;
  }

  onClick(event: Event) {
    this.click.emit(this.data);
  }

}
