import {Component, Output, OnInit, Input, EventEmitter} from '@angular/core';

@Component({
  selector: 'bb-action-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {
  public _data: any;
  @Output() click: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  get data(): any {
    return this._data;
  }

  @Input() set data(value: any) {
    this._data = value;
  }

  onClick(event: Event) {
    this.click.emit(this.data);
  }
}
