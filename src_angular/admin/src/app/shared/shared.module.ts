import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonUiModule } from '@bluebamboo/common-ui';
import { ColumnMapCardComponent } from './column-map-card/column-map-card.component';
import { EditComponent } from './row-actions/edit/edit.component';
import { DeleteComponent } from './row-actions/delete/delete.component';

@NgModule({
  imports: [
    CommonModule,
    CommonUiModule
  ],
  declarations: [
    ColumnMapCardComponent,
    EditComponent,
    DeleteComponent,
  ],
  exports: [
    ColumnMapCardComponent,
    EditComponent,
    DeleteComponent,
  ]
})
export class SharedModule { }
