import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { GenericResultModel } from '.';

@Injectable()
export class FrameworkService {

  constructor (protected http: Http) { }

  // -------------------------------------------------------------------------------------
  // generic post request

  protected postRequest(model: any, url: string): Observable<GenericResultModel> {
    return this.http.post(url, model)
      .map (
        (response: Response) => new GenericResultModel(response.json())
      )
      .catch (
        (error: any) => (
          new Observable<GenericResultModel>(
            observable => observable.next(
              new GenericResultModel().setError(error.text() == null || error.text() == '' ? "Fehler" : error.text())
            )
          )
        )
      )
  }

  // -------------------------------------------------------------------------------------
  // generic get request

  protected getRequest(url: string): Observable<GenericResultModel> {
    return this.http.get(url)
      .map (
        (response: Response) => new GenericResultModel(response.json())
      )
      .catch (
        (error: any) => (
          new Observable<GenericResultModel>(
            observable => observable.next(
              new GenericResultModel().setError(error.text() == null || error.text() == '' ? "Fehler" : error.text())
            )
          )
        )
      )
  }

  // -------------------------------------------------------------------------------------
  // generic error handler

  protected handleError (error: Response | any) {
    let errMsg: string;

    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);

      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    console.error(errMsg);

    return Observable.throw(errMsg);
  }

}
