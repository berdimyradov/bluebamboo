export class AbstractResultModel {
  private _error: string = '';

  public get isSuccess(): boolean {
    return this._error === '';
  }
  public get error():string {
    return this._error;
  }

  public setError(error: string): any {
    this._error =  error === null || error === undefined ? '' : error;
    return this;
  }

  public get message(): string {
    if (this._error === '') return '';
    let e = JSON.parse(this._error);
    if (!('message' in e)) return '';
    return e.message;
  }

}

export class GenericResultModel extends AbstractResultModel {
  private _result: any = null;

  constructor (result?: any) {
    super();
    if (result) this._result = result;
  }

  public get result(): any {
    return this._result;
  }

}
