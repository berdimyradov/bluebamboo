import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

import { routeAnimationOpacity, CommonPageComponent, DialogService } from "app/common";

import { CRUDObject }			from './crud-object.interface';
import { CRUDService }			from './crud-service.framework';

export abstract class CRUDFormComponent<SERVICE extends CRUDService<CLSTYPE>, CLSTYPE extends CRUDObject> implements OnInit {
	public object: CLSTYPE;

	constructor(
		protected commonPage: CommonPageComponent,
		protected router: Router,
		protected activeRoute: ActivatedRoute,
		protected service: SERVICE,
		protected dialog: DialogService
	) { }

	ngOnInit() {
		this.object = this.activeRoute.snapshot.data.obj;
		this.commonPage.title = this.getLabelKeys().pageTitle + (this.object.id ? "EDIT" : "NEW");
	}

	protected abstract getLabelKeys ();

	public back() {
		this.router.navigate(['../'], { relativeTo: this.activeRoute });
	}

	public save() {
		this.service.save(this.object)
			.subscribe(
				result => this.back()
			);
	}

	public delete() {
		this.dialog.confirm(this.getLabelKeys().delText,this.getLabelKeys().delYes,this.getLabelKeys().delNo)
			.then((res: Boolean) => {
				if (res) {
					this.service.delete(this.object.id).subscribe(
						result => this.back()
					);
				}
			});
	}
}
