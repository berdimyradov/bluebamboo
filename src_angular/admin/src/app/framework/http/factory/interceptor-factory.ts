import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import { InterceptorService } from 'ng2-interceptors';
import { SliderInterceptor } from '../interceptors/slider.interceptor';
import { ServerSessionInterceptor } from '../interceptors/server-session.interceptor';
/**
 * Base factory for Slider and Sesion handling
 * @param xhrBackend
 * @param requestOptions
 * @param SliderInterceptor
 * @param ServerSessionInterceptor
 */
export function interceptorFactory(
      xhrBackend: XHRBackend,
      requestOptions: RequestOptions,
      SliderInterceptor: SliderInterceptor,
      ServerSessionInterceptor: ServerSessionInterceptor) {
        const service = new InterceptorService(xhrBackend, requestOptions);
        service.addInterceptor(SliderInterceptor);
        service.addInterceptor(ServerSessionInterceptor);
        return service;
}
