import { Injectable } from '@angular/core';
import {
  Interceptor,
  InterceptedRequest,
  InterceptedResponse } from 'ng2-interceptors';
import { PreLoaderService } from '../../../common/preloader/preloader.service';

@Injectable()
export class SliderInterceptor implements Interceptor {

  constructor(private preloader: PreLoaderService) {}

  public interceptBefore(request: InterceptedRequest): InterceptedRequest {
    this.preloader.displayLoader(true);
    return request;
  }

  public interceptAfter(response: InterceptedResponse): InterceptedResponse {
    this.preloader.displayLoader(false);
    return response;
  }
}
