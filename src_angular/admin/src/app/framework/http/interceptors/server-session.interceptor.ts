import { Location } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import {
  Interceptor,
  InterceptedRequest,
  InterceptedResponse
} from 'ng2-interceptors';
import { PreLoaderService } from '../../../common/preloader/preloader.service';

@Injectable()
export class ServerSessionInterceptor implements Interceptor {

  constructor(private location: Location, @Inject(Window) private winRef: Window) { }

  public interceptBefore(request: InterceptedRequest): InterceptedRequest {
    return request;
  }

  public interceptAfter(response: InterceptedResponse): InterceptedResponse {
    if (+response.response.status === 403) {
      this.winRef.location.href = '/de/admin/auto_logout';
    }
    return response;
  }
}
