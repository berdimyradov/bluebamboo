import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { GenericResultModel } from '.';

import { FrameworkService } from '.';

@Injectable()
export abstract class CRUDService<T> extends FrameworkService {

  constructor (protected http: Http) { super(http); }

  // -------------------------------------------------------------------------------------
  // generic post request

	public abstract get     (id: number): Observable<T>;
  public abstract getList (): Observable<T[]>;
  public abstract save    (obj: T): Observable<T>;
  public abstract delete  (id: number): Observable<void>;


}
