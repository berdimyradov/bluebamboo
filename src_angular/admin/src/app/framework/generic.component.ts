import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { TranslateModule } from "@ngx-translate/core";

import { HelpService } from 'app/common/help/help.service';

export class GenericComponent implements OnInit {

	constructor(
		protected help: HelpService
	) { }

	ngOnInit() {
	}

}
