import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { TranslateModule } from "@ngx-translate/core";

import { CommonPageComponent, routeAnimationOpacity, DialogService } from '../common';
import { BbGridComponent, ArrayDataProvider } from "@bluebamboo/common-ui";

import { CRUDService } from './crud-service.framework';

export abstract class CRUDGridComponent<SERVICE extends CRUDService<CLSTYPE>, CLSTYPE> implements OnInit {

	public gridModules: any [] = [TranslateModule];

	@ViewChild('grid') private grid: BbGridComponent;

	public templateAction: string = '<a class="btn-floating btn-sm white btn-command" (click)="row.edit($event)" data-toggle="tooltip" data-placement="top" title="{{\'PAGE.REPORT-CONFIGURATION.ACTIONS.EDIT\'|translate}}"><i class="fa fa-pencil"></i></a><a class="btn-floating btn-sm white btn-command" (click)="row.delete($event)" data-toggle="tooltip" data-placement="top" title="{{\'PAGE.REPORT-CONFIGURATION.ACTIONS.DELETE\'|translate}}"><i class="fa fa-trash"></i></a>';

	public list: CLSTYPE[] = [];
	public row: Element;

	constructor(
		protected commonPage: CommonPageComponent,
		protected router: Router,
		protected activeRoute: ActivatedRoute,
		protected dialogService: DialogService,
		protected service: SERVICE
	) { }

	ngOnInit() {
		this.commonPage.title = this.getLabelKeys().pageTitle;
		this.refresh();
	}

	protected abstract getLabelKeys ();

	protected refresh() {
		this.service.getList().subscribe( result => {
			this.list = result.map(it => this.initRow(it));
//			this.startSearch(this._search, 0);
		});
	}

	protected initRow(row: any): any {
		row.edit = ($event) => {
			$event.stopPropagation();
			this.router.navigate(['./' + row.id], { relativeTo: this.activeRoute });
		};
		row.delete = ($event) => {
			$event.stopPropagation();
			this.dialogService.confirm(this.getLabelKeys().delText,this.getLabelKeys().delYes,this.getLabelKeys().delNo)
				.then((res: Boolean) => {
					if (res) {
						this.service.delete(row.id).subscribe(() => this.refresh());
					}
				});
		};
		return row;
	}

	protected addEntry () {
		this.router.navigate(['./add'], { relativeTo: this.activeRoute });
	}

	protected back () {
		this.router.navigate(['../'], { relativeTo: this.activeRoute });
	}

}
