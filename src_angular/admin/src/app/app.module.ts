import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import { RouterModule, Routes, ActivatedRouteSnapshot} from '@angular/router';
import { LocalizeRouterModule } from 'localize-router';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { CommonUiModule } from '@bluebamboo/common-ui';
import {SharedModule} from "./shared/shared.module";

import { HelpComponent } from 'app/common/help/help.component';
import { HelpService } from 'app/common/help/help.service';
import { AdminCommonModule } from './common';
import { UserService } from 'app/user.service';
import { AppComponent } from './app.component';
import { DesktopPageComponent } from './desktop/desktop-page/desktop-page.component';

import { authenticationRoutes } from './login-page/login-page.routing';

import { loginPageDeclarations, loginPageImports, loginPageProviders } from './login-page/';

import { PAdminModule, padminRoutes } from './padmin/';
import { PSupportModule, psupportRoutes } from './psupport/';

import { ConfigurationModule, configurationRoutes } from './configuration/';
import { InvoiceModule, invoiceRoutes } from './invoice/';
import { FaqModule, faqRoutes } from './faq/';
import { httpFactory } from './dev/';
import { CanActivateSecurity } from './can-activate-security';
import { UserPageComponent } from './user-page/user-page.component';
import { BookingModule, bookingRoutes } from './booking/';

import { MyAccountModule, myaccountRoutes } from './myaccount/';
import { ExpensesModule, expensesRoutes } from './expenses/';
import { PurchaseInvoicesModule, purchaseinvoicesRoutes } from './purchaseinvoices/';
import { AccountingModule } from './accounting/accounting.module';
import { accountingRoutes } from './accounting/accounting.routing';
import { PortalModule, portalRoutes } from './portal/';

import { Cms2Module } from './cms2/cms2.module';
import { cms2Routes } from './cms2/cms2.routing';

import { locale as en } from './resources/en';
import { locale as de } from './resources/de';
import { DesktopModule, desktopRoutes } from './desktop/';
import { DesktopModuleComponent } from './desktop/desktop-page/desktop-module/desktop-module.component';
import { DesktopService } from './desktop/desktop.service';
import { TrialPeriodResolver, MessageUnreadListResolver } from './desktop/desktop-page/resolvers';

import { FrontModule } from './front/front.module';
import { frontRoutes } from './front/front-routing';


declare var WOW: any;

export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, '/assets/locales/', '.json');
}
/**
 * HTTP interceptors section.
 * Based On:: InterceptorService from lib  'ng2-interceptors'
 * 1. SliderInterceptor :: constructor() :: responsible for displaying/hiding slider on evry XHR request
 * 2. AuthCheckInterceptor :: constructor() :: responsible for redirecting user if response.status == 403
 */
import { InterceptorService } from 'ng2-interceptors';
import { SliderInterceptor } from './framework/http/interceptors/slider.interceptor';
import { ServerSessionInterceptor } from './framework/http/interceptors/server-session.interceptor';
import { PreLoaderService } from './common/preloader/preloader.service';
import { ReflectiveInjector } from '@angular/core';
import { interceptorFactory} from './framework/http/factory/interceptor-factory';


import { OpenHoursModule } from './open-hours/open-hours.module';
import { openHoursRoutes } from './open-hours/open-hours.routing'



// ---------------------------------------------------------------------------------------
// app routes

let appRoutes = [];
appRoutes = appRoutes.concat(authenticationRoutes);
appRoutes = appRoutes.concat(frontRoutes);
appRoutes = appRoutes.concat(
  [
    {
      path: 'admin',
      component: UserPageComponent,
      canActivate: [CanActivateSecurity],
      children: [
        {
          path: '',
          pathMatch: 'full',
          component: DesktopPageComponent,
          resolve: {
            trialPeriod: TrialPeriodResolver,
            messages: MessageUnreadListResolver
          }
        }
      ]
    }
  ]
);

const rix = appRoutes.length - 1;
appRoutes[rix].children = invoiceRoutes.concat(appRoutes[rix].children);
appRoutes[rix].children = padminRoutes.concat(appRoutes[rix].children);
appRoutes[rix].children = psupportRoutes.concat(appRoutes[rix].children);
appRoutes[rix].children = configurationRoutes.concat(appRoutes[rix].children);
appRoutes[rix].children = faqRoutes.concat(appRoutes[rix].children);
appRoutes[rix].children = bookingRoutes.concat(appRoutes[rix].children);
appRoutes[rix].children = openHoursRoutes.concat(appRoutes[rix].children);
appRoutes[rix].children = myaccountRoutes.concat(appRoutes[rix].children);
appRoutes[rix].children = portalRoutes.concat(appRoutes[rix].children);
appRoutes[rix].children = desktopRoutes.concat(appRoutes[rix].children);
appRoutes[rix].children = expensesRoutes.concat(appRoutes[rix].children);
appRoutes[rix].children = purchaseinvoicesRoutes.concat(appRoutes[rix].children);
appRoutes[rix].children = accountingRoutes.concat(appRoutes[rix].children);
appRoutes[rix].children = cms2Routes.concat(appRoutes[rix].children);


// ---------------------------------------------------------------------------------------
// declarations

let declarations: any[] = [
  AppComponent,
  HelpComponent,
  DesktopPageComponent,
  DesktopModuleComponent,
  UserPageComponent,
];

declarations = declarations.concat(loginPageDeclarations);

// ---------------------------------------------------------------------------------------
// imports

let imports: any[] = [
  PAdminModule,
  PSupportModule,
  BrowserModule,
  FormsModule,
  BrowserAnimationsModule,
  CommonUiModule,
  HttpModule,
  InvoiceModule,
  ConfigurationModule,
  FaqModule,
  AdminCommonModule,
  BookingModule,
  MyAccountModule,
  ExpensesModule,
  PurchaseInvoicesModule,
  AccountingModule,
  PortalModule,

  Cms2Module,

  DesktopModule,
  OpenHoursModule,
  FrontModule,
  FroalaEditorModule.forRoot(),
  FroalaViewModule.forRoot(),
  SharedModule,
  TranslateModule.forRoot({
    loader: {
      provide: TranslateLoader,
      useFactory: HttpLoaderFactory,
      deps: [ Http ]
    }
  }),
  RouterModule.forRoot(appRoutes),
  LocalizeRouterModule.forRoot(appRoutes)
];

imports = imports.concat(loginPageImports);

// ---------------------------------------------------------------------------------------
// providers

let providers: any[] = [
    UserService,
    CanActivateSecurity,
    HelpService,
    PreLoaderService,
    SliderInterceptor,
    ServerSessionInterceptor,
    DesktopService,
    MessageUnreadListResolver,
    TrialPeriodResolver,
    {
      provide: Http,
      useFactory: httpFactory,
      deps: [XHRBackend, RequestOptions]
    },
    {
      provide: Http,
      useFactory: interceptorFactory,
      deps: [XHRBackend, RequestOptions, SliderInterceptor, ServerSessionInterceptor]
    },
  ];

providers = providers.concat(loginPageProviders);

// ---------------------------------------------------------------------------------------
// module declaration

@NgModule({
  declarations: declarations,
  imports: imports,
  providers: providers,
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
    setTimeout(() => { new WOW().init(); })
  }
}

// ---------------------------------------------------------------------------------------
