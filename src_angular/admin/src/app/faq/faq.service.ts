import { Injectable } from '@angular/core';
import { FaqModule } from './types/'
import {Observable} from "rxjs/Observable";

@Injectable()
export class FaqService {
  private _modules: FaqModule[] = [
    new FaqModule().parse({
      code: 'organization',
      icon: 'fa-vcard-o',
      implement: false})
  ];

  constructor() { }

  public getListModules(): Observable<FaqModule[]> {
    return (new Observable<FaqModule[]>(observable => observable.next(this._modules)));
  }
}
