export class FaqModule {
  public code: string;
  public icon: string;

  public parse( data: any ): FaqModule {
    this.code = data.code;
    this.icon = data.icon;
    return this;
  }
}
