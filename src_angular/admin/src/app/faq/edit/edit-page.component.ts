import {AfterViewInit, Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {UiGridComponent} from "../../common/grid/ui-grid.component";

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditPageComponent implements OnInit, AfterViewInit {


  @ViewChild(UiGridComponent)
  public UiGridComponent: UiGridComponent;
  public nickNamesForRows: Array<{
    colName: string,
    displayName: string,
    sortable: boolean
  }>;

  constructor() {
  }

  ngOnInit() {
    console.log('init');

    this.nickNamesForRows = [
      { colName: '', displayName: 'Actions', sortable: false },
      { colName: 'id', displayName: 'ID', sortable: true },
      { colName: 'import', displayName: 'Import', sortable: true },
      { colName: 'imported', displayName: 'Imported', sortable: true },
      { colName: 'col01', displayName: 'col01', sortable: true },
      { colName: 'col02', displayName: 'col02', sortable: true },
      { colName: 'col03', displayName: 'col03', sortable: true },
      { colName: 'col04', displayName: 'col04', sortable: true },
      { colName: 'col05', displayName: 'col05', sortable: true },
      { colName: 'col06', displayName: 'col06', sortable: true },
      { colName: 'col07', displayName: 'col07', sortable: true },
    ];
  }

  ngAfterViewInit(): void {
    this.UiGridComponent.LoadData([
      {'id': '1', 'import': 1, 'imported': 1, 'col01': 'some text', 'col02': 'some', 'col03': 'some', 'col04': 'some', 'col05': 'some', 'col06': 'some', 'col07': 'some'},
      {'id': '1', 'import': 1, 'imported': 1, 'col01': 'some text', 'col02': 'some', 'col03': 'some', 'col04': 'some', 'col05': 'some', 'col06': 'some', 'col07': 'some'},
      {'id': '1', 'import': 1, 'imported': 1, 'col01': 'some text', 'col02': 'some', 'col03': 'some', 'col04': 'some', 'col05': 'some', 'col06': 'some', 'col07': 'some'},
      {'id': '1', 'import': 1, 'imported': 1, 'col01': 'some text', 'col02': 'some', 'col03': 'some', 'col04': 'some', 'col05': 'some', 'col06': 'some', 'col07': 'some'},
      {'id': '1', 'import': 1, 'imported': 1, 'col01': 'some text', 'col02': 'some', 'col03': 'some', 'col04': 'some', 'col05': 'some', 'col06': 'some', 'col07': 'some'},
      {'id': '1', 'import': 1, 'imported': 1, 'col01': 'some text', 'col02': 'some', 'col03': 'some', 'col04': 'some', 'col05': 'some', 'col06': 'some', 'col07': 'some'},
      {'id': '1', 'import': 1, 'imported': 1, 'col01': 'some text', 'col02': 'some', 'col03': 'some', 'col04': 'some', 'col05': 'some', 'col06': 'some', 'col07': 'some'},
      {'id': '1', 'import': 1, 'imported': 1, 'col01': 'some text', 'col02': 'some', 'col03': 'some', 'col04': 'some', 'col05': 'some', 'col06': 'some', 'col07': 'some'},
      {'id': '1', 'import': 1, 'imported': 1, 'col01': 'some text', 'col02': 'some', 'col03': 'some', 'col04': 'some', 'col05': 'some', 'col06': 'some', 'col07': 'some'},
      {'id': '1', 'import': 1, 'imported': 1, 'col01': 'some text', 'col02': 'some', 'col03': 'some', 'col04': 'some', 'col05': 'some', 'col06': 'some', 'col07': 'some'},
      {'id': '1', 'import': 1, 'imported': 1, 'col01': 'some text', 'col02': 'some', 'col03': 'some', 'col04': 'some', 'col05': 'some', 'col06': 'some', 'col07': 'some'}
    ]);
  }

  editAction(data: any) {
    console.log(data);
  }
}
