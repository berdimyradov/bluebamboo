import {Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {FaqModule} from '../../types';

@Component({
  selector: 'admin-faq-module',
  templateUrl: './faq-module.component.html',
  styleUrls: ['./faq-module.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FaqModuleComponent implements OnInit {
  @HostBinding('class.col-xs-6')private classXs6 = true;
  @HostBinding('class.col-md-3')private classMd3 = true;
  @Input() public module: FaqModule;
  @Output() public action: EventEmitter<FaqModule> = new EventEmitter<FaqModule>();

  constructor() { }

  ngOnInit() {
  }

  public click() {
    this.action.emit(this.module);
  }
}
