import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaqModuleComponent } from './faq-module.component';

describe('FaqModuleComponent', () => {
  let component: FaqModuleComponent;
  let fixture: ComponentFixture<FaqModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaqModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaqModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
