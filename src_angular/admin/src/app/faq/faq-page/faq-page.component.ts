import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {routeAnimationOpacity, CommonPageComponent} from "../../common/";
import {ActivatedRoute, Router} from "@angular/router";
import {FaqService} from "../faq.service";
import {FaqModule} from "../types/";

@Component({
  selector: 'admin-faq-page',
  templateUrl: './faq-page.component.html',
  styleUrls: ['./faq-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@routeAnimationOpacity]': 'true'
  },
  animations: [
    routeAnimationOpacity
  ]
})
export class FaqPageComponent implements OnInit {
  public modules: FaqModule[] = [];

  constructor(
    private faqService: FaqService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private commonPage: CommonPageComponent
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.FAQ.TITLE';
    this.faqService.getListModules()
      .subscribe(
        result => this.modules = result
      );
  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public executeAction(module: FaqModule) {
    this.router.navigate(['./'+module.code], {relativeTo: this.activeRoute});
  }
}
