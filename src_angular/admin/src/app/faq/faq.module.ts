import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { AdminCommonModule } from '../common';
import { TranslateModule } from '@ngx-translate/core';
import { CommonUiModule } from '@bluebamboo/common-ui';
import { FaqPageComponent } from './faq-page/faq-page.component';
import { FaqModuleComponent } from './faq-page/faq-module/faq-module.component';
import {FaqService} from "./faq.service";
import {EditPageComponent} from "./edit/edit-page.component";

@NgModule({
  imports: [
    BrowserModule, BrowserAnimationsModule, FormsModule, CommonModule, TranslateModule, CommonUiModule, AdminCommonModule
  ],
  declarations: [FaqPageComponent, FaqModuleComponent, EditPageComponent],
  providers: [FaqService]
})
export class FaqModule { }
