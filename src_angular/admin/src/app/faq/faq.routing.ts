import {Routes} from "@angular/router";
import {FaqPageComponent} from "./faq-page/faq-page.component";
import {EditPageComponent} from "./edit/edit-page.component";

export const faqRoutes : Routes = [
  { path: 'faq', pathMatch: 'full', component: FaqPageComponent },
  { path: 'edit', pathMatch: 'full', component: EditPageComponent}
];
