import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { MyAccountService } from "../services";

@Injectable()
export class AdminPurchasedResolver implements Resolve<any> {
  constructor (private service: MyAccountService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.service.checkIfAdminIsPurchased();
  }

}
