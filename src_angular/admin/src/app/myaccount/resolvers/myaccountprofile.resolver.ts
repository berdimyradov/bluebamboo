import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Organization } from '../../common';
import { MyAccountService } from "../services";

@Injectable()
export class MyAccountProfileResolver implements Resolve<Organization> {
  constructor (private service : MyAccountService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Organization | Observable<Organization> | Promise<Organization> {
    return this.service.getOrg();
  }

}
