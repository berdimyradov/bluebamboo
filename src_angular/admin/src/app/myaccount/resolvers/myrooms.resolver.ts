import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Room } from '../../common';
import { MyAccountService } from "../services";

@Injectable()
export class MyRoomsResolver implements Resolve<Room> {
  constructor (private service : MyAccountService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Room | Observable<Room> | Promise<Room> {
    let id: string = route.paramMap.get('id');
    if (id === '-') {
      return Observable.of(new Room());
    }
    return this.service.getRoom(parseInt(id));
  }

}
