export const locale = {
  "COMPONENT": {
    "MY_ACCOUNT": {
      "COMMON": {
        "ORDER": "Verbindlich bestellen",
        "LABEL_AGB": "AGB <a target='_blank' href='https://bluebamboo.blob.core.windows.net/public/AGB.pdf'>(lesen)</a>",
        "LABEL_LICENCE": "Nutzungsbestimmungen  <a target='_blank' href='https://bluebamboo.blob.core.windows.net/public/Nutzungsbedingungen.pdf'>(lesen)</a>",
        "READ_AGB": "den Allgemeinen Geschäftsbedingungen zustimmen",
        "READ_LICENCE": "den Nutzungsbestimmungen zustimmen",
        "PAYMENT_SCHEME": "Zahlungsweise",
        "PAYMENT_SCHEME_HF": "Halbjährlich (105.- CHF)",
        "PAYMENT_SCHEME_FF": "Jährlich (199.- CHF)"
      }
    }
  },
  "PAGE": {
    "MPRIME": {
      'PURCHASE-ADMIN': {
        'HEADER': 'Modul "Administration" noch nicht gebucht!',
        'TEXT': 'Bitte buchen Sie zunächst das Administrations-Modul, da dieses als Basis für das mPrime Modul benötigt wird.',
        'BUTTON': 'Modul "Administration" buchen'
      },
      "PURCHASED": {
        "THANKS-MESSAGE": {
          "TITLE": "Danke für Ihre Bestellung bei bluebamboo!",
          "TEXT": "Sie bekommen die Rechnung Ende Dezember zugeschickt. Bei Fragen stehen wir Ihnen gerne zur Verfügung. Verwenden Sie bitte die Support- oder die Feedback-Funktion auf unserer Plattform.",
          "LINK": "Vertragsdokumente (SIX Payment Services) herunterladen"
        }
      }
    },
    "PURCHASED": {
      "THANKS-MESSAGE": {
        "TITLE": "Danke für Ihre Bestellung bei bluebamboo!",
        "TEXT": "Sie bekommen die Rechnung Ende Dezember zugeschickt. Bei Fragen stehen wir Ihnen gerne zur Verfügung. Verwenden Sie bitte die Support- oder die Feedback-Funktion auf unserer Plattform."
      },
      "INVITE-LIST": {
        "ONE_INVITE": "Ihnen wurde bluebamboo von einer bluebamboo Kundin oder einem bluebamboo Kunden empfohlen - diese Weiterempfehlung wurde gespeichert, so dass der/die EmpfehlerIn profitiert!",
        "MANY_INVITERS": "Wir sind Ihnen von mehreren Personen empfohlen worden? Bitte wählen Sie den Kontakt, aufgrund dessen Empfehlung Sie sich für uns entschieden haben.",
        "ACCEPT_INVITATION": "Weiterempfehlung annehmen",
      },
      "INVITE-OTHERS": {
        "QUESTION": "Möchten Sie wissen, worum es sich dabei handelt? Klicken Sie <a target='_blank' href='https://www.bluebamboo.ch/de/weiterempfehlung'>hier</a>!<br>Möchten auch Sie uns helfen und gleichzeitig profitieren? Klicken Sie einfach auf Weiterempfehlen.",
        "GOTO_PAGE": "Weiterempfehlen"
      }
    },
    "PURCHASED-CC": {
      "THANKS-MESSAGE": {
        "TITLE": "Danke für Ihre Bestellung bei bluebamboo!",
        "TEXT": "Bitte geben Sie nachfolgend die Kreditkartenangaben im Fenster unseres Zahlungspartners SIX Payments ein."
      }
    },
    "MY_ACCOUNT": {
      "MODULE-PURCHASED": {
        "PAGE_TITLE": "Danke für Ihre Bestellung!"
      },
      "MODULE-PURCHASED-CC": {
        "PAGE_TITLE": "Danke für Ihre Bestellung!"
      },
      "MODULE-WEB": {
        "PAGE_TITLE": "Modul 'Webseite'",
        "PURCHASED": {
          "THANKS-MESSAGE": {
            "TITLE": "Danke für Ihre Bestellung bei bluebamboo!",
            "TEXT": "Sie bekommen die Rechnung mit den Konto- oder Kreditkarten-Informationen Ende Dezember zugeschickt. <br>Die Bestellung für den Workshop oder die Erstellung der Webseite haben wir direkt an unseren Premium Partner DirectCom weitergeleitet, welcher sich in den nächsten Tagen mit Ihnen für die weitere Planung in Verbindung setzen wird.<br>Bei Fragen stehen wir Ihnen gerne zur Verfügung. Verwenden Sie bitte die Support- oder die Feedback-Funktion auf unserer Plattform."
          }
        }
      },
      "MODULE-MPRIME": {
        "PAGE_TITLE": "mPRIME Bestellung",
        "PURCHASED": {
          "THANKS-MESSAGE": {
            "TITLE": "Danke für Ihre Bestellung bei bluebamboo!",
            "TEXT": "Sie bekommen die Rechnung mit den Konto- oder Kreditkarten-Informationen Ende Dezember zugeschickt. Bei Fragen stehen wir Ihnen gerne zur Verfügung. Verwenden Sie bitte die Support- oder die Feedback-Funktion auf unserer Plattform."
          }
        }
      },
      "DESKTOP": {
        "PAGE_TITLE": "Mein Konto",
        "ITEMS": {
          "password": {
            "TITLE": "Passwort ändern"
          },
          "modules": {
            "TITLE": "Meine Module"
          },
          "invoices": {
            "TITLE": "Meine Rechnungen und Gutschriften"
          }
        }
      },
      "PASSWORD": {
        "TITLE": "Ändern Sie hier Ihr Passwort"
      },
      "MODULES": {
        "PAGE_TITLE": "Ihre Module"

      },
      "MODULE-ADMIN": {
        "PAGE_TITLE": "Modul 'Administration'"

      }
    }
  }
}
