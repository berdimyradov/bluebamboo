export const locale = {
    "PAGE": {
        "MyAccount": {
            "ITEMS": {
                "myprofile": {
                    "TITLE": "Mein Profil"
                },
                "myrooms": {
                    "TITLE": "Meine R\u00e4ume"
                },
                "mypassword": {
                    "TITLE": ""
                },
                "mymodules": {
                    "TITLE": ""
                },
                "myinvoices": {
                    "TITLE": ""
                }
            }
        },
        "MyRooms": {
            "TITLE": "Meine R\u00e4ume"
        },
        "MyRooms_EDIT": {
            "TITLE": "Meine R\u00e4ume"
        }
    }
}