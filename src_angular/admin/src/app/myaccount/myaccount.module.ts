import { AdminPurchasedResolver } from './resolvers/admin-purchased.resolver';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CommonUiModule } from '@bluebamboo/common-ui';
import { HttpClientModule } from '@angular/common/http'
import { AdminCommonModule } from '../common';

import { locale as en } from './resources/en';
import { locale as de } from './resources/de';

import { MyAccountService } from './services/myaccount.service';

import { MyAccountComponent } from './myaccount/myaccount.component';
import { MyProfileComponent } from './myaccount/myprofile/myprofile.component';
import { MyAccountProfileResolver } from './resolvers/myaccountprofile.resolver';
import { MyRoomsComponent } from './myaccount/myrooms/myrooms.component';
import { MyRoomsResolver } from './resolvers/myrooms.resolver';
import { MyRoomsEditComponent } from './myaccount/myrooms/myrooms_edit.component';
import { PasswordComponent } from './myaccount/password/password.component';
import { ModulesComponent } from './myaccount/modules/modules.component';
import { ModuleAdminComponent } from './myaccount/modules/module-admin/module-admin.component';
import { ModulePurchasedComponent } from './myaccount/modules/module-purchased/module-purchased.component';
import { ModulePurchasedCCComponent } from './myaccount/modules/module-purchased-cc/module-purchased-cc.component';
import { ModuleWebComponent } from './myaccount/modules/module-web/module-web.component';
import { MyInvoicesComponent } from './myaccount/myinvoices/myinvoices.component';
import { InvitationService } from './services/invitation-service';
import { ModuleMprimeComponent } from 'app/myaccount/myaccount/modules/module-mprime/module-mprime-component';
import { PurchaseModulesService } from 'app/myaccount/services/purchase-modules.service';
import { MprimeMessageComponent } from 'app/myaccount/myaccount/modules/module-mprime/message-page/mprime-message.component';
import { ModuleWebPurchasedComponent } from 'app/myaccount/myaccount/modules/module-web/module-purchased/module-web-purchased.component';

//import { SafeUrlPipe } from 'app/common/pipes';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    CommonUiModule,
    AdminCommonModule,
    FormsModule,
    BrowserAnimationsModule,
    TranslateModule,
    RouterModule
  ],
  declarations: [
    MyAccountComponent,
    MyProfileComponent,
    MyRoomsComponent,
    MyRoomsEditComponent,
    PasswordComponent,
    ModulesComponent,
    ModulePurchasedComponent,
    ModulePurchasedCCComponent,
    ModuleAdminComponent,
    ModuleMprimeComponent,
    MprimeMessageComponent,
    ModuleWebComponent,
    ModuleWebPurchasedComponent,
    MyInvoicesComponent
  ],
  providers: [
    MyAccountService,
    MyAccountProfileResolver,
    MyRoomsResolver,
    InvitationService,
    PurchaseModulesService,
    AdminPurchasedResolver
  ]
})

export class MyAccountModule {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
  }
}
