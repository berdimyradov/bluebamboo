import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PurchaseModulesService {

  constructor(http: HttpClient) {

  }

  public getDocumentsListForMprime() {
    return Observable.of([
        {id: 1, docName: 'Form F18', link: 'http://url.com'},
        {id: 2, docName: 'Billing account', link: 'http://url.com'},
        {id: 3, docName: 'Some new Doc', link: 'http://url.com'},
        {id: 4, docName: 'PDF form', link: 'http://url.com'},
        {id: 5, docName: 'Account info template', link: 'http://url.com'},
      ])
  }


}
