import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class InvitationService {

  constructor(http: HttpClient) {

  }

  public getInviteList(userId: number) {
    return Observable.of({
      inviters: [
        {id: 1, email: 'mike-dreihorst@meik.de'},
        {id: 2, email: 'michael-amman@mycoole.email.com'},
        {id: 3, email: 'roman-grob@microsoft.com'},
      ]
    })
  }

  public acceptInvite(inviterId: number) {
    alert(`invite from ${inviterId} is accepted!`);
  }

}
