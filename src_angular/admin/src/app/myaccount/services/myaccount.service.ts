import { Injectable, EventEmitter, Output } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { FrameworkService, GenericResultModel } from 'app/framework/';

import {
  Organization,
  Room,
  Location,
  SysModule,
  SaferpayTrxInitRequest,
  SaferpayTrxInitResponse,
  SaferpayTrxInitResponse2
} from 'app/common';

@Injectable()

export class MyAccountService extends FrameworkService {

  private saferPayUrl = '';
  public invitersList;

  public setSaferPayUrl(url: string) {
    this.saferPayUrl = url;
  }

  public getSaferPayUrl(): string {
    return this.saferPayUrl;
  }

  constructor(http: Http) {
    super(http);
  }

  // -------------------------------------------------------------------------------------
  // init saferpay

  public initSaferPay2(): Observable<SaferpayTrxInitResponse2> {

    let data = {};

    return this.http.post('/api/v1/platform/payments/', data)
      .map((res: Response) => new SaferpayTrxInitResponse2().parse(res.json()))
      .catch(this.handleError);
  }

  public initSaferPay(): Observable<SaferpayTrxInitResponse> {

    let sp_key = "API_242713_89202864";
    let sp_pass = "JsonApiPwd1_RrjPPn8t";
    let sp_host = "https://test.saferpay.com/api/";
    let sp_url = sp_host + "Payment/v1/Transaction/Initialize";

    let req = new SaferpayTrxInitRequest();

    let headers = new Headers();
    let authInfo = sp_key + ":" + sp_pass;
    var auth = 'Basic ' + window.btoa(authInfo);
    console.log("auth: " + auth);
    headers.append("Authorization", auth);
    headers.append("Accept", "application/json");
    headers.append("Content-Type", "application/json; charset=utf-8");

    return this.http.post(sp_url, req, { headers: headers })
      .map((res: Response) => new SaferpayTrxInitResponse().parse(res.json()))
      .catch(this.handleError);
  }

  // -------------------------------------------------------------------------------------
  // get organization data

  public getOrg(): Observable<Organization> {
    return this.http.get('/api/v1/org/organization/')
      .map((res: Response) => new Organization().parse(res.json()[0]))
      .catch(this.handleError);
  }

  // -------------------------------------------------------------------------------------
  // save organization data

  public saveOrg(obj: Organization): Observable<Organization> {
    return this.http.put('/api/v1/org/organization/' + obj.id, obj)
      .map((res: Response) => new Organization().parse(res.json()))
      .catch(this.handleError);
  }

  // -------------------------------------------------------------------------------------
  // get modules

  public getModules(): Observable<SysModule[]> {
    return this.http.get(`/api/v1/platform/modules`)
      .map((res: Response) => res.json().map(item => new SysModule().parse(item)))
      .catch(this.handleError);
  }

  public checkIfAdminIsPurchased() {
    return this.http.get(`/api/v1/platform/modules/admin_ordered`)
    .map((res: Response) => res.json())
    .catch(this.handleError);
  }

  // -------------------------------------------------------------------------------------
  // order modules

  public orderModule(order: any): Observable<any> {
    return this.http.post(`/api/v1/platform/modules/order`, order)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  public acceptInvite(id) {
    return this.http.put(`/api/v1/platform/recommendations/${id}/accept`, {})
      .map(resp => resp.json())
      .catch(this.handleError);
  }

  // -------------------------------------------------------------------------------------
  // rooms

  public getRooms(fields = ['id', 'title', 'description']): Observable<Room[]> {
    let requestOptions: RequestOptions = new RequestOptions();
    requestOptions.params = new URLSearchParams();
    requestOptions.params.set('_fields', fields.join(','));
    return this.http.get('/api/v1/loc/rooms', requestOptions)
      .map((res: Response) => res.json().map((it: any) => new Room().parse(it)))
      .catch(this.handleError);
  }

  public getRoom(id: number): Observable<Room> {
    return this.http.get('/api/v1/loc/rooms/' + id)
      .map((res: Response) => new Room().parse(res.json()))
      .catch(this.handleError);
  }

  public saveRoom(obj: Room): Observable<Room> {
    //(room.products || []).forEach((p: any) => {p.room_id = room.id;});
    const res: Observable<Response> = obj.id
      ? this.http.put('/api/v1/loc/rooms/' + obj.id, obj)
      : this.http.post('/api/v1/loc/rooms/', obj);
    return res.map((res: Response) => new Room().parse(res.json()))
      .catch(this.handleError);
  }

  public deleteRoom(obj: Room): Observable<void> {
    return this.http.delete('/api/v1/loc/rooms/' + obj.id)
      .map(() => { return; })
      .catch(this.handleError);
  }

}
