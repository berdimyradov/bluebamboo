import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonListItem } from "../../../common";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { GenericComponent } from "app/framework";
import { UserService } from "../../../user.service";
import { PwdResetIModel } from "../../../types/pwdireset";

import { HelpService } from 'app/common/help/help.service';

@Component({
	selector:		    'app-password',
	templateUrl:	  './password.component.html',
	styleUrls:		  ['./password.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			      {'[@routeAnimationOpacity]': 'true'},
	animations:		  [routeAnimationOpacity]
})

export class PasswordComponent extends GenericComponent {
  public me = this;
  public passwordRepeat: string = '';
  public model = new PwdResetIModel();
  public message = '';
  public processing: boolean = false;

	constructor(
    	private commonPage: CommonPageComponent,
    	private router: Router,
    	private activeRoute: ActivatedRoute,
    	private service: UserService,
    	protected help: HelpService
    )
	{
	  super(help);
	}

	ngOnInit() {
	  super.ngOnInit();
		this.commonPage.title = 'Mein Passwort';
	}

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public save() {
    if (this.passwordRepeat != this.model.passwordNew) {
      this.message = "Neues Passwort und Neues Passwort (Wiederholung) stimmen nicht überein.";
      return;
    }
    this.processing = true;
    this.service.resetIPassword(this.model)
      .subscribe(
      result => {
          this.processing = false;
        if (result.isSuccess) {
          this.message = "Ihr Passwort wurde erfolgreich geändert.";
        } else {
          let m = JSON.parse(result.error);
          this.message = m.message;
        }
      }
    );
  }

}
