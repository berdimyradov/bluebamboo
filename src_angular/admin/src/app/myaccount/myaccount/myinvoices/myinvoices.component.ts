import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonListItem } from "../../../common";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { MyAccountService } from "../../services/myaccount.service";

@Component({
	selector:		'app-myinvoices',
	templateUrl:	'./myinvoices.component.html',
	styleUrls:		['./myinvoices.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			{'[@routeAnimationOpacity]': 'true'},
	animations:		[routeAnimationOpacity]
})

export class MyInvoicesComponent implements OnInit {

	constructor(
    	private commonPage: CommonPageComponent,
    	private router: Router,
    	private activeRoute: ActivatedRoute,
    	private service: MyAccountService
    )
	{}

	ngOnInit() {
		this.commonPage.title = 'PAGE.INVOICE.TITLE';
	}

}
