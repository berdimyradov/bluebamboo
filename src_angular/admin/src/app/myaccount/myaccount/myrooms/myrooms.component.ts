import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonListItem } from "../../../common";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { MyAccountService } from "../../services/myaccount.service";

@Component({
	selector:		'app-myrooms',
	templateUrl:	'./myrooms.component.html',
	styleUrls:		['./myrooms.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			{'[@routeAnimationOpacity]': 'true'},
	animations:		[routeAnimationOpacity]
})

export class MyRoomsComponent implements OnInit {

	constructor(
    	private commonPage: CommonPageComponent,
    	private router: Router,
    	private activeRoute: ActivatedRoute,
    	private service: MyAccountService
    )
	{}

	ngOnInit() {
		this.commonPage.title = 'PAGE.MyRooms.TITLE';
	}

	public getList = ():Observable<CommonListItem[]> => {
		return new Observable<CommonListItem[]>(observer => {
			this.service.getRooms().subscribe(
				result => {
					observer.next(result);
					observer.complete();
				}
			);
		});
	}

	public edit(obj: CommonListItem) {
		this.router.navigate(['./' + obj.id], { relativeTo: this.activeRoute });
	}

	public add() {
		this.router.navigate(['./-'], { relativeTo: this.activeRoute });
	}
}
