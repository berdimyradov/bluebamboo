import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { routeAnimationOpacity, CommonPageComponent, DialogService } from "../../../common";
import { SelectOption } from '@bluebamboo/common-ui';

import { Room} from "../../../common";
import { MyAccountService } from "../../services/myaccount.service";

@Component({
	selector:		'app-myrooms',
	templateUrl:	'./myrooms.component.html',
	styleUrls:		['./myrooms.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			{'[@routeAnimationOpacity]': 'true'},
	animations:		[routeAnimationOpacity]
})

export class MyRoomsEditComponent implements OnInit {
  public room: Room;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private service: MyAccountService,
    private dialog: DialogService
  ) { }

  ngOnInit() {
    this.room = this.activeRoute.snapshot.data.room;
    this.commonPage.title = 'PAGE.MyRooms_EDIT.TITLE';
  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public save() {
    this.service.saveRoom(this.room)
      .subscribe(
        result => this.back()
      );
  }

  public delete() {
//  this.dialog.confirm('PAGE.ROOM-FORM.CONFIRM-DELETE','PAGE.ROOM-FORM.CONFIRM-YES','PAGE.ROOM-FORM.CONFIRM-NO')
    this.dialog.confirm('PAGE.ROOM-FORM.CONFIRM-DELETE')
      .then((res: Boolean) => {
        if (res) {
          this.service.deleteRoom(this.room).subscribe(
            result => this.back()
          );
        }
      });
  }
}
