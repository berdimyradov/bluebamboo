import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonMenuItem } from "../../common";
import { MyAccountService } from "../services/myaccount.service";

@Component({
	selector:		'app-myaccount',
	templateUrl:	'./myaccount.component.html',
	styleUrls:		['./myaccount.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			{'[@routeAnimationOpacity]': 'true'},
	animations:		[routeAnimationOpacity]
})

export class MyAccountComponent implements OnInit {
//	public modules: CommonMenuItem[] = [];

	public modules: CommonMenuItem[] = [

			new CommonMenuItem().parse({
				code: 'password',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'modules',
				icon: 'fa-table',
				implement: true
			}),
/*
			new CommonMenuItem().parse({
				code: 'modules',
				icon: 'fa-cubes',
				implement: true
			}),
*/
/*
			new CommonMenuItem().parse({
				code: 'myrooms',
				icon: 'fa-university',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'mypassword',
				icon: 'fa-key',
				implement: true
			}),
			new CommonMenuItem().parse({
				code: 'myinvoices',
				icon: 'fa-files-o',
				implement: true
			}),
*/
	];

	constructor(
	  private service: MyAccountService,
	  private commonPage: CommonPageComponent
	)
	  {}

	ngOnInit() {
		this.commonPage.title = 'PAGE.MY_ACCOUNT.DESKTOP.PAGE_TITLE';
		/*
		this.service.getListModules().subscribe(
			result => this.modules = result
		);
		*/
	}
}
