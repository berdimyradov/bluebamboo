import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonListItem } from '../../../../common';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { SaferpayTrxInitResponse, SaferpayTrxInitResponse2 } from 'app/common';

import { MyAccountService } from '../../../services/myaccount.service';

@Component({
  selector: 'app-module-admin',
  templateUrl: './module-admin.component.html',
  styleUrls: ['./module-admin.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: { '[@routeAnimationOpacity]': 'true' },
  animations: [routeAnimationOpacity]
})

export class ModuleAdminComponent implements OnInit {
  public agb: boolean = false;
  public licence: boolean = false;
  public _paymentScheme = 1;
  public paymentMethod = 'inv';
  public association = '';
  public prices = {
    1: 199.00,
    2: 105.00,
    3: 19.50
  };
  public _licenceVersion = '1.0.1';
  public _agbVersion = '1.0.1';

  get paymentScheme(): number {
    return this._paymentScheme;
  }
  set paymentScheme(val: number) {
    if (val == 3) this.paymentMethod = 'cc';
    this._paymentScheme = val;
  }

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private service: MyAccountService
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.MY_ACCOUNT.MODULE-ADMIN.PAGE_TITLE';
  }

  public btnOrderDisabled() {
    if (!this.agb) return true;
    if (!this.licence) return true;
    return false;
  }

  private goToCCPage(res: SaferpayTrxInitResponse2) {
    this.service.setSaferPayUrl(res.url);
    this.router.navigate(['../module-purchased-cc'], { relativeTo: this.activeRoute });
  }

  public executeOrder() {
    /*
    if (this.paymentMethod == "cc") {

      this.service.initSaferPay2().subscribe(
        (res: SaferpayTrxInitResponse2) => this.goToCCPage(res)
      );

    }
    else {
      this.router.navigate(['../module-purchased'], {relativeTo: this.activeRoute});
    }
    */
    this.service.orderModule(this.createJSON())
      .subscribe((data) => {
        if (data.id) {
          this.service.invitersList = data.inviters;
          this.router.navigate(['../module-purchased'], {relativeTo: this.activeRoute});
        }
      });
  }

  public executeBack() {
    this.router.navigate(['../modules'], { relativeTo: this.activeRoute });
  }

  public createJSON() {
    return {
      'agb_version_accepted': this._agbVersion,
      'licence_version_accepted': this._licenceVersion,
      'amount': this.prices[this.paymentScheme],
      'payment_method': this.paymentMethod,
      'payment_scheme': this.paymentScheme,
      'module_ids': [1],
      'comment': this.association
    };
  }

}
