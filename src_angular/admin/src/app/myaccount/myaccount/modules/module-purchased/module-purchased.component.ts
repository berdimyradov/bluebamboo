import { Component, OnInit, OnChanges, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonListItem } from "../../../../common";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { MyAccountService } from '../../../services/myaccount.service';


@Component({
  selector: 'app-module-purchased',
  templateUrl: './module-purchased.component.html',
  styleUrls: ['./module-purchased.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: { '[@routeAnimationOpacity]': 'true' },
  animations: [routeAnimationOpacity]
})

export class ModulePurchasedComponent implements OnInit, OnChanges {
  public invitesList = <any>[];

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private service: MyAccountService,
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.MY_ACCOUNT.MODULE-PURCHASED.PAGE_TITLE';
    this.invitesList = this.service.invitersList;
  }

  ngOnChanges() {

  }

  public redirectToInvitePage() {
    this.router.navigate(['../../invite'], { relativeTo: this.activeRoute });
  }

  public back() {
    this.router.navigate(['../modules'], { relativeTo: this.activeRoute });
  }

  public acceptInvite(id: number) {
    this.service.acceptInvite(id)
    .subscribe((data) => {
      this.invitesList = [];
      console.log(data)
    });
  }

}
