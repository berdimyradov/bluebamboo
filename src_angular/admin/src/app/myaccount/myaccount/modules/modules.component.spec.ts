import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyModulesComponent } from './mymodules/mymodules.component';

describe('MyModulesComponent', () => {
  let component: MyModulesComponent;
  let fixture: ComponentFixture<MyModulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyModulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyModulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
