import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleWebComponent } from './modules/module-web/module-web.component';

describe('ModuleWebComponent', () => {
  let component: ModuleWebComponent;
  let fixture: ComponentFixture<ModuleWebComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuleWebComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleWebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
