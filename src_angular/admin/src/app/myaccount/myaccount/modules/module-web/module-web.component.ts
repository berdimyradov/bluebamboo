import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonListItem } from "../../../../common";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { MyAccountService } from "../../../services/myaccount.service";

@Component({
	selector:		    'app-module-web',
	templateUrl:	  './module-web.component.html',
	styleUrls:		  ['./module-web.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			      {'[@routeAnimationOpacity]': 'true'},
	animations:		  [routeAnimationOpacity]
})

export class ModuleWebComponent implements OnInit {
  public agb: boolean = false;
  public licence: boolean = false;
  private _paymentScheme: number = 1;
  public offerSelection: number = 1;
  public paymentMethod = "inv";
  public association = '';
  public prices = {
    1: 199.00,
    2: 105.00
  };
  public pricesOffer = {
    1: 299.00,
    2: 490.00
  }
  public _licenceVersion = '1.0.1';
  public _agbVersion = '1.0.1';

  get paymentScheme(): number {
    return this._paymentScheme;
  }
  set paymentScheme(val: number) {
    if (val == 3) this.paymentMethod = 'cc';
    this._paymentScheme = val;
  }

	constructor(
    	private commonPage: CommonPageComponent,
    	private router: Router,
    	private activeRoute: ActivatedRoute,
    	private service: MyAccountService
    )
	{}

	ngOnInit() {
		this.commonPage.title = 'PAGE.MY_ACCOUNT.MODULE-WEB.PAGE_TITLE';
	}

  public btnOrderDisabled () {
    if (!this.agb) return true;
    if (!this.licence) return true;
    return false;
  }

  public executeOrder () {
    this.service.orderModule(this.createJSON(2))
    .subscribe((data) => {
      if (data.id) {
        this.service.orderModule(this.createJSON(3))
        .subscribe((data) => {
          if (data.id) {
            this.router.navigate(['./purchased'], {relativeTo: this.activeRoute});
          }
        });
      }
    });
    // this.router.navigate(['../../invite'], {relativeTo: this.activeRoute});
  }

  public executeBack() {
    this.router.navigate(['../modules'], {relativeTo: this.activeRoute});
  }

  public createJSON(mod_id:number) {
    let price: number;
    let scheme: number;
    if(mod_id === 2) {
      price = this.prices[this.paymentScheme];
      scheme = this.paymentScheme;
    }
    if(mod_id === 3) {
      price = this.pricesOffer[this.offerSelection];
      scheme = 4;
    }
    return {
      'agb_version_accepted': this._agbVersion,
      'licence_version_accepted': this._licenceVersion,
      'amount': price,
      'payment_method': this.paymentMethod,
      'payment_scheme': scheme,
      'module_ids': [mod_id],
      'comment': this.association
    };
  }

}
