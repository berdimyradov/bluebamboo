import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { routeAnimationOpacity, CommonPageComponent, CommonListItem } from '../../../../../common';

@Component({
  selector: 'app-module-web-purchased',
  templateUrl: 'module-web-purchased.component.html',
  styleUrls: ['module-web-purchased.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class ModuleWebPurchasedComponent implements OnInit {
  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute) { }

  ngOnInit() { }

  public back() {
    this.router.navigate(['../../modules'], { relativeTo: this.activeRoute });
  }

}
