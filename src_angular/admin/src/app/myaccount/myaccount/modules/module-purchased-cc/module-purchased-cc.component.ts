import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonListItem } from "app/common";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { MyAccountService } from "../../../services/myaccount.service";

@Component({
	selector:		    'app-module-purchased-cc',
	templateUrl:	  './module-purchased-cc.component.html',
	styleUrls:		  ['./module-purchased-cc.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			      {'[@routeAnimationOpacity]': 'true'},
	animations:		  [routeAnimationOpacity]
})

export class ModulePurchasedCCComponent implements OnInit {

  public url = "";

	constructor(
    	private commonPage: CommonPageComponent,
    	private router: Router,
    	private activeRoute: ActivatedRoute,
    	private service: MyAccountService
    )
	{}

	ngOnInit() {
		this.commonPage.title = 'PAGE.MY_ACCOUNT.MODULE-PURCHASED-CC.PAGE_TITLE';

		this.url = this.service.getSaferPayUrl();
		console.log("purchased cc --> set url to --> " + this.url);

	}

  public back() {
    this.router.navigate(['../modules'], {relativeTo: this.activeRoute});
  }

}
