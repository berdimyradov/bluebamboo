import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { routeAnimationOpacity, CommonPageComponent, CommonListItem } from '../../../../../common';

@Component({
  selector: 'app-mprime-message',
  templateUrl: 'mprime-message.component.html',
  styleUrls: ['mprime-message.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class MprimeMessageComponent implements OnInit {
  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute) { }

  ngOnInit() { }

  public back() {
    this.router.navigate(['../../modules'], { relativeTo: this.activeRoute });
  }

}
