import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonListItem } from "../../../../common";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { SaferpayTrxInitResponse, SaferpayTrxInitResponse2 } from 'app/common';

import { MyAccountService } from "../../../services/myaccount.service";
import { PurchaseModulesService } from 'app/myaccount/services/purchase-modules.service';

@Component({
  selector: 'app-module-mprime',
  templateUrl: './module-mprime-component.html',
  styleUrls: ['./module-mprime-component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: { '[@routeAnimationOpacity]': 'true' },
  animations: [routeAnimationOpacity]
})

export class ModuleMprimeComponent implements OnInit {
  public adminOrdered = false;
  public _paymentScheme = 3;
  public paymentMethod = "inv";
  public docList = <any>[];
  public prices = {
    3: 9.90,
    4: 0.00
  };

  get paymentScheme(): number {
    return this._paymentScheme;
  }
  set paymentScheme(val: number) {
    if (val == 3) this.paymentMethod = 'cc';
    this._paymentScheme = val;
  }

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private service: MyAccountService,
    private purchaseModulesService: PurchaseModulesService
  ) { }

  ngOnInit() {
    this.commonPage.title = 'PAGE.MY_ACCOUNT.MODULE-MPRIME.PAGE_TITLE';
    this.adminOrdered = this.activeRoute.snapshot.data['adminData'].ordered;
    this.purchaseModulesService.getDocumentsListForMprime()
      .subscribe((docs) => {
        this.docList = docs;
      })
  }

  public btnOrderDisabled() {
    return false;
  }

  public executeOrder() {
    this.service.orderModule(this.createJSON())
    .subscribe((data) => {
      if (data.id) {
        this.router.navigate(['./message'], { relativeTo: this.activeRoute });
      }
    });
  }

  public executeBack() {
    this.router.navigate(['../modules'], { relativeTo: this.activeRoute });
  }

  public redirectToAdmin() {
    this.router.navigate(['../../myaccount/module-admin'], { relativeTo: this.activeRoute });
  }


  public createJSON() {
    return {
      "agb_version_accepted": null,
      "licence_version_accepted": null,
      "amount": this.prices[this.paymentScheme],
      "payment_method": this.paymentMethod,
      "payment_scheme": this.paymentScheme,
      "module_ids": [5],
      "comment": null
    };
  }

}
