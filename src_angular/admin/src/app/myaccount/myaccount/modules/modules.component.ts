import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonListItem } from "../../../common";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { SysModule } from "app/common";
import { MyAccountService } from "../../services/myaccount.service";
import { BookingModule } from 'app/booking';

@Component({
	selector:		    'app-modules',
	templateUrl:	  './modules.component.html',
	styleUrls:		  ['./modules.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			      {'[@routeAnimationOpacity]': 'true'},
	animations:		  [routeAnimationOpacity]
})

export class ModulesComponent implements OnInit {

  public modules: SysModule[] = [];
  public visibleModules: SysModule[] = [];
  public availableModules = [
    'ADMIN',
    'PORTAL',
    'MPRIME'
  ];
  public adminOrdered = false;

	constructor(
    	private commonPage: CommonPageComponent,
    	private router: Router,
    	private activeRoute: ActivatedRoute,
      private service: MyAccountService

    )
	{}

	ngOnInit() {
    this.commonPage.title = 'PAGE.MY_ACCOUNT.MODULES.PAGE_TITLE';
    this.adminOrdered = this.activeRoute.snapshot.data['adminData'].ordered;
		this.service.getModules().subscribe(
		  (res) => {
        this.modules = res;
        this.visibleModules = this.createVisibleModules(res);
      }
		);
  }

  private createVisibleModules (source: SysModule[]): SysModule[] {
    let modules: SysModule[] = [];
    source.forEach(function(mod:SysModule) {
      if(mod.visible) modules.push(mod);
    });
    return modules;
  }

  public canBook (mod: SysModule): boolean {
    const index = this.availableModules.indexOf(mod.code);
    let active = mod.booking.order_active || false;
    return (index > -1) && !active;
  }

  public bookModule (code: string) {
    if (code == "ADMIN") this.bookAdmin();
    if (code == "PORTAL") this.bookWeb();
    if (code == "MPRIME") this.bookMprime();
  }

  public bookAdmin () {
    this.router.navigate(['../module-admin'], {relativeTo: this.activeRoute});
  }

  public bookMprime () {
    this.router.navigate(['../module-mprime'], {relativeTo: this.activeRoute});
  }

  public bookWeb () {
    this.router.navigate(['../module-web'], {relativeTo: this.activeRoute});
  }

  public invite () {
    this.router.navigate(['../../invite'], {relativeTo: this.activeRoute});
  }

  public support () {
    this.router.navigate(['../../support'], {relativeTo: this.activeRoute});
  }

  public executeBack() {
    this.router.navigate(['../../'], {relativeTo: this.activeRoute});
  }


  // call to actions for useful links section

  public showAGB () {
    window.open("https://bluebamboo.blob.core.windows.net/public/AGB.pdf", "_blank");
  }

  public showLicence () {
    window.open("https://bluebamboo.blob.core.windows.net/public/Nutzungsbedingungen.pdf", "_blank");
  }

  public showPdfManual () {
    window.open("https://bluebamboo.blob.core.windows.net/public/bluebamboo-die-anleitung.pdf", "_blank");
  }

  public showPdfPreisblatt () {
    window.open("https://bluebamboo.blob.core.windows.net/public/bluebamboo-preisblatt.pdf", "_blank");
  }

  public showVideosPage () {
    window.open("https://www.bluebamboo.ch/de/videos", "_blank");
  }

  public showSupportPage() {
      this.router.navigate(['../../support'], {relativeTo: this.activeRoute});
  }

  public showFeedbackPage() {
      this.router.navigate(['../../feedback'], {relativeTo: this.activeRoute});
  }

  public showRecommendPage() {
      this.router.navigate(['../../invite'], {relativeTo: this.activeRoute});
  }

}
