import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routeAnimationOpacity, CommonPageComponent, CommonMenuItem } from "../../../common";
import { ActivatedRoute, Router } from "@angular/router";

import { Organization} from "../../../common";
import { MyAccountService } from "../../services/myaccount.service";

@Component({
	selector:		'app-myprofile',
	templateUrl:	'./myprofile.component.html',
	styleUrls:		['./myprofile.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	host:			{'[@routeAnimationOpacity]': 'true'},
	animations:		[routeAnimationOpacity]
})

export class MyProfileComponent implements OnInit {
  public organization: Organization;

  constructor(
    private commonPage: CommonPageComponent,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private service: MyAccountService,
  ) { }

  ngOnInit() {
    this.organization = this.activeRoute.snapshot.data.organization;
    this.commonPage.title = 'Mein Profil';
  }

  public back() {
    this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public save() {
    this.service.saveOrg(this.organization)
      .subscribe(
        result => this.back()
      );
  }
}
