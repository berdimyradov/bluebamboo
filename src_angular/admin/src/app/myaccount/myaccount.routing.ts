import { Routes } from "@angular/router";

import { MyAccountComponent } from './myaccount/myaccount.component';
import { MyAccountProfileResolver } from './resolvers/myaccountprofile.resolver';
import { MyProfileComponent } from './myaccount/myprofile/myprofile.component';
import { MyRoomsResolver } from './resolvers/myrooms.resolver';
import { MyRoomsEditComponent } from './myaccount/myrooms/myrooms_edit.component';
import { MyRoomsComponent } from './myaccount/myrooms/myrooms.component';
import { PasswordComponent } from './myaccount/password/password.component';
import { ModulesComponent } from './myaccount/modules/modules.component';
import { ModuleAdminComponent } from './myaccount/modules/module-admin/module-admin.component';
import { ModulePurchasedComponent } from './myaccount/modules/module-purchased/module-purchased.component';
import { ModulePurchasedCCComponent } from './myaccount/modules/module-purchased-cc/module-purchased-cc.component';
import { ModuleWebComponent } from './myaccount/modules/module-web/module-web.component';
import { MyInvoicesComponent } from './myaccount/myinvoices/myinvoices.component';
import { ModuleMprimeComponent } from "app/myaccount/myaccount/modules/module-mprime/module-mprime-component";
import { ModuleWebPurchasedComponent } from './myaccount/modules/module-web/module-purchased/module-web-purchased.component';
import { MprimeMessageComponent } from "app/myaccount/myaccount/modules/module-mprime/message-page/mprime-message.component";
import { AdminPurchasedResolver } from './resolvers/admin-purchased.resolver';

export const myaccountRoutes : Routes = [
	{
		path: "myaccount",
		component: MyAccountComponent,
	},
	{
		path: "myaccount/myprofile",
		resolve: {
			organization: MyAccountProfileResolver
		},
		component: MyProfileComponent,
	},
	{
		path: "myaccount/myrooms",
		pathMatch: 'full',
		component: MyRoomsComponent,
	},
	{
		path: 'myaccount/myrooms/:id',
		pathMatch: 'full',
		component: MyRoomsEditComponent,
		resolve: {
			room: MyRoomsResolver
		}
	},
	{
		path: "myaccount/password",
		component: PasswordComponent
	},
	{
		path: "myaccount/modules",
		component: ModulesComponent,
		resolve: {
		  adminData: AdminPurchasedResolver
		}
	},
	{
		path: "myaccount/module-admin",
		component: ModuleAdminComponent
  },
  {
    path: 'myaccount/module-mprime',
    component: ModuleMprimeComponent,
    resolve: {
      adminData: AdminPurchasedResolver
    }
  },
  {
    path: 'myaccount/module-mprime/message',
    component: MprimeMessageComponent
  },
	{
		path: "myaccount/module-purchased",
		component: ModulePurchasedComponent
	},
	{
		path: "myaccount/module-purchased-cc",
		component: ModulePurchasedCCComponent
	},
	{
		path: "myaccount/module-web",
		component: ModuleWebComponent
	},
	{
    path: 'myaccount/module-web/purchased',
    component: ModuleWebPurchasedComponent
  },
	{
		path: "myaccount/myinvoices",
		component: MyInvoicesComponent,
	},
];

