import { Component, OnInit } from '@angular/core';
import {UserService} from "../user.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {

  constructor(
    private userService: UserService,
    private router: Router,
    private activeRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
  }

  public userLogout() {
    this.userService.logout().subscribe(
      () => {
        window.location.href = '/de/admin/user_logout';
//        this.router.navigate(['./login'], {relativeTo: this.activeRoute});
      }
    );
  }
}
