import { Injectable } from '@angular/core';
import { LoginModel, LoginInfoModel, RegistrationModel, PwdResetModel, PwdResetIModel, PwdRequestModel, ConfirmRegistrationModel } from './types/';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { FrameworkService, GenericResultModel } from 'app/framework/';

@Injectable()
export class UserService extends FrameworkService {

  private version: string = "V2.5a";

  constructor (http: Http) { super(http); }

  // -------------------------------------------------------------------------------------
  // login

  public login(model: LoginModel): Observable<GenericResultModel> {
    return this.postRequest(model, '/api/v1/auth/login');
  }

  // -------------------------------------------------------------------------------------
  // logout

  public logout(): Observable<void> {
    return this.http.get('/api/v1/auth/logout')
      .map((response) => null)
      .catch(() => (new Observable<void>(observable => {
        observable.next(null);
        observable.complete();
      })));
  }

  // -------------------------------------------------------------------------------------
  // get info

  public getLoginInfo(): Observable<LoginInfoModel> {
    return this.http.get('/api/v1/auth/state')
      .map((response: Response) => new LoginInfoModel(response.json()))
      .catch(() => (new Observable<LoginInfoModel>(observable => observable.next(null))))
  }

  // -------------------------------------------------------------------------------------
  // register

  public register(model: RegistrationModel): Observable<GenericResultModel> {
    return this.postRequest(model, '/api/v1/auth/register');
  }

  // -------------------------------------------------------------------------------------
  // confirm registration

  public confirmRegistration(model: ConfirmRegistrationModel): Observable<GenericResultModel> {
    return this.postRequest(model, '/api/v1/auth/confirm_registration');
  }

  // -------------------------------------------------------------------------------------
  // request link to reset password

  public requestPassword(model: PwdRequestModel): Observable<GenericResultModel> {
    return this.postRequest(model, '/api/v1/auth/requestpassword');
  }

  // -------------------------------------------------------------------------------------
  // reset password

  public resetPassword(model: PwdResetModel): Observable<GenericResultModel> {
    return this.postRequest(model, '/api/v1/auth/resetpassword');
  }

  // -------------------------------------------------------------------------------------
  // reset i password

  public resetIPassword(model: PwdResetIModel): Observable<GenericResultModel> {
    return this.postRequest(model, '/api/v1/auth/resetipassword');
  }

  // -------------------------------------------------------------------------------------
  // get app info

  public getAppInfo() : Observable<string> {
    let info: string = this.version;
    return Observable.of(info);
  }

}
