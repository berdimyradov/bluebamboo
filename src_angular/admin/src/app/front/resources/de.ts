export const locale = {
  PAGE: {
    CANCELATION: {
      IS_CANCELLABLE: {
        HEADER: 'Buchung kann kostenfrei storniert werden!',
        HEADER_PAY: 'Buchung kann nur zahlungspflichtig storniert werden!',
        MESSAGE_PAY: 'Es ist leider zu spät für eine kostenlose Stornierung. Wenn Sie die Buchung stornieren möchten, wird Ihnen trotzdem ein Betrag von CHF {{price}} in Rechnung gestellt werden.',
        QUESTION: 'Möchten Sie die Buchung stornieren?',
        CANCEL_BUTTON: 'Stornieren'
      },
      IS_NOT_CANCELLABE: {
        HEADER: 'Die Buchung kann nicht (mehr) storniert werden!',
        MESSAGE_LATE: 'Es ist leider zu spät für eine Stornierung der Buchung.',
        MESSAGE_ALREADY: 'Die Buchung ist schon storniert worden.'
      },
      TITLE: 'Buchung stornieren',
      BOOKING_INFO: {
        HEADER: 'Buchung',
        TITLE: 'Leistung:',
        DATE: 'Datum:',
        TIME: 'Uhrzeit:',
        DURATION: 'Dauer:',
        PRICE: 'Preis:'
      },
      ERROR: {
        HEADER: 'Ein Fehler ist aufgetreten',
        MESSAGE: 'Beim Laden der Buchungsdaten ist leider ein Fehler aufgetreten. Bitte versuchen Sie es erneut oder wenden Sie sich direkt an den Leistungserbringer.'
      }
    },
    FRONT: {
      ALL: 'Alle',
      SHOW_OFFERS: 'Angebote zeigen',
      TERMS_OF_USE: {
        HEADER: 'Terms of use',
        AGREE: 'Ich stimme den Nutzungsbedingungen zu',
        BUTTON: 'Nutzungsbedingungen'
      },
      BOOKING_MODAL: {
        STEP_1: {
          HEADER_LOCATION: 'Standort wählen',
          HEADER_EMPLOYEE: 'Mitarbeiter wählen',
          ANY_EMPLOYEE: 'Mitarbeiter egal'
        },
        STEP_2: {
          HEADER: 'Bitte wählen Sie eine Zeit für die Buchung'
        },
        STEP_3: {
          FIRSTNAME: 'Vorname',
          NAME: 'Nachname',
          EMAIL: 'E-Mail',
          PHONE: 'Telefon',
          COMMENT: 'Kommentar'
        },
        STEP_4: {
          HEADER: 'Vielen Dank!',
          HEADER_FAIL: 'Buchung fehlgeschlagen.',
          MESSAGE: 'Wir haben Ihre Buchungsanfrage erhalten. Sie erhalten ebenso eine Mail zur Bestätigung der Buchungsanfrage.',
          MESSAGE_APPROVE_NEEDED_TIME: 'Ihre Anfrage muss allerdings noch bestätigt werden. Sie werden über eine erfolgte Bestätigung oder Ablehnung der Buchung innerhalb der nächsten {{settings.auto_confirm}} Stunden per Mail informiert.',
          MESSAGE_APPROVE_NEEDED: 'Ihre Anfrage muss allerdings noch bestätigt werden. Sie werden über eine erfolgte Bestätigung oder Ablehnung der Buchung per Mail informiert.',
          MESSAGE_FAIL_ALREADY_BOOKED: 'Die von Ihnen gewählte Zeit ist leider in der Zwischenscheit schon anderweitig vergeben worden. Bitte wählen Sie eine andere Zeit / einen anderen Tag für Ihre Buchung.'
        },
        BUTTON_PREV: 'Zurück',
        BUTTON_NEXT: 'Weiter',
        BUTTON_ANOTHER: 'Weiteren Termin buchen',
        BUTTON_BOOK: 'Buchen',
        BUTTON_CLOSE: 'Schließen'
      }

    }
  }
}

