import { MomentDatePipe, MomentTimePipe } from '@bluebamboo/common-ui';

export class CancellableCheckup {
    public cancellable: boolean = false;
    public reason: number = 0;
    public pay: boolean = false;
    public pay_price: number = 0;
    public booking: CancelBookingInfo;
    
    constructor(data: any = null) {
      if (data) {
        this.cancellable = data.cancellable || false;
        this.reason = data.reason || 0;
        this.pay = data.pay || false;
        this.pay_price = data.pay_price || 0.0;
        if(data.booking) {
            this.booking = new CancelBookingInfo(data.booking);
        } else {
            this.booking = new CancelBookingInfo({});
        }
      }
    }

  }

export class CancelBookingInfo { 
	public title: string = "";
    public date: string = "";
    public startTime: string = "";
    public duration: number;
	public price: number;

	constructor(data: any = null, timePipe: MomentTimePipe = new MomentTimePipe(), datePipe: MomentDatePipe = new MomentDatePipe()) {
        if (data) {
          this.title = data.title || "";
          if(data.date) {
              this.date = datePipe.transform(data.date);
          }
          if(data.startTime) {
            this.startTime = timePipe.transform(data.startTime);
          }
          this.duration = data.duration || 0;
          this.price = data.price || 0.0;
        }
    }
}
  