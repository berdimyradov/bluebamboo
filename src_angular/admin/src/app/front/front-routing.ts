import { EndUserAgreementComponent } from './booking/eua/end-user-agreement.component';
import { CancelationComponent } from './cancelation/cancelation.component';
import { BookingComponent } from './booking/booking.component';
import { Routes } from '@angular/router';

export const frontRoutes: Routes = [
  {
    path: 'onlinebooking/:key',
    pathMatch: 'full',
    component: BookingComponent
  },
  {
    path: 'onlinebooking/cancel/:key',
    pathMatch: 'full',
    component: CancelationComponent
  },
  {
    path: 'onlinebooking/agreement',
    pathMatch: 'full',
    component: EndUserAgreementComponent
  }
];
