/**
    * Created by aleksay on 04.05.2017.
    * Project: tmp_booking
    */
import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-booking-front',
    templateUrl: 'front.html'
})
export class FrontComponent {
    public color: string = "#ff0000";
    constructor(private translate: TranslateService) { }

    ngOnInit() {
        this.translate.addLangs(["de", "en", "fr"]);
        this.translate.setDefaultLang('de');

        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/de|en|fr/) ? browserLang : 'de');
    }
}
