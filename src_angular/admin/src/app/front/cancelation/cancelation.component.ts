import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CancelationService } from './service/cancelation-service';
import { CancellableCheckup, CancelBookingInfo } from '../types/cancellable-checkup';

@Component({
  selector: 'app-cancelation-component',
  templateUrl: 'cancelation.component.html'
})

export class CancelationComponent implements OnInit {
  public setting: CancellableCheckup;
  public booking: CancelBookingInfo;
  public error: boolean = false;
  public settingsLoading: boolean = false;

  constructor(private activeRoute: ActivatedRoute,
    private cancelService: CancelationService) {
  }

  ngOnInit() {
    let cancelationKey = this.activeRoute.snapshot.params.key;
    this.cancelService.setAuthKey(cancelationKey);

    this.setting = new CancellableCheckup();
    this.booking = new CancelBookingInfo();

    this.settingsLoading = true;
    this.cancelService.getCancelationBookingInfo()
      .subscribe((res: CancellableCheckup) => {
        this.settingsLoading = false;
        this.setting = res;
        this.booking = this.setting.booking;
      }
      ,(err: any) => { 
        this.settingsLoading = false;
        this.error = true;
      });
  }

  public cancelBooking() {
    this.cancelService.cancelBooking();
  }
}
