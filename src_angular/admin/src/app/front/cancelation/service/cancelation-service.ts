import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { CancellableCheckup } from '../../types/cancellable-checkup';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CancelationService {

  private requestHeaders: Headers;

  constructor(private http: Http) {
    this.requestHeaders = new Headers();
  }

  public setAuthKey(key: string): void {
    this.requestHeaders.append('X-Booking-Id', key);
  }

  public getCancelationBookingInfo(): Observable<CancellableCheckup> {
    return this.http.get('/api/v1/onlinebooking/bookings/cancelcheck', { headers: this.requestHeaders })
      .map( (res: Response) => new CancellableCheckup(res.json()) );
   }

  public cancelBooking(): Observable<Response> {
    return this.http.put('/api/v1/onlinebooking/bookings/cancel', {}, { headers: this.requestHeaders });
  }

}
