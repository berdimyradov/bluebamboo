import { EndUserAgreementComponent } from './booking/eua/end-user-agreement.component';
import { CancelationComponent } from './cancelation/cancelation.component';
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
import { AdminCommonModule } from '../common/common.module';


import { BookingComponent } from './booking/booking.component';
import { BookingPlaceComponent } from './booking/place/booking-place.component';
import { BookingServicesListComponent } from './booking/place/services-list/booking-services-list.component';
import { BookingServiceComponent } from './booking/place/services-list/service/booking-service.component';
import { BookingProcessModalComponent } from './booking/process/boking-process.component';
import { BookingStepClientInfoComponent } from './booking/process/step-client-info/booking-step-client-info.component';
import { BookingStepLocationComponent } from './booking/process/step-location/booking-step-location.component';
import { BookingStepNotificationComponent } from './booking/process/step-notification/booking-step-notification.components';
import { BookingStepTimeComponent } from './booking/process/step-time/booking-step-time.component';
import { BookingApiService } from './booking/bookingApi.service';
import { BookingToolService } from './booking/bookingTool.service';
import { CancelationService } from './cancelation/service/cancelation-service';

import { FrontComponent } from './front.component';
import { CommonModule } from '@angular/common';
import { CommonUiModule } from '@bluebamboo/common-ui';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { locale as en } from './resources/en';
import { locale as de } from './resources/de';

export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    CommonUiModule,
    AdminCommonModule,
    FormsModule,
    BrowserAnimationsModule,
    RouterModule,
    TranslateModule
  ],
  declarations: [
    FrontComponent,
    BookingComponent,
    BookingPlaceComponent,
    BookingServicesListComponent,
    BookingServiceComponent,
    BookingProcessModalComponent,
    BookingStepClientInfoComponent,
    BookingStepLocationComponent,
    BookingStepNotificationComponent,
    BookingStepTimeComponent,

    CancelationComponent,
    EndUserAgreementComponent
  ],
  providers: [
    BookingApiService,
    BookingToolService,
    CancelationService
  ]
})

export class FrontModule {
  constructor(translate: TranslateService) {
    translate.setTranslation('en', en, true);
    translate.setTranslation('de', de, true);
  }
}
