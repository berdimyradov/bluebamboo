/**
    * Created by aleksay on 13.04.2017.
    * Project: tmp_booking
    */
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Place } from './type/place';
import { Product } from './type/product';
import { Location } from './type/location';
import { BookingDate } from './type/booking-date';
import { TimeInfo } from './type/time-info';
import { HttpClient } from '@angular/common/http';
/* TODO: get rid from old HTTP service*/
@Injectable()
export class BookingApiService {
  private urlGetServices: string = '/api/v1/onlinebooking/products';
  private urlGetLocations: string = 'api/v1/onlinebooking/locations/p/{product_id}';
  private urlGetDateForBooking: string = '/api/v1/onlinebooking/freedays/p/{product_id}/l/{location_id}/e/{employee_id}/{year}/{month}';
  private urlGetTimeForBooking: string = '/api/v1/onlinebooking/freetimes/p/{product_id}/l/{location_id}/e/{employee_id}/{year}/{month}/{day}';

  private requestHeaders: Headers;

  constructor(private http: Http, private httpClient: HttpClient) {
    this.requestHeaders = new Headers();
  }

  public SaveNewBooking(order) {
    const dateString = `${order.date.getFullYear()}/${order.date.getMonth() + 1}/${order.date.getDate()}`;
    const dataForNewBooking = {
      firstname: order.firstName,
      name: order.surname,
      mail: order.email,
      phone: order.phone,
      comment: order.comment,
      date: dateString,
      startTime: order.time,
      product_id: order.service_id,
      employee_id: order.employee_id,
      location_id: order.location_id
    };

    return this.http.post('/api/v1/onlinebooking/book',
      dataForNewBooking,
      { headers: this.requestHeaders }
    );

  }

  private getUrl(url: string, params: any) {
    Object.keys(params).forEach((key: string) => {
      url = url.replace(new RegExp('\{' + key + '\}', 'g'), params[key]);
    });
    return url;
  }

  public getConfirmation() {
    return this.http.get('/api/v1/onlinebooking/settings', { headers: this.requestHeaders })
           .map( (res) => res.json())
  }

  // try this one and it should work. And please Change the Header key Definition to "X-Client-Id" (small d at the end)
  public setAuthKey(key: string): void {
    this.requestHeaders.append('X-Client-Id', key);
  }

  public getProducts(): Observable<Product[]> {
    return this.http.get('/api/v1/onlinebooking/products', { headers: this.requestHeaders })
      .map(this.extractServicesData)
      .catch(this.handleError);
  }

  public getLocations(service_id: string): Observable<Location[]> {
    return this.http.get(this.getUrl(this.urlGetLocations,
      { product_id: service_id }),
      { headers: this.requestHeaders })
      .map(this.extractLocationsData)
      .catch(this.handleError);
  }

  public getDateForBooking(service_id: string,
    location_id: string,
    employee_id: string,
    year: number,
    month: number): Observable<BookingDate> {
    return this.http.get(this.getUrl(this.urlGetDateForBooking, {
      product_id: service_id,
      location_id: location_id,
      employee_id: employee_id,
      year,
      month: month > 9 ? '' + month : '0' + month
    }), { headers: this.requestHeaders })
      .map(this.extractDateForBookingData)
      .catch(this.handleError);
  }

  public getTimeForBooking(service_id: string,
    location_id: string,
    employee_id: string,
    date: Date): Observable<TimeInfo> {

    let year = date.getFullYear();
    let m = date.getMonth() + 1;
    let month = m > 9 ? '' + m : '0' + m;
    let d = date.getDate()
    let day = d > 9 ? '' + d : '0' + d;

    return this.http.get(this.getUrl(this.urlGetTimeForBooking, {
      product_id: service_id,
      location_id: location_id,
      employee_id: employee_id,
      year,
      month,
      day
    }), { headers: this.requestHeaders })
      .map((data) => this.extractTimeForBookingData(data))
      .catch(this.handleError);
  }

  private extractServicesData(res: Response) {
    let result: Product[] = [];
    res.json().forEach((el: any) => result.push(new Product(el)));
    return result;
  }

  private extractLocationsData(res: Response) {
    let result: Location[] = [];
    res.json().forEach((el: any) => result.push(new Location(el)));
    return result;
  }

  private extractDateForBookingData(res: Response) {
    return new BookingDate(res.json());
  }

  private extractTimeForBookingData(res: Response) {
    return new TimeInfo(res.json());
  }

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
