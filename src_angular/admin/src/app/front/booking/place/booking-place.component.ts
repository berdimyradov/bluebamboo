/**
    * Created by aleksay on 13.04.2017.
    * Project: tmp_booking
    */
import {Component, Input} from '@angular/core';
import {Place} from "../type/place";

@Component({
    selector: 'booking-place',
    templateUrl: './booking-place.html'
})
export class BookingPlaceComponent {
    @Input()
    public collection_id: string;
    @Input()
    public place: Place = null;
}
