/**
    * Created by aleksay on 13.04.2017.
    * Project: tmp_booking
    */
import { Component, Input } from '@angular/core';
import { Product } from '../../../type/product';
import { BookingToolService } from '../../../bookingTool.service';

@Component({
  selector: 'booking-service',
  templateUrl: './booking-service.html'
})
export class BookingServiceComponent {
  @Input()
  public collection_id: string;

  @Input()
  public place_id: string;

  @Input()
  public service: Product;

  constructor(private tool: BookingToolService) { }

  public makeBooking() {
    this.tool.booking(this.collection_id, this.place_id, this.service.id);
  }
}
