/**
    * Created by aleksay on 13.04.2017.
    * Project: tmp_booking
    */
import {Component, Input} from '@angular/core';
import {Product} from '../../type/product';

const serviceLineMinCount : number = 5;

@Component({
    selector: 'booking-services-list',
    templateUrl: './booking-services-list.html'
})
export class BookingServicesListComponent {
    @Input()
    public services: Product[] = [];
    public showAll: boolean = false;

    public getTop() : number {
        return this.showAll ? this.services.length : serviceLineMinCount;
    }

    public toggle(){
        this.showAll = !this.showAll;
    }
}
