/**
 * Created by aleksay on 20.04.2017.
 */
export class TimeInfo{
    public step: number;
    public startTime: string;
    public endTime: string;
    public availableTime: string[] = [];
    public timeSpots: TimeSpot[] = [];

    constructor(data: any = null){
        if (data){
            data.forEach((d:any) => {
              if (d.available) {
                this.availableTime.push(d.time);
              }
              this.timeSpots.push(new TimeSpot(d));
            });
            this.startTime = this.availableTime[0];
            this.endTime = this.availableTime[this.availableTime.length-1];
            console.log("Timespots:");
            console.log(this.timeSpots);
        }
    }
}

export class TimeSpot {
    public available: boolean;
    public time: string;
    public date: string;

    constructor(spot: any) {
        this.available = spot.available;
        this.time = spot.time;
        this.date = spot.date;
    }
}
