/**
    * Created by aleksay on 18.04.2017.
    * Project: tmp_booking
    */
export class Order {
    // public collection_id: string;
    public place_id: string;
    public service_id: string;
    public location_id: string;
    public employee_id: string;
    public date: Date;
    public time: string;
    public firstName: string;
    public surname: string;
    public email: string;
    public phone: string;
    public comment: string;
    public agree: boolean = null;

    private stringIsFilled(s: string): boolean{
        return !(s === undefined || s === null || s.trim() === "")
    }

    public contactIsValid(): boolean{
        return this.stringIsFilled(this.firstName) && this.stringIsFilled(this.surname) && this.stringIsFilled(this.email) && this.agree;
    }

    public again(){
        this.location_id = null;
        this.employee_id = null;
        this.date = null;
        this.time = null;
        this.agree = false;
    }
}
