/**
    * Created by aleksay on 13.04.2017.
    * Project: tmp_booking
    */

export class Service{
    public id: string;
    public name: string;
    public price: number;
    public currency: number;
    public duration: number;
    public detail: string;

    constructor(data: any){
        if (data){
            this.id = data.id;
            this.name = data.name;
            this.price =  data.price;
            this.currency = data.currency;
            this.duration = data.duration;
            this.detail = data.detail;
        }
    }
}