/**
    * Created by aleksay on 18.04.2017.
    * Project: tmp_booking
    */
import { Employee } from "./employee";

export class Location {
    public id: string;
    public name: string;
    public description: string;
    public employees: Employee[] = [];
    constructor(data: any) {
        if (data) {
            this.id = data.id;
            this.name = data.title;
            this.description = data.description;
            data.employees.forEach((e: any) => this.employees.push(new Employee(e)));
        }
    }
}