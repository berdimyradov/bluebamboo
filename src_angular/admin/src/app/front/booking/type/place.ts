/**
    * Created by aleksay on 13.04.2017.
    * Project: tmp_booking
    */
import {Contact}        from './contact';
import {Service}        from './service';

export class Place {
    public id: string;
    public name: string;
    public stars: number;
    public location: string;
    public description: string;
    public contact: Contact;
    public image: string;
    public services: Service[] = [];

    constructor(data: any){
        if (data){
            this.id = data.id;
            this.name = data.name;
            this.stars = data.stars;
            this.location = data.location;
            this.description = data.description;
            this.contact = new Contact(data.contact);
            this.image = data.image;
            //data.services.forEach((service:any) => this.services.push(new Service(service)));
        }
    }
}