/**
    * Created by aleksay on 13.04.2017.
    * Project: tmp_booking
    */

export class Product{
    public id: string;
    public description: string;
    public title: string;
    public active: boolean;
    public color: string;
    public breakAfter: string;
    public clientId: number;
    public price: number;
    public type: number;
    public currency: number;
    public duration: number;
    public onlineBookingAvailable: boolean

    constructor(data: any){
        if (data){
            this.id = data.id;
            this.type = data.type;
            this.title = data.title;
            this.description = data.description;
            this.price =  data.price;
            this.currency = data.currency;
            this.duration = data.duration;
            this.breakAfter = data.breakAfter;
            this.active = data.active;
            this.onlineBookingAvailable = data.online_booking_available;
            this.color = data.color;
            this.clientId = data.clientId;
        }
    }
}