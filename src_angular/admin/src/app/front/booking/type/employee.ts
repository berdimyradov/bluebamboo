/**
    * Created by aleksay on 18.04.2017.
    * Project: tmp_booking
    */
export class Employee{
    public id: string;
    public name: string;

    constructor(data: any = null){
        if (data){
            this.id = data.id;
            this.name = data.name;
        }
    }
}