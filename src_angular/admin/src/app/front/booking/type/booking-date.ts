/**
    * Created by aleksay on 19.04.2017.
    * Project: tmp_booking
    */
    export class BookingDate {
      public startDate: Date;
      public endDate: Date;
      public availableDate: Date[] = [];
      public bookableDays = [];

      constructor(data: any = null) {
        if (data) {
          data.forEach((day: any) => {
            this.availableDate.push(day.date);
            if (day.available) {
              this.bookableDays.push(day.date);
            }
          });
          this.startDate = new Date(this.availableDate[0]);
          this.endDate = new Date(this.availableDate[this.availableDate.length - 1]);
        }
      }
    }
