/**
    * Created by aleksay on 13.04.2017.
    * Project: tmp_booking
    */

export class Contact{
    public phone: string;
    public email: string;
    public www: string;

    constructor(data:any){
        if (data){
            this.phone = data.phone;
            this.email = data.email;
            this.www = data.www;
        }
    }
}