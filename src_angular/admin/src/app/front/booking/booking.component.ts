/**
    * Created by aleksay on 13.04.2017.
    * Project: tmp_booking
    */
import { Component, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { BookingApiService } from './bookingApi.service';
import { ActivatedRoute } from '@angular/router';
import { Product } from './type/product';

@Component({
  selector: 'booking',
  templateUrl: './booking.html'
})
export class BookingComponent implements OnInit, OnDestroy {
  public products: Product[] = [];
  private collection_id = '10';
  private routeSubscription;
  public inviteKey: string;

  constructor(private bookingApi: BookingApiService,
              private el: ElementRef,
              private route: ActivatedRoute) {

  }
  public ngOnInit() {
    this.bookingApi.setAuthKey(this.route.snapshot.params.key);

    this.bookingApi.getProducts().subscribe(
      result => this.products = result,
      error => this.errorHandler(error)
    );
  }

  public ngOnDestroy() {

  }
  private errorHandler(error: any) {
    console.error(error);
  }
}
