/**
    * Created by aleksay on 18.04.2017.
    * Project: tmp_booking
    */
import { Component, Input, ViewChild } from '@angular/core';
import { Order } from '../../type/order';
import { Location } from '../../type/location';
import { BookingApiService } from '../../bookingApi.service';
import { SelectOption } from '../../../../common/types/select-option';
import { Employee } from '../../type/employee';
import { CalendarDayState } from '../../../../common/calendar/types/calendar-day-state';
import { BookingDate } from '../../type/booking-date';
import { ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'booking-step-location',
  templateUrl: './booking-step-location.html',
  styleUrls: ['./booking-step-location.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BookingStepLocationComponent {
  @Input() public settings: any;
  @Input() public order: Order;
  @ViewChild('employeeselect') empselect;

  private _locations: Location[];
  private _bookingDate: BookingDate;
  private _currentDate: Date;
  public locations: SelectOption[];
  public employees: SelectOption[];
  private _locationSelected: boolean;

  public get startDate(): Date {
    return this._bookingDate ? this._bookingDate.startDate : this._currentDate;
  }
  public get endDate(): Date {
    return this._bookingDate ? this._bookingDate.endDate : this._currentDate;
  }

  public get location_id(): string {
    return this.order ? this.order.location_id : null;
  }
  public set location_id(val: string) {
    /*
    if (this.order.location_id == val) {
      return;
    }
    */
    this.order.location_id = val;
    // this.order.employee_id = '0';
    this._locationSelected = true;
    this.initEmployee();
  }

  public get locationSelected(): boolean {
    return this._locationSelected;
  }

  public get employee_id(): string {
    return this.order ? this.order.employee_id : null;
  }

  public set employee_id(val: string) {

    /*
    if (this.order.employee_id == val) {
      return;
    }
    */

    this.order.employee_id = val;
    //this.order.date = null;
    //this.order.time = null;
    if (val === null || val === '') {
      this._bookingDate = null;
      return;
    }
    let currentData = new Date();
    let year = currentData.getFullYear();
    let month = currentData.getMonth() + 1;

    this.bookingApi.getDateForBooking(this.order.service_id, this.order.location_id, this.order.employee_id, year, month)
      .subscribe(
        result => { this._bookingDate = result; },
        error => this.errorHandler(error)
      );
  }

  constructor(private bookingApi: BookingApiService, private translate: TranslateService) { }  

  ngOnInit() {
    this.init();
  }

  public init() {

    this._locations = [];
    this._bookingDate = null;
    this._currentDate = new Date();
    this.locations = [];
    this.employees = [];
    this._locationSelected = false;
    this.bookingApi.getLocations(this.order.service_id)
    .subscribe(
      result => this.transformLocations(result),
      error => this.errorHandler(error)
    );
  }

  public onCalendarNewMonth(newCalendarData) {
    this.bookingApi.getDateForBooking(
      this.order.service_id,
      this.order.location_id,
      this.order.employee_id,
      newCalendarData.year,
      newCalendarData.month)
      .subscribe(
        result => { this._bookingDate = result; },
        error => this.errorHandler(error)
      );
  }

  public getDayState = (day: number, month: number, year: number): CalendarDayState => {
    const result: CalendarDayState = new CalendarDayState();
    const newMonth = month < 10 ? '0' + month + 1 : month + 1;
    const newDay = day < 10 ? '0' + day : day;
    const bookingSearchStr = `${year}-${newMonth}-${newDay}`;
    const index = this._bookingDate.bookableDays.indexOf(bookingSearchStr);

    result.isEnabled = index > -1;
    return result;
  };

  private initEmployee() {
    let employees: SelectOption[] = [];

    const anyEmployee = new SelectOption();
    anyEmployee.title = this.translate.instant('PAGE.FRONT.BOOKING_MODAL.STEP_1.ANY_EMPLOYEE');
    anyEmployee.code = '0';
    employees.push(anyEmployee);

    this._locations.forEach((l: Location) => {
      if (l.id == this.order.location_id) {
        l.employees.forEach((e: Employee) => {
          let option: SelectOption = new SelectOption();
          option.title = e.name;
          option.code = e.id;
          employees.push(option);
        });
      }
    });
    this.employees = employees;

    if (employees.length == 2) {
      this.employee_id = employees[1].code;
      return;
    }

    if(this.order.employee_id === null || this.order.employee_id === '' || !this.order.employee_id) {
      this.employee_id = '0';
    }
    else {
      this.employee_id = this.order.employee_id;
    }
    
  }

  private transformLocations(data: Location[]) {
    let res: SelectOption[] = [];
    this._locations = data;
    data.forEach((d: Location) => {
      let option: SelectOption = new SelectOption();
      option.title = d.name;
      option.code = d.id;
      res.push(option);
    });
    this.locations = res;
    if (this.order.location_id === null || this.order.location_id === '' || !this.order.location_id) {
      if (res.length == 1) {
        this.location_id = res[0].code;
      }
    } else {
      this.location_id = this.order.location_id;
      // let employee_id: string = this.order.employee_id || '0',
      let date: Date = this.order.date,
          time: string = this.order.time;
      this.order.date = date;
      this.order.time = time;
      this.initEmployee();
      // this.employee_id = employee_id;
      
    }
  }

  private errorHandler(error: any) { }
}
