/**
    * Created by aleksay on 18.04.2017.
    * Project: tmp_booking
    */
import { BookingApiService } from '../../bookingApi.service';
import { Component, Input } from '@angular/core';
import { SelectOption } from '../../../../common/types/select-option';
import { Order } from '../../type/order';
import { RouterLink } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
@Component({
  selector: 'booking-step-client-info',
  templateUrl: './booking-step-client-info.html',
  styleUrls: ['./booking-step-client-info.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BookingStepClientInfoComponent implements OnInit {
  @Input() public order: Order;
  @Input() public settings: any;

  public must_accept: boolean;
  public terms_of_use: string;
  public show_agb = false;

  constructor(
    private activeRoute: ActivatedRoute,
    private router: Router,
    private bookingApi: BookingApiService
  ) { }

  ngOnInit() {   
    this.must_accept = this.settings.must_accept;
    if (!this.settings.must_accept) {
      this.order.agree = true;
    } else {
      this.terms_of_use = this.settings.terms_of_use
    }
  }

  public displayAgreement() {
    this.router.navigate(['../'], { relativeTo: this.activeRoute });
  }

  public showAGB() {
    this.show_agb = true;
  }

  public hideAGB() {
    this.show_agb = false;
  }
}
