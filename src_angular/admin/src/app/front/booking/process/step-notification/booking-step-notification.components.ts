/**
 * Created by aleksay on 18.04.2017.
 * Project: tmp_booking
 */
import {Component, Input} from "@angular/core";
import {Order} from "../../type/order";

@Component({
    selector: 'booking-step-notification',
    templateUrl: './booking-step-notification.html'
})
export class BookingStepNotificationComponent {
    @Input() public order: Order;
    @Input() public processSuccess: boolean;
    @Input() public processCode: number;
    @Input() public settings: any;

}
