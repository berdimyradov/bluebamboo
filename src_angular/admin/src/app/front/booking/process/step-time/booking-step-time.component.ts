/**
 * Created by aleksay on 18.04.2017.
 * Project: tmp_booking
 */
import { Component, Input, OnInit } from '@angular/core';
import { Order } from '../../type/order';
import { BookingApiService } from '../../bookingApi.service';
import { ViewEncapsulation } from '@angular/core';
import { TimeInfo, TimeSpot } from '../../type/time-info';

@Component({
  selector: 'booking-step-time',
  templateUrl: './booking-step-time.html',
  styleUrls: ['./booking-step-time.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BookingStepTimeComponent implements OnInit {
  public timeData:TimeSpot[] = [];
  public columncount: number = 4;
  @Input() public order: Order;
  @Input() public settings: any;


  constructor(private bookingApi: BookingApiService) { }

  ngOnInit() {
    this.bookingApi
      .getTimeForBooking(
        this.order.service_id,
        this.order.location_id,
        this.order.employee_id,
        this.order.date).subscribe(result => {
          this.timeData = result.timeSpots;
          },
          error => this.errorHandler(error)
        );

    let step: number = this.settings.time_period || 15;    
    this.columncount = 60 / step;
  }

  private errorHandler(error: any) { }
}
