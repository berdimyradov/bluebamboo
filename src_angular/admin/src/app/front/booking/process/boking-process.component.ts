/**
    * Created by aleksay on 18.04.2017.
    * Project: tmp_booking
    */
import { Component } from '@angular/core';
import { Modal } from '../../../common/modal/modal.injector';
import { Order } from '../type/order';
import { ViewEncapsulation } from '@angular/core';

import { BookingApiService } from '../bookingApi.service';

@Component({
  selector: 'booking',
  templateUrl: './booking-process.html',
  styleUrls: ['./booking-process.scss'],
  encapsulation: ViewEncapsulation.None
})
@Modal()
export class BookingProcessModalComponent {
  private destroy: Function;
  private closeModal: Function;

  // public collection_id: string;
  public place_id: string;
  public service_id: string;
  public step: number = 1;
  public order: Order = new Order();
  public orderProcessSuccess = false;
  public orderProcessCode: number;
  public settings:any;

  constructor(private bookingService: BookingApiService) {

  }

  ngOnInit(): void {
    this.order.place_id = this.place_id;
    this.order.service_id = this.service_id;
    this.bookingService.getConfirmation().subscribe( (data) => {
      this.settings = data;
    });
  }

  public close() {
    this.closeModal();
    this.destroy();
  }

  public next() {
    if (this.step >= 4) {
      this.step = 4;
      return;
    }
    this.step++;
  }

  public prev() {
    if (this.step <= 1) {
      this.step = 1;
      return;
    }
    this.step--;
  }

  public nextStepIsDisabled(): boolean {
    switch (this.step) {
      case 1:
        return this.order.date == null;
      case 2:
        return this.order.time == null;
      case 3:
        return !this.order.contactIsValid();
    }
    return true;
  }

  public booking() {
    this.bookingService.SaveNewBooking(this.order)
      .subscribe((res) => {
        if (res.ok) {
          this.orderProcessSuccess = true; 
        } else {
          this.orderProcessSuccess = false;
          this.orderProcessCode = res.status;
        }
        this.step++;
      })

  }

  public bookingAnother() {
    this.order.again();
    this.step = 1;
  }
}
