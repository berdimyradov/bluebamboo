/**
 * Created by aleksay on 18.04.2017.
 */
import { Injectable } from '@angular/core';
import { ModalService } from '@bluebamboo/common-ui';
import { FrontModule } from '../front.module';
import {BookingProcessModalComponent} from './process/boking-process.component';

@Injectable()
export class BookingToolService {
    constructor(private modalService: ModalService) {}

    public booking(collection_id: string, place_id: string, service_id: string) {
        this.modalService
            .create(FrontModule,
                    BookingProcessModalComponent,
                    {
                      collection_id: collection_id,
                      place_id: place_id,
                      service_id: service_id
                    });
    }
}
