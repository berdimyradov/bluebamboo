import { Component, OnInit } from '@angular/core';
import { BookingApiService } from '../bookingApi.service';


@Component({
  selector: 'app-user-agreement',
  templateUrl: './end-user-agreement.component.html',
  styleUrls: ['./end-user-agreement.component.scss']
})

export class EndUserAgreementComponent implements OnInit {
  public terms_of_use: string;
  constructor(private bookingApiService: BookingApiService ) { }

  ngOnInit() {
    this.bookingApiService.getConfirmation().subscribe( (data) => {
      this.terms_of_use = data.terms_of_use;
    });
  }
}
