// In order to get options from url uncomment:
// server_options = $.get('/api/template-strings')

$.FroalaEditor.DefineIcon('dropdownIcon', { NAME: 'magic' });

// Define a dropdown button.
$.FroalaEditor.RegisterCommand('templateVarsDropdown', {
  title: 'Platzhalter',
  type: 'dropdown',
  icon: 'dropdownIcon',
  options: {},
  undo: true,
  focus: true,
  refreshAfterCallback: true,

  callback: function (cmd, val) {
     this.html.insert(val);
  },

  refreshOnShow: function ($btn, $dropdown) {
    var list = $dropdown.find('ul.fr-dropdown-list');
    var options = this.opts.getOptions() || [];
    var listItems = options.map(function (item) {
      return '<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="templateVarsDropdown" data-param1="' + item.code + '" title="' + item.label + '" aria-selected="false">' + item.label + '</a></li>';
    });

    list.empty().append(listItems);

    if (!this.selection.inEditor()) {
      // Move cursor position to end.
      this.selection.setAtEnd(this.$el.get(0));
      this.selection.restore();
    }
  }
});
