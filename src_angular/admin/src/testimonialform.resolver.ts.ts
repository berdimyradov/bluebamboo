import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";

import { Cms2Service } from "app/cms2/services/cms2.service";
import { Testimonial } from "app/cms2/types/testimonial";

@Injectable()

export class TestimonialformResolver implements Resolve<Testimonial> {

  constructor (private service : Cms2Service) {}

  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Testimonial | Observable<Testimonial> | Promise<Testimonial> {
    const id: string = route.paramMap.get('id');
    if (id === 'add') return Observable.of(new Testimonial());
    return this.service.getTestimonial(parseInt(id));
  }

}
