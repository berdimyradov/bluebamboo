import { BluebamboAdminPage } from './app.po';

describe('bluebambo-admin App', () => {
  let page: BluebamboAdminPage;

  beforeEach(() => {
    page = new BluebamboAdminPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
