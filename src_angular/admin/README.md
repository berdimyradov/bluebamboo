# BluebamboAdmin

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Generate new components

* cd admin
* ng g component <component-name> 

## Setup dev environment
For example we use "www.bluebamboo_dev.com" as domain name of application

### Apache
Find and uncomment line
```httpd.conf
LoadModule headers_module modules/mod_headers.so
```

Add lines to httpd.conf
```httpd.conf
<ifModule mod_headers.c>
  Header set Access-Control-Allow-Origin "http://www.bluebamboo_dev.com:4200"
  Header set Access-Control-Allow-Headers "Origin, X-Requested-With, Content-Type, Accept"
  Header set Access-Control-Allow-Credentials true
  Header set Access-Control-Allow-Methods GET,PUT,POST,DELETE,OPTIONS
</ifModule>
```
Restart apache.
* cd admin
* ng serve --disable-host-check
* open http://www.bluebamboo_dev.com:4200/de/admin/login

## Create module as example "configuration"

* cd admin
* ng g module configuration
* cd src/app/configuration
* modify configuration.module.ts
  * add link to module : import { AdminCommonModule } from '../common'
  * add it to imports: imports: [ ... AdminCommonModule ...]
* create file configuration.routing.ts
```typescript
import {Routes} from "@angular/router";
import {ConfigurationPageComponent} from "./configuration-page/configuration-page.component";
import {RoomConfigurationComponent} from "./configuration-page/room-configuration/room-configuration.component";
import {RoomFormComponent} from "./configuration-page/room-form/room-form.component";
import {RoomResolver} from "./configuration-page/room-form/room-resolver";

export const configurationRoutes : Routes = [
  { path: 'configuration', pathMatch: 'full', component: ConfigurationPageComponent },
  { path: 'configuration/room', pathMatch: 'full', component: RoomConfigurationComponent },
  {
    path: 'configuration/room/:id',
    pathMatch: 'full',
    component: RoomFormComponent,
    resolve: {
      room: RoomResolver
    }
  }
];
```
* create file index.ts
* edit file index.tx
  ```typescript
      export * from './configuration.module';
      export * from './configuration.routing';
  ```
* cd ..
* edit file app.modules.ts
    ```typescript
      import { ConfigurationModule, configurationRoutes } from './configuration/';
      ...
      appRoutes[1].children = configurationRoutes.concat(appRoutes[1].children);
      ...
      imports: [...] -> imports: [ ... ConfigurationModule ...]
    ```
* cd configuration
* create page
  * ng g component configuration-page
    * add parameters to constructor
     
    ```typescript
      constructor(
        private router: Router,                  
        private activeRoute: ActivatedRoute,    
        private commonPage: CommonPageComponent
      ) { }
	```
	* add to ngInit next code 
    ```typescript
        this.commonPage.title = 'PAGE.CONFIGURATION.TITLE';
    ```
  * add route animation
    ```typescript
    @Component({
      ...
      host: {
        '[@routeAnimationOpacity]': 'true'
      },
      animations: [
        routeAnimationOpacity
      ]
    })
    ```
