"use strict";
const
    fs = require('fs'),
    path = require('path'),
    gulp = require('gulp'),
    modules = {};

let
    deploy_task = [],
    build_task = [],
    deploy_prod_task = [],
    build_prod_task = [];


function autoload() {
    let files = fs.readdirSync(path.join(__dirname, ".gulp/"));
    files.forEach((file) => {
        const
            key = path.basename(file,'.js'),
            module = require(`./.gulp/${key}`);
        modules[key] = module;
        if(module.deploy) {
            deploy_task = deploy_task.concat(module.deploy);
        }
        if(module.build) {
            build_task = build_task.concat(module.build);
        }
        if(module.deploy_prod) {
            deploy_prod_task = deploy_prod_task.concat(module.deploy_prod);
        }
        if(module.build_prod) {
            build_prod_task = build_prod_task.concat(module.build_prod);
        }
    });
}

autoload();

gulp.task('build', gulp.series(build_task));
gulp.task('deploy', gulp.series(deploy_task));
gulp.task('build-deploy', gulp.series(['build', 'deploy']));
gulp.task('default', gulp.series(['build-deploy']));

gulp.task('build-prod', gulp.series(build_prod_task));
gulp.task('deploy-prod', gulp.series(deploy_prod_task));
gulp.task('build-deploy-prod', gulp.series(['build-prod', 'deploy-prod']));
