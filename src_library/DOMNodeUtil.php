<?php 

namespace GSCLibrary;

/**
 * DOMNode Utility
 */
class DOMNodeUtil
{
	public static function printNode ($node) {
		$res = "DOMNode-Dump\n";
		
		if ($node instanceof \DOMNode) {
			DOMNodeUtil::printOne($node, 0, $res);
		}
		else if ($node instanceof \DOMElement) {
			DOMNodeUtil::printOne($node, 0, $res);
		}
		else if ($node instanceof \DOMNodeList) {
			for ($i = 0; $i < $node->length; $i++) {
				DOMNodeUtil::printOne($node->item($i), 0, $res);
			}
		}
		else return "invalid type in DOMNodeUtil.print: " . get_class($node);
		return $res;
	}
	
	private static function printOne ($node, $level, &$res) {
		if (!$node->hasChildNodes()) return;
		foreach ($node->childNodes as $child) {
			//$this->logger->log("child name[" . ($index++) . "]: " . $child->nodeName);
			//$node->appendChild($doc->importNode($child, true));
			for ($i=0; $i<$level; $i++) $res .= "--";
			$res .= $child->nodeName;
			
			if ($child->nodeName == "#text") {
				$str = $child->nodeValue;
				if (strlen($str) > 10) $str = substr($str, 0, 7) . '...';
				$res .= ", '" . $str . "'";
			}
			if ($child->hasAttributes()) {
				foreach ($child->attributes as $name => $attrNode) {
					$res .= ", " . $name . "=" . $attrNode->nodeValue;
				}
			}
			
			$res .= "\n";
			DOMNodeUtil::printOne ($child, $level + 1, $res);
		}
	}
}
