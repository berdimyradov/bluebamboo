<?php

namespace GSCLibrary;

class GSCFileSwitch {

	private static $_instance = null;
	public static function getInstance()
	{
		if(null === self::$_instance) {
			self::$_instance = new self();
			$container = getenv("blob_storage_container_data");
			if(empty($container) && boolval(getenv("use_blob_storage"))) { 
				throw new \Exception("Blob Storage should be used but no container name defined in Environment", 500);
			}
			self::$_instance->setContainer($container);
		}
		return self::$_instance;
	}

	private $_container = "data";
	public function getContainer()
	{
		return $this->_container;
	}
	public function setContainer($container)
	{
		$this->_container = $container;
	}

	/* -------------------------------------------------------------------------
	 * Blob Storage Service
	 */
	private static $_blobStorage = null;
	private function getBlobStorage()
	{
		if(null === self::$_blobStorage) {
			self::$_blobStorage = new GSCAzureBlobStorage();
		}
		return self::$_blobStorage;
	}

	/* -------------------------------------------------------------------------
	 * Bool to check if Blob Storage should be used
	 */
	private static $_useBlobStorage = null;
	private function useBlobStorage()
	{
		if(null === self::$_useBlobStorage) {
			self::$_useBlobStorage = boolval(getenv("use_blob_storage"));
		}
		return self::$_useBlobStorage;
	}

	/* -------------------------------------------------------------------------
	 * Get the Client Id if set
	 */
	private function getClientID()
	{
		return \Phalcon\Di::getDefault()->getSession()->get("auth-identity-client");
	}

	/* -------------------------------------------------------------------------
	 * path Check and Creation if not existing
	 */
	private function condCreateDirectory($dir)
	{
		if (!file_exists($dir)) {
			if (!mkdir($dir, 0777, true)) {
				// TODO: Exception Handling
				throw new \Exception("Directory could not be created", 500);
			}
		}
		return $dir;
	}

	/* -----------------------------------------------------------------------------------
	 * getClientDataPath
	 * returns client data base directory path
	 */
	private function getDataPath()
	{
		return SystemInfo::getDataPath();
	}

	/* -----------------------------------------------------------------------------------
	 * getClientDataPath
	 * returns client id based data path without Path to data base directory/container
	 */
	public function getClientDataPath($sysAdminPath, $subPath = "")
	{
		$path = $sysAdminPath . DIRECTORY_SEPARATOR;
		$path .= "client" . DIRECTORY_SEPARATOR;
		$path .= $this->getClientID() . DIRECTORY_SEPARATOR;
		if($subPath != "") $path .= $subPath . DIRECTORY_SEPARATOR;
		return str_replace(["/","\\"], DIRECTORY_SEPARATOR , $path);
	}

	/* -----------------------------------------------------------------------------------
	 * getClientDataPath
	 * returns client id based data path without Path to data base directory/container
	 */
	public function getSiteDataPath($sysAdminPath, $site_id, $subPath = "")
	{
		$path = $sysAdminPath . DIRECTORY_SEPARATOR;
		$path .= "site" . DIRECTORY_SEPARATOR;
		$path .= $site_id . DIRECTORY_SEPARATOR;
		if($subPath != "") $path .= $subPath . DIRECTORY_SEPARATOR;
		return str_replace(["/","\\"], DIRECTORY_SEPARATOR , $path);
	}

	/* -----------------------------------------------------------------------------------
	 * getClientPDFPath
	 * returns client based pdf path
	 */
	public function getClientPDFPath($sysAdminPath)
	{
		return $this->getClientDataPath($sysAdminPath, "pdf");
	}

	/* -----------------------------------------------------------------------------------
	 * getClientInvoDocPath
	 * returns client based invo doc path
	 */
	public function getClientInvoDocPath($sysAdminPath)
	{
		$pdfPath = $this->getClientPDFPath($sysAdminPath);
		$path = $pdfPath . "invo_doc" . DIRECTORY_SEPARATOR;
		return $path;
	}

	/* -----------------------------------------------------------------------------------
	 * getClientImagePath
	 * returns client based image path
	 */
	public function getClientImagePath($sysAdminPath)
	{
		return $this->getClientDataPath($sysAdminPath, "images");
	}

	/* -----------------------------------------------------------------------------------
	 * getClientICSPath
	 * returns client based path for ics files
	 */
	public function getClientICSPath($sysAdminPath)
	{
		return $this->getClientDataPath($sysAdminPath, "ics_files");
	}

	/* -----------------------------------------------------------------------------------
	 * getTempPath
	 * returns sys admin path based path for temporary files
	 */
	public function getTempPath()
	{
		$temppath  = $this->getDataPath();
		$temppath .= "temp" . DIRECTORY_SEPARATOR;
		$temppath .= $this->getClientID() . DIRECTORY_SEPARATOR;
		return $this->condCreateDirectory($temppath);
	}

	/* -------------------------------------------------------------------------
	 *
	 */
	public function fileExists($file)
	{
		$dir = $this->getDataPath();
		$filename = $dir.$file;
		if(file_exists($filename)) {
			return true;
		} else {
			if($this->useBlobStorage()) {
				$blobStorage = $this->getBlobStorage();
				$container = $this->getContainer();
				return $blobStorage->blobExists($container, $file);
			}
			return false;
		}
	}

	/* -------------------------------------------------------------------------
	 * Saves a file from temporary folder to a specified location either in Azure Storage or locally
	 */
	 // TODO: maybe add Exception throws
	public function saveFileFromTemp($filename, $moveTo)
	{
		$path = $this->getTempPath();
		$fileFrom = $path.$filename;

		if(file_exists($fileFrom)) {
			if($this->useBlobStorage()) {
				$blobStorage = $this->getBlobStorage();
				$blobContainer = $this->getContainer();
				if(!$blobStorage->createBlob($blobContainer, $moveTo, $fileFrom)) {
					return false;
				}
			}

			$fileTo = $this->getDataPath() . $moveTo;
			// Create Directories if not already existing
			$this->condCreateDirectory(dirname($fileTo));
			return rename($fileFrom, $fileTo);
		} else {
			return false;
		}
	}

	/**
	 * Move a file from
	 * @param  $src - source filepath related to data path
	 * @param  $dst - destination filepath related to data path
	 * @return boolean
	 */
	public function moveFile($src, $dst)
	{
		$dir = $this->getDataPath();
		$sourcefile = $dir.$src;
		$destfile = $dir.$dst;

		if($this->getFile($src)) {
			if($this->useBlobStorage()) {
				$blobStorage = $this->getBlobStorage();
				$blobContainer = $this->getContainer();
				if(!$blobStorage->createBlob($blobContainer, $dst, $sourcefile)) {
					// TODO: error handling?
					return false;
				}
				$blobStorage->deleteBlob($blobContainer, $src);
			}

			$this->condCreateDirectory(dirname($destfile));
			return rename($sourcefile, $destfile);
		} else {
			return false;
		}
	}

	/* -------------------------------------------------------------------------
	 * Uses the phalcon file moveTo method to move a file to the designated destination
	 * Also checks for use of blob storage and uploads the file to blob storage
	 */
	public function phalconFileMoveTo($phalconfile, $dest)
	{
		$dir = $this->getDataPath();
		$destPath = $dir . $dest;
		$this->condCreateDirectory(dirname($destPath));

		if(!$phalconfile->moveTo($destPath)) {
			return false;
		}

		if($this->useBlobStorage()) {
			$blobStorage = $this->getBlobStorage();
			$blobContainer = $this->getContainer();

			if(!$blobStorage->createBlob($blobContainer, $dest, $destPath)) {
				return false;
			}
		}

		return true;
	}

	/* -------------------------------------------------------------------------
	 * Deletes a file from blob storage and data folder
	 */
	public function deleteFile($file)
	{

		$dir = $this->getDataPath();
		$filename = $dir.$file;

		if($this->useBlobStorage()) {
			$blobStorage = $this->getBlobStorage();
			$container = $this->getContainer();
			if(!$blobStorage->deleteBlob($container, $file)) {
				return false;
			}
		}

		if(!file_exists($filename)) {
			return true;
		} else {
			return unlink($filename);
		}

	}

	/**
	 * Get a full file path and download file from Azure Storage if needed
	 * @param  string $file filepath related to data path
	 * @return string full data path to file or empty String if not existing
	 */
	public function getFile($file)
	{
		$dir = $this->getDataPath();
		$filename = $dir.$file;

		if(!file_exists($filename)) {
			if($this->useBlobStorage()) {
				// Check if directory exists and create it if not:
				$this->condCreateDirectory(dirname($filename));

				$blobStorage = $this->getBlobStorage();
				$container = $this->getContainer();
				if($blobStorage->getBlob($container, $file, $filename)) {
					return $filename;
				} else {
					return "";
				}
			} else {
				return "";
			}
		} else {
			return $filename;
		}
	}

}
