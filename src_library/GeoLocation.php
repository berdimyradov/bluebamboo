<?php 

/**
 * utility for IP lookup
 */

namespace GSCLibrary;

class GeoLocation
{
	public static function getCountry ($user_ip) {
		try {
			if ($user_ip == "127.0.0.1") {
				$user_ip = "80.218.11.243";
			}
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => "http://api.wipmania.com/" . $user_ip,
			));
			$user_country = curl_exec($curl);
			curl_close($curl);
			return $user_country;
		}
		catch (\Exception $err) {
			return null;
		}
	}
	
}
