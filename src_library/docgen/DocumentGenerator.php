<?php

namespace DocumentGenerator;

/*
 * für Prüfung/Formatierung JSON:
 * https://jsonformatter.curiousconcept.com
 *
 */

class DocumentGenerator {

	// File Switch for Blob Storage Handling
	private static $_fileSwitch;
	public static function getFileSwitch()
	{
		if(empty(self::$_fileSwitch)) {
			self::$_fileSwitch = new FileSwitch();
			if(empty(getenv("blob_storage_container_data"))) {
				$container = "data";
			}
			else {
				$container = getenv("blob_storage_container_data");
			}
			self::$_fileSwitch->setContainer($container);
		}
		return self::$_fileSwitch;
	}

    /* -----------------------------------------------------------------------------------
     * constructor
     */
	public function __construct() {
		require_once('DocumentStyle.php');
		require_once('BorderStyle.php');
		require_once('FontStyle.php');
		require_once('JsonDocument.php');
		require_once('JsonPDF.php');
		require_once('JsonTCPDF.php');
		require_once('FileSwitch.php');
	}

    /* -----------------------------------------------------------------------------------
     * Layout Data
     */
	private $_layout;

    public function setLayout(string $layout_as_json) {
        $this->_layout = json_decode($layout_as_json);
    }

    public function getLayout() {
        return $this->_layout;
    }

    /* -----------------------------------------------------------------------------------
     * Content Data
     */
	private $_content;

    public function setContent(string $content_as_json) {
        $this->_content = json_decode($content_as_json);
    }

    public function getContent() {
        return $this->_content;
    }

    /* -----------------------------------------------------------------------------------
     * Create a PDF with the given Style and Content in the given Directory dir
     */
	 public function createPDF(string $file, string $content = "", string $layout = "") {
		try {

			if(empty($layout)) {
				$layout = $this->getLayout();
			} else {
				$layout = json_decode($layout);
			}
			if(empty($layout)) {
				throw new \Exception("No Layout defined!");
			}

			if(empty($content)) {
				$content = $this->getContent();
			} else {
				$content = json_decode($content);
			}
			if(empty($content)) {
				throw new \Exception("No Content defined!");
			}

			// Content and Layout set -> create PDF!
			$document = new JsonPDF($layout, $content);
			if($document->Output($file)) {
				return true;
			} else {
				throw new \Exception("Error during PDF Output.");
			}
		}
		catch (\Exception $e) {
			error_log("Error generating PDF Page with DocumentGenerator: " . $e->getMessage());
			return false;
		}
	}

    /* -----------------------------------------------------------------------------------
     * Create a Docx Document with the given Style and Content in the given Directory dir
     */
    public function createDOCX($file) {
        // TODO
    }
}

?>
