<?php

namespace DocumentGenerator;

/**
 * Mother Class for all Json-Documents
 *
 * @author Meik
 */
abstract class JsonDocument {

    protected $document;

	// Layout
	protected $_layout;
	protected function setLayout($layout)
	{
		$this->_layout = json_decode(json_encode($layout));
		$this->setElements($layout->elements)
        	 ->createStyles($layout);
		return $this;
	}

	protected $_elements;
	protected function setElements($elements)
	{
		$this->_elements = json_decode(json_encode($elements));
		return $this;
	}
	protected function getElements()
	{
		if(empty($this->_elements)) {
			return new \stdClass();
		}
		return $this->_elements;
	}

	// Content
    protected $_vars;
	protected function setVars($vars)
	{
		if(empty($vars)) {
			$this->_vars = new \stdClass();
		} else {
			$this->_vars = json_decode(json_encode($vars));
		}
		return $this;
	}
	protected function getVars()
	{
		return $this->_vars;
	}

    protected $_tablevars;
	protected function setTablevars($vars)
	{
		if(empty($vars)) {
			$this->_tablevars = new \stdClass();
		} else {
			$this->_tablevars = json_decode(json_encode($vars));
		}
		return $this;
	}
	protected function getTablevars()
	{
		return $this->_tablevars;
	}

	/* -------------------------------------------------------------------------
	 * Get the dynamic Table content vars Array by Name
	 */
	private function getDynamicTableVars($name) {
		return isset($this->getTablevars()->$name) ? $this->getTablevars()->$name : false;
	}

	protected $_docSettings;
	protected function setDocSettings($docsettings)
	{
		if(empty($docsettings)) {
			$settings = new \stdClass();
			$settings->author = "Blue Bamboo";
			$settings->title = "";
			$settings->producer = "BlueBamboo PDF Creator";
			$settings->creator = "BlueBamboo PDF Creator";
		} else {
			$settings = json_decode(json_encode($docsettings));
		}
		$this->_docSettings = $settings;
		return $this;
	}
	protected function getDocSettings()
	{
		return $this->_docSettings;
	}

	protected function setContent($content)
	{
		$this->setVars($content->values)
			 ->setTablevars($content->dynamicTableContent)
			 ->setDocSettings($content->documentSettings);
		return $this;
	}

	// FileSwitch
	protected static $_fileSwitch;
	protected function getFileSwitch()
	{
		if(empty(self::$_fileSwitch)) {
			self::$_fileSwitch = DocumentGenerator::getFileSwitch();
		}
		return self::$_fileSwitch;
	}

    /* -----------------------------------------------------------------------------------
     * constructor
     */
    public function __construct($layout, $content) {
        $this->setLayout($layout)
			 ->setContent($content)
			 ->buildDoc();
    }

	/* -------------------------------------------------------------------------
	 * Styles
	 */

	// Fontstyles
	protected $_fontstyles = array();

    protected function addFontStyle($style = NULL)
	{
        $s = new FontStyle($style);
        array_push($this->_fontstyles, $s);
        return $s;
    }

    protected function getFontStyleByName($styleName)
	{
        $style = NULL;
        foreach ($this->_fontstyles as $s) {
            if($s->sName == $styleName) {
                $style = $s;
                break;
            }
        }
        if($style == NULL) {
            $style = new FontStyle();
        }
        return $style;
    }

	protected function getFontStyle($fontstyle)
	{
		if(is_string($fontstyle)) {
			return $this->getFontStyleByName($fontstyle);
		} else if(is_object($fontstyle)) {
			return new FontStyle($fontstyle);
		} else {
			return new FontStyle();
		}
	}

	// Borderstyles
	protected $_borderstyles = array();

    protected function addBorderStyle($style = NULL)
	{
        $s = new BorderStyle($style);
        array_push($this->_borderstyles, $s);
        return $s;
    }

    protected function getBorderStyleByName($styleName)
	{
        $style = NULL;
        foreach ($this->_borderstyles as $s) {
            if($s->sName == $styleName) {
                $style = $s;
                break;
            }
        }
        if($style == NULL) {
            $style = new BorderStyle();
        }
        return $style;
    }

	protected function getBorderStyle($style)
	{
	   if(is_string($style)) {
		   return $this->getBorderStyleByName($style);
	   } elseif(is_object($style)) {
		   return new BorderStyle($style);
	   } else {
		   return new BorderStyle();
	   }
	}

    // Create the Styles from the Layout File
    protected function createStyles($layout)
	{
        // Create Font Styles
        $this->_fontstyles = array();
		if(!empty($layout->fontstyles)) {
			foreach($layout->fontstyles as $style) {
	            $this->addFontStyle($style);
	        }
		}

        // Create Border Styles
        $this->borderstyles = array();
		if(!empty($layout->borderstyles)) {
	        foreach($layout->borderstyles as $style) {
	            $this->addBorderStyle($style);
	        }
		}
    }

    /* -----------------------------------------------------------------------------------
     * Replace the variables in the String with the corresponding Vars $vars
     */
    protected static function replaceContentVars($content, $vars) {
		/*
		$outstring = $content;
        $treffer = array();

        preg_match_all("/%([A-Za-z0-9_]+)%/", $content, $treffer, PREG_PATTERN_ORDER);

        foreach($treffer[1] as $treff) {
            if(isset($vars->$treff)) {
                $searchstring = "%".$treff."%";
                $outstring = str_replace($searchstring, $vars->$treff, $outstring);
            }
        }

        return $outstring;
		*/
		return self::replaceVars($content, $vars);
	}

	/* -----------------------------------------------------------------------------------
     * Replace the variables in the String with the corresponding Vars $vars for Dynamic Table Content
     */
    protected static function replaceContentVarsDT($content, $vars) {
		/*
		$outstring = $content;
        $treffer = array();

        preg_match_all("/#([A-Za-z0-9_]+)#/", $content, $treffer, PREG_PATTERN_ORDER);

        foreach($treffer[1] as $treff) {
            if(isset($vars->$treff)) {
                $searchstring = "#".$treff."#";
                $outstring = str_replace($searchstring, $vars->$treff, $outstring);
            }
        }

        return $outstring;
		*/
		return self::replaceVars($content, $vars, "#");
    }

	protected static function replaceVars($content, $vars, $identifier = "%") {
		$outstring = $content;
        $treffer = array();

		$matchstring = '/'.$identifier.'([A-Za-z0-9_]+)'.$identifier.'/';
        preg_match_all($matchstring, $content, $treffer, PREG_PATTERN_ORDER);

        foreach($treffer[1] as $treff) {
            if(isset($vars->$treff)) {
                $searchstring = $identifier.$treff.$identifier;
                $outstring = str_replace($searchstring, $vars->$treff, $outstring);
            }
        }

        return $outstring;
	}

	/* -------------------------------------------------------------------------
	 * Create Image Src from string src to handle Blob Storage contained files.
	 * returns: http-Url, server-Path or empty string if server-path-file not found
	 */
	protected function handleImageSrc(string $src) {
		if(substr($src, 0, 4) === "http") {
			return $src;
		} else {
			return $this->getFileSwitch()->getFile(str_replace("/", DIRECTORY_SEPARATOR , $src));
		}
	}

    /* -----------------------------------------------------------------------------------
     * Create Dynamic Table Element by replacing its Content Vars
     */
	protected static function createDynamicTableElement($templateobj, $contentobj) {
		if(!is_array($contentobj)) {
			return false;
		}
		if(empty($templateobj->content->rows)) {
			return false;
		}

		$ret = json_decode(json_encode($templateobj));

		$ret_rows = array();
		foreach($contentobj as $position) {
			$rows = json_decode(json_encode($templateobj->content->rows));
			foreach($rows as $row) {
				foreach($row->cells as $cell) {
					$cell->value = empty($cell->value) ? "" : self::replaceContentVarsDT($cell->value, $position);
				}
				$ret_rows[] = $row;
			}
		}

		$ret->content->rows = $ret_rows;

		return $ret;
	}
	/*
    protected static function replaceDynamicTableContent($content, $vars) {

		$out = array();
		$line = new \stdClass();
		$line->cells = array();
		$out[] = $line;
        $treffer = array();

        preg_match("/%([A-Za-z0-9_]+)%/", $content, $treffer);

		$treff = $treffer[1];

        if(isset($vars->$treff)) {
            $out = $vars->$treff;
        }

        return $out;


    }
	*/

    /* -----------------------------------------------------------------------------------
     * Build the document
     */
    protected function buildDoc() {
        return $this->startDoc()
        	 		->addElements();
    }

    /* -----------------------------------------------------------------------------------
     * Start the generation of a Document
     * Note: Has to be overwritten for all child classes
     */
    protected function startDoc() { return $this; }

	/* -----------------------------------------------------------------------------------
	 * Add the Elements from the layout to the document
	 * Note: Has to be overwritten for all child classes
	 */
	protected function addElements() {
		// Dem Dokument die Elemente hinzufügen:
		foreach($this->getElements() as $element)
		{
			if(isset($element->type)) {
				switch($element->type) {
					case "string":
						$this->addString($element);
						break;
					case "table":
						$this->addTable($element);
						break;
					case "image":
						$element->url = $this->handleImageSrc($element->url);
						$this->addImage($element);
						break;
					case "hline":
						$this->addHLine($element);
						break;
					case "rect":
						$this->addRect($element);
						break;
					case "datamatrix":
						$this->addDataMatrix($element);
						break;
					case "dynamictable":
						// Replace dynamic Table Content
						// $element->content->rows = self::replaceDynamicTableContent($element->content->rows, $this->tablevars);

						if(empty($element->content->rowcontent)) {
							throw new \Exception("No Rowcontent template object defined for dynamictable!");
						}

						$dynTableVars = $this->getDynamicTableVars($element->content->rowcontent);
						if($dynTableVars === false) {
							throw new \Exception("No Content Var Object defined for dynamictable!");
						}

						$table_elem = self::createDynamicTableElement($element, $dynTableVars);

						$this->addTable($table_elem);
						break;
				}
			}
		}

		return $this;
	}

    /* -----------------------------------------------------------------------------------
     * Add a Table to the Document
     * Note: Has to be overwritten for all child classes
     */
    protected function addTable($table) { return $this; }

    /* -----------------------------------------------------------------------------------
     * Add a String to the Document
     * Note: Has to be overwritten for all child classes
     */
    protected function addString($string) { return $this; }

    /* -----------------------------------------------------------------------------------
     * Add a Image to the Document
     * Note: Has to be overwritten for all child classes
     */
    protected function addImage($image) { return $this; }

	/* -----------------------------------------------------------------------------------
     * Add a hoizontal line to the Document
     * Note: Has to be overwritten for all child classes
     */
    protected function addHLine($line) { return $this; }

	/* -----------------------------------------------------------------------------------
     * Add a Rectangle to the Document
     * Note: Has to be overwritten for all child classes
     */
    protected function addRect($rect) { return $this; }

	/* -----------------------------------------------------------------------------------
	 * Add a Image to the Document
	 * Note: Has to be overwritten for all child classes
	 */
	protected function addDataMatrix($datamatrix) { return $this; }

    /* -----------------------------------------------------------------------------------
     * Save the Document in the given directory
     * Note: Has to be overwritten for all child classes
     */
    public function Output(string $file) { }
}
