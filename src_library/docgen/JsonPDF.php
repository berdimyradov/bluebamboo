<?php

namespace DocumentGenerator;

/**
 * Class for a Invoice PDF file Created with TCPDF
 *
 * @author Meik
 */

class JsonPDF extends JsonDocument {

    /* -----------------------------------------------------------------------------------
     * constructor
     */
    public function __construct($layout, $content) {
        parent::__construct($layout, $content);
    }

    /* -----------------------------------------------------------------------------------
     * Save the PDF in the given Directory
     */
    // Old: public function Output($dir = '', $filename = '')
	public function Output(string $file)
	{
		/*
        // Dateinamen und -Ort erstellen
        if($filename == '' || !is_string($filename)) {
            if(isset($this->getDocSettings()->filename)) {
                $filename = parent::replaceContentVars($this->getDocSettings()->filename, $this->getVars());
            } else {
                $filename = 'Outputdoc';
            }
        }

        if(substr_compare($filename, '.pdf', -4, 4, true) != 0) {
            $filename .= '.pdf';
        }

        $file = $dir . DIRECTORY_SEPARATOR . $filename;
		*/

        // Datei speichern
        $this->document->Output($file, 'F');
		return file_exists($file);
    }

    /* -----------------------------------------------------------------------------------
     * build the Document from the Layout and Content
     */
    protected function startDoc() {

        // PDF erstellen
        $this->document = new JsonTCPDF('', 'mm', 'A4');

        // Hier kommen noch die PDF-Dateiinformationen wie Autor etc.
        // set document information
        $this->document->SetAuthor(parent::replaceContentVars($this->getDocSettings()->author, $this->getVars()));
        $this->document->SetTitle(parent::replaceContentVars($this->getDocSettings()->title, $this->getVars()));
        $this->document->SetCreator($this->getDocSettings()->creator);
        $this->document->SetProducer($this->getDocSettings()->producer);

        // remove default header/footer
        $this->document->setPrintHeader(false);
        $this->document->setPrintFooter(false);
		$this->document->SetHeaderMargin(0);
		$this->document->SetFooterMargin(0);

        // set margins
        $this->document->SetMargins(0, 0, 0, true);

        // set auto page breaks
        $this->document->SetAutoPageBreak(true, 0);

        // Seite hinzufügen
        $this->document->AddPage();

		return $this;
    }

	protected function addPage() {
		$this->document->AddPage('', 'A4', true);
		return $this;
	}

	/* -----------------------------------------------------------------------------------
	 * Add a string to the document
	 */
	protected function addString($string) {

		// Font Style
		if(isset($string->fontstyle)) {
			$fontstyle = $this->getFontStyle($string->fontstyle);
		} else {
			$fontstyle = $this->getFontStyle('default');
		}

		// Border Style
		if(isset($string->borderstyle)) {
			$borderstyle = $this->getBorderStyle($string->borderstyle)->aTCPDFBorderStyle();
		} else {
			$borderstyle = 0;
		}

		// Style für das Dokument setzen
		// Stilvariablen erstellen:
		$fontfamily = $fontstyle->sFace;
		$fontsize = $fontstyle->iSize;
		$fontdesign = $fontstyle->sFontDesignTag();
		$fontfile = '';
		$fontsubset = false;
		$fontlineheight = $fontstyle->fLineHeight;
		$fontletterspacing = $fontstyle->fLetterSpacing;

		$this->document->SetFont($fontfamily, $fontdesign, $fontsize, $fontfile, $fontsubset);

		// Position erstellen
		$xpos = $string->location->x;
		$ypos = $string->location->y;

		// Breite der Zelle handhaben:
		$stringWidth = isset($string->width) ? (float)$string->width : 0;
// TODO: Handling für Ausrichtung an vorherigem Element oder Seite

		// Inhalt anpassen, Variablen ersetzen
		$content = parent::replaceContentVars($string->content, $this->getVars());

		// set cell padding
		$this->document->setCellPaddings(0, 0, 0, 0);

		// set cell margins
		$this->document->setCellMargins(0, 0, 0, 0);

		// set fill color for cell
		$fillcolor = $fontstyle->aBGColor;
		if($fillcolor['R'] + $fillcolor['G'] + $fillcolor['B'] == 765) {
			$printBG = false;
		} else {
			$this->document->SetFillColor((int)$fillcolor['R'], (int)$fillcolor['G'], (int)$fillcolor['B']);
			$printBG = true;
		}

		// set text alignment
		$textalign = isset($string->textalign) ? self::textalignTag($string->textalign) : '';

		// set line height
		$this->document->setCellHeightRatio($fontlineheight);

		// set font letter spacing
		$this->document->setFontSpacing($fontletterspacing);

		if($stringWidth > 0) {
			// Cut content text if it exeeds line width
			$content = $this->cutTextToWidth($content, $stringWidth);
		}

		// Text zum Dokument hinzufügen
		$this->document->MultiCell($stringWidth, 0, $content, $borderstyle, $textalign, $printBG, 1, $xpos, $ypos, true, 0, false, true, 0, 'T', false);

		return $this;
	}

	/* -----------------------------------------------------------------------------------
	 * Add a string to the document
	 */
	protected function addHLine($line) {

		// Positionen erstellen
		$xpos = (float)$line->location->x;
		$ypos = (float)$line->location->y;

		$length = isset($line->length) ? (float)$line->length : 210.0 - $xpos;
		$xposEnd = $xpos + $length;
		$yposEnd = $ypos;

		// Style für die Line
		$col = isset($line->color) ? $line->color : "#000";
		$color = DocumentStyle::aTCPDFColorConversion($col);
		$width = isset($line->width) ? $line->width : 0.2;
		$style = array(
			"width" => $width,
			"cap" => "butt",
			"join" => "miter",
			"dash" => 0,
			"phase" => 0,
			"color" => $color,
		);

		// Linie zum Dokument hinzufügen
		$this->document->Line($xpos, $ypos, $xposEnd, $yposEnd, $style);

		return $this;
	}

	/* -----------------------------------------------------------------------------------
	 * Add a rectangular to the document
	 */
	protected function addRect($rect) {

		// Font Style
		if(isset($rect->fontstyle)) {
			$fontstyle = $this->getFontStyle($rect->fontstyle);
		} else {
			$fontstyle = $this->getFontStyle('default');
		}

		// Border Style
		if(isset($rect->borderstyle)) {
			$borderstyle = $this->getBorderStyle($rect->borderstyle)->aTCPDFBorderStyle();
		} else {
			$borderstyle = 0;
		}

		// Style für das Dokument setzen
		// Stilvariablen erstellen:
		$fontfamily = $fontstyle->sFace;
		$fontsize = $fontstyle->iSize;
		$fontdesign = $fontstyle->sFontDesignTag();
		$fontfile = '';
		$fontsubset = false;
		$fontlineheight = $fontstyle->fLineHeight;
		$fontletterspacing = $fontstyle->fLetterSpacing;

		$this->document->SetFont($fontfamily, $fontdesign, $fontsize, $fontfile, $fontsubset);

		// Position erstellen
		$xpos = $rect->location->x;
		$ypos = $rect->location->y;

		// Breite und Höhe der Zelle handhaben:
		$rectWidth = isset($rect->width) ? (float)$rect->width : 0;
		$rectHeight = isset($rect->height) ? (float)$rect->height : 0;
// TODO: Handling für Ausrichtung an vorherigem Element oder Seite

		// Inhalt anpassen, Variablen ersetzen
		if(isset($rect->content)) {
			$content = parent::replaceContentVars($rect->content, $this->getVars());
		} else {
			$content = "";
		}

		// set cell padding
		$this->document->setCellPaddings(0, 0, 0, 0);

		// set cell margins
		$this->document->setCellMargins(0, 0, 0, 0);

		// set fill color for cell
		$fillcolor = $fontstyle->aBGColor;
		if($fillcolor['R'] + $fillcolor['G'] + $fillcolor['B'] == 765) {
			$printBG = false;
		} else {
			$this->document->SetFillColor((int)$fillcolor['R'], (int)$fillcolor['G'], (int)$fillcolor['B']);
			$printBG = true;
		}

		// set text alignment
		$textalign = isset($rect->textalign) ? self::textalignTag($rect->textalign) : '';

		// set line height
		$this->document->setCellHeightRatio($fontlineheight);

		// set font letter spacing
		$this->document->setFontSpacing($fontletterspacing);

		if($rectWidth > 0) {
			// Cut content text if it exeeds line width
			$content = $this->cutTextToWidth($content, $rectWidth);
		}

		// Text zum Dokument hinzufügen
		$this->document->MultiCell($rectWidth, $rectHeight, $content, $borderstyle, $textalign, $printBG, 1, $xpos, $ypos, true, 0, false, true, 0, 'T', false);

		return $this;
	}

	/* -----------------------------------------------------------------------------------
	 * Add a table to the document
	 */
	protected function addTable($table) {

		$colwidths = explode(',', $table->colwidth);

		if(isset($table->fontstyle)) {
			$tablefontstyle = $this->getFontStyle($table->fontstyle);
		} else {
			$tablefontstyle = NULL;
		}

		if(isset($table->borderstyle)) {
			$tableborderstyle = $this->getBorderStyle($table->borderstyle)->aTCPDFBorderStyle();
		} else {
			$tableborderstyle = NULL;
		}

		if(isset($table->textalign)) {
			$textalign = self::textalignTag($table->textalign);
		} else {
			$textalign = '';
		}

		// Pointer auf die Startposition der Tabelle setzen:
// TODO: Positionen relativ oder absolute erstellen
		$xpos = $table->location->x;
		$ypos = $table->location->y;
		$this->document->SetAbsXY($xpos,$ypos);

		// Header ausgeben:
		if(isset($table->header)) {
			$this->addTableRow($table->header, $colwidths, $xpos, $tablefontstyle, $tableborderstyle, $textalign);
		}

		// Inhalt ausgeben:

		// Abfrage nach spezifischen Stilen
		if(isset($table->content->fontstyle)) {
			$contentfontstyle = $this->getFontStyle($table->content->fontstyle);
		} else {
			$contentfontstyle = $tablefontstyle;
		}

		if(isset($table->content->borderstyle)) {
			$contentborderstyle = $this->getBorderStyle($table->content->borderstyle)->aTCPDFBorderStyle();
		} else {
			$contentborderstyle = $tableborderstyle;
		}

		if(isset($table->content->textalign)) {
			$contenttextalign = self::textalignTag($table->textalign);
		} else {
			$contenttextalign = '';
		}

		// Tabellenreihen ausgeben
		foreach($table->content->rows as $row) {
			$this->addTableRow($row, $colwidths, $xpos, $contentfontstyle, $contentborderstyle, $contenttextalign);
		}

		// Footer ausgeben:
		if(isset($table->footer)) {
			$this->addTableRow($table->footer, $colwidths, $xpos, $tablefontstyle, $tableborderstyle, $textalign);
		}

		return $this;
	}

	/* -----------------------------------------------------------------------------------
	 * Add a table-row to the document
	 */
	protected function addTableRow($row, $colwidths, $startposx = 0, $tablefontstyle = NULL, $tableborderstyle = NULL, $textalign = '') {

		// Check for specific Rowstyle for Font
		if(isset($row->fontstyle)) {
			$rowfontstyle = $this->getFontStyle($row->fontstyle);
		} else {
			$rowfontstyle = $tablefontstyle;
		}

		// Check for specific Rowstyle for Border
		if(isset($row->borderstyle)) {
			$rowborderstyle = $this->getBorderStyle($row->borderstyle)->aTCPDFBorderStyle();
		} else {
			$rowborderstyle = $tableborderstyle;
		}

		// Check for text-alignment
		if($textalign != '') {
			$textalign = self::textalignTag($textalign);
		}

		// Set Row starting Position
		$this->document->SetAbsX($startposx);

		// Add the Cells
		$i = 0;
		foreach($row->cells as $cell) {
			if(isset($cell->colspan)) {
				$span = $cell->colspan;
				$cellwidth = 0;
				for($j = 0; $j < $span; $j++) {
					$cellwidth += $colwidths[$i];
					$i++;
				}
			} else {
				$cellwidth = $colwidths[$i];
				$i++;
			}
			$this->addTableCell($cell, $cellwidth, $rowfontstyle, $rowborderstyle, $textalign);
		}

		// Perform a line break
		$this->document->Ln();

		return $this;
	}

	protected function addTableCell($cell, $cellwidth, $fontstyle = NULL, $borderstyle = NULL, $textalign = '') {

		// Font Style
		if(isset($cell->fontstyle)) {
			$fontstyle = $this->getFontStyle($cell->fontstyle);
		} elseif ($fontstyle == NULL) {
			$fontstyle = $this->getFontStyle('default');
		}

		// Border Style
		if(isset($cell->borderstyle)) {
			$borderstyle = $this->getBorderStyle($cell->borderstyle)->aTCPDFBorderStyle();
		} elseif ($borderstyle == NULL) {
			$borderstyle = 0;
		}

		// Textausrichtung
		if(isset($cell->textalign)) {
			$textalign = self::textalignTag($cell->textalign);
		} elseif ($textalign != '') {
			$textalign = self::textalignTag($textalign);
		}

		// Style für das Dokument setzen
		// Stilvariablen erstellen:
		$fontfamily = $fontstyle->sFace;
		$fontsize = $fontstyle->iSize;
		$fontdesign = $fontstyle->sFontDesignTag();
		$fontfile = '';
		$fontsubset = false;
		$fontlineheight = $fontstyle->fLineHeight;
		$fontletterspacing = $fontstyle->fLetterSpacing;

		$this->document->SetFont($fontfamily, $fontdesign, $fontsize, $fontfile, $fontsubset);


		if(empty($cell->value)) {
			$cell->value = "";
		}
		// Inhalt anpassen, Variablen ersetzen
		$content = parent::replaceContentVars($cell->value, $this->getVars());

		// set cell padding
		$this->document->setCellPaddings(0,0,0,0);

		$fillcolor = $fontstyle->aBGColor;
		if($fillcolor['R'] + $fillcolor['G'] + $fillcolor['B'] == 765) {
			$printBG = false;
		} else {
			$this->document->SetFillColor((int)$fillcolor['R'], (int)$fillcolor['G'], (int)$fillcolor['B']);
			$printBG = true;
		}

		// set line height
		$this->document->setCellHeightRatio($fontlineheight);

		// set font letter spacing
		$this->document->setFontSpacing($fontletterspacing);

		// Cut content text if it exeeds line width
		$content = $this->cutTextToWidth($content, $cellwidth);

		// Text zum Dokument hinzufügen
		$this->document->Cell($cellwidth, 0, $content, $borderstyle, 0, $textalign, $printBG, '', 0, false, 'T', 'C');

		return $this;
	}


	/* -----------------------------------------------------------------------------------
	 * Add a image to the document
	 */
	protected function addImage($image) {
		$src = $image->url;
		$posx = $image->location->x;
		$posy = $image->location->y;
		$width = $image->width;
		$height = $image->height;

		$this->document->Image($src, $posx, $posy, $width, $height);

		return $this;
	}

	/* -----------------------------------------------------------------------------------
	 * Add a datamatrix code to the document
	 */
	protected function addDataMatrix($datamatrix) {

		// Position erstellen
		$xpos = $datamatrix->location->x;
		$ypos = $datamatrix->location->y;

		// Width and height
		$width = isset($datamatrix->width) ? (float)$datamatrix->width : 0;
		$height = isset($datamatrix->height) ? (float)$datamatrix->height : 0;

		// Inhalt anpassen, Variablen ersetzen
		$content = parent::replaceContentVars($datamatrix->content, $this->getVars());

		$style = array(
			'border' => false,
			'padding' => 0,
			'vpadding' => 0,
			'hpadding' => 0,
			'fgcolor' => array(0,0,0),
			'bgcolor' => false, //array(255,255,255)
			'module_width' => 1, // width of a single module in points
			'module_height' => 1 // height of a single module in points
		);

		$this->document->write2DBarcode($content,'DATAMATRIX',$xpos,$ypos,$width,$height,$style,'N');

		return $this;
	}

	/* -----------------------------------------------------------------------------------
	 * Create a alignment string for TCPDF
	 */
	protected static function textalignTag($textalign) {
		$string = '';
		switch($textalign) {
			case 'right':
				$string = 'R';
				break;
			case 'center':
				$string = 'C';
				break;
			case 'middle':
				$string = 'C';
				break;
			case 'justify':
				$string = 'J';
				break;
		}
		return $string;
	}

	/* -------------------------------------------------------------------------
	 * Calculate the width of each line of a text and return clipped text with
	 * ellipsis at end of each line if line is too long
	 */
	protected function cutTextToWidth($text, $width, $addEllipsis = true) {
		$pieces = explode("\n", $text);
		$ret = "";
		foreach($pieces as $piece) {
			$ret .= $this->cutStringToWidth(trim($piece), $width, $addEllipsis);
			$ret .= "\n";
		}
		return trim($ret);
	}

	/* -------------------------------------------------------------------------
	 * Calculate a String width and return clipped string with ellipsis if String too long
	 */
	protected function cutStringToWidth($string, $width, $addEllipsis = true) {
		$doc = $this->document;
		if($doc->GetStringWidth($string) >= $width) {
			$teststring = $string;
			if($addEllipsis) {
				while($doc->GetStringWidth($teststring."...") >= $width) {
					$teststring = substr($teststring, 0, -1);
				}
				return $teststring . "...";
			} else {
				while($doc->GetStringWidth($teststring) >= $width) {
					$teststring = substr($teststring, 0, -1);
				}
				return $teststring;
			}
		} else {
			return $string;
		}
	}

}
