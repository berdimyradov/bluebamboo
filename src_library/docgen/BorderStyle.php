<?php

namespace DocumentGenerator;

/**
 * Border Style for an Invoice Element
 *
 * @author Meik
 */
class BorderStyle extends DocumentStyle {

    public $fWidthTop = 0;
    public $fWidthLeft = 0;
    public $fWidthRight = 0;
    public $fWidthBottom = 0;
    public $sColor = "#fff";

    /* -----------------------------------------------------------------------------------
     * constructor
     */
    public function __construct($style = NULL) {

        parent::__construct($style);

        if(isset($style->width)) {
            $this->fWidthTop = $style->width;
            $this->fWidthLeft = $style->width;
            $this->fWidthRight = $style->width;
            $this->fWidthBottom = $style->width;
        }

        if(isset($style->top)) {
            $this->fWidthTop = $style->top->width;
        }

        if(isset($style->left)) {
            $this->fWidthLeft = $style->left->width;
        }

        if(isset($style->right)) {
            $this->fWidthRight = $style->right->width;
        }

        if(isset($style->bottom)) {
            $this->fWidthBottom = $style->bottom->width;
        }

        if(isset($style->color)) {
            $this->sColor = $style->color;
        }
    }

    /* -----------------------------------------------------------------------------------
     * Border Style array for TCPDF Cells
     */
    public function aTCPDFBorderStyle() {
        $borderStyle = array();
        $color = parent::aTCPDFColorConversion($this->sColor);

        if($this->fWidthTop > 0) $borderStyle['T'] = array('width' => $this->fWidthTop, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'color' => $color);
        if($this->fWidthLeft > 0) $borderStyle['L'] = array('width' => $this->fWidthLeft, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'color' => $color);
        if($this->fWidthBottom > 0) $borderStyle['B'] = array('width' => $this->fWidthBottom, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'color' => $color);
        if($this->fWidthRight > 0) $borderStyle['R'] = array('width' => $this->fWidthRight, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'color' => $color);

        return $borderStyle;
    }

}
