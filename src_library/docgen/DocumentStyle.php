<?php

namespace DocumentGenerator;

/**
 * Parent Class for Document Style Classes
 *
 * @author Meik
 */
class DocumentStyle {

    protected static $iNr = 1;
    public $sName = "";

    /* -----------------------------------------------------------------------------------
     * constructor
     */
    public function __construct($style = NULL) {
        if(isset($style->name)) {
            $this->sName = $style->name;
        }
        else {
            $this->sName = "Style".self::$iNr++;
        }
    }

    public static function aTCPDFColorConversion($sColor) {
		$retArr = array(
			'R' => 0,
			'G' => 0,
			'B' => 0,
		);
		$split = array();

		if(preg_match("/\#([a-fA-F0-9]{6})/", $sColor, $split)) {
			$color = $split[1];
			$retArr['R'] = hexdec(substr($color,0,2));
 			$retArr['G'] = hexdec(substr($color,2,2));
			$retArr['B'] = hexdec(substr($color,4,2));
		}
		elseif(preg_match("/\#([a-fA-F0-9]{3})/", $sColor, $split)) {
			$color = $split[1];
			$retArr['R'] = hexdec(substr($color,0,1).substr($color,0,1));
			$retArr['G'] = hexdec(substr($color,1,1).substr($color,1,1));
			$retArr['B'] = hexdec(substr($color,2,1).substr($color,2,1));
		}

		return $retArr;
    }
}
