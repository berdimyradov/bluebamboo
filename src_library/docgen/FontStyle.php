<?php

namespace DocumentGenerator;

/**
 * Font Style for an Invoice Element
 *
 * @author Meik
 */
class FontStyle extends DocumentStyle {

    public $sFace = "helvetica";
    public $iSize = 11;
    public $bBold = false;
    public $bItalic = false;
    public $bUnderlined = false;
    public $sColor = "#000";
	public $aColor;
	public $sBGColor = "#FFF";
	public $aBGColor;
	public $fLineHeight = 1.25;
	public $fLetterSpacing = 0.0;

	/* -----------------------------------------------------------------------------------
	 * constructor
	 */
	public function __construct($style = NULL) {

		parent::__construct($style);

		if($style != NULL) {
			$this->sFace = $style->face;
			$this->iSize = $style->size;
			$this->sColor = $style->color;

			$this->bBold = isset($style->bold) ? $style->bold : false;
			$this->bItalic = isset($style->italic) ? $style->italic : false;
			$this->bUnderlined = isset($style->underlined) ? $style->underlined : false;
			$this->sBGColor = isset($style->backgroundcolor) ? $style->backgroundcolor : "#FFF";

			$this->fLineHeight = isset($style->lineheight) ? $style->lineheight : 1.25;
			$this->fLetterSpacing = isset($style->letterspacing) ? $style->letterspacing : 0.0;
		}

		$this->aColor = parent::aTCPDFColorConversion($this->sColor);
		$this->aBGColor = parent::aTCPDFColorConversion($this->sBGColor);
	}

    /* -----------------------------------------------------------------------------------
     * Style String for TCPDF Fonts
     */
    public function sFontDesignTag() {
        $string = '';
        if($this->bBold) {
            $string .= 'B';
        }
        if($this->bItalic) {
            $string .= 'I';
        }
        if($this->bUnderlined) {
            $string .= 'U';
        }
        return $string;
    }

}
