<?php

namespace DocumentGenerator;

/*
 * Azure Blob Storage FileSwitch simple Version for Document Generator
 */

class FileSwitch {

 	private $_container = "data";
 	public function getContainer()
 	{
 		return $this->_container;
 	}
 	public function setContainer($container)
 	{
 		$this->_container = $container;
 	}

 	/* -------------------------------------------------------------------------
 	 * Blob Storage Service
 	 */
 	private static $_blobStorage;
 	private function getBlobStorage()
 	{
 		if(empty(self::$_blobStorage)) {
 			self::$_blobStorage = new \GSCLibrary\GSCAzureBlobStorage();
 		}
 		return self::$_blobStorage;
 	}

 	/* -------------------------------------------------------------------------
 	 * Bool to check if Blob Storage should be used
 	 */
 	private static $_useBlobStorage;
 	private function useBlobStorage()
 	{
		if(!class_exists('\GSCLibrary\GSCAzureBlobStorage')) {
			self::$_useBlobStorage = false;
		}
		if(!isset(self::$_useBlobStorage)) {
 			self::$_useBlobStorage = getenv("use_blob_storage");
 		}
 		return self::$_useBlobStorage;
 	}

 	/* -------------------------------------------------------------------------
 	 * path Check and Creation if not existing
 	 */
 	private function condCreateDirectory ($dir)
 	{
 		if (!file_exists($dir)) {
 			if (!mkdir($dir, 0777, true)) {
 				// TODO: Exception Handling
 				throw new \Exception("Directory could not be created", 500);
 			}
 		}
 		return $dir;
 	}

 	/* -----------------------------------------------------------------------------------
 	 * getClientDataPath
 	 * returns client data base directory path
 	 */
 	private function getDataPath()
 	{
		$data_path = getenv("data_path") . DIRECTORY_SEPARATOR;
		return str_replace(["/","\\"], DIRECTORY_SEPARATOR , $data_path);
 	}

 	/* -------------------------------------------------------------------------
 	 * File exists pendant for BlobStorage
 	 */
 	public function fileExists($file)
 	{
 		$dir = $this->getDataPath();
 		$filename = $dir.$file;
 		if(file_exists($filename)) {
 			return true;
 		} else {
 			if($this->useBlobStorage()) {
 				$blobStorage = $this->getBlobStorage();
 				$container = $this->getContainer();
 				return $blobStorage->blobExists($container, $file);
 			}
 			return false;
 		}
 	}

 	/**
 	 * Move a file from
 	 * @param  $src - source filepath related to data path
 	 * @param  $dst - destination filepath related to data path
 	 * @return boolean
 	 */
 	public function moveFile($src, $dst)
 	{
 		$dir = $this->getDataPath();
 		$sourcefile = $dir.$src;
 		$destfile = $dir.$dst;

 		if($this->getFile($src)) {
 			if($this->useBlobStorage()) {
 				$blobStorage = $this->getBlobStorage();
 				$blobContainer = $this->getContainer();
 				if(!$blobStorage->createBlob($blobContainer, $dst, $sourcefile)) {
 					// TODO: error handling?
 					return false;
 				}
 				$blobStorage->deleteBlob($blobContainer, $src);
 			}

 			$this->condCreateDirectory(dirname($destfile));
 			return rename($sourcefile, $destfile);
 		} else {
 			return false;
 		}
 	}

 	/* -------------------------------------------------------------------------
 	 * Deletes a file from blob storage and data folder
 	 */
 	public function deleteFile($file)
 	{
 		$dir = $this->getDataPath();
 		$filename = $dir.$file;

 		if($this->useBlobStorage()) {
 			$blobStorage = $this->getBlobStorage();
 			$container = $this->getContainer();
 			if(!$blobStorage->deleteBlob($container, $file)) {
 				return false;
 			}
 		}

 		if(!file_exists($filename)) {
 			return true;
 		} else {
 			return unlink($filename);
 		}
 	}

 	/**
 	 * Get a full file path and download file from Azure Storage if needed
 	 * @param  $file - filepath related to data path
 	 * @return full data path to file | empty String if not existing
 	 */
 	public function getFile($file)
 	{
 		$dir = $this->getDataPath();
 		$filename = $dir.$file;

 		if(!file_exists($filename)) {
 			if($this->useBlobStorage()) {
 				// Check if directory exists and create it if not:
 				$this->condCreateDirectory(dirname($filename));

 				$blobStorage = $this->getBlobStorage();
 				$container = $this->getContainer();
 				if($blobStorage->getBlob($container, $file, $filename)) {
 					return $filename;
 				} else {
 					return "";
 				}
 			} else {
 				return "";
 			}
 		} else {
 			return $filename;
 		}
 	}

 }

?>
