<?php

/* ---------------------------------------------------------------------------------------
 * generic controller
 */

namespace GSCLibrary;

class GSCAssetsIncludes extends \Phalcon\Mvc\Controller {

	const APP_VERSION = "2.4q";
	private $version = GSCAssetsIncludes::APP_VERSION;
	private $scope = "admin";
	private $editMode = false;
	private $cssVersion = 0;
	private $angularVersion = 1;

	/* -----------------------------------------------------------------------------------
	 * init
	 */
	public function init ($scope, $angularVersion = 1, $editMode = false, $cssVersion = 0) {
		$this->scope = $scope;
		$this->editMode = $editMode;
		$this->cssVersion = $cssVersion;
		$this->angularVersion = $angularVersion;
	}

	/* -----------------------------------------------------------------------------------
	 * set edit mode js context for edit mode scripts
	 */
	public function setEditModeJSContext ($appContext, $siteId, $pageId, $pagePath, $pagePreviewPath) {
		if ($this->scope != "portal") return "";
		if (!$this->editMode) return "";

		return <<<EOT
		<script>
			var app_context = "$appContext";
			var page_id = $pageId;
			var site_id = $siteId;
			var page_path = "$pagePath";
			var page_preview_path = "$pagePreviewPath";
			var edit_mode = "content";
		</script>
EOT;
	}

	/* -----------------------------------------------------------------------------------
	 * embed google analytics
	 */
	public function googleTracking ($trackingGA) {
		if ($this->editMode) return "";
		if ($trackingGA == null) return "";

		return <<<EOT
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		  ga('create', '$trackingGA', 'auto');
		  ga('send', 'pageview');
		</script>
EOT;
	}

	/* -----------------------------------------------------------------------------------
	 * embed facebook analytics
	 */
	public function facebookTracking ($trackingFB) {
		if ($this->editMode) return "";
		if ($trackingFB == null) return "";

		return <<<EOT
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '$trackingFB');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=$trackingFB&ev=PageView&noscript=1"/></noscript>
EOT;
	}

	/* -----------------------------------------------------------------------------------
	 * compute assets
	 */
	public function computeAssets ($css_path, $js_path) {
		$header = $this->assets->collection("header");
		$footer = $this->assets->collection("footer");
		$this->computeCssAssets($css_path, $js_path, $header, $footer);
		$this->computeJsAssets($css_path, $js_path, $header, $footer);
		$this->portalEditModeAssets($header, $footer);
	}

	/* -----------------------------------------------------------------------------------
	 * compute css assets
	 */
	protected function computeCssAssets ($css_path, $js_path, $header, $footer) {
		$header->addCss("/admin/css/font-awesome.min.css");

		if ($this->scope == "admin") {
			$header->addCss("/common/css/bootstrap-4v5.min.css");
			$header->addCss("/admin/css/sticky-footer-navbar.css");
		}
		if ($this->scope == "portal") {
			$header->addCss("/admin/css/bootstrap.min.css");




			$header->addCss("//fonts.googleapis.com/css?family=Open+Sans:300?v=" . $this->version);

			$header->addCss("/admin/css/bootstrap-theme.min.css?v=" . $this->version);
			$header->addCss("/admin/vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css?v=" . $this->version);

			$header->addCss("https://fonts.googleapis.com/css?family=Denk+One|Hind+Guntur:300,400,700|Open+Sans:300,400,400i,600|Slabo+27px|Tangerine:400,700");
			$header->addCss("https://fonts.googleapis.com/css?family=Carme|Droid+Sans");

			$header->addCss("../css/font-awesome.min.css");
			$header->addCss("/common/share/shariff.min.css");
			//$header->addCss("/common/css/mdb.min.css");
			if ($this->editMode) {
				$header->addCss("/css/main.css?version=" . $this->cssVersion);
				$header->addCss("https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.2/cropper.min.css");

				$header->addCss("/admin/css/wysiwyg2.css?v=" . $this->version);
				$header->addCss("/admin/css/main.css?v=" . $this->version);

				$header->addCss("//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css");
			}
			else {
				$header->addCss("/css/main.css?version=" . $this->cssVersion);
				$header->addCss("/admin/css/main.css?v=" . $this->version);
			}


		}

		//$header->addCss("/admin/css/ie10-viewport-bug-workaround.css");

		//<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flickity/1.1.2/flickity.min.css">
		/*
		<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:100,400,300,300italic,400italic,700' type='text/css'>
		<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato:400,100,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="/admin/css/font-awesome.min.css">
		<link rel="stylesheet" href="/admin/css/simpletextrotator.css">
		<!- -link rel="stylesheet" href="/admin/css/nested.css"- ->
		<link rel="stylesheet" href="/admin/css/animate.min.css">
		<link rel="stylesheet" href="/admin/vendor/xeditable/xeditable.min.css">
		*/

		//<!--link rel="stylesheet" href="/admin/css/main.css"-->

//	    $header->addCss("/admin/css/main.css");
		if ($this->scope == "admin") {
		    $header->addCss("/common/css/mdb.min.css");
		    $header->addCss("/admin/css/main.css");
		}

		$header->addCss("/common/calendar/fullcalendar.css");
		// $header->addCss("/common/calendar/fullcalendar.print.css");

		$header->addCss("/common/css/meik.css");

		if ($css_path != "") {
		    $header->addCss($css_path);
	    }

// "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"-->


	}

	/* -----------------------------------------------------------------------------------
	 * compute js assets
	 */
	protected function computeJsAssets ($css_path, $js_path, $header, $footer) {

        $footer->addJs("/admin/js/init_map.js?v=" . $this->version);

		if ($this->scope == "admin") {
			//$footer->addJs("/admin/vendor/jquery/dist/jquery.min.js?v=" . $this->version);
			//$header->addCss("/admin/vendor/jquery-ui-1.12.1/jquery-ui.css?v=" . $this->version);
			//$footer->addJs("/admin/vendor/jquery-ui-1.12.1/jquery-ui.js?v=" . $this->version);
			$footer->addJs("/admin/js/jquery.min.js?v=" . $this->version);
			$footer->addJs("/admin/js/jquery-ui.js?v=" . $this->version);
		}
		else {
			$footer->addJs("/admin/js/jquery.min.js?v=" . $this->version);
			$footer->addJs("/admin/js/jquery-ui.js?v=" . $this->version);
		}
		$footer->addJs("/admin/vendor/tether/dist/js/tether.min.js?v=" . $this->version);
		if ($this->scope == "admin") {
			//$footer->addJs("/admin/vendor/bootstrap/dist/js/bootstrap.min.js?v=" . $this->version);
			$footer->addJs("/admin/js/bootstrap.min.js?v=" . $this->version);
		}
		else {
			$footer->addJs("/admin/js/bootstrap.min.js?v=" . $this->version);
		}
		//test shariff
		$footer->addJs("/common/share/shariff.complete.js?v=" . $this->version);
		//
		if ($this->angularVersion == 1) {
			$this->computeAngular1Assets ($css_path, $js_path, $header, $footer);
		}
		if ($this->angularVersion == 2) {
			$this->computeAngular2Assets ($css_path, $js_path, $header, $footer);
		}
		if ($this->scope == "portal") {
//			if (getenv("mode") == "dev") {
			if ($this->editMode) {
				$this->computeFroalaAssets ($css_path, $js_path, $header, $footer);
			}
		}
		if ($this->scope == "admin") {
			$this->computeFroalaAssets ($css_path, $js_path, $header, $footer);
		}

/*

		$footer->addJs("https://cdnjs.cloudflare.com/ajax/libs/flickity/1.1.2/flickity.pkgd.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/ace-src-min-noconflict/ace.js" type="text/javascript" charset="utf-8");
	<!--https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.6/ace.js-->


    <script src="/admin/js/jquery.simple-text-rotator.js?v=" . $this->version);
		$footer->addJs("/admin/admin_incls/generic/gsc_dialog_confirm.js?v=" . $this->version);
*/

		if ($this->scope == "admin") {
			if ($this->angularVersion == 1) {
				$footer->addJs("/admin/vendor/moment/moment.js?v=" . $this->version);
				$footer->addJs("/admin/vendor/angular-ui-calendar/src/calendar.js?v=" . $this->version);
				$footer->addJs("/admin/vendor/fullcalendar/dist/fullcalendar.js?v=" . $this->version);
				$footer->addJs("/admin/vendor/fullcalendar/dist/locale-all.js?v=" . $this->version);
				$footer->addJs("/common/js/mdb.js?v=" . $this->version);
			}
		}
		$footer->addJs("/admin/vendor/angular-checkboxes/angular-checkboxes.js?v=" . $this->version);
		if ($this->scope == "portal") {
			if ($this->editMode) {
				$footer->addJs("/admin/app/portal/app.js?v=" . $this->version);
			}
			else {
				$footer->addJs("/admin/app/portal/app_dist.js?v=" . $this->version);
			}
		}

		if ($this->scope == "admin") {
			if ($this->angularVersion == 1) {
				$footer->addJs("/admin/app/office/app.js?v=" . $this->version);
			}
		}

		$footer->addJs("/admin/app/base.js?v=" . $this->version);

		if ($this->scope == "portal") {
			$footer->addJs("/common/form/form.js?v=" . $this->version);
			$footer->addJs("/common/audiojs/audio.min.js?v=" . $this->version);
			$footer->addJs("/common/get-video-id/get-video-id.js?v=" . $this->version);
			$footer->addJs("/common/js/jssor.slider.min.js?v=" . $this->version);
			$footer->addJs("/common/js/jssor_options.js?v=" . $this->version);
			////$footer->addJs("/admin/inpage/inpage_navigation_scripts.js?v=" . $this->version);
		}
		// pica image resizer
		$footer->addJs("/admin/vendor/pica/dist/pica.min.js?v=" . $this->version);
		// blueimp JavaScript-Load-Image
		$footer->addJs("/admin/vendor/blueimp-load-image/js/load-image.all.min.js?v=" . $this->version);

		if ($this->scope == "portal") {
			if ($this->editMode) {
				$footer->addJs("/admin/assistant/assistant.js?v=" . $this->version);
			}
		}
		if ($this->angularVersion == 1) {
			$footer->addJs("/admin/app/library.js?v=" . $this->version);
		}


		if ($this->angularVersion == 1) {
			if ($this->scope == "admin") {
				$footer->addJs("/admin/app/modules.js?v=" . $this->version);
			}
		}

		if (!empty($js_path)) $footer->addJs($js_path);

	}

	/* -----------------------------------------------------------------------------------
	 * compute angular assets
	 */
	protected function computeAngular1Assets ($css_path, $js_path, $header, $footer) {
		if (getenv("mode") == "dev") {
			$footer->addJs("/admin/vendor/angular/angular.js?v=" . $this->version);
		}
		else {
			$footer->addJs("/admin/vendor/angular/angular.min.js?v=" . $this->version);
		}
		$footer->addJs("/admin/vendor/angular-route/angular-route.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-cookies/angular-cookies.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-animate/angular-animate.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-sanitize/angular-sanitize.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-messages/angular-messages.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-i18n/angular-locale_de.js?v=" . $this->version);
//		$footer->addJs("/admin/vendor/angular-file-upload-shim/dist/angular-file-upload-shim.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/ng-file-upload/dist/ng-file-upload.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-local-storage/dist/angular-local-storage.min.js?v=" . $this->version);

		$footer->addJs("/admin/vendor/underscore/underscore-min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/restangular/dist/restangular.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-xeditable/dist/js/xeditable.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-bootstrap/ui-bootstrap-tpls.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/ui-select/dist/select.min.js?v=" . $this->version);
	}

	/* -----------------------------------------------------------------------------------
	 * compute angular assets
	 */
	protected function computeAngular2Assets ($css_path, $js_path, $header, $footer) {

//		$footer->addJs("https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.3/require.js");

		$footer->addJs("https://jspm.io/system@0.19.js");
		$footer->addJs("/admin/account/systemjs.config.js");

    	$files = array(
    				"node_modules/core-js/client/shim.min.js",
    				"node_modules/zone.js/dist/zone.js",
    				"node_modules/rxjs/bundles/Rx.js",
    				"node_modules/@angular/core/bundles/core.umd.js",
    				"node_modules/@angular/common/bundles/common.umd.js",
    				"node_modules/@angular/compiler/bundles/compiler.umd.js",
    				"node_modules/@angular/platform-browser/bundles/platform-browser.umd.js",
    				"node_modules/@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js"
    				);

		foreach ($files as $file) {
			$footer->addJs("/admin/vendor/" . $file);
		}

		$footer->addJs("account/app.component.js");
		$footer->addJs("account/app.module.js");

		return;
		$footer->addJs("https://jspm.io/system@0.19.js");

		$footer->addJs("/admin/vendor/angular2/core.js");
		$footer->addJs("https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.3/require.js");


		if (getenv("mode") == "dev") {
			$footer->addJs("/admin/vendor/angular2/bundles/angular2.js?v=" . $this->version);
//			$footer->addJs("/admin/vendor/angular2/bundles/angular2-all.umd.js");

		}
		else {
			$footer->addJs("/admin/vendor/angular2/bundles/angular2.js?v=" . $this->version);
//			$footer->addJs("/admin/vendor/angular2/bundles/angular2-all.umd.js");
		}

		$footer->addJs("/admin/account/systemjs.config.js");
		$footer->addJs("/admin/account/main.js");

		return;
		$footer->addJs("/admin/vendor/angular-route/angular-route.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-cookies/angular-cookies.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-animate/angular-animate.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-sanitize/angular-sanitize.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-messages/angular-messages.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-i18n/angular-locale_de.js?v=" . $this->version);
//		$footer->addJs("/admin/vendor/angular-file-upload-shim/dist/angular-file-upload-shim.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/ng-file-upload/dist/ng-file-upload.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-local-storage/dist/angular-local-storage.min.js?v=" . $this->version);

		$footer->addJs("/admin/vendor/underscore/underscore-min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/restangular/dist/restangular.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-xeditable/dist/js/xeditable.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/angular-bootstrap/ui-bootstrap-tpls.min.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/ui-select/dist/select.min.js?v=" . $this->version);
	}

	/* -----------------------------------------------------------------------------------
	 * compute froala assets
	 */
	protected function computeFroalaAssets ($css_path, $js_path, $header, $footer) {

		if ($this->scope == "portal") {
			if ($this->editMode) {
				$header->addCss("/admin/vendor/froala/css/froala_editor.min.css");
				$header->addCss("/admin/vendor/froala/css/froala_style.min.css");

				$header->addCss("/admin/vendor/froala/css/plugins/char_counter.css");
				$header->addCss("/admin/vendor/froala/css/plugins/code_view.css");
				$header->addCss("/admin/vendor/froala/css/plugins/colors.css");
				$header->addCss("/admin/vendor/froala/css/plugins/emoticons.css");
				$header->addCss("/admin/vendor/froala/css/plugins/file.css");
				$header->addCss("/admin/vendor/froala/css/plugins/fullscreen.css");
				$header->addCss("/admin/vendor/froala/css/plugins/image.css");
				$header->addCss("/admin/vendor/froala/css/plugins/image_manager.css");
				$header->addCss("/admin/vendor/froala/css/plugins/line_breaker.css");
				$header->addCss("/admin/vendor/froala/css/plugins/quick_insert.css");
				$header->addCss("/admin/vendor/froala/css/plugins/table.css");
				$header->addCss("/admin/vendor/froala/css/plugins/video.css");
				$header->addCss("/admin/vendor/ui-select/dist/select.min.css");
			}
		}

		$footer->addJs("/admin/vendor/froala/js/froala_editor.min.js?v=" . $this->version);
//		if (!$publicArea) {
			$footer->addJs("/admin/vendor/froala/js/plugins/align.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/char_counter.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/code_beautifier.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/code_view.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/colors.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/emoticons.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/entities.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/file.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/font_family.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/font_size.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/fullscreen.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/image.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/image_manager.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/inline_style.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/line_breaker.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/link.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/lists.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/paragraph_format.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/paragraph_style.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/quick_insert.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/quote.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/table.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/save.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/url.min.js?v=" . $this->version);
			$footer->addJs("/admin/vendor/froala/js/plugins/video.min.js?v=" . $this->version);

			//$footer->addJs("/froala/js/languages/de.js?v=" . $this->version);
//		}
		$footer->addJs("/admin/vendor/froala-angular/src/angular-froala.js?v=" . $this->version);
		$footer->addJs("/admin/vendor/froala-angular/src/froala-sanitize.js?v=" . $this->version);
	}


	/* -----------------------------------------------------------------------------------
	 * portalEditModeAssets
	 */
	protected function portalEditModeAssets($header, $footer) {
		if ($this->scope != "portal") return "";
		if (!$this->editMode) return "";
		$header->addCss("/admin/vendor/spectrum-colorpicker/spectrum.css?v=" . $this->version);
		$footer->addJs("/admin/vendor/spectrum-colorpicker/spectrum.js?v=" . $this->version);

		$footer->addJs("/admin/admin_incls/generic/gsc_dialog_confirm.js?v=" . $this->version);
/*
		$footer->addJs("/admin/admin_incls/editor/edit_row.js?v=" . $this->version);
		$footer->addJs("/admin/admin_incls/editor/edit_fulltext.js?v=" . $this->version);
		$footer->addJs("/admin/admin_incls/editor/edit_textline.js?v=" . $this->version);
		$footer->addJs("/admin/admin_incls/editor/edit_image.js?v=" . $this->version);
		$footer->addJs("/admin/admin_incls/editor/edit_iframe.js?v=" . $this->version);
		$footer->addJs("/admin/admin_incls/editor/edit_table.js?v=" . $this->version);
		$footer->addJs("/admin/admin_incls/editor/edit_form.js?v=" . $this->version);
		$footer->addJs("/admin/admin_incls/editor/edit_downloads.js?v=" . $this->version);
		$footer->addJs("/admin/admin_incls/editor/edit_carousel.js?v=" . $this->version);
		$footer->addJs("/admin/admin_incls/editor/edit_embed.js?v=" . $this->version);
		$footer->addJs("/admin/admin_incls/editor/edit_component.js?v=" . $this->version);
		$footer->addJs("/admin/admin_incls/editor/edit_navigation.js?v=" . $this->version);
		$footer->addJs("/admin/admin_incls/editor/edit_style_page.js?v=" . $this->version);
		$footer->addJs("/admin/admin_incls/editor/edit_media_page.js?v=" . $this->version);
*/
		$footer->addJs("/admin/inpage/controller_inpage_edit.js?v=" . $this->version);
		////$footer->addJs("/admin/js/controller_inpage_mode.js?v=" . $this->version);
		$footer->addJs("/admin/inpage/controller_inpage_mouse.js?v=" . $this->version);
		$footer->addJs("/admin/inpage/controller_inpage_menu.js?v=" . $this->version);
		////$footer->addJs("/admin/js/controller_inpage_wizard.js?v=" . $this->version);
		
		
		
		////$footer->addJs("/admin/inpage/controller_inpage_editors.js?v=" . $this->version);
		
		
		
		//$footer->addJs("/admin/inpage/controller_inpage_widgets.js?v=" . $this->version);
		$footer->addJs("/admin/inpage/controller_inpage_rows.js?v=" . $this->version);

		$footer->addJs("/admin/common/dialogs.js?v=" . $this->version);
		$footer->addJs("/admin/assistant/assistant.js?v=" . $this->version);

		$footer->addJs("/admin/inpage/inpage_widgets.js?v=" . $this->version);
		////$footer->addJs("/admin/inpage/inpage_edit_generic.js?v=" . $this->version);
		////$footer->addJs("/admin/inpage/inpage_edit_image.js?v=" . $this->version);

		$footer->addJs("/admin/inpage/inpage_parts.js?v=" . $this->version);

		////$footer->addJs("/admin/inpage/inpage_edit_map.js?v=" . $this->version);
		////$footer->addJs("/admin/inpage/inpage_edit_social.js?v=" . $this->version);
		////$footer->addJs("/admin/inpage/inpage_edit_form.js?v=" . $this->version);

		$footer->addJs("/admin/inpage/inpage_config_pages.js?v=" . $this->version);
		////$footer->addJs("/admin/inpage/inpage_config_page.js?v=" . $this->version);
		$footer->addJs("/admin/inpage/inpage_config_colors.js?v=" . $this->version);
		////$footer->addJs("/admin/inpage/inpage_config_navigation.js?v=" . $this->version);
		$footer->addJs("/admin/inpage/inpage_config_part.js?v=" . $this->version);
		////$footer->addJs("/admin/inpage/inpage_config_seo.js?v=" . $this->version);
//		$footer->addJs("/admin/inpage/inpage_config_row.js?v=" . $this->version);
		$footer->addJs("/admin/inpage/inpage_config_panel.js?v=" . $this->version);
		$footer->addJs("/admin/inpage/inpage.js?v=" . $this->version);
		$footer->addJs("/admin/inpage/inpage_styles.js?v=" . $this->version);

		$header->addCss("/admin/vendor/contextmenu/jquery.contextMenu.css?v=" . $this->version);
		$footer->addJs("/admin/vendor/contextmenu/jquery.contextMenu.js?v=" . $this->version);

		$footer->addJs("https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.2/cropper.min.js");
	}
}
