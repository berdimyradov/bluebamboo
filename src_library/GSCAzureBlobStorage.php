<?php

/**
 * LICENSE: The MIT License (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://github.com/azure/azure-storage-php/LICENSE
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Microsoft
 * @package   MicrosoftAzure\Storage\Samples
 * @author    Azure Storage PHP SDK <dmsh@microsoft.com>
 * @copyright 2016 Microsoft Corporation
 * @license   https://github.com/azure/azure-storage-php/LICENSE
 * @link      https://github.com/azure/azure-storage-php
 */

namespace GSCLibrary;

use MicrosoftAzure\Storage\Blob\Models\CreateContainerOptions;
use MicrosoftAzure\Storage\Blob\Models\PublicAccessType;
use MicrosoftAzure\Storage\Blob\Models\ListContainersResult;
use MicrosoftAzure\Storage\Common\ServicesBuilder;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Common\Exceptions\InvalidArgumentTypeException;

class GSCAzureBlobStorage {

	private static $_blobClient = null;
	protected function getBlobClient()
	{
		if(null === self::$_blobClient) {
			$accountName = getenv('blob_storage_account');
			$accountKey = getenv('blob_storage_key');
			if(empty($accountName) || empty($accountKey)) { throw new \Exception("No Blob Storage Account Settings defined", 500); }
			$connectionString = 'DefaultEndpointsProtocol=https;AccountName=' . $accountName . ';AccountKey=' . $accountKey;
			self::$_blobClient = ServicesBuilder::getInstance()->createBlobService($connectionString);
		}
		return self::$_blobClient;
	}

	/***
	 * Helper function to replace slashes to fit them to azure blob slashes
	 */
	private function replaceSlashes($dir)
	{
		return str_replace(["\\", DIRECTORY_SEPARATOR], "/" , $dir);
	}

	public function blobExists($container, $file)
	{
		$blob = $this->replaceSlashes($file);
		try {
    		$blob = $this->getBlobClient()->getBlob($container, $blob);
    		return true;
		}
		catch (ServiceException $e) {
    		return false;
		}
	}

	public function createBlob($container, $file, $content)
	{
	    $file_content = fopen($content, "r");
	    $blob_name = $this->replaceSlashes($file);
	    $blobClient = $this->getBlobClient();

	    try {
			if(!$this->containerExists($container) && !$this->createContainer($container)) {
				return false;
			}
	        //Upload blob
	        $blobClient->createBlockBlob($container, $blob_name, $file_content);
			return true;
	    } catch (ServiceException $e) {
			return false;
			/*
	        $code = $e->getCode();
	        $error_message = $e->getMessage();
	        echo $code.": ".$error_message.PHP_EOL;
			*/
		}
	}

	public function getBlob($container, $blob_name, $saveTo)
	{
	    $blobClient = $this->getBlobClient();
		$file = $this->replaceSlashes($blob_name);

		try {
	        $getBlob = $blobClient->getBlob($container, $blob_name);
	    } catch (ServiceException $e) {
			return false;
			/*
	        $code = $e->getCode();
	        $error_message = $e->getMessage();
	        echo $code.": ".$error_message.PHP_EOL;
			*/
	    }

	    if(file_put_contents($saveTo, $getBlob->getContentStream()) === false) {
			return false;
		} else {
			return true;
		}
	}

	public function deleteBlob($container, $file)
	{
		$blobClient = $this->getBlobClient();
		$blob = $this->replaceSlashes($file);
		try {
	        $blobClient->deleteBlob($container, $blob);
			return true;
	    } catch (ServiceException $e) {
			/*
	        $code = $e->getCode();
	        $error_message = $e->getMessage();
	        echo $code.": ".$error_message.PHP_EOL;
			*/
			return false;
	    }
	}

	public function createContainer($container)
	{
		$blobClient = $this->getBlobClient();
	    // OPTIONAL: Set public access policy and metadata.
	    // Create container options object.
	    $createContainerOptions = new CreateContainerOptions();
	    // Set public access policy. Possible values are
	    // PublicAccessType::CONTAINER_AND_BLOBS and PublicAccessType::BLOBS_ONLY.
	    // CONTAINER_AND_BLOBS: full public read access for container and blob data.
	    // BLOBS_ONLY: public read access for blobs. Container data not available.
	    // If this value is not specified, container data is private to the account owner.
	    $createContainerOptions->setPublicAccess(PublicAccessType::CONTAINER_AND_BLOBS);
	    // Set container metadata
		/*
	    $createContainerOptions->addMetaData("key1", "value1");
	    $createContainerOptions->addMetaData("key2", "value2");
		*/
		try {
	        // Create container.
	        $blobClient->createContainer($container, $createContainerOptions);
			return true;
	    } catch (ServiceException $e) {
			/*
	        $code = $e->getCode();
	        $error_message = $e->getMessage();
	        echo $code.": ".$error_message.PHP_EOL;
			*/
			return false;
	    }
	}

	public function containerExists($container)
	{
		$blobClient = $this->getBlobClient();
		try {
	        // Create container.
	        $blobClient->getContainerProperties($container);
			return true;
	    } catch (ServiceException $e) {
			/*
	        $code = $e->getCode();
	        $error_message = $e->getMessage();
	        echo $code.": ".$error_message.PHP_EOL;
			*/
			return false;
	    }
	}

}
