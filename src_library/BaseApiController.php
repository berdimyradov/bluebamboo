<?php

/* ---------------------------------------------------------------------------------------
 * BaseApiController for CRUD in the API
 */

namespace GSCLibrary;

class BaseApiController extends GSCController {

	protected $obj;
	protected $obj_acl = null;

	/* -----------------------------------------------------------------------------------
	 * response Json content out of the res-object
	 */
	protected function _jsonResponse($res) {
		$this->response->setStatusCode(200);
		$this->response->setJsonContent($res);
		return $this->response;
	}

	/* -----------------------------------------------------------------------------------
	 * grant access with rights
	 */
	private function grantAccessWithRights ($roles) {
		if (!$this->session->has("user_role")) throw new \Exception("forbidden", 403);
		$user_role = json_decode($this->session->get("user_role"));
		foreach ($user_role as $role) {
			if (in_array($role, $roles)) {
				return;
			}
		}
		throw new \Exception("forbidden", 403);
	}

	protected function grantAccessForClient($client_id = NULL)
	{
		if (!$this->session->has("auth-identity-client")) {
			throw new \Exception("Forbidden", 403);
		}

		$clientId = $this->session->get("auth-identity-client");
		if(!empty($client_id) && (int)$client_id != (int)$clientId) {
			throw new \Exception("Forbidden", 403);
		}

		return $clientId;
	}

	/* -----------------------------------------------------------------------------------
	 * returns data with the option to select fields, filter results and sort result.
	 * authentication required


	 join via main - acl - user
	 $qb->addFrom(acl)
	 $qb->addFrom(user)

	 $qb->addWhere(obj.id == acl.obj_id)
	 $qb->addWhere(current_user_id == acl.user_id)

	 */
	protected function _indexAction($default_projection, $attrs_for_projection, $from, $attrs_for_where, $orderby, $attrs_for_orderby, $filter = null, $postLoad = null) {
		try {
//			$this->grantAccessWithRights(array('admin'));

			if ($this->request->has("_lookup")) {
				return $this->indexLookupAction();
			}
			$qb = $this->modelsManager->createBuilder();
			$qb->addFrom($from, "o");
			if ($this->obj_acl != null) {
				$qb->addFrom($this->obj_acl, "acl");
//				$qb->addFrom("Models\V1\Auth\Users", "users");
			}

			$projection = $default_projection;
			if ($this->request->has("_fields")) {
				$fields = explode(",", $this->request->getQuery("_fields"));
				//if (count($fields) > 3) throw new \Exception("too many fields specified");
				$projection = array();
				foreach ($fields as $f) {
					if (
							(!in_array("o." . $f, $attrs_for_projection)) &&
							(!in_array($f, $attrs_for_projection))
						) {
						throw new \Exception("illegal field " . $f . " in fields", 400);
					}
					$projection[] .= "o." . $f;
				}
			}
			$qb->columns($projection);

			foreach ($this->request->getQuery() as $key => $value) {
				if ($key == "_fields") continue;
				if ($key == "_orderby") continue;
				if ($key == "_url") continue;
				if ($key == "_pix") continue;
				if ($key == "_wrap") continue;
				if ($key == "_psize") continue;
				if (!in_array("o." . $key, $attrs_for_where)) {
					throw new \Exception("illegal field " . $key . " in request parameters", 400);
				}
				if (strpos($value, "%") === false) {	// TODO: protect from calls retrieving all objects in some cases?
						if ($value == "null") {
							$qb->andWhere("o." . $key . " is null");
						}
						else if ($value == "!null") {
							$qb->andWhere("o." . $key . " is not null");
						}
						else {
							$qb->andWhere("o." . $key . " = :" . $key . ":", array($key => $value));
						}
				}
				else {
					$qb->andWhere("o." . $key . " like :" . $key . ":", array($key => $value));
				}
			}

			if ($this->obj_acl != null) {
				if (!$this->session->has('auth-identity-user')) throw new \Exception(403);
				$user_id = $this->session->get('auth-identity-user');

				$qb->andWhere("o.id = acl.obj_id");
				$qb->andWhere("acl.user_id = :" . $key . ":", array($key => $user_id));
			}

			$metaData = new \Phalcon\Mvc\Model\MetaData\Memory();
			if ($metaData->hasAttribute(new $from(), "client_id")) {
				if (!$this->session->has("auth-identity-client")) {
					throw new \Exception("can't access $from objects without a valid client id set", 403);
				}
				$clientId = $this->session->get("auth-identity-client");
				$qb->andWhere("o.client_id = :id:", array("id" => $clientId));
				// $this->logger->log("restricted access of $from to client id $clientId");
			}

			if ($filter != null) {
				foreach ($filter as $key => $value) {
					if (strpos($value, "%") === false) {
						if ($value === "null") {
							$qb->andWhere("o." . $key . " IS NULL");
						}
						else if ($value === "not null") {
							$qb->andWhere("o." . $key . " IS NOT NULL");
						}
						else {
							$qb->andWhere("o." . $key . " = :" . $key . ":", array($key => $value));
						}
					}
					else {
						$qb->andWhere("o." . $key . " like :" . $key . ":", array($key => $value));
					}
				}
			}

			$sort = $orderby;
			if ($this->request->has("_orderby")) {
				$orderby = explode(",", $this->request->getQuery("_orderby"));
				if (count($orderby) > 3) throw new \Exception("too many fields specified in orderby", 400);
				$sort = "";
				foreach ($orderby as $f) {
					if (strlen($f) == 0) throw new \Exception("illegal field name in orderby", 400);
					$fch = substr($f, 0, 1);
					if (($fch == "+") || ($fch == "-")) $f = substr($f, 1);
					if (!in_array("o." . $f, $attrs_for_orderby)) {
						throw new \Exception("illegal field " . $f . " in orderby", 400);
					}
					if ($fch == "-") {
						$sort[] .= "o." . $f . " DESC";
					}
					else if ($fch == "+") {
						$sort[] .= "o." . substr($f, 1);
					}
					else {
						$sort[] .= "o." . $f;
					}
				}
			}
			$qb->orderBy($sort);

			$wrap = $this->request->has("_wrap") && ($this->request->get("_wrap") == "true");

			$objs = [];
			$page = null;
			if ($this->request->has("_pix")) {
				$psize = 10;
				$pix = (int)$this->request->getQuery("_pix");
				if ($pix < 1) throw new \Exception("_pix too small", 400);
				if ($this->request->has("_psize")) {
					$psize = (int)$this->request->getQuery("_psize");
					if ($psize < 1) throw new \Exception("_psize too small", 400);
				}
				$paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
					"builder" => $qb,
					"limit"   => $psize,
					"page"    => $pix
				));
				$page = $paginator->getPaginate();
				if (($page->total_pages == 0) && ($pix == 1)) {
					// ok case in case of no results
				}
				else if ($page->total_pages < $pix) {
					throw new \Exception("_pix too big", 400);
				}

				if (!$wrap) {
					$this->response->setHeader("X-Current", "" . $page->current);
					$this->response->setHeader("X-Before", "" . $page->before);
					$this->response->setHeader("X-Next", "" . $page->next);
					$this->response->setHeader("X-Last", "" . $page->last);
					$this->response->setHeader("X-Total-Pages", "" . $page->total_pages);
					$this->response->setHeader("X-Total-Count", "" . $page->total_items);
				}

				$objs = $page->items;
			}
			else {
				$objs = $qb->getQuery()->execute();
			}

			$res = array();
			foreach ($objs as $o) {
				if (property_exists($from, "attrs_index")) {
					$o = $this->_valueConversionPostload($o, $from::$attrs_index);
				}
				if ($postLoad != null) $o = $postLoad($o);
				$res[] = $o;
			}

			if ($wrap) {
				$wres = new \stdClass();
				$wres->data = $res;
				if ($page != null) {
					$wres->paging = new \stdClass();
					$wres->paging->current = $page->current;
					$wres->paging->before = $page->before;
					$wres->paging->next = $page->next;
					$wres->paging->last = $page->last;
					$wres->paging->total_pages = $page->total_pages;
					$wres->paging->total_count = $page->total_items;
				}
				$res = $wres;
			}

			return $this->_jsonResponse($res);
		}
		catch (\Exception $err) {
			if ($err->getCode() == 403) {
				$this->response->setStatusCode(403, "forbidden");
			}
			else {
				$this->response->setStatusCode(400, "Bad Request: " . $err->getMessage());
			}
			/*
			$res = new \stdClass();
			$res->message = "no no...access";
			$res->url = "/msc/admin";
			$this->response->setContentType('application/json', 'UTF-8');
			$this->response->setJsonContent($res);
			*/
			return $this->response;
		}
	}

	/* -----------------------------------------------------------------------------------
	 * lookup for index
	 * /api/v1/invo/invoices_lookup?_lookup=12
	 */
	protected function _indexLookupAction ($projection, $from, $lookup_fields, $orderby, $label) {
		try {

			$qb = $this->modelsManager->createBuilder();
			$qb->addFrom($from, "o");
			$qb->columns($projection);

			$id = $this->request->get("_id");
			if ($id != null) {
				$key = "id";
				$value = $id;
				$qb->andWhere("o." . $key . " = :" . $key . ":", array($key => $value));
			}
			else {
				$value = "%" . $this->request->get("_lookup") . "%";
				foreach ($lookup_fields as $key) {
					$qb->orWhere("o." . $key . " like :" . $key . ":", array($key => $value));
				}
			}

			// TODO: and-or-precedence check
			$metaData = new \Phalcon\Mvc\Model\MetaData\Memory();
			if ($metaData->hasAttribute(new $from(), "client_id")) {
				if (!$this->session->has("auth-identity-client")) {
					throw new \Exception("can't access $from objects without a valid client id set", 403);
				}
				$clientId = $this->session->get("auth-identity-client");
				$qb->andWhere("o.client_id = :id:", array("id" => $clientId));
			}

			$qb->orderBy($orderby);

			$wrap = $this->request->has("_wrap") && ($this->request->get("_wrap") == "true");

			$objs = [];
			$psize = 50;
			$paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
				"builder" => $qb,
				"limit"   => $psize,
				"page"    => 1
			));
			$page = $paginator->getPaginate();
			$objs = $page->items;

			$res = array();
			foreach ($objs as $o) $res[] = $label($o);

			return $this->_jsonResponse($res);
		}
		catch (\Exception $err) {
			if ($err->getCode() == 403) {
				$this->response->setStatusCode(403, "Forbidden: " . $err->getMessage());
			}
			else {
				$this->response->setStatusCode(400, "Bad Request: " . $err->getMessage());
			}
			return $this->response;
		}
	}

	/* -----------------------------------------------------------------------------------
	 * returns one object
	 */
	protected function _oneAction($modelClass, $pk, $postLoad = null) {
		return $this->jsonReadRequest(function(&$transaction) use ($modelClass, $pk, $postLoad) {
			$obj = $this->_oneActionObj($modelClass, $pk, $postLoad);
			return $obj;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * one Action returning the original phalcon object
	 */
	protected function _oneActionObj($modelClass, $pk, $postLoad = null) {
		if (!isset($pk)) {
			throw new \Exception("Missing parameter: ID", 400);
		}

		$obj = $modelClass::findFirstByid($pk);
		if ($obj == null) {
			throw new \Exception("Item not found", 404);
		}

		// Access checkup
		$metaData = $obj->getModelsMetaData();
		if ($metaData->hasAttribute($obj, "client_id")) {
			$this->grantAccessForClient($obj->client_id);
		}

		if (property_exists($modelClass, "attrs_index")) {
			$obj = $this->_valueConversionPostload($obj, $modelClass::$attrs_index);
		}
		// else if (property_exists($modelClass, "attrs")) {
		//	$obj = $this->_valueConversionPostload($obj, $modelClass::$attrs);
		//}
		if ($postLoad != null) {
			$obj = $postLoad($obj);
		}

		return $obj;
	}

	/* -----------------------------------------------------------------------------------
	 * one Action returning the object together with its related objects
	 */
	protected function _oneActionRelations($modelClass, $pk, $relations, $postLoad = null) {
		return $this->jsonReadRequest(function(&$transaction) use ($modelClass, $pk, $relations, $postLoad) {
			$obj = $this->_oneActionObj($modelClass, $pk, $postLoad);
			$relobj = $this->_getActionAddRelations($obj, $relations);
			return $relobj;
		});

		/*
		$obj = $this->_oneActionObj($modelClass, $pk, $postLoad);
		$obj = $this->_getActionAddRelations($obj, $relations);
		return $this->_jsonResponse($obj);
		*/
	}

	/* -----------------------------------------------------------------------------------
	 * returns the related objects for one object specified by modelclass and id
	 */
	protected function _relationsAction($modelClass, $pk, $relationAlias) {
		try {
			if (!isset($pk)) {
				throw new \Exception("missing parameter", 400);
			}

			$obj = $modelClass::findFirstByid($pk);
			if ($obj == null) {
				throw new \Exception("relation not found", 404);
			}

			$metaData = $obj->getModelsMetaData();
			if ($metaData->hasAttribute($obj, "client_id")) {
				$this->grantAccessForClient($obj->client_id);
			}

			$objs = $obj->$relationAlias;

			$res = array();
			foreach ($objs as $o) {
				if ($postLoad != null) $o = $postLoad($o);
				$res[] = $o;
			}

			return $this->_jsonResponse($res);
		}
		catch (\Exception $err) {
			if ($err->getCode() == 403) {
				$this->response->setStatusCode(403, "Forbidden: " . $err->getMessage());
			}
			else {
				$this->response->setStatusCode(400, "Bad Request: " . $err->getMessage());
			}
			return $this->response;
		}
	}

	protected function objectCreated($object) {
		
	}

	/* -----------------------------------------------------------------------------------
	 * creates a new object and returns JSON response
	 */
	protected function _createAction($modelClass, $attrs, $values = null, $valueConverter = null)
	{
		return $this->jsonWriteRequest(function(&$transaction) use ($modelClass, $attrs, $values, $valueConverter)
		{
			$transaction = $this->transaction;
			$data = $this->request->getJsonRawBody();
			$res = $this->_createActionObj($data, $modelClass, $attrs, $values, $valueConverter);
			$transaction->commit();
			return $res;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * creates a new object and returns model obj
	 */
	protected function _createActionObj($valueObj, $modelClass, $attrs, $values = null, $valueConverter = null)
	{
//		$this->grantAccessWithRights(array('admin'));

		$metaData = new \Phalcon\Mvc\Model\MetaData\Memory();
		if ($metaData->hasAttribute(new $modelClass(), "client_id")) {	
			$id = $this->grantAccessForClient();
			if ($values == null) $values = array();
			$values["client_id"] = $id;
		}

		if ($metaData->hasAttribute(new $modelClass(), "createdBy")) {
			if (!$this->session->has("auth-identity-user")) {
				throw new \Exception("can't access $from objects without a valid user id set", 403);
			}
			$id = $this->session->get("auth-identity-user");
			if ($values == null) $values = array();
			$values["createdBy"] = $id;
		}

		if ($metaData->hasAttribute(new $modelClass(), "platform_id")) {
			if (!$this->session->has("auth-identity-platform")) {
				throw new \Exception("can't access $from objects without a valid platform id set", 403);
			}
			$id = $this->session->get("auth-identity-platform");
			if ($values == null) $values = array();
			$values["platform_id"] = $id;
		}

		$o = $valueObj;
		$obj = new $modelClass();
		$obj->setTransaction($this->transaction);

//		$user_role = $this->session->get("user_role");
		foreach ($attrs as $attr => $roles) {
//				if ($roles != "") $this->grantAccessWithRights(array($roles));
			if (!property_exists($o, $attr)) {
//				$this->logger->log("do not set attribute " . $attr . " because there is no value present in the post request");
				continue;
			}
/* TODO: sanitize
			$val = null;
			$val = $this->filter->sanitize($data->$field, $filter);
*/
			if ($attr == "client_id") continue;

			// Value/Type Conversion
			$conval = $this->MYSQLconvert($o->$attr, $roles);
			if ($valueConverter != null) $obj->$attr = $valueConverter($attr, $conval);
			else $obj->$attr = $conval;
		}
		if ($values != null) {
			foreach ($values as $key => $val) {
				$obj->$key = $val;
			}
		}

		if ($obj->create()) {
			$this->obj = $obj;
			// Update / Handle new Files -> move from Temp to Folders
			$this->_updateFiles($modelClass, $obj);
			$this->objectCreated($obj);
			return $obj;
		}
		else {
			$res = new \stdClass();
			$res->status = "error";
			$res->message = "Bitte prüfen Sie Ihre Eingaben";
			$res->errors = array();
			foreach ($obj->getMessages() as $msg) {
				$m = new \stdClass();
				$m->type = $msg->getType();
				$m->message = $msg->getMessage();
				$m->field = $msg->getField();
				$res->errors[] = $m;
			}
			throw new \GSCLibrary\GSCException($res);
		}
	}

	/* -----------------------------------------------------------------------------------
	 * creates a new object and the specified Relations
	 */
	protected function _createActionRelations($modelClass, $attrs, $relations, $values = null, $valueConverter = null) {
		try {
			$o = $this->request->getJsonRawBody();
			$obj = $this->_createActionObj($o, $modelClass, $attrs, $values, $valueConverter);
			$newId = $obj->id;
			$o->id = $newId;

			foreach($relations as $relation) {
				$type = $relation["type"];
				switch($type) {
					case "manyToMany":
						$this->_createActionManyToMany($o, $relation);
						break;
					case "hasOne":
						$this->_createActionHasOne($o, $relation);
						break;
				}
				/* old Part, new in own function
				if($relation["type"] == "manyToMany") {

					$alias = $relation["alias"];
					if(isset($o->$alias)) {
						$relClass = $relation["throughModel"];
						foreach($o->$alias as $rel) {
							$tia = $relation["throughIdAlias"];
							$rel->$tia = $newId;
							$this->_createActionObj($rel, $relClass, $relClass::$attrs, null, $valueConverter);
						}
					}
				}
				*/
			}

			$this->transaction->commit();

			return $this->_jsonResponse($this->_getActionAddRelations($obj, $relations));
		}
		catch (\Exception $err) {
			if ($err->getCode() == 403) {
				$this->response->setStatusCode(403, "Forbidden: " . $err->getMessage());
			}
			else {
				$this->response->setStatusCode(400, "Bad Request: " . $err->getMessage());
			}
			return $this->response;
		}
	}

	/* -----------------------------------------------------------------------------------
	 * create a new Relations Entry for a Many to Many relation
	 */
	protected function _createActionManyToMany($valueObj, $relation)
	{
		$alias = $relation["alias"];
		if(isset($valueObj->$alias)) {
			$relClass = $relation["throughModel"];
			foreach($valueObj->$alias as $rel) {
				$tia = $relation["throughIdAlias"];
				$rel->$tia = $valueObj->id;
				$this->_createActionObj($rel, $relClass, $relClass::$attrs);
			}
		}
		/*
		else {
			throw new \Exception("can't create relations without the valueObj set");
		}
		*/
	}

	/* -----------------------------------------------------------------------------------
	 * create a new Relations Entry for a HasOne relation
	 */
	protected function _createActionHasOne($valueObj, $relation)
	{
		$alias = $relation["alias"];
		if(isset($valueObj->$alias)) {
			$relClass = $relation["relationModel"];
			$relobj = $valueObj->$alias;
			$ria = $relation["relationIdAlias"];

			if(!isset($relobj->id)) {
				throw new \Exception("primary key of relation object not defined", 400);
			}
			if(!isset($valueObj->id)) {
				throw new \Exception("primary key of value object not defined", 400);
			}

			$values = new \stdClass();
			$values->$ria = $valueObj->id;

			$this->_updateActionObj($values, $relClass, $relClass::$attrs, $relobj->id);
		}
	}

	/* -----------------------------------------------------------------------------------
	 * updates an existing object and returns JSON response
	 */
	protected function _updateAction($modelClass, $attrs, $pk, $values = null, $valueConverter = null) {
		return $this->jsonWriteRequest(function(&$transaction) use ($modelClass, $attrs, $pk, $values, $valueConverter) {
			$transaction = $this->transaction;
			$data = $this->request->getJsonRawBody();
			$res = $this->_updateActionObj($data, $modelClass, $attrs, $pk, $values, $valueConverter);
			$transaction->commit();
			return $res;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * creates an existing object and returns model obj
	 */
	protected function _updateActionObj($valueObj, $modelClass, $attrs, $pk, $values = null, $valueConverter = null) {
		if (!isset($pk)) {
			throw new \Exception("missing parameter", 400);
		}
		/*
		if (gettype ($pk) == "string") {
			if (strlen($pk) < 1) {
				throw new \Exception("invalid parameter");
			}
			$pk = substr($pk, 1);
		}
		*/

		$obj = $modelClass::findFirstByid($pk);

		$metaData = $obj->getModelsMetaData();
		if ($metaData->hasAttribute($obj, "client_id")) {
			$this->grantAccessForClient($obj->client_id);
			if ($values == null) $values = array();
			$values["client_id"] = $obj->client_id;
		}

		// save Old data if needed later (for Example File update purposes)
		//$this->logger->log(json_encode($obj, JSON_PRETTY_PRINT));
		$obj_old = json_decode(json_encode($obj));

		if ($obj == null) {
			throw new \Exception("object not found", 404);
		}
		$obj->setTransaction($this->transaction);

		$o = $valueObj;

		foreach ($attrs as $attr => $roles) {
			if (!property_exists($o, $attr)) {
//					$this->logger->log("do not set attribute " . $attr . " because there is no value present in the post request");
				continue;
			}
			if ($attr == "client_id") continue;

			// Value/Type Conversion
			$conval = $this->MYSQLconvert($o->$attr, $roles);
			if ($valueConverter != null) $obj->$attr = $valueConverter($attr, $conval);
			else $obj->$attr = $conval;
		}

		if ($values != null) {
			foreach ($values as $key => $val) {
				$obj->$key = $val;
			}
		}

		if ($obj->update()) {
			$this->obj = $obj;
			$this->_updateFiles($modelClass, $obj, $obj_old);
			return $obj;
		}

		$res = new \stdClass();
		$res->status = "error";
		$res->errors = array();
		foreach ($obj->getMessages() as $msg) {
			$m = new \stdClass();
			$m->type = $msg->getType();
			$m->message = $msg->getMessage();
			$m->field = $msg->getField();
			$res->errors[] = $m;
		}
//		$this->logger->log(json_encode($res, JSON_PRETTY_PRINT));
//		$this->logger->commit();
		$this->rollback("update failed", $res);
	}

	/* -----------------------------------------------------------------------------------
	 * updates an existing object and its Relations
	 */
	protected function _updateActionRelations($modelClass, $attrs, $pk, $relations, $values = null, $valueConverter = null) {

		return $this->jsonWriteRequest(function(&$transaction) use ($modelClass, $attrs, $pk, $relations, $values, $valueConverter) {

			$transaction = $this->transaction;
			$o = $this->request->getJsonRawBody();
			$obj = $this->_updateActionObj($o, $modelClass, $attrs, $pk, $values, $valueConverter);

			foreach($relations as $relation) {
				if($relation["type"] == "manyToMany") {
					$alias = $relation["alias"];
					if(isset($o->$alias)) {
						// Hilfsarray aus vorhandenen Relationen erstellen:
						$relObjs = array();
						$ta = $relation["throughAlias"];
						foreach($obj->$ta as $add) {
							$relObjs[] = $add;
						}
						$relClass = $relation["throughModel"];
						$rel1Alias = $relation["throughIdAlias"];
						$rel2Alias = $relation["throughRelIdAlias"];
						// Definierte Relationen auf Neu oder Update prüfen
						foreach($o->$alias as $rel) {
							$new = true;
							for($i = 0; $i < count($relObjs); $i++) {
								$relObj = $relObjs[$i];
								if($rel->$rel1Alias == $pk && $relObj->$rel1Alias == $pk && $rel->$rel2Alias == $relObj->$rel2Alias) {
									// Beziehung ist schon vorhanden: Update!
									$this->_updateActionObj($rel, $relClass, $relClass::$attrs, $relObj->id, null, $valueConverter);
									$new = false;
									unset($relObjs[$i]);
									$relObjs = array_values($relObjs);
									break;
								}
							}
							if($new) {
								// Beziehung ist neu: Anlegen!
								$this->_createActionObj($rel, $relClass, $relClass::$attrs, null, $valueConverter);
							}
						}
						// Alte, nicht gefundene Beziehungen löschen
						foreach($relObjs as $relDel) {
							$this->_deleteActionObj($relClass, $relDel->id);
						}
					}
					else {
						$this->logger->log("current relation " . json_encode($relation, JSON_PRETTY_PRINT));
						$this->logger->log("alias " . $alias);
						$this->logger->log("relations metadata " . json_encode($relations, JSON_PRETTY_PRINT));
						$this->logger->log("data  " . json_encode($o, JSON_PRETTY_PRINT));
						$this->response->setStatusCode(404, "relation not defined");
						return $this->response->send();
					}
				}
			}

			$this->transaction->commit();
			return $this->_jsonResponse($this->_getActionAddRelations($obj, $relations));

		});
	}

	/* -----------------------------------------------------------------------------------
	 * deletes an existing object and returns JSON response
	 */
	protected function _deleteAction($modelClass, $pk) {
		return $this->jsonWriteRequest(function(&$transaction) use ($modelClass, $pk) {

//			$this->grantAccessWithRights(array('admin'));
			if (!isset($pk)) {
				throw new \Exception("missing parameter", 400);
			}
			$obj = $modelClass::findFirstByid($pk);
			if ($obj == null) {
				throw new \Exception("object not found", 404);
			}

			// Access Stuff
			$metaData = $obj->getModelsMetaData();
			if ($metaData->hasAttribute($obj, "client_id")) {
				$this->grantAccessForClient($obj->client_id);
			}

			$obj->setTransaction($this->transaction);
			// Save Object data for later use - eg file delete
			$oldObj = json_decode(json_encode($obj));
			if ($obj->delete()) {
				$this->transaction->commit();
				// Delete old now unused files
				$this->_updateFiles($modelClass, null, $oldObj);
				$res = new \stdClass();
				$res->pk = $pk;
				$this->response->setStatusCode(201, "deleted");
				return $res;
			}
			$res = new \stdClass();
			$res->status = "error";
			$res->errors = array();
			foreach ($obj->getMessages() as $msg) {
				$m = new \stdClass();
				$m->type = $msg->getType();
				$m->message = $msg->getMessage();
				$m->field = $msg->getField();
				$res->errors[] = $m;
			}
			$this->rollback("delete failed", $res);
		});
	}

	/* -----------------------------------------------------------------------------------
	 * deletes an object and the specified relations
	 */
	protected function _deleteActionRelations($modelClass, $pk, $relations) {
		try {
//			$this->grantAccessWithRights(array('admin'));

			if (!isset($pk)) {
				throw new \Exception("missing parameter", 400);
			}

			$obj = $modelClass::findFirstByid($pk);
			if ($obj == null) {
				throw new \Exception("object not found", 404);
			}

			$metaData = $obj->getModelsMetaData();
			if ($metaData->hasAttribute($obj, "client_id")) {
				$this->grantAccessForClient($obj->client_id);
			}

			$obj->setTransaction($this->transaction);

			$res = new \stdClass();
			$res->deletedRelations = array();
			$res->errors = array();

			foreach($relations as $relation) {
				if($relation["type"] == "manyToMany") {
					$ta = $relation["throughAlias"];
					$objs = $obj->$ta;
					if ($objs == null) {
						throw new \Exception("object not found", 404);
					}

					if(count($objs) > 0) {
						$res->deletedRelations[$relation["alias"]] = array();
						$res->errorRelations[$relation["alias"]] = array();
						foreach($objs as $rel) {
							$relId = $rel->id;
							$resAdd = new \stdClass();
							if ($rel->delete()) {
								$resAdd->pk = $relId;
								$res->deletedRelations[$relation["alias"]][] = $resAdd;
							}
							else {
								$resAdd->status = "relation error";
								$resAdd->errors = array();
								foreach ($rel->getMessages() as $msg) {
									$m = new \stdClass();
									$m->type = $msg->getType();
									$m->message = $msg->getMessage();
									$m->field = $msg->getField();
									$resAdd->errors[] = $m;
								}
								$res->errors[] = $resAdd;
							}
						}
					}
				}
			}

			// save old obj for later use - for example file deleting
			$oldObj = json_decode(json_encode($obj));

			if ($obj->delete()) {
				// update files -> delete now unused ones
				$this->_updateFiles($modelClass, null, $oldObj);
				$res->pk = $pk;
				$this->response->setStatusCode(201, "deleted");
				$this->transaction->commit();
				return $this->_jsonResponse($res);
			}

			$resAdd = new \stdClass();
			$resAdd->status = "object error";
			foreach ($obj->getMessages() as $msg) {
				$m = new \stdClass();
				$m->type = $msg->getType();
				$m->message = $msg->getMessage();
				$m->field = $msg->getField();
				$resAdd->errors[] = $m;
			}
			$res->errors[] = $redAdd;
			$res->status = "error";
			$this->rollback("delete failed", $res);
		}
		catch (\Exception $err) {
			if ($err->getCode() == 403) {
				$this->response->setStatusCode(403, "Forbidden: " . $err->getMessage());
			}
			else {
				$this->response->setStatusCode(400, "Bad Request: " . $err->getMessage());
			}
			return $this->response;
		}
	}

	/* -----------------------------------------------------------------------------------
	 * deletes an existing object and returns an obj with pk as response -> does not commit the transaction
	 */
	protected function _deleteActionObj($modelClass, $pk) {
		if (!isset($pk)) {
			throw new \Exception("missing parameter", 400);
		}
		
		$obj = $modelClass::findFirstByid($pk);
		if ($obj == null) {
			throw new \Exception("Item not found", 404);
		}

		$metaData = $obj->getModelsMetaData();
		if ($metaData->hasAttribute($obj, "client_id")) {
			$this->grantAccessForClient($obj->client_id);
		}

		$obj->setTransaction($this->transaction);

		// Save Object data for later use - eg file delete
		$oldObj = json_decode(json_encode($obj));
		if (!$obj->delete()) {
			throw new \Exception("Item delete failed", 400);
		}

		$this->_updateFiles($modelClass, null, $oldObj);
		$res = new \stdClass();
		$res->pk = $pk;
		return $res;
	}

	/* -----------------------------------------------------------------------------------
	 * Create a Array of an object and add the specified relations to the object-array
	 */
	protected function _getActionAddRelations($obj, $relations) {
		if(method_exists($obj, "toArray")) {
			// Create Object-Array
			$res = $obj->toArray();
			// Add Relations to the Array
			foreach($relations as $relation) {
				$type = $relation["type"];
				$model = $relation["relationModel"];
				$relationAlias = $relation["alias"];
				// Different adding of Relations depending on the Relation Type
				switch($type) {
					case "manyToMany":
						$aliasOne = $relation["aliasOneObj"];
						$relThrough = $relation["throughAlias"];
						$relModel = $relation["throughModel"];

						$relObjs = $obj->$relThrough;
						$objs = array();

						foreach($relObjs as $rel) {
							$rel = $this->_valueConversionPostload($rel, $relModel::$attrs);
							if(isset($rel->$aliasOne)) {
								$relObj = $this->_valueConversionPostload($rel->$aliasOne, $model::$attrs);
								if(method_exists($rel, "toArray")) {
									$resAdd = $rel->toArray();
									$resAdd[$aliasOne] = $relObj;
									$objs[] = $resAdd;
								}
							}
						}

						$res[$relationAlias] = $objs;
						break;
					case "oneToMany":
					case "hasMany":
						if(isset($obj->$relationAlias)) {
							$relObjs = $obj->$relationAlias;
							$objs = array();
							foreach($relObjs as $rel) {
								$relObj = $this->_valueConversionPostload($rel, $model::$attrs);
								$objs[] = $relObj;
							}
							$res[$relationAlias] = $objs;
						}
						break;
					case "manyToOne":
					case "belongsTo":
						if(isset($obj->$relationAlias)) {
							$rel = $obj->$relationAlias;
							$rel = $this->_valueConversionPostload($rel, $model::$attrs);
							if(method_exists($rel, "toArray")) {
								$res[$relationAlias] = $rel->toArray();
							}
						}
						break;
					case "oneToOne":
					case "hasOne":
						if(isset($obj->$relationAlias)) {
							$rel = $obj->$relationAlias;
							$rel = $this->_valueConversionPostload($rel, $model::$attrs);
							if(method_exists($rel, "toArray")) {
								$res[$relationAlias] = $rel->toArray();
							}
						}
						break;
				}
			}
		}
		else {
			$res = array();
		}

		return $res;
	}

	/* -------------------------------------------------------------------------
	 * Update Files in Create / Update / Delete Actions
	 */
	protected function _updateFiles($modelClass, $data, $oldData = null)
	{
		if(property_exists($modelClass, "fileattrs")) {
			$attrs = $modelClass::$fileattrs;
			foreach($attrs as $attr => $filetypearr) {

				$filetype = $filetypearr[0];
				$filecount = $filetypearr[1];

				$oldFile = !empty($oldData->$attr) ? $oldData->$attr : "";
				$newFile = !empty($data->$attr) ? $data->$attr : "";

				switch($filetype) {
					case "image":
						if($filecount == "one") {
							$this->_condMoveDeleteImage($newFile, $oldFile);
						} else {
							// TODO: Handle JSON strings with many filenames
						}
						break;
					// TODO: Handle other filetypes
				}
			}
		}
	}

	/* -------------------------------------------------------------------------
	 * Check if a Image has to be deleted / moved from temp or replaced
	 *	$attr: The property name of the Image in dataObj
	 *	$dataObj: The entry object containing the data
	 *	$valObj: The value (old) object containing the old data, null for new objects that shall be generated
	 */
	protected function _condMoveDeleteFile($path, $newFile = null, $oldFile = null)
	{
		$fileSwitch = $this->getFileSwitch();
		$new = !empty($newFile) ? $path.$newFile : "";
		$old = !empty($oldFile) ? $path.$oldFile : "";

		if($new != "") {
			if($old != "") {
				if($new != $old) {
					if(!$fileSwitch->saveFileFromTemp($newFile, $new)) {
						throw new \Exception("Unable to move file from temp folder!", 404);
					}
					$fileSwitch->deleteFile($old);
				}
			} else {
				if(!$fileSwitch->saveFileFromTemp($newFile, $new)) {
					throw new \Exception("Unable to move file from temp folder!", 404);
				}
			}
		} else if($old != "") {
			$fileSwitch->deleteFile($old);
		}
	}

	/* -------------------------------------------------------------------------
	 * Check if a Image has to be deleted / moved from temp or replaced
	 *	$attr: The property name of the Image in dataObj
	 *	$dataObj: The entry object containing the data
	 *	$valObj: The value (old) object containing the old data, null for new objects that shall be generated
	 */
	protected function _condMoveDeleteImage($newFile = null, $oldFile = null)
	{
		$imgPath = $this->getClientImagePath();
		$this->_condMoveDeleteFile($imgPath, $newFile, $oldFile);
	}

	/* -----------------------------------------------------------------------------------
	 * Value Conversion for Postload
	 */

	 protected function _valueConversionPresave($obj, $attrs) {
		 $o = $obj;
 		foreach($attrs as $attr => $type) {
 			if(isset($o->$attr)) {
 				$o->$attr = $this->MYSQLconvert($o->$attr, $type);
 			}
 		}
 		return $o;
	 }

	protected function _valueConversionPostload($obj, $attrs) {
		$o = $obj;
		foreach($attrs as $attr => $type) {
			if(isset($o->$attr)) {
				$o->$attr = $this->JSONconvert($o->$attr, $type);
			}
		}
		return $o;
	}

	protected function JSONconvert($attr, $type) {
		if($attr === null) return null;
		switch($type) {
			case "string":
				return (string)$attr;
			case "int":
				return (int)$attr;
			case "decimal":
			case "float":
				return (float)$attr;
			case "bool":
				return (bool)$attr;
			case "date":
			case "datetime":
			case "timestamp":
				return date('c', strtotime($attr));
			case "time":
				return date('c', strtotime($attr, 0));
			case "json":
				return json_decode($attr);
			default:
				return $attr;
		}
	}
	
	protected function allowTags () {
		return false;
	}

	protected function MYSQLconvert($attr, $type) {
		// https://docs.phalconphp.com/tr/3.2/filter
		$filter = new \Phalcon\Filter();
		try {
			if ($attr === null) return null;
			switch($type) {
				case "string":
					if (gettype($attr) == "object") return null;
					if ($this->allowTags()) {
						return (string)$attr;
					}
					else {
						return $filter->sanitize($attr, ["striptags", "trim"]);
					}
				case "email":
					return $filter->sanitize($attr, ["striptags", "trim", "email"]);
				case "int":
					return $filter->sanitize($attr, ["int"]);
//					return (int)$attr;
				case "decimal":
				case "float":
					return $filter->sanitize($attr, ["float"]);
//					return (float)$attr;
				case "bool":
					return (int)filter_var($attr, FILTER_VALIDATE_BOOLEAN);
				case "date":
					$val = strtotime($attr);
					return date('Y-m-d', $val);
				case "time":
					$val = strtotime($attr);
					return date('H:i:s', $val);
				case "datetime":
					$val = strtotime($attr);
					return date('Y-m-d H:i:s', $val);
				case "timestamp":
					if($attr == null) {
						return $attr;
					}
					$val = strtotime($attr);
					return date('Y-m-d H:i:s.u', $val);
					//$date = new \DateTime();
					//$date->setTimestamp($val);
					//return date_format($date, 'Y-m-d H:i:s.u');
					// return date_format(new \DateTime($val), 'Y-m-d H:i:s.u');
				case "json":
					return json_encode($attr);
				default:
					return $attr;
			}
		}
		catch (\Exception $err) {
			error_log("MYSQLconvert failed for attr[$type]: $attr");
			throw new \Exception();
		}
		catch (\Error $err) {
			error_log("logger")->log("MYSQLconvert failed for attr[$type]: $attr");
			throw new \Exception();
		}
	}

}
