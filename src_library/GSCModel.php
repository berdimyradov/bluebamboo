<?php

namespace GSCLibrary;

class GSCModel extends \Phalcon\Mvc\Model
{

	/* **********************
	 * Overwrite original toArray() Method to add Relations in Json-Data
	 */
	public function toArray($columns = null)
	{
		$res = parent::toArray($columns);
		$class = get_class($this);

		$aliases = array();
		if(isset($class::$alias_one)) {
			$aliases[] = $class::$alias_one;
		}
		if(isset($class::$alias_many)) {
			$aliases[] = $class::$alias_many;
		}

		if(isset($class::$relations)) {
			$relations = $class::$relations;
			foreach($relations as $rel) {
				$rel_alias = $rel['alias'];
				$res[$rel_alias] = $this->_relationAddition($rel, $aliases);
			}
		}

	    return $res;
	}

	/* **********************
	 * alternative toArray() function that recognizes the source
	 */
	public function _toArrayFrom(array $fromAliases)
	{
		$res = parent::toArray();
		$class = get_class($this);

		if(isset($class::$alias_one)) {
			$fromAliases[] = $class::$alias_one;
		}
		if(isset($class::$alias_many)) {
			$fromAliases[] = $class::$alias_many;
		}
		/*
		$alias_from_one = isset($fromAlias["alias_one"]) ? $fromAlias["alias_one"] : "";
		$alias_from_many = isset($fromAlias["alias_many"]) ? $fromAlias["alias_many"] : "";
		*/

		if(isset($class::$relations)) {
			$relations = $class::$relations;
			foreach($relations as $rel) {
				$rel_alias = $rel['alias'];
				if(in_array($rel_alias, $fromAliases)) continue;
				//if($rel_alias == $alias_from_one || $rel_alias == $alias_from_many) continue;
				$res[$rel_alias] = $this->_relationAddition($rel, $fromAliases);
			}
		}

	    return $res;
	}

	/* ------------------------------------------------
	 * Object / Array of relation to Add to this Model entry
	 */
	protected function _relationAddition($rel, $aliases)
	{
		$rel_type = $rel['type'];
		$rel_alias = $rel['alias'];
		$rel_model = $rel['relationModel'];
		$attrs = $rel_model::$attrs;
		if(isset($this->$rel_alias) && $this->$rel_alias != false) {
			switch($rel_type) {
				case "manyToOne":
				case "belongsTo":
				case "oneToOne":
				case "hasOne":
					$rel_obj = $this->$rel_alias;
					$rel_obj = $this->_valueConversionPostload($rel_obj, $attrs);
					if(method_exists($rel_obj, "_toArrayFrom")) {
						return $rel_obj->_toArrayFrom($aliases);
					} else {
						return $rel_obj->toArray();
					}
				case "oneToMany":
				case "hasMany":
					$rel_objs = $this->$rel_alias;
					$addArr = array();
					foreach($rel_objs as $rel_obj) {
						$rel_o = $this->_valueConversionPostload($rel_obj, $attrs);
						if(method_exists($rel_o, "_toArrayFrom")) {
							$addArr[] = $rel_o->_toArrayFrom($aliases);
						} else {
							$addArr[] = $rel_o->toArray();
						}
					}
					return $addArr;
				case "manyToMany":
					$rel_aliasOne = $relation["aliasOneObj"];
					$rel_aliasThrough = $relation["throughAlias"];
					$rel_modelThrough = $relation["throughModel"];

					$rel_objs = $this->$rel_aliasThrough;
					$addArr = array();
					foreach($rel_objs as $rel_obj) {
						if(method_exists($rel_obj, "_toArrayFrom")) {
							$rel_t_arr = $rel_obj->_toArrayFrom($aliases);
						} else {
							$rel_t_arr = $rel_obj->toArray();
						}
						if(isset($rel_obj->$rel_aliasOne) && $rel_obj->$rel_aliasOne != false) {
							$rel = $rel_obj->$rel_aliasOne;
							$rel_o = $this->_valueConversionPostload($rel, $attrs);
							if(method_exists($rel_o, "_toArrayFrom")) {
								$rel_t_arr[$rel_aliasOne] = $rel_o->_toArrayFrom($aliases);
							} else {
								$rel_t_arr[$rel_aliasOne] = $rel_o->toArray();
							}
						} else {
							$rel_t_arr[$rel_aliasOne] = new \stdClass();
						}

						$addArr[] = $rel_t_arr;
					}
					return $addArr;
			}

		} else {
			switch($rel_type) {
				case "manyToMany":
				case "oneToMany":
				case "hasMany":
					return array();
				case "manyToOne":
				case "belongsTo":
				case "oneToOne":
				case "hasOne":
					return new \stdClass();
			}
		}
	}

	/* **********************
	 * Return JSON-String of this Object
	 */
	public function toJSON()
	{
		 return json_encode($this->toArray(), JSON_PRETTY_PRINT);
	}

	protected function _valueConversionPostload($obj, $attrs) {
		$o = $obj;
		foreach($attrs as $attr => $type) {
			if(isset($o->$attr)) {
				$o->$attr = $this->JSONconvert($o->$attr, $type);
			}
		}
		return $o;
	}

	protected function JSONconvert($attr, $type) {
		if($attr === null) return null;
		switch($type) {
			case "string":
				return (string)$attr;
			case "int":
				return (int)$attr;
			case "decimal":
			case "float":
				return (float)$attr;
			case "bool":
				return (bool)$attr;
			case "date":
			case "datetime":
			case "timestamp":
				return date('c', strtotime($attr));
			case "time":
				return date('c', strtotime($attr, 0));
			case "json":
				return json_decode($attr);
			default:
				return $attr;
		}
	}

}
