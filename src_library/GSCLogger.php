<?php 

/* ---------------------------------------------------------------------------------------
 * generic controller
 */

namespace GSCLibrary;

class GSCLogger extends \Phalcon\Logger\Adapter\File {

	/* -----------------------------------------------------------------------------------
	 * constructor
	 */
	public function __construct($name, $options = null) {
		parent::__construct($name, $options);
	}

	/* -----------------------------------------------------------------------------------
	 * access logged events
	 */
	public function getLogForLog () {
		$res = "";
		if ($this->_queue == null) return $res;
		foreach ($this->_queue as $message) {		
			$res .= $this->getFormatter()->format($message->getMessage(), $message->getType(), $message->getTime(), $message->getContext());
		}
		return $res;
	}

}
