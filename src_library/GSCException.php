<?php

/* ---------------------------------------------------------------------------------------
 * GSCException Class
 */

namespace GSCLibrary;

class GSCException extends \Exception {

	public static function raise ($message, $code) {
		$error = new \stdClass();
		$error->message = $message;
		$error->code = $code;
		throw new \GSCLibrary\GSCException($error);
	}

	public $error;

	public function __construct($error) {
		parent::__construct("GSCException");
		$this->error = $error;
	}

}
?>
