<?php

namespace GSCLibrary;

class GSCFinancialMath {

    protected static $_instance = null;
    public static function getInstance()
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self;
        }
        return self::$_instance;
    }
    
    protected function __clone() {}
    protected function __construct() {}
    private function __wakeup() {}

    /*
	 * Price float rounding to 5 cent values
	 */
	public function priceRound($price)
	{
        $ret = 0.0;
		if(!is_numeric($price)) {
			$price = (float)$price;
		}
		if(is_numeric($price)) {
            $p = round($price, 2);
			$ret = round(ceil($p * 20 - 0.5) / 20, 2);
		}	
		return $ret;
	}

	/*
	 * Price float rounding to 5 cent values, ever rounding down to next 5 cent value
	 * 5.00 -> 5.00
	 * 5.01 -> 5.00
	 * 5.02 -> 5.00
	 * 5.03 -> 5.00
	 * 5.04 -> 5.00
	 * 5.05 -> 5.05
	 * 5.06 -> 5.05
	 * 5.07 -> 5.05
	 * ...
	 */
	public function priceRoundFloor($price) {
		$ret = 0.0;
		if(!is_numeric($price)) {
			$price = (float)$price;
		}
		if(is_numeric($price)) {
            $p = round($price, 2);
			$ret = round(floor($p * 20) / 20, 2);
		}	
		return $ret;
    }
    
    /*
	 * Price float rounding to 5 cent values, ever rounding up to next 5 cent value
	 * 5.00 -> 5.00
	 * 5.01 -> 5.05
	 * 5.02 -> 5.05
	 * 5.03 -> 5.05
	 * 5.04 -> 5.05
	 * 5.05 -> 5.05
	 * 5.06 -> 5.10
	 * 5.07 -> 5.10
	 * ...
	 */
	public function priceRoundCeil($price) {
		$ret = 0.0;
		if(!is_numeric($price)) {
			$price = (float)$price;
		}
		if(is_numeric($price)) {
            $p = round($price, 2);
			$ret = round(ceil($p * 20) / 20, 2);
		}
		return $ret;
	}

	/*
	 * MWST Part of a Price Value, mwst unit: percent
	 */
	public function mwstPart($price, $mwstPercent = 0)
	{
		$ret = 0.0;
		if(is_numeric($price) && is_numeric($mwstPercent) && $mwstPercent != 0) {
			$m = 100 / $mwstPercent + 1;
			$ret = round($price / $m, 2);
		}
		return $ret;
	}
	
	/*
	 * The price excl. MWST (VAT) calculated from a price incl. MWST
	 */
	public function priceExclMwst($priceInclMwst, $mwstPercent = 0)
	{
		if(!is_numeric($priceInclMwst)) {
			$price = (float)$priceInclMwst;
		} else {
			$price = $priceInclMwst;
		}

        if(!is_numeric($mwstPercent)) {
			$percent = (float)$mwstPercent;
		} else {
			$percent = $mwstPercent;
		}

		return round($price - $this->mwstPart($price, $percent), 2);
	}

	/*
	 * The price incl. MWST (VAT) calculated from a price excl. MWST
	 */
	public function priceInclMwst($priceExclMwst, $mwstPercent = 0)
	{
		if(!is_numeric($priceExclMwst)) {
			$price = (float)$priceExclMwst;
		} else {
			$price = $priceExclMwst;
		}

        if(!is_numeric($mwstPercent)) {
			$percent = (float)$mwstPercent;
		} else {
			$percent = $mwstPercent;
		}

		return round($price * (1 + ($percent / 100)), 2);
	}
    
    /*
	 * Price Conversion String Output for floats
	 */
	public function priceConversion($price, $roundToFive = false, $decimalSymbol = ".", $thousandsSymbol = ",", $decimals = 2, $currency = "")
	{
		$retStr = "";
		if($roundToFive) {
			$ret = $this->priceRound($price);
		} else {
			$ret = $price;
		}

		$retVal = number_format($ret, $decimals, $decimalSymbol, $thousandsSymbol);

		switch($currency) {
			case "EUR":
				$retStr = $retVal . " €";
				break;
			case "CHF":
				$retStr = "CHF " . $retVal;
				break;
			default:
				$retStr = $retVal;
				break;
		}

		return $retStr;
    }

}
?>