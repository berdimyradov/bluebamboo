<?php 

namespace GSCLibrary;

//require_once "Mail.php";

/**
 * E-Mail Utility
 */
class EMailUtil {

	/* -----------------------------------------------------------------------------------
	 * get domain of current request
	 */
	public static function getDomain () {
		$domain = "http";
		if (array_key_exists("HTTPS", $_SERVER) && $_SERVER["HTTPS"] != "" && $_SERVER["HTTPS"] != "off") $domain .= "s";
		$domain .=  "://" . $_SERVER["HTTP_HOST"];
//		error_log($domain);
		return $domain;
	}

	/* -----------------------------------------------------------------------------------
	 * send text email using sendgrid service
	 */
	public static function sendTextEmail ($toMail, $toLabel, $fromMail, $fromLabel, $subject, $body) {
		$apiKey = getenv('send_grid_key');
		$sendgrid = new \SendGrid($apiKey);
		$sgmail = new \SendGrid\Mail();

		$from = new \SendGrid\Email($fromLabel, $fromMail);

		$personalization = new \SendGrid\Personalization();
		$to = new \SendGrid\Email($toLabel, $toMail);
		$personalization->addTo($to);

		$content = new \SendGrid\Content("text/plain", $body);

		$sgmail->setFrom($from);
		$sgmail->addPersonalization($personalization);
		$sgmail->setSubject($subject);
		$sgmail->addContent($content);
		//$sgmail->addHeader("Content-Type", "text/plain; charset=utf-8");
		//$sgmail->addHeader("Content-Transfer-Encoding", "quoted-printable");

		$response = $sendgrid->client->mail()->send()->post($sgmail);
		// TODO: error handling
	}

	/* -----------------------------------------------------------------------------------
	 * send html email using sendgrid service
	 */
	public static function sendHtmlEmail ($toMail, $toLabel, $fromMail, $fromLabel, $subject, $body) {
		$apiKey = getenv('send_grid_key');
		$sendgrid = new \SendGrid($apiKey);
		$sgmail = new \SendGrid\Mail();

		$from = new \SendGrid\Email($fromLabel, $fromMail);

		$personalization = new \SendGrid\Personalization();
		$to = new \SendGrid\Email($toLabel, $toMail);
		$personalization->addTo($to);

		$content = new \SendGrid\Content("text/html", $body);

		$sgmail->setFrom($from);
		$sgmail->addPersonalization($personalization);
		$sgmail->setSubject($subject);
		$sgmail->addContent($content);
		//$sgmail->addHeader("Content-Type", "text/plain; charset=utf-8");
		//$sgmail->addHeader("Content-Transfer-Encoding", "quoted-printable");

		$response = $sendgrid->client->mail()->send()->post($sgmail);
		// TODO: error handling
	}

	/* -----------------------------------------------------------------------------------
	 * send email with old method signature
	 */
	public static function sendEmail ($mailconfig, $to, $subject, $body) {
		EMailUtil::sendHtmlEmail($to, "", $mailconfig->sender, "", $subject, $body);
		return;
	
		$mail_subject_prefix = $mailconfig->subject_prefix;
		if (!isset($mail_subject_prefix)) $mail_subject_prefix = "";
			
		$from = $mailconfig->sender;
		$bcc = $mailconfig->bcc;

//		$smtp_ssl=yes;

		$headers = array (
			'From' => $from,   
			'To' => $to,  
			'Subject' => $mail_subject_prefix . $subject,
			'MIME-Version' => "1.0",
			"Content-Type" => "text/html; charset=UTF-8"
		); 
			
		$p = array (
				'host' => $mailconfig->smtp_host,
				'port' => $mailconfig->smtp_port,
				'auth' => $mailconfig->auth,
//				'auth' => 'plain',
				'username' => $mailconfig->smtp_user,     
				'password' => $mailconfig->smtp_password,
//				'debug' => true

			);

		$smtp = @\Mail::factory('smtp', $p);

		$mail = $smtp->send($to . "," . $bcc, $headers, $body);
		// TODO: Exception Handling
		if (\PEAR::isError($mail)) {
//			echo("<p>" . $mail->getMessage() . "</p>");
		}
		else {   
//			echo("<p>Message successfully sent!</p>");  
		}
	}
}
