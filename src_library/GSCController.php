<?php

/* ---------------------------------------------------------------------------------------
 * generic controller
 */

namespace GSCLibrary;

class GSCController extends \Phalcon\Mvc\Controller {

	private $errorInfo = null;

	protected function getFileSwitch()
	{
		return GSCFileSwitch::getInstance();
	}

	/* -----------------------------------------------------------------------------------
	 * redirect to login page
	 */
	public function goToLoginAction ($lang) {
		$response = new \Phalcon\Http\Response();
		$response->redirect("/$lang/admin/login");
		$response->send();
		return null;
	}

	/* -----------------------------------------------------------------------------------
	 * rollback transaction
	 */
	protected function rollback ($message, $errorInfo = null) {
		$trxManager = $this->transactionManager;
		if (!$trxManager->has()) {
			$this->logger->error("GSCController::rollback - no active transaction");
			throw new \Exception();
		}
		$this->errorInfo = $errorInfo;
		$transaction = $this->transaction;
		$transaction->rollback($message);
		$this->logger->error("GSCController::rollback - transaction rollback did not throw exception ($message)");
		throw new \Phalcon\MVC\Model\Transaction\Failed("rollback failed - $message");
	}

	/**
	 * wrap html api requests which uses transactions with potential rollback.
	 *
	 * @param function $fn function to be called and wrapped
	 *
	 * @return  void
	 * @throws  Exception
	 *
	 * @todo    nothing
	 *
	 * @since   2016-03-01
	 * @author  Michael Ammann <michael.ammann@guided-swarms.com>
	 *
	 * @edit    -<br />
	 */
	protected function htmlReadRequest ($fn) {
		$transaction = null;
//		$this->response->setContentType('application/json', 'UTF-8');
		try {
			$data = $fn($transaction);
			if (!isset($data) || ($data == null)) {
				try {throw new \Exception();}
				catch (\Exception $err) {
					$this->logger->error("error: " . json_encode($err->getTrace(), JSON_PRETTY_PRINT));
				}
			}
			$this->response->setContent($data);
			$this->response->setStatusCode(200);
			return $this->response;//->send();
		}
		catch (\Phalcon\MVC\Model\Transaction\Failed $err) {
			$this->response->setStatusCode(400, "Bad Request (tx failed)!");
			return $this->response;//->send();
		}
		catch (\Exception $err) {
			if ($err->getCode() == 403) {
				$this->response->setStatusCode(403, "forbidden");
			}
			else {
				$this->response->setStatusCode(400, "Bad Request: " . $err->getMessage());
			}
			return $this->response;//->send();
		}
	}

	/* -----------------------------------------------------------------------------------
	 * wrap html-view api requests which uses transactions with potential rollback
	 */
	protected function viewReadRequest ($fn) {
		$transaction = null;
//		$this->response->setContentType('application/json', 'UTF-8');
		try {
			$fn($transaction);
			//$this->response->setStatusCode(200);
			//return $this->response;//->send();
		}
		catch (\Phalcon\MVC\Model\Transaction\Failed $err) {
			$this->response->setStatusCode(400, "Bad Request (tx failed)!");
			return $this->response;//->send();
		}
		catch (\Exception $err) {
			if ($err->getCode() == 403) {
				$this->response->setStatusCode(403, "forbidden");
			}
			else {
				$this->response->setStatusCode(400, "Bad Request: " . $err->getMessage());
			}
			return $this->response;//->send();
		}
	}

	/* -----------------------------------------------------------------------------------
	 * wrap json api requests which uses transactions with potential rollback
	 */
	protected function jsonReadRequest ($fn) {
		$transaction = null;
		$this->response->setContentType('application/json', 'UTF-8');
		try {
			$res = $fn($transaction);
			if ($res === null) {
				$res = new \stdClass();
				$res->status = "ok";
			}
			$this->response->setJsonContent($res);
			$this->response->setStatusCode(200);
			return $this->response;//->send();
		}
		catch (\Phalcon\MVC\Model\Transaction\Failed $err) {
			if ($this->errorInfo != null) {
				$this->response->setJsonContent($this->errorInfo);
			}
			$this->response->setStatusCode(400, "Bad Request (tx failed)!");
			return $this->response;//->send();
		}
		catch (\Exception $err) {
			if ($this->errorInfo != null) {
				$this->response->setJsonContent($this->errorInfo);
			}
			if ($err->getCode() == 403) {
				$this->response->setStatusCode(403, "forbidden");
			}
			else {
				$this->response->setStatusCode(400, "Bad Request: " . $err->getMessage());
			}
			return $this->response;//->send();
		}
	}

	/* -----------------------------------------------------------------------------------
	 * wrap json api requests which uses transactions with potential rollback
	 */
	protected function jsonWriteRequest ($fn) {
		$transaction = null;
		$this->response->setContentType('application/json', 'UTF-8');
		try {
			$this->response->setStatusCode(200);
			$res = $fn($transaction);
			if ($res === null) {
				$res = new \stdClass();
				$res->status = "ok";
			}
			$this->response->setJsonContent($res);
			return $this->response;//->send();
		}
		catch (\Phalcon\MVC\Model\Transaction\Failed $err) {
			if ($this->errorInfo != null) {
				$this->response->setJsonContent($this->errorInfo);
			}
			$this->response->setStatusCode(400, "Bad Request (tx failed)!");
			return $this->response;//->send();
		}
		catch (\GSCLibrary\GSCException $err) {
			$this->response->setJsonContent($err->error);
			if ($transaction != null) {
				try {
					$transaction->rollback();
				}
				catch (\Exception $err2) {
					$this->logger->critical("was not able to rollback transaction (" . $err2->getMessage() . " / " . $err->getMessage() . ")");
					$this->logger->critical(json_encode(debug_backtrace(), JSON_PRETTY_PRINT));
				}
			}
			if ($err->getCode() == 403) {
				$this->response->setStatusCode(403, "Forbidden");
			}
			else {
				$this->response->setStatusCode(400, "Bad Request");
			}
			return $this->response;//->send();
		}
		catch (\Exception $err) {
			if ($this->errorInfo != null) {
				$this->response->setJsonContent($this->errorInfo);
			}
			if ($transaction != null) {
				try {
					$transaction->rollback();
				}
				catch (\Exception $err2) {
					$this->logger->critical("was not able to rollback transaction (" . $err2->getMessage() . " / " . $err->getMessage() . ")");
					$this->logger->critical(json_encode(debug_backtrace(), JSON_PRETTY_PRINT));
				}
			}
			if ($err->getCode() == 403) {
				$this->response->setStatusCode(403, "Forbidden: " . $err->getMessage());
			}
			else {
				$this->response->setStatusCode(400, "Bad Request: " . $err->getMessage());
			}
			return $this->response;//->send();
		}
	}

	/* -----------------------------------------------------------------------------------
	 * get sanitized data
	 */

	private $filter = null;

	public function getField ($field, $filter) {
		if ($this->filter == null) $this->filter = new \Phalcon\Filter();
		$data = $this->request->getJsonRawBody();
		if (!isset($data->$field)) throw new \Exception("missing parameter '" . $field . "'");
		$val = $this->filter->sanitize($data->$field, $filter);
		if ($val != $data->$field) throw new \Exception("parameter '" . $field . "' contains invalid content");
		return $val;
	}

	/* -----------------------------------------------------------------------------------
	 * hasRole checks if current user has a specific role
	 */
	protected function hasRole ($role) {
		if ($role == "user") {
			if ($this->hasRole("p-admin")) return true;
			if ($this->hasRole("p-support")) return true;
		}
		if ($role == "p-support") {
			if ($this->hasRole("p-admin")) return true;
		}
		return \GSCLibrary\UserRoleUtil::hasRole($this->logger, $this->session, $role);
	}

	/* -----------------------------------------------------------------------------------
	 * in edit mode
	 */
	protected function inWYSIWYGEditMode () {
		/*
		if ($this->session->has("gsc_cms_site_state")) {
			$state = $this->session->get("gsc_cms_site_state");
			if ($state == "s") return false; // no edit in setup-mode
		}
		*/
		$preview = $this->request->getQuery("preview");
		if (isset($preview) && ($preview == "true")) {
			return false;
		}
		return $this->hasRole("user");
	}

	/* -----------------------------------------------------------------------------------
	 * in preview mode
	 */
	protected function inPreviewMode () {
		if ($this->request->getQuery("preview") == null) return false;
		return $this->request->getQuery("preview") == "true";
	}

	/* -----------------------------------------------------------------------------------
	 * requiresRole throws an exception in case current user does not have a required role
	 */
	protected function requiresRole ($role) {
		if ($role == "user") {
			if ($this->hasRole("p-admin")) return;
			if ($this->hasRole("p-support")) return;
		}
		if ($role == "p-support") {
			if ($this->hasRole("p-admin")) return;
		}
		if (!$this->hasRole($role)) {
			throw new \Exception("permission denied", 403);
		}
	}

	/* -----------------------------------------------------------------------------------
	 * startsWith and endsWith utility --> TODO: move to utility class
	 * returns site id based on host or active site in edit mode
	 */
	protected function startsWith ($str, $test) {
		return substr( $str, 0, strlen( $test ) ) == $test;
	}

	protected function endsWith ($str, $test) {
		return substr( $str, -strlen( $test ) ) == $test;
	}

	/* ------------------------------------------------------------------------------------
	 * path Check and Creation if not existing
	 */
	protected function condCreateDirectory ($dir) {
		if (!file_exists($dir)) {
			if (!mkdir($dir, 0777, true)) {
				// TODO: Exception Handling
				throw new \Exception("Directory could not be created", 500);
			}
		}
		return $dir;
	}

	/* -----------------------------------------------------------------------------------
	 * getSysAdminPath
	 * returns the sys admin path
	 */
	protected function getSysAdminPath () {
//		$this->logger->log("get sys admin path");
		$bagName = "default";
		$bag = new \Phalcon\Session\Bag("bag-" . $bagName);
		if ($bag->has("sys_admin_path")) {
			return $bag->sys_admin_path;
		}
		/*
		$siteId = $this->getSiteId();
//		$this->logger->log("site id " . $siteId);
		$site = \Models\V1\CMS\Sites::findFirstById($siteId);
		if (!$site) {
			throw new \Exception("sys admin path not found (1/$siteId)");
		}
		
		$bag->sys_admin_path = $site->platform->admin_path;
		return $bag->sys_admin_path;
		*/
		$bag->sys_admin_path = "bluebamboo";
		return $bag->sys_admin_path;
	}

	/* -----------------------------------------------------------------------------------
	 * getSiteDataPath
	 * returns site id based data path
	 */
	protected function getSiteDataPath($subPath = "")
	{
		return $this->getFileSwitch()->getSiteDataPath($this->getSysAdminPath(), $this->getSiteId(), $subPath);
	}

	/* -----------------------------------------------------------------------------------
	 * getClientDataPath
	 * returns client id based data path
	 */
	protected function getClientDataPath($subPath = "")
	{
		return $this->getFileSwitch()->getClientDataPath($this->getSysAdminPath(), $subPath);
	}

	/* -----------------------------------------------------------------------------------
	 * getClientPDFPath
	 * returns client based pdf path
	 */
	protected function getClientPDFPath() {
		return $this->getFileSwitch()->getClientPDFPath($this->getSysAdminPath());
	}

	/* -----------------------------------------------------------------------------------
	 * getClientInvoDocPath
	 * returns client based invo doc path
	 */
	protected function getClientInvoDocPath()
	{
		return $this->getFileSwitch()->getClientInvoDocPath($this->getSysAdminPath());
	}

	/* -----------------------------------------------------------------------------------
	 * getClientImagePath
	 * returns client based image path
	 */
	protected function getClientImagePath()
	{
		return $this->getFileSwitch()->getClientImagePath($this->getSysAdminPath());
	}

	/* -----------------------------------------------------------------------------------
	 * getClientICSPath
	 * returns client based path for ics files
	 */
	protected function getClientICSPath()
	{
		return $this->getFileSwitch()->getClientICSPath($this->getSysAdminPath());
	}

	/* -----------------------------------------------------------------------------------
	 * getTempPath
	 * returns full path for temporary files
	 */
	protected function getTempPath()
	{
		return $this->getFileSwitch()->getTempPath();
	}

	/* -----------------------------------------------------------------------------------
	 * get session bag based on host
	 */

	protected function getSessionBag () {
		$host = $this->request->getHeader("Host");
		$parts = explode(".", $host);
		$bagName = "default";
		if (count($parts) == 3) {
			if (is_numeric($parts[0])) {
				$bagName = "" . $parts[0];
			}
		}
		return new \Phalcon\Session\Bag("bag-" . $bagName);
	}

	/* -----------------------------------------------------------------------------------
	 * prepareSession
	 * puts all the relevant information from the site and platform into the session
	 */
	private function prepareSession ($site, $bagName = "default") {

		$bag = new \Phalcon\Session\Bag("bag-" . $bagName);

		$platform = $site->platform;
		if (!$platform) {
//			$this->logger->log("platform not found for site " . $site->id);
			throw new \Exception("domain/site/platform not valid");
		}

		$bag->site_id = $site->id;
		$bag->site_navigation = $site->navigation;

		if ($site->default_page) {
			$bag->site_default_page = $site->default_page;
		}
		else {
			$bag->remove("site_default_page");
		}
		$bag->site_meta_css = "{}";
		$bag->site_css_version = 0;
		if (isset($site->meta_css) && ($site->meta_css != null)) {
			$bag->site_meta_css = $site->meta_css;
			$css = json_decode($site->meta_css);
			if (isset($css->version)) {
				$bag->site_css_version = intval($css->version);
			}
		}
		$bag->sys_admin_path = $platform->admin_path;
		$bag->sys_title = $platform->title;
		$bag->sys_footer = $platform->footer;
		$bag->sys_logo = $platform->logo;
		if (isset($site->tracking_ga) && ($site->tracking_ga != null) && (trim($site->tracking_ga) != "")) {
			$bag->site_tracking_ga = $site->tracking_ga;
		}
		else {
			$bag->remove("site_tracking_ga");
		}
		if (isset($site->tracking_fb) && ($site->tracking_fb != null) && (trim($site->tracking_fb) != "")) {
			$bag->site_tracking_fb = $site->tracking_fb;
		}
		else {
			$bag->remove("site_tracking_fb");
		}
		if (isset($site->title_prefix) && ($site->title_prefix != null) && (trim($site->title_prefix) != "")) {
			$bag->site_title_prefix = $site->title_prefix;
		}
		else {
			$bag->site_title_prefix = "";
		}
		if (isset($site->metadata) && ($site->metadata != null) && (trim($site->metadata) != "")) {
			$bag->site_metadata = $site->metadata;
		}
		else {
			$bag->remove("site_metadata");
		}
		return $bag;
	}

	/* -----------------------------------------------------------------------------------
	 * getSiteId
	 * returns site id based on host or active site in edit mode

	 	domains in platforms-model: :my.bluebamboo.ch:my.bluebamboo.de:localhost:

		localhost
	     --> illegal

		domain.ch
         --> illegal

		www.platformdomain.ch, e.g. my.bluebamboo.ch
	     --> site id keine???

		kundendomain.platformdomain.ch, e.g. shiatsu-winterthur.bluembamoo.ch
	     --> site id der kunden-domain

		www.kundendomain.platformdomain.ch, e.g. www.shiatsu-winterthur.bluebamboo.ch?
         --> vorerst illegal

		siteid.platformdomain.ch, e.g. 121.bluebamboo.ch
	     --> site id gemäss prefix

		www.kundendomain.ch, e.g. www.shiatsu-winterthur.ch
	     --> site id der kunden-domain

		kundendomain.ch, e.g. shiatsu-winterthur.ch
	     --> site id der kunden-domain

		siteid.kundendomain.ch, e.g. 121.shiatsu-winterthur.ch
	     --> site id gemäss prefix

		cookies: gsc_cms_site_id
		 --> nur in edit mode

		session: site_id


		precedence?

			siteid.whatever.ch

			if edit mode: gsc_cms_site_id / cookie

			if not edit mode: session site_id if set

			kundendomain.ch und kundendomain.platformdomain.ch gleichwertig (SEO?)

	 */
	protected function getSiteId ($edit = false) {
//		$edit = $this->session->has('auth-identity');

//		$this->logger->log("getSiteId, edit (1): $edit");

		$edit = $this->inWYSIWYGEditMode();

//		$this->logger->log("getSiteId, edit (2): $edit");

		$host = $this->request->getHeader("Host");
		$parts = explode(".", $host);

		$level1 = null;
		$level2 = null;
		$level3 = null;

		if (count($parts) == 2) {
			$level1 = $parts[1];
			$level2 = $parts[0];
		}
		else if (count($parts) == 3) {
			$level1 = $parts[2];
			$level2 = $parts[1];
			$level3 = $parts[0];
		}
		else {
			throw new \Exception("getSiteId - illegal number of elements - " . $host);
		}

/*
		$this->logger->log("host parts: $level1 - $level2 - $level3");
		$this->logger->log("is int: " . is_numeric($level3));
		$this->logger->log("edit: " . $edit);
*/
		if (is_numeric($level3)) {
			$bag = new \Phalcon\Session\Bag("bag-" . $level3);
			$site = \Models\V1\CMS\Sites::findFirstById(intval($level3));
			$this->prepareSession($site, "" . $level3);
			return $bag->site_id;
		}

		$bag = new \Phalcon\Session\Bag("bag-default");

		$show = $edit;
		if ($this->inPreviewMode()) $show = true;

		if ($show) {
			if ($this->cookies->has("gsc_cms_site_id")) {
				$eid = $this->cookies->get("gsc_cms_site_id")->getValue('trim');
//				$this->logger->log("getSiteId, has cookie gsc_cms_site_id: " . $eid);
				$site = \Models\V1\CMS\Sites::findFirstById($eid);
//				if (!$site) {
//					$this->logger->log("no site entry for " . $eid . " found");
//					throw new \Exception("site not valid: $eid");
//				}
				$x = intval($eid);
//				$this->logger->log("return " . $x);
				return $x;
			}
//			$this->logger->log("getSiteId, does not have cookie gsc_cms_site_id");
		}
		else if ($bag->has("site_id")) {
//			$this->logger->log("return bag site id: " . $bag->site_id);
//			return $bag->site_id;
		}

//		$this->logger->log("getSiteId, find by domain $host");
		$domain = \Models\V1\CMS\Domains::findFirstByDomain($host);
		if ($domain) {
			$site_id = $domain->site_id;
//			$this->logger->log("getSiteId, find by domain $host, found $site_id");
			$bag = $this->prepareSession($domain->site);
//			$this->logger->log("return domain-based bag site id: " . $bag->site_id . "/" . $site_id . "/" . $domain->site->id);
			return $bag->site_id;
		}

//		$this->logger->log("getSiteId, find by domain $host, not found");
		throw new \Exception("getSiteId for '$host' failed");
	}

	/* -----------------------------------------------------------------------------------
	 * afterExecuteRoute
	 * hook to log failed requests
	 */
    public function afterExecuteRoute($dispatcher) {
    	if ($this->response->getStatusCode() != 200) {
//			$this->logger->log("failed request: [" . $this->response->getStatusCode() . "]" . $dispatcher->getControllerName() . ", " . $dispatcher->getActionName() . ", " . json_encode($dispatcher->getParams()) . ", " . json_encode($dispatcher->getReturnedValue(), JSON_PRETTYPRINT));
		}
    }

	/* -----------------------------------------------------------------------------------
	 * afterExecuteRoute
	 * hook to log failed requests
	 */
    protected function logExceptionStackTrace($msg, $e) {
		if (!getenv('log_exception_stacktrace')) return;
		if (getenv('log_exception_stacktrace') != 'true') return;
		error_log("x-trace, $msg --> " . $e->getTraceAsString());
    }

	/* -----------------------------------------------------------------------------------
	 * returns current version
	 */
    protected function versionString() {
    	return GSCAssetsIncludes::APP_VERSION;
    }
}
