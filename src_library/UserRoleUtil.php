<?php 

namespace GSCLibrary;

class UserRoleUtil {

	public static function hasRole ($logger, $session, $role) {
		if (!$session->has("auth-identity-profile")) return false;
		$aip = $session->get("auth-identity-profile");
		if ($aip != $role) {
//			error_log("don't have role $role, my role is $aip");
			return false;
		}
		return true;

		/*
		if (!$session->has("user_role")) {
//			$logger->log(__METHOD__ . "/" . __LINE__);
			return false;
		}
		$user_roles = json_decode(preg_replace('/\s+/', '', $session->get("user_role")));
		$roles = explode(",", preg_replace('/\s+/', '', $role));

//		$logger->log("user_roles " . json_encode($user_roles, JSON_PRETTY_PRINT));
//		$logger->log("roles " . json_encode($roles, JSON_PRETTY_PRINT));

		$intersect = array_intersect($user_roles, $roles);
		$num = count($intersect);
//		$logger->log("intersect " . $num);
		return $num > 0;
		*/
	}
	
}
