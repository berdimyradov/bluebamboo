<?php 

namespace Api\V1\Logic;

/**
 * Session Logic
 */
class SessionLogic
{

	/*
	 * gets the visitors country based on a IP-lookup
	 */
	public static function getUserCountry ($session, $request) {
		if ($session->has("user_country")) {
			return $session->get("user_country");
		}
		try {
			$user_ip = $request->getClientAddress();
			if ($user_ip == "127.0.0.1") {
				$user_ip = "80.218.11.243";
			}
			$user_country = \GSCLibrary\GeoLocation::getCountry($user_ip);
			if ($user_country == "CH") {
			}
			else if ($user_country == "LI") {
				$user_country = "CH";
			}
			else if ($user_country == "DE") {
			}
			else {
				$user_country = "CH";	// OTHER? TODO
			}
			$session->set("user_country", $user_country);
			return $user_country;
		}
		catch (\Exception $err) {
			$logger = new \Phalcon\Logger\Adapter\File("../app/logs/trace.txt");
			$logger->log("SessionLogic.getUserCountry failed: " . $err->getMessage());
			$session->set("user_country", "CH");
			return "CH";
		}
	}

	/*
	 * return user currency
	 */
	public static function getUserCurrency ($session, $request) {
		$country = SessionLogic::getUserCountry($session, $request); //$session->get("user_country");
		if (!isset($country)) {
			throw new \Exception("getUserCurrency failed: country not set");
		}
		$currency = "EUR";
		if ($country == "CH") $currency = "CHF";
		if ($country == "LI") $currency = "CHF";
		return $currency;
	}

}
