<?php

namespace GSCLibrary;

class SystemInfo {

	const OS_UNKNOWN = 1;
	const OS_WIN = 2;
	const OS_LINUX = 3;
	const OS_OSX = 4;

	/**
	 * @return int
	 */
	static public function getOS() {
		switch (true) {
			case stristr(PHP_OS, 'DAR'): return self::OS_OSX;
			case stristr(PHP_OS, 'WIN'): return self::OS_WIN;
			case stristr(PHP_OS, 'LINUX'): return self::OS_LINUX;
			default : return self::OS_UNKNOWN;
		}
	}

	// ! Deprecated ! Don't use anymore! ---------------------------------------
	// Use getDataPath instead!
	static public function platformPrefixedPath ($subPath = "") {
		$pp = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "platforms" . DIRECTORY_SEPARATOR . $subPath;
		$pp = str_replace(["/","\\"], DIRECTORY_SEPARATOR , $pp);
		if (!file_exists($pp)) {
			$pp = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "platforms" . DIRECTORY_SEPARATOR . $subPath;
			$pp = str_replace(["/","\\"], DIRECTORY_SEPARATOR , $pp);
			// TODO: Maybe Exception Handling if this DIR also not exists?
		}
		return $pp;
	}

	// ! Deprecated ! Don't use anymore! ---------------------------------------
	// Use getDataPath instead!
	static public function prefixedPath ($subPath = "") {
		$pp = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $subPath;
		$pp = str_replace(["/","\\"], DIRECTORY_SEPARATOR , $pp);
		if (!file_exists($pp)) {
			$pp = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . $subPath;
			$pp = str_replace(["/","\\"], DIRECTORY_SEPARATOR , $pp);
			// TODO: Maybe Exception Handling if this DIR also not exists?
		}
		return $pp;
	}

	static public function getDataPath($subPath = "")
	{
		$data_path = getenv("data_path") . DIRECTORY_SEPARATOR;
		if($subPath != "") {
			$data_path .= $subPath . DIRECTORY_SEPARATOR;
		}
		return str_replace(["/","\\"], DIRECTORY_SEPARATOR , $data_path);
	}

	static public function appPath ($subPath = DIRECTORY_SEPARATOR) {
		$appPath = "";
		if ($_SERVER['SERVER_NAME'] == "schoggi-test.myswisschocolate.ch") {
			$appPath = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "app";
		}
		else if (!defined('PHP_WINDOWS_VERSION_MAJOR') && (($_SERVER['SERVER_ADDR'] == "127.0.0.1") || ($_SERVER['SERVER_ADDR'] == "::1"))) {
			$appPath = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "app";
		}
		else if ($_SERVER['HTTP_HOST'] == "bb-gsc-staging.azurewebsites.net") {
			$appPath = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "app";
		}
		else if ($_SERVER['SERVER_NAME'] == "www.meik.de") {
			$appPath = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "app";
		}
		else {
			$appPath = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "app";
		}
		$appPath = $appPath . $subPath;
		return str_replace(["/","\\"], DIRECTORY_SEPARATOR , $appPath);
	}

	static public function randomStr($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyz')
	{
	    $str = '';
	    $max = mb_strlen($keyspace, '8bit') - 1;
	    for ($i = 0; $i < $length; ++$i) {
	        $str .= $keyspace[random_int(0, $max)];
	    }
	    return $str;
	}

	static public function uniqueFilename($filename_extension = "") {
		$session = \Phalcon\Di::getDefault()->getSession();
		$client_id = $session->get("auth-identity-client");
		$prefix = $client_id . "_";
		$filename = uniqid($prefix);
		$filename .= self::randomStr(3);
		if ($filename_extension != "") $filename .= "." . $filename_extension;
		return $filename;
	}

	/***************************************************************************
	 * Get the Mime Type of a File depending on
	 * which Functions are available for that
	 */
	static public function getMimeType($file)
	{

		if (function_exists("mime_content_type")) {
    		return mime_content_type($file);
		} else if (function_exists("finfo_file")) {
		    $finfo = finfo_open(FILEINFO_MIME_TYPE);
		    $mime = finfo_file($finfo, $file);
		    finfo_close($finfo);
		    return $mime;
  		} else if (!stristr(ini_get("disable_functions"), "shell_exec")) {
		    // http://stackoverflow.com/a/134930/1593459
		    $file = escapeshellarg($file);
		    $mime = shell_exec("file -bi " . $file);
		    return $mime;
		} else {
			$idx = pathinfo($file, PATHINFO_EXTENSION);
		    $mimet = array(
		        'txt' => 'text/plain',
		        'htm' => 'text/html',
		        'html' => 'text/html',
		        'php' => 'text/php',
				'php3' => 'text/php',
				'php4' => 'text/php',
				'php5' => 'text/php',
		        'css' => 'text/css',
		        'js' => 'application/javascript',
		        'json' => 'application/json',
		        'xml' => 'application/xml',
		        'swf' => 'application/x-shockwave-flash',
		        'flv' => 'video/x-flv',
				'coffee' => 'application/vnd.coffeescript',
				'pug' => 'text/plain',
				'ts' => 'text/plain',

		        // images
		        'png' => 'image/png',
		        'jpe' => 'image/jpeg',
		        'jpeg' => 'image/jpeg',
		        'jpg' => 'image/jpeg',
		        'gif' => 'image/gif',
		        'bmp' => 'image/bmp',
		        'ico' => 'image/vnd.microsoft.icon',
		        'tiff' => 'image/tiff',
		        'tif' => 'image/tiff',
		        'svg' => 'image/svg+xml',
		        'svgz' => 'image/svg+xml',

		        // archives
		        'zip' => 'application/zip',
		        'rar' => 'application/x-rar-compressed',
		        'exe' => 'application/x-msdownload',
		        'msi' => 'application/x-msdownload',
		        'cab' => 'application/vnd.ms-cab-compressed',
				'7z' => 'application/x-7z-compressed',

		        // audio/video
				'mp2' => 'audio/mpeg',
		        'mp3' => 'audio/mpeg',
				'mp4' => 'audio/mpeg4',
		        'qt' => 'video/quicktime',
		        'mov' => 'video/quicktime',
				'mpa' => 'video/mpeg',
				'mpe' => 'video/mpeg',
				'mpeg' => 'video/mpeg',
				'mpg' => 'video/mpeg',
				'wav' => 'audio/x-wav',
				'avi' => 'video/x-msvideo',

		        // adobe
		        'pdf' => 'application/pdf',
		        'psd' => 'image/vnd.adobe.photoshop',
		        'ai' => 'application/postscript',
		        'eps' => 'application/postscript',
		        'ps' => 'application/postscript',

		        // ms office
		        'doc' => 'application/msword',
		        'rtf' => 'application/rtf',
		        'xls' => 'application/vnd.ms-excel',
		        'ppt' => 'application/vnd.ms-powerpoint',
		        'docx' => 'application/msword',
		        'xlsx' => 'application/vnd.ms-excel',
		        'pptx' => 'application/vnd.ms-powerpoint',

		        // open office
		        'odt' => 'application/vnd.oasis.opendocument.text',
		        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
    		);

		    if (isset($mimet[$idx])) {
				return $mimet[$idx];
		    } else {
				return 'application/octet-stream';
		    }
		}
	}

}

?>
