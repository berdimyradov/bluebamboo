<?php 

namespace GSCLibrary;

class ImageResize {
/**
 * easy image resize function
 * @param  $file - file name to resize
 * @param  $string - The image data, as a string
 * @param  $width - new image width
 * @param  $height - new image height
 * @param  $proportional - keep image proportional, default is no
 * @param  $output - name of the new file (include path if needed)
 * @param  $delete_original - if true the original image will be deleted
 * @param  $use_linux_commands - if set to true will use "rm" to delete the image, if false will use PHP unlink
 * @param  $quality - enter 1-100 (100 is best quality) default is 100
 * @param  $grayscale - if true, image will be grayscale (default is false)
 * @return boolean|resource
 */
 public static function smart_resize_image(
			$file,
			$string             = null,
			$width              = 0, 
			$height             = 0, 
			$proportional       = false, 
			$output             = 'file', 
			$delete_original    = true, 
			$use_linux_commands = false,
			$quality            = 100,
			$grayscale          = false,
			&$img_width,
			&$img_height
  		 ) {
      
		ini_set('memory_limit', '256M');
		if ( $height <= 0 && $width <= 0 ) return false;
		if ( $file === null && $string === null ) return false;

    # Setting defaults and meta
    $info                         = $file !== null ? getimagesize($file) : getimagesizefromstring($string);
    $img_width = $info[0];
    $img_height = $info[1];
    $image                        = '';
    $final_width                  = 0;
    $final_height                 = 0;
    list($width_old, $height_old) = $info;
	$cropHeight = $cropWidth = 0;

    # Calculating proportionality
    if ($proportional) {
      if      ($width  == 0)  $factor = $height/$height_old;
      elseif  ($height == 0)  $factor = $width/$width_old;
      else                    $factor = min( $width / $width_old, $height / $height_old );

      $final_width  = round( $width_old * $factor );
      $final_height = round( $height_old * $factor );
    }
    else {
      $final_width = ( $width <= 0 ) ? $width_old : $width;
      $final_height = ( $height <= 0 ) ? $height_old : $height;
	  $widthX = $width_old / $width;
	  $heightX = $height_old / $height;
	  
	  $x = min($widthX, $heightX);
	  $cropWidth = ($width_old - $width * $x) / 2;
	  $cropHeight = ($height_old - $height * $x) / 2;
    }

    # Loading image to memory according to type
    switch ( $info[2] ) {
      case IMAGETYPE_JPEG:  $file !== null ? $image = imagecreatefromjpeg($file) : $image = imagecreatefromstring($string);  break;
      case IMAGETYPE_GIF:   $file !== null ? $image = imagecreatefromgif($file)  : $image = imagecreatefromstring($string);  break;
      case IMAGETYPE_PNG:   $file !== null ? $image = imagecreatefrompng($file)  : $image = imagecreatefromstring($string);  break;
      default: return false;
    }
    
    # Making the image grayscale, if needed
    if ($grayscale) {
      imagefilter($image, IMG_FILTER_GRAYSCALE);
    }    
    
    # This is the resizing/resampling/transparency-preserving magic
    $image_resized = imagecreatetruecolor( $final_width, $final_height );
    if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
      $transparency = imagecolortransparent($image);
      $palletsize = imagecolorstotal($image);

      if ($transparency >= 0 && $transparency < $palletsize) {
        $transparent_color  = imagecolorsforindex($image, $transparency);
        $transparency       = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
        imagefill($image_resized, 0, 0, $transparency);
        imagecolortransparent($image_resized, $transparency);
      }
      elseif ($info[2] == IMAGETYPE_PNG) {
        imagealphablending($image_resized, false);
        $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
        imagefill($image_resized, 0, 0, $color);
        imagesavealpha($image_resized, true);
      }
    }
    imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);
	
	
    # Taking care of original, if needed
    if ( $delete_original ) {
      if ( $use_linux_commands ) exec('rm '.$file);
      else @unlink($file);
    }

    # Preparing a method of providing result
    switch ( strtolower($output) ) {
      case 'browser':
        $mime = image_type_to_mime_type($info[2]);
        header("Content-type: $mime");
        $output = NULL;
      break;
      case 'file':
        $output = $file;
      break;
      case 'return':
        return $image_resized;
      break;
      default:
      break;
    }
    
    # Writing image according to type to the output destination and image quality
    switch ( $info[2] ) {
      case IMAGETYPE_GIF:   imagegif($image_resized, $output);    break;
      case IMAGETYPE_JPEG:  imagejpeg($image_resized, $output, $quality);   break;
      case IMAGETYPE_PNG:
        $quality = 9 - (int)((0.9*$quality)/10.0);
        imagepng($image_resized, $output, $quality);
        break;
      default: return false;
    }
	
	imagedestroy($image);
	imagedestroy($image_resized);

    return true;
  }



	public static function crop($src, $dst, $data, $logger) {
		// http://stackoverflow.com/questions/24337495/php-allowed-memory-size-exhausted-on-image-upload-rotation
		ini_set('memory_limit', '512M');
		$type = exif_imagetype($src);
		if (!$type) throw new \Exception("crop failed - no valid type detected");
		$extension = image_type_to_extension($type);

		if (!empty($src) && !empty($dst) && !empty($data)) {
			switch ($type) {
				case IMAGETYPE_GIF:
					$src_img = imagecreatefromgif($src);
					break;
				case IMAGETYPE_JPEG:
					$src_img = imagecreatefromjpeg($src);
					break;
				case IMAGETYPE_PNG:
					$src_img = imagecreatefrompng($src);
					break;
				default:
					throw new \Exception("crop failed - invalid type detected: " . $type);
			}
 			if (!$src_img) {
				throw new \Exception("crop failed - failed to read file: " . $src);
			}
		}

		$size = getimagesize($src);
		$size_w = $size[0]; // natural width
		$size_h = $size[1]; // natural height
		$src_img_w = $size_w;
		$src_img_h = $size_h;
		$degrees = $data -> rotate;
//		$e = exif_read_data($src);
//		$logger->log("image-exif: " . json_encode($e));
//		$logger->log("image: " . $extension . ", w=" . $size_w . ", h=" . $size_h . ", imgw=" . $src_img_w . ", imgh=" . $src_img_h);
		
		// Rotate the source image
		if (is_numeric($degrees) && $degrees != 0) {
			// PHP's degrees is opposite to CSS's degrees
			$new_img = imagerotate( $src_img, -$degrees, imagecolorallocatealpha($src_img, 0, 0, 0, 127) );
			imagedestroy($src_img);
			$src_img = $new_img;
			$deg = abs($degrees) % 180;
			$arc = ($deg > 90 ? (180 - $deg) : $deg) * M_PI / 180;
			$src_img_w = $size_w * cos($arc) + $size_h * sin($arc);
			$src_img_h = $size_w * sin($arc) + $size_h * cos($arc);
			// Fix rotated image miss 1px issue when degrees < 0
			$src_img_w -= 1;
			$src_img_h -= 1;
		}
		$tmp_img_w = $data -> width;
		$tmp_img_h = $data -> height;
		
		$dst_img_w = 220;
		$dst_img_h = 220;
		
		$dst_img_w = $tmp_img_w;
		$dst_img_h = $tmp_img_h;
		
		$src_x = $data -> x;
		$src_y = $data -> y;
		if ($src_x <= -$tmp_img_w || $src_x > $src_img_w) {
			$src_x = $src_w = $dst_x = $dst_w = 0;
		} else if ($src_x <= 0) {
			$dst_x = -$src_x;
			$src_x = 0;
			$src_w = $dst_w = min($src_img_w, $tmp_img_w + $src_x);
		} else if ($src_x <= $src_img_w) {
			$dst_x = 0;
			$src_w = $dst_w = min($tmp_img_w, $src_img_w - $src_x);
		}
		if ($src_w <= 0 || $src_y <= -$tmp_img_h || $src_y > $src_img_h) {
			$src_y = $src_h = $dst_y = $dst_h = 0;
		} else if ($src_y <= 0) {
			$dst_y = -$src_y;
			$src_y = 0;
			$src_h = $dst_h = min($src_img_h, $tmp_img_h + $src_y);
		} else if ($src_y <= $src_img_h) {
			$dst_y = 0;
			$src_h = $dst_h = min($tmp_img_h, $src_img_h - $src_y);
		}
		// Scale to destination position and size
		$ratio = $tmp_img_w / $dst_img_w;
		$dst_x /= $ratio;
		$dst_y /= $ratio;
		$dst_w /= $ratio;
		$dst_h /= $ratio;
		$dst_img = imagecreatetruecolor($dst_img_w, $dst_img_h);
		// Add transparent background to destination image
		imagefill($dst_img, 0, 0, imagecolorallocatealpha($dst_img, 0, 0, 0, 127));
		imagesavealpha($dst_img, true);
		$result = imagecopyresampled($dst_img, $src_img, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
		if ($result) {
			if (!imagepng($dst_img, $dst)) {
				throw new \Exception("Failed to save the cropped image file");
			}
		} else {
			throw new \Exception("Failed to crop the image file");
		}
		imagedestroy($src_img);
		imagedestroy($dst_img);
	}

}
