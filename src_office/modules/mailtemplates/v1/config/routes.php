<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$api = new RouterGroup(array('module' => 'api_v1_mailtemplates'));
$api->setPrefix('/api/v1/mailtemplates');

$api->addGet	("/templates",			    array('controller' => 'Templates',	'action' => 'index'));
$api->addGet	("/templates/:int",		    array('controller' => 'Templates',	'action' => 'one',		'id' => 1));
// $api->addPost	("/templates",			array('controller' => 'Templates',	'action' => 'create'));
$api->addPut	("/templates/:int",		    array('controller' => 'Templates',	'action' => 'update',	'id' => 1));
// $api->addDelete	("/templates/:int",     array('controller' => 'Templates',	'action' => 'delete',	'id' => 1));

$api->addGet	("/role/:int",    array('controller' => 'Templates',	'action' => 'oneByRole',		'role_id' => 1));
$api->addGet	("/roles",		array('controller' => 'Templates',	'action' => 'indexRoles'));

$router->mount($api);
