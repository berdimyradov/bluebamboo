<?php

namespace Models\V1\Mailtemplates;

class Vars extends \Phalcon\Mvc\Model
{

	public $id;

	public $code;

	public $label;

	public $role_id;

	public static $attrs = array(
		'id' => 'int',
		'code' => 'string',
		'label' => 'string',
		'role_id' => 'int',
	);

	public static $relations = array(
		"role" => [
			"type" => "belongsTo",
			"alias" => "role",
			"relationModel" => "Models\V1\Mailtemplates\Roles",
			"relationIdAlias" => "role_id"
		],
	);

	public function columnMap()
	{
		return array(
			'id' => 'id',
			'code' => 'code',
			'label' => 'label',
			'role_id' => 'role_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
    {
		$this->setSource("gsc_templates_mail_vars");
	
		$this->belongsTo(
			"role_id",
			"Models\V1\Mailtemplates\Roles",
			"id",
			[
				"alias" => "role",
			]
		);
	}

}
