<?php

/**
 * Model for app "mailtemplates", class "Templates"
 */

namespace Models\V1\Mailtemplates;

class Templates extends \Phalcon\Mvc\Model
{

	public $id;
	public $template;
	public $role_id;
	public $client_id;

	public static $attrs = array(
		'id' => 'int',
		'subject' => 'string',
		'template' => 'string',
		'role_id' => 'int',
		'client_id' => 'int',
	);

	public static $relations = array(
		"role" => [
			"type" => "belongsTo",
			"alias" => "role",
			"relationModel" => "Models\V1\Mailtemplates\Roles",
			"relationIdAlias" => "role_id"
		],
		[
			"type" => "hasMany",
			"alias" => "vars",
			"relationModel" => "Models\V1\Mailtemplates\Vars",
			"relationIdAlias" => "role_id"
		],
		[
			"type" => "hasOne",
			"alias" => "templatebase",
			"relationModel" => "Models\V1\Mailtemplates\Templatebases",
			"relationIdAlias" => "role_id"
		],
	);

	public function columnMap()
	{
		return array(
			'id' => 'id',
			'subject' => 'subject',
			'template' => 'template',
			'role_id' => 'role_id',
			'client_id' => 'client_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
    {
		$this->setSource("gsc_templates_mail");

		$this->belongsTo(
			"role_id",
			"Models\V1\Mailtemplates\Roles",
			"id",
			[
				"alias" => "role",
			]
		);

		$this->hasMany(
            "role_id",
            "Models\V1\Mailtemplates\Vars",
			"role_id",
			[
				"alias" => "vars",
			]
		);
		
		$this->hasOne(
            "role_id",
            "Models\V1\Mailtemplates\Templatebases",
			"role_id",
			[
				"alias" => "templatebase",
			]
        );
	}

	public static function getTemplateByRoleAndClient(int $role_id, int $client_id)
	{
		$temp = self::findFirst("role_id = '" . $role_id . "' AND client_id = '" . $client_id. "'");
		if(!$temp) {
			$temp = \Models\V1\Mailtemplates\Templatebases::findFirstByRole_id($role_id);
			if(!$temp) return false;
		}
		return $temp;
	}

	public static function getMailSubject(int $role_id, int $client_id, array $vars)
	{
		$template_obj = self::getTemplateByRoleAndClient($role_id, $client_id);
		if(!$template_obj) return false;
		$subject = $template_obj->subject;
		return self::replaceVars($subject, $vars);
	}

	public static function getMailBody(int $role_id, int $client_id, array $vars)
	{
		$template_obj = self::getTemplateByRoleAndClient($role_id, $client_id);
		if(!$template_obj) return false;
		$template = $template_obj->template;
		return self::replaceVars($template, $vars);
	}

	private static function replaceVars(string $text, array $vars)
	{
		return preg_replace_callback(
			'/(%([A-Za-z0-9_\-.]+)%)/',
			function($treffer) use ($vars) {
				$var = $treffer[1];
				if(isset($vars[$var])) {
					return (string)$vars[$var];
				}
				return "";
			},
			$text
		);
	}

}
