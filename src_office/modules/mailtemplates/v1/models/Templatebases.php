<?php

namespace Models\V1\Mailtemplates;

class Templatebases extends \Phalcon\Mvc\Model
{

	public $id;
	public $subject;
	public $template;
	public $role_id;

	public static $attrs = array(
		'id' => 'int',
		'subject' => 'string',
		'template' => 'string',
		'role_id' => 'int',
	);

	public static $relations = array(
		"role" => [
			"type" => "belongsTo",
			"alias" => "role",
			"relationModel" => "Models\V1\Mailtemplates\Roles",
			"relationIdAlias" => "role_id"
		],
		"vars" => [
			"type" => "hasMany",
			"alias" => "vars",
			"relationModel" => "Models\V1\Mailtemplates\Vars",
			"relationIdAlias" => "role_id"
		],
	);

	public function columnMap()
	{
		return array(
			'id' => 'id',
			'subject' => 'subject',
			'template' => 'template',
			'role_id' => 'role_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
    {
		$this->setSource("gsc_templates_mail_bases");

		$this->belongsTo(
			"role_id",
			"Models\V1\Pdf\Roles",
			"id",
			[
				"alias" => "role",
			]
		);

		$this->hasMany(
            "role_id",
            "Models\V1\Mailtemplates\Vars",
			"role_id",
			[
				"alias" => "vars",
			]
		);
	}

}
