<?php

namespace Models\V1\Mailtemplates;

class Roles extends \Phalcon\Mvc\Model
{

	public $id;
	public $title;

	public static $attrs = array(
		'id' => 'int',
		'title' => 'string',
	);

	public function columnMap()
	{
		return array(
			'id' => 'id',
			'title' => 'title',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
    {
		$this->setSource("gsc_templates_mail_roles");
	}

}
