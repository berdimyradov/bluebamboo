<?php

/**
 * Controller class TemplatesController
 */

namespace Api\V1\Mailtemplates\Controllers;

class TemplatesController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Mailtemplates\Templates";

	/**
	 * returns data with the option to select fields, filter results and sort result.
	 * authentication required
	 */
	public function indexAction () {

		// site id or similar filter? just about access rights

		$default_projection		= array("o.id", "o.template", "o.role_id", "o.client_id");
		$orderby				= array("o.role_id", "o.id");
		$attrs_for_projection	= array("o.id", "o.template", "o.role_id", "o.client_id");
		$attrs_for_where		= array("o.id", "o.role_id", "o.client_id");
		$attrs_for_orderby		= array("o.id", "o.role_id", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/**
	 * lookup for index
	 */
	public function indexLookupAction () {
		$projection				= array("o.id");
		$lookup_fields			= array("id");
		$orderby				= array("o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->name = "";
			$l->name .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/**
	 * returns one Template
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneActionRelations($model, $id, $model::$relations, $postLoad);
	}

	/**
	 * returns one Template by its role_id, cause templates are like unique for each user and role
	 */
	public function oneByRoleAction($role_id) {
		return $this->jsonWriteRequest(function(&$transaction) use ($role_id) {
			$model = $this->from;
			$transaction = $this->transaction;

			$client_id = $this->grantAccessForClient($client_id);
			
			$template = $model::findFirst(
				[
					"conditions" => "client_id = '".$client_id."' AND role_id = '".$role_id."'",
				]
			);

			if(!$template) {
				$modelBases = "Models\V1\Mailtemplates\Templatebases";
				$base = $modelBases::findFirstByRole_id($role_id);
				if(!$base) {
					throw new \Exception("No Base for this Role available!", 400);
				}
				$data = new \stdClass();
				$data->template = $base->template;
				$data->subject = $base->subject;
				$data->role_id = $role_id;
				$data->client_id = $client_id;
				$template = $this->_createActionObj($data, $model, $model::$attrs);
				$this->transaction->commit();
			}

			$t = $this->_getActionAddRelations($template, $model::$relations);
			
			return $this->_valueConversionPostload($t, $model::$attrs);
		});
	}

	/**
	 * returns all available roles
	 */
	public function indexRolesAction() {
		return $this->jsonReadRequest(function(&$transaction) {
			$model = "Models\V1\Mailtemplates\Roles";
			$transaction = $this->transaction;
			$roles = $model::find();

			if(count($roles) == 0) {
				return array();
			}

			$ret = array();
			foreach($roles as $role) {
				$ret[] = $this->_valueConversionPostload($role, $model::$attrs);
			}
			return $ret;
		});
	}

	/**
	 * creates a new template
	 */
	public function createAction() {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);
		$model = $this->from;
		return $this->_createAction($model, $model::$attrs);
	}

	/**
	 * updates an existing invoice
	 */
	public function updateAction($id) {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);
		$model = $this->from;
		return $this->_updateAction($model, $model::$attrs, $id);
	}

	/**
	 * deletes an existing template
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

}
