<?php

/**
 * module-config for V1/API/Mailtemplates API
 */

namespace Api\V1\Mailtemplates;

use Phalcon\Loader,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\View,
    Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface
{
    public function registerAutoloaders(\Phalcon\DiInterface $dependencyInjector = null)
    {
        $loader = new Loader();
        $loader->registerNamespaces(
            array(
                'GSCLibrary'					    => '../library/',

                'Models\V1\Mailtemplates'		    => '../modules/mailtemplates/v1/models/',
                'Models\V1\Sys'					    => '../modules/sys/v1/models/',

                'Api\V1\Mailtemplates\Logic' 	    => '../modules/mailtemplates/v1/api/logic/',
                'Api\V1\Mailtemplates\Controllers' 	=> '../modules/mailtemplates/v1/api/controllers/',
            )
        );
        $loader->register();
    }

    public function registerServices(\Phalcon\DiInterface $di)
    {
        $di->set('dispatcher', function() {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace("Api\V1\Mailtemplates\Controllers");
            return $dispatcher;
        });

        $di->set('view', function() {
            $view = new View();
//            $view->setViewsDir('../app/api/v1/mailtemplates/views/');
            return $view;
        });
    }
}
