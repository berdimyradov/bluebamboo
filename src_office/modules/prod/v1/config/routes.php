<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$api = new RouterGroup(array('module' => 'api_v1_prod'));
$api->setPrefix('/api/v1/prod');

$api->addGet	("/products", array('controller' => 'Products', 'action' => 'index'));
$api->addGet	("/products/:int", array('controller' => 'Products', 'action' => 'one', 'id' => 1));
$api->addGet	("/products/:int/locations", array('controller' => 'Products', 'action' => 'locations', 'id' => 1));
$api->addGet	("/products/:int/employees", array('controller' => 'Products', 'action' => 'employees', 'id' => 1));
$api->addPost	("/products", array('controller' => 'Products', 'action' => 'create'));
$api->addPut	("/products/:int", array('controller' => 'Products', 'action' => 'update', 'id' => 1));
$api->addDelete	("/products/:int", array('controller' => 'Products', 'action' => 'delete', 'id' => 1));

$api->addGet	("/pricegroups", array('controller' => 'Pricegroups', 'action' => 'index'));
$api->addGet	("/pricegroups/:int", array('controller' => 'Pricegroups', 'action' => 'one', 'id' => 1));
$api->addPost	("/pricegroups", array('controller' => 'Pricegroups', 'action' => 'create'));
$api->addPut	("/pricegroups/:int", array('controller' => 'Pricegroups', 'action' => 'update', 'id' => 1));
$api->addDelete	("/pricegroups/:int", array('controller' => 'Pricegroups', 'action' => 'delete', 'id' => 1));

$api->addGet	("/prices", array('controller' => 'Prices', 'action' => 'index'));
$api->addGet	("/prices/:int", array('controller' => 'Prices', 'action' => 'one', 'id' => 1));
$api->addPost	("/prices", array('controller' => 'Prices', 'action' => 'create'));
$api->addPut	("/prices/:int", array('controller' => 'Prices', 'action' => 'update', 'id' => 1));
$api->addDelete	("/prices/:int", array('controller' => 'Prices', 'action' => 'delete', 'id' => 1));

$api->addGet	("/discounts", array('controller' => 'Discounts', 'action' => 'index'));
$api->addGet	("/discounts/:int", array('controller' => 'Discounts', 'action' => 'one', 'id' => 1));
$api->addPost	("discounts", array('controller' => 'Discounts', 'action' => 'create'));
$api->addPut	("/discounts/:int", array('controller' => 'Discounts', 'action' => 'update', 'id' => 1));
$api->addDelete	("/discounts/:int", array('controller' => 'Discounts', 'action' => 'delete', 'id' => 1));

$api->addGet	("/offers", array('controller' => 'Offers', 'action' => 'index'));
$api->addGet	("/offers/:int", array('controller' => 'Offers', 'action' => 'one', 'id' => 1));
$api->addPost	("/offers", array('controller' => 'Offers', 'action' => 'create'));
$api->addPut	("/offers/:int", array('controller' => 'Offers', 'action' => 'update', 'id' => 1));
$api->addDelete	("/offers/:int", array('controller' => 'Offers', 'action' => 'delete', 'id' => 1));

$api->addGet	("/productparts", array('controller' => 'Productparts', 'action' => 'index'));
$api->addGet	("/productparts/:int", array('controller' => 'Productparts', 'action' => 'one', 'id' => 1));
$api->addPost	("/productparts", array('controller' => 'Productparts', 'action' => 'create'));
$api->addPut	("/productparts/:int", array('controller' => 'Productparts', 'action' => 'update', 'id' => 1));
$api->addDelete	("/productparts/:int", array('controller' => 'Productparts', 'action' => 'delete', 'id' => 1));

$api->addGet	("/tariffpositions", array('controller' => 'Tariffpositions', 'action' => 'index'));
$api->addGet	("/tariffpositions/:int", array('controller' => 'Tariffpositions', 'action' => 'one', 'id' => 1));
$api->addPost	("/tariffpositions", array('controller' => 'Tariffpositions', 'action' => 'create'));
$api->addPut	("/tariffpositions/:int", array('controller' => 'Tariffpositions', 'action' => 'update', 'id' => 1));
$api->addDelete	("/tariffpositions/:int", array('controller' => 'Tariffpositions', 'action' => 'delete', 'id' => 1));

$router->mount($api);
