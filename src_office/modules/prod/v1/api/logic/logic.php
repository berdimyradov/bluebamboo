<?php

namespace Api\V1\Prod\Logic;

class Logic extends \Phalcon\Mvc\User\Component {

	private $feedback = null;

	/* ---------------------------------------------------------------------------------
	 * constructor
	 */
	public function __construct($di) {
		$this->setDI($di);
	}

	/* ---------------------------------------------------------------------------------
	 * addError
	 */
	protected function addError ($field, $message) {
		if ($this->feedback == null) $this->feedback = array();
		$this->feedback[$field] = $message;
	}

	/* ---------------------------------------------------------------------------------
	 * applyFeedback
	 */
	protected function applyFeedback ($response) {
		if ($this->feedback == null) $response->fields = array();
		else $response->fields = $this->feedback;
	}

	/* ---------------------------------------------------------------------------------
	 * stop processing
	 */
	protected function stopProcessing ($field, $message, $response, $xmessage = "invalid state") {
		$this->addError($field, $message);
		$this->applyFeedback($response);
		throw new \Exception($xmessage);
	}

}
