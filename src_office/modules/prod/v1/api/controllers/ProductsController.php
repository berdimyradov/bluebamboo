<?php

namespace Api\V1\Prod\Controllers;

class ProductsController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Prod\Products";

	/* -----------------------------------------------------------------------------------
	 * index action
	 */
	public function indexAction () {
		// site id or similar filter? just about access rights
		$model					= $this->from;
		$default_projection		= array("o.id", "o.type", "o.title", "o.description", "o.duration", "o.breakAfter", "o.price", "o.active", "o.online_booking_available", "o.color", "o.client_id");
		$orderby				= array("o.title");
		$attrs_for_projection	= array("o.id", "o.type", "o.title", "o.description", "o.duration", "o.breakAfter", "o.price", "o.active", "o.online_booking_available", "o.color", "o.client_id");
		$attrs_for_where		= array("o.id", "o.type", "o.title", "o.duration", "o.breakAfter", "o.price", "o.active", "o.online_booking_available", "o.color", "o.client_id");
		$attrs_for_orderby		= array("o.id", "o.type", "o.title", "o.duration", "o.price", "o.active", "o.online_booking_available", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * index lookup action
	 */
	public function indexLookupAction () {
		$projection			= array("o.id", "o.title");
		$lookup_fields		= array("title");
		$orderby			= array("o.title", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->title = "";
			$l->title .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/* -----------------------------------------------------------------------------------
	 * action to list all locations for one item
	 */
	public function locationsAction($id) {
		return $this->_relationsAction($this->from, $id, "locations");
	}

	/* -----------------------------------------------------------------------------------
	 * action to list all locations for one item
	 */
	public function employeesAction($id) {
		return $this->_relationsAction($this->from, $id, "employees");
	}

	/* -----------------------------------------------------------------------------------
	 * one action
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		// Load phalcon Object
		$obj = $this->_oneActionObj($model, $id, $postLoad);

		// Output of the object
		return $this->_jsonResponse($this->_getActionAddRelations($obj, $model::$relations));
	}

	/* -----------------------------------------------------------------------------------
	 * create action
	 */
	public function createAction() {
		try {
			// Copied from creatActionRelations with modifying, maybe possible more generic...
			$modelClass = $this->from;
			$attrs = $modelClass::$attrs;
			$relations = $modelClass::$relations;
			$values = null;
			$valueConverter = null;

			$o = $this->request->getJsonRawBody();

			if(empty($o->productparts)) {
				throw new \Exception("No Invoice Positions defined", 400);
			}
			$prodparts = $o->productparts;
			$prod_price = 0;
			$prod_duration = 0;
			foreach($prodparts as $part) {
				$part_price = (float)$part->price;
				$prod_price += $part_price;
				$part_duration = (float)$part->duration;
				$prod_duration += $part_duration;
			}
			$o->price = $prod_price;
			$o->duration = $prod_duration;

			$obj = $this->_createActionObj($o, $modelClass, $attrs, $values, $valueConverter);
			$newId = $obj->id;

			foreach($relations as $relation) {
				if($relation["type"] == "manyToMany") {
					$alias = $relation["alias"];
					if(isset($o->$alias)) {
						$relClass = $relation["throughModel"];
						foreach($o->$alias as $rel) {
							$tia = $relation["throughIdAlias"];
							$rel->$tia = $newId;
							$this->_createActionObj($rel, $relClass, $relClass::$attrs, null, $valueConverter);
						}
					}
				}

				if($relation["type"] == "hasMany") {
					$alias = $relation["alias"];
					if(isset($o->$alias)) {
						$relClass = $relation["relationModel"];
						$ria = $relation["relationIdAlias"];
						foreach($o->$alias as $rel) {
							$rel->$ria = $newId;
							$this->_createActionObj($rel, $relClass, $relClass::$attrs, null, $valueConverter);
						}
					}
				}
			}

			$this->transaction->commit();

			return $this->_jsonResponse($obj);
		}
		catch (\Exception $err) {
			if ($err->getCode() == 403) {
				$this->response->setStatusCode(403, "forbidden");
			}
			else {
				$this->response->setStatusCode(400, "Bad Request: " . $err->getMessage());
			}
			return $this->response;
		}
	}

	/* -----------------------------------------------------------------------------------
	 * update action
	 */
	public function updateAction($id) {
		/* Old Part
		$model = $this->from;
		$values = array(
			//"active" => 0,
		);
		return $this->_updateActionRelations($model, $model::$attrs, $id, $model::$relations, $values);
		*/
		$modelClass = $this->from;
		$attrs = $modelClass::$attrs;
		$pk = $id;
		$relations = $modelClass::$relations;
		$values = null;
		$valueConverter = null;

		return $this->jsonWriteRequest(function(&$transaction) use ($modelClass, $attrs, $pk, $relations, $values, $valueConverter) {

			$transaction = $this->transaction;
			$o = $this->request->getJsonRawBody();

			if(empty($o->productparts)) {
				throw new \Exception("No Invoice Positions defined", 400);
			}
			$prodparts = $o->productparts;
			$prod_price = 0;
			$prod_duration = 0;
			foreach($prodparts as $part) {
				$part_price = (float)$part->price;
				$prod_price += $part_price;
				$part_duration = (float)$part->duration;
				$prod_duration += $part_duration;
			}
			$o->price = $prod_price;
			$o->duration = $prod_duration;

			$obj = $this->_updateActionObj($o, $modelClass, $attrs, $pk, $values, $valueConverter);

			foreach($relations as $relation) {
				$type = $relation["type"];
				switch($type) {
					case "manyToMany":
						$alias = $relation["alias"];
						if(isset($o->$alias)) {
							// Hilfsarray aus vorhandenen Relationen erstellen:
							$relObjs = array();
							$ta = $relation["throughAlias"];
							foreach($obj->$ta as $add) {
								$relObjs[] = $add;
							}
							$relClass = $relation["throughModel"];
							$rel1Alias = $relation["throughIdAlias"];
							$rel2Alias = $relation["throughRelIdAlias"];
							// Definierte Relationen auf Neu oder Update prüfen
							foreach($o->$alias as $rel) {
								$new = true;
								for($i = 0; $i < count($relObjs); $i++) {
									$relObj = $relObjs[$i];
									if($rel->$rel1Alias == $pk && $relObj->$rel1Alias == $pk && $rel->$rel2Alias == $relObj->$rel2Alias) {
										// Beziehung ist schon vorhanden: Update!
										$this->_updateActionObj($rel, $relClass, $relClass::$attrs, $relObj->id, null, $valueConverter);
										$new = false;
										unset($relObjs[$i]);
										$relObjs = array_values($relObjs);
										break;
									}
								}
								if($new) {
									// Beziehung ist neu: Anlegen!
									$this->_createActionObj($rel, $relClass, $relClass::$attrs, null, $valueConverter);
								}
							}
							// Alte, nicht gefundene Beziehungen löschen
							foreach($relObjs as $relDel) {
								$this->_deleteActionObj($relClass, $relDel->id);
							}
						}
						else {
							$this->logger->log("current relation " . json_encode($relation, JSON_PRETTY_PRINT));
							$this->logger->log("alias " . $alias);
							$this->logger->log("relations metadata " . json_encode($relations, JSON_PRETTY_PRINT));
							$this->logger->log("data  " . json_encode($o, JSON_PRETTY_PRINT));
							$this->response->setStatusCode(404, "relation not defined");
							return $this->response->send();
						}
						break;

					case "hasMany":
						$alias = $relation["alias"];
						$relClass = $relation['relationModel'];
						if(isset($o->$alias)) {
							$relObjs = array();
							foreach($obj->$alias as $r) {
								$add = new \stdClass();
								$add->id = $r->id;
								$relObjs[] = $add;
							}
							// Definierte Relationen auf Neu oder Update prüfen
							foreach($o->$alias as $rel) {
								$new = true;
								for($i = 0; $i < count($relObjs); $i++) {
									$relObj = $relObjs[$i];
									if((int)$rel->id == (int)$relObj->id) {
										// Beziehung ist schon vorhanden: Update!
										$this->_updateActionObj($rel, $relClass, $relClass::$attrs, $rel->id, null, $valueConverter);
										$new = false;
										unset($relObjs[$i]);
										$relObjs = array_values($relObjs);
										break;
									}
								}
								if($new) {
									// Beziehung ist neu: Anlegen!
									$this->_createActionObj($rel, $relClass, $relClass::$attrs, null, $valueConverter);
								}
							}
							// Alte, nicht gefundene Beziehungen löschen
							foreach($relObjs as $relDel) {
								$this->_deleteActionObj($relClass, $relDel->id);
							}
						}
						else {
							$this->logger->log("current relation " . json_encode($relation, JSON_PRETTY_PRINT));
							$this->logger->log("alias " . $alias);
							$this->logger->log("relations metadata " . json_encode($relations, JSON_PRETTY_PRINT));
							$this->logger->log("data  " . json_encode($o, JSON_PRETTY_PRINT));
							$this->response->setStatusCode(404, "relation not defined");
							return $this->response->send();
						}
						break;
				}
			}

			$this->transaction->commit();
			return $this->_jsonResponse($obj);

		});
	}

	/* -----------------------------------------------------------------------------------
	 * delete action
	 */
	public function deleteAction($id) {
		return $this->jsonWriteRequest(function(&$transaction) use ($id) {
			$model = $this->from;
			$modelParts = 'Models\V1\Prod\Productparts';

			// Delete Parts
			$parts = $this->_oneActionObj($model, $id)->productparts;
			foreach($parts as $part) {
				$part_id = $part->id;
				$this->_deleteActionObj($modelParts, $part_id);
			}

			return $this->_deleteActionRelations($model, $id, $model::$relations);
		});
	}

}
