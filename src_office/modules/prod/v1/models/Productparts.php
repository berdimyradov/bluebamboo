<?php

namespace Models\V1\Prod;

class Productparts extends \Phalcon\Mvc\Model
{

	public $id;
	public $title;
	public $tariff;
	public $price;
	public $duration;
	public $mwst_group;
	public $tariffposition_id;
	public $product_id;
	public $client_id;

	public static $attrs = array(
		"id" => "int",
		"tariff" => "int",
		"title" => "string",
		"duration" => "int",
		"price" => "decimal",
		"mwst_group" => "int",
		"tariffposition_id" => "int",
		"product_id" => "int",
		"client_id" => "int"
	);

	public static $relations = array(
		[
			"type" => "belongsTo",
			"alias" => "product",
			"relationModel" => "Models\V1\Prod\Products",
			"relationIdAlias" => "product_id"
		],
		[
			"type" => "belongsTo",
			"alias" => "tariffposition",
			"relationModel" => "Models\V1\Prod\Tariffpositions",
			"relationIdAlias" => "tariffposition_id"
		],
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
			"id" => "id",
			"tariff" => "tariff",
			"title" => "title",
			"duration" => "duration",
			"price" => "price",
			"mwst_group" => "mwst_group",
			"tariffposition_id" => "tariffposition_id",
			"product_id" => "product_id",
			"client_id" => "client_id"
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_prod_productparts");

		$this->belongsTo(
			"booking_id",
			"Models\V1\Prod\Products",
			"id",
			[
				"alias" => "product",
			]
		);

		$this->belongsTo(
			"tariffposition_id",
			"Models\V1\Prod\Tariffpositions",
			"id",
			[
				"alias" => "tariffposition",
			]
		);
	}

}
