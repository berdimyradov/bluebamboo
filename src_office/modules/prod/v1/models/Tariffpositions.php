<?php

namespace Models\V1\Prod;

class Tariffpositions extends \Phalcon\Mvc\Model
{

	public $id;
	public $nr;
	public $title;

	public static $attrs = array(
		"id" => "int",
		"nr" => "int",
		"title" => "string",
	);

	public static $relations = array(

	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
			'id' => 'id',
			'nr' => 'nr',
			'title' => 'title',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_prod_tariffpositions");
	}

}
