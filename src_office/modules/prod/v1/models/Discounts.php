<?php

namespace Models\V1\Prod;

class Discounts extends \Phalcon\Mvc\Model
{

	public $id;

	public $title;

	public $description;

	public $value;

    public $order;

    public $validFrom;

    public $validTo;

    public $validFor;

    public $validDay;

    public $validTimeFrom;

    public $validTimeTo;

    public $customer_id;

    public $employee_id;

    public $product_id;

    public $pricegroup_id;

    public $location_id;

    public $offer_id;

    public $booking_id;

    public $active;

	public $client_id;

	public static $attrs = array(
		'id' => 'int',
		'title' => 'string',
		'description' => 'string',
		'value' => 'decimal',
		'order' => 'int',
		'validFrom' => 'date',
		'validTo' => 'date',
		'validFor' => 'int',
		'validDay' => 'int',
		'validTimeFrom' => 'time',
		'validTimeTo' => 'time',
		'customer_id' => 'int',
		'employee_id' => 'int',
		'product_id' => 'int',
		'pricegroup_id' => 'int',
		'location_id' => 'int',
		'offer_id' => 'int',
		'booking_id' => 'int',
		'active' => 'bool',
		'client_id' => 'int',
	);

	/**
	 * Independent Column Mapping.
	 */
	public function columnMap()
	{
		return array(
			'id' => 'id',
			'title' => 'title',
			'description' => 'description',
			'value' => 'value',
			'order' => 'order',
			'validFrom' => 'validFrom',
			'validTo' => 'validTo',
			'validFor' => 'validFor',
			'validDay' => 'validDay',
			'validTimeFrom' => 'validTimeFrom',
			'validTimeTo' => 'validTimeTo',
			'customer_id' => 'customer_id',
			'employee_id' => 'employee_id',
			'product_id' => 'product_id',
			'pricegroup_id' => 'pricegroup_id',
			'location_id' => 'location_id',
			'offer_id' => 'offer_id',
			'booking_id' => 'booking_id',
			'active' => 'active',
			'client_id' => 'client_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_prod_discounts");
	}

}
