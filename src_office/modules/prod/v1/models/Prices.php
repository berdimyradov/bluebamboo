<?php

namespace Models\V1\Prod;

class Prices extends \Phalcon\Mvc\Model
{


	public $id;

	public $product_id;

	public $pricegroup_id;

	public $location_id;

    public $validFrom;

    public $validTo;

    public $currency;

    public $price;

	public $client_id;

	public static $attrs = array(
		'id' => 'int',
		'product_id' => 'int',
		'pricegroup_id' => 'int',
		'location_id' => 'int',
		'validFrom' => 'date',
		'validTo' => 'date',
		'currency' => 'string',
		'price' => 'decimal',
		'client_id' => 'int',
	);

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'product_id' => 'product_id',
            'pricegroup_id' => 'pricegroup_id',
            'location_id' => 'location_id',
            'validFrom' => 'validFrom',
            'validTo' => 'validTo',
            'currency' => 'currency',
            'price' => 'price',
			'client_id' => 'client_id',
        );
    }
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_prod_prices");
	}

}
