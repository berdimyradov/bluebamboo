<?php

namespace Models\V1\Prod;

class Pricegroups extends \Phalcon\Mvc\Model
{

	public $id;

	public $title;

	public $client_id;

	public static $attrs = array(
	'id' => 'int',
	'title' => 'string',
	'client_id' => 'int',
	);

	/**
 	 * Independent Column Mapping.
 	 */
	public function columnMap()
	{
		return array(
		'id' => 'id',
		'title' => 'title',
		'client_id' => 'client_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_prod_pricegroups");
	}

}
