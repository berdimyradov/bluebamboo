<?php

namespace Models\V1\Prod;

class Offers extends \Phalcon\Mvc\Model
{

	public $id;

	public $product_id;

	public $validFrom;

	public $validTo;

	public $active;

	public $invoiceable;

	public $cancelDaysBefore;

	public $client_id;

	public static $attrs = array(
		'id' => 'int',
		'product_id' => 'int',
		'validFrom' => 'date',
		'validTo' => 'date',
		'active' => 'bool',
		'invoiceable' => 'bool',
		'cancelDaysBefore' => 'int',
		'client_id' => 'int',
	);

	/**
	 * Independent Column Mapping.
	 */
	public function columnMap()
	{
		return array(
			'id' => 'id',
			'product_id' => 'product_id',
			'validFrom' => 'validFrom',
			'validTo' => 'validTo',
			'active' => 'active',
			'invoiceable' => 'invoiceable',
			'cancelDaysBefore' => 'cancelDaysBefore',
			'client_id' => 'client_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_prod_offers");
	}

}
