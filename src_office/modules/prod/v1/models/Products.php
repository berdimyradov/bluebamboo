<?php

namespace Models\V1\Prod;

class Products extends \Phalcon\Mvc\Model
{

	/**
	 * @Primary
	 * @Identity
	 * @Column(type="integer", nullable=false)
	 */
	public $id;

	/**
	 * @Column(type="integer", nullable=false)
	 */
	public $type;

	/**
	 * @Column(type="string", nullable=false)
	 */
	public $title;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $description;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	public $duration;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	public $breakAfter;

	/**
	 * @Column(type="decimal", nullable=false)
	 */
	public $price;

	/**
	 * @Column(type="boolean", nullable=false)
	 */
	public $active;

	/**
	 * @Column(type="boolean", nullable=false)
	 */
	public $online_booking_available;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $color;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	public $client_id;

	public static $attrs = array(
		"id" => "int",
		"type" => "int",
		"title" => "string",
		"description" => "string",
		"duration" => "int",
		"breakAfter" => "int",
		"price" => "float",
		"active" => "bool",
		"online_booking_available" => "bool",
		"color" => "string",
		"client_id" => "int",
	);

	public static $relations = array(
		[
			"type" => "manyToMany",
			"alias" => "rooms",
			"aliasOneObj" => "room",
			"throughAlias" => "roomsProducts",
			"throughModel" => "Models\V1\Relations\RoomsProducts",
			"throughIdAlias" => "product_id",
			"throughRelIdAlias" => "room_id",
			"relationModel" => "Models\V1\Loc\Rooms"
		],
		[
			"type" => "manyToMany",
			"alias" => "employees",
			"aliasOneObj" => "employee",
			"throughAlias" => "employeesProducts",
			"throughModel" => "Models\V1\Relations\EmployeesProducts",
			"throughIdAlias" => "product_id",
			"throughRelIdAlias" => "employee_id",
			"relationModel" => "Models\V1\Org\Employees"
		],
		[
			"type" => "hasMany",
			"alias" => "productparts",
			"relationModel" => "Models\V1\Prod\Productparts",
			"relationIdAlias" => "product_id"
		],
		/*
		 * Not needed for now, commented out to save space and time
		[
			"type" => "hasMany",
			"alias" => "bookings",
			"relationModel" => "Models\V1\Booking\Bookings"
		],
		*/
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
			'id' => 'id',
			'type' => 'type',
			'title' => 'title',
			'description' => 'description',
			'duration' => 'duration',
			'breakAfter' => 'breakAfter',
			'price' => 'price',
			'active' => 'active',
			'online_booking_available' => 'online_booking_available',
			'color' => 'color',
			'client_id' => 'client_id',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_prod_products");

		$this->hasMany(
			"id",
			"Models\V1\Relations\RoomsProducts",
			"product_id",
			[
				"alias" => "roomsProducts",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Relations\RoomsProducts",
			"product_id",
			"room_id",
			"Models\V1\Loc\Rooms",
			"id",
			[
				"alias" => "rooms",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Relations\EmployeesProducts",
			"product_id",
			[
				"alias" => "employeesProducts",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Relations\EmployeesProducts",
			"product_id",
			"employee_id",
			"Models\V1\Org\Employees",
			"id",
			[
				"alias" => "employees",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Booking\Bookings",
			"product_id",
			[
				"alias" => "bookings",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Prod\Productparts",
			"product_id",
			[
				"alias" => "productparts",
			]
		);
	}

	public function getLocations()
	{
		$locations = array();

		foreach($this->rooms as $room) {
			if(empty($room->location_id)) { continue; }
			if(!$room->location) { continue; }
			$loc = $room->location;
			$found = false;
			foreach($locations as $l) {
				if($l->id == $loc->id) {
					$found = true;
					break;
				}
			}
			if(!$found) {
				$locations[] = $loc;
			}
		}

		return $locations;
	}

	public function getRoomsByLocation($location_id)
	{
		$rooms = array();

		foreach($this->rooms as $room) {
			if(isset($room->location_id) && $room->location_id == $location_id) {
				$rooms[] = $room;
			}
		}

		return $rooms;
	}

	public function getActiveEmployees()
	{
		$employees = array();

		foreach($this->employees as $employee) {
			if(boolval($employee->active) == true) {
				$employees[] = $employee;
			}
		}

		return $employees;
	}

}
