<?php

namespace Models\V1\Loc;

class Locations extends \Phalcon\Mvc\Model
{
	/**
	 * @Primary
	 * @Identity
	 * @Column(type="integer", nullable=false)
	 */
	public $id;

	/**
	 * @Column(type="string", nullable=false)
	 */
	public $title;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $description;

	/**
	 * @Column(type="string", nullable=true)
	 */
    public $street;

	/**
	 * @Column(type="string", nullable=true)
	 */
    public $zip;

	/**
	 * @Column(type="string", nullable=true)
	 */
    public $city;

	/**
	 * @Column(type="string", nullable=true)
	 */
    public $state;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $country;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $email;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $phone;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	public $client_id;

	public static $attrs = array(
		"id" => "int",
		"title" => "string",
		"description" => "string",
		"street" => "string",
		"zip" => "string",
		"city" => "string",
		"state" => "string",
		"country" => "string",
		"email" => "string",
		"phone" => "string",
		"client_id" => "int",
	);

	public static $relations = array(
		 [
			"type" => "manyToMany",
			"alias" => "employees",
			"aliasOneObj" => "employee",
			"throughAlias" => "locationsEmployees",
			"throughModel" => "Models\V1\Relations\LocationsEmployees",
			"throughIdAlias" => "location_id",
			"throughRelIdAlias" => "employee_id",
			"relationModel" => "Models\V1\Org\Employees"
		],
		[
			"type" => "hasMany",
			"alias" => "rooms",
			"relationModel" => "Models\V1\Loc\Rooms",
			"relationIdAlias" => "location_id"
		]
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
			'id' => 'id',
			'title' => 'title',
			'description' => 'description',
			'street' => 'street',
			'zip' => 'zip',
			'city' => 'city',
			'state' => 'state',
			'country' => 'country',
			'email' => 'email',
			'phone' => 'phone',
			'client_id' => 'client_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_loc_locations");

		$this->hasMany(
			"id",
			"Models\V1\Relations\LocationsEmployees",
			"location_id",
			[
				"alias" => "locationsEmployees",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Relations\LocationsEmployees",
			"location_id",
			"employee_id",
			"Models\V1\Org\Employees",
			"id",
			[
				"alias" => "employees",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Loc\Rooms",
			"location_id",
			[
				"alias" => "rooms",
			]
		);

	}

	public function getActiveEmployees()
	{
		$employees = array();

		foreach($this->employees as $employee) {
			if(boolval($employee->active) == true) {
				$employees[] = $employee;
			}
		}

		return $employees;
	}

}
