<?php

namespace Models\V1\Loc;

class Rooms extends \Phalcon\Mvc\Model
{
	/**
	 * @Primary
	 * @Identity
	 * @Column(type="integer", nullable=false)
	 */
	public $id;

	/**
	 * @Column(type="string", nullable=false)
	 */
	public $title;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $description;

	/**
	 * @Column(type="decimal", nullable=true)
	 */
    public $area;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $active_from;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $active_to;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	public $location_id;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	public $client_id;

	public static $attrs = array(
		"id" => "int",
		"title" => "string",
		"description" => "string",
		"area" => "float",
		"active_from" => "date",
		"active_to" => "date",
		"location_id" => "int",
		"client_id" => "int",
	);

	public static $relations = array(
		[
			"type" => "manyToMany",
			"alias" => "products",
			"aliasOneObj" => "product",
			"throughAlias" => "roomsProducts",
			"throughModel" => "Models\V1\Relations\RoomsProducts",
			"throughIdAlias" => "room_id",
			"throughRelIdAlias" => "product_id",
			"relationModel" => "Models\V1\Prod\Products"
		],
		[
			"type" => "hasMany",
			"alias" => "bookings",
			"relationModel" => "Models\V1\Booking\Bookings",
			"relationIdAlias" => "room_id"
		],
		[
			"type" => "belongsTo",
			"alias" => "location",
			"relationModel" => "Models\V1\Loc\Locations",
			"relationIdAlias" => "location_id"
		],
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
			"id" => "id",
			"title" => "title",
			"description" => "description",
			"area" => "area",
			"active_from" => "active_from",
			"active_to" => "active_to",
			"location_id" => "location_id",
			"client_id" => "client_id",
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_loc_rooms");

		$this->hasMany(
			"id",
			"Models\V1\Relations\RoomsProducts",
			"room_id",
			[
				"alias" => "roomsProducts",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Relations\RoomsProducts",
			"room_id",
			"product_id",
			"Models\V1\Prod\Products",
			"id",
			[
				"alias" => "products",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Booking\Bookings",
			"room_id",
			[
				"alias" => "bookings",
			]
		);

		$this->belongsTo(
			"location_id",
			"Models\V1\Loc\Locations",
			"id",
			[
				"alias" => "location",
			]
		);

	}

}
