<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$api = new RouterGroup(array('module' => 'api_v1_loc'));
$api->setPrefix('/api/v1/loc');

$api->addGet	("/locations", array('controller' => 'Locations', 'action' => 'index'));
$api->addGet	("/locations/:int", array('controller' => 'Locations', 'action' => 'one', 'id' => 1));
$api->addGet	("/locations/:int/employees", array('controller' => 'Locations', 'action' => 'employees', 'id' => 1));
$api->addPost	("/locations", array('controller' => 'Locations', 'action' => 'create'));
$api->addPut	("/locations/:int", array('controller' => 'Locations', 'action' => 'update', 'id' => 1));
$api->addDelete	("/locations/:int", array('controller' => 'Locations', 'action' => 'delete', 'id' => 1));

$api->addGet	("/rooms", array('controller' => 'Rooms', 'action' => 'index'));
$api->addGet	("/rooms/:int", array('controller' => 'Rooms', 'action' => 'one', 'id' => 1));
$api->addGet	("/rooms/:int/products", array('controller' => 'Rooms', 'action' => 'products', 'id' => 1));
$api->addPost	("/rooms", array('controller' => 'Rooms', 'action' => 'create'));
$api->addPut	("/rooms/:int", array('controller' => 'Rooms', 'action' => 'update', 'id' => 1));
$api->addDelete	("/rooms/:int", array('controller' => 'Rooms', 'action' => 'delete', 'id' => 1));

$router->mount($api);
