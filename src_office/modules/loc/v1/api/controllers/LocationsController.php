<?php

namespace Api\V1\Loc\Controllers;

class LocationsController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Loc\Locations";

	/* -----------------------------------------------------------------------------------
	 * index action
	 */
	public function indexAction () {
		// site id or similar filter? just about access rights
		$default_projection     = array("o.id", "o.title", "o.description", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.email", "o.phone", "o.client_id");
		$orderby                = array("o.title", "o.id");
		$attrs_for_projection	= array("o.id", "o.title", "o.description", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.email", "o.phone", "o.client_id");
		$attrs_for_where        = array("o.id", "o.title", "o.description", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.email", "o.phone", "o.client_id");
		$attrs_for_orderby      = array("o.id", "o.title", "o.description", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.email", "o.phone", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * index lookup action
	 */
	public function indexLookupAction () {
		$projection			= array("o.id", "o.title");
		$lookup_fields			= array("title");
		$orderby			= array("o.title", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->title = "";
			$l->title .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/* -----------------------------------------------------------------------------------
	 * one action
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneActionRelations($model, $id, $model::$relations, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * action to list all employees for one item
	 */
	public function employeesAction($id) {
		$model = $this->from;
		return $this->_relationsAction($model, $id, "employees");
	}

	/* -----------------------------------------------------------------------------------
	 * create action
	 */
	public function createAction() {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);
		$model = $this->from;
		return $this->_createActionRelations($model, $model::$attrs, $model::$relations);
	}

	/* -----------------------------------------------------------------------------------
	 * update action
	 */
	public function updateAction($id) {
		$model = $this->from;
		return $this->_updateActionRelations($model, $model::$attrs, $id, $model::$relations);
	}

	/* -----------------------------------------------------------------------------------
	 * delete action
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteActionRelations($model, $id, $model::$relations);
	}

}
