<?php

/**
 * Controller class PaymentsInvoicesController
 */

namespace Api\V1\Invo\Controllers;

class InvoicesInvoicerequestsController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Invo\InvoicesInvoicerequests";

	/**
	 * returns data with the option to select fields, filter results and sort result.
	 * authentication required
	 */
	public function indexAction () {

		// site id or similar filter? just about access rights

		$default_projection		= array("o.id", "o.invoicerequest_id", "o.invoice_id");
		$orderby				= array("o.id");
		$attrs_for_projection	= array("o.id", "o.invoicerequest_id", "o.invoice_id");
		$attrs_for_where		= array("o.id", "o.invoicerequest_id", "o.invoice_id");
		$attrs_for_orderby		= array("o.id", "o.invoicerequest_id", "o.invoice_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

	return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /* filter */, $postLoad);
	}

	/**
	 * lookup for index
	 */
	public function indexLookupAction () {
		$projection				= array("o.id", "o.invoicerequest_id", "o.invoice_id");
		$lookup_fields			= array("invoicerequest_id", "invoice_id");
		$orderby				= array("o.invoicerequest_id", "o.invoice_id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/**
	 * returns one entry
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneAction($model, $id, $postLoad);
	}

}
