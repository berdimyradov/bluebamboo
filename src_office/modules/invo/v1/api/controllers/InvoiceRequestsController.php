<?php

/**
 * Controller class InvoicesController
 */

namespace Api\V1\Invo\Controllers;

class InvoicerequestsController extends InvoController {

	private $from = "Models\V1\Invo\Invoicerequests";

	/**
	 * returns data with the option to select fields, filter results and sort result.
	 * authentication required
	 */
	public function indexAction () {

		// site id or similar filter? just about access rights

		$default_projection		= array("o.id", "o.type", "o.createdAt", "o.file", "o.client_id");
		$orderby				= array("o.createdAt", "o.id");
		$attrs_for_projection	= array("o.id", "o.type", "o.createdAt", "o.file", "o.client_id");
		$attrs_for_where		= array("o.id", "o.type", "o.createdAt", "o.file", "o.client_id");
		$attrs_for_orderby		= array("o.id", "o.type", "o.createdAt", "o.file", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/**
	 * Index action for the invoicerequest list
	 */
	public function indexListAction() {
		return $this->jsonReadRequest(function(&$transaction) {
			
			$model = $this->from;
			$client_id = $this->grantAccessForClient();
			$rows = $this->modelsManager->createBuilder()
				->columns('ir.*')
				->from(['ir' => $model])
				->where("ir.client_id = :cid:", ["cid" => $client_id])
				->orderBy('ir.createdAt, ir.id')
				->getQuery()
				->execute();

			// Create custom structure with bookings associated to customers
			$retArr = array();
			foreach($rows as $row) {
				$request = $this->_valueConversionPostload($row, $model::$attrs)->toArray();
				$request['invoicecount'] = count($row->invoices);
				$retArr[] = $request;
			}

			return $retArr;
		});
	}

	/**
	 * lookup for index
	 */
	public function indexLookupAction () {
		$projection				= array("o.id", "o.createdAt");//, "o.description");
		$lookup_fields			= array("number", "description");
		$orderby				= array("o.number", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->name = "";
			$l->name .= $obj->number;
//			$l->desc = $obj->description;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/**
	 * returns one invoicerequest
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneActionRelations($model, $id, $model::$relations, $postLoad);
	}

	/**
	 * creates a new invoice request and the corresponding invoices
	 */
	public function createAction() {
		// Create Action for Request and Invoices
		return $this->jsonWriteRequest(function(&$transaction) {
// Time Tracking for testing
			$request_start_time = microtime(true);
			$request_created_invoices = 0;
			$request_created_pdfs = 0;
// END Time Tracking for testing
			
			$transaction = $this->transaction;
			$model = $this->from;
			$modelInvo = "Models\V1\Invo\Invoices";
			$modelInvopos = "Models\V1\Invo\Invoicepositions";
			$modelRel = "Models\V1\Invo\InvoicesInvoicerequests";
			$modelBookings = "Models\V1\Booking\Bookings";
			$modelCustomers = "Models\V1\Cust\Customers";
			$modelEmp = "Models\V1\Org\Employees";
			$modelOrg = "Models\V1\Org\Organization";
			$modelCases = "Models\V1\Cust\Cases";
			$modelTemplates = "Models\V1\Pdf\Templates";
			$sysInfo = "\GSCLibrary\SystemInfo";

			$data = $this->request->getJsonRawBody();
			$client_id = $this->grantAccessForClient();

			$printCopyArchive = isset($data->printPrivCopy) ? $data->printPrivCopy : false;

			$invoCounter = 0;

			// Get Path for Invoice pdfs
			$pdfPath = $this->getClientInvoDocPath();
			$tempPath = $this->getTempPath();

			// Create the Invoice Request
			$invoRequestData = new \stdClass();
			$invoRequestData->type = $model::TYPES["create"];
			$invoRequestData->createdAt = isset($data->createdAt) ? $data->createdAt : date('Y-m-d H:i:s.u');
			$invoRequestFile = $sysInfo::uniqueFilename("pdf");
			$invoRequestData->file = $invoRequestFile;
			$invoicerequest = $this->_createActionObj($invoRequestData, $model, $model::$attrs);

			// Start new Merge-PDF for the Invoicerequest
			$invoRequestPDF = new \DocumentGenerator\PDFMerger();

			// Get Organization Data for the Invoices
			$organization = $modelOrg::getOrganization();

			// Is there a Tarif 590 Template for reclaim slips?
			$templates_reclaim = $modelTemplates::find("role_id = 4");
			$print_insurance = count($templates_reclaim) > 0;
			$has_sent_mails = false;

			// Create the Invoices and their PDFs
			if(isset($data->customers)) {
				$custs = $data->customers;
				if(count($custs) == 0) {
					throw new \Exception("No Customers defined", 404);
				}

				foreach($custs as $cust) {
					// Collect Data
					$customer = $this->_oneActionObj($modelCustomers, $cust->id);

					if(!isset($cust->cases) || count($cust->cases) == 0) {
						throw new \Exception("No Cases for Customer defined", 404);
					}

					$cases = $cust->cases;
					foreach($cases as $c) {

						if (isset($c->bookings) && !empty($c->bookings)) {
							$case_bookings_ids = $c->bookings;
						} else continue;

						// Get Case Object
						$case = $this->_oneActionObj($modelCases, $c->id);

						$case_sendmail = isset($c->sendmail) ? $c->sendmail : false;
						$case_print = isset($c->print) ? $c->print : true;

						// Collect Bookings, order them by Employee and create one Invoice per Employee
						$case_employees = array();
						foreach($case_bookings_ids as $booking_id) {
							$bookingObj = $modelBookings::findFirstById($booking_id);

							// Check Access for alternation process of booking
							$this->grantAccessForClient($bookingObj->client_id);

							if(empty($bookingObj->employee_id)) {
								throw new \Exception("No Employee set for Booking: " . $bookingObj->id . " - " . $bookingObj->title, 400);
							}

							$e_id = $bookingObj->employee_id;

							if (!isset($case_employees[$e_id])) {
								$case_employees[$e_id] = array();
							}

							$case_employees[$e_id][] = $bookingObj;
						}

						foreach($case_employees as $emp_id => $emp_bookings) {

							// Get Employee Obj
							$employee = $this->_oneActionObj($modelEmp, (int)$emp_id);
							$employee_print_insurance = $print_insurance && boolval($employee->hasTariff590);

							// Generate Invoice Model Entry Data
							$invo = new \stdClass();

							// Create Invoice Date, add seconds for following invoices to ensure uniqueness of Invoice ID
							$dateAddText = "";
							if($invoCounter > 0) {
								$dateAddText = " +" . $invoCounter . " seconds";
							}
							$invoDate = date("Y-m-d H:i:s", strtotime($invoRequestData->createdAt . $dateAddText));
							$invoCounter++;

							$invo->number = $this->generateInvoNumber($invoDate, $customer);
							$invo->date = $invoDate;
							$invo->organization_id = $organization->id;
							$invo->customer_id = $customer->id;
							$invo->customer_name = $customer->name;
							$invo->status = $modelInvo::STATUS_OPEN;
							$invo->value = !empty($c->value) ? $c->value : 0;

							$invoFile = $sysInfo::uniqueFilename("pdf");
							$invo->file = $invoFile;
							if($printCopyArchive) {
								$invoCAFile = $sysInfo::uniqueFilename("pdf");
								$invo->file_copy_archive = $invoCAFile;
							}
							if($employee_print_insurance) {
								$invoCIFile = $sysInfo::uniqueFilename("pdf");
								$invo->file_copy_insurance = $invoCIFile;
							}

							// Create entry, generate ID
							$invoObj = $this->_createActionObj($invo, $modelInvo, $modelInvo::$attrs);

							// Create Relation for Invoice and Invoicerequest
							$this->_createActionObj(new \stdClass, $modelRel, $modelRel::$attrs, ["invoicerequest_id" => $invoicerequest->id, "invoice_id" => $invoObj->id]);

							// Add Bookings to Invoice and add Relations in Bookings
							foreach($emp_bookings as $b) {
								if(!$b->invoice($invoObj->id, $this->transaction)) {
									throw new \Exception("Booking - id: " . $b->id . " - could not be invoiced", 500);
								}
							}

							// Create and add Invoice Positions
							$positions = $this->_createAddInvoPositions($emp_bookings);
							$invoObj->invoicepositions = $positions;

							// Calculate Price of Invoice by 5 cent rounded invoiceposition prices
							$val = 0;
							foreach($positions as $invopos) {
								$posVal = (float)$invopos->price;
								$posVal = \GSCLibrary\GSCFinancialMath::getInstance()->priceRound($posVal);
								$val += $posVal;
							}
							$invoObj->value = $val;

							if(!$invoObj->Update()) {
								throw new \Exception("Invoice Positions could not be added", 400);
							}
// Time Tracking for testing
							$request_created_invoices++;
// END Time Tracking for testing
							$this->createInvoicePDF($invoObj, $organization, $case, $employee, $customer, $invoFile, 1, false);
// Time Tracking for testing
							$request_created_pdfs++;
// END Time Tracking for testing
							if($case_print) {
								$invoRequestPDF->addPDF($this->getFileSwitch()->getFile($pdfPath.$invoFile), 'all');
							}

							// Create Insurance Copy, if wanted
							if($employee_print_insurance) {
								$this->createInvoicePDF($invoObj, $organization, $case, $employee, $customer, $invoCIFile, 4, false);
// Time Tracking for testing
								$request_created_pdfs++;
// END Time Tracking for testing
								if($case_print) {
									$invoRequestPDF->addPDF($this->getFileSwitch()->getFile($pdfPath.$invoCIFile), 'all');
								}
							}

							// Create Archive Copy, if wanted
							if($printCopyArchive) {
								$this->createInvoicePDF($invoObj, $organization, $case, $employee, $customer, $invoCAFile, 1, true);
// Time Tracking for testing
								$request_created_pdfs++;
// END Time Tracking for testing
								$invoRequestPDF->addPDF($this->getFileSwitch()->getFile($pdfPath.$invoCAFile), 'all');
							}

							// If wanted and mail defined: send Invoice per mail
							if($case_sendmail) {
								$this->sendInvoiceMail($invoObj);
								$has_sent_mails = true;
							}
						}
					}
				}
			}
			else {
				throw new \Exception("No Invoice Data defined", 404);
			}

			// Output the merge-print-file PDF
			if($invoRequestPDF->filecount() > 0) {
				// Create the Invoice Request PDF Data
				$docSettings = new \stdClass();
				$invoRequestDate = strtotime($invoRequestData->createdAt);
				$docSettings->author = $organization->name;
				$docSettings->title = "Rechnungsausdrucke Rechnungslauf vom " . date('d.m.Y', $invoRequestDate);
				$docSettings->producer = "bluebamboo Invoice PDF Creator";
				$docSettings->creator = "bluebamboo Invoice PDF Creator";

				if(!$invoRequestPDF->merge('file', $tempPath.$invoRequestFile, $docSettings)) {
					throw new \Exception("Merge File could not be created", 404);
				}
				if(!$this->getFileSwitch()->saveFileFromTemp($invoRequestFile, $pdfPath.$invoRequestFile)) {
					throw new \Exception("Merge File could not be saved/stored!", 404);
				}
			} else {
				// Delete Filename if there is no file to create
				//$invoicerequest->file = null;
				unset($invoicerequest->file);
				$invoicerequest = $this->_updateActionObj($invoicerequest, $model, $model::$attrs, $invoicerequest->id);
			}

			// Complete! Commit Transaction
			$transaction->commit();
			$response = $invoicerequest->toArray();
			$response["sentMails"] = $has_sent_mails;

// Time Tracking for testing
			if (!getenv('execution_time_tracking') || (getenv('execution_time_tracking') != 'true')) {
				return $response;
			}
			$request_time = microtime(true) - $request_start_time;
			error_log("ZEITMESSUNG RECHNUNGSLAUF: Benötigte Zeit: ".$request_time." Sekunden, Erstellte Rechnungen: ".$request_created_invoices.", Erstellte PDFs: ".$request_created_pdfs);
// END Time Tracking for testing

			
			return $response;
		});
	}

	/**
	 * creates a new invoice request and the corresponding invoices
	 */
	public function createPreprintAction() {
		return $this->jsonWriteRequest(function(&$transaction) {
			$transaction = $this->transaction;
			$model = $this->from;
			$modelInvo = "Models\V1\Invo\Invoices";
			$modelRel = "Models\V1\Invo\InvoicesInvoicerequests";
			$modelBookings = "Models\V1\Booking\Bookings";
			$modelOrg = "Models\V1\Org\Organization";
			$sysInfo = "\GSCLibrary\SystemInfo";

			$models = array(
				"this" => $this->from,
				"Invoices" => "Models\V1\Invo\Invoices",
				"Invoicepositions" => "Models\V1\Invo\Invoicepositions",
				"InvoicesInvoicerequests" => "Models\V1\Invo\InvoicesInvoicerequests",
				"Bookings" => "Models\V1\Booking\Bookings",
				"Customers" => "Models\V1\Cust\Customers",
				"Employees" => "Models\V1\Org\Employees",
				"Organization" => "Models\V1\Org\Organization",
				"Cases" => "Models\V1\Cust\Cases",
				"Templates" => "Models\V1\Pdf\Templates",
			);

			$data = $this->request->getJsonRawBody();
			$client_id = $this->grantAccessForClient();

			// Get Path for Invoice pdfs
			$pdfPath = $this->getClientInvoDocPath();
			$tempPath = $this->getTempPath();

			// Create the Invoice Request
			$invoRequestData = new \stdClass();
			$invoRequestData->type = $model::TYPES["preprint"];
			$invoRequestData->createdAt = date('Y-m-d H:i:s');
			$invoRequestFile = $sysInfo::uniqueFilename("pdf");
			$invoRequestData->file = $invoRequestFile;
			$invoicerequest = $this->_createActionObj($invoRequestData, $model, $model::$attrs);

			// Start new Merge-PDF for the Invoicerequest and DocumentGenerator
			$invoRequestPDF = new \DocumentGenerator\PDFMerger();
			// $docGen = new \DocumentGenerator\DocumentGenerator();

			// Get Organization Data for the Invoices
			$organization = $modelOrg::getOrganization();

			// Is there a Tarif 590 Template for reclaim slips?
			$mod = $models['Templates'];
			$templates_reclaim = $mod::find("role_id = 4");
			$has_reclaim_temps = count($templates_reclaim) > 0;

			$printCopyArchive = isset($data->print_copy_archive) ? $data->print_copy_archive : false;

			foreach($data as $book) {
				$booking = $this->_oneActionObj($modelBookings, $book->id);
				$this->grantAccessForClient($booking->client_id);

				$day = date("Y-m-d", strtotime($booking->date));
				$time = date("H:i:s", strtotime($booking->endTime) + 120);
				$invoDate = $day . " " . $time;

				if(empty($day) || empty($time)) {
					throw new \Exception("Invoice Date could not be created", 400);
				}

				$invoFile = $sysInfo::uniqueFilename("pdf");

				$customer = $booking->customer;
				$case = $booking->case;
				$employee = $booking->employee;

				$printCopyInsurance = $has_reclaim_temps && boolval($employee->hasTariff590);

				// Generate Invoice Model Entry Data
				$invo = new \stdClass();
				$invo->number = $this->generateInvoNumber($invoDate, $customer);
				$invo->date = $invoDate;
				$invo->file = $invoFile;
				if($printCopyArchive) {
					$invoCAFile = $sysInfo::uniqueFilename("pdf");
					$invo->file_copy_archive = $invoCAFile;
				}
				if($printCopyInsurance) {
					$invoCIFile = $sysInfo::uniqueFilename("pdf");
					$invo->file_copy_insurance = $invoCIFile;
				}
				$invo->organization_id = $organization->id;
				$invo->customer_id = $customer->id;
				$invo->customer_name = $customer->name;
				$invo->status = $modelInvo::STATUS_OPEN;
				$invo->value = $booking->price;

				// Create entry, generate ID
				$invoObj = $this->_createActionObj($invo, $modelInvo, $modelInvo::$attrs);
				// Create Relation for Invoice and Invoicerequest
				$this->_createActionObj(new \stdClass, $modelRel, $modelRel::$attrs, ["invoicerequest_id" => $invoicerequest->id, "invoice_id" => $invoObj->id]);

				if(!$booking->invoice($invoObj->id, $transaction)) {
					throw new \Exception("Booking - id: " . $booking_id . " - could not be invoiced", 500);
				}

				// Create and add Invoice Positions
				$positions = $this->_createAddInvoPositions([$booking]);
				$invoObj->invoicepositions = $positions;

				// Calculate Price of Invoice by 5 cent rounded invoiceposition prices
				$val = 0;
				foreach($positions as $invopos) {
					$posVal = (float)$invopos->price;
					$posVal = \GSCLibrary\GSCFinancialMath::getInstance()->priceRound($posVal);
					$val += $posVal;
				}
				$invoObj->value = $val;

				if(!$invoObj->Update()) {
					throw new \Exception("Invoice Positions could not be added", 400);
				}

				$this->createInvoicePDF($invoObj, $organization, $case, $employee, $customer, $invoFile, 1, false);
				$invoRequestPDF->addPDF($this->getFileSwitch()->getFile($pdfPath.$invoFile), 'all');

				// Create Insurance Copy, if wanted
				if($printCopyInsurance) {
					$this->createInvoicePDF($invoObj, $organization, $case, $employee, $customer, $invoCIFile, 4, false);
					$invoRequestPDF->addPDF($this->getFileSwitch()->getFile($pdfPath.$invoCIFile), 'all');
				}

				// Create Archive Copy, if wanted
				if($printCopyArchive) {
					$this->createInvoicePDF($invoObj, $organization, $case, $employee, $customer, $invoCAFile, 1, true);
					$invoRequestPDF->addPDF($this->getFileSwitch()->getFile($pdfPath.$invoCAFile), 'all');
				}
			}

			// Output the merge-print-file PDF
			if($invoRequestPDF->filecount() > 0) {
				// Create the Invoice Request PDF Data
				$docSettings = new \stdClass();
				$invoRequestDate = strtotime($invoicerequest->createdAt);
				$docSettings->author = $organization->name;
				$docSettings->title = "Rechnungsausdrucke Rechnungslauf vom " . date('d.m.Y', $invoRequestDate);
				$docSettings->producer = "bluebamboo Invoice PDF Creator";
				$docSettings->creator = "bluebamboo Invoice PDF Creator";

				if(!$invoRequestPDF->merge('file', $tempPath.$invoicerequest->file, $docSettings)) {
					throw new \Exception("Merge File could not be created", 404);
				}
				if(!$this->getFileSwitch()->saveFileFromTemp($invoicerequest->file, $pdfPath.$invoicerequest->file)) {
					throw new \Exception("Merge File could not be saved/stored!", 404);
				}
			} else {
				// Delete Filename if there is no file to create
				//$invoicerequest->file = null;
				unset($invoicerequest->file);
				$invoicerequest = $this->_updateActionObj($invoicerequest, $model, $model::$attrs, $invoicerequest->id);
			}

			// Complete! Commit Transaction
			$transaction->commit();
			return $invoicerequest;
		});
	}

	/**
	 * send / create a reminder for a invoice
	 */
	public function createReminderAction($id = 0) {
		return $this->jsonWriteRequest(function(&$transaction) use ($id) {

			$model = "Models\V1\Invo\Invoices";
			$modelReq = $this->from;
			$modelRel = "Models\V1\Invo\InvoicesInvoicerequests";
			$data = $this->request->getJsonRawBody();
			$transaction = $this->transaction;
			$client_id = $this->grantAccessForClient();

			// Get Path for Invoice pdfs
			$pdfPath = $this->getClientInvoDocPath();
			$tempPath = $this->getTempPath();

			// Start new Merge-PDF
			$invoRequestPDF = new \DocumentGenerator\PDFMerger();

			if($id != 0) {
				$invoices = array();
				$data->id = $id;
				$invoices[] = $data;
			} else {
				$invoices = $data->invoices;
			}

			$date = !empty($data->createdAt) ? $data->createdAt : date('Y-m-d H:i:s.u');

			$retArr = array();
			$invoIds = array();
			foreach($invoices as $invoice) {
				$sendmail = isset($invoice->_sendMail) ? $invoice->_sendMail : false;
				$print = isset($invoice->_print) ? $invoice->_print : false;
				$invoObj = $this->remindInvoice($invoice->id, $date, $sendmail);

				if($invoObj->status == $model::STATUS_REMINDER_1 || $invoObj->status == $model::STATUS_DEPOSIT_REMINDER_1) {
					$file = $invoObj->file_reminder_1;
				} else if($invoObj->status == $model::STATUS_REMINDER_2 || $invoObj->status == $model::STATUS_DEPOSIT_REMINDER_2) {
					$file = $invoObj->file_reminder_2;
				} else {
					$print = false;
				}

				if($print) {
					$invoRequestPDF->addPDF($this->getFileSwitch()->getFile($pdfPath.$file), 'all');
				}
				$invoObj->_print = $print;
				$retArr[] = $invoObj;
				$invoIds[] = $invoObj->id;
			}

			// Create Request Entry
			$reqData = new \stdClass();
			$reqData->type = $modelReq::TYPES["remind"];
			$reqData->createdAt = $date;
			$reqData->client_id = $client_id;

			// Output the merge-print-file PDF
			if($invoRequestPDF->filecount() > 0) {
				$modelOrg = "Models\V1\Org\Organization";
				$organization = $modelOrg::getOrganization();

				// Create the Invoice Request PDF Data
				$docSettings = new \stdClass();
				$docSettings->author = $organization->name;
				$docSettings->title = "Rechnungsausdrucke Mahnungen vom " . date('d.m.Y', strtotime($date));
				$docSettings->producer = "bluebamboo Invoice PDF Creator";
				$docSettings->creator = "bluebamboo Invoice PDF Creator";

				$sysInfo = "\GSCLibrary\SystemInfo";
				$reqFile = $sysInfo::uniqueFilename("pdf");

				if(!$invoRequestPDF->merge('file', $tempPath.$reqFile, $docSettings)) {
					throw new \Exception("Merge File could not be created", 404);
				}
				if(!$this->getFileSwitch()->saveFileFromTemp($reqFile, $pdfPath.$reqFile)) {
					throw new \Exception("Merge File could not be saved/stored!", 404);
				}

				$reqData->file = $reqFile;
			}

			$reqObj = $this->_createActionObj($reqData, $modelReq, $modelReq::$attrs);
			$reqId = $reqObj->id;

			// Create relationship entries
			foreach($invoIds as $invoId) {
				$this->_createActionObj(new \stdClass(), $modelRel, $modelRel::$attrs, ['invoicerequest_id' => $reqId, 'invoice_id' => $invoId]);
			}

			$res = $reqObj->toArray();
			$res["invoices"] = $retArr;

			$transaction->commit();
			return $res;
		});
	}

	private function remindInvoice($id, $date, $sendmail = false)
	{
		$model = "Models\V1\Invo\Invoices";
		$modelEmp = "Models\V1\Org\Employees";
		$modelInvopos = "Models\V1\Invo\Invoicepositions";
		$modelTempRoles = "Models\V1\Pdf\Roles";
		$modelCases = "Models\V1\Cust\Cases";
		$sysInfo = "\GSCLibrary\SystemInfo";
		$tempPath = $this->getTempPath();
		$invoPath = $this->getClientInvoDocPath();

		$invoObj = $model::findFirstById($id);
		if(!$invoObj) {
			throw new \Exception("Invoice not found", 404);
		}
		$this->grantAccessForClient($invoObj->client_id);
		if(!$invoObj->remind()) {
			throw new \Exception("Invoice reminder action not possible!", 400);
		}
		$org = $invoObj->organization;

		$posData = new \stdClass();

		$price_first = !empty($org->cost_reminder_first) ?
			(float)\GSCLibrary\GSCFinancialMath::getInstance()->priceRound($org->cost_reminder_first) :
			0;
		$price_second = !empty($org->cost_reminder_second) ?
			(float)\GSCLibrary\GSCFinancialMath::getInstance()->priceRound($org->cost_reminder_second) :
			0;

		$createPos = false;
		if($invoObj->status == $model::STATUS_REMINDER_1 || $invoObj->status == $model::STATUS_DEPOSIT_REMINDER_1) {
			$posData->type = $modelInvopos::TYPE_REMINDER_1;
			$posData->title = "Mahngebühren 1. Mahnung";
			$posData->price = $price_first;
			$createPos = !empty($price_first);
		} elseif ($invoObj->status == $model::STATUS_REMINDER_2 || $invoObj->status == $model::STATUS_DEPOSIT_REMINDER_2) {
			$posData->type = $modelInvopos::TYPE_REMINDER_2;
			$posData->title = "Mahngebühren 2. Mahnung";
			$posData->price = $price_second;
			$createPos = !empty($price_second);
		} else {
			throw new \Exception("Wrong Invoice Status!", 400);
		}

		if($createPos) {
			$posData->date = $date;
			$posData->tariff = 999;
			$posData->tariffposition_nr = "";
			$posData->tariffposition_title = $posData->title;
			$posData->quantity = 1;
			$posData->duration = 0;
			$posData->mwst = 0;
			$posData->invoice_id = $invoObj->id;
			$posData->client_id = $this->session->get("auth-identity-client");

			$invoPos = $this->_createActionObj($posData, $modelInvopos, $modelInvopos::$attrs);
		}

		// Add Reminder Data to Invo Object
		$remFile = $sysInfo::uniqueFilename("pdf");
		if($invoObj->status == $model::STATUS_REMINDER_1 || $invoObj->status == $model::STATUS_DEPOSIT_REMINDER_1) {
			$invoObj->file_reminder_1 = $remFile;
			$invoObj->date_reminder_1 = $date;
			$invoObj->value = (float)$invoObj->value + (float)$price_first;
			$remRole = $modelTempRoles::ROLES['REMINDER_1'];
		} else if($invoObj->status == $model::STATUS_REMINDER_2 || $invoObj->status == $model::STATUS_DEPOSIT_REMINDER_2) {
			$invoObj->file_reminder_2 = $remFile;
			$invoObj->date_reminder_2 = $date;
			$invoObj->value = (float)$invoObj->value + (float)$price_second;
			$remRole = $modelTempRoles::ROLES['REMINDER_2'];
		} else {
			throw new \Exception("Wrong Status of invoice", 400);
		}

		if(!$invoObj->update()) {
			throw new \Exception("Update of Invoice Data for Reminder failed", 400);
		}

		// Get some Data for the Invoices
		$bookings = $invoObj->bookings;
		$last_booking_index = count($bookings) - 1;
		if($last_booking_index < 0) {
			throw new \Exception("The Invoice has no bookings", 400);
		}
		$last_booking = $bookings[$last_booking_index];

		$employee = $this->_oneActionObj($modelEmp, $last_booking->employee_id);
		$case = $this->_oneActionObj($modelCases, $last_booking->case_id);
		$customer = $invoObj->customer;

		// Create the Reminder PDF
		$this->createInvoicePDF($invoObj, $org, $case, $employee, $customer, $remFile, $remRole, false);

		if($sendmail && !empty($customer->getPrimaryMail())) {
			$this->sendReminderMail($invoObj);
			$invoObj->_mailsent = true;
		} else {
			$invoObj->_mailsent = false;
		}

		return $invoObj;
	}

}
