<?php

/**
 * Controller class PaymentsController
 */

namespace Api\V1\Invo\Controllers;

class PaymentsController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Invo\Payments";

	/**
	 * returns data with the option to select fields, filter results and sort result.
	 * authentication required
	 */
	public function indexAction () {

		// site id or similar filter? just about access rights

		$default_projection		= array("o.id", "o.created_at", "o.payed_by", "o.payed_at", "o.value", "o.bankaccount_iban", "o.bankaccount_bic", "o.bankaccount_bank", "o.bankaccount_owner", "o.comment", "o.customer_id", "o.client_id");
		$orderby				= array("o.id");
		$attrs_for_projection	= array("o.id", "o.created_at", "o.payed_by", "o.payed_at", "o.value", "o.bankaccount_iban", "o.bankaccount_bic", "o.bankaccount_bank", "o.bankaccount_owner", "o.comment", "o.customer_id", "o.client_id");
		$attrs_for_where		= array("o.id", "o.created_at", "o.payed_by", "o.payed_at", "o.value", "o.bankaccount_iban", "o.bankaccount_bic", "o.bankaccount_bank", "o.bankaccount_owner", "o.customer_id", "o.client_id");
		$attrs_for_orderby		= array("o.id", "o.created_at", "o.payed_by", "o.payed_at", "o.value", "o.bankaccount_iban", "o.bankaccount_bic", "o.bankaccount_bank", "o.bankaccount_owner", "o.customer_id", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

	return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /* filter */, $postLoad);
	}

	/**
	 * lookup for index
	 */
	public function indexLookupAction () {
		$projection				= array("o.id", "o.value", "o.payed_by", "o.customer_id");
		$lookup_fields			= array("customer_id", "payed_by", "value");
		$orderby				= array("o.customer_id", "o.payed_by");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/**
	 * returns one entry
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneAction($model, $id, $postLoad);
	}

	/**
	 * creates a new entry
	 */
	public function createAction() {
		$model = $this->from;
		return $this->_createAction($model, $model::$attrs);
	}

	/**
	 * updates an existing entry
	 */
	public function updateAction($id) {
		$model = $this->from;
		return $this->_updateAction($model, $model::$attrs, $id);
	}

	/**
	 * deletes an existing entry
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

}
