<?php

/**
 * Controller class InvoicesController
 */

namespace Api\V1\Invo\Controllers;

class InvoicesController extends InvoController {

	private $from = "Models\V1\Invo\Invoices";

	/**
	 * returns data with the option to select fields, filter results and sort result.
	 * authentication required
	 */
	public function indexAction () {

		// site id or similar filter? just about access rights

		$default_projection		= array("o.id", "o.number", "o.date", "o.date_reminder_1", "o.date_reminder_2", "o.file", "o.file_reminder_1", "o.file_reminder_2", "o.file_copy_insurance", "o.file_copy_archive", "o.value", "o.customer_name", "o.status", "o.organization_id", "o.customer_id", "o.client_id");
		$orderby				= array("o.number", "o.id");
		$attrs_for_projection	= array("o.id", "o.number", "o.date", "o.date_reminder_1", "o.date_reminder_2", "o.file", "o.file_reminder_1", "o.file_reminder_2", "o.file_copy_insurance", "o.file_copy_archive", "o.value", "o.customer_name", "o.status", "o.organization_id", "o.customer_id", "o.client_id");
		$attrs_for_where		= array("o.id", "o.number", "o.date", "o.date_reminder_1", "o.date_reminder_2", "o.file", "o.file_reminder_1", "o.file_reminder_2", "o.file_copy_insurance", "o.file_copy_archive", "o.value", "o.customer_name", "o.status", "o.organization_id", "o.customer_id", "o.client_id");
		$attrs_for_orderby		= array("o.id", "o.number", "o.date", "o.date_reminder_1", "o.date_reminder_2", "o.value", "o.customer_name", "o.status", "o.organization_id", "o.customer_id", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/**
	 * lookup for index
	 */
	public function indexLookupAction () {
		$projection				= array("o.id", "o.number", "o.date");//, "o.description");
		$lookup_fields			= array("number", "description", "date");
		$orderby				= array("o.number", "o.date", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->name = "";
			$l->name .= $obj->number;
//			$l->desc = $obj->description;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/**
	 * returns data with the option to select fields, filter results and sort result.
	 * authentication required
	 */
	public function indexAllAction () {
		return $this->jsonReadRequest(function(&$transaction) {

			$model = $this->from;
			$modelCust = "Models\V1\Cust\Customers";
			$client_id = $this->grantAccessForClient();

			$rows = $this->modelsManager->createBuilder()
				->columns('invoice.*, customer.id AS cust_id, customer.firstname AS cust_firstname, customer.name AS cust_name')
				->from(['invoice' => $model,])
				->leftJoin($modelCust, "customer.id = invoice.customer_id", "customer")
				->where("invoice.client_id = :cid:", ["cid" => $client_id])
				->orderBy('invoice.date, invoice.number')
				->getQuery()
				->execute();

			// Create custom structure with bookings associated to customers
			$retArr = array();
			foreach($rows as $row) {
				$inv = $this->_valueConversionPostload($row->invoice, $model::$attrs)->toArray();
				$inv['customer_id'] = (int)$row->cust_id;
				$inv['customer_firstname'] = $row->cust_firstname;
				$inv['customer_name'] = $row->cust_name;
				$retArr[] = $inv;
			}

			return $retArr;
		});
	}

	/**
	 * returns data with the option to select fields, filter results and sort result.
	 * authentication required
	 */
	public function indexUncanceledAction () {
		return $this->jsonReadRequest(function(&$transaction) {

			$model = $this->from;
			$modelCust = "Models\V1\Cust\Customers";
			$client_id = $this->grantAccessForClient();

			$rows = $this->modelsManager->createBuilder()
				->columns('invoice.*, customer.id AS cust_id, customer.firstname AS cust_firstname, customer.name AS cust_name')
				->from(['invoice' => $model,])
				->leftJoin($modelCust, "customer.id = invoice.customer_id", "customer")
				->where("invoice.client_id = :cid:", ["cid" => $client_id])
				->notInWhere("invoice.status", [$model::STATUS_CANCELLED, $model::STATUS_PAYED])
				//->andWhere("invoice.status <> :can:", ["can" => $model::STATUS_CANCELLED])
				->orderBy('invoice.date, invoice.number')
				->getQuery()
				->execute();

			// Create custom structure with bookings associated to customers
			$retArr = array();
			foreach($rows as $row) {
				$inv = $this->_valueConversionPostload($row->invoice, $model::$attrs)->toArray();
				$inv['customer_id'] = (int)$row->cust_id;
				$inv['customer_firstname'] = $row->cust_firstname;
				$inv['customer_name'] = $row->cust_name;
				$retArr[] = $inv;
			}

			return $retArr;
		});
	}

	/**
	 * lookup for index for this month
	 */
	public function indexThismonth () {
		/*
		$projection				= array("o.id", "o.createdAt");//, "o.description");
		$lookup_fields			= array("number", "description");
		$orderby				= array("o.number", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->name = "";
			$l->name .= $obj->number;
//			$l->desc = $obj->description;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
		*/
	}

	/**
	 * invoice stats for the current year
	 */
	public function indexYearstats () {
		/*
		// current year:
		$year =
		return $this->_indexYearstatsYear($year);
		*/
	}

	/**
	 * invoice stats for a specific year
	 */
	public function indexYearstatsYear ($year) {
		/*
		$projection				= array("o.id", "o.createdAt");//, "o.description");
		$lookup_fields			= array("number", "description");
		$orderby				= array("o.number", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->name = "";
			$l->name .= $obj->number;
//			$l->desc = $obj->description;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
		*/
	}

	/**
	 * index invoices that are over due time
	 */
	public function indexOverdueAction() {
		return $this->jsonReadRequest(function(&$transaction) {

			$model = $this->from;
			$modelCust = "Models\V1\Cust\Customers";
			$modelOrg = "Models\V1\Org\Organization";
			$client_id = $this->grantAccessForClient();

			$org = $modelOrg::getOrganization();
			$termOpen = (int)$org->payment_term;
			$termRem = (int)$org->payment_term_reminder_first;

			$conditionTerm  = "(invoice.status = '".$model::STATUS_OPEN."' AND DATEDIFF(NOW(), invoice.date) > ".$termOpen.") ";
			$conditionTerm .= "OR (invoice.status = '".$model::STATUS_REMINDER_1."' AND DATEDIFF(NOW(), invoice.date_reminder_1) > ".$termRem.")";
// TODO: Handling of deposits

			$rows = $this->modelsManager->createBuilder()
				->columns('invoice.*, customer.id AS cust_id, customer.firstname AS cust_firstname, customer.name AS cust_name')
				->from(['invoice' => $model,])
				->leftJoin($modelCust, "customer.id = invoice.customer_id", "customer")
				->where("invoice.client_id = :cid:", ["cid" => $client_id])
				->andWhere($conditionTerm)
				->orderBy('invoice.date, invoice.number')
				->getQuery()
				->execute();

			// Create custom structure with bookings associated to customers
			$retArr = array();
			foreach($rows as $row) {
				$inv = $this->_valueConversionPostload($row->invoice, $model::$attrs)->toArray();
				$inv['customer_id'] = (int)$row->cust_id;
				$inv['customer_firstname'] = !empty($row->cust_firstname) ? $row->cust_firstname : "";
				$inv['customer_name'] = !empty($row->cust_name) ? $row->cust_name : "";
				$retArr[] = $inv;
			}

			return $retArr;
		});
	}

	/**
	 * index invoices that are already unpaid
	 */
	public function indexUnpaidAction() {
		return $this->jsonReadRequest(function(&$transaction) {

			$model = $this->from;
			$modelCust = "Models\V1\Cust\Customers";
			$client_id = $this->grantAccessForClient();

			$rows = $this->modelsManager->createBuilder()
				->columns('invoice.*, customer.id AS cust_id, customer.firstname AS cust_firstname, customer.name AS cust_name')
				->from(['invoice' => $model,])
				->leftJoin($modelCust, "customer.id = invoice.customer_id", "customer")
				->where("invoice.client_id = :cid:", ["cid" => $client_id])
				->andWhere("invoice.status NOT IN (:status_can:,:status_pay:)", [
					'status_can' => $model::STATUS_CANCELLED,
					'status_pay' => $model::STATUS_PAYED,
				])
				->orderBy('invoice.date, invoice.number')
				->getQuery()
				->execute();

			// Create custom structure with bookings associated to customers
			$retArr = array();
			foreach($rows as $row) {
				$inv = $this->_valueConversionPostload($row->invoice, $model::$attrs)->toArray();
				$inv['customer_id'] = (int)$row->cust_id;
				$inv['customer_firstname'] = $row->cust_firstname;
				$inv['customer_name'] = $row->cust_name;
				$retArr[] = $inv;
			}

			return $retArr;
		});
	}

	/**
	 * returns one invoice
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneActionRelations($model, $id, $model::$relations, $postLoad);
	}

	/**
	 * send a invoice again per mail
	 */
	public function resendAction($id = 0) {
		return $this->jsonWriteRequest(function(&$transaction) use ($id) {

			$model = $this->from;
			$data = $this->request->getJsonRawBody();
			$transaction = $this->transaction;

			if($id != 0) {
				$invoices = array();
				if($data->id != $id) {
					throw new \Exception("Wrong Route - id called", 400);
				}
				$invoices[] = $data;
			} else {
				$invoices = $data;
			}

			foreach($invoices as $invoice) {
				$invoObj = $this->_oneActionObj($model, $invoice->id);
				$this->sendInvoiceMail($invoObj);
			}

			$transaction->commit();
			return $data;
		});

	}

	/**
	 * Rescind one or more Invoices
	 */
	public function stornoAction($id = 0) {
		return $this->jsonWriteRequest(function(&$transaction) use ($id) {

			$model = $this->from;
			$data = $this->request->getJsonRawBody();
			$transaction = $this->transaction;

			if($id != 0) {
				$invoices = array();
				$inv = new \stdClass();
				$inv->id = $id;
				$invoices[] = $inv;
			} else {
				$invoices = $data->invoices;
			}

			$comment = empty($data->comment) ? "" : $data->comment;

			$resp = array();
			foreach($invoices as $invoice) {
				$invoObj = $model::findFirstById($invoice->id);
				if (!$invoObj) {
					throw new \Exception("Item not found", 404);
				}
				$this->grantAccessForClient($invoObj->client_id);
				$resp[] = $this->fullStornoInvoice($invoice->id, $comment);
			}
			if(count($resp) == 1) {
				$resp = $resp[0];
			}

			$transaction->commit();
			return $resp;
		});
	}

	/**
	 * Pay one Invoice
	 */
	public function payAction($id) {
		return $this->jsonWriteRequest(function(&$transaction) use ($id) {
			$transaction = $this->transaction;

			$model = $this->from;
			$modelPayments = "Models\V1\Invo\Payments";
			$modelRel = "Models\V1\Invo\PaymentsInvoices";
			$data = $this->request->getJsonRawBody();

			$invoice = $this->_oneActionObj($model, $id);
			$this->grantAccessForClient($invoice->client_id);

			unset($data->id);
			// By now - set the payment value exactly to the invoice value:
			$data->value = $invoice->value;
			$payment = $this->_createActionObj($data, $modelPayments, $modelPayments::$attrs);
			$rel = $this->_createActionObj(new \stdClass(), $modelRel, $modelRel::$attrs, ["payment_id" => $payment->id, "invoice_id" => $id]);

			if(!$invoice->pay()) {
				throw new \Exception("Invoice payment not possible!", 400);
			}

			$transaction->commit();
			return $invoice;
		});
	}

	/**
	 * updates an existing invoice
	 */
	public function updateAction($id) {
		$model = $this->from;
		return $this->_updateAction($model, $model::$attrs, $id);
	}

	/**
	 * deletes an existing invoice
	 */
	public function deleteAction($id)
	{
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

	private function fullStornoInvoice($id, $comment) {

		$model = $this->from;
		$invoObj = $model::findFirstById($id);
		if ($invoObj == null) {
			throw new \Exception("missing parameter", 404);
		}

		$this->grantAccessForClient($invoObj->client_id);

		if(!empty($comment)) {
			$add = "Kommentar Storno:
			" . $comment;
			if(!empty($invoObj->comment)) {
				$invoObj->comment = $invoObj->comment . "
				" . $add;
			} else $invoObj->comment = $add;
		}

		$bookings = $invoObj->bookings;
		foreach($bookings as $booking) {
			$booking->setTransaction($this->transaction);
			if(!$booking->reopen()) {
				throw new \Exception("Unable to reopen Booking", 400);
			}
		}

		if(!$invoObj->cancel()) {
			throw new \Exception("Wrong Status of Invoice", 400);
		}

		return $invoObj;
	}

	private function condRescindReopenInvoiceposition($id, $rescind) {
		$modelInvopos = "Models\V1\Invo\Invoicepositions";
		$modelBookings = "Models\V1\Booking\Bookings";

		$invopos = $modelInvopos::findFirstById($id);
		if ($invopos == null) {
			throw new \Exception("missing parameter - invoiceposition id", 404);
		}

		switch($invopos->type) {
			case $modelInvopos::TYPE_BOOKING:
				$booking = $invopos->booking;
				if($rescind) {
					if(!$booking->rescind()) {
						throw new \Exception("Booking rescind failed", 400);
					}
				} else {
					if(!$booking->reopen()) {
						throw new \Exception("Booking reopen failed", 400);
					}
				}
				break;
			case $modelInvopos::TYPE_REMINDER_1:
				break;
			case $modelInvopos::TYPE_REMINDER_2:
				break;
		}

		return true;
	}

}
