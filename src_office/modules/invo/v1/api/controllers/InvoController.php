<?php

/**
 * Controller class InvoicesController
 */

namespace Api\V1\Invo\Controllers;

class InvoController extends \GSCLibrary\BaseApiController {

	/**
	 * Get all sent Invoice Mails for the Client
	 */
	public function indexEmailsAction()
	{
		return $this->jsonReadRequest(function(&$transaction) {
			$model = "Models\V1\Sys\Emails";
			$client_id = $this->grantAccessForClient();

			$objs = $model::find("client_id = '".$client_id."' AND type >= '200' AND type < '300'");
			$res = array();
			foreach($objs as $o) {
				unset($o->body_html);
				unset($o->body_plain);
				unset($o->priority);
				unset($o->send_errors);
				unset($o->attachments);
				$res[] = $this->_valueConversionPostload($o, $model::$attrs);
			}
			return $res;
		});
	}

	/**
	 * Send an Invoice Mail to customer
	 */
	protected function sendInvoiceMail($invoice) {

		$this->grantAccessForClient($invoice->client_id);
		$platform_id = $this->session->get("auth-identity-platform");

		$customer = $invoice->customer;
		$bookings = $invoice->bookings;
		$employee = $bookings[0]->employee;
		$organization = \Models\V1\Org\Organization::getOrganization();

		$custName = $customer->firstname . " " . $customer->name;
		$mailto = $customer->getPrimaryMail();
		if(empty($mailto)) {
			throw new \Exception("Customer has no Email Address", 400);
		}
		$to = array(
			$mailto => $custName,
		);

		if(empty($organization->email)) {
			throw new \Exception("No Organization Email Address defined", 400);
		} else {
			$mailFrom = $organization->email;
		}

		if($customer->address_you) {
			if($customer->gender) {
				$text = "Lieber ";
			} else {
				$text = "Liebe ";
			}
			$text .= $customer->firstname . ",\r\n\r\n";
			$text .= "im Anhang findest Du die Rechnung Nr. " . $invoice->number;
			$text .= ".\r\n\r\nLiebe Grüße,\r\n\r\n";
			$text .= $employee->firstname . " " . $employee->name . "\r\n\r\n\r\n\r\n";
			$text .= $organization->name . "\r\n\r\n";
			$text .= $organization->street . "\r\n\r\n";
			$text .= $organization->zip . " " . $organization->city . "\r\n\r\n";
			$text .= $organization->phone . "\r\n\r\n";
			$text .= $organization->email;
		} else {
			if($customer->gender) {
				$text = "Sehr geehrter ";
				if(isset($customer->address)) {
					$text .= $customer->address;
				} else {
					$text .= "Herr";
				}
			} else {
				$text = "Sehr geehrte ";
				if(isset($customer->address)) {
					$text .= $customer->address;
				} else {
					$text .= "Frau";
				}
			}

			$text .= " " . $customer->name . ",\r\n\r\n";
			$text .= "im Anhang finden Sie die Rechnung Nr. " . $invoice->number;
			$text .= ".\r\n\r\nMit freundlichen Grüßen,\r\n\r\n";
			$text .= $employee->firstname . " " . $employee->name . "\r\n\r\n\r\n\r\n";
			$text .= $organization->name . "\r\n\r\n";
			$text .= $organization->street . "\r\n\r\n";
			$text .= $organization->zip . " " . $organization->city . "\r\n\r\n";
			$text .= $organization->phone . "\r\n\r\n";
			$text .= $organization->email;
		}

		// Add Attachments
		$pdfPath = $this->getClientInvoDocPath();
		$attachments = array(
			[
				'url' => $pdfPath.$invoice->file,
				'filename' => "Rechnung_" . $invoice->number . ".pdf",
				'label' => "Rechnung Nr. " . $invoice->number
			]
		);

		// If copy for Insurance defined -> add it to sendmail-pdf
		if(!empty($invoice->file_copy_insurance)) {
			$attachments[] = array(
				'url' => $pdfPath.$invoice->file_copy_insurance,
				'filename' => "Rückforderungsbeleg_Rechnung_" . $invoice->number . ".pdf",
				'label' => "Rückforderungsbeleg Rechnung " . $invoice->number,
			);
		}

		// Create Data for Email-DB-Entry
		$model = "Models\V1\Sys\Emails";
		$mail = new \stdClass();
		$mail->state = $model::STATES["OPEN"];
		$mail->type = $model::TYPES["INVOICE"];
		$mail->language = "de";
		$mail->priority = 5;
		$mail->send_trials = 0;
		$mail->subject = "Rechnung Nr. " . $invoice->number;
		$mail->from = $mailFrom;
		$mail->from_label = $organization->name;
		$mail->to = $to;
		$mail->body_plain = $text;
		$mail->attachments = $attachments;
		$mail->client_id = $invoice->client_id;
		$mail->platform_id = $platform_id;
		$mail->item_id = $invoice->id;

		$this->_createActionObj($mail, $model, $model::$attrs);
	}

	/**
	 * Send a Reminder Mail to customer
	 */
	protected function sendReminderMail($invoice) {

		$this->grantAccessForClient($invoice->client_id);
		$platform_id = $this->session->get("auth-identity-platform");
		$model = "Models\V1\Sys\Emails";

		$customer = $invoice->customer;
		$bookings = $invoice->bookings;
		$employee = $bookings[0]->employee;
		$organization = \Models\V1\Org\Organization::getOrganization();

		$custName = $customer->firstname . " " . $customer->name;
		$mailto = $customer->getPrimaryMail();
		if(empty($mailto)) {
			throw new \Exception("Customer has no Email Address", 400);
		}
		$to = array(
			$mailto => $custName,
		);

		if(!isset($organization->email)) {
			throw new \Exception("No Organization Email Address defined", 400);
		}
		$mailFrom = $organization->email;
		
		if($invoice->status == $modelInvo::STATUS_REMINDER_1) {
			$subject = "Erste Mahnung, Rechnung Nr. " . $invoice->number;
			$type = $model::TYPES["INVOICE_REMINDER_1"];
		} else if($invoice->status == $modelInvo::STATUS_REMINDER_2) {
			$subject = "Zweite Mahnung, Rechnung Nr. " . $invoice->number;
			$type = $model::TYPES["INVOICE_REMINDER_2"];
		} else {
			throw new \Exception("Wrong status of invoice", 400);
		}

		if($customer->address_you) {
			if($customer->gender) {
				$text = "Lieber ";
			} else {
				$text = "Liebe ";
			}
			$text .= $customer->firstname . ",\r\n\r\n";
			$text .= "im Anhang findest Du die ";
			$text .= ($invoice->status == $modelInvo::STATUS_REMINDER_1) ? "erste" : "zweite";
			$text .= " Mahnung zu Rechnung Nr. " . $invoice->number;
			$text .= ".\r\n\r\nLiebe Grüße,\r\n\r\n";
			$text .= $employee->firstname . " " . $employee->name . "\r\n\r\n\r\n\r\n";
			$text .= $organization->name . "\r\n\r\n";
			$text .= $organization->street . "\r\n\r\n";
			$text .= $organization->zip . " " . $organization->city . "\r\n\r\n";
			$text .= $organization->phone . "\r\n\r\n";
			$text .= $organization->email;
		} else {
			if($customer->gender) {
				$text = "Sehr geehrter ";
				if(isset($customer->address)) {
					$text .= $customer->address;
				} else {
					$text .= "Herr";
				}
			} else {
				$text = "Sehr geehrte ";
				if(isset($customer->address)) {
					$text .= $customer->address;
				} else {
					$text .= "Frau";
				}
			}

			$text .= " " . $customer->name . ",\r\n\r\n";
			$text .= "im Anhang finden Sie die ";
			$text .= ($invoice->status == $modelInvo::STATUS_REMINDER_1) ? "erste" : "zweite";
			$text .= " Mahnung zu Rechnung Nr. " . $invoice->number;
			$text .= ".\r\n\r\nMit freundlichen Grüßen,\r\n\r\n";
			$text .= $employee->firstname . " " . $employee->name . "\r\n\r\n\r\n\r\n";
			$text .= $organization->name . "\r\n\r\n";
			$text .= $organization->street . "\r\n\r\n";
			$text .= $organization->zip . " " . $organization->city . "\r\n\r\n";
			$text .= $organization->phone . "\r\n\r\n";
			$text .= $organization->email;
		}

		// Add Attachments
		$attachments = array();
		$pdfPath = $this->getClientInvoDocPath();
		if($invoice->status == $modelInvo::STATUS_REMINDER_1) {
			$attachments[] = array(
				'url' => $pdfPath.$invoice->file_reminder_1,
				'filename' => "Mahnung_1_Rechnung_" . $invoice->number . ".pdf",
				'label' => "Mahnung 1 für Rechnung " . $invoice->number,
			);
		} else if ($invoice->status == $modelInvo::STATUS_REMINDER_2) {
			$attachments[] = array(
				'url' => $pdfPath.$invoice->file_reminder_2,
				'filename' => "Mahnung_2_Rechnung_" . $invoice->number . ".pdf",
				'label' => "Mahnung 2 für Rechnung " . $invoice->number,
			);
		}
		
		// If copy for Insurance defined -> add it to sendmail-pdf
		if(!empty($invoice->file_copy_insurance)) {
			$attachments[] = array(
				'url' => $pdfPath.$invoice->file_copy_insurance,
				'filename' => "Rückforderungsbeleg_Rechnung_" . $invoice->number . ".pdf",
				'label' => "Rückforderungsbeleg Rechnung " . $invoice->number,
			);
		}

		// Create Data for Email-DB-Entry
		$mail = new \stdClass();
		$mail->state = $model::STATES["OPEN"];
		$mail->type = $type;
		$mail->language = "de";
		$mail->priority = 5;
		$mail->send_trials = 0;
		$mail->subject = $subject;
		$mail->from = $mailFrom;
		$mail->from_label = $organization->name;
		$mail->to = $to;
		$mail->body_plain = $text;
		$mail->attachments = $attachments;
		$mail->client_id = $invoice->client_id;
		$mail->platform_id = $platform_id;
		$mail->item_id = $invoice->id;

		$this->_createActionObj($mail, $model, $model::$attrs);
	}

	/**
	 * Generate the JSON for an Invoice PDF
	 */
// TODO: Move this into Invoice Model
	protected function generateInvoContent($customer, $employee, $organization, $invoice, $case, $copyfor = 0, $copy = false) {
		$modelInvo = "Models\V1\Invo\Invoices";
		$modelInvopos = "Models\V1\Invo\Invoicepositions";
		$mathLib = \GSCLibrary\GSCFinancialMath::getInstance();
		$content = new \stdClass();
		$content->type = "data";
		$content->values = new \stdClass();

		$content->values->InvoiceNumber = $invoice->number;
		$content->values->InvoiceId = $this->generateInvoId590($invoice->date);
		$invoicedate = strtotime($invoice->date);
		$content->values->InvoiceMonth = date('n', $invoicedate);
		$content->values->InvoiceYear = date('Y', $invoicedate);
		$content->values->InvoiceDay = date('j', $invoicedate);
		$content->values->InvoiceDate = date('d.m.Y', $invoicedate);
		$content->values->InvoiceCurrency = "CHF";

		// TG/TP differentiation
		if(isset($case->payment_type)) {
			switch(intval($case->payment_type)) {
				case 1:
					$content->values->Remuneration = "TG";
					break;
				case 2:
					$content->values->Remuneration = "TP";
					break;
				default:
					$content->values->Remuneration = "";
					break;
			}
		}

		// Receiver Data
		switch($copyfor) {
			case 1:
				$content->values->InvoiceFor = "Versicherer";
				$content->values->InvoiceType = "Rückforderungsbeleg";
				break;
			case 2:
				$content->values->InvoiceFor = "Rechnungssteller";
				$content->values->InvoiceType = "Rechnung";
				break;
			case 0:
			default:
				$content->values->InvoiceFor = "Rechnungsempfänger";
				$content->values->InvoiceType = "Rechnung";
				break;
		}

		// Reminder Stuff
		switch($invoice->status) {
			case $modelInvo::STATUS_REMINDER_1:
			case $modelInvo::STATUS_DEPOSIT_REMINDER_1:
				$reminderdate = strtotime($invoice->date_reminder_1);
				$content->values->InvoiceNumberReminder = $invoice->number;
				$content->values->InvoiceMonthReminder = date('m', $reminderdate);
				$content->values->InvoiceYearReminder = date('Y', $reminderdate);
				$content->values->InvoiceDayReminder = date('d', $reminderdate);
				$content->values->InvoiceDateReminder = date('d.m.Y', $reminderdate);
				$content->values->InvoiceType = "Erste Mahnung";
				$content->values->InvoicePaymentTerm = isset($organization->payment_term_reminder_first) ? $organization->payment_term_reminder_first : "";
				break;
			case $modelInvo::STATUS_REMINDER_2:
			case $modelInvo::STATUS_DEPOSIT_REMINDER_2:
				$reminderdate = strtotime($invoice->date_reminder_2);
				$content->values->InvoiceNumberReminder = $invoice->number;
				$content->values->InvoiceMonthReminder = date('m', $reminderdate);
				$content->values->InvoiceYearReminder = date('Y', $reminderdate);
				$content->values->InvoiceDayReminder = date('d', $reminderdate);
				$content->values->InvoiceDateReminder = date('d.m.Y', $reminderdate);
				$content->values->InvoiceType = "Zweite Mahnung";
				$content->values->InvoicePaymentTerm = isset($organization->payment_term_reminder_second) ? $organization->payment_term_reminder_second : "";
				break;
			case $modelInvo::STATUS_OPEN:
			case $modelInvo::STATUS_CANCELLED:
			case $modelInvo::STATUS_PAYED:
			case $modelInvo::STATUS_DEPOSIT:
			default:
				$content->values->InvoiceNumberReminder = "";
				$content->values->InvoiceDateReminder = "";
				$content->values->InvoiceMonthReminder = "";
				$content->values->InvoiceYearReminder = "";
				$content->values->InvoiceDayReminder = "";
				$content->values->InvoicePaymentTerm = isset($organization->payment_term) ? $organization->payment_term : "";
				break;
		}

		$totalPrice = 0;

		$content->values->InvoiceNumberKoGu = "";
		$content->values->InvoiceDateKoGu = "";
		$content->values->InvoiceCopy = "Ja";

		$content->values->Organization = $organization->name;
		$content->values->OrganizationAdressAdd = isset($organization->address_line) ? $organization->address_line : "";
		$content->values->OrganizationStreet = isset($organization->street) ? $organization->street : "";
		$content->values->OrganizationZip = isset($organization->zip) ? $organization->zip : "";
		$content->values->OrganizationCity = isset($organization->city) ? $organization->city : "";
		$content->values->OrganizationState = isset($organization->state) ? $organization->state : "";
		$content->values->OrganizationCountry = isset($organization->country) ? $organization->country : "";
		$content->values->OrganizationPhone = isset($organization->phone) ? $organization->phone : "";
		$content->values->OrganizationEmail = isset($organization->email) ? $organization->email : "";
		$content->values->OrganizationIban = isset($organization->bankaccount_iban) ? $organization->bankaccount_iban : "";
		$content->values->OrganizationBic = isset($organization->bankaccount_bic) ? $organization->bankaccount_bic : "";
		$content->values->OrganizationBank = isset($organization->bankaccount_bank) ? $organization->bankaccount_bank : "";
		$content->values->OrganizationAccountOwner = isset($organization->bankaccount_owner) ? $organization->bankaccount_owner : "";
		if(isset($organization->mwst) && $organization->mwst == true) {
			$content->values->OrganizationHasMWST = true;
			$content->values->OrganizationMwstNr = isset($organization->mwst_nr) ? $organization->mwst_nr : "";
		} else {
			$content->values->OrganizationHasMWST = false;
			$content->values->OrganizationMwstNr = "";
		}
		$content->values->OrganizationGLN = isset($organization->gln) ? $organization->gln : "";
		//$content->values->OrganizationZSR = isset($organization->zsr) ? $organization->zsr : "";
// TODO: Add right use of organization zsr
		$content->values->OrganizationZSR = isset($employee->zsr) ? $employee->zsr : "";
		$content->values->OrganizationPaymentTerm = isset($organization->payment_term) ? $organization->payment_term : "";
		$content->values->OrganizationFirstReminderCost = isset($organization->cost_reminder_first) ? $mathLib->priceConversion($organization->cost_reminder_first) : $mathLib->priceConversion(0);
		$content->values->OrganizationSecondReminderCost = isset($organization->cost_reminder_second) ? $mathLib->priceConversion($organization->cost_reminder_second) : $mathLib->priceConversion(0);

		// Customer Data
		$content->values->CustomerTitle = isset($customer->address) ? $customer->address : "";
		$content->values->CustomerFirstname = $customer->firstname;
		$content->values->CustomerName = $customer->name;
		$content->values->CustomerAdressAdd = isset($customer->address_line) ? $customer->address_line : "";
		$content->values->CustomerStreet = isset($customer->street) ? $customer->street : "";
		$content->values->CustomerZip = isset($customer->zip) ? $customer->zip : "";
		$content->values->CustomerCity = isset($customer->city) ? $customer->city : "";
		$content->values->CustomerState = isset($customer->state) ? $customer->state : "";
		$content->values->CustomerCountry = isset($customer->country) ? $customer->country : "";
		if(isset($customer->birth_day) && isset($customer->birth_month)) {
			$content->values->CustomerBirthday = (string)$customer->birth_day . "." . (string)$customer->birth_month . ".";
			if(isset($customer->birth_year)) {
				$content->values->CustomerBirthday .= $customer->birth_year;
			}
		} else {
			$content->values->CustomerBirthday = "";
		}
		if(isset($customer->gender)) {
			if($customer->gender == true) {
				$content->values->CustomerGender = "M";
			} else {
				$content->values->CustomerGender = "F";
			}
		} else {
			$content->values->CustomerGender = "";
		}
		$content->values->CustomerNr = isset($customer->nr) ? $customer->nr : "";
		$content->values->CustomerAHV = isset($customer->ahv) ? $customer->ahv : "";
		$content->values->CustomerVEKA = isset($customer->veka) ? $customer->veka : "";
		$content->values->CustomerInsuranceNr = isset($customer->insurance_nr) ? $customer->insurance_nr : "";
		$content->values->CustomerContractNr =  isset($customer->contract_nr) ? $customer->contract_nr : "";

		// Recipient data
		// Vormund / Bezugsperson (guardian) if set, customer = recipient otherwise
		if(!empty($customer->guardian_id)) {
			$guardian = $customer->guardian;
			$content->values->RecipientTitle = isset($guardian->address) ? $guardian->address : "";
			$content->values->RecipientFirstname = $guardian->firstname;
			$content->values->RecipientName = $guardian->name;
			$content->values->RecipientAdressAdd = isset($guardian->address_line) ? $guardian->address_line : "";
			$content->values->RecipientStreet = isset($guardian->street) ? $guardian->street : "";
			$content->values->RecipientZip = isset($guardian->zip) ? $guardian->zip : "";
			$content->values->RecipientCity = isset($guardian->city) ? $guardian->city : "";
			$content->values->RecipientState = isset($guardian->state) ? $guardian->state : "";
			$content->values->RecipientCountry = isset($guardian->country) ? $guardian->country : "";
		} else {
			$content->values->RecipientTitle = isset($customer->address) ? $customer->address : "";
			$content->values->RecipientFirstname = $customer->firstname;
			$content->values->RecipientName = $customer->name;
			$content->values->RecipientAdressAdd = isset($customer->address_line) ? $customer->address_line : "";
			$content->values->RecipientStreet = isset($customer->street) ? $customer->street : "";
			$content->values->RecipientZip = isset($customer->zip) ? $customer->zip : "";
			$content->values->RecipientCity = isset($customer->city) ? $customer->city : "";
			$content->values->RecipientState = isset($customer->state) ? $customer->state : "";
			$content->values->RecipientCountry = isset($customer->country) ? $customer->country : "";
		}

		// Therapist data
		$content->values->EmployeeFirstname = isset($employee->firstname) ? $employee->firstname : "";
		$content->values->EmployeeName = isset($employee->name) ? $employee->name : "";
		$content->values->EmployeeGLN = isset($employee->gln) ? $employee->gln : "";
		$content->values->EmployeeZSR = isset($employee->zsr) ? $employee->zsr : "";
// TODO: Addition of work address for employees
		// $content->values->EmployeeAdressAdd = isset($employee->address_line) ? $employee->address_line : "";
		// $content->values->EmployeeStreet = isset($employee->street) ? $employee->street : "";
		// $content->values->EmployeeZip = isset($employee->zip) ? $employee->zip : "";
		// $content->values->EmployeeCity = isset($employee->city) ? $employee->city : "";
		// $content->values->EmployeeState = isset($employee->state) ? $employee->state : "";
		// $content->values->EmployeeCountry = isset($employee->country) ? $employee->country : "";
		$content->values->EmployeeAdressAdd = isset($organization->address_line) ? $organization->address_line : "";
		$content->values->EmployeeStreet = isset($organization->street) ? $organization->street : "";
		$content->values->EmployeeZip = isset($organization->zip) ? $organization->zip : "";
		$content->values->EmployeeCity = isset($organization->city) ? $organization->city : "";
		$content->values->EmployeeState = isset($organization->state) ? $organization->state : "";
		$content->values->EmployeeCountry = isset($organization->country) ? $organization->country : "";
		$content->values->EmployeePhone = isset($employee->phone_work) ? $employee->phone_work : "";
		$content->values->EmployeeEmail = isset($employee->email) ? $employee->email : "";

		// Data of Referring Physician
		if(isset($case->referral_type)) {
			switch(intval($case->referral_type)) {
				case 1:
					$content->values->ReferringPhysicianType = "Arzt";
					break;
				case 2:
					$content->values->ReferringPhysicianType = "Therapeut";
					break;
				case 3:
					$content->values->ReferringPhysicianType = "Psychologe";
					break;
				case 4:
					$content->values->ReferringPhysicianType = "Psichiater";
					break;
				case 5:
					$content->values->ReferringPhysicianType = "Sonstige";
					break;
				default:
					$content->values->ReferringPhysicianType = "";
					break;
			}
		} else {
			$content->values->ReferringPhysicianType = "";
		}
		$content->values->ReferringPhysicianName = !empty($case->referral_name) ? $case->referral_name : "";
		$content->values->ReferringPhysicianGLN = !empty($case->referral_gln) ? $case->referral_gln : "";
		$content->values->ReferringPhysicianZSR = !empty($case->referral_zsr) ? $case->referral_zsr : "";

		// Therapy/Case data
		$content->values->CaseAccidentDate = isset($case->accident_date) ? date("d.m.Y", strtotime($case->accident_date)) : "";
		if(isset($case->law)) {
			switch($case->law) {
				case 1:
					$content->values->CaseLaw = "KVG";
					break;
				case 2:
					$content->values->CaseLaw = "UVG";
					break;
				case 3:
					$content->values->CaseLaw = "IVG";
					break;
				case 4:
					$content->values->CaseLaw = "MVG";
					break;
				case 5:
					$content->values->CaseLaw = "VVG";
					break;
				case 6:
					$content->values->CaseLaw = "ORG";
					break;
				default:
					$content->values->CaseLaw = "";
					break;
			}
		} else {
			$content->values->CaseLaw = "";
		}
		$content->values->InvoiceComment = !empty($case->comment_invoice) ? $case->comment_invoice : "";

		if(isset($case->therapy_type)) {
			switch(intval($case->therapy_type)) {
				case 1:
					$content->values->CaseType = "Einzeltherapie";
					break;
				case 2:
					$content->values->CaseType = "Gruppentherapie";
					break;
				default:
					$content->values->CaseType = "";
					break;
			}
		} else {
			$content->values->CaseType = "";
		}

		if(isset($case->type)) {
			switch(intval($case->type)) {
				case 1:
					$content->values->CaseReason = "Krankheit";
					break;
				case 2:
					$content->values->CaseReason = "Unfall";
					break;
				case 3:
					$content->values->CaseReason = "Prävention";
					break;
				case 4:
					$content->values->CaseReason = "Schwangerschaft";
					break;
				case 5:
					$content->values->CaseReason = "Geburtsgebrechen";
					break;
				default:
					$content->values->CaseReason = "";
					break;
			}
		} else {
			$content->values->CaseReason = "";
		}

		if(!empty($case->diagnosis_text)) {
			$content->values->CaseDiagnosisText = $case->diagnosis_text;
			if(isset($case->diagnosis_type)) {
				switch($case->diagnosis_type) {
					case 1:
						$content->values->CaseDiagnosisType = "Text";
						break;
					case 2:
						$content->values->CaseDiagnosisType = "ICD-10";
						break;
					case 3:
						$content->values->CaseDiagnosisType = "ICPC";
						break;
					case 4:
						$content->values->CaseDiagnosisType = "Tessiner Code";
						break;
					default:
						$content->values->CaseDiagnosisType = "Text";
						break;
				}
			} else {
				$content->values->CaseDiagnosisType = "Text";
			}
		} else {
			$content->values->CaseDiagnosisType = "";
			$content->values->CaseDiagnosisText = "";
		}

		$content->values->CasePeriod = "";

		// Bookings
		$content->dynamicTableContent = new \stdClass();
		$content->dynamicTableContent->InvoicePositions = array();

		// Preparation of Booking needed Variables
		$case_start = 0;
		$case_end = 0;

		$priceMWST0 = 0.0;
		$priceMWST25 = 0.0;
		$priceMWST8 = 0.0;

		foreach($invoice->invoicepositions as $invopos) {
			$invopos_date = strtotime($invopos->date);

			if((int)$invopos->type == $modelInvopos::TYPES["bookingpart"] || (int)$invopos->type == $modelInvopos::TYPES["product"]) {
				// Set Period Data for all invoice positions
				if(empty($case_start)) {
					$case_start = $invopos_date;
					$case_end = $invopos_date;
				} else {
					if($invopos_date < $case_start) {
						$case_start = $invopos_date;
					} else if ($invopos_date > $case_end) {
						$case_end = $invopos_date;
					}
				}
			}

			if((int)$invopos->type == $modelInvopos::TYPES["bookingpart"]) {

			}
			$pricePerPos = (float)$invopos->price / (float)$invopos->quantity;
			
			$price = $mathLib->priceRound((float)$invopos->price);
			// Price Handling and collecting
			$totalPrice += $price;
			// Generate Price per Minute
			$pricePerM = (float)$invopos->duration != 0 ? $price / (float)$invopos->duration : 0;
			// Generate Price per Hour
			$pricePerH = $pricePerM * 60;
			// Generate Price per 5 Min.

			if($content->values->OrganizationHasMWST && !empty($invopos->mwst)) {
				switch($invopos->mwst) {
					case 1:
						$mwst = 2.5;
						$priceMWST25 += $price;
						break;
					case 2:
						$mwst = 8.0;
						$priceMWST8 += $price;
						break;
					case 0:
					default:
						$mwst = 0.0;
						$priceMWST0 += $price;
						break;
				}
			} else {
				$mwst = 0.0;
			}

			// Create Data Object for Invoiceposition
			$invoposObj = new \stdClass();
			$invoposObj->date = date('d.m.Y', $invopos_date);
			if(!empty($invopos->title)) {
				$invoposObj->title = $invopos->title;
			} else if (!empty($invopos->tariffposition_title)) {
				$invoposObj->title = $invopos->tariffposition_title;
			} else {
				$invoposObj->title = "";
			}
			$invoposObj->duration = number_format($invopos->duration, 2, ".", ",");
			$invoposObj->pricePerHour = $mathLib->priceConversion($pricePerH);
			$invoposObj->price = $mathLib->priceConversion($price, true);
			$invoposObj->mwstpercent = number_format($mwst, 1, ".", ",") . "%";
			$invoposObj->mwstvalue = $mathLib->priceConversion($mathLib->mwstPart($price), true);
			$invoposObj->pricewomwst = $mathLib->priceConversion($price - $mathLib->mwstPart($price), true);
			$invoposObj->tarif = !empty($invopos->tariff) ? (string)$invopos->tariff : "";
			$invoposObj->tarifnr = !empty($invopos->tariffposition_nr) ? (string)$invopos->tariffposition_nr : "";
			$invoposObj->tarifdescription = !empty($invopos->tariffposition_title) ? (string)$invopos->tariffposition_title : $invopos->title;
			$invoposObj->count = $mathLib->priceConversion($invopos->quantity);
			$invoposObj->pricePerTerm = $mathLib->priceConversion($pricePerPos);
			$invoposObj->tpw = $mathLib->priceConversion(1);

			// Add the Object to the dynamic Table Content Array
			$content->dynamicTableContent->InvoicePositions[] = $invoposObj;

		}

		$content->values->CasePeriod = date('d.m.Y', $case_start) . " - " . date('d.m.Y', $case_end);

		// MWST Stuff
		$content->values->InvoiceBetragSum0 = $content->values->OrganizationHasMWST ? $mathLib->priceConversion($priceMWST0, true) : "0.00";
		$content->values->InvoiceBetragSum25 = $content->values->OrganizationHasMWST ? $mathLib->priceConversion($priceMWST25, true) : "0.00";
		$content->values->InvoiceBetragSum8 = $content->values->OrganizationHasMWST ? $mathLib->priceConversion($priceMWST8, true) : "0.00";
		$content->values->InvoiceMWSTSum0 = $content->values->OrganizationHasMWST ? $mathLib->priceConversion($mathLib->mwstPart($priceMWST0, 0), true) : "0.00";
		$content->values->InvoiceMWSTSum25 = $content->values->OrganizationHasMWST ? $mathLib->priceConversion($mathLib->mwstPart($priceMWST25, 2.5), true) : "0.00";
		$content->values->InvoiceMWSTSum8 = $content->values->OrganizationHasMWST ? $mathLib->priceConversion($mathLib->mwstPart($priceMWST8, 8), true) : "0.00";

// TODO: Deposit Opportunity
		$deposit = 0;
		$dueValue = $totalPrice - $deposit;

		$content->values->InvoiceTotal = $mathLib->priceConversion($totalPrice, true);
		$content->values->InvoiceDeposit = $mathLib->priceConversion($deposit, true);
		$content->values->InvoiceDue = $mathLib->priceConversion($dueValue, true);

		$content->documentSettings = new \stdClass();
		$content->documentSettings->author = $organization->name;
		$content->documentSettings->title = "Rechnung Nr. " . $invoice->number;
		$content->documentSettings->producer = "bluebamboo Invoice PDF Creator";
		$content->documentSettings->creator = "bluebamboo Invoice PDF Creator";

		// Add Datamatrix Code:
		$content->values->InvoiceDatamatrix = $this->generateDataMatrixValue($invoice, $employee, date('Y-m-d', $case_start));

		return $content;
	}


	/**
	 * Generate a unique Invoice Number
	 */
	protected function generateInvoId590($invoDate)
	{
		$id = strtotime($invoDate . " Europe/Zurich");
		$date = date("d.m.Y H:i:s", strtotime($invoDate));
		return $id . " " . $date;
	}

	/**
	 * Generate a unique Invoice Number
	 */
	protected function generateInvoNumber($invoDate, $customer)
	{
		$date = strtotime($invoDate);
		$dateStr = date('Ymd', $date);
		$custStr = str_pad($customer->nr, 4, "0", STR_PAD_LEFT);
		$invoNumb = count($customer->invoices) + 1;
		$countStr = str_pad($invoNumb, 3, "0", STR_PAD_LEFT);
		return $dateStr . "-" . $custStr . "-" . $countStr;
	}

// TODO:
	/**
	 * Generate a unique Invoice Number
	 */
	protected function generateReminderNumber($reminderDate, $invoice, $customer, $reminder = 1)
	{
		$date = strtotime($reminderDate);
		$dateStr = date('Ymd', $date);
		$custStr = str_pad($customer->nr, 4, "0", STR_PAD_LEFT);
		$invoNumb = $invoice->nr;
		$countStr = str_pad($invoNumb, 3, "0", STR_PAD_LEFT);
		return $dateStr . "-" . $custStr . "-" . $countStr;
	}

	/**
	 * download of a invoice pdf
	 */
	public function invoDownloadAction($file)
	{
		try {
			$this->view->disable();
			//$this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);

			$this->grantAccessForClient();
			$pdfPath = $this->getClientInvoDocPath();
			$path = $this->getFileSwitch()->getFile($pdfPath.$file);

			if(empty($path)) {
				throw new \Exception("File not found!", 404);
			}

			$response = $this->response;
			$response->setHeader('Content-Length', filesize($path));
			$response->setContentType('application/pdf');
			$response->setFileToSend($path);
			!$response->isSent() && $response->send();
		}
		catch (\Exception $err) {
			if ($err->getCode() == 403) {
				$this->response->setStatusCode(403, "Forbidden: " . $err->getMessage());
			}
			else {
				$this->response->setStatusCode(400, "Bad Request: " . $err->getMessage());
//				$this->logger->log("error: " . json_encode(debug_backtrace(), JSON_PRETTY_PRINT));
//				$this->logger->log("error: " . json_encode($err->getTrace(), JSON_PRETTY_PRINT));
			}
			return $this->response->send();
		}
	}

	/**
	 * directly show an invoice PDF in the browser
	 */
	public function invoPDFDownloadAction($filename)
	{
		try {
			$this->view->disable();
			//$this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);

			$this->grantAccessForClient();
			$pdfPath = $this->getClientInvoDocPath();
			$file = $this->getFileSwitch()->getFile($pdfPath.$filename);

			if(!is_file($file)) {
				throw new \Exception("File not found!", 404);
			}

			$response = $this->response;
			$response->setHeader('Content-Length', filesize($file));
			$response->setContentType('application/pdf');
			$response->setFileToSend($file, null, false);
			$response->setHeader('Content-Disposition', "inline; filename=" . $file);
			!$response->isSent() && $response->send();
		}
		catch (\Exception $err) {
			if ($err->getCode() == 403) {
				$this->response->setStatusCode(403, "Forbidden: " . $err->getMessage());
			}
			else {
				$this->response->setStatusCode(400, "Bad Request: " . $err->getMessage());
//				$this->logger->log("error: " . json_encode(debug_backtrace(), JSON_PRETTY_PRINT));
//				$this->logger->log("error: " . json_encode($err->getTrace(), JSON_PRETTY_PRINT));
			}
			return $this->response->send();
		}
	}

	/**
	 * generate a tariff 590 datamatrix value
	 */
	public function generateDataMatrixValue($invoice, $employee = "", $date_first = "")
	{
		$cust = $invoice->customer;

		$randNr = mt_rand(10, 99);
		$dateTime = date('dmYHis', strtotime($invoice->date));
		$zsr = (int)preg_replace('/[^0-9]/', '' , $employee->zsr);
		$birth_cust = $cust->birth_year . "-" . $cust->birth_month .  "-" . $cust->birth_day;
		$birth_cust = date_create($birth_cust);
		$relDate = date_create("1900-01-01");
		$birthdiff = (int)(date_diff($relDate, $birth_cust)->format("%a"));
		$zip_cust = (int)$cust->zip;
		$value = round((float)$invoice->value);
		$first_date = date_create($date_first);
		$datediff = (int)(date_diff($relDate, $first_date)->format("%a"));
		$pruefziff = $zsr + $birthdiff + $zip_cust + $value + $datediff;

		$code  = "/-/#";
		$code .= $randNr;
		$code .= "#";
		$code .= $dateTime;
		$code .= "#";
		$code .= $pruefziff;

		//return "/-/#14#10012017213349#32362";
		return $code;
	}

	/**
	 * create new Invoicepositions for the related bookingparts and product purchases
	 * returns: array with the new invoiceposition objects
	 */
	protected function _createAddInvoPositions($bookings = array(), $products = array())
	{
		$modelInvopos = "Models\V1\Invo\Invoicepositions";
		$modelBookings = "Models\V1\Booking\Bookings";
		$client_id = $this->session->get("auth-identity-client");
		$addPositions = array();

		foreach($bookings as $booking) {
			if($booking->status == $modelBookings::STATUS_LATECANCELLED || $booking->status == $modelBookings::STATUS_CANCELLED_RESCINDED_OPEN) {
				if(!empty($booking->price)) {
					// Create the Invoice Request
					$invopos = new \stdClass();
					$invopos->type = $modelInvopos::TYPES("cancellation");
					$invopos->title = "Verpasster Termin / Zu spät abgesagter Termin";
					$invopos->date = $booking->date;
					$invopos->tariff = 999;
					$invopos->tariffposition_nr = "";
					$invopos->tariffposition_title = "Verpasster Termin / Zu spät abgesagter Termin";
					$invopos->quantity = 1;
					$invopos->price = floatval($booking->price);
					//$invopos->duration = 0;
					$invopos->mwst = 0;
					//$invopos->bookingpart_id = $bookingpart->id;
					// $invopos->invoice_id = $invoice_id;
					$invopos->client_id = $client_id;

					$invoPosition = $this->_createActionObj($invopos, $modelInvopos, $modelInvopos::$attrs);
					$addPositions[] = $invoPosition;
				}
			} else {
				$bparts = $booking->bookingparts;
				$discount = empty($booking->discount) ? 0 : (float)$booking->discount;

				foreach($bparts as $bookingpart) {
					// TODO: maybe also show bookings with duration and price 0 in Invoice
					//if(!empty($bookingpart->duration) && !empty($bookingpart->price)) {
					// Create the Invoice Request
					$invopos = new \stdClass();
					$invopos->type = 1;
					$invopos->title = !empty($bookingpart->title) ? $bookingpart->title : "";
					$invopos->date = $booking->date;
					$invopos->tariff = !empty($bookingpart->tariff) ? $bookingpart->tariff : "";
					$invopos->tariffposition_nr = isset($bookingpart->tariffposition) ? $bookingpart->tariffposition->nr : "";
					$invopos->tariffposition_title = isset($bookingpart->tariffposition) ? $bookingpart->tariffposition->title : "";
					$invopos->quantity = floatval($bookingpart->duration) / 5;
					//$invopos->price = floatval($bookingpart->price);
					$invopos->price = $this->calcPriceFromBookingpartPrice($bookingpart, $discount);
					$invopos->duration = $bookingpart->duration;
					$invopos->mwst = !empty($bookingpart->mwst_group) ? $bookingpart->mwst_group : NULL;
					$invopos->bookingpart_id = $bookingpart->id;
					// $invopos->invoice_id = $invoice_id;
					$invopos->client_id = $client_id;

					$invoPosition = $this->_createActionObj($invopos, $modelInvopos, $modelInvopos::$attrs);
					$addPositions[] = $invoPosition;
					//}
				}
			}
		}

		// TODO: Add Handling for products

		return $addPositions;
	}


	/**
	 * Get the Invoice Templates of a given role
	 */
	protected function getInvoiceTemplates($role = 0) {
		$modelTemp = "Models\V1\Pdf\Templates";
		$modelGroups = "Models\V1\Pdf\Groups";
		$modelRoles = "Models\V1\Pdf\Roles";

		if($role == 0) {
			$role == $modelRoles::ROLES['INVOICE'];
		}

		$group = $modelGroups::GROUPS['INVOICES'];
		$client_id = $this->session->get("auth-identity-client");

		$rows = $this->modelsManager->createBuilder()
			->columns('t.*')
			->from(['t' => $modelTemp])
			->where("t.client_id = :cid:", ["cid" => $client_id])
			->andWhere("t.group_id = :gid:", ["gid" => $group])
			->andWhere("t.role_id = :rid:", ["rid" => $role])
			->andWhere("t.active = 1")
			->orderBy('t.id')
			->getQuery()
			->execute();

		$ret = array();

		if(!empty($rows)) {
			foreach($rows as $row) {
				$ret[] = $this->_valueConversionPostload($row, $modelTemp::$attrs);
			}
		}

		return $ret;
	}

	/**
	 * Create an Invoice PDF
	 */
	protected function createInvoicePDF($invoice, $organization, $case, $employee, $customer, $filename = null, $role = 1, $copy = false) {

		$transaction = $this->transaction;

		$sysInfo = "\GSCLibrary\SystemInfo";

		// Get Path for Invoice pdfs
		$pdfPath = $this->getClientInvoDocPath();
		$tempPath = $this->getTempPath();

		$invoFile = empty($filename) ? $sysInfo::uniqueFilename("pdf") : $filename;

		// Start new Merge-PDF for the Invoicerequest and DocumentGenerator
		$invoPDF = new \DocumentGenerator\PDFMerger();
		$docGen = new \DocumentGenerator\DocumentGenerator();

		$templates = $this->getInvoiceTemplates($role);
		if(count($templates) == 0) {
			throw new \Exception("No Invoice Templates defined!", 400);
		}

		$deleteFiles = array();

		if($copy) {
			$for = 2;
		} else {
			$for = 0;
		}

		// Generate Invoice PDF Content JSONs
		$content = $this->generateInvoContent($customer, $employee, $organization, $invoice, $case, $for, $copy);
		$contentJSON = json_encode($content, JSON_PRETTY_PRINT);
		$docGen->setContent($contentJSON);

		foreach($templates as $template) {
			$layoutJSON = json_encode($template->template,  JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT | JSON_FORCE_OBJECT);

			$file = $tempPath . $sysInfo::uniqueFilename("pdf");
			$docGen->setLayout($layoutJSON);
			if(!$docGen->createPDF($file)) {
				throw new \Exception("Invoice with Template " . $template->title . " could not be created!", 404);
			}
			$invoPDF->addPDF($file, 'all');
			$deleteFiles[] = $file;
		}

		// Output the merge-print-file PDF
		$docSettings = $content->documentSettings;

		if(!$invoPDF->merge('file', $tempPath.$invoFile, $docSettings)) {
			throw new \Exception("Merge Invoice File could not be created", 404);
		}
		if(!$this->getFileSwitch()->saveFileFromTemp($invoFile, $pdfPath.$invoFile)) {
			throw new \Exception("Merge Invoice File could not be saved/stored!", 404);
		}

		// delete all temporarly saved deleteFiles
		foreach($deleteFiles as $f) {
			unlink($f);
		}

		return $invoFile;
	}


	protected function calcPriceFromBookingpartPrice($bookingpart, $discount = 0) {
		$modelHourly = "Models\V1\Insu\Hourlyrates";
		$mathLib = \GSCLibrary\GSCFinancialMath::getInstance();

		$price = (float)$bookingpart->price;
		// Reduce price by discount
		$price = $price * (1 - ($discount / 100));

		$customer = $bookingpart->booking->customer;
		$insurance_id = $customer->insurance_id;
		if(empty($insurance_id)) { return $price; }

		$rate = null;
		$rates = $modelHourly::findByInsurance_id($insurance_id);
		if(count($rates) == 0) { return $price; }
		if(count($rates) == 1 && $rates[0]->allPositions()) {
			$rate = $rates[0];
		} else {
			foreach($rates as $r) {
				if(!$r->allPositions() && (int)$r->tariffposition_id == (int)$bookingpart->tariffposition_id) {
					$rate = $r;
					break;
				}
			}
		}

		if($rate === null) { return $price; }
		if(empty($rate->max_pph)) { return $price; }

		$r_pph = (float)$rate->max_pph;
		$b_duration = (int)$bookingpart->duration;
		$b_price = $price;
		$includeMWST = isset($rate->includeMWST) ? boolval($rate->includeMWST) : true;
		
		if($includeMWST) {
			// b_price is incl. mwst - calculate b_pph directly
			$b_pph = $b_price / $b_duration * 60;
			if($b_pph > $r_pph) {
				$price = $r_pph / 60 * $b_duration;
				// Round price down to next 5 Cent value to ever be below maximum rate
				$price = $mathLib->priceRoundFloor($price);
				return $price;
			}
		} else {
			// b_price is incl. mwst - calculate price excl. mwst first and then compare
			$mwstPercent = $bookingpart->mwstPercent();
			$b_price_excl = $mathLib->priceExclMwst($b_price, $mwstPercent);
			$b_pph = $b_price_excl / $b_duration * 60;
			if($b_pph > $r_pph) {
				$price_excl = $r_pph / 60 * $b_duration;
				$price = $mathLib->priceInclMwst($price_excl, $mwstPercent);
				// Round price down to next 5 Cent value to ever be below maximum rate
				$price = $mathLib->priceRoundFloor($price);
				return $price;
			}
		}

		return $price;
	}
}
