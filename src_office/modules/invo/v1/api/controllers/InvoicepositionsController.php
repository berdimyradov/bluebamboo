<?php

/**
 * Controller class InvoicesController
 */

namespace Api\V1\Invo\Controllers;

class InvoicepositionsController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Invo\Invoicepositions";

	/**
	 * returns data with the option to select fields, filter results and sort result.
	 * authentication required
	 */
	public function indexAction () {

		// site id or similar filter? just about access rights

		$default_projection		= array("o.id", "o.type", "o.title", "o.date", "o.tariff", "o.tariffposition_nr", "o.tariffposition_title", "o.quantity", "o.price", "o.duration", "o.mwst", "o.bookingpart_id", "o.invoice_id", "o.client_id");
		$orderby				= array("o.date", "o.id");
		$attrs_for_projection	= array("o.id", "o.type", "o.title", "o.date", "o.tariff", "o.tariffposition_nr", "o.tariffposition_title", "o.quantity", "o.price", "o.duration", "o.mwst", "o.bookingpart_id", "o.invoice_id", "o.client_id");
		$attrs_for_where		= array("o.id", "o.type", "o.title", "o.date", "o.tariff", "o.tariffposition_nr", "o.tariffposition_title", "o.quantity", "o.price", "o.duration", "o.mwst", "o.bookingpart_id", "o.invoice_id", "o.client_id");
		$attrs_for_orderby		= array("o.id", "o.type", "o.title", "o.date", "o.tariff", "o.tariffposition_nr", "o.tariffposition_title", "o.quantity", "o.price", "o.duration", "o.mwst", "o.bookingpart_id", "o.invoice_id", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/**
	 * lookup for index
	 */
	public function indexLookupAction () {
		$projection				= array("o.id", "o.date");
		$lookup_fields			= array("id", "date", "title");
		$orderby				= array("o.date", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->title = "";
			$l->title .= $obj->title;
//			$l->desc = $obj->description;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/**
	 * returns one invoice
	 */
	public function oneAction($id) {
		$model = $this->from;

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_oneAction($model, $id, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * create action
	 */
	public function createAction() {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);
		$model = $this->from;
		return $this->_createAction($model, $model::$attrs);
	}

	/**
	 * updates an existing invoice
	 */
	public function updateAction($id) {
		$model = $this->from;
		return $this->_updateAction($model, $model::$attrs, $id);
	}

	/**
	 * deletes an existing invoice
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

}
