<?php

/**
 * module-config for V1/API/Invo API
 */

namespace Api\V1\Invo;

use Phalcon\Loader,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\View,
    Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface
{
    public function registerAutoloaders(\Phalcon\DiInterface $dependencyInjector = null)
    {
        $loader = new Loader();
        $loader->registerNamespaces(
            array(
                'GSCLibrary'					=> '../library/',
				'DocumentGenerator'				=> '../library/',

				'Models\V1\CMS'					=> '../modules/cms/v1/models/',

                'Models\V1\Booking'				=> '../modules/booking/v1/models/',
                'Models\V1\Cust'				=> '../modules/cust/v1/models/',
                'Models\V1\Invo'				=> '../modules/invo/v1/models/',
                'Models\V1\Insu'                => '../modules/insu/v1/models',
                'Models\V1\Loc'					=> '../modules/how/v1/models/',
                'Models\V1\Org'					=> '../modules/org/v1/models/',
				'Models\V1\Pdf'					=> '../modules/pdf/v1/models',
                'Models\V1\Prod'				=> '../modules/prod/v1/models/',
                'Models\V1\Relations'			=> '../modules/relations/v1/models/',
                'Models\V1\Sys'					=> '../modules/sys/v1/models/',

                'Api\V1\Invo\Logic' 			=> '../modules/invo/v1/api/logic/',
                'Api\V1\Invo\Controllers' 		=> '../modules/invo/v1/api/controllers/',
            )
        );
        $loader->register();
    }

    public function registerServices(\Phalcon\DiInterface $di)
    {
        $di->set('dispatcher', function() {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace("Api\V1\Invo\Controllers");
            return $dispatcher;
        });

        $di->set('view', function() {
            $view = new View();
//            $view->setViewsDir('../app/api/v1/invo/views/');
            return $view;
        });
    }
}
