<?php

/**
 * Model for app "romi", class "Invoices"
 */

namespace Models\V1\Invo;

class Payments extends \Phalcon\Mvc\Model
{
	public $id;
	public $created_at;
	public $payed_by;
	public $payed_at;
	public $value;
	public $bankaccount_iban;
	public $bankaccount_bic;
	public $bankaccount_bank;
	public $bankaccount_owner;
	public $comment;
	public $customer_id;
	public $client_id;

	public static $alias_one = "payment";
	public static $alias_many = "payments";

	public static $attrs = array(
		'id' => 'int',
		'created_at' => 'timestamp',
		'payed_by' => 'int',
		'payed_at' => 'date',
		'value' => 'decimal',
		'bankaccount_iban' => 'string',
		'bankaccount_bic' => 'string',
		'bankaccount_bank' => 'string',
		'bankaccount_owner' => 'string',
		'comment' => 'string',
		'customer_id' => 'int',
		'client_id' => 'int',
	);

	public static $relations = array(
		"customer" => [
			"type" => "belongsTo",
			"alias" => "customer",
			"relationModel" => "Models\V1\Cust\Customers",
			"relationIdAlias" => "customer_id"
		],
		"invoices" => [
			"type" => "manyToMany",
			"alias" => "invoices",
			"aliasOneObj" => "invoice",
			"throughAlias" => "paymentsInvoices",
			"throughModel" => "Models\V1\Invo\PaymentsInvoices",
			"throughIdAlias" => "payment_id",
			"throughRelIdAlias" => "invoice_id",
			"relationModel" => "Models\V1\Invo\Invoices"
		],
	);

	public function columnMap()
	{
		return array(
			'id' => 'id',
			'created_at' => 'created_at',
			'payed_by' => 'payed_by',
			'payed_at' => 'payed_at',
			'value' => 'value',
			'bankaccount_iban' => 'bankaccount_iban',
			'bankaccount_bic' => 'bankaccount_bic',
			'bankaccount_bank' => 'bankaccount_bank',
			'bankaccount_owner' => 'bankaccount_owner',
			'comment' => 'comment',
			'customer_id' => 'customer_id',
			'client_id' => 'client_id',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_invo_payments");

		$this->hasMany(
			"id",
			"Models\V1\Invo\PaymentsInvoices",
			"payment_id",
			[
				"alias" => "paymentsInvoices",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Invo\PaymentsInvoices",
			"payment_id",
			"invoice_id",
			"Models\V1\Invo\Invoices",
			"id",
			[
				"alias" => "invoices",
			]
		);

		$this->belongsTo(
			"customer_id",
			"Models\V1\Cust\Customers",
			"id",
			[
				"alias" => "customer",
			]
		);
	}

}
