<?php

/**
 * Model for app "romi", class "Invoices"
 */

namespace Models\V1\Invo;

class InvoicesInvoicerequests extends \Phalcon\Mvc\Model
{

	public $id;
	public $invoicerequest_id;
	public $invoice_id;

	public static $attrs = array(
		"id" => "int",
		"invoicerequest_id" => "int",
		"invoice_id" => "int",
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
		'id' => 'id',
		'invoicerequest_id' => 'invoicerequest_id',
		'invoice_id' => 'invoice_id',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_invo_invoices_invoicerequests");

		$this->belongsTo(
			"invoicerequest_id",
			"Models\V1\Invo\Invoicerequests",
			"id",
			[
				"alias" => "invoicerequest",
			]
		);

		$this->belongsTo(
			"invoice_id",
			"Models\V1\Invo\Invoices",
			"id",
			[
				"alias" => "invoice",
			]
		);
	}

}
