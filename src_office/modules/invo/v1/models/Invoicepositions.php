<?php

/**
 * Model for app "romi", class "Invoices"
 */

namespace Models\V1\Invo;

class Invoicepositions extends \GSCLibrary\GSCModel
{

	public $id;
	public $type;
	public $title;
	public $date;
	public $tariff;
	public $tariffposition_nr;
	public $tariffposition_title;
	public $quantity;
	public $price;
	public $duration;
	public $mwst;
	public $bookingpart_id;
	public $invoice_id;
	public $client_id;

	public static $alias_one = "invoiceposition";
	public static $alias_many = "invoicepositions";

	const TYPE_BOOKINGPART = 1;
	const TYPE_PRODUCT = 2;
	const TYPE_REMINDER_1 = 10;
	const TYPE_REMINDER_2 = 11;
	const TYPE_CANCELLATION = 20;
	const TYPES = array(
		"bookingpart" => 1,
		"product" => 2,
		"reminder1" => 10,
		"reminder2" => 11,
		"cancellation" => 20,
	);

	public static $attrs = array(
		'id' => 'int',
		'type' => 'int',
		'title' => 'string',
		'date' => 'date',
		'tariff' => 'int',
		'tariffposition_nr' => 'int',
		'tariffposition_title' => 'string',
		'quantity' => 'float',
		'price' => 'decimal',
		'duration' => 'float',
		'mwst' => 'float',
		'bookingpart_id' => 'int',
		'invoice_id' => 'int',
		'client_id' => 'int',
	);

	public static $relations = array(
		"invoice" => [
			"type" => "belongsTo",
			"alias" => "invoice",
			"relationModel" => "Models\V1\Invo\Invoices",
			"relationIdAlias" => "invoice_id"
		],
		"bookingpart" => [
			"type" => "belongsTo",
			"alias" => "bookingpart",
			"relationModel" => "Models\V1\Booking\Bookingparts",
			"relationIdAlias" => "bookingpart_id"
		],
	);

	public function columnMap()
	{
		return array(
			'id' => 'id',
			'type' => 'type',
			'title' => 'title',
			'date' => 'date',
			'tariff' => 'tariff',
			'tariffposition_nr' => 'tariffposition_nr',
			'tariffposition_title' => 'tariffposition_title',
			'quantity' => 'quantity',
			'price' => 'price',
			'duration' => 'duration',
			'mwst' => 'mwst',
			'bookingpart_id' => 'bookingpart_id',
			'invoice_id' => 'invoice_id',
			'client_id' => 'client_id',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_invo_invoicepositions");

		$this->belongsTo(
			"invoice_id",
			"Models\V1\Invo\Invoices",
			"id",
			[
				"alias" => "invoice",
			]
		);

		$this->belongsTo(
			"bookinpart_id",
			"Models\V1\Booking\Bookingparts",
			"id",
			[
				"alias" => "bookingpart",
			]
		);
	}

}
