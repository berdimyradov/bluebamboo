<?php

// DEPRECATED!!!


/**
 * Model for app "romi", class "Templates"
 *  DEPRECATED!!!
 */

namespace Models\V1\Invo;

class Templates extends \Phalcon\Mvc\Model
{

	public $id;

	public $title;

	public $template;

	public $isDefault;

	public $client_id;

	public static $attrs = array(
		'id' => 'int',
		'title' => 'string',
		'template' => 'json',
		'isDefault' => 'bool',
		'client_id' => 'int',
	);

	public function columnMap()
	{
		return array(
		'id' => 'id',
		'title' => 'title',
		'template' => 'template',
		'isDefault' => 'isDefault',
		'client_id' => 'client_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
    {
		$this->setSource("gsc_invo_templates");
	}

	/**
	 * Tariff 590 Template is the same for all Clients
	 */
	public static function getTariff590TemplateJSON() {
		// Search for Template Entry
		$template = self::findFirstByTitle('Tariff 590 Template');
		if(!$template) {
			throw new \Exception("No Tariff 590 Template defined", 404);
		} else {
			return $template->template;
		}
	}

	/**
	 * Get the default Layout JSON for the Invoice PDF
	 */
	public static function getLayoutJSON() {
		$session = \Phalcon\Di::getDefault()->getSession();
		$client_id = $session->get("auth-identity-client");

		// Conditions for searching the template
		$searchConditions  = "client_id = " . $client_id; // search for Client ID
		$searchConditions .= " AND isDefault = 1"; // only the default Template

		$temp = self::findFirst(["conditions" => $searchConditions]);

		if($temp == null) {
			throw new \Exception("No default template found", 404);
		}

		return $temp->template;
	}

}
