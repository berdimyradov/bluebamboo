<?php

/**
 * Model for app, class "InvoiceRequests"
 */

namespace Models\V1\Invo;

class Invoicerequests extends \Phalcon\Mvc\Model
{

	public $id;
	public $type;
	public $createdAt;
	public $file;
	public $client_id;

	const TYPES = array(
		"create" => 1,
		"preprint" => 2,
		"remind" => 3,
	);

	public static $attrs = array(
		'id' => 'int',
		'type' => 'int',
		'createdAt' => 'timestamp',
		'file' => 'string',
		'client_id' => 'int',
	);

	public static $relations = array(
		"invoices" => [
			"type" => "manyToMany",
			"alias" => "invoices",
			"aliasOneObj" => "invoice",
			"throughAlias" => "invoicesInvoicerequests",
			"throughModel" => "Models\V1\Invo\InvoicesInvoicerequests",
			"throughIdAlias" => "invoicerequest_id",
			"throughRelIdAlias" => "invoice_id",
			"relationModel" => "Models\V1\Invo\Invoices"
		]
	);

	public function columnMap()
	{
		return array(
		'id' => 'id',
		'type' => 'type',
		'createdAt' => 'createdAt',
		'file' => 'file',
		'client_id' => 'client_id',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_invo_invoicerequests");

		$this->hasMany(
			"id",
			"Models\V1\Invo\InvoicesInvoicerequests",
			"invoicerequest_id",
			[
				"alias" => "invoicesInvoicerequests",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Invo\InvoicesInvoicerequests",
			"invoicerequest_id",
			"invoice_id",
			"Models\V1\Invo\Invoices",
			"id",
			[
				"alias" => "invoices",
			]
		);

		/*
		$this->hasMany(
			"id",
			"Models\V1\Invo\Invoices",
			"invoicerequest_id",
			[
				"alias" => "invoices",
			]
		);
		*/
	}

}
