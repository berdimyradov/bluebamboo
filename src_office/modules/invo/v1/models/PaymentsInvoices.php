<?php

/**
 * Model for app "romi", class "Invoices"
 */

namespace Models\V1\Invo;

class PaymentsInvoices extends \Phalcon\Mvc\Model
{

	public $id;
	public $payment_id;
	public $invoice_id;
	public $value;

	public static $attrs = array(
		"id" => "int",
		"payment_id" => "int",
		"invoice_id" => "int",
		"value" => "decimal",
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
		'id' => 'id',
		'payment_id' => 'payment_id',
		'invoice_id' => 'invoice_id',
		'value' => 'value',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_invo_payments_invoices");

		$this->belongsTo(
			"payment_id",
			"Models\V1\Invo\Payments",
			"id",
			[
				"alias" => "payment",
			]
		);

		$this->belongsTo(
			"invoice_id",
			"Models\V1\Invo\Invoices",
			"id",
			[
				"alias" => "invoice",
			]
		);
	}

}
