<?php

/**
 * Model for app "romi", class "Invoices"
 */

namespace Models\V1\Invo;

class Invoices extends \Phalcon\Mvc\Model
{
	public $id;
	public $number;
	public $date;
	public $date_reminder_1;
	public $date_reminder_2;
	public $file;
	public $file_reminder_1;
	public $file_reminder_2;
	public $file_copy_insurance;
	public $file_copy_archive;
	public $value;
	public $customer_name;
	public $comment;
	public $status;
	public $organization_id;
	public $customer_id;
	public $client_id;

	public static $alias_one = "invoice";
	public static $alias_many = "invoices";

	// Status constants:
	const STATUS_OPEN = 'ope';
	const STATUS_CANCELLED = 'can';
	const STATUS_PAYED = 'pay';
	const STATUS_REMINDER_1 = 're1';
	const STATUS_REMINDER_2 = 're2';
	const STATUS_DEPOSIT = 'dep';
	const STATUS_DEPOSIT_REMINDER_1 = 'dr1';
	const STATUS_DEPOSIT_REMINDER_2 = 'dr2';

	public static $attrs = array(
		'id' => 'int',
		'number' => 'string',
		'date' => 'datetime',
		'date_reminder_1' => 'date',
		'date_reminder_2' => 'date',
		'file' => 'string',
		'file_reminder_1' => 'string',
		'file_reminder_2' => 'string',
		'file_copy_insurance' => 'string',
		'file_copy_archive' => 'string',
		'value' => 'float',
		'customer_name' => 'string',
		'comment' => 'string',
		'status' => 'string',
		'organization_id' => 'int',
		'customer_id' => 'int',
		'client_id' => 'int',
	);

	public static $relations = array(
		"organization" => [
			"type" => "belongsTo",
			"alias" => "organization",
			"relationModel" => "Models\V1\Org\Organization",
			"relationIdAlias" => "organization_id"
		],
		/*
		"invoicerequest" => [
			"type" => "belongsTo",
			"alias" => "invoicerequest",
			"relationModel" => "Models\V1\Invo\Invoicerequests",
			"relationIdAlias" => "invoicerequest_id"
		],
		*/
		"invoicerequests" => [
			"type" => "manyToMany",
			"alias" => "invoicerequests",
			"aliasOneObj" => "invoicerequest",
			"throughAlias" => "invoicesInvoicerequests",
			"throughModel" => "Models\V1\Invo\InvoicesInvoicerequests",
			"throughIdAlias" => "invoice_id",
			"throughRelIdAlias" => "invoicerequest_id",
			"relationModel" => "Models\V1\Invo\Invoicerequests"
		],
		"customer" => [
			"type" => "belongsTo",
			"alias" => "customer",
			"relationModel" => "Models\V1\Cust\Customers",
			"relationIdAlias" => "customer_id"
		],
		/*
		"bookings" => [
			"type" => "hasMany",
			"alias" => "bookings",
			"relationModel" => "Models\V1\Booking\Bookings",
			"relationIdAlias" => "invoice_id"
		],
		*/
		"invoicepositions" => [
			"type" => "hasMany",
			"alias" => "invoicepositions",
			"relationModel" => "Models\V1\Invo\Invoicepositions",
			"relationIdAlias" => "invoice_id"
		],
		"payments" => [
			"type" => "manyToMany",
			"alias" => "payments",
			"aliasOneObj" => "payment",
			"throughAlias" => "paymentsInvoices",
			"throughModel" => "Models\V1\Invo\PaymentsInvoices",
			"throughIdAlias" => "invoice_id",
			"throughRelIdAlias" => "payment_id",
			"relationModel" => "Models\V1\Invo\Payments"
		],
		"bookings" => [
			"type" => "manyToMany",
			"alias" => "bookings",
			"aliasOneObj" => "booking",
			"throughAlias" => "bookingsInvoices",
			"throughModel" => "Models\V1\Relations\BookingsInvoices",
			"throughIdAlias" => "invoice_id",
			"throughRelIdAlias" => "booking_id",
			"relationModel" => "Models\V1\Booking\Bookings"
		],
	);

	public function columnMap()
	{
		return array(
		'id' => 'id',
		'number' => 'number',
		'date' => 'date',
		'date_reminder_1' => 'date_reminder_1',
		'date_reminder_2' => 'date_reminder_2',
		'file' => 'file',
		'file_reminder_1' => 'file_reminder_1',
		'file_reminder_2' => 'file_reminder_2',
		'file_copy_insurance' => 'file_copy_insurance',
		'file_copy_archive' => 'file_copy_archive',
		'value' => 'value',
		'customer_name' => 'customer_name',
		'comment' => 'comment',
		'status' => 'status',
		'organization_id' => 'organization_id',
		'customer_id' => 'customer_id',
		'client_id' => 'client_id',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_invo_invoices");

		$this->belongsTo(
			"organization_id",
			"Models\V1\Org\Organization",
			"id",
			[
				"alias" => "organization",
			]
		);

		/*
		$this->belongsTo(
			"invoicerequest_id",
			"Models\V1\Invo\Invoicerequests",
			"id",
			[
				"alias" => "invoicerequest",
			]
		);
		*/

		$this->hasMany(
			"id",
			"Models\V1\Invo\InvoicesInvoicerequests",
			"invoice_id",
			[
				"alias" => "invoicesInvoicerequests",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Invo\InvoicesInvoicerequests",
			"invoice_id",
			"invoicerequest_id",
			"Models\V1\Invo\Invoicerequests",
			"id",
			[
				"alias" => "invoicerequests",
			]
		);

		$this->belongsTo(
			"customer_id",
			"Models\V1\Cust\Customers",
			"id",
			[
				"alias" => "customer",
			]
		);

		/*
		$this->hasMany(
			"id",
			"Models\V1\Booking\Bookings",
			"invoice_id",
			[
				"alias" => "bookings",
			]
		);
		*/

		$this->hasMany(
			"id",
			"Models\V1\Invo\Invoicepositions",
			"invoice_id",
			[
				"alias" => "invoicepositions",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Invo\PaymentsInvoices",
			"invoice_id",
			[
				"alias" => "paymentsInvoices",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Invo\PaymentsInvoices",
			"invoice_id",
			"payment_id",
			"Models\V1\Invo\Payments",
			"id",
			[
				"alias" => "payments",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Relations\BookingsInvoices",
			"invoice_id",
			[
				"alias" => "bookingsInvoices",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Relations\BookingsInvoices",
			"invoice_id",
			"booking_id",
			"Models\V1\Booking\Bookings",
			"id",
			[
				"alias" => "bookings",
			]
		);
	}

	/* -------------------------------------------------------------------------
	 * Action to set the status of a new invoice
	 */
	public function open()
	{
		$this->status = self::STATUS_OPEN;
		return $this->update();
	}

	/* -------------------------------------------------------------------------
	 * Action mark this invoice as cancelled
	 */
	public function cancel()
	{
		switch($this->status) {
			case self::STATUS_OPEN:
			case self::STATUS_REMINDER_1:
			case self::STATUS_REMINDER_2:
				$this->status = self::STATUS_CANCELLED;
				return $this->update();
			default:
				return false;
		}
	}

	/* -------------------------------------------------------------------------
	 * Action to mark this invoice as payed
	 */
	public function pay()
	{
		switch($this->status) {
			case self::STATUS_OPEN:
			case self::STATUS_REMINDER_1:
			case self::STATUS_REMINDER_2:
			case self::STATUS_DEPOSIT;
			case self::STATUS_DEPOSIT_REMINDER_1;
			case self::STATUS_DEPOSIT_REMINDER_2;
				$this->status = self::STATUS_PAYED;
				return $this->update();
			default:
				return false;
		}
	}

	/* -------------------------------------------------------------------------
	 * Action to mark this invoice as payed
	 */
	public function deposit()
	{
		switch($this->status) {
			case self::STATUS_OPEN:
				$this->status = self::STATUS_DEPOSIT;
				return $this->update();
			case self::STATUS_REMINDER_1:
				$this->status = self::STATUS_DEPOSIT_REMINDER_1;
				return $this->update();
			case self::STATUS_REMINDER_2:
				$this->status = self::STATUS_DEPOSIT_REMINDER_2;
				return $this->update();
			case self::STATUS_DEPOSIT;
			case self::STATUS_DEPOSIT_REMINDER_1;
			case self::STATUS_DEPOSIT_REMINDER_2;
				return true;
			default:
				return false;
		}
	}

	/* -------------------------------------------------------------------------
	 * Action to mark this invoice as reminded
	 */
	public function remind()
	{
		switch($this->status) {
			case self::STATUS_OPEN:
				$this->status = self::STATUS_REMINDER_1;
				return $this->update();
			case self::STATUS_REMINDER_1:
				$this->status = self::STATUS_REMINDER_2;
				return $this->update();
			case self::STATUS_DEPOSIT:
				$this->status = self::STATUS_DEPOSIT_REMINDER_1;
				return $this->update();
			case self::STATUS_DEPOSIT_REMINDER_1:
				$this->status = self::STATUS_DEPOSIT_REMINDER_2;
				return $this->update();
			default:
				return false;
		}
	}

}
