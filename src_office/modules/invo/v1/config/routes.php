<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$api = new RouterGroup(array('module' => 'api_v1_invo'));
$api->setPrefix('/api/v1/invo');

$api->addGet("/invoices",						array('controller' => 'Invoices',	'action' => 'index'));
$api->addGet("/invoices/all",					array('controller' => 'Invoices',	'action' => 'indexAll'));
$api->addGet("/invoices/:int",					array('controller' => 'Invoices',	'action' => 'one',		'id' => 1,));
// $api->addPut	("/invoices/:int",					array('controller' => 'Invoices',	'action' => 'update',	'id' => 1,));
// $api->addDelete	("/invoices/:int",				array('controller' => 'Invoices',	'action' => 'delete',	'id' => 1,));
$api->addGet("/invoices_lookup",				array('controller' => 'Invoices',	'action' => 'indexLookup'));
$api->addGet("/invoices/unpaid",				array('controller' => 'Invoices',	'action' => 'indexUnpaid'));
$api->addGet("/invoices/overdue",				array('controller' => 'Invoices',	'action' => 'indexOverdue'));
$api->addGet("/invoices/uncanceled",			array('controller' => 'Invoices',	'action' => 'indexUncanceled'));

// Action Routes
$api->addPut("/invoices/actions/resend/:int",	array('controller' => 'Invoices',	'action' => 'resend',	'id' => 1));
$api->addPut("/invoices/actions/resend",		array('controller' => 'Invoices',	'action' => 'resend'));
$api->addPut("/invoices/actions/storno/:int",	array('controller' => 'Invoices',	'action' => 'storno',	'id' => 1));
$api->addPut("/invoices/actions/storno",		array('controller' => 'Invoices',	'action' => 'storno'));
$api->addPut("/invoices/actions/pay/:int",		array('controller' => 'Invoices',	'action' => 'pay',		'id' => 1));

/* Not used by now:
$api->addGet("/invoices/thismonth",			array('controller' => 'Invoices',	'action' => 'indexThismonth'));
$api->addGet("/invoices/yearstats",			array('controller' => 'Invoices',	'action' => 'indexYearstats'));
$api->addGet("/invoices/yearstats/:int",	array('controller' => 'Invoices',	'action' => 'indexYearstatsYear', 'year' => 1,));
*/

// Invoiceposition routes
$api->addGet("/invoicepositions", 					array('controller' => 'Invoicepositions', 	'action' => 'index'));
$api->addGet("/invoicepositions/:int", 				array('controller' => 'Invoicepositions', 	'action' => 'one',		'id' => 1));
// $api->addPost	("/invoicepositions", array('controller' => 'Invoicepositions', 'action' => 'create'));
// $api->addPut	("/invoicepositions/:int", array('controller' => 'Invoicepositions', 'action' => 'update', 'id' => 1));
// $api->addDelete	("/invoicepositions/:int", array('controller' => 'Invoicepositions', 'action' => 'delete', 'id' => 1));

// Invoicerequest routes
$api->addGet("/invoicerequests",					array('controller' => 'Invoicerequests',	'action' => 'indexList'));
$api->addGet("/invoicerequests/:int",				array('controller' => 'Invoicerequests',	'action' => 'one',		'id'  => 1,));
$api->addPost("/invoicerequests",					array('controller' => 'Invoicerequests',	'action' => 'create',));
$api->addPost("/invoicerequests/preprint",			array('controller' => 'Invoicerequests',	'action' => 'createPreprint',));
$api->addPost("/invoicerequests/reminder",			array('controller' => 'Invoicerequests',	'action' => 'createReminder'));
$api->addPost("/invoicerequests/reminder/:int",		array('controller' => 'Invoicerequests',	'action' => 'createReminder',	'id' => 1));
$api->addGet("/invoicerequests_lookup",				array('controller' => 'Invoicerequests',	'action' => 'indexLookup'));
// $api->addPut("/invoicerequests/:int", array('controller' => 'Invoicerequests', 'action' => 'update', 'id' => 1,));
// $api->addDelete("/invoicerequests/:int",	array('controller' => 'Invoicerequests', 'action' => 'delete', 'id' => 1,));

/* ---------------------------------------------------------------------------------
 * pdf file download
 */
$api->addGet("/download/([a-zA-ZöäüÖÄÜ0-9_/\.\-]+)", array('controller'	=> 'Invo',		'action' => 'invoDownload',	'file' => 1));
$api->addGet("/download/pdf/([a-zA-ZöäüÖÄÜ0-9_/\.\-]+)", array('controller'	=> 'Invo',		'action' => 'invoPDFDownload',	'filename' => 1));

// Email index List
$api->addGet("/emails", array('controller'	=> 'Invo',	'action' => 'indexEmails'));

/*
$api->addGet	("/templates",				array('controller' => 'Templates',			'action' => 'index'));
$api->addGet	("/templates/:int",			array('controller' => 'Templates',			'action' => 'one',		'id' => 1));
$api->addPost	("/templates",				array('controller' => 'Templates',			'action' => 'create'));
$api->addPut	("/templates/:int",			array('controller' => 'Templates',			'action' => 'update',	'id' => 1));
$api->addDelete	("/templates/:int",			array('controller' => 'Templates',			'action' => 'delete',	'id' => 1));
*/

$router->mount($api);
