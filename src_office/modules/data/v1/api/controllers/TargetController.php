<?php


namespace Api\V1\Data\Controllers;


use Models\V1\Data\Targets;

class TargetController extends \GSCLibrary\BaseApiController {

    private $from = "Models\V1\Data\Targets";

    public function indexAllAction() {
        return $this->jsonReadRequest(function (&$transaction) {

            $targets = Targets::find();

            return $targets;
        });
    }

    public function oneAction($id) {
        $model = $this->from;
        $postLoad = function ($obj) {
            $model = $this->from;
            return $this->_valueConversionPostload($obj, $model::$attrs);
        };
        return $this->_oneActionRelations($model, $id, $model::$relations, $postLoad);
    }
}