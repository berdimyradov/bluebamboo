<?php


namespace Api\V1\Data\Controllers;


use Models\V1\Data\Fields;
use Models\V1\Data\Imports;
use Models\V1\Data\Records;
use Phalcon\Http\Response;
use Phalcon\Http\Response\Exception;
use PHPExcel_IOFactory;

class ImportController extends \GSCLibrary\BaseApiController {

    private $from = "Models\V1\Data\Imports";
    const FILES_FOLDER = "../bluebamboo3/doc/";

    public function indexAllAction() {
        return $this->jsonReadRequest(function (&$transaction) {

            $client_id = $this->session->get("auth-identity-client");
            $this->grantAccessForClient($client_id);

            $imports = Imports::find();
            $retArray = [];

            foreach ($imports as $import) {
                $importArray = $import->toArray();
                $importArray['target'] = $import->target;
                $retArray[] = $importArray;
            }

            return $retArray;
        });
    }

    public function oneAction($id) {
        return $this->jsonReadRequest(function (&$transaction) use ($id) {
            $transaction = $this->transaction;

            $model = $this->from;
            $modelTargets = "Models\V1\Data\Targets";
            $modelRecords = "Models\V1\Data\Records";
            $modelFields = "Models\V1\Data\Fields";

            $client_id = $this->grantAccessForClient();

            $importObj = $this->_oneActionObj($model, $id);
            $target = $importObj->target;
            $t = $this->_valueConversionPostload($target, $modelTargets::$attrs)->toArray();

            $records = $importObj->records;
            $recordsArray = [];
            foreach ($records as $record) {
                $recordsArray[] = $this->_valueConversionPostload($record, $modelRecords::$attrs);
            }

            $fields = $target->fields;
            $f_arr = [];
            foreach ($fields as $field) {
                $f_arr[] = $this->_valueConversionPostload($field, $modelFields::$attrs);
            }

            $t['fields'] = $f_arr;

            $ret = $this->_valueConversionPostload($importObj, $model::$attrs)->toArray();
            $ret['target'] = $t;
            $ret['records'] = $recordsArray;

            return $ret;
        });
    }

    /* -----------------------------------------------------------------------------------
     * update action
     */
    public function updateAction($id) {
        return $this->jsonWriteRequest(/**
         * @param $transaction
         * @return mixed
         * @throws \Exception
         */
            function (&$transaction) use ($id) {
                $transaction = $this->transaction;
                $client_id = $this->grantAccessForClient();

                $model = $this->from;
                $modelCustomers = "Models\V1\Cust\Customers";
                $modelRecords = "Models\V1\Data\Records";

                // save the mapping for 'gsc_data_imports'
                $data = $this->request->getJsonRawBody();
                $importObj = $this->_updateActionObj($data, $model, $model::$attrs, $id);

                // CHECK DUPLICATION START
                $customers = $modelCustomers::findByClient_id($client_id);
                if ($customers && count($customers) !== 0) {
                    // Init fields
                    $fields = Fields::find()->toArray();
                    $firstnameField = array_shift(array_filter($fields, function ($field) {
                        return $field['code'] === 'firstname';
                    }));
                    $lastnameField = array_shift(array_filter($fields, function ($field) {
                        return $field['code'] === 'name';
                    }));
                    $emailField = array_shift(array_filter($fields, function ($field) {
                        return $field['code'] === 'email';
                    }));
                    $emailSecondField = array_shift(array_filter($fields, function ($field) {
                        return $field['code'] === 'email_second';
                    }));
                    $streetField = array_shift(array_filter($fields, function ($field) {
                        return $field['code'] === 'street';
                    }));
                    $zipField = array_shift(array_filter($fields, function ($field) {
                        return $field['code'] === 'zip';
                    }));

                    //Retrieve mapped import.col** that are mapped
                    foreach (Records::COLUMN_KEYS as $key) {
                        $colKey = $key . '_target_field';
                        switch ($importObj->$colKey) {
                            case $firstnameField['id']:
                                $firstnameFieldIndex = $key;
                                $firstnameFieldIndexImported = $key . '_imported';
                                break;
                            case $lastnameField['id']:
                                $lastnameFieldIndex = $key;
                                $lastnameFieldIndexImported = $key . '_imported';
                                break;
                            case $emailField['id']:
                                $emailFieldIndex = $key;
                                $emailFieldIndexImported = $key . '_imported';
                                break;
                            case $emailSecondField['id']:
                                $emailSecondFieldIndex = $key;
                                $emailSecondFieldIndexImported = $key . '_imported';
                                break;
                            case $streetField['id']:
                                $streetFieldIndex = $key;
                                $streetFieldIndexImported = $key . '_imported';
                                break;
                            case $zipField['id']:
                                $zipFieldIndex = $key;
                                $zipFieldIndexImported = $key . '_imported';
                                break;
                            default:
                                continue;
                        }
                    }

                    $records = $importObj->records;

                    foreach ($records as $record) {
                        $recordFirstName = trim(strtolower($record->$firstnameFieldIndexImported ?? $record->$firstnameFieldIndex));
                        $recordLastName = trim(strtolower($record->$lastnameFieldIndexImported ?? $record->$lastnameFieldIndex));

                        $recordEmail = trim(strtolower($record->$emailFieldIndexImported ?? $record->$emailFieldIndex));
                        $recordEmailSecond = trim(strtolower($record->$emailSecondFieldIndexImported ?? $record->$emailSecondFieldIndex));

                        $recordStreet = trim(strtolower($record->$streetFieldIndexImported ?? $record->$streetFieldIndex));
                        $recordZip = trim(strtolower($record->$zipFieldIndexImported ?? $record->$zipFieldIndex));

                        $isDuplicated = false;

                        foreach ($customers as $customer) {
                            //check if duplicated
                            $customerFirstName = trim(strtolower($customer->firstname));
                            $customerLastName = trim(strtolower($customer->name));
                            $customerEmail = trim(strtolower($customer->email));
                            $customerEmailSecond = trim(strtolower($customer->email_second));
                            $customerStreet = trim(strtolower($customer->street));
                            $customerZip = trim(strtolower($customer->zip));

                            if ($recordFirstName === $customerFirstName && $recordLastName === $customerLastName) {
                                $isDuplicated = true;

                                if ($recordEmail || $recordEmailSecond) {
                                    if (($recordEmail !== $customerEmail && $recordEmail !== $customerEmailSecond) &&
                                        ($recordEmailSecond !== $customerEmail && $recordEmailSecond !== $customerEmailSecond)) {
                                        $isDuplicated = false;
                                        break;
                                    }
                                }

                                if (($recordStreet && $customerStreet) && $recordStreet !== $customerStreet) {
                                    $isDuplicated = false;
                                    break;
                                }

                                if (($recordZip && $customerZip) && $recordZip !== $customerZip) {
                                    $isDuplicated = false;
                                    break;
                                }

                                if ($isDuplicated) {
                                    break;
                                }
                            }
                        }

                        $record->duplication = $isDuplicated ? 1 : 0;
                        $this->_updateActionObj($record, $modelRecords, $modelRecords::$attrs, $record->id);
                    }
                }

                $transaction->commit();

                return $importObj;
            });
    }

    public function uploadAction() {
        return $this->jsonWriteRequest(function (&$transaction) {
            try {
                $transaction = $this->transaction;

                $modelRecords = "Models\V1\Data\Records";

                $import = null;
                $client_id = $this->grantAccessForClient();

                if (true == $this->request->hasFiles() && $this->request->isPost()) {

                    $file = $this->request->getUploadedFiles()[0];

                    $this->getFileSwitch()->phalconFileMoveTo($file, self::FILES_FOLDER . $file->getName());

                    $absFilePath = $this->getFileSwitch()->getFile(self::FILES_FOLDER . $file->getName());

                    if (!$absFilePath) {
                        return false;
                    }

                    $extractedData = $this->extractExcelOrCsvFile($absFilePath);

                    $import = new Imports();
                    $import->filename = $file->getName();
                    $import->target_id = 1;
                    $import->client_id = $client_id;
                    $import->created_at = date("Y-m-d H:i:s");

//                    @todo cant use createActionObj because of transaction ??
//                    $this->_createActionObj($import, $this->from, $this->from::$attrs);
                    $import->save();

                    foreach ($extractedData as $datum) {
                        $record = new Records();
                        $record->client_id = $client_id;
                        $record->import_id = $import->id;
                        $record->import = 0;
                        $record->imported = 0;
                        $record->duplication = 0;

                        foreach (Records::COLUMN_KEYS as $index => $key) {
                            $record->$key = $datum[$index + 1];
                        }

                        $this->_createActionObj($record, $modelRecords, $modelRecords::$attrs);
                    }

                    $this->getFileSwitch()->deleteFile($absFilePath);
                }

                $transaction->commit();

                return $import;
            } catch (Exception $exception) {
                $transaction->rollback();
                throw $exception;
            }
        });
    }

    public function storeRecordDataAction() {
        $body = json_decode($this->request->getRawBody());

        $import = Imports::findFirst("id = " . $body->import_id);
        $filePath = $this->getFileSwitch()->getFile(self::FILES_FOLDER . $import->filename);

        $recordRows = $this->extractExcelOrCsvFile($filePath);

        foreach ($recordRows as $recordRow) {
            $record = new Records();
            $record->client_id = 1;
            $record->import_id = intval($import->id);
            $record->import = 1;
            $record->imported = 1;

            for ($i = 1; $i < 21; $i++) {
                $colIndex = "col" . sprintf("%02d", $i);
                $importedIndex = $colIndex . "_imported";
                $validIndex = $colIndex . "_valid";

                $record->$colIndex = strval($recordRow[$i]);
                $record->$importedIndex = "";
                $record->$validIndex = 1;

//                $this->checkIfColIsValid($value, $validityRule);
            }

            if ($record->imported) {
                $isSaved = $record->save();

                if (!$isSaved) {
                    throw new Exception("Could not save the records data to the database! Try again! <br>" . $record->getMessages()[0] . "<br>", 500);
                }
            } else {
                continue;
            }
        }

        $response = new Response();
        return $response->appendContent(json_encode($body));
    }

    private function extractExcelOrCsvFile(String $inputFileName) {
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            throw $e;
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = ($sheet->getHighestRow() > 21) ? 21 : $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $rows = [];
        //  Loop through each row of the worksheet in turn
        for ($row = 0; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE)[0];

            if (empty($rowData) || !is_numeric($rowData[0])) {
                continue;
            }

            $rows[] = $rowData;
        }

        return $rows;
    }
}