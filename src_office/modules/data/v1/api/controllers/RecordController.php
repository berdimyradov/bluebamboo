<?php

namespace Api\V1\Data\Controllers;

use Models\V1\Data\Records;
use Phalcon\Http\Response\Exception;

class RecordController extends \GSCLibrary\BaseApiController {

    private $from = "Models\V1\Data\Records";

    public function indexAllAction() {
        // site id or similar filter? just about access rights
        $default_projection = array("o.*");
        $orderby = array("o.id");
        $attrs_for_projection = array("o.*");
        $attrs_for_where = array("o.*");
        $attrs_for_orderby = array("o.*");

        $postLoad = function ($obj) {
            $model = $this->from;
            return $this->_valueConversionPostload($obj, $model::$attrs);
        };

        return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
    }

    public function saveAction() {
        return $this->jsonWriteRequest(function (&$transaction) {
            $body = json_decode($this->request->getRawBody());

            if (is_array($body)) {
                foreach ($body as $recordArray) {
                    $record = Records::findFirst("id = " . $recordArray->id);
                    $this->saveRecord($record, $recordArray);
                }
            } else if (is_object($body)) {
                $record = Records::findFirst("id = " . $body->id);
                $this->saveRecord($record, $body);
            }

            return true;
        });
    }

    public function deleteAction($id) {
        $model = $this->from;
        return $this->_deleteAction($model, $id);
    }

    public function importRecordsToCustomersAction() {
        return $this->jsonWriteRequest(function (&$transaction) {
            $transaction = $this->transaction;
            $client_id = $this->grantAccessForClient();

            $model = $this->from;
            $modelCustomers = "Models\V1\Cust\Customers";
            $modelImports = "Models\V1\Data\Imports";
            $modelFields = "Models\V1\Data\Fields";

            $inputData = $this->request->getJsonRawBody();
            $importObj = $this->_oneActionObj($modelImports, $inputData->import_id);
            $mappedFields = [];
            $importedCustomers = [];

            foreach (Records::COLUMN_KEYS as $key) {
                $colKey = $key . '_target_field';
                if ($importObj->$colKey) {
                    $mappedFields[$key] = $this->_oneActionObj($modelFields, $importObj->$colKey);
                }
            }

            foreach ($inputData->record_ids as $id) {
                $record = $this->_oneActionObj($model, $id);

                // for each record to be imported, create Customer obj and save it
                $custObj = new \stdClass();
                foreach($mappedFields as $key => $mappedField) {
                    if ($mappedField->code === "birth_date") {
                        $time = strtotime($record->$key);
                        $custObj->birth_day = date('d', $time);
                        $custObj->birth_month = date('m', $time);
                        $custObj->birth_year = date('Y', $time);
                    } else {
                        $attribute_name = $mappedField->code;
                        $custObj->$attribute_name = $record->$key;
                    }
                }
                $custObj->nr = $modelCustomers::createNewCustNr();

                $importedCustomers[] = $this->_createActionObj($custObj, $modelCustomers, $modelCustomers::$attrs);

                $record->import = 0;
                $record->imported = 1;
                $record->duplication = 1;

                $this->_updateActionObj($record, $this->from, $this->from::$attrs, $record->id);
            }

            $transaction->commit();

            return $importedCustomers;
        });
    }

    private function saveRecord(Records $record, $newRecordArray): bool {
        if (!$record) {
            throw new Exception("Could not find entry in order to map fields! Try again!", 500);
        }

        foreach ($newRecordArray as $key => $value) {
            $record->$key = $value;
        }

        $isSaved = $record->save();

        if (!$isSaved) {
            throw new Exception("Could not save the imported data to the database! Try again!", 500);
        }

        return $isSaved;
    }
}