<?php

namespace Api\V1\Data;

use Phalcon\Loader,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\View,
    Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface {
    public function registerAutoloaders(\Phalcon\DiInterface $dependencyInjector = null) {
        $loader = new Loader();
        $loader->registerNamespaces(
            array(
                'GSCLibrary' => '../library/',

                'Models\V1\Data' => '../modules/data/v1/models/',
                'Models\V1\Cust' => '../modules/cust/v1/models/',

                'Api\V1\Data\Logic' => '../modules/data/v1/api/logic/',
                'Api\V1\Data\Controllers' => '../modules/data/v1/api/controllers/',


            )
        );
        $loader->register();
    }

    public function registerServices(\Phalcon\DiInterface $di) {
        $di->set('dispatcher', function () {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace("Api\V1\Data\Controllers");
            return $dispatcher;
        });
        /*
        */
        $di->set('view', function () {
            $view = new View();
            $view->setViewsDir('../app/api/v1/data/views/');
            return $view;
        });
    }
}
