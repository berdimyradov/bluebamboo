<?php


namespace Models\V1\Data;


class Records extends \Phalcon\Mvc\Model {
    const COLUMN_KEYS = [
        'col01',
        'col02',
        'col03',
        'col04',
        'col05',
        'col06',
        'col07',
        'col08',
        'col09',
        'col10',
        'col11',
        'col12',
        'col13',
        'col14',
        'col15',
        'col16',
        'col17',
        'col18',
        'col19',
        'col20',
    ];

    public $id;
    public $import_id;
    public $client_id;
    public $import;
    public $imported;
    public $duplication;
    public $col01;
    public $col01_imported;
    public $col02;
    public $col02_imported;
    public $col03;
    public $col03_imported;
    public $col04;
    public $col04_imported;
    public $col05;
    public $col05_imported;
    public $col06;
    public $col06_imported;
    public $col07;
    public $col07_imported;
    public $col08;
    public $col08_imported;
    public $col09;
    public $col09_imported;
    public $col10;
    public $col10_imported;
    public $col11;
    public $col11_imported;
    public $col12;
    public $col12_imported;
    public $col13;
    public $col13_imported;
    public $col14;
    public $col14_imported;
    public $col15;
    public $col15_imported;
    public $col16;
    public $col16_imported;
    public $col17;
    public $col17_imported;
    public $col18;
    public $col18_imported;
    public $col19;
    public $col19_imported;
    public $col20;
    public $col20_imported;
    public $col01_valid;
    public $col02_valid;
    public $col03_valid;
    public $col04_valid;
    public $col05_valid;
    public $col06_valid;
    public $col07_valid;
    public $col08_valid;
    public $col09_valid;
    public $col10_valid;
    public $col11_valid;
    public $col12_valid;
    public $col13_valid;
    public $col14_valid;
    public $col15_valid;
    public $col16_valid;
    public $col17_valid;
    public $col18_valid;
    public $col19_valid;
    public $col20_valid;

    public static $attrs = array(
        "id" => "int",
        "import_id" => "int",
        "client_id" => "int",
        "import" => "boolean",
        "imported" => "boolean",
        "duplication" => "boolean",
        "col01" => "string",
        "col01_imported" => "string",
        "col02" => "string",
        "col02_imported" => "string",
        "col03" => "string",
        "col03_imported" => "string",
        "col04" => "string",
        "col04_imported" => "string",
        "col05" => "string",
        "col05_imported" => "string",
        "col06" => "string",
        "col06_imported" => "string",
        "col07" => "string",
        "col07_imported" => "string",
        "col08" => "string",
        "col08_imported" => "string",
        "col09" => "string",
        "col09_imported" => "string",
        "col10" => "string",
        "col10_imported" => "string",
        "col11" => "string",
        "col11_imported" => "string",
        "col12" => "string",
        "col12_imported" => "string",
        "col13" => "string",
        "col13_imported" => "string",
        "col14" => "string",
        "col14_imported" => "string",
        "col15" => "string",
        "col15_imported" => "string",
        "col16" => "string",
        "col16_imported" => "string",
        "col17" => "string",
        "col17_imported" => "string",
        "col18" => "string",
        "col18_imported" => "string",
        "col19" => "string",
        "col19_imported" => "string",
        "col20" => "string",
        "col20_imported" => "string",
        "col01_valid" => "boolean",
        "col02_valid" => "boolean",
        "col03_valid" => "boolean",
        "col04_valid" => "boolean",
        "col05_valid" => "boolean",
        "col06_valid" => "boolean",
        "col07_valid" => "boolean",
        "col08_valid" => "boolean",
        "col09_valid" => "boolean",
        "col10_valid" => "boolean",
        "col11_valid" => "boolean",
        "col12_valid" => "boolean",
        "col13_valid" => "boolean",
        "col14_valid" => "boolean",
        "col15_valid" => "boolean",
        "col16_valid" => "boolean",
        "col17_valid" => "boolean",
        "col18_valid" => "boolean",
        "col19_valid" => "boolean",
        "col20_valid" => "boolean",
    );

    public static $relations = array(
        [
            "type" => "belongsTo",
            "alias" => "imports",
            "relationModel" => "Models\V1\Data\Imports",
            "relationIdAlias" => "id"
        ],
    );

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id' => 'id',
            "import_id" => "import_id",
            "client_id" => "client_id",
            "import" => "import",
            "imported" => "imported",
            "duplication" => "duplication",
            "col01" => "col01",
            "col01_imported" => "col01_imported",
            "col02" => "col02",
            "col02_imported" => "col02_imported",
            "col03" => "col03",
            "col03_imported" => "col03_imported",
            "col04" => "col04",
            "col04_imported" => "col04_imported",
            "col05" => "col05",
            "col05_imported" => "col05_imported",
            "col06" => "col06",
            "col06_imported" => "col06_imported",
            "col07" => "col07",
            "col07_imported" => "col07_imported",
            "col08" => "col08",
            "col08_imported" => "col08_imported",
            "col09" => "col09",
            "col09_imported" => "col09_imported",
            "col10" => "col10",
            "col10_imported" => "col10_imported",
            "col11" => "col11",
            "col11_imported" => "col11_imported",
            "col12" => "col12",
            "col12_imported" => "col12_imported",
            "col13" => "col13",
            "col13_imported" => "col13_imported",
            "col14" => "col14",
            "col14_imported" => "col14_imported",
            "col15" => "col15",
            "col15_imported" => "col15_imported",
            "col16" => "col16",
            "col16_imported" => "col16_imported",
            "col17" => "col17",
            "col17_imported" => "col17_imported",
            "col18" => "col18",
            "col18_imported" => "col18_imported",
            "col19" => "col19",
            "col19_imported" => "col19_imported",
            "col20" => "col20",
            "col20_imported" => "col20_imported",
            "col01_valid" => "col01_valid",
            "col02_valid" => "col02_valid",
            "col03_valid" => "col03_valid",
            "col04_valid" => "col04_valid",
            "col05_valid" => "col05_valid",
            "col06_valid" => "col06_valid",
            "col07_valid" => "col07_valid",
            "col08_valid" => "col08_valid",
            "col09_valid" => "col09_valid",
            "col10_valid" => "col10_valid",
            "col11_valid" => "col11_valid",
            "col12_valid" => "col12_valid",
            "col13_valid" => "col13_valid",
            "col14_valid" => "col14_valid",
            "col15_valid" => "col15_valid",
            "col16_valid" => "col16_valid",
            "col17_valid" => "col17_valid",
            "col18_valid" => "col18_valid",
            "col19_valid" => "col19_valid",
            "col20_valid" => "col20_valid",
        );
    }

    /**
     * Initializes relationships in the model
     */
    public function initialize() {
        $this->setSource("gsc_data_records");

        $this->belongsTo(
            "import_id",
            "Models\V1\Data\Imports",
            "id",
            [
                "alias" => "import"
            ]
        );
    }

}