<?php


namespace Models\V1\Data;


class Imports extends \Phalcon\Mvc\Model {
    public $id;
    public $client_id;
    public $target_id;
    public $filename;
    public $col01_target_field;
    public $col02_target_field;
    public $col03_target_field;
    public $col04_target_field;
    public $col05_target_field;
    public $col06_target_field;
    public $col07_target_field;
    public $col08_target_field;
    public $col09_target_field;
    public $col10_target_field;
    public $col11_target_field;
    public $col12_target_field;
    public $col13_target_field;
    public $col14_target_field;
    public $col15_target_field;
    public $col16_target_field;
    public $col17_target_field;
    public $col18_target_field;
    public $col19_target_field;
    public $col20_target_field;
    public $created_at;

    public static $attrs = array(
        "id" => "int",
        "client_id" => "int",
        "target_id" => "int",
        "filename" => "string",
        "col01_target_field" => "int",
        "col02_target_field" => "int",
        "col03_target_field" => "int",
        "col04_target_field" => "int",
        "col05_target_field" => "int",
        "col06_target_field" => "int",
        "col07_target_field" => "int",
        "col08_target_field" => "int",
        "col09_target_field" => "int",
        "col10_target_field" => "int",
        "col11_target_field" => "int",
        "col12_target_field" => "int",
        "col13_target_field" => "int",
        "col14_target_field" => "int",
        "col15_target_field" => "int",
        "col16_target_field" => "int",
        "col17_target_field" => "int",
        "col18_target_field" => "int",
        "col19_target_field" => "int",
        "col20_target_field" => "int",
        "created_at" => "date",
    );

    public static $relations = array(
        "records" => [
          "type" => "hasMany",
          "alias" => "records",
          "relationModel" => "Models\V1\Data\Records",
          "relationIdAlias" => "id"
        ],
        "target" => [
            "type" => "belongsTo",
            "alias" => "target",
            "relationModel" => "Models\V1\Data\Targets",
            "relationIdAlias" => "id"
        ],
        "field" => [
            "type" => "belongsTo",
            "alias" => "field",
            "relationModel" => "Models\V1\Data\Fields",
            "relationIdAlias" => "id"
        ]
    );

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            "id" => "id",
            "client_id" => "client_id",
            "target_id" => "target_id",
            "filename" => "filename",
            "col01_target_field" => "col01_target_field",
            "col02_target_field" => "col02_target_field",
            "col03_target_field" => "col03_target_field",
            "col04_target_field" => "col04_target_field",
            "col05_target_field" => "col05_target_field",
            "col06_target_field" => "col06_target_field",
            "col07_target_field" => "col07_target_field",
            "col08_target_field" => "col08_target_field",
            "col09_target_field" => "col09_target_field",
            "col10_target_field" => "col10_target_field",
            "col11_target_field" => "col11_target_field",
            "col12_target_field" => "col12_target_field",
            "col13_target_field" => "col13_target_field",
            "col14_target_field" => "col14_target_field",
            "col15_target_field" => "col15_target_field",
            "col16_target_field" => "col16_target_field",
            "col17_target_field" => "col17_target_field",
            "col18_target_field" => "col18_target_field",
            "col19_target_field" => "col19_target_field",
            "col20_target_field" => "col20_target_field",
            "created_at" => "created_at",
        );
    }

    /**
     * Initializes relationships in the model
     */
    public function initialize() {
        $this->setSource("gsc_data_imports");

        $this->hasMany(
            "id",
            "Models\V1\Data\Records",
            "import_id",
            [
                "alias" => "records",
            ]
        );

        $this->belongsTo(
            "target_id",
            "Models\V1\Data\Targets",
            "id",
            [
                "alias" => "target"
            ]
        );

        $this->belongsTo(
            "col01_target_field",
            "Models\V1\Data\Fields",
            "id",
            [
                "alias" => "field"
            ]
        );

        for ($i = 2; $i < 21; $i++) {
            $this->belongsTo(
                "col" . str_pad($i, 2, "0", STR_PAD_LEFT). "_target_field",
                "Models\V1\Data\Fields",
                "id",
                [
                    "alias" => "field"
                ]
            );
        }
    }
}