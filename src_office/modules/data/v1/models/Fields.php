<?php


namespace Models\V1\Data;


class Fields extends \Phalcon\Mvc\Model {
    public $id;
    public $label;
    public $code;
    public $type;
    public $target_id;

    public static $attrs = array(
        "id" => "int",
        "label" => "string",
        "code" => "string",
        "type" => "string",
        "target_id" => "int",
    );

    public static $relations = array(
        [
            "type" => "belongsTo",
            "alias" => "targets",
            "relationModel" => "Models\V1\Data\Targets",
            "relationIdAlias" => "id"
        ],
    );

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id' => 'id',
            'label' => 'label',
            'code' => 'code',
            'type' => 'type',
            'target_id' => 'target_id',
        );
    }

    /**
     * Initializes relationships in the model
     */
    public function initialize() {
        $this->setSource("gsc_data_fields");

        $this->belongsTo(
            "target_id",
            "Models\V1\Data\Targets",
            "id",
            [
                "alias" => "target"
            ]
        );
    }
}