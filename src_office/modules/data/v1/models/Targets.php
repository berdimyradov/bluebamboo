<?php


namespace Models\V1\Data;


class Targets extends \Phalcon\Mvc\Model {
    public $id;
    public $label;
    public $code;

    public static $attrs = array(
        "id" => "int",
        "label" => "string",
        "code" => "string",
    );

    public static $relations = array(
        'fields' => [
            "type" => "hasMany",
            "alias" => "fields",
            "relationModel" => "Models\V1\Data\Fields",
            "relationIdAlias" => "target_id"
        ],
        'imports' => [
            "type" => "hasMany",
            "alias" => "imports",
            "relationModel" => "Models\V1\Data\Imports",
            "relationIdAlias" => "target_id"
        ],
    );

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id' => 'id',
            'label' => 'label',
            'code' => 'code',
        );
    }

    /**
     * Initializes relationships in the model
     */
    public function initialize() {
        $this->setSource("gsc_data_targets");

        $this->hasMany(
            "id",
            "Models\V1\Data\Fields",
            "target_id",
            [
                "alias" => "fields",
            ]
        );

        $this->hasMany(
            "id",
            "Models\V1\Data\Imports",
            "target_id",
            [
                "alias" => "imports",
            ]
        );
    }
}