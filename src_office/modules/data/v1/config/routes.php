<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$api = new RouterGroup(array('module' => 'api_v1_data'));
$api->setPrefix('/api/v1/data');

//Targets routes
$api->addGet("/targets/all", array('controller' => 'Target', 'action' => 'indexAll'));
$api->addGet("/targets/:int", array('controller' => 'Target', 'action' => 'one', 'id' => 1));

//Imports routes
$api->addGet("/imports/all", array('controller' => 'Import', 'action' => 'indexAll'));
$api->addGet("/imports/:int", array('controller' => 'Import', 'action' => 'one', 'id' => 1));
$api->addPost("/upload", array('controller' => 'Import', 'action' => 'upload'));
$api->addPut("/imports/:int", array('controller' => 'Import', 'action' => 'update', 'id' => 1));

// Records routes
$api->addGet("/records/all", array('controller' => 'Record', 'action' => 'indexAll'));
$api->addPost("/record/store", array('controller' => 'Import', 'action' => 'storeRecordData'));
$api->addDelete	("/records/:int", array('controller' => 'Record', 'action' => 'delete', 'id' => 1));
$api->addPut("/records/save", array('controller' => 'Record', 'action' => 'save'));
$api->addPost("/records/import", array('controller' => 'Record', 'action' => 'importRecordsToCustomers'));

$router->mount($api);
