<?php

/**
 * Model for app "romi", class "Invoices"
 */

namespace Models\V1\Relations;

class LocationsEmployees extends \Phalcon\Mvc\Model
{

	/**
	 *
	 * @var integer
	 */
	public $id;

	public $location_id;

	public $employee_id;

	public static $attrs = array(
		"int" => "int",
		"location_id" => "int",
		"employee_id" => "int"
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
		'id' => 'id',
		'location_id' => 'location_id',
		'employee_id' => 'employee_id',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_relations_locations_employees");

		$this->belongsTo(
			"location_id",
			"Models\V1\Loc\Locations",
			"id",
			[
				"alias" => "location",
			]
		);

		$this->belongsTo(
			"employee_id",
			"Models\V1\Org\Employees",
			"id",
			[
				"alias" => "employee",
			]
		);
	}

}
