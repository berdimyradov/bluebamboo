<?php

namespace Models\V1\Relations;

class GroupsEmployees extends \Phalcon\Mvc\Model
{

	public $id;

	public $employee_id;

	public $group_id;

	public static $attrs = array(
		"id" => "int",
		"employee_id" => "int",
		"group_id" => "int"
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
		'id' => 'id',
		'employee_id' => 'employee_id',
		'group_id' => 'group_id',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_relations_groups_employees");

		$this->belongsTo(
			"group_id",
			"Models\V1\Cust\Groups",
			"id",
			[
				"alias" => "group",
			]
		);

		$this->belongsTo(
			"employee_id",
			"Models\V1\Org\Employees",
			"id",
			[
				"alias" => "employee",
			]
		);
	}

}
