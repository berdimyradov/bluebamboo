<?php

/**
 * Model for app "romi", class "Invoices"
 */

namespace Models\V1\Relations;

class RoomsProducts extends \Phalcon\Mvc\Model
{

	/**
	 *
	 * @var integer
	 */
	public $id;

	public $room_id;

	public $product_id;

	public static $attrs = array(
		"id" => "int",
		"room_id" => "int",
		"product_id" => "int"
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
		'id' => 'id',
		'room_id' => 'room_id',
		'product_id' => 'product_id',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_relations_rooms_products");

		$this->belongsTo(
			"room_id",
			"Models\V1\Loc\Rooms",
			"id",
			[
				"alias" => "room",
			]
		);

		$this->belongsTo(
			"product_id",
			"Models\V1\Prod\Products",
			"id",
			[
				"alias" => "product",
			]
		);
	}

}
