<?php

/**
 * Model for app "romi", class "Invoices"
 */

namespace Models\V1\Relations;

class EmployeesProducts extends \Phalcon\Mvc\Model
{

	/**
	 *
	 * @var integer
	 */
	public $id;

	public $product_id;

	public $employee_id;

	public static $attrs = array(
		"id" => "int",
		"product_id" => "int",
		"employee_id" => "int"
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
		'id' => 'id',
		'product_id' => 'product_id',
		'employee_id' => 'employee_id',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_relations_employees_products");

		$this->belongsTo(
			"product_id",
			"Models\V1\Prod\Products",
			"id",
			[
				"alias" => "product",
			]
		);

		$this->belongsTo(
			"employee_id",
			"Models\V1\Org\Employees",
			"id",
			[
				"alias" => "employee",
			]
		);
	}

}
