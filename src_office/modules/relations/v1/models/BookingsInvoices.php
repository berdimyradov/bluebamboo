<?php

/**
 * Model for app "romi", class "Invoices"
 */

namespace Models\V1\Relations;

class BookingsInvoices extends \Phalcon\Mvc\Model
{

	/**
	 *
	 * @var integer
	 */
	public $id;

	public $booking_id;

	public $invoice_id;

	public static $attrs = array(
		"id" => "int",
		"booking_id" => "int",
		"invoice_id" => "int"
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
		'id' => 'id',
		'booking_id' => 'booking_id',
		'invoice_id' => 'invoice_id',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_relations_bookings_invoices");

		$this->belongsTo(
			"booking_id",
			"Models\V1\Booking\Bookings",
			"id",
			[
				"alias" => "booking",
			]
		);

		$this->belongsTo(
			"invoice_id",
			"Models\V1\Invo\Invoices",
			"id",
			[
				"alias" => "invoice",
			]
		);
	}

}
