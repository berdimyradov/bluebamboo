<?php

/**
 * Model for app "romi", class "Invoices"
 */

namespace Models\V1\Relations;

class GroupsCustomers extends \Phalcon\Mvc\Model
{

	public $id;

	public $customer_id;

	public $group_id;

	public static $attrs = array(
		"id" => "int",
		"customer_id" => "int",
		"group_id" => "int"
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
		'id' => 'id',
		'customer_id' => 'customer_id',
		'group_id' => 'group_id',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_relations_groups_customers");

		$this->belongsTo(
			"group_id",
			"Models\V1\Cust\Groups",
			"id",
			[
				"alias" => "group",
			]
		);

		$this->belongsTo(
			"customer_id",
			"Models\V1\Cust\Customers",
			"id",
			[
				"alias" => "customer",
			]
		);
	}

}
