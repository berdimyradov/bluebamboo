<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$api = new RouterGroup(array('module' => 'api_v1_relations'));
$api->setPrefix('/api/v1/relations');

/* Routes for Relations not needed ?! Everything is handled "internally" */

$api->addGet	("/locations_employees", array('controller' => 'LocationsEmployees', 'action' => 'index'));
$api->addGet	("/locations_employees/:int", array('controller' => 'LocationsEmployees', 'action' => 'one', 'id' => 1));
//$api->addPost	("/locations_employees", array('controller' => 'LocationsEmployees', 'action' => 'create'));
//$api->addPut	("/locations_employees/:int", array('controller' => 'LocationsEmployees', 'action' => 'update', 'id' => 1));
//$api->addDelete	("/locations_employees/:int", array('controller' => 'LocationsEmployees', 'action' => 'delete', 'id' => 1));

$api->addGet	("/locations_products", array('controller' => 'LocationsProducts', 'action' => 'index'));
$api->addGet	("/locations_products/:int", array('controller' => 'LocationsProducts', 'action' => 'one', 'id' => 1));
//$api->addPost	("/locations_products", array('controller' => 'LocationsProducts', 'action' => 'create'));
//$api->addPut	("/locations_products/:int", array('controller' => 'LocationsProducts', 'action' => 'update', 'id' => 1));
//$api->addDelete	("/locations_products/:int", array('controller' => 'LocationsProducts', 'action' => 'delete', 'id' => 1));

$api->addGet	("/employees_products", array('controller' => 'EmployeesProducts', 'action' => 'index'));
$api->addGet	("/employees_products/:int", array('controller' => 'EmployeesProducts', 'action' => 'one', 'id' => 1));
//$api->addPost	("/employees_products", array('controller' => 'EmployeesProducts', 'action' => 'create'));
//$api->addPut	("/employees_products/:int", array('controller' => 'EmployeesProducts', 'action' => 'update', 'id' => 1));
//$api->addDelete	("/employees_products/:int", array('controller' => 'EmployeesProducts', 'action' => 'delete', 'id' => 1));

$api->addGet	("/groups_customers", array('controller' => 'GroupsCustomers', 'action' => 'index'));
$api->addGet	("/groups_customers/:int", array('controller' => 'GroupsCustomers', 'action' => 'one', 'id' => 1));
//$api->addPost	("/groups_customers", array('controller' => 'GroupsCustomers', 'action' => 'create'));
//$api->addPut	("/groups_customers/:int", array('controller' => 'GroupsCustomers', 'action' => 'update', 'id' => 1));
//$api->addDelete	("/groups_customers/:int", array('controller' => 'GroupsCustomers', 'action' => 'delete', 'id' => 1));

$api->addGet	("/groups_employees", array('controller' => 'GroupsEmployees', 'action' => 'index'));
$api->addGet	("/groups_employees/:int", array('controller' => 'GroupsEmployees', 'action' => 'one', 'id' => 1));
//$api->addPost	("/groups_employees", array('controller' => 'GroupsEmployees', 'action' => 'create'));
//$api->addPut	("/groups_employees/:int", array('controller' => 'GroupsEmployees', 'action' => 'update', 'id' => 1));
//$api->addDelete	("/groups_employees/:int", array('controller' => 'GroupsEmployees', 'action' => 'delete', 'id' => 1));

$router->mount($api);
