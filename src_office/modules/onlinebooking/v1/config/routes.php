<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$api = new RouterGroup(array('module' => 'api_v1_onlinebooking'));
$api->setPrefix('/api/v1/onlinebooking');

// Main - available from everywhere with crypted id in header:
// Products Actions
$api->addGet	("/products", array('controller' => 'OnlineBooking', 'action' => 'getProducts'));
$api->addGet	("/settings", array('controller' => 'OnlineBooking', 'action' => 'getSettings'));

// Actions for Free Days and Timeslots
$api->addGet	("/freedays/p/:int/l/:int/e/:int/([0-9]{4})/([0-9]{2})",
	array(
		'controller' => 'OnlineBooking',
		'action' => 'getFreeDays',
		'product' => 1,
		'location' => 2,
		'employee' => 3,
		'year' => 4,
		'month' => 5
	)
);
$api->addGet	("/freedays/p/:int/l/:int/([0-9]{4})/([0-9]{2})",
	array(
		'controller' => 'OnlineBooking',
		'action' => 'getFreeDaysAllEmployees',
		'product' => 1,
		'location' => 2,
		'year' => 3,
		'month' => 4,
	)
);
$api->addGet	("/freetimes/p/:int/l/:int/e/:int/([0-9]{4})/([0-9]{2})/([0-9]{2})",
	array(
		'controller' => 'OnlineBooking',
		'action' => 'getFreeTimes',
		'product' => 1,
		'location' => 2,
		'employee' => 3,
		'year' => 4,
		'month' => 5,
		'day' => 6,
	)
);
$api->addGet	("/freetimes/p/:int/l/:int/([0-9]{4})/([0-9]{2})/([0-9]{2})",
	array(
		'controller' => 'OnlineBooking',
		'action' => 'getFreeTimesAllEmployees',
		'product' => 1,
		'location' => 2,
		'year' => 3,
		'month' => 4,
		'day' => 5,
	)
);

// Locations Action
$api->addGet	("/locations/p/:int", array('controller' => 'OnlineBooking', 'action' => 'getLocationsByProduct', 'product' => 1));

// Employees Actions
// $api->addGet	("/employees/l/:int", array('controller' => 'OnlineBooking', 'action' => 'getEmployees', 'location' => 1));
// $api->addGet	("/employees/p/:int/l/:int", array('controller' => 'OnlineBooking', 'action' => 'getEmployees', 'product' => 1, 'location' => 2));

// Booking Action
$api->addPost	("/book", array('controller' => 'OnlineBooking', 'action' => 'createBooking'));

// Admin - available from within admin session
// Get Crypted Client ID
$api->addGet	("/cryptedid", array('controller' => 'OnlineBooking', 'action' => 'getClientCrypted'));
$api->addGet	("/cryptedid/:int", array('controller' => 'OnlineBooking', 'action' => 'getClientCrypted', 'id' => 1));

// Locations Actions
$api->addGet	("/locations", array('controller' => 'OnlineBooking', 'action' => 'getLocations'));
$api->addGet	("/locations/:int/weektimes", array('controller' => 'OnlineBooking', 'action' => 'getLocationWeektimes', 'id' => 1));
$api->addPost	("/locations/:int/weektimes", array('controller' => 'OnlineBooking', 'action' => 'setLocationWeektimes', 'id' => 1));
$api->addGet	("/locations/:int/monthtimes/([0-9]{4})/([0-9]{2})", array('controller' => 'OnlineBooking', 'action' => 'getLocationMonthtimes', 'id' => 1, 'year' => 2, 'month' => 3));
$api->addPost	("/locations/:int/monthtimes/([0-9]{4})/([0-9]{2})", array('controller' => 'OnlineBooking', 'action' => 'setLocationMonthtimes', 'id' => 1, 'year' => 2, 'month' => 3));

// Employees Actions
$api->addGet	("/employees", array('controller' => 'OnlineBooking', 'action' => 'getEmployees'));
$api->addGet	("/employees/:int/weektimes", array('controller' => 'OnlineBooking', 'action' => 'getEmployeeWeektimes', 'id' => 1));
$api->addPost	("/employees/:int/weektimes", array('controller' => 'OnlineBooking', 'action' => 'setEmployeeWeektimes', 'id' => 1));
$api->addGet	("/employees/:int/monthtimes/([0-9]{4})/([0-9]{2})", array('controller' => 'OnlineBooking', 'action' => 'getEmployeeMonthtimes', 'id' => 1, 'year' => 2, 'month' => 3));
$api->addPost	("/employees/:int/monthtimes/([0-9]{4})/([0-9]{2})", array('controller' => 'OnlineBooking', 'action' => 'setEmployeeMonthtimes', 'id' => 1, 'year' => 2, 'month' => 3));

// Onlinebookin Approval / Assignment ... Actions
$api->addGet	("/bookings", array('controller' => 'OnlineBooking', 'action' => 'index'));
$api->addGet	("/bookings/open", array('controller' => 'OnlineBooking', 'action' => 'indexOpenBookings'));
$api->addGet	("/bookings/:int", array('controller' => 'OnlineBooking', 'action' => 'one', 'id' => 1));
$api->addPut	("/bookings/:int/assign/:int", array('controller' => 'OnlineBooking', 'action' => 'assignCustomer', 'id' => 1, 'customer' => 2));
$api->addPut	("/bookings/:int/accept", array('controller' => 'OnlineBooking', 'action' => 'acceptBooking', 'id' => 1));
$api->addPut	("/bookings/:int/decline", array('controller' => 'OnlineBooking', 'action' => 'declineBooking', 'id' => 1));

// Open available route for Cancellation of an appointment
$api->addGet	("/bookings/cancelcheck", array('controller' => 'OnlineBooking', 'action' => 'cancelCheck'));
$api->addPut	("/bookings/cancel", array('controller' => 'OnlineBooking', 'action' => 'cancelBooking'));

// Index List of Emails sent by the System
$api->addGet	("/emails", array('controller' => 'OnlineBooking', 'action' => 'indexEmails'));

// Asynchronous Service for Reminder Mails and Automatic Approvement of Bookings
$api->addGet	("/service/remindermails", array('controller' => 'OnlineBooking', 'action' => 'reminderMailService'));
$api->addGet	("/service/autoapproval", array('controller' => 'OnlineBooking', 'action' => 'autoapprovalService'));

$router->mount($api);
