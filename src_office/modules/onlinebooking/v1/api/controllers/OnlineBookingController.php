<?php

/**
 * Controller class RoomsProductsController
 */

namespace Api\V1\OnlineBooking\Controllers;

class OnlineBookingController extends \GSCLibrary\BaseApiController {

	private $from = array(
		"Bookings"			=> "Models\V1\Booking\Bookings",
		"Bookingparts"		=> "Models\V1\Booking\Bookingparts",
		"Onlinebookings"	=> "Models\V1\Onlinebooking\Bookings",
		"Customers"			=> "Models\V1\Cust\Customers",
		"Employees"			=> "Models\V1\Org\Employees",
		"Products"			=> "Models\V1\Prod\Products",
		"Locations"			=> "Models\V1\Loc\Locations",
		"Organisation"		=> "Models\V1\Org\Organization",
		"Rooms"				=> "Models\V1\Loc\Rooms",
		"RoomsProducts" 	=> "Models\V1\Relations\RoomsProducts",
		"LocationsEmployees" => "Models\V1\Relations\LocationsEmployees",
		"Availabilities"	=> "Models\V1\Booking\Availabilities",
		"Settings"			=> "Models\V1\Settings\Onlinebooking",
	);

	private function encryptId($id)
	{
		return rtrim($this->crypt->encryptBase64((string)$id, $this->crypt->getKey(), true), "=");
	}

	private function decryptId($cryptedId)
	{
		return (int)$this->crypt->decryptBase64((string)$cryptedId, $this->crypt->getKey(), true);
	}

	private function getSettings($client_id = null)
	{
		if($client_id === null) {
			$client_id = $this->getClientID();
		}
		if(empty($client_id)) {
			throw new \Exception("No Client ID submitted", 400);
		}
		$model = $this->from['Settings'];
		$settings = $model::findFirstByClient_id($client_id);
		if(!$settings) {
			throw new \Exception("No Settings for Onlinebooking", 404);
		}
		return $this->_valueConversionPostload($settings, $model::$attrs);
		//return $model::getSettings($client_id);
	}

	/**
	 * Get the onlinebooking settings for the frontend
	 */
	public function getSettingsAction()
	{
		return $this->jsonReadRequest(function(&$transaction) {

			$client_id = $this->getClientID();
			if($client_id == 0) {
				throw new \Exception("No Client ID set", 400);
			}

			$settings = $this->getSettings($client_id);
			return array(
				"time_period" => $settings->time_period,
				"min_time_from_now" => $settings->min_time_from_now,
				"max_time_from_now" => $settings->max_time_from_now,
				"max_cancel_before" => $settings->max_cancel_before,
				"auto_confirm" => $settings->auto_confirm,
				"employee_notification" => $settings->employee_notification,
				"must_accept" => $settings->must_accept,
				"terms_of_use" => $settings->terms_of_use,
				"send_reminder_mail" => $settings->send_reminder_mail,
			);
		});
	}

	/**
	 * Get the crypted Client ID string
	 */
	public function getClientCryptedAction($id = null)
	{
		if($id === null) $client_id = $this->session->get("auth-identity-client");
		else $client_id = $id;
		$cryptedID = $this->encryptId($client_id);

		$ret = new \stdClass();
		$ret->crypted_id = $cryptedID;
		$ret->client_id = $client_id;

		return $this->_jsonResponse($ret);
	}

	private function getClientID($cryptedID = null)
	{
		if($cryptedID === null) {
			if (!empty($this->request->getHeader('X-Client-Id'))) {
				return $this->decryptId($this->request->getHeader('X-Client-Id'));
			} else if($this->session->has("auth-identity-client")) {
				return (int)$this->session->get("auth-identity-client");
			} else {
				return 0;
			}
		} else {
			return $this->decryptId($cryptedID);
		}
	}

	/**
	 * Index all Products
	 */
	public function getProductsAction()
	{
		return $this->jsonReadRequest(function(&$transaction) {

			$model = $this->from["Products"];

			$client_id = $this->getClientID();
			if(empty($client_id)) {
				throw new \Exception("No Client ID", 400);
			}

			$rows = $this->modelsManager->createBuilder()
				->columns('product.*')
				->from(['product' => $model])
				->where("product.client_id = :cid:", ["cid" => $client_id])
				->andWhere("product.online_booking_available = 1")
				->andWhere("product.active = 1")
				->orderBy('product.type, product.title')
				->getQuery()
				->execute();

			$retArr = array();
			foreach($rows as $row) {
				$retArr[] = $this->_valueConversionPostload($row, $model::$attrs)->toArray();
			}

			return $retArr;
		});
	}

	/**
	 *	Get all Locations and Employees for a specific product
	 */
	public function getLocationsByProductAction($product)
	{
		return $this->jsonReadRequest(function(&$transaction) use ($product) {

			$model = $this->from["Locations"];
			$modelProd = $this->from["Products"];
			$modelEmp = $this->from["Employees"];

			$client_id = $this->getClientID();
			if(empty($client_id)) {
				throw new \Exception("No Client ID", 400);
			}

			$prod = $this->modelsManager->createBuilder()
				->columns('product.*')
				->from(['product' => $modelProd])
				->where("product.client_id = :cid:", ["cid" => $client_id])
				->andWhere("product.id = :id:", ['id' => $product])
				->getQuery()
				->execute();

			$prodObj = $prod[0];
			$locations = $prodObj->getLocations();
			/*
			$locations = array();

			foreach($prodObj->rooms as $room) {
				if(!isset($room->location)) { continue; }
				$loc = $room->location;
				$found = false;
				foreach($locations as $l) {
					if($l->id == $loc->id) {
						$found = true;
						break;
					}
				}
				if(!$found) {
					$locations[] = $loc;
				}
			}
			*/

			$retArr = array();
			foreach($locations as $loca) {
				$location = $this->_valueConversionPostload($loca, $model::$attrs)->toArray();
				$emps = $loca->getActiveEmployees();
				$location["employees"] = array();
				foreach($emps as $e) {
					$products = $e->products;
					$can = false;
					if(count($products) > 0) {
						foreach($products as $prod) {
							if((int)$prod->id == (int)$product) {
								$can = true;
								break;
							}
						}
					}

					if($can) {
						$employee = $this->_valueConversionPostload($e, $modelEmp::$attrs);
						$location["employees"][] = $employee;
					}
				}
				$retArr[] = $location;
			}

			return $retArr;
		});
	}

	/**
	 * get the days with available Timeslots for a month with choosen product, location and optional employee
	 */
	public function getFreeDaysAllEmployeesAction($product, $location, $year, $month)
	{
		return $this->getFreeDaysAction($product, $location, 0, $year, $month);
	}

	public function getFreeDaysAction($product, $location, $employee, $year, $month)
	{
		return $this->jsonReadRequest(function(&$transaction) use ($year, $month, $product, $location, $employee) {

			$model = $this->from["Bookings"];
			$modelRooms = $this->from["Rooms"];
			$modelEmployees = $this->from["Employees"];
			$modelAvailabilities = $this->from["Availabilities"];
			$modelProducts = $this->from['Products'];
			$modelLocEmp = $this->from["LocationsEmployees"];

			$client_id = $this->getClientID();
			if($client_id == 0) {
				throw new \Exception("No Client ID", 400);
			}

			$settings = $this->getSettings($client_id);
			$spotLength = $settings->time_period;
			$period_before = $settings->min_time_from_now;
			$period_after = $settings->max_time_from_now;
			
			$ressources = [
				'rooms' => array(),
				'employees' => array(),
			];

			// Get needed Rooms and Employees for the Product
			$product = $modelProducts::findFirstById($product);
			if(!$product) {
				throw new \Exception("Service not found: ".$product, 404);
			}

			$rooms = $product->getRoomsByLocation($location);
			if(empty($rooms)) {
				throw new \Exception("No Rooms available for Product", 404);
			}
			foreach($rooms as $room) {
				$ressources['rooms'][] = $room->id;
			}

			if($employee == 0) {
				$prodemps = $product->getActiveEmployees();
				foreach($prodemps as $emp) {
					if($emp->worksAtLocation((int)$location)) $ressources['employees'][] = $emp->id;
				}
				if(count($ressources['employees']) === 0) {
					return array();
				}
			} else {
				$ressources['employees'][] = $employee;
			}

			$firstDay = 1;
			$startDate = $year . "-" . sprintf("%02d", $month) . "-" . sprintf("%02d", $firstDay);
			$date = new \DateTime($startDate);
			$endDate = $date->format("Y-m-t");
			$lastDay = (int)$date->format("t");

			// Employee Condition for Bookings and availabilities
			$emparr = "";
			foreach($ressources['employees'] as $emp_id) {
				$emparr .= $emp_id;
				$emparr .= ",";
			}
			$emparr = substr($emparr, 0, -1);

			$employeeCondition = "booking.employee_id IS NULL OR booking.employee_id IN (".$emparr.")";
			if($employee != 0) {
				$employeeCondition = "booking.employee_id = " . $employee;
			}

			// Room Condition for Bookings
			$roomarr = "";
			foreach($ressources['rooms'] as $room_id) {
				$roomarr .= $room_id;
				$roomarr .= ",";
			}
			$roomarr = substr($roomarr, 0, -1);
			$roomCondition = "booking.room_id IN (".$roomarr.")";

			$empRoomCondition = "((" . $roomCondition . ") OR (" . $employeeCondition . "))";

			// Create Array with Availabilities
			$spots = new \Api\V1\OnlineBooking\Logic\AvailSpotsHandler($startDate, $endDate, $spotLength, $ressources, $period_before, $period_after);

			// Get all Bookings for specified month
			$bookings = $this->modelsManager->createBuilder()
				->columns('booking.*')
				->from(['booking' => $model])
				->where("booking.client_id = :cid:", ["cid" => $client_id])
				->betweenWhere("booking.date", $startDate, $endDate)
				//->andWhere("booking.date BETWEEN '" . $startDate . "' AND '" . $endDate . "'")
				->inWhere("booking.status", [
					$model::STATUS_OPEN,
					$model::STATUS_NOT_TO_INVOICE,
					$model::STATUS_INVOICED,
					$model::STATUS_RESCINDED,
					$model::STATUS_RESCINDED_OPEN])
				//->andWhere("booking.status IN (:status_ope:,:status_nti:,:status_inv:,:status_res:,:status_sop:)", [
				//	'status_ope' => $model::STATUS_OPEN,
				//	'status_nti' => $model::STATUS_NOT_TO_INVOICE,
				//	'status_inv' => $model::STATUS_INVOICED,
				//	'status_res' => $model::STATUS_RESCINDED,
				//	'status_sop' => $model::STATUS_RESCINDED_OPEN,
				//])
				//->andWhere($employeeCondition)
				//->andWhere($roomCondition)
				->andWhere($empRoomCondition)
				->getQuery()
				->execute();
			
			// Block Booking Timespots
			if(count($bookings) > 0) {
				foreach($bookings as $booking) {
					$spots->addBooking($booking);
				}
			}
			
			// SQL Condition for Employees or Locations
			$emplocCondition  = "(a.employee_id IN (" . $emparr . ")) OR ";
			$emplocCondition .= "(a.location_id = " . $location . ")";

			// SQL Condition for Types / Dates
			$typeCondition  = "(a.type = ".$modelAvailabilities::TYPES['LOCATION_GENERAL']." OR a.type = ".$modelAvailabilities::TYPES['EMPLOYEE_GENERAL'].") OR ";
			$typeCondition .= "((a.type = ".$modelAvailabilities::TYPES['LOCATION_CUSTOM']." OR a.type = ".$modelAvailabilities::TYPES['EMPLOYEE_CUSTOM'].") AND a.date BETWEEN '" . $startDate . "' AND '" . $endDate ."')";

			// Get all Availabilities for the specified month
			$availabilities = $this->modelsManager->createBuilder()
				->columns('a.*')
				->from(['a' => $modelAvailabilities])
				->where("a.client_id = :cid:", ["cid" => $client_id])
				->andWhere($emplocCondition)
				->andWhere($typeCondition)
				->orderBy('a.type, a.location_id, a.employee_id, a.date, a.weekday, a.start')
				->getQuery()
				->execute();
			
			// Block / Handle the availabilities:
			foreach($availabilities as $availability) {
				$spots->addAvailability($availability);
			}
			
			// Get Duration of Service
			$duration = empty($product->breakAfter) ? (int)$product->duration : (int)$product->duration + (int)$product->breakAfter;

			// Get days with open Spots from spots array
			$retArr = $spots->getOpenDays($duration);
			
			return $retArr;
		});
	}

	/**
	 * get the available Timeslots for choosen product, location and optional employee on a selected day
	 */
	
	public function getFreeTimesAllEmployeesAction($product, $location, $year, $month, $day)
	{
		return $this->getFreeTimesAction($product, $location, 0, $year, $month, $day);
	}

	public function getFreeTimesAction($product, $location, $employee, $year, $month, $day)
	{
		return $this->jsonReadRequest(function(&$transaction) use ($year, $month, $day, $product, $location, $employee) {
			
			$model = $this->from["Bookings"];
			$modelRooms = $this->from["Rooms"];
			$modelEmployees = $this->from["Employees"];
			$modelAvailabilities = $this->from["Availabilities"];
			$modelProducts = $this->from['Products'];
			$modelLocEmp = $this->from["LocationsEmployees"];

			$client_id = $this->getClientID();
			if($client_id == 0) throw new \Exception("Wrong Client Id", 400);

			$settings = $this->getSettings($client_id);
			$spotLength = $settings->time_period;
			$period_before = $settings->min_time_from_now;
			$period_after = $settings->max_time_from_now;

			$ressources = array(
				'rooms' => array(),
				'employees' => array(),
			);

			// Get needed Rooms and Employees for the Product
			$product = $modelProducts::findFirstById($product);
			if($product == null ) {
				throw new \Exception("Service not found: ".$product, 404);
			}

			$rooms = $product->getRoomsByLocation($location);
			if(empty($rooms)) {
				throw new \Exception("No Rooms available for Product", 404);
			}
			foreach($rooms as $room) {
				$ressources['rooms'][] = $room->id;
			}

			if($employee == 0) {
				$prodemps = $product->getActiveEmployees();
				foreach($prodemps as $emp) {
					if($emp->worksAtLocation((int)$location)) $ressources['employees'][] = $emp->id;
				}
				if(count($ressources['employees']) === 0) {
					return array();
				}
			} else {
				$ressources['employees'][] = $employee;
			}

			$date = $year . "-" . sprintf("%02d", $month) . "-" . sprintf("%02d", $day);
			$dt = new \DateTime($date);
			$weekday = (int)$dt->format("w");

			// Employee Condition for Bookings and availabilities
			$emparr = "";
			foreach($ressources['employees'] as $emp_id) {
				$emparr .= $emp_id;
				$emparr .= ",";
			}
			$emparr = substr($emparr, 0, -1);

			$employeeCondition = "booking.employee_id IS NULL OR booking.employee_id IN (".$emparr.")";
			if($employee != 0) {
				$employeeCondition = "booking.employee_id = " . $employee;
			}

			// Room Condition for Bookings
			$roomarr = "";
			foreach($ressources['rooms'] as $room_id) {
				$roomarr .= $room_id;
				$roomarr .= ",";
			}
			$roomarr = substr($roomarr, 0, -1);
			$roomCondition = "booking.room_id IN (".$roomarr.")";

			$empRoomCondition = "((" . $roomCondition . ") OR (" . $employeeCondition . "))";

			// Create Array with Availabilities
			$spots = new \Api\V1\OnlineBooking\Logic\AvailSpotsHandler($date, $date, $spotLength, $ressources, $period_before, $period_after);
			
			// Get all Bookings for specified month
			$bookings = $this->modelsManager->createBuilder()
				->columns('booking.*')
				->from(['booking' => $model])
				->where("booking.client_id = :cid:", ["cid" => $client_id])
				->andWhere("booking.date = '" . $date . "'")
				->inWhere("booking.status", [
					$model::STATUS_OPEN,
					$model::STATUS_NOT_TO_INVOICE,
					$model::STATUS_INVOICED,
					$model::STATUS_RESCINDED,
					$model::STATUS_RESCINDED_OPEN])
				->andWhere($empRoomCondition)
				->getQuery()
				->execute();
			
			// Block Booking Timespots
			foreach($bookings as $booking) {
				$spots->addBooking($booking);
			}

			// SQL Condition for Employees or Locations
			$emplocCondition  = "(a.employee_id IN (" . $emparr . ")) OR ";
			$emplocCondition .= "(a.location_id = " . $location . ")";

			// SQL Condition for Types / Dates
			$typeCondition  = "((a.type = ".$modelAvailabilities::TYPES['LOCATION_GENERAL']." OR a.type = ".$modelAvailabilities::TYPES['EMPLOYEE_GENERAL'].") AND a.weekday = ".$weekday.") OR ";
			$typeCondition .= "((a.type = ".$modelAvailabilities::TYPES['LOCATION_CUSTOM']." OR a.type = ".$modelAvailabilities::TYPES['EMPLOYEE_CUSTOM'].") AND a.date = '" . $date . "')";

			// Get all Availabilities for the specified month
			$availabilities = $this->modelsManager->createBuilder()
				->columns('a.*')
				->from(['a' => $modelAvailabilities])
				->where("a.client_id = :cid:", ["cid" => $client_id])
				->andWhere($emplocCondition)
				->andWhere($typeCondition)
				->orderBy('a.type, a.location_id, a.employee_id, a.date, a.weekday, a.start')
				->getQuery()
				->execute();
			
			// Block / Handle the availabilities:
			foreach($availabilities as $availability) {
				$spots->addAvailability($availability);
			}
			
			// Get Duration of Service
			$duration = empty($product->breakAfter) ? (int)$product->duration : (int)$product->duration + (int)$product->breakAfter;

			// Get days with open Spots from spots array
			$retArr = $spots->getOpenTimeslots($date, $duration);
			
			return $retArr;
		});
	}

	/**
	 * Create an online booking
	 */
	public function createBookingAction()
	{
		return $this->jsonWriteRequest(function(&$transaction) {

			$client_id = $this->getClientID();
			if($client_id == 0) {
				throw new \Exception("No Client ID set", 400);
			}

			$transaction = $this->transaction;
			$data = $this->request->getJsonRawBody();

			// Get needed setup
			$model = $this->from['Onlinebookings'];
			$modelBookings = $this->from["Bookings"];
			$modelBookingparts = $this->from["Bookingparts"];
			$modelRooms = $this->from["Rooms"];
			$modelEmployees = $this->from["Employees"];
			$modelAvailabilities = $this->from["Availabilities"];
			$modelProducts = $this->from['Products'];
			$modelLocEmp = $this->from["LocationsEmployees"];
			
			$settings = $this->getSettings($client_id);
			$spotLength = $settings->time_period;
			$period_before = $settings->min_time_from_now;
			$period_after = $settings->max_time_from_now;

			$ressources = array(
				'rooms' => array(),
				'employees' => array(),
			);
			
			// Required fields checkup
			if(empty($data->firstname)) { throw new \Exception("No Firstname entered", 400); }
			if(empty($data->name)) { throw new \Exception("No Name entered", 400); }
			if(empty($data->mail)) { throw new \Exception("No E-Mail address entered", 400); }

			// Product, Location, Employee Checkup and Availability Checkup
			if(empty($data->product_id)) { throw new \Exception("No Service defined", 400); }
			$product = $modelProducts::findFirstById($data->product_id);
			if(!$product) {
				throw new \Exception("Service not found: ".$data->product_id, 404);
			}

			// Get needed Rooms and Employees for the Product
			if(empty($data->location_id)) { throw new \Exception("No Location defined", 400); }
			$location = $data->location_id;
			$rooms = $product->getRoomsByLocation($location);
			if(empty($rooms)) {
				throw new \Exception("No Rooms available for Product", 404);
			}
			foreach($rooms as $room) {
				$ressources['rooms'][] = $room->id;
			}

			if(empty($data->employee_id)) {
				$prodemps = $product->getActiveEmployees();
				foreach($prodemps as $emp) {
					if($emp->worksAtLocation((int)$location)) $ressources['employees'][] = $emp->id;
				}
				if(count($ressources['employees']) == 0) {
					throw new \Exception("No Employees available for Product and Location", 404);
				}
			} else {
				$ressources['employees'][] = $data->employee_id;
			}

			if(!isset($data->date)) { throw new \Exception("No Date defined", 400); }
			if(!isset($data->startTime)) { throw new \Exception("No Time defined", 400); }
			$dt = new \DateTime($data->date);
			$date = $dt->format("Y-m-d");
			$weekday = (int)$dt->format("w");
			// Get Duration of Service
			$duration = empty($product->breakAfter) ? (int)$product->duration : (int)$product->duration + (int)$product->breakAfter;
			$st = new \DateTime($data->startTime);
			$bookstart = $st->format("H:i:s");
			$st->add(new \DateInterval('PT'.$duration.'M'));
			$bookend = $st->format("H:i:s");

			// Employee Condition for Bookings and availabilities
			$emparr = "";
			$first = true;
			foreach($ressources['employees'] as $emp_id) {
				if($first) $first = false;
				else $emparr .= ",";
				$emparr .= $emp_id;
			}
			$employeeCondition = "booking.employee_id IS NULL OR booking.employee_id IN (".$emparr.")";

			// Room Condition for Bookings
			$roomarr = "";
			$first = true;
			foreach($ressources['rooms'] as $room_id) {
				if($first) $first = false;
				else $roomarr .= ",";
				$roomarr .= $room_id;
			}
			$roomCondition = "booking.room_id IS NULL OR booking.room_id IN (".$roomarr.")";

			$empRoomCondition = "((" . $roomCondition . ") OR (" . $employeeCondition . "))";

			// Create Array with Availabilities
			$spots = new \Api\V1\OnlineBooking\Logic\AvailSpotsHandler($date, $date, $spotLength, $ressources, $period_before, $period_after);

			// Get all Bookings for specified month
			$bookings = $this->modelsManager->createBuilder()
				->columns('booking.*')
				->from(['booking' => $modelBookings])
				->where("booking.client_id = :cid:", ["cid" => $client_id])
				->andWhere("booking.date = '" . $date . "'")
				->andWhere("booking.startTime < '" . $bookend . "'")
				->andWhere("booking.endTime > '" . $bookstart . "'")
				->inWhere("booking.status", [
					$modelBookings::STATUS_OPEN,
					$modelBookings::STATUS_NOT_TO_INVOICE,
					$modelBookings::STATUS_INVOICED,
					$modelBookings::STATUS_RESCINDED,
					$modelBookings::STATUS_RESCINDED_OPEN])
				->andWhere($empRoomCondition)
				->getQuery()
				->execute();

			// Block Booking Timespots
			foreach($bookings as $booking) {
				$spots->addBooking($booking);
			}

			// SQL Condition for Employees or Locations
			$emplocCondition  = "(a.employee_id IN (" . $emparr . ")) OR ";
			$emplocCondition .= "(a.location_id = " . $location . ")";

			// SQL Condition for Types / Dates
			$typeCondition  = "((a.type = ".$modelAvailabilities::TYPES['LOCATION_GENERAL']." OR a.type = ".$modelAvailabilities::TYPES['EMPLOYEE_GENERAL'].") AND a.weekday = ".$weekday.") OR ";
			$typeCondition .= "((a.type = ".$modelAvailabilities::TYPES['LOCATION_CUSTOM']." OR a.type = ".$modelAvailabilities::TYPES['EMPLOYEE_CUSTOM'].") AND a.date = '" . $date . "')";

			// Get all Availabilities for the specified month
			$availabilities = $this->modelsManager->createBuilder()
				->columns('a.*')
				->from(['a' => $modelAvailabilities])
				->where("a.client_id = :cid:", ["cid" => $client_id])
				->andWhere($emplocCondition)
				->andWhere($typeCondition)
				->orderBy('a.type, a.location_id, a.employee_id, a.date, a.weekday, a.start')
				->getQuery()
				->execute();
			
			// Block / Handle the availabilities:
			foreach($availabilities as $availability) {
				$spots->addAvailability($availability);
			}

			// Get days with open Spots from spots array
			$info = $spots->timeSlotInfo($date, $bookstart, $duration);

			if(empty($info)) { throw new \Exception("Availability Checkup Error", 404); }
			if(!$info['available']) { throw new \Exception("Time no longer available", 451); }
			
			// Select available room and employee and Create Booking
			if(empty($info['ressources']['rooms'])) { throw new \Exception("No rooms available", 451); }
			if(empty($info['ressources']['employees'])) { throw new \Exception("No employees available", 451); }
			$available_rooms = $info['ressources']['rooms'];
			$available_employees = $info['ressources']['employees'];
			shuffle($available_rooms);
			shuffle($available_employees);
			$book_room_id = (int)array_rand(array_flip($available_rooms));
			$book_emp_id = (int)array_rand(array_flip($available_employees));

			// Create Booking Object which will be connected to the Onlinebooking object
			$book = new \StdClass(); 
			$book->type = 300;
			$book->employee_id = $book_emp_id;
			$book->product_id = $product->id;
			$book->room_id = $book_room_id;
			$book->price = $product->price;
			$book->discount = 0;
			$book->date = $date;
			$book->startTime = $bookstart;
			$book->endTime = $bookend;
			$book->duration = $product->duration;
			$book->breakAfter = $product->breakAfter;
			$book->title = $product->title;
			$book->comment = empty($data->comment) ? null : "Onlinebooking Kommentar: ".$data->comment;
			$book->status = $modelBookings::STATUS_OPEN;
			$book->client_id = $client_id;

			$booking_obj = $this->_createActionObj($book, $modelBookings, $modelBookings::$attrs);

			// Create Bookingparts for that Booking Obj
			foreach($product->productparts as $part)
			{
				$book_part = $modelBookingparts::createContentObjFromProductPart($part, (int)$booking_obj->id);
				$this->_createActionObj($book_part, $modelBookingparts, $modelBookingparts::$attrs);
			}

			// Everything defined! Create Onlinebooking!
			$book_obj = new \StdClass();
			$book_obj = (new \DateTime('now'))->format('Y-m-d H:i:s');
			$book_obj->firstname = $data->firstname;
			$book_obj->name = $data->name;
			$book_obj->email = $data->mail;
			$book_obj->phone = empty($data->phone) ? null : $data->phone;
			$book_obj->comment = empty($data->comment) ? null : $data->comment;
			$book_obj->title = $product->title;
			$book_obj->date = $date;
			$book_obj->startTime = $bookstart;
			$book_obj->endTime = $bookend;
			$book_obj->duration = $product->duration;
			$book_obj->breakAfter = $product->breakAfter;
			$book_obj->price = $product->price;
			$book_obj->status = $model::STATUS_OPEN;
			$book_obj->product_id = $product->id;		
			$book_obj->employee_id = $book_emp_id;
			$book_obj->room_id = $book_room_id;
			$book_obj->booking_id = $booking_obj->id;
			$book_obj->client_id = $client_id;

			$obj = $this->_createActionObj($book_obj, $model, $model::$attrs);

			// Objects created - send Confirmation to Employee and Customer
			$this->sendCustomerConfirmationMail($obj);
			if(boolval($settings->employee_notification)) {
				$this->sendEmployeeNotificationMail($obj);
			}

			// Finished -> checkout!
			$transaction->commit();
			return $obj;
		});
	}

	/**
	 *	Get all Locations - admin side action
	 */
	public function getLocationsAction()
	{
		return $this->jsonReadRequest(function(&$transaction) {

			$model = $this->from["Locations"];

			$client_id = $this->getClientID();
			if($client_id == 0) {
				throw new \Exception("No Client ID set", 400);
			}

			$rows = $this->modelsManager->createBuilder()
				->columns('location.*')
				->from(['location' => $model])
				->where("location.client_id = :cid:", ["cid" => $client_id])
				->orderBy('location.title, location.id')
				->getQuery()
				->execute();

			$retArr = array();
			foreach($rows as $row) {
				$retArr[] = $this->_valueConversionPostload($row, $model::$attrs)->toArray();
			}

			return $retArr;
		});
	}

	/**
	 * Get all weekly open times for a specific location
	 */
	public function getLocationWeektimesAction($id)
	{
		return $this->jsonReadRequest(function(&$transaction) use ($id) {

			$model = $this->from["Availabilities"];

			$client_id = $this->getClientID();
			if($client_id == 0) {
				throw new \Exception("No Client ID set", 400);
			}

			$rows = $this->modelsManager->createBuilder()
				->columns('a.*')
				->from(['a' => $model])
				->where("a.client_id = :cid:", ["cid" => $client_id])
				->andWhere("a.type = :type:", ["type" => $model::TYPES['LOCATION_GENERAL']])
				->andWhere("a.location_id = :lid:", ["lid" => $id])
				->orderBy('a.weekday, a.start')
				->getQuery()
				->execute();

			$retArr = array();
			foreach($rows as $row) {
				$retArr[] = $this->_valueConversionPostload($row, $model::$attrs)->toArray();
			}

			return $retArr;
		});
	}

	/**
	 * Set all general weekly availabilities of a location
	 */
	public function setLocationWeektimesAction($id)
	{
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);

		return $this->jsonWriteRequest(function(&$transaction) use ($id) {

			$transaction = $this->transaction;

			// Get needed setup
			$model = $this->from['Availabilities'];
			$attrs = $model::$attrs;
			
			$newrows = $this->request->getJsonRawBody();
			if(!is_array($newrows)) throw new \Exception("Wrong data structure", 400);

			$client_id = $this->getClientID();
			if($client_id == 0) {
				throw new \Exception("No Client ID set", 400);
			}

			// Delete Old Rows first:
			// Get old rows
			$oldrows = $this->modelsManager->createBuilder()
				->columns('a.*')
				->from(['a' => $model])
				->where("a.client_id = :cid:", ["cid" => $client_id])
				->andWhere("a.type = :type:", ["type" => $model::TYPES['LOCATION_GENERAL']])
				->andWhere("a.location_id = :lid:", ["lid" => $id])
				->getQuery()
				->execute();

			foreach($oldrows as $ava) {
				$this->_deleteActionObj($model, (int)$ava->id);
			}

			// Old ones deleted: Create new ones:
			foreach($newrows as $newava) {
				$this->_createActionObj($newava, $model, $attrs);
			}

			// Finished -> checkout!
			$transaction->commit();
			return $newrows;
		});	
	}

	/**
	 * Get all special daily availabilites of a location
	 */
	public function getLocationMonthtimesAction($id, $year, $month)
	{
		return $this->jsonReadRequest(function(&$transaction) use ($id, $year, $month){

			$model = $this->from["Availabilities"];

			$client_id = $this->getClientID();
			if(empty($client_id)) {
				throw new \Exception("No Client ID set", 400);
			}

			$startDate = $year . "-" . sprintf("%02d", $month) . "-01";
			$date = new \DateTime($startDate);
			$endDate = $date->format("Y-m-t");

			$rows = $this->modelsManager->createBuilder()
				->columns('a.*')
				->from(['a' => $model])
				->where("a.client_id = :cid:", ["cid" => $client_id])
				->andWhere("a.type = :type:", ["type" => $model::TYPES['LOCATION_CUSTOM']])
				->andWhere("a.location_id = :lid:", ["lid" => $id])
				->andWhere("a.date BETWEEN '" . $startDate . "' AND '" . $endDate . "'")
				->orderBy('a.date, a.start')
				->getQuery()
				->execute();

			$retArr = array();
			foreach($rows as $row) {
				$retArr[] = $this->_valueConversionPostload($row, $model::$attrs)->toArray();
			}

			return $retArr;
		});
	}

	/**
	 * Set new Monthly Availabilities for a specific location
	 */
	public function setLocationMonthtimesAction($id, $year, $month)
	{
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);
		
		return $this->jsonWriteRequest(function(&$transaction) use ($id, $year, $month) {

			$transaction = $this->transaction;

			// Get needed setup
			$model = $this->from['Availabilities'];
			$attrs = $model::$attrs;
			
			$newrows = $this->request->getJsonRawBody();
			if(!is_array($newrows)) throw new \Exception("Wrong data structure", 400);

			$client_id = $this->getClientID();
			if($client_id == 0) {
				throw new \Exception("No Client ID set", 403);
			}

			$startDate = $year . "-" . sprintf("%02d", $month) . "-01";
			$date = new \DateTime($startDate);
			$endDate = $date->format("Y-m-t");
			
			// Delete Old Rows first:
			// Get old rows
			$oldrows = $this->modelsManager->createBuilder()
				->columns('a.*')
				->from(['a' => $model])
				->where("a.client_id = :cid:", ["cid" => $client_id])
				->andWhere("a.type = :type:", ["type" => $model::TYPES['LOCATION_CUSTOM']])
				->andWhere("a.location_id = :lid:", ["lid" => $id])
				->andWhere("a.date BETWEEN '" . $startDate . "' AND '" . $endDate . "'")
				->getQuery()
				->execute();

			foreach($oldrows as $ava) {
				$this->_deleteActionObj($model, (int)$ava->id);
			}

			// Old ones deleted: Create new ones:
			foreach($newrows as $newava) {
				$this->_createActionObj($newava, $model, $attrs);
			}

			// Finished -> checkout!
			$transaction->commit();
			return $newrows;
		});
	}

	/**
	 * Get List of all Employees
	 */
	public function getEmployeesAction($location = 0, $product = 0)
	{
		return $this->jsonReadRequest(function(&$transaction) {

			$model = $this->from["Employees"];
			$modelLoc = $this->from["Locations"];

			$client_id = $this->getClientID();
			if($client_id == 0) {
				throw new \Exception("No Client ID set", 400);
			}

			$rows = $this->modelsManager->createBuilder()
				->columns('employee.*')
				->from(['employee' => $model])
				->where("employee.client_id = :cid:", ["cid" => $client_id])
				->andWhere("employee.active = 1")
				->orderBy('employee.name, employee.firstname, employee.id')
				->getQuery()
				->execute();

			$retArr = array();
			foreach($rows as $row) {
				$emp = $this->_valueConversionPostload($row, $model::$attrs)->toArray();
				$locations = $row->locations;
				$locs = array();
				foreach($locations as $l) {
					$loc = $this->_valueConversionPostload($l, $modelLoc::$attrs)->toArray();
					$locs[] = $loc;
				}
				$emp['locations'] = $locs;
				$retArr[] = $emp;
			}

			return $retArr;
		});
	}

	/**
	 * Get all weekly Availabilities for a specific employee
	 */
	public function getEmployeeWeektimesAction($id)
	{
		return $this->jsonReadRequest(function(&$transaction) use ($id){

			$model = $this->from["Availabilities"];

			$client_id = $this->getClientID();
			if($client_id == 0) {
				throw new \Exception("No Client ID set", 400);
			}

			$rows = $this->modelsManager->createBuilder()
				->columns('a.*')
				->from(['a' => $model])
				->where("a.client_id = :cid:", ["cid" => $client_id])
				->andWhere("a.type = :type:", ["type" => $model::TYPES['EMPLOYEE_GENERAL']])
				->andWhere("a.employee_id = :eid:", ["eid" => $id])
				->orderBy('a.weekday, a.start')
				->getQuery()
				->execute();

			$retArr = array();
			foreach($rows as $row) {
				$retArr[] = $this->_valueConversionPostload($row, $model::$attrs)->toArray();
			}

			return $retArr;
		});
	}

	/**
	 * Set all Employee Weektimes
	 */
	public function setEmployeeWeektimesAction($id)
	{
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);

		return $this->jsonWriteRequest(function(&$transaction) use ($id) {

			$transaction = $this->transaction;

			// Get needed setup
			$model = $this->from['Availabilities'];
			$attrs = $model::$attrs;

			$newrows = $this->request->getJsonRawBody();
			if(!is_array($newrows)) throw new \Exception("Wrong data structure", 400);

			$client_id = $this->getClientID();
			if($client_id == 0) {
				throw new \Exception("No Client ID set", 403);
			}

			// Delete Old Rows first:
			// Get old rows
			$oldrows = $this->modelsManager->createBuilder()
				->columns('a.*')
				->from(['a' => $model])
				->where("a.client_id = :cid:", ["cid" => $client_id])
				->andWhere("a.type = :type:", ["type" => $model::TYPES['EMPLOYEE_GENERAL']])
				->andWhere("a.employee_id = :eid:", ["eid" => $id])
				->getQuery()
				->execute();

			foreach($oldrows as $ava) {
				$this->_deleteActionObj($model, (int)$ava->id);
			}

			// Old ones deleted: Create new ones:
			foreach($newrows as $newava) {
				$this->_createActionObj($newava, $model, $attrs);
			}

			// Finished -> checkout!
			$transaction->commit();
			return $newrows;
		});
	}

	/**
	 * Get all Monthly special availabilities for a specific Employee
	 */
	public function getEmployeeMonthtimesAction($id, $year, $month)
	{
		return $this->jsonReadRequest(function(&$transaction) use ($id, $year, $month){
			
			$model = $this->from["Availabilities"];

			$client_id = $this->getClientID();
			if(empty($client_id)) {
				throw new \Exception("No Client ID set", 400);
			}

			$startDate = $year . "-" . sprintf("%02d", $month) . "-01";
			$date = new \DateTime($startDate);
			$endDate = $date->format("Y-m-t");

			$rows = $this->modelsManager->createBuilder()
				->columns('a.*')
				->from(['a' => $model])
				->where("a.client_id = :cid:", ["cid" => $client_id])
				->andWhere("a.type = :type:", ["type" => $model::TYPES['EMPLOYEE_CUSTOM']])
				->andWhere("a.employee_id = :eid:", ["eid" => $id])
				->andWhere("a.date BETWEEN '" . $startDate . "' AND '" . $endDate . "'")
				->orderBy('a.date, a.start')
				->getQuery()
				->execute();

			$retArr = array();
			foreach($rows as $row) {
				$retArr[] = $this->_valueConversionPostload($row, $model::$attrs)->toArray();
			}

			return $retArr;
		});
	}

	/**
	 * Set all monthly specific availabilities for an employee
	 */
	public function setEmployeeMonthtimesAction($id, $year, $month)
	{
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);
		
		return $this->jsonWriteRequest(function(&$transaction) use ($id, $year, $month) {

			$transaction = $this->transaction;

			// Get needed setup
			$model = $this->from['Availabilities'];
			$attrs = $model::$attrs;
			
			$newrows = $this->request->getJsonRawBody();
			if(!is_array($newrows)) throw new \Exception("Wrong data structure", 400);

			$client_id = $this->getClientID();
			if($client_id == 0) {
				throw new \Exception("No Client ID set", 403);
			}

			$startDate = $year . "-" . sprintf("%02d", $month) . "-01";
			$date = new \DateTime($startDate);
			$endDate = $date->format("Y-m-t");
			
			// Delete Old Rows first:
			// Get old rows
			$oldrows = $this->modelsManager->createBuilder()
				->columns('a.*')
				->from(['a' => $model])
				->where("a.client_id = :cid:", ["cid" => $client_id])
				->andWhere("a.type = :type:", ["type" => $model::TYPES['EMPLOYEE_CUSTOM']])
				->andWhere("a.employee_id = :eid:", ["eid" => $id])
				->andWhere("a.date BETWEEN '" . $startDate . "' AND '" . $endDate . "'")
				->getQuery()
				->execute();

			// Delete them
			foreach($oldrows as $ava) {
				$this->_deleteActionObj($model, (int)$ava->id);
			}

			// Old ones deleted: Create new ones:
			foreach($newrows as $newava) {
				$this->_createActionObj($newava, $model, $attrs);
			}

			// Finished -> checkout!
			$transaction->commit();
			return $newrows;
		});
	}

	// Index all Onlinebookings for a client
	public function indexAction()
	{
		// site id or similar filter? just about access rights
		$default_projection     = array("o.id", "o.bookedAt", "o.firstname", "o.name", "o.email", "o.phone", "o.comment", "o.title", "o.date", "o.startTime", "o.endTime", "o.duration", "o.breakAfter", "o.price", "o.status", "o.cancelledAt", "o.cancel_reason", "o.product_id", "o.employee_id", "o.room_id", "o.booking_id", "o.client_id");
		$orderby                = array("o.date", "o.startTime", "o.status", "o.name");
		$attrs_for_projection	= array("o.id", "o.bookedAt", "o.firstname", "o.name", "o.email", "o.phone", "o.comment", "o.title", "o.date", "o.startTime", "o.endTime", "o.duration", "o.breakAfter", "o.price", "o.status", "o.cancelledAt", "o.cancel_reason", "o.product_id", "o.employee_id", "o.room_id", "o.booking_id", "o.client_id");
		$attrs_for_where        = array("o.id", "o.bookedAt", "o.firstname", "o.name", "o.email", "o.phone", "o.date", "o.startTime", "o.endTime", "o.duration", "o.breakAfter", "o.price", "o.status", "o.cancelledAt", "o.cancel_reason", "o.product_id", "o.employee_id", "o.room_id", "o.booking_id", "o.client_id");
		$attrs_for_orderby      = array("o.id", "o.bookedAt", "o.firstname", "o.name", "o.email", "o.phone", "o.date", "o.startTime", "o.endTime", "o.duration", "o.breakAfter", "o.price", "o.status", "o.cancelledAt", "o.cancel_reason", "o.product_id", "o.employee_id", "o.room_id", "o.booking_id", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from["Onlinebookings"];
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from["Onlinebookings"], $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}
		

	public function indexOpenBookingsAction()
	{
		return $this->jsonReadRequest(function(&$transaction) {

			$model = $this->from["Onlinebookings"];
			$client_id = $this->grantAccessForClient();
			$transaction = $this->transaction;

			$rows = $this->modelsManager->createBuilder()
				->columns('booking.*')
				->from(['booking' => $model])
				->where("booking.client_id = :cid:", ["cid" => $client_id])
				->andWhere("booking.status IN (:status_ope:,:status_opa:,:status_app:)", [
					'status_ope' => $model::STATUS_OPEN,
					'status_opa' => $model::STATUS_OPEN_ASSIGNED,
					'status_app' => $model::STATUS_APPROVED
				])
				->orderBy('booking.date, booking.startTime, booking.status, booking.name, booking.id')
				->getQuery()
				->execute();
			// Create custom structure with bookings associated to customers
			$retArr = array();
			foreach($rows as $row) {
				$retArr[] = $this->_valueConversionPostload($row, $model::$attrs)->toArray();
			}
			return $retArr;
		});
	}

	public function assignCustomerAction($id, $customer)
	{
		return $this->jsonWriteRequest(function(&$transaction) use ($id, $customer) {

			$model = $this->from["Onlinebookings"];
			$modelcust = $this->from["Customers"];
			$client_id = (int)$this->grantAccessForClient();
			$transaction = $this->transaction;

			$onlinebooking = $this->_oneActionObj($model, $id);
			$onlinebooking->setTransaction($this->transaction);
			$booking = $onlinebooking->booking;
			$booking->setTransaction($this->transaction);
			$customer_obj = $this->_oneActionObj($modelcust, $customer);

			if((int)$booking->client_id != $client_id) throw new \Exception("Wrong Client Id", 400);
			if((int)$onlinebooking->client_id != $client_id) throw new \Exception("Wrong Client Id", 400);
			if((int)$customer_obj->client_id != $client_id) throw new \Exception("Wrong Client Id", 400);

			// Permissions checked -> assign Booking
			$booking->customer_id = $customer;
			if(!$booking->update()) {
				throw new \Exception("Error Saving Phalcon Booking Object: ".$booking->id, 500);
			}
			if(!$onlinebooking->assignCustomer()) {
				throw new \Exception("Error Assigning Customer to Phalcon Onlinebooking Object: ".$onlinebooking->id, 500);
			}

			$transaction->commit();
			return $onlinebooking->jsonArray();
		});
	}

	public function oneAction($id)
	{
		return $this->jsonReadRequest(function(&$transaction) use ($id) {
			$model = $this->from["Onlinebookings"];
			$postLoad = function ($obj) {
				$model = $this->from["Onlinebookings"];
				return $this->_valueConversionPostload($obj, $model::$attrs);
			};
			$obj = $this->_oneActionObj($model, $id, $postLoad);

			$retArr = $obj->jsonArray();
			$retArr['customer_match'] = null;
			if(!$obj->isAssignedToCustomer()) {
				$retArr['customer_match'] = $this->getPossibleCustomerMatches($obj);
			}

			return $retArr;
		});
	}

	public function acceptBookingAction($id)
	{
		return $this->jsonWriteRequest(function(&$transaction) use ($id) {
			$transaction = $this->transaction;
			$onlinebooking = $this->approveOnlinebooking($id);
			$transaction->commit();
			return $onlinebooking->jsonArray();
		});
	}

	public function declineBookingAction($id)
	{
		return $this->jsonWriteRequest(function(&$transaction) use ($id) {
			$transaction = $this->transaction;
			$model = $this->from["Onlinebookings"];
			$modelBook = $this->from["Bookings"];
			$client_id = (int)$this->grantAccessForClient();
	
			$onlinebooking = $this->_oneActionObj($model, $id);
			if((int)$onlinebooking->client_id != $client_id) throw new \Exception("Wrong Client Id", 400);
			$onlinebooking->setTransaction($this->transaction);

			$booking = $onlinebooking->booking;
			$booking->setTransaction($this->transaction);
			if((int)$booking->client_id != $client_id) throw new \Exception("Wrong Client Id", 400);
	
			// Permissions checked -> cancel Booking and Onlinebooking
			if(!$onlinebooking->cancel($model::CANCEL_REASONS['NOT_APPROVED'])) {
				throw new \Exception("Error Declining Phalcon Onlinebooking Object: ".$onlinebooking->id, 500);
			}

			if(!$booking->cancel($modelBook::CANCEL_REASONS['SELF'])) {
				throw new \Exception("Error Cancelling Phalcon Booking Object: ".$booking->id, 500);
			}

			// Send Decline Information Mail to Customer
			$this->sendCustomerDeclineMail($onlinebooking);

			$transaction->commit();
			return "ok";
		});
	}

	/*
	 * Check if one booking is cancellable by customer
	 */
	public function cancelCheckAction()
	{
		return $this->jsonReadRequest(function(&$transaction) {
			$transaction = $this->transaction;

			if(empty($this->request->getHeader('X-Booking-Id'))) {
				throw new \Exception("No Booking ID set", 400);
			}
			$obooking_id = $this->decryptId($this->request->getHeader('X-Booking-Id'));
			if(empty($obooking_id)) throw new \Exception("Wrong Booking ID", 400);

			$model = $this->from["Onlinebookings"];
			$modelBook = $this->from["Bookings"];
			$onlinebooking = $model::findFirstById($obooking_id);
			if(!$onlinebooking) throw new \Exception("Booking not found", 404);

			$client_id = (int)$onlinebooking->client_id;
			if($client_id == 0) {
				throw new \Exception("No Client ID set", 400);
			}

			$settings = $this->getSettings($client_id);
			$latest_cancel_diff = (int)$settings->max_cancel_before * 60 * 60; // max_cancel_before is in hours --> calculate seconds

			if(empty($onlinebooking->booking_id)) throw new \Exception("No connected booking", 404);
			$booking = $onlinebooking->booking;
			if((int)$booking->client_id != $client_id) throw new \Exception("Wrong Client Id", 400);

			$retArr = array(
				'cancellable'	=> true,
				'reason'		=> null,
				'pay'			=> false,
				'pay_price'		=> 0.0,
			);

			// Check cancellable by status:
			if(!$onlinebooking->cancellable()) {
				$retArr['cancellable'] = false;
				$retArr['reason'] = 1;
			} else {
				// Status ok, check time
				$booking_datetime = $onlinebooking->getTimestamp();
				$diff = $booking_datetime - time();
				if($diff <= 0) {
					$retArr['cancellable'] = false;
					$retArr['reason'] = 2;
				} else if($diff < $latest_cancel_diff) {
					// Cancellation too late - customer has to pay
		// TODO: Maybe price calculation if not fully payment for late cancelled bookings
					$retArr['pay'] = true;
					$retArr['pay_price'] = $booking->price;	
				}
			}

			// Add Booking Data
			$book = $this->_valueConversionPostload($booking, $modelBook::$attrs);
			$retArr['booking'] = array(
				"title"		=> $book->title,
				"date"		=> $book->date,
				"startTime"	=> $book->startTime,
				"duration"	=> $book->duration,
				"price"		=> $book->price,
			);

			$transaction->commit();
			return $retArr;
		});
	}

	public function cancelBookingAction()
	{
		return $this->jsonWriteRequest(function(&$transaction) {

			$transaction = $this->transaction;
			$model = $this->from['Onlinebookings'];
			$modelBook = $this->from["Bookings"];

			if(empty($this->request->getHeader('X-Booking-Id'))) {
				throw new \Exception("No Booking ID set", 400);
			}
			$obooking_id = $this->decryptId($this->request->getHeader('X-Booking-Id'));
			if(empty($obooking_id)) throw new \Exception("Wrong Booking ID", 400);
			

			$onlinebooking = $model::findFirstById($obooking_id);
			if(!$onlinebooking) throw new \Exception("Booking not found", 404);
			$onlinebooking->setTransaction($this->transaction);
			
			$client_id = $onlinebooking->client_id;
			if($client_id == 0) {
				throw new \Exception("No Client ID set", 400);
			}

			$settings = $this->getSettings($client_id);
			$latest_cancel_diff = (int)$settings->max_cancel_before * 60 * 60; // max_cancel_before is in hours

			if(empty($onlinebooking->booking_id)) throw new \Exception("No connected booking", 404);
			$booking = $onlinebooking->booking;
			$booking->setTransaction($this->transaction);
			if((int)$booking->client_id != $client_id) throw new \Exception("Wrong Booking Client Id", 400);
	
			// Check Reason
			$booking_datetime = $onlinebooking->getTimestamp();
			$diff = $booking_datetime - time();
			if($diff <= 0) {
				throw new \Exception("Booking not in the future", 400);
			}
			if($diff < $latest_cancel_diff) {
				// Cancelled too late - customer has to pay
// TODO: Maybe price calculation if not fully payment for late cancelled bookings
				$invoiceable = true;		
			} else {
				$invoiceable = false;
			}

			// Permissions checked -> cancel Booking and Onlinebooking
			if(!$onlinebooking->cancel($model::CANCEL_REASONS['CUSTOMER'])) {
				throw new \Exception("Error Declining Phalcon Onlinebooking Object: ".$onlinebooking->id, 500);
			}

			if(!$booking->cancel($modelBook::CANCEL_REASONS['CUSTOMER'], null, $invoiceable, $booking->price)) {
				throw new \Exception("Error Cancelling Phalcon Booking Object: ".$booking->id, 500);
			}

			// Send Information Mail to Customer and Employee
			$this->sendCustomerCancellationMail($onlinebooking);
			$this->sendEmployeeCancellationMail($onlinebooking);

			$transaction->commit();
			return "ok";
		});
	}

	private function approveOnlinebooking($id, $employeemail = false, $client_id = null)
	{
		$model = $this->from["Onlinebookings"];

		if($client_id === null) {
			$client_id = (int)$this->grantAccessForClient();
		}
		
		$onlinebooking = $model::findFirstByid($id);
		if (!$onlinebooking) {
			throw new \Exception("Item not found", 404);
		}

		if((int)$onlinebooking->client_id != $client_id) throw new \Exception("Wrong Client Id", 400);
		$onlinebooking->setTransaction($this->transaction);

		// Permissions checked -> accept Booking
		if(!$onlinebooking->approve()) {
			throw new \Exception("Error Approving Phalcon Onlinebooking Object: ".$onlinebooking->id, 500);
		}

		// Send Approvement Mail to Customer
		$this->sendCustomerApprovementMail($onlinebooking);
		if($employeemail) {
			$this->sendEmployeeApprovementMail($onlinebooking);
		}

		return $onlinebooking;
	}

	// Search and return possible matching Customers for one onlinebooking entry
	private function getPossibleCustomerMatches($onlinebooking)
	{
		$possible_matches = array();

		$model = $this->from['Customers'];
		$client_id	= $onlinebooking->client_id;
		
		$name = trim($onlinebooking->name);
		$firstname = trim($onlinebooking->firstname);
		$email = str_replace(' ', '', trim($onlinebooking->email));

		$search  = "customer.firstname LIKE '%".$firstname."%'";
		$search .= " OR customer.name LIKE '%".$name."%'";
		$search .= " OR customer.email LIKE '%".$email."%'";
		$search .= " OR customer.email_second LIKE '%".$email."%'";

		if(!empty($onlinebooking->phone)) {
			$phone = ltrim(preg_replace("/[^0-9]/", "", preg_replace("/\+[0-9]{2}/", "", $onlinebooking->phone)), '0');
			$search .= " OR REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(customer.phone_home,' ',''),'/',''),'-',''),'(',''),')','') LIKE '%".$phone."%'";
			$search .= " OR REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(customer.phone_mobile,' ',''),'/',''),'-',''),'(',''),')','') LIKE '%".$phone."%'";
			$search .= " OR REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(customer.phone_work,' ',''),'/',''),'-',''),'(',''),')','') LIKE '%".$phone."%'";
		}

		$rows = $this->modelsManager->createBuilder()
			->columns('customer.id, customer.firstname, customer.name, customer.email, customer.email_second, customer.phone_home, customer.phone_mobile, customer.phone_work')
			->from(['customer' => $model,])
			->where("customer.client_id = :cid:", ["cid" => $client_id])
			->andWhere($search)
			->orderBy('customer.name, customer.firstname')
			->getQuery()
			->execute();

		foreach($rows as $cust)
		{
			$entry = new \stdClass();
			$entry->id = $cust->id;
			$entry->name = !empty($cust->name) ? $cust->name : null;
			$entry->firstname = !empty($cust->firstname) ? $cust->firstname : null;
			$entry->matches = 0;
			$entry->match = array(
				"name" => false,
				"firstname" => false,
				"phone" => false,
				"email" => false,
			);
			$entry->email = "";
			$entry->phone = "";

			if(strpos(strtolower($cust->name), strtolower($name)) !== false) {
				$entry->match["name"] = true;
				$entry->matches++;
			}

			if(strpos(strtolower($cust->firstname), strtolower($firstname)) !== false) {
				$entry->match["firstname"] = true;
				$entry->matches++;
			}

			if(!empty($cust->email) && strpos(strtolower($cust->email), strtolower($email)) !== false) {
				$entry->match["email"] = true;
				$entry->email = $cust->email;
				$entry->matches++;
			} elseif(!empty($cust->email_second) && strpos(strtolower($cust->email_second), strtolower($email)) !== false) {
				$entry->match["email"] = true;
				$entry->email = $cust->email_second;
				$entry->matches++;
			} else {
				$entry->email = $model::getCustomerPrimaryMail($cust);
			}

			if(!empty($cust->phone_home) && strpos(preg_replace("/[^0-9]/", "", $cust->phone_home), $phone) !== false) {
				$entry->match["phone"] = true;
				$entry->phone = $cust->phone_home;
				$entry->matches++;
			} elseif(!empty($cust->phone_mobile) && strpos(preg_replace("/[^0-9]/", "", $cust->phone_mobile), $phone) !== false) {
				$entry->match["phone"] = true;
				$entry->phone = $cust->phone_mobile;
				$entry->matches++;
			} elseif(!empty($cust->phone_work) && strpos(preg_replace("/[^0-9]/", "", $cust->phone_work), $phone) !== false) {
				$entry->match["phone"] = true;
				$entry->phone = $cust->phone_work;
				$entry->matches++;
			} else {
				if(!empty($cust->phone_home)) {
					$entry->phone = $cust->phone_home;
				} elseif(!empty($cust->phone_mobile)) {
					$entry->phone = $cust->phone_mobile;
				} elseif(!empty($cust->phone_work)) {
					$entry->phone = $cust->phone_work;
				}
			}

			$possible_matches[] = $entry;
		}

		usort($possible_matches, function($a, $b) {
			if($a->matches < $b->matches) return 1;
			if($a->matches > $b->matches) return -1;

			if($a->match["email"] && !$b->match["email"]) return -1;
			if(!$a->match["email"] && $b->match["email"]) return 1;

			if($a->match["firstname"] && !$b->match["firstname"]) return -1;
			if(!$a->match["firstname"] && $b->match["firstname"]) return 1;

			if($a->match["name"] && !$b->match["name"]) return -1;
			if(!$a->match["name"] && $b->match["name"]) return 1;

			$a_match = $a->match["phone"] ? $a->matches - 1 : $a->matches;
			$b_match = $b->match["phone"] ? $b->matches - 1 : $b->matches;
			if($a_match < $b_match) return 1;
			if($a_match > $b_match) return -1;

			$a_match = $a->match["name"] ? $a_match - 1 : $a_match;
			$b_match = $b->match["name"] ? $b_match - 1 : $b_match;
			if($a_match < $b_match) return 1;
			if($a_match > $b_match) return -1;

			$a_match = $a->match["firstname"] ? $a_match - 1 : $a_match;
			$b_match = $b->match["firstname"] ? $b_match - 1 : $b_match;
			if($a_match < $b_match) return 1;
			if($a_match > $b_match) return -1;

			if(strcasecmp($a->name, $b->name) < 0) return -1;
			if(strcasecmp($a->name, $b->name) > 0) return 1;

			if(strcasecmp($a->firstname, $b->firstname) < 0) return -1;
			if(strcasecmp($a->firstname, $b->firstname) > 0) return 1;

			return 0;
		});

		return array_slice($possible_matches, 0, 5);
	}

	// Send Mail Notification of Approval to Customer
	private function sendCustomerApprovementMail($onlinebooking)
	{
		$type = \Models\V1\Sys\Emails::TYPES['ONLINEBOOKING_APPROVAL_CUSTOMER'];
		$to = array(
			$onlinebooking->email => $onlinebooking->firstname." ".$onlinebooking->name,
		);
		$vars = array(

		);
		$this->sendMail($onlinebooking, $type, $to, $vars);
	}

	// Send Mail Notification of Auto Approval to Employee
	private function sendEmployeeApprovementMail($onlinebooking)
	{
		$type = \Models\V1\Sys\Emails::TYPES['ONLINEBOOKING_APPROVAL_EMPLOYEE'];
		$employee = $onlinebooking->employee;
		$mailto = $employee->getPrimaryMail();
		$to = array(
			$mailto => $employee->firstname." ".$employee->name,
		);
		$vars = array(

		);
		$this->sendMail($onlinebooking, $type, $to, $vars);
	}

	// Send Mail Notification of Decline to Customer
	private function sendCustomerDeclineMail($onlinebooking)
	{
		$type = \Models\V1\Sys\Emails::TYPES['ONLINEBOOKING_DECLINATION_CUSTOMER'];
		$to = array(
			$onlinebooking->email => $onlinebooking->firstname." ".$onlinebooking->name,
		);
		$vars = array(

		);
		$this->sendMail($onlinebooking, $type, $to, $vars);
	}

	// Send Mail Notification of successful new Onlinebooking to Customer
	private function sendCustomerConfirmationMail($onlinebooking)
	{
		$type = \Models\V1\Sys\Emails::TYPES['ONLINEBOOKING_CONFIRMATION_CUSTOMER'];
		$to = array(
			$onlinebooking->email => $onlinebooking->firstname." ".$onlinebooking->name,
		);
		$vars = array(

		);
		$this->sendMail($onlinebooking, $type, $to, $vars);
	}

	// Send Mail Notification of successful new Onlinebooking to Employee
	private function sendEmployeeNotificationMail($onlinebooking)
	{
		$type = \Models\V1\Sys\Emails::TYPES['ONLINEBOOKING_CONFIRMATION_EMPLOYEE'];
		$employee = $onlinebooking->employee;
		$mailto = $employee->getPrimaryMail();
		$to = array(
			$mailto => $employee->firstname." ".$employee->name,
		);
		$vars = array(

		);
		$this->sendMail($onlinebooking, $type, $to, $vars);
	}

	// Send Information about cancelled Booking to Customer
	private function sendCustomerCancellationMail($onlinebooking)
	{
		$type = \Models\V1\Sys\Emails::TYPES['ONLINEBOOKING_CANCELLATION_CUSTOMER'];
		$to = array(
			$onlinebooking->email => $onlinebooking->firstname." ".$onlinebooking->name,
		);
		$vars = array(

		);
		$this->sendMail($onlinebooking, $type, $to, $vars);
	}

	// Send Information about cancelled Booking to Employee
	private function sendEmployeeCancellationMail($onlinebooking)
	{
		$type = \Models\V1\Sys\Emails::TYPES['ONLINEBOOKING_CANCELLATION_EMPLOYEE'];
		$employee = $onlinebooking->employee;
		$mailto = $employee->getPrimaryMail();
		$to = array(
			$mailto => $employee->firstname." ".$employee->name,
		);
		$vars = array(

		);
		$this->sendMail($onlinebooking, $type, $to, $vars);
	}

	// Send Reminder Mail for upcoming Booking to Customer
	private function sendCustomerReminderMail($onlinebooking)
	{
		$type = \Models\V1\Sys\Emails::TYPES['ONLINEBOOKING_REMINDER_CUSTOMER'];
		$to = array(
			$onlinebooking->email => $onlinebooking->firstname." ".$onlinebooking->name,
		);
		$vars = array(

		);
		$this->sendMail($onlinebooking, $type, $to, $vars);
	}

	private function sendMail($onlinebooking, $type, $to, $vars = [], $attachments = null)
	{
		$modelTemplates = "\Models\V1\Mailtemplates\Templates";
		$client_id = $onlinebooking->client_id;
		$mailfrom = $this->getSettings($client_id)->getSenderMail();
		$subject = $modelTemplates::getMailSubject($type, $client_id, $vars);
		$text = $modelTemplates::getMailBody($type, $client_id, $vars);
		$platform = \Models\V1\Sys\Platforms::findFirstByName('bluebamboo');

		$model = "Models\V1\Sys\Emails";
		$mail = new $model();
		$mail->state = $model::STATES["OPEN"];
		$mail->type = $type;
		$mail->language = "de";
		$mail->priority = 5;
		$mail->send_trials = 0;
		$mail->subject = $subject;
		$mail->from = $mailfrom;
		$mail->to = $to;
		$mail->body_plain = $text;
		$mail->attachments = $attachments;
		$mail->client_id = $client_id;
		$mail->platform_id = $platform->id;
		$mail->item_id = $onlinebooking->id;

		if (!$mail->create()) {
			throw new \Exception("Could not create Mail object", 500);
		}
	}

	/**
	 * Get all sent Onlinebooking Mails for the Client
	 */
	public function indexEmailsAction()
	{
		return $this->jsonReadRequest(function(&$transaction) {
			$model = "Models\V1\Sys\Emails";
			$client_id = $this->grantAccessForClient();

			$objs = $model::find("client_id = '".$client_id."' AND type >= '300' AND type < '400'");
			$res = array();
			foreach($objs as $o) {
				unset($o->body_html);
				unset($o->body_plain);
				unset($o->priority);
				unset($o->send_errors);
				unset($o->attachments);
				$res[] = $this->_valueConversionPostload($o, $model::$attrs);
			}
			return $res;
		});
	}

	public function reminderMailServiceAction()
	{
		return $this->jsonWriteRequest(function(&$transaction) {
			$model = $this->from["Onlinebookings"];
			$modelSettings = $this->from["Settings"];
			$transaction = $this->transaction;

			// Spooling key checkups
			if(!$this->request->hasQuery("spooling_key")) {
				throw new \Exception("No key provided!", 400);
			}
			$key_request = $this->request->getQuery("spooling_key");
			$key_server = getenv('spooling_key');
			if(empty($key_server)) {
				throw new \Exception("Spooling Key not defined", 500);
			}
			if($key_request != $key_server) {
				throw new \Exception("Wrong key!", 400);
			}

			// Get Clients with onlinebooking settings that defined to send reminder mails
			$settings = $modelSettings::find("send_reminder_mail > '0'");
			if(!$settings || count($settings) === 0) throw new \Exception("No entries found", 404);

			foreach($settings as $s) {
				$diff_days = $s->send_reminder_mail;
				$client_id = $s->client_id;

				$day = new \DateTime('today');
				$day->sub(new \DateInterval('P'.$diff_days.'D'));
				$d = $day->format('Y-m-d');

				$bookings = $model::find("client_id = '".$client_id."' AND date = '".$d."'");
				if(!$bookings || count($bookings) === 0) continue;

				foreach($bookings as $booking) {
					$this->sendCustomerReminderMail($booking);
				}
			}

			return "ok";
			
		});
	}

	public function autoapprovalServiceAction()
	{
		return $this->jsonWriteRequest(function(&$transaction) {
			$model = $this->from["Onlinebookings"];
			$modelSettings = $this->from["Settings"];
			$transaction = $this->transaction;

			// Spooling key checkups
			if(!$this->request->hasQuery("spooling_key")) {
				throw new \Exception("No key provided!", 400);
			}
			$key_request = $this->request->getQuery("spooling_key");
			$key_server = getenv('spooling_key');
			if(empty($key_server)) {
				throw new \Exception("Spooling Key not defined", 500);
			}
			if($key_request != $key_server) {
				throw new \Exception("Wrong key!", 400);
			}

			// Get Clients with onlinebooking settings that defined to send reminder mails
			$settings = $modelSettings::find("auto_confirm != '0' AND auto_confirm IS NOT NULL");
			if(!$settings || count($settings) === 0) throw new \Exception("No entries found", 404);

			foreach($settings as $s) {
				$diff_hours = $s->auto_confirm;
				$client_id = $s->client_id;
				$send_emp_mail = boolval($s->employee_notification);

				$search  = "client_id = :client_id:";
				$search .= " AND status IN (:s_ope:,:s_opa:)";

				$bindparams = array(
					"client_id" => $client_id,
					"s_ope" => $model::STATUS_OPEN,
					"s_opa" => $model::STATUS_OPEN_ASSIGNED,
				);

				$dt = "";
				if((int)$diff_hours > 0) {
					$now = new \DateTime('now');
					$searchtime->sub(new \DateInterval('PT'.$diff_hours.'H'));
					$dt = $searchtime->format('Y-m-d H:i:s');
					$search .= " AND bookedAt <= :bookdate:";
					$bindparams["bookdate"] = $dt;
				}

				$bookings = $model::find([
					"conditions" => $search,
					"bind"       => $bindparams,
				]);

				if(!$bookings || count($bookings) === 0) continue;

				foreach($bookings as $booking) {
					try {
						$this->approveOnlinebooking($booking->id, $send_emp_mail, $client_id);
					}
					catch(\Exception $e) {
						$subject = "Fehler beim Onlinebooking Auto-Bestätigen";
						$message = "Konnte das Onlinebooking mit der id: ".$booking->id." nicht automatisch bestätigen.
						Fehler: ".$e->getMessage()."
						Code: ".$e->getCode();
						if(!\Api\V1\Sys\Controllers\EmailsController::sendAdminMail($subject, $message)) error_log($message);
						continue;
					}
				}
			}

			$transaction->commit();
			return "ok";
			
		});
	}
}
