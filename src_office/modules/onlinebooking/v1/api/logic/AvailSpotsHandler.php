<?php

namespace Api\V1\OnlineBooking\Logic;

class AvailSpotsHandler {

	private $_spots = [];
	private $_days = [];
	private $_ressources = [];
	private $_timespots = [];
	private $_spotLength = 60; // Spot Length in minutes
	private $_period_before = 24 * 7; // Period before now in hours
	private $_period_after = 30 * 3; // Period after now in days
	private $_minDate;
	private $_maxDate;
	private $_now;
	private $_bookings = [];
	private $_availabilities = [];
	private $modelBookings = "Models\V1\Booking\Bookings";
	private $modelAvailabilities = "Models\V1\Booking\Availabilities";
	private $dateformat = "Y-m-d";

	function __construct($startDate, $endDate, $spotLength, $ressources, $period_before, $period_after)
	{
		$this->_availabilities = array();
		$this->_bookings = array();
		$this->setNow()
			 ->buildRessources($ressources)
			 ->setSpotLength($spotLength)
			 ->setPeriodBefore($period_before)
			 ->setPeriodAfter($period_after)
			 ->buildTimeSpots()
			 ->buildDays($startDate, $endDate)
			 ->buildEmptySpots();
	}

	private function setNow()
	{
		$now = new \DateTime();
		$this->_now = $now->format("Y-m-d H:i:s");
		return $this;
	}

	private function getNow()
	{
		return new \DateTime($this->_now);
	}

	private function isDateTimeAvailable(string $date, int $hour, int $minute)
	{
		$checkDate = new \DateTime($date);
		$checkDate->setTime($hour, $minute);
		$minDate = $this->getMinDate();
		$maxDate = $this->getMaxDate();

		if($minDate <= $checkDate && $maxDate >= $checkDate) {
			return true;
		}
		return false;
	}

	private function setPeriodBefore(int $period)
	{
		$this->_period_before = $period;
		$minDate = $this->getNow()->add($this->getPeriodBefore());
		$this->setMinDate($minDate);
		return $this;
	}

	private function getPeriodBefore()
	{
		$formatString = "PT" . $this->_period_before . "H";
		return new \DateInterval($formatString);
	}

	private function setPeriodAfter(int $period)
	{
		$this->_period_after = $period;
		$maxDate = $this->getNow()->add($this->getPeriodAfter());
		$this->setMaxDate($maxDate);
		return $this;
	}

	private function getPeriodAfter()
	{
		$formatString = "P" . $this->_period_after . "D";
		return new \DateInterval($formatString);
	}

	private function setMinDate($minDate)
	{
		$this->_minDate = $minDate;
		return $this;
	}

	private function getMinDate()
	{
		return $this->_minDate;
	}

	private function setMaxDate($maxDate)
	{
		$this->_maxDate = $maxDate;
		return $this;
	}

	private function getMaxDate()
	{
		return $this->_maxDate;
	}

	private function setSpotLength(int $spotLength)
	{
		$this->_spotLength = $spotLength;
		return $this;
	}

	private function getSpotLength()
	{
		return $this->_spotLength;
	}

	private function addRessource($title, $res)
	{
		$this->_ressources[$title] = $res;
	}

	private function getEmptyRessources()
	{
		$res = $this->_ressources;
		$ret_arr = array();

		foreach($res as $title => $ids) {
			$ret_arr[$title] = array();
			foreach($ids as $id) {
				$index = (int)$id;
				$ret_arr[$title][$index] = 0;
			}
		}

		return $ret_arr;
	}

	private function getRessources()
	{
		return $this->_ressources;
	}

	private function buildRessources($ressources)
	{
		$this->_ressources = array();

		foreach($ressources as $key => $val) {
			$this->addRessource($key, $val);
		}

		return $this;
	}

	private function buildTimeSpots()
	{
		$this->_timespots = array();
		$start = 0;
		$end = 24 * 60;
		$spot = $this->getSpotLength();

		for($step = $start; $step <= $end; $step += $spot) {
			$this->_timespots[] = $step;
		}

		return $this;
	}

	private function getEmptyTimeSpots()
	{
		$res = $this->_timespots;
		$ret_arr = array();

		foreach($res as $spot) {
			$ret_arr[$spot] = $this->getEmptyRessources();
		}

		return $ret_arr;
	}

	private function buildDays($startDate, $endDate)
	{
		$this->_spots = array();

		$dateInterval = new \DateInterval('P1D');
		$dateStart = new \DateTime($startDate);
		$dateEnd = new \DateTime($endDate);

		if($dateEnd < $dateStart) { $dateEnd = $dateStart; }

		$date = $dateStart;
		do {
			$this->_days[] = $date->format($this->dateformat);
			$date = $date->add($dateInterval);
		} while ($date <= $dateEnd);

		return $this;
	}

	private function buildEmptySpots()
	{
		$this->_spots = array();

		foreach($this->_days as $day) {
			$this->_spots[$day] = $this->getEmptyTimeSpots();
		}

		return $this;
	}

	public function getEmptySpots()
	{
		return $this->_spots;
	}

	public function addAvailable(&$spots, $date, $time, $res, $res_id)
	{
		if(!array_key_exists($date, $spots)) { return false; }
		if(!array_key_exists((int)$time, $spots[$date])) { return false; }
		if(!array_key_exists($res, $spots[$date][$time])) { return false; }
		if(!array_key_exists((int)$res_id, $spots[$date][$time][$res])) { return false; }

		$spots[$date][$time][$res][$res_id]++;
	}

	public function removeAvailable(&$spots, $date, $time, $res, $res_id)
	{
		if(!array_key_exists($date, $spots)) { return false; }
		if(!array_key_exists((int)$time, $spots[$date])) { return false; }
		if(!array_key_exists($res, $spots[$date][$time])) { return false; }
		if(!array_key_exists((int)$res_id, $spots[$date][$time][$res])) { return false; }

		$spots[$date][$time][$res][$res_id]--;
	}

	public function addBooking($booking)
	{
		$this->_bookings[] = $booking;
	}

	private function getBookings()
	{
		return $this->_bookings;
	}

	private function blockBooking(&$spots, $booking)
	{
		$date = new \DateTime($booking->date);
		$date_str = $date->format($this->dateformat);

		$start = $booking->startTime;
		$end = $booking->endTime;

		$blocktimes = $this->getAffectedTimespotsBlocking($start, $end);

		if(empty($blocktimes)) { return; }
		if(empty($booking->room_id) && empty($booking->employee_id)) { return; }

		$employee = empty($booking->employee_id) ? 0 : (int)$booking->employee_id;
		$room = empty($booking->room_id) ? 0 : (int)$booking->room_id;

		foreach($blocktimes as $time) {
			if($room != 0) $this->removeAvailable($spots, $date_str, $time, "rooms", $room);
			if($employee != 0) $this->removeAvailable($spots, $date_str, $time, "employees", $employee);
		}
	}

	public function addAvailability($availability)
	{
		$this->_availabilities[] = $availability;
	}

	private function getAvailabilities()
	{
		return $this->_availabilities;
	}

	private function handleAvailability(&$spots, $availability, $date = null) {
		$modelAvailabilities = $this->modelAvailabilities;

		if(empty($availability->type)) { return; }
		switch($availability->type) {
			case $modelAvailabilities::TYPES['LOCATION_GENERAL']:
			case $modelAvailabilities::TYPES['EMPLOYEE_GENERAL']:
				if(empty($date)) { return; }
				$availability->date = $date;
				break;
			default:
				break;
		}

		$date = new \DateTime($availability->date);
		$day = $date->format($this->dateformat);

		if(!array_key_exists($day, $spots)) { return; }
		if(isset($availability->wholeday) && $availability->wholeday) {
			foreach($spots[$day] as $time => $res) {
				switch($availability->type) {
					case $modelAvailabilities::TYPES['LOCATION_CUSTOM']:
						foreach($spots[$day][$time]['rooms'] as $room => $val) {
							if($availability->available) {
								$this->addAvailable($spots, $day, $time, "rooms", $room);
							} else {
								$this->removeAvailable($spots, $day, $time, "rooms", $room);
							}
						}
						break;
					case $modelAvailabilities::TYPES['EMPLOYEE_CUSTOM']:
						if(empty($availability->employee_id)) { return; }
						if($availability->available && $availability->online_booking_available) {
							$this->addAvailable($spots, $day, $time, "employees", $availability->employee_id);
						} else {
							$this->removeAvailable($spots, $day, $time, "employees", $availability->employee_id);
						}
						break;
					default:
						break;
				}
			}
		} else {
			if(!isset($availability->start) || !isset($availability->end)) { return; }
			$start = $availability->start;
			$end = $availability->end;
			
			if($availability->available) {
				$affected_spots = $this->getAffectedTimespotsAvailable($start, $end);
			} else {
				$affected_spots = $this->getAffectedTimespotsBlocking($start, $end);
			}

			switch($availability->type) {
				case $modelAvailabilities::TYPES['LOCATION_CUSTOM']:
				case $modelAvailabilities::TYPES['LOCATION_GENERAL']:
					foreach($affected_spots as $time) {
						if(array_key_exists('rooms', $spots[$day][$time])) {
							foreach($spots[$day][$time]['rooms'] as $room => $val) {
								if($availability->available) {
									$this->addAvailable($spots, $day, $time, "rooms", $room);
								} else {
									$this->removeAvailable($spots, $day, $time, "rooms", $room);
								}
							}
						}
					}
					break;
				case $modelAvailabilities::TYPES['EMPLOYEE_CUSTOM']:
				case $modelAvailabilities::TYPES['EMPLOYEE_GENERAL']:
					if(empty($availability->employee_id)) { return; }
					foreach($affected_spots as $time) {
						if($availability->available && $availability->online_booking_available) {
							$this->addAvailable($spots, $day, $time, "employees", $availability->employee_id);
						} else {
							$this->removeAvailable($spots, $day, $time, "employees", $availability->employee_id);
						}
					}
					break;
				default:
					break;
			}
		}
	}

	public function getOpenDays(int $duration)
	{
		$spots = $this->getSpots();
		$retArr = array();

		foreach($spots as $day => $times) {
			$date = new \DateTime($day);
			$d = array();
			$d['available'] = $this->hasOpenSpots($spots, $day, $duration);
			$d['date'] = $day;
			$d['value'] = (int)$date->format("d");
			$retArr[] = $d;
		}

		return $retArr;
	}

	public function getOpenTimeslots(string $date, int $duration)
	{
		$day = new \DateTime($date);
		$index = $day->format($this->dateformat);
		$spots = $this->getSpots();
		$retArr = array();

		if(!array_key_exists($index, $spots)) { return $retArr; }

		foreach($spots[$index] as $time => $res) {
			$h = (int)round(floor($time / 60), 0);
			$m = (int)($time % 60);

			$d = array();
			$d['available'] = $this->isOpen($spots, $date, $time, $duration);
			$d['date'] = $index;
			$d['time'] = sprintf('%02d', $h).":".sprintf('%02d', $m);
			$retArr[] = $d;

		}

		return $retArr;
	}

	public function timeSlotInfo(string $date, string $startTime, int $duration) {
		$day = new \DateTime($date);
		$index = $day->format($this->dateformat);

		$t = strtotime($startTime);
		$h = (int)date("G", $t);
		$m = (int)date("i", $t);
		$time_id = $h * 60 + $m;

		$spots = $this->getSpots();

		$retArr = array();	
		if(!array_key_exists($index, $spots)) { return $retArr; }

		$retArr['available'] = $this->isOpen($spots, $date, $time_id, $duration);
		$retArr['date'] = $date;
		$retArr['time'] = sprintf('%02d', $h).":".sprintf('%02d', $m);
		$retArr['duration'] = $duration;

		$ava_res = array();
		$ressources = $this->getRessources();
		foreach($ressources as $res => $ids) {
			$ava_res[$res] = array();
			foreach($ids as $id) {
				if($this->isAvailable($spots, $index, $time_id, $duration, $res, $id)) {
					$ava_res[$res][] = $id;
				}
			}
		}
		$retArr['ressources'] = $ava_res;
		
		return $retArr;
	}

	public function hasOpenSpots(array $spots, string $date, int $duration)
	{
		$day = new \DateTime($date);
		$index = $day->format($this->dateformat);

		if(!array_key_exists($index, $spots)) { return false; }
		foreach($spots[$index] as $time => $res) {
			if($this->isOpen($spots, $index, $time, $duration)) {
				return true;
			}
		}
		return false;
	}

	public function isOpen(array $spots, string $date, $time, int $duration)
	{
		if(is_string($time)) {
			$t = strtotime($time);
			$h = (int)date("G", $t);
			$m = (int)date("i", $t);
			$time_id = $h * 60 + $m;
		} elseif (is_int($time)) {
			$time_id = $time;
			$h = (int)round(floor($time / 60), 0);
			$m = (int)($time % 60);
		} else { return false; }

		$day = new \DateTime($date);
		$index = $day->format($this->dateformat);

		// Is the Time in between the available defined Time
		if(!$this->isDateTimeAvailable($index, $h, $m)) { return false; }

		// $neededTimes = $this->getAffectedTimespotsBlocking($time_id, $time_id_to);
		$ressources = $this->getRessources();

		foreach($ressources as $res => $ids) {
			$available = false;
			foreach($ids as $id) {
				if($this->isAvailable($spots, $index, $time_id, $duration, $res, $id)) {
					$available = true;
					break;
				}
			}
			if(!$available) {
				return false;
			}
		}

		return true;
	}

	private function isAvailable(array $spots, string $date, int $time_id, int $duration, string $ressource, $id) {
		if(!array_key_exists($date, $spots)) { return false; }
		if(!array_key_exists($time_id, $spots[$date])) { return false; }
		
		$neededTimes = $this->getAffectedTimespotsBlocking($time_id, $time_id + $duration);

		foreach($neededTimes as $time) {
			if(!array_key_exists($ressource, $spots[$date][$time])) { return false; }
			if(!array_key_exists($id, $spots[$date][$time][$ressource])) { return false; }
			if($spots[$date][$time][$ressource][$id] <= 0) { return false; }
		}

		return true;
	}

	private function getAffectedTimespotsBlocking($timeFrom, $timeTo)
	{
		$retArr = array();
		$interval = $this->getSpotLength();

		if(is_string($timeFrom)) {
			$t_from = strtotime($timeFrom);
			$h_from = (int)date("G", $t_from);
			$m_from = (int)date("i", $t_from);
			$minutes_from = $h_from * 60 + $m_from;
		} elseif (is_int($timeFrom)) {
			$minutes_from = $timeFrom;
		} else { return array(); }
		$index_from = $minutes_from - ($minutes_from % $interval);

		if(is_string($timeTo)) {
			$t_to = strtotime($timeTo);
			$h_to = (int)date("G", $t_to);
			$m_to = (int)date("i", $t_to);
			$minutes_to = $h_to * 60 + $m_to;
		} elseif (is_int($timeTo)) {
			$minutes_to = $timeTo;
		} else { return array(); }
		$index_to = (int)round(ceil($minutes_to / $interval) * $interval - $interval, 0);

		for($index = $index_from; $index <= $index_to; $index += $interval) {
			$retArr[] = $index;
		}

		return $retArr;
	}

	private function getAffectedTimespotsAvailable($timeFrom, $timeTo)
	{
		$retArr = array();

		$interval = $this->getSpotLength();

		if(is_string($timeFrom)) {
			$t_from = strtotime($timeFrom);
			$h_from = (int)date("G", $t_from);
			$m_from = (int)date("i", $t_from);
			$minutes_from = $h_from * 60 + $m_from;
		} elseif (is_int($timeFrom)) {
			$minutes_from = $timeFrom;
		} else { return array(); }
		$index_from = (int)round(ceil($minutes_from / $interval) * $interval, 0);

		if(is_string($timeTo)) {
			$t_to = strtotime($timeTo);
			$h_to = (int)date("G", $t_to);
			$m_to = (int)date("i", $t_to);
			$minutes_to = $h_to * 60 + $m_to;
		} elseif (is_int($timeTo)) {
			$minutes_to = $timeTo;
		} else { return array(); }
		$index_to = $minutes_to - ($minutes_to % $interval) - $interval;

		for($index = $index_from; $index <= $index_to; $index += $interval) {
			$retArr[] = $index;
		}

		return $retArr;
	}

	private function getSpots()
	{
		$spots = $this->getEmptySpots();

		$bookings = $this->getBookings();
		foreach($bookings as $booking) {
			$this->blockBooking($spots, $booking);
		}

		$employees = empty($this->_ressources['employees']) ? [] : $this->_ressources['employees'];

		foreach($spots as $date => $times) {
			$availLoc = $this->getAvailabilitiesLocation($date);
			foreach($availLoc as $a) {
				$this->handleAvailability($spots, $a, $date);
			}
			foreach($employees as $emp_id) {
				$availEmp = $this->getAvailabilitiesEmployee($date, $emp_id);
				foreach($availEmp as $a) {
					$this->handleAvailability($spots, $a, $date);
				}
			}
		}

		return $spots;
	}

	private function getAvailabilitiesLocation(string $date)
	{
		$availabilities = $this->getCustomAvailabilitiesLocation($date);
		if(count($availabilities) == 0) {
			$availabilities = $this->getGeneralAvailabilitiesLocation($date);
		}
		return $availabilities;
	}

	private function getGeneralAvailabilitiesLocation(string $date)
	{
		$avail = $this->getAvailabilities();
		$model = $this->modelAvailabilities;

		$ret = array();
		foreach($avail as $a) {
			if($a->type == $model::TYPES['LOCATION_GENERAL']) {
				$d = new \DateTime($date);
				$day = (int)$d->format("w");
				if((int)$a->weekday == $day) {
					$ret[] = $a;
				}
			}
		}
		return $ret;
	}

	private function getCustomAvailabilitiesLocation(string $date)
	{
		$avail = $this->getAvailabilities();
		$model = $this->modelAvailabilities;

		$ret = array();
		foreach($avail as $a) {
			if($a->type == $model::TYPES['LOCATION_CUSTOM']) {
				$d = new \DateTime($a->date);
				$day = $d->format($this->dateformat);
				if($date == $day) {
					$ret[] = $a;
				}
			}
		}
		return $ret;
	}

	private function getAvailabilitiesEmployee(string $date, int $id)
	{
		$availabilities = $this->getCustomAvailabilitiesEmployee($date, $id);
		if(count($availabilities) == 0) {
			$availabilities = $this->getGeneralAvailabilitiesEmployee($date, $id);
		}
		return $availabilities;
	}

	private function getGeneralAvailabilitiesEmployee(string $date, int $id)
	{
		$avail = $this->getAvailabilities();
		$model = $this->modelAvailabilities;

		$ret = array();
		foreach($avail as $a) {
			if($a->type == $model::TYPES['EMPLOYEE_GENERAL'] && $id == (int)$a->employee_id) {
				$d = new \DateTime($date);
				$day = (int)$d->format("w");
				if((int)$a->weekday == $day) {
					$ret[] = $a;
				}
			}
		}
		return $ret;
	}

	private function getCustomAvailabilitiesEmployee(string $date, int $id)
	{
		$avail = $this->getAvailabilities();
		$model = $this->modelAvailabilities;

		$ret = array();
		foreach($avail as $a) {
			if($a->type == $model::TYPES['EMPLOYEE_CUSTOM'] && $id == (int)$a->employee_id) {
				$d = new \DateTime($a->date);
				$day = $d->format($this->dateformat);
				if($date == $day) {
					$ret[] = $a;
				}
			}
		}
		return $ret;
	}
	
	

}
