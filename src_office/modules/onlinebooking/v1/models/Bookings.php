<?php

namespace Models\V1\Onlinebooking;

class Bookings extends \Phalcon\Mvc\Model
{

	public $id;
	public $bookedAt;
	public $firstname;
	public $name;
	public $email;
	public $phone;
	public $comment;
	public $title;
	public $date;
	public $startTime;
	public $endTime;
	public $duration;
	public $breakAfter;
	public $price;
	public $status;
	public $cancelledAt;
	public $cancel_reason;
	public $product_id;
	public $employee_id;
	public $room_id;
	public $booking_id;
	public $client_id;

	public static $alias_one = "onlinebooking";
	public static $alias_many = "onlinebookings";

	// Status constants:
	const STATUS_OPEN = 'ope';
	const STATUS_OPEN_ASSIGNED = 'opa';
	const STATUS_CANCELLED = 'can';
	const STATUS_CANCELLED_ASSIGNED = 'caa';
	const STATUS_APPROVED = 'app';
	const STATUS_APPROVED_ASSIGNED = 'apa';

	const CANCEL_REASONS = array(
		'NOT_APPROVED'		=> 2,
		'CUSTOMER'			=> 3,
		'OTHER'				=> 1,
	);

	public static $attrs = array(
		"id" => "int",
		"bookedAt" => "timestamp",
		"firstname" => "string",
		"name" => "string",
		"email" => "string",
		"phone" => "string",
		"comment" => "string",
		"title" => "string",
		"date" => "date",
		"startTime" => "time",
		"endTime" => "time",
		"duration" => "int",
		"breakAfter" => "int",
		"price" => "decimal",
		"status" => "string",
		"cancelledAt" => "timestamp",
		"cancel_reason" => "int",
		"product_id" => "int",		
		"employee_id" => "int",
		"room_id" => "int",
		"booking_id" => "int",
		"client_id" => "int",
	);

	public static $relations = array(
		"employee" => [
			"type" => "belongsTo",
			"alias" => "employee",
			"relationModel" => "Models\V1\Org\Employees",
			"relationIdAlias" => "employee_id"
		],
		"product" => [
			"type" => "belongsTo",
			"alias" => "product",
			"relationModel" => "Models\V1\Prod\Products",
			"relationIdAlias" => "product_id"
		],
		"room" => [
			"type" => "belongsTo",
			"alias" => "room",
			"relationModel" => "Models\V1\Loc\Rooms",
			"relationIdAlias" => "room_id"
		],
		"booking" => [
			"type" => "belongsTo",
			"alias" => "booking",
			"relationModel" => "Models\V1\Booking\Bookings",
			"relationIdAlias" => "booking_id"
		],
	);

	/**
	 * Independent Column Mapping.
	 */
	public function columnMap()
	{
		return array(
			"id" => "id",
			"bookedAt" => "bookedAt",
			"firstname" => "firstname",
			"name" => "name",
			"email" => "email",
			"phone" => "phone",
			"comment" => "comment",
			"title" => "title",
			"date" => "date",
			"startTime" => "startTime",
			"endTime" => "endTime",
			"duration" => "duration",
			"breakAfter" => "breakAfter",
			"price" => "price",
			"status" => "status",
			"cancelledAt" => "cancelledAt",
			"cancel_reason" => "cancel_reason",
			"product_id" => "product_id",		
			"employee_id" => "employee_id",
			"room_id" => "room_id",
			"booking_id" => "booking_id",
			"client_id" => "client_id",
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_booking_onlinebookings");

		$this->belongsTo(
			"product_id",
			"Models\V1\Prod\Products",
			"id",
			[
				"alias" => "product",
			]
		);

		$this->belongsTo(
			"room_id",
			"Models\V1\Loc\Rooms",
			"id",
			[
				"alias" => "room",
			]
		);

		$this->belongsTo(
			"employee_id",
			"Models\V1\Org\Employees",
			"id",
			[
				"alias" => "employee",
			]
		);

		$this->belongsTo(
			"booking_id",
			"Models\V1\Booking\Bookings",
			"id",
			[
				"alias" => "booking",
			]
		);
	}

	/* -------------------------------------------------------------------------
	 * Action to open a new booking and set its status
	 */
	public function open()
	{
		if(empty($this->status)) {
			$this->status = self::STATUS_OPEN;
			return $this->update();
		} else {
			return false;
		}
	}

	/* -------------------------------------------------------------------------
	 * Action to cancel this booking and set status depending on actual status
	 */
	public function cancel($reason, $date = null)
	{
		if($date === null) { $date = time(); }
		if(!in_array($reason, self::CANCEL_REASONS, true)) return false;
		if($this->status === self::STATUS_OPEN) {
			$this->status = self::STATUS_CANCELLED;
			$this->cancel_reason = $reason;
			$this->cancelledAt = date('Y-m-d H:i:s', $date);
			return $this->update();
		} else if ($this->status === self::STATUS_OPEN_ASSIGNED) {
			$this->status = self::STATUS_CANCELLED_ASSIGNED;
			$this->cancel_reason = $reason;
			$this->cancelledAt = date('Y-m-d H:i:s', $date);
			return $this->update();
		} else {
			return false;
		}
	}

	/* -------------------------------------------------------------------------
	 * Action to cancel/rescind an already invoiced booking and set status depending on actual status
	 */
	public function approve()
	{
		if($this->status === self::STATUS_OPEN) {
			$this->status = self::STATUS_APPROVED;
			return $this->update();
		} else if ($this->status === self::STATUS_OPEN_ASSIGNED) {
			$this->status = self::STATUS_APPROVED_ASSIGNED;
			return $this->update();
		} else {
			return false;
		}
	}

	/* -------------------------------------------------------------------------
	 * Set Status of Booking to assigned
	 */
	public function assignCustomer()
	{
		if ($this->status === self::STATUS_OPEN) {
			$this->status = self::STATUS_OPEN_ASSIGNED;
			return $this->update();
		} else if ($this->status === self::STATUS_CANCELLED) {
			$this->status = self::STATUS_CANCELLED_ASSIGNED;
			return $this->update();
		} else if ($this->status === self::STATUS_APPROVED) {
			$this->status = self::STATUS_APPROVED_ASSIGNED;
			return $this->update();
		} else if ($this->isAssignedToCustomer()){
			return true;
		} else {
			return false;
		}
	}

	public function isApproved()
	{
		return ($this->status === self::STATUS_APPROVED || $this->status === self::STATUS_APPROVED_ASSIGNED);
	}

	public function isCancelled()
	{
		return ($this->status === self::STATUS_CANCELLED || $this->status === self::STATUS_CANCELLED_ASSIGNED);
	}

	public function isOpen()
	{
		return ($this->status === self::STATUS_OPEN || $this->status === self::STATUS_OPEN_ASSIGNED);
	}

	public function cancellable()
	{
		return !($this->isCancelled());
	}

	public function isAssignedToCustomer()
	{
		return ($this->status === self::STATUS_OPEN_ASSIGNED || $this->status === self::STATUS_CANCELLED_ASSIGNED || $this->status === self::STATUS_APPROVED_ASSIGNED);
	}

	public function getCustomer()
	{
		if(!empty($this->booking) && !empty($this->booking->customer)) {
			return $this->booking->customer;
		}
		return null;
	}

	public function getLocation()
	{
		if(!empty($this->room) && !empty($this->room->location)) {
			return $this->room->location;
		}
		return null;
	}

	public function jsonArray()
	{
		$retArr = $this->toArray();
		foreach(self::$relations as $relation) {
			$alias = $relation["alias"];
			$retArr[$alias] = null;
			if(!empty($this->$alias)) {
				$rel = $this->$alias->toArray();
				$retArr[$alias] = $rel;
			}
		}

		$retArr["customer"] = $this->getCustomer();
		$retArr["location"] = $this->getLocation();

		return $retArr;
	}

	// Returns the Unix Timestamp out of the Date and StartTime
	public function getTimestamp()
	{
		$date = date('Y-m-d', strtotime($this->date));
		$time = date('H:i:s', strtotime($this->startTime));
		return strtotime($date . " " . $time);
	}

}
