<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$api = new RouterGroup(array('module' => 'api_v1_booking'));
$api->setPrefix('/api/v1/booking');

$api->addGet	("/bookings", array('controller' => 'Bookings', 'action' => 'index'));
$api->addGet	("/bookings/:int", array('controller' => 'Bookings', 'action' => 'one', 'id' => 1));
$api->addPost	("/bookings", array('controller' => 'Bookings', 'action' => 'create'));
$api->addPut	("/bookings/:int", array('controller' => 'Bookings', 'action' => 'update', 'id' => 1));
$api->addDelete	("/bookings/:int", array('controller' => 'Bookings', 'action' => 'delete', 'id' => 1));
$api->addGet	("/bookings/invoiceable", array('controller' => 'Bookings', 'action' => 'getInvoiceable'));
$api->addGet	("/bookings/invoiceable/tomtod", 								array(
					'controller' => 'Bookings',
					'action' => 'indexInvoiceableTomtod'
				));
$api->addGet	("/bookings/invoiceable/tomtod/([a-zA-ZöäüÖÄÜ0-9_/\.\-]+)",		array(
					'controller' => 'Bookings',
					'action' => 'indexInvoiceableTomtod',
					'date' => 1
				));
$api->addGet	("/bookings/customerlist/:int[/]?([a-zA-ZöäüÖÄÜ0-9_/\.\-]+)?", array('controller' => 'Bookings', 'action' => 'getCustomerlist', 'id' => 1, 'date' => 2));

// Special Index Action for calendar events
$api->addGet	("/calendarevents", array('controller' => 'Bookings', 'action' => 'indexCalendarevents'));
$api->addGet	("/calendarevents/:int", array('controller' => 'Bookings', 'action' => 'indexCalendarevents', 'employee' => 1));
$api->addGet	("/calendarevents/([0-9]{4}-[0-9]{2}-[0-9]{2})/([0-9]{4}-[0-9]{2}-[0-9]{2})", array(
					'controller'	=> 'Bookings',
					'action'		=> 'indexCalendareventsTimespot',
					'startDay'		=> 1,
					'endDay'		=> 2
));
/*
$api->addGet	("/calendarevents/([0-9]{4}-[0-9]{2}-[0-9]{2})/([0-9]{4}-[0-9]{2}-[0-9]{2})/:int", array(
					'controller'	=> 'Bookings',
					'action'		=> 'indexCalendareventsTimespot',
					'startDay'		=> 1,
					'endDay'		=> 2,
					'employee'		=> 3
				));
*/
$api->addGet	("/calendarsettings", array('controller' => 'Bookings', 'action' => 'calendarSettings'));

// Actions for Bookings:
$api->addPut	("/bookings/actions/cancel/:int", array('controller' => 'Bookings', 'action' => 'cancel', 'id' => 1));
$api->addPut	("/bookings/actions/notinvoice/:int", array('controller' => 'Bookings', 'action' => 'notinvoice', 'id' => 1));

$api->addGet	("/availabilities", array('controller' => 'Availabilities', 'action' => 'index'));
$api->addGet	("/availabilities/:int", array('controller' => 'Availabilities', 'action' => 'one', 'id' => 1));
$api->addPost	("/availabilities", array('controller' => 'Availabilities', 'action' => 'create'));
$api->addPut	("/availabilities/:int", array('controller' => 'Availabilities', 'action' => 'update', 'id' => 1));
$api->addDelete	("/availabilities/:int", array('controller' => 'Availabilities', 'action' => 'delete', 'id' => 1));

$router->mount($api);
