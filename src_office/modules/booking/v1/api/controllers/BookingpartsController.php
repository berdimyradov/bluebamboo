<?php

namespace Api\V1\Booking\Controllers;

class BookingpartsController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Booking\Bookingparts";

	/* -----------------------------------------------------------------------------------
	 * index action
	 */
	public function indexAction () {
		// site id or similar filter? just about access rights
		$default_projection     = array("o.id", "o.title", "o.tariff", "o.price", "o.duration", "o.mwst_group", "o.booking_id", "o.tariffposition_id");
		$orderby                = array("o.date", "o.person_id");
		$attrs_for_projection	= array("o.id", "o.title", "o.tariff", "o.price", "o.duration", "o.mwst_group", "o.booking_id", "o.tariffposition_id");
		$attrs_for_where        = array("o.id", "o.title", "o.tariff", "o.price", "o.duration", "o.mwst_group", "o.booking_id", "o.tariffposition_id");
		$attrs_for_orderby      = array("o.id", "o.title", "o.tariff", "o.price", "o.duration", "o.mwst_group", "o.booking_id", "o.tariffposition_id");

//		$this->obj_acl = "Models\V1\CMS\SitesACL";
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * index lookup action
	 */
	public function indexLookupAction () {
		$projection			= array("o.id", "o.title");
		$lookup_fields			= array("title");
		$orderby			= array("o.booking_id", "o.title", "o.tariffposition_id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->title = "";
			$l->title .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/* -----------------------------------------------------------------------------------
	 * one action
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneAction($model, $id, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * create action
	 */
	public function createAction() {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);
		$model = $this->from;
		$res = $this->_createAction($model, $model::$attrs);
		return $res;
	}

	/* -----------------------------------------------------------------------------------
	 * update action
	 */
	public function updateAction($id) {
		$model = $this->from;
		return $this->_updateAction($model, $model::$attrs, $id);
	}

	/* -----------------------------------------------------------------------------------
	 * delete action
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

}
