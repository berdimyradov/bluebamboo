<?php

namespace Api\V1\Booking\Controllers;

class BookingsController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Booking\Bookings";

	/* -----------------------------------------------------------------------------------
	 * index action
	 */
	public function indexAction () {

		// site id or similar filter? just about access rights
		$default_projection     = array("o.id", "o.bookedAt", "o.type", "o.employee_id", "o.product_id", "o.customer_id", "o.room_id", "o.location_title", "o.price", "o.discount", "o.date", "o.startTime", "o.endTime", "o.duration", "o.breakAfter", "o.title", "o.comment", "o.invoice_id", "o.case_id", "o.report_id", "o.cancelledAt", "o.cancel_reason", "o.status", "o.client_id");
		$orderby                = array("o.title");
		$attrs_for_projection	= array("o.id", "o.bookedAt", "o.type", "o.employee_id", "o.product_id", "o.customer_id", "o.room_id", "o.location_title", "o.price", "o.discount", "o.date", "o.startTime", "o.endTime", "o.duration", "o.breakAfter", "o.title", "o.comment", "o.invoice_id", "o.case_id", "o.report_id", "o.cancelledAt", "o.cancel_reason", "o.status", "o.client_id");
		$attrs_for_where        = array("o.id", "o.bookedAt", "o.type", "o.employee_id", "o.product_id", "o.customer_id", "o.room_id", "o.location_title", "o.price", "o.discount", "o.date", "o.startTime", "o.endTime", "o.duration", "o.breakAfter", "o.title", "o.invoice_id", "o.case_id", "o.report_id", "o.cancelledAt", "o.cancel_reason", "o.status", "o.client_id");
		$attrs_for_orderby      = array("o.id", "o.bookedAt", "o.type", "o.employee_id", "o.product_id", "o.customer_id", "o.room_id", "o.location_title", "o.price", "o.discount", "o.date", "o.startTime", "o.endTime", "o.duration", "o.breakAfter", "o.title", "o.invoice_id", "o.case_id", "o.report_id", "o.cancelledAt", "o.cancel_reason", "o.status", "o.client_id");

//		$this->obj_acl = "Models\V1\CMS\SitesACL";

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * index lookup action
	 */
	public function indexLookupAction () {
		$projection			= array("o.id", "o.date", "o.startTime", "o.bookedAt");
		$lookup_fields			= array("bookedAt");
		$orderby			= array("o.date", "o.startTime", "o.bookedAt", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->title = "";
			$l->title .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/* -----------------------------------------------------------------------------------
	 * index all events for the calendar
	 */
	public function indexCalendareventsAction($employee = 0)
	{
		return $this->jsonReadRequest(function(&$transaction) use ($employee) {

			$model = $this->from;
			$modelEmp = "Models\V1\Org\Employees";
			$modelCust = "Models\V1\Cust\Customers";
			$modelProd = "Models\V1\Prod\Products";

			$client_id = $this->grantAccessForClient();

			$employeeCondition = "(booking.employee_id IS NULL OR booking.employee_id > 0)";
			if($employee != 0) {
				$employeeCondition = "booking.employee_id = " . $employee;
			}

			$rows = $this->modelsManager->createBuilder()
				->columns('booking.*, customer.*, employee.*, product.color as color')
				->from(['booking' => $model,])
				->leftJoin($modelCust, "customer.id = booking.customer_id", "customer")
				->leftJoin($modelEmp, "employee.id = booking.employee_id", "employee")
				->leftJoin($modelProd, "product.id = booking.product_id", "product")
				->where("booking.client_id = :cid:", ["cid" => $client_id])
				->andWhere("booking.status IN (:status_ope:,:status_nti:,:status_inv:,:status_res:,:status_sop:)", [
					'status_ope' => $model::STATUS_OPEN,
					'status_nti' => $model::STATUS_NOT_TO_INVOICE,
					'status_inv' => $model::STATUS_INVOICED,
					'status_res' => $model::STATUS_RESCINDED,
					'status_sop' => $model::STATUS_RESCINDED_OPEN,
				])
				->andWhere($employeeCondition)
				->orderBy('booking.date, booking.startTime')
				->getQuery()
				->execute();

			// Create custom structure with bookings associated to customers
			$retArr = array();
			foreach($rows as $row) {
				$booking = $this->_valueConversionPostload($row->booking, $model::$attrs)->toArray();
				$booking['customer'] = $row->customer->id == NULL ? NULL : $this->_valueConversionPostload($row->customer, $modelCust::$attrs);
				$booking['employee'] = $row->employee->id == NULL ? NULL : $this->_valueConversionPostload($row->employee, $modelEmp::$attrs);
				$booking['color'] = empty($row->color) ? NULL : $row->color;
				$retArr[] = $booking;
			}

			return $retArr;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * index all events for the calendar
	 */
	public function indexCalendareventsTimespotAction($startDay, $endDay)
	{
		return $this->jsonReadRequest(function(&$transaction) use ($startDay, $endDay) {

			$transaction = $this->transaction;
			$model = $this->from;
			
			$modelEmp	= "Models\V1\Org\Employees";
			$modelCust	= "Models\V1\Cust\Customers";
			$modelProd	= "Models\V1\Prod\Products";
			$modelOnl	= "Models\V1\Onlinebooking\Bookings";
			$client_id	= $this->grantAccessForClient();

			$hasEmployees = false;
			if ($this->request->has("employees")) {
				$hasEmployees = true;
				$employees = explode(",", $this->request->getQuery("employees"));
				if(count($employees) === 0 || (count($employees) === 1 && empty($employees[0]))) $hasEmployees = false;
				$emp_condition = "(booking.employee_id IS NULL";
				foreach($employees as $e) {
					$emp_condition .= " OR ";
					$emp_condition .= "booking.employee_id = '" . $e . "'";
				}
				$emp_condition .= ")";
			}
			
			$hasRooms = false;
			if ($this->request->has("rooms")) {
				$hasRooms = true;
				$rooms = explode(",", $this->request->getQuery("rooms"));
				if(count($rooms) === 0 || (count($rooms) === 1 && empty($rooms[0]))) $hasRooms = false;
				$room_condition = "(booking.room_id IS NULL";
				foreach($rooms as $r) {
					$room_condition .= " OR ";
					$room_condition .= "booking.room_id = '" . $r . "'";
				}
				$room_condition .= ")";
			}

			if(!$hasEmployees && !$hasRooms) {
				$emp_room_condition = "(booking.employee_id IS NULL OR booking.employee_id > 0)";
			} else if ($hasEmployees && !$hasRooms) {
				$emp_room_condition = $emp_condition;
			} else if (!$hasEmployees && $hasRooms) {
				$emp_room_condition = $room_condition;
			} else {
				$emp_room_condition = "(" . $emp_condition . " OR " . $room_condition . ")";
			}

			$rows = $this->modelsManager->createBuilder()
				// ->columns('booking.*, customer.*, employee.*, product.color as color')
				->columns('booking.*, customer.*, employee.active as employee_active, obooking.name as obooking_name, obooking.firstname as obooking_firstname, product.color as color')
				->from(['booking' => $model,])
				->leftJoin($modelCust, "customer.id = booking.customer_id", "customer")
				->leftJoin($modelEmp, "employee.id = booking.employee_id", "employee")
				->leftJoin($modelOnl, "obooking.booking_id = booking.id", "obooking")
				->leftJoin($modelProd, "product.id = booking.product_id", "product")
				->where("booking.client_id = :cid:", ["cid" => $client_id])
				->andWhere("booking.status IN (:status_ope:,:status_nti:,:status_inv:,:status_res:,:status_sop:)", [
					'status_ope' => $model::STATUS_OPEN,
					'status_nti' => $model::STATUS_NOT_TO_INVOICE,
					'status_inv' => $model::STATUS_INVOICED,
					'status_res' => $model::STATUS_RESCINDED,
					'status_sop' => $model::STATUS_RESCINDED_OPEN,
				])
				->andWhere($emp_room_condition)
				->betweenWhere("booking.date", $startDay, $endDay)
				->orderBy('booking.date, booking.startTime')
				->getQuery()
				->execute();

			// Create custom structure with bookings associated to customers
			$retArr = array();
			foreach($rows as $row) {
				// $b = $this->_valueConversionPostload($row->booking, $model::$attrs);
				if(empty($row->employee_active) && !empty($row->booking->employee_id)) continue;
				$booking = array(
					"id" 		=> (int)$row->booking->id,
					"type"		=> (int)$row->booking->type,
					"title"		=> $row->booking->title,
					"date"		=> date('c', strtotime($row->booking->date)),
					"startTime"	=> date('c', strtotime($row->booking->startTime, 0)),
					"endTime"	=> date('c', strtotime($row->booking->endTime, 0)),
					"status"	=> $row->booking->status,
				);
				if((int)$row->booking->type === 300 && empty($row->booking->customer_id)) {
					$booking['customer'] = array(
						"name" 			=> $row->obooking_name,
						"firstname"		=> $row->obooking_firstname,
					);
				} else {
					$booking['customer'] = empty($row->customer->id) ? NULL : array(
						"name" 			=> $row->customer->name,
						"firstname"		=> $row->customer->firstname,
					);
				}
				// $booking['employee'] = $row->employee->id == NULL ? NULL : $this->_valueConversionPostload($row->employee, $modelEmp::$attrs);
				$booking['color'] = empty($row->color) ? NULL : $row->color;
				$retArr[] = $booking;
			}

			return $retArr;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * Get Calendar Settings
	 */
	public function calendarSettingsAction()
	{
		return $this->jsonReadRequest(function(&$transaction) {

			$model		= $this->from;
			$modelEmp	= "Models\V1\Org\Employees";
			$modelRoom	= "Models\V1\Loc\Rooms";
			
			$client_id	= $this->grantAccessForClient();

			$retArr = array(
				"employeeCount" => 0,
				"employees" => array(),
				"roomCount" => 0,
				"rooms" => array()
			);

			$employees = $modelEmp::find("client_id = '".$client_id."' AND active = '1'");

			foreach($employees as $e) {
				$employee = array(
					"id" 		=> (int)$e->id,
					"firstname"	=> $e->firstname,
					"name"		=> $e->name,
					"show"		=> true
				);
				$retArr['employees'][] = $employee;
				$retArr['employeeCount']++;
			}

			$roomobjs = $modelRoom::find("client_id = '".$client_id."'");

			foreach($roomobjs as $r) {
				$room = array(
					"id" 		=> (int)$r->id,
					"title"		=> $r->title,
					"show"		=> false
				);
				$retArr['rooms'][] = $room;
				$retArr['roomCount']++;
			}

			return $retArr;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * get Action for open invoiceable Bookings in desired structure
	 */
	public function getInvoiceableAction() {
		return $this->jsonReadRequest(function(&$transaction) {

			$model = $this->from;
			$modelCust = "Models\V1\Cust\Customers";
			$modelCases = "Models\V1\Cust\Cases";
			$modelEmployees = "Models\V1\Org\Employees";
			$modelParts = "Models\V1\Booking\Bookingparts";
			$modelTemplates = "Models\V1\Pdf\Templates";
			$client_id = $this->grantAccessForClient();

			$rows = $this->modelsManager->createBuilder()
				->columns('booking.*, customer.*, c.*, employee.hasTariff590 as hasTariff590')
				->from(['booking' => $model])
				->leftJoin($modelCust, "customer.id = booking.customer_id", "customer")
				->leftJoin($modelCases, "c.id = booking.case_id", "c")
				->leftJoin($modelEmployees, "employee.id = booking.employee_id", "employee")
				->where("booking.client_id = :cid:", ["cid" => $client_id])
				->andWhere("booking.customer_id IS NOT NULL")
				->andWhere("booking.case_id IS NOT NULL")
				->andWhere("booking.status = :ope: OR booking.status = :cao: OR booking.status = :cro: OR booking.status = :reo:", [
					'ope' => $model::STATUS_OPEN,
					'cao' => $model::STATUS_LATECANCELLED,
					'cro' => $model::STATUS_CANCELLED_RESCINDED_OPEN,
					'reo' => $model::STATUS_RESCINDED_OPEN,
				])
				->andWhere("booking.date < CURDATE() OR (booking.date = CURDATE() AND booking.endTime < CURTIME())")
				->orderBy('booking.customer_id, booking.case_id, booking.date, booking.startTime')
				->getQuery()
				->execute();

			// further handling only needed if something found:
			if(count($rows) == 0) { return array(); }

			// Ask for Template Data
			$temps = $this->getInvoiceTemplates();
			$hasTemps = count($temps) > 0;
			// Checkup only needed if an employee has Tarif 590
			$rec_temps_checked = false;

			// Create custom structure with bookings associated to customers
			$retArr = array();
			$customer = null;
			$cases = null;
			$case = null;
			$actCust = 0;
			$actCase = 0;
			$bookings = array();

			foreach($rows as $row) {

				// New Customer?
				if($row->booking->customer_id != $actCust) {
					// if new Customer -> add old to return Array
					if($customer != null) {
						// Add old Bookings to old Case
						$case["bookings"] = $bookings;
						// Add old Case to old Cases Array
						$cases[] = $case;
						// Add old Case to old Customer
						$customer["cases"] = $cases;
						// Add old Customer to return Array
						$retArr[] = $customer;
					}

					// Start new Customer
					$actCust = $row->customer->id;
					$customer = $this->_valueConversionPostload($row->customer, $modelCust::$attrs)->toArray();

					$cases = array();
					// Start new Case
					$case = $this->_valueConversionPostload($row->c, $modelCases::$attrs)->toArray();
					$actCase = $row->c->id;
					$bookings = array();
				}

				// New Case?
				if($row->booking->case_id != $actCase) {

					// If case deleted -> continue
					//if(!$row->booking->case) continue;

					// Save old case and bookings
					$case["bookings"] = $bookings;
					$cases[] = $case;

					// Start new case
					$case = $this->_valueConversionPostload($row->c, $modelCases::$attrs)->toArray();
					$actCase = $row->c->id;
					$bookings = array();
				}

				// Value Conversions
				$booking = $this->_valueConversionPostload($row->booking, $model::$attrs)->toArray();
				// Add Bookingparts to the Booking
				$booking['bookingparts'] = array();
				foreach($row->booking->bookingparts as $part) {
					$booking['bookingparts'][] = $this->_valueConversionPostload($part, $modelParts::$attrs);
				}
				$booking['customer'] = $customer;

				// Add required fields for invoicing checkup data:
				// Has Employee Tarif 590?
				if(boolval($row->hasTariff590)) {
					$standard = $modelTemplates::STANDARDS['TARIF590'];
					if(!$rec_temps_checked) {
						$has590recTemps = count($this->getTarif590ReclaimslipTemplates()) > 0;
						$rec_temps_checked = true;
					}
					$booking['invoiceable'] = $hasTemps && $has590recTemps ? $row->booking->invoiceable($standard) : false;
					$booking['missing_fields_invoice'] = $booking['invoiceable'] ? array() : $row->booking->missingFieldsForInvoicing($standard);
					if(!$hasTemps) {
						$booking['missing_fields_invoice'][] = "TEMPLATE.INVOICE";
					}
					if(!$has590recTemps) {
						$booking['missing_fields_invoice'][] = "TEMPLATE.RECLAIM_SLIP_590";
					}
				}
				else {
					$standard = "";
					$booking['invoiceable'] = $hasTemps ? $row->booking->invoiceable($standard) : false;
					$booking['missing_fields_invoice'] = $booking['invoiceable'] ? array() : $row->booking->missingFieldsForInvoicing($standard);
					if(!$hasTemps) {
						$booking['missing_fields_invoice'][] = "TEMPLATE.INVOICE";
					}
				}

				// Add booking to bookings array
				$bookings[] = $booking;
			}

			if($customer != null) {
				// Add old Bookings to old Case
				$case["bookings"] = $bookings;
				// Add old Case to old Cases Array
				$cases[] = $case;
				// Add old Case to old Customer
				$customer["cases"] = $cases;
				// Add old Customer to return Array
				$retArr[] = $customer;
			}

			return $retArr;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * get Action for open invoiceable Bookings of today and tomorrow
	 */
	public function indexInvoiceableTomtodAction($date = "") {
		return $this->jsonReadRequest(function(&$transaction) {

			if(!empty($date)) {
				$tod = date("Y-m-d", strtotime($date));
				$tom = date("Y-m-d", strtotime($date . " + 1 days"));
			} else {
				$tod = date("Y-m-d");
				$tom = date("Y-m-d", strtotime('tomorrow'));
			}

			$model = $this->from;
			$modelCust = "Models\V1\Cust\Customers";
			$modelCases = "Models\V1\Cust\Cases";
			$modelParts = "Models\V1\Booking\Bookingparts";
			$modelEmployees = "Models\V1\Org\Employees";
			$modelTemplates = "Models\V1\Pdf\Templates";
			$client_id = $this->grantAccessForClient();

			$rows = $this->modelsManager->createBuilder()
				->columns('booking.*, customer.*, c.*, employee.hasTariff590 as hasTariff590')
				->from(['booking' => $model])
				->leftJoin($modelCust, "customer.id = booking.customer_id", "customer")
				->leftJoin($modelCases, "c.id = booking.case_id", "c")
				->leftJoin($modelEmployees, "employee.id = booking.employee_id", "employee")
				->where("booking.client_id = :cid:", ["cid" => $client_id])
				->andWhere("booking.customer_id IS NOT NULL")
				->andWhere("booking.case_id IS NOT NULL")
				->andWhere("booking.status = :ope: OR booking.status = :cao: OR booking.status = :cro: OR booking.status = :reo:", [
					'ope' => $model::STATUS_OPEN,
					'cao' => $model::STATUS_LATECANCELLED,
					'cro' => $model::STATUS_CANCELLED_RESCINDED_OPEN,
					'reo' => $model::STATUS_RESCINDED_OPEN,
				])
				->andWhere("booking.date = :today: OR booking.date = :tomorrow:", [
					'today' => $tod,
					'tomorrow' => $tom,
				])
				->orderBy('booking.date, booking.startTime')
				->getQuery()
				->execute();

			// further handling only needed if something found:
			if(count($rows) == 0) {
				return array(
					"today" => array(),
					"tomorrow" => array(),
				);
			}

			// Ask for Template Data
			$temps = $this->getInvoiceTemplates();
			$hasTemps = count($temps) > 0;
			// Checkup only needed if an employee has Tarif 590
			$rec_temps_checked = false;

			// Create custom structure with bookings associated to customers
			$ret = array();
			$ret["today"] = array();
			$ret["tomorrow"] = array();

			foreach($rows as $row) {
				// Value Conversions and relation additions
				$booking = $this->_valueConversionPostload($row->booking, $model::$attrs)->toArray();
				$booking['bookingparts'] = array();
				foreach($row->booking->bookingparts as $part) {
					$booking['bookingparts'][] = $this->_valueConversionPostload($part, $modelParts::$attrs);
				}
				$booking['customer'] = $this->_valueConversionPostload($row->customer, $modelCust::$attrs);
				$booking['case'] = $this->_valueConversionPostload($row->c, $modelCases::$attrs);

				// Add required fields for invoicing checkup data:
				// Has Employee Tarif 590?
				if(boolval($row->hasTariff590)) {
					$standard = $modelTemplates::STANDARDS['TARIF590'];
					if(!$rec_temps_checked) {
						$has590recTemps = count($this->getTarif590ReclaimslipTemplates()) > 0;
						$rec_temps_checked = true;
					}
					$booking['invoiceable'] = $hasTemps && $has590recTemps ? $row->booking->invoiceable($standard) : false;
					$booking['missing_fields_invoice'] = $booking['invoiceable'] ? array() : $row->booking->missingFieldsForInvoicing($standard);
					if(!$hasTemps) {
						$booking['missing_fields_invoice'][] = "TEMPLATE.INVOICE";
					}
					if(!$has590recTemps) {
						$booking['missing_fields_invoice'][] = "TEMPLATE.RECLAIM_SLIP_590";
					}
				}
				else {
					$standard = "";
					$booking['invoiceable'] = $hasTemps ? $row->booking->invoiceable($standard) : false;
					$booking['missing_fields_invoice'] = $booking['invoiceable'] ? array() : $row->booking->missingFieldsForInvoicing($standard);
					if(!$hasTemps) {
						$booking['missing_fields_invoice'][] = "TEMPLATE.INVOICE";
					}
				}

				$date = date("Y-m-d", strtotime($booking['date']));
				if($date == $tod) {
					$ret["today"][] = $booking;
				} else if($date == $tom) {
					$ret["tomorrow"][] = $booking;
				}
			}

			return $ret;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * get Action for open invoiceable Bookings of today and tomorrow
	 */
	public function getCustomerlistAction($id, $date = "") {
		return $this->jsonReadRequest(function(&$transaction) use ($id, $date) {

			if(!empty($date)) {
				$tod = strtotime(date("Y-m-d", strtotime($date)));
			} else {
				$tod = strtotime('today');
			}

			$model = $this->from;
			$modelCust = "Models\V1\Cust\Customers";
			$modelCases = "Models\V1\Cust\Cases";
			$modelProd = "Models\V1\Prod\Products";
			$modelParts = "Models\V1\Booking\Bookingparts";
			$client_id = $this->grantAccessForClient();

			$rows = $this->modelsManager->createBuilder()
				->columns('booking.*, customer.*, c.title as case_title, product.color as product_color')
				->from(['booking' => $model])
				->leftJoin($modelCust, "customer.id = booking.customer_id", "customer")
				->leftJoin($modelCases, "c.id = booking.case_id", "c")
				->leftJoin($modelProd, "product.id = booking.product_id", "product")
				->where("booking.client_id = :cid:", ["cid" => $client_id])
				->andWhere("booking.customer_id = :cuid:", ["cuid" => $id])
				->orderBy('booking.date, booking.startTime')
				->getQuery()
				->execute();

			// Create custom structure with bookings associated to customers
			$ret = array();
			$ret["before"] = array();
			$ret["after"] = array();

			if(count($rows) > 0) {
				$ret['customer'] = $this->_valueConversionPostload($rows[0]->customer, $modelCust::$attrs);

				foreach($rows as $row) {
					// Value Conversions and relation additions
					$booking = $this->_valueConversionPostload($row->booking, $model::$attrs)->toArray();
					$booking['bookingparts'] = array();
					foreach($row->booking->bookingparts as $part) {
						$booking['bookingparts'][] = $this->_valueConversionPostload($part, $modelParts::$attrs);
					}
					$booking['case_title'] = $row->case_title;
					$booking['product_color'] = $row->product_color;
					// $booking['customer'] = $this->_valueConversionPostload($row->customer, $modelCust::$attrs);
					// $booking['case'] = $this->_valueConversionPostload($row->c, $modelCases::$attrs);

					$date = strtotime(date("Y-m-d", strtotime($booking['date'])));

					$booking['testdate_booking'] = $date;
					$booking['testdate_today'] = $tod;

					if($date < $tod) {
						$ret["before"][] = $booking;
					} else if($date >= $tod) {
						$ret["after"][] = $booking;
					}
				}
			} else {
				$ret['customer'] = NULL;
			}

			return $ret;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * one action
	 */
	public function oneAction($id) {

		return $this->jsonReadRequest(function(&$transaction) use ($id) {
			$model = $this->from;
			$modelEmployees = "Models\V1\Org\Employees";
			$modelProducts = "Models\V1\Prod\Products";
			$modelCustomers = "Models\V1\Cust\Customers";
			$modelInvoices = "Models\V1\Invo\Invoices";
			$modelCases = "Models\V1\Cust\Cases";
			$modelReports = "Models\V1\Cust\Reports";
			$modelBookingparts = "Models\V1\Booking\Bookingparts";
			$modelRooms = "Models\V1\Loc\Rooms";
			$modelLocations = "Models\V1\Loc\Locations";
			$modelOnlinebookings = "Models\V1\Onlinebooking\Bookings";

			$client_id = $this->grantAccessForClient();

			$rows = $this->modelsManager->createBuilder()
				->columns('booking.*')
				->from([
					'booking' => $model,
					])
				->where(
    				"booking.client_id = :cid:",
					["cid" => $client_id]
					)
				->andWhere("booking.id = :bid:", ["bid" => $id])
				->getQuery()
				->execute();

			$book_obj = $this->_valueConversionPostload($rows[0], $model::$attrs);
			$booking = $book_obj->toArray();
			$booking["customer"] = !$book_obj->customer ? NULL : $this->_valueConversionPostload($book_obj->customer, $modelCustomers::$attrs)->toArray();
			$booking["employee"] = !$book_obj->employee ? NULL : $this->_valueConversionPostload($book_obj->employee, $modelEmployees::$attrs);
			$booking["invoice"] = !$book_obj->invoice ? NULL : $this->_valueConversionPostload($book_obj->invoice, $modelInvoices::$attrs);
			$booking["product"] = !$book_obj->product ? NULL : $this->_valueConversionPostload($book_obj->product, $modelProducts::$attrs);
			$booking["case"] = !$book_obj->case ? NULL : $this->_valueConversionPostload($book_obj->case, $modelCases::$attrs);
			$booking["report"] = !$book_obj->report ? NULL : $this->_valueConversionPostload($book_obj->report, $modelReports::$attrs);
			$booking["room"] = !$book_obj->room ? NULL : $this->_valueConversionPostload($book_obj->room, $modelRooms::$attrs);
			$booking["location"] = $book_obj->room && $book_obj->room->location ? $this->_valueConversionPostload($book_obj->room->location, $modelLocations::$attrs) : NULL;
			$booking["onlinebooking"] = !$book_obj->onlinebooking ? NULL : $this->_valueConversionPostload($book_obj->onlinebooking, $modelOnlinebookings::$attrs);

			if(!empty($book_obj->customer)) {
				$cases = $book_obj->customer->cases;
				$ca = array();
				foreach($cases as $case) {
					$ca[] = $this->_valueConversionPostload($case, $modelCases::$attrs);
				}
				$booking["customer"]["cases"] = $ca;
			}

			if(!empty($booking["product"])) {
				$booking["product"] = $booking["product"]->toArray();
				$booking["product"]["rooms"] = array();
				foreach($rows[0]->product->rooms as $room) {
					$r = new \stdClass();
					$r->room_id = (int)$room->id;
					$r->product_id = (int)$rows[0]->product->id;
					$r->room = $this->_valueConversionPostload($room, $modelRooms::$attrs)->toArray();
					$booking["product"]["rooms"][] = $r;
				}
				$booking["product"]["employees"] = array();
				foreach($rows[0]->product->employees as $employee) {
					if(boolval($employee->active)) {
						$e = new \stdClass();
						$e->employee_id = (int)$employee->id;
						$e->product_id = (int)$rows[0]->product->id;
						$e->employee = $this->_valueConversionPostload($employee, $modelEmployees::$attrs)->toArray();
						$booking["product"]["employees"][] = $e;
					}
				}
			}

			$booking["bookingparts"] = array();
			foreach($book_obj->bookingparts as $part) {
				$bpart = $this->_valueConversionPostload($part, $modelBookingparts::$attrs);
				$booking["bookingparts"][] = $bpart;
			}

			return $booking;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * create action
	 */
	public function createAction() {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);

		return $this->jsonWriteRequest(function(&$transaction) {

			$model = $this->from;
			$attrs = $model::$attrs;
			$relations = $model::$relations;

			$transaction = $this->transaction;
			$o = $this->request->getJsonRawBody();
			$obj = $this->_createActionObj($o, $model, $attrs);
			$newId = $obj->id;

			$obj = $model::findFirstByid($newId);
			if(!$obj->open()) {
				throw new \Exception("Booking type not set or status change not possible!", 400);
			}

			foreach($relations as $relation) {
				if($relation["type"] == "hasMany") {
					$alias = $relation["alias"];
					if(isset($o->$alias)) {
						$relClass = $relation["relationModel"];
						$ria = $relation["relationIdAlias"];
						foreach($o->$alias as $rel) {
							$rel->$ria = $newId;
							$this->_createActionObj($rel, $relClass, $relClass::$attrs);
						}
					}
				}
			}

			if(property_exists($o, "sendmail") && $o->sendmail) {
				if(isset($o->customer_id)) {
					$customer_id = $o->customer_id;
				} else {
					throw new \Exception("No Customer for SendMail defined", 404);
				}
				$booking_ids = $obj->id;
				$this->sendBookingsMail($booking_ids, $customer_id);
			}

			$transaction->commit();
			return $obj;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * create action
	 */
	public function fixWrongBookingsAction() {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);

		return $this->jsonWriteRequest(function(&$transaction) {

			$model = $this->from;
			$attrs = $model::$attrs;
			$relations = $model::$relations;

			$transaction = $this->transaction;

			$rows = $this->modelsManager->createBuilder()
			->columns('booking.*')
			->from([
				'booking' => $model,
			])
			->where(
				"booking.discount != :disc: AND booking.status = 'ope'",
				["disc" => 0]
			)
			->getQuery()
			->execute();

			foreach($rows as $booking) {
				$discount = $booking->discount;
				$product = $booking->product;
				$parts = $product->productparts;
				$bparts = $booking->bookingparts;
				foreach($bparts as $bpart) {
					$matchpart = false;
					$m_part = null;

					foreach($parts as $part) {
						if($bpart->tariff == $part->tariff) {
							if(!empty($bpart->tariffposition_id)) {
								if($bpart->tariffposition_id == $part->tariffposition_id) {
									$matchpart = true;
									$m_part = $part;
									break;
								}
							}
							else {
								if($bpart->title == $part->title) {
									$matchpart = true;
									$m_part = $part;
									break;
								}
							}
						}
					}

					if($matchpart) {
						if(!empty($m_part->duration)) {
							$ppm = (float)$m_part->price / (float)$m_part->duration;
							$bpart->price = $ppm * (float)$bpart->duration;
							$bpart->update();
						}
					}

				}
			}

			$transaction->commit();
			return $obj;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * update action
	 */
	public function updateAction($id) {

		return $this->jsonWriteRequest(function(&$transaction) use ($id) {
			$model = $this->from;
			$pk = $id;
			$attrs = $model::$attrs;
			$relations = $model::$relations;
			$values = null;
			$valueConverter = null;

			$o = $this->request->getJsonRawBody();
			$obj = $this->_updateActionObj($o, $model, $attrs, $pk);

			/* TODO: Add Sendmail for Updates with fitting text
			if(property_exists($data, "sendmail") && $data->sendmail) {
				if(isset($data->customer_id)) {
					$customer_id = $data->customer_id;
				} else {
					throw new \Exception("No Customer for SendMail defined", 404);
				}
				$booking_ids = $res->id;
				$this->sendBookingsMail($booking_ids, $customer_id);
			}
			*/

			$relation = $relations["bookingparts"];
			$relation["type"];

			$alias = $relation["alias"];
			$relClass = $relation['relationModel'];
			$relIdAlias = $relation['relationIdAlias'];

			if(!isset($o->$alias) && !is_array($obj->$alias)) {
				$this->response->setStatusCode(404, "bookingparts not defined");
				return $this->response->send();
			}
			if(!isset($obj->$alias) && !is_array($obj->$alias)) {
				$this->response->setStatusCode(400, "bookingparts not found");
				return $this->response->send();
			}

			// collect "old" relation objects
			$relObjs = array();
			foreach($obj->$alias as $r) {
				//$add = new \stdClass();
				//$add->id = $r->id;
				$relObjs[] = $r->id;
			}

			// Definierte Relationen auf Neu oder Update prüfen
			foreach($o->$alias as $rel) {
				$new = true;
				if(empty($rel->$relIdAlias)) {
					$rel->$relIdAlias = $pk;
				}
				if(!empty($rel->id)) {
					for($i = 0; $i < count($relObjs); $i++) {
						$relObj = $relObjs[$i];
						if((int)$rel->id == (int)$relObj) {
							// Beziehung ist schon vorhanden: Update!
							$this->_updateActionObj($rel, $relClass, $relClass::$attrs, $rel->id);
							$new = false;
							unset($relObjs[$i]);
							$relObjs = array_values($relObjs);
							break;
						}
					}
				}
				if($new) {
					// Beziehung ist neu: Anlegen!
					$this->_createActionObj($rel, $relClass, $relClass::$attrs);
				}
			}

			// Alte, nicht gefundene Beziehungen löschen
			foreach($relObjs as $relDel) {
				$this->_deleteActionObj($relClass, $relDel);
			}

			$transaction = $this->transaction;
			$transaction->commit();
			return $obj;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * delete action
	 */
	public function deleteAction($id) {
		$model = $this->from;
		$modelParts = 'Models\V1\Booking\Bookingparts';

		// Delete Parts
		$parts = $this->_oneActionObj($model, $id)->bookingparts;
		foreach($parts as $part) {
			$part_id = $part->id;
			$this->_deleteActionObj($modelParts, $part_id);
		}
		return $this->_deleteActionObj($model, $id);
	}

	/* -----------------------------------------------------------------------------------
	 * send Calendar confirmation
	 */
	public function sendBookingsMail($booking_ids, $customer_id, $subject = "", $text = "") {

		// TODO: New composer compatible library

		// Include ICS Creation Library
		$vendorpath = "app" . DIRECTORY_SEPARATOR . "vendor";
		$includepath = \GSCLibrary\SystemInfo::prefixedPath($vendorpath);
		require_once $includepath . DIRECTORY_SEPARATOR . "icalcreator" . DIRECTORY_SEPARATOR . "zapcallib.php";

		$client_id = $this->session->get("auth-identity-client");

		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		$modelCust = "Models\V1\Cust\Customers";
		$postLoadCust = function ($obj) {
			$model = "Models\V1\Cust\Customers";
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		$customer = $this->_oneActionObj($modelCust, $customer_id, $postLoadCust);

		$modelOrg = "Models\V1\Org\Organization";
		$organization = $modelOrg::getOrganization();

		// E-Mail Content Data -------------------------------------------------------------

		$mailto2 = "0";
		if(isset($customer->email)) {
			$mailto = $customer->email;
			if(isset($customer->email_second)) {
				$mailto2 = $customer->email_second;
			}
		} else if(isset($customer->email_second)) {
			$mailto = $customer->email_second;
		} else {
			throw new \Exception("No Email Address defined", 404);
		}

		if(!isset($organization->email)) {
			throw new \Exception("No Organization Email Address defined", 404);
		} else {
			$mailFrom = $organization->email;
		}

		if($subject == "") {
			$subject = "Buchungsbestätigung für ihre Behandlung bei " . $organization->name;
		}
		if($text == "") {
			if($customer->address_you) {
				if($customer->gender) {
					$text = "Lieber ";
				} else {
					$text = "Liebe ";
				}
				$text .= $customer->firstname . ",\r\n\r\n";
				$text .= "Gern bestätigen wir die Buchung Deines Termins. ";
				$text .= "Im Anhang findest Du eine Kalender-Datei zum einfachen Einfügen des Termins in Deinen Terminkalender.";
				$text .= ".\r\n\r\nLiebe Grüße,\r\n\r\n";
				$text .= $employee->firstname . " " . $employee->name . "\r\n\r\n\r\n\r\n";
				$text .= $organization->name . "\r\n\r\n";
				$text .= $organization->street . "\r\n\r\n";
				$text .= $organization->zip . " " . $organization->city . "\r\n\r\n";
				$text .= $organization->phone . "\r\n\r\n";
				$text .= $organization->email;
			} else {
				if($customer->gender) {
					$text = "Sehr geehrter ";
					if(isset($customer->address)) {
						$text .= $customer->address;
					} else {
						$text .= "Herr";
					}
				} else {
					$text = "Sehr geehrte ";
					if(isset($customer->address)) {
						$text .= $customer->address;
					} else {
						$text .= "Frau";
					}
				}

				$text .= " " . $customer->name . ",\r\n\r\n";
				$text .= "Gern bestätigen wir die Buchung Ihres Termins. ";
				$text .= "Im Anhang finden Sie eine Kalender-Datei zum einfachen Einfügen des Termins in Ihren Terminkalender.";
				$text .= ".\r\n\r\nMit freundlichen Grüßen,\r\n\r\n";
				$text .= $employee->firstname . " " . $employee->name . "\r\n\r\n\r\n\r\n";
				$text .= $organization->name . "\r\n\r\n";
				$text .= $organization->street . "\r\n\r\n";
				$text .= $organization->zip . " " . $organization->city . "\r\n\r\n";
				$text .= $organization->phone . "\r\n\r\n";
				$text .= $organization->email;
			}
		}

		// Create Attachment -----------------------------------------------------------------

		// create the ical object
		$icalobj = new \ZCiCal();

		// Add timezone data
		$tzid = "Europe/Zurich";
		$tzYearS = (int)date('Y');
		$tzYearE = $tzYearS + 1;
		\ZCTimeZoneHelper::getTZNode($tzYearS,$tzYearE,$tzid, $icalobj->curnode);

		// check, if one or many bookings specified
		if(!is_array($booking_ids)) {
			$book = (int)$booking_ids;
			$booking_ids = array($book);
		}

		// Add Eventobjects to calendar object
		foreach($booking_ids as $booking_id) {

			// Collect Data for the Event
			$booking = $this->_oneActionObj($model, $booking_id, $postLoad);

			$uid  = uniqid();
			$uid .= "-" . str_pad($client_id, 3, "0", STR_PAD_LEFT );
			$uid .= "@" . $_SERVER['SERVER_NAME'];

			$title = $booking->title;

			$event_date = date('Y-m-d', strtotime($booking->date));
			$event_startTime = date('H:i:s', strtotime($booking->$startTime));
			$event_endTime = date('H:i:s', strtotime($booking->$endTime));
			$event_start = $event_date . " " . $event_startTime;
			$event_end = $event_date . " " . $event_endTime;

			$description = isset($booking->comment) ? $booking->comment : "";

			if(isset($booking->room_id)) {
				$roomObj = $booking->room;
// TODO: New Room structure! $location = $organization->name . ", " . $roomObj->street . ", " . $roomObj->zip . " " . $roomObj->city;
			} elseif (isset($booking->location_title)) {
				$location = $booking->location_title;
			} else {
				$location = "";
			}

			// create the event within the ical object
			$eventobj = new ZCiCalNode("VEVENT", $icalobj->curnode);

			// add Data to the Eventobject
			$eventobj->addNode(new \ZCiCalDataNode("UID:" . $uid));
			$eventobj->addNode(new \ZCiCalDataNode("SUMMARY:" . $title));
			$eventobj->addNode(new \ZCiCalDataNode("DTSTART:" . \ZCiCal::fromSqlDateTime($event_start)));
			$eventobj->addNode(new \ZCiCalDataNode("DTEND:" . \ZCiCal::fromSqlDateTime($event_end)));
			$eventobj->addNode(new \ZCiCalDataNode("DTSTAMP:" . \ZCiCal::fromSqlDateTime()));
			$eventobj->addNode(new \ZCiCalDataNode("LOCATION:" . \ZCiCal::formatContent($location)));
			$eventobj->addNode(new \ZCiCalDataNode("DESCRIPTION:" . \ZCiCal::formatContent($description)));
		}

		// Write Calendar Output to ICS File
		$path = $this->getTempPath();
		$filename = \GSCLibrary\SystemInfo::uniqueFilename("ics");
		$attachFile = $path . DIRECTORY_SEPARATOR . $filename;
		$attachName = "Termine.ics";
		$attachFileText = $icalobj->export();
		$file = fopen($attachFile, "w");
		fwrite($file, $attachFileText);
		fclose($file);

		// Send Email via SendGrid ---------------------------------------------------------------------------
		$sendgrid = new \SendGrid("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");
		$sgmail = new \SendGrid\Email();
		$sgmail
			->addTo($mailto)
			->setFrom($mailFrom)
			->setSubject($subject)
			->setText($text)
			->setAttachment($attachFile, $attachName)
			->addHeader("Content-Type", "text/plain; charset=utf-8")
			->addHeader("Content-Transfer-Encoding", "quoted-printable")
			;
		if($mailto2 != "0") {
			$sgmail->addTo($mailto2);
		}
		$sendgrid->send($sgmail);

		// Delete the created File to save disk space
		unlink($attachFile);
	}

	/* -----------------------------------------------------------------------------------
	 * cancel action
	 */
	public function cancelAction($id) {
		return $this->jsonWriteRequest(function(&$transaction) use ($id) {
			$model = $this->from;
			$attrs = $model::$attrs;
			$transaction = $this->transaction;

			$o = $this->request->getJsonRawBody();
			$obj = $this->_oneActionObj($model, $id);

			if(!$obj->cancel($o->cancel_reason, strtotime($o->cancelledAt), boolval($o->invoiceable), (float)$o->price, $o->comment)) {
				throw new \Exception("Booking Cancellation not possible.", 400);
			}
			
			/* OLD WAY
			$obj = $this->_updateActionObj($o, $model, $attrs, $id);

			if(empty($o->invoiceable)) {
				if(!$obj->cancel()) {
					throw new \Exception("Status Change not possible", 400);
				}
			} else {
				if(!$obj->lateCancel()) {
					throw new \Exception("Status Change not possible", 400);
				}
			}
			*/

/* TODO: Add Sendmail for Cancellation Information to Customer
			if(property_exists($data, "sendmail") && $data->sendmail) {
				if(isset($data->customer_id)) {
					$customer_id = $data->customer_id;
				} else {
					throw new \Exception("No Customer for SendMail defined", 404);
				}
				$booking_ids = $res->id;
				$this->sendBookingsMail($booking_ids, $customer_id);
			}
			*/

			$transaction->commit();
			return $obj;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * set one booking not to invoice action
	 */
	public function notinvoiceAction($id) {
		return $this->jsonWriteRequest(function(&$transaction) use ($id) {
			$model = $this->from;
			$attrs = $model::$attrs;
			$transaction = $this->transaction;

			$obj = $this->_oneActionObj($model, $id);

			if(!$obj->notInvoice()) {
				throw new \Exception("Status Change not possible", 400);
			}

			$transaction->commit();
			return $obj;
		});
	}

	private function getInvoiceTemplates()
	{
		$modelTemplates = "Models\V1\Pdf\Templates";
		$modelGroups = "Models\V1\Pdf\Groups";
		$modelRoles = "Models\V1\Pdf\Roles";
		$client_id = $this->grantAccessForClient();

		// Ask for available Templates:
		$role = $modelRoles::ROLES['INVOICE'];
		$group = $modelGroups::GROUPS['INVOICES'];
		return $this->modelsManager->createBuilder()
			->columns('t.*')
			->from(['t' => $modelTemplates])
			->where("t.client_id = :cid:", ["cid" => $client_id])
			->andWhere("t.group_id = :gid:", ["gid" => $group])
			->andWhere("t.role_id = :rid:", ["rid" => $role])
			->andWhere("t.active = 1")
			->orderBy('t.id')
			->getQuery()
			->execute();
	}

	private function getTarif590ReclaimslipTemplates()
	{
		$modelTemplates = "Models\V1\Pdf\Templates";
		$modelGroups = "Models\V1\Pdf\Groups";
		$modelRoles = "Models\V1\Pdf\Roles";
		$client_id = $this->grantAccessForClient();

		// Ask for available 590 reclaim slip Templates:
		$role = $modelRoles::ROLES['RECLAIM_SLIP'];
		$group = $modelGroups::GROUPS['INVOICES'];
		return $this->modelsManager->createBuilder()
			->columns('t.*')
			->from(['t' => $modelTemplates])
			->where("t.client_id = :cid:", ["cid" => $client_id])
			->andWhere("t.group_id = :gid:", ["gid" => $group])
			->andWhere("t.role_id = :rid:", ["rid" => $role])
			->andWhere("t.standard = '" . $modelTemplates::STANDARDS['TARIF590'] . "'")
			->andWhere("t.active = 1")
			->orderBy('t.id')
			->getQuery()
			->execute();
	}

}
