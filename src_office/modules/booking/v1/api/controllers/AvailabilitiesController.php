<?php

namespace Api\V1\Booking\Controllers;

class AvailabilitiesController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Booking\Availabilities";

	/* -----------------------------------------------------------------------------------
	 * index action
	 */
	public function indexAction () {
		// site id or similar filter? just about access rights
		$default_projection     = array("o.id", "o.type", "o.available", "o.online_booking_available", "o.date", "o.weekday", "o.start", "o.end", "o.wholeday", "o.reason", "o.employee_id", "o.location_id", "o.client_id");
		$orderby                = array("o.type", "o.date", "o.weekday", "o.start");
		$attrs_for_projection	= array("o.id", "o.type", "o.available", "o.online_booking_available", "o.date", "o.weekday", "o.start", "o.end", "o.wholeday", "o.reason", "o.employee_id", "o.location_id", "o.client_id");
		$attrs_for_where        = array("o.id", "o.type", "o.available", "o.online_booking_available", "o.date", "o.weekday", "o.start", "o.end", "o.wholeday", "o.reason", "o.employee_id", "o.location_id", "o.client_id");
		$attrs_for_orderby      = array("o.id", "o.type", "o.available", "o.online_booking_available", "o.date", "o.weekday", "o.start", "o.end", "o.wholeday", "o.reason", "o.employee_id", "o.location_id", "o.client_id");

//		$this->obj_acl = "Models\V1\CMS\SitesACL";
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * index lookup action
	 */
	public function indexLookupAction () {
		$projection			= array("o.id", "o.date", "o.start", "o.weekday");
		$lookup_fields			= array("date", "weekday", "start");
		$orderby			= array("o.date", "o.weekday", "o.start", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->title = "";
			$l->title .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/* -----------------------------------------------------------------------------------
	 * one action
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneAction($model, $id, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * create action
	 */
	public function createAction() {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);
		$model = $this->from;
		$res = $this->_createAction($model, $model::$attrs);
		return $res;
	}

	/* -----------------------------------------------------------------------------------
	 * update action
	 */
	public function updateAction($id) {
		$model = $this->from;
		return $this->_updateAction($model, $model::$attrs, $id);
	}

	/* -----------------------------------------------------------------------------------
	 * delete action
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

}
