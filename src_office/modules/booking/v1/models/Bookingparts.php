<?php

namespace Models\V1\Booking;

class Bookingparts extends \Phalcon\Mvc\Model
{

	public $id;
	public $title;
	public $tariff;
	public $price;
	public $duration;
	public $mwst_group;
	public $tariffposition_id;
	public $booking_id;

	public static $alias_one = "bookingpart";
	public static $alias_many = "bookingparts";

	public static $attrs = array(
		'id' => "int",
		'title' => 'string',
		'tariff' => 'int',
		'price' => 'decimal',
		'duration' => 'int',
		'mwst_group' => 'int',
		'booking_id' => 'int',
		'tariffposition_id' => 'int',
	);

	public function mwstPercent()
	{
		$val = 0.0;
		if(empty($this->mwst_group)) {
			return $val;
		}
		if((int)$this->mwst_group == 1) {
			return 2.5;
		}
		if((int)$this->mwst_group == 2) {
			return 8.0;
		}
		return $val;
	}

	public static $relations = array(
		[
			"type" => "belongsTo",
			"alias" => "booking",
			"relationModel" => "Models\V1\Booking\Bookings",
			"relationIdAlias" => "booking_id"
		],
		[
			"type" => "belongsTo",
			"alias" => "tariffposition",
			"relationModel" => "Models\V1\Prod\Tariffpositions",
			"relationIdAlias" => "tariffposition_id"
		],
		[
			"type" => "hasOne",
			"alias" => "invoiceposition",
			"relationModel" => "Models\V1\Invo\Invoicepositions",
			"relationIdAlias" => "bookingpart_id"
		],
	);

	/**
	 * Independent Column Mapping.
	 */
	public function columnMap()
	{
		return array(
			'id' => 'id',
			'title' => 'title',
			'tariff' => 'tariff',
			'price' => 'price',
			'duration' => 'duration',
			'mwst_group' => 'mwst_group',
			'booking_id' => 'booking_id',
			'tariffposition_id' => 'tariffposition_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_booking_bookingparts");

		$this->belongsTo(
			"booking_id",
			"Models\V1\Booking\Bookings",
			"id",
			[
				"alias" => "booking",
			]
		);

		$this->belongsTo(
			"tariffposition_id",
			"Models\V1\Prod\Tariffpositions",
			"id",
			[
				"alias" => "tariffposition",
			]
		);

		$this->hasOne(
			"id",
			"Models\V1\Invo\Invoicepositions",
			"bookingpart_id",
			[
				"alias" => "invoiceposition",
			]
		);
	}

	// Create content object for the generic create Function out of a Productpart object
	public static function createContentObjFromProductPart(\Models\V1\Prod\Productparts $part, int $booking_id)
	{
		$ret = new \stdClass();
		foreach(self::$attrs as $attr => $type)
		{
			if($attr == "id") continue;
			if(isset($part->$attr)) $ret->$attr = $part->$attr;
		}
		$ret->booking_id = $booking_id;
		return $ret;
	}

}
