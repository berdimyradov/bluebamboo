<?php

namespace Models\V1\Booking;

class Availabilities extends \Phalcon\Mvc\Model
{

	public $id;

	public $type;

	public $available;

	public $online_booking_available;

	public $date;

	public $weekday;

	public $start;

	public $end;

	public $wholeday;

	public $reason;

	public $employee_id;

	public $location_id;

	public $client_id;

	const REASONS = array(
		'OTHER' => 0,
		'HOLIDAY' => 1,
		'SICKNESS' => 2,
		'EDUCATION' => 3,
		'PUBLIC_HOLIDAY' => 4,
	);

	const TYPES = array(
		'LOCATION_GENERAL' => 1,
		'LOCATION_CUSTOM' => 2,
		'EMPLOYEE_GENERAL' => 3,
		'EMPLOYEE_CUSTOM' => 4,
	);

	const WEEKDAYS = array(
		'Mon' => 1,
		'Tue' => 2,
		'Wed' => 3,
		'Thu' => 4,
		'Fri' => 5,
		'Sat' => 6,
		'Sun' => 0,
		'Monday' => 1,
		'Tuesday' => 2,
		'Wednesday' => 3,
		'Thursday' => 4,
		'Friday' => 5,
		'Saturday' => 6,
		'Sunday' => 0,
	);

	public static $attrs = array(
		'id' => "int",
		'type' => 'int',
		'available' => "bool",
		'online_booking_available' => "bool",
		'date' => 'date',
		'weekday' => 'int',
		'start' => 'time',
		'end' => 'time',
		'wholeday' => 'bool',
		'reason' => 'int',
		'employee_id' => 'int',
		'location_id' => 'int',
		'client_id' => 'int'
	);

	/**
	 * Independent Column Mapping.
	 */
	public function columnMap()
	{
		return array(
			'id' => "id",
			'type' => 'type',
			'available' => "available",
			'online_booking_available' => 'online_booking_available',
			'date' => 'date',
			'weekday' => 'weekday',
			'start' => 'start',
			'end' => 'end',
			'wholeday' => 'wholeday',
			'reason' => 'reason',
			'employee_id' => 'employee_id',
			'location_id' => 'location_id',
			'client_id' => 'client_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_booking_availabilities");
	}

}
