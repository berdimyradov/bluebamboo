<?php
//use Carbon\Carbon;

namespace Models\V1\Booking;

class Bookings extends \Phalcon\Mvc\Model
{

	public $id;
	public $bookedAt;
	public $type;
	public $employee_id;
	public $product_id;
	public $customer_id;
	public $room_id;
	public $location_title;
	public $price;
	public $discount;
	public $date;
	public $startTime;
	public $endTime;
	public $duration;
	public $breakAfter;
	public $title;
	public $comment;
	public $invoice_id;
	public $cancelledAt;
	public $cancel_reason;
	public $case_id;
	public $report_id;
	public $status;
	public $client_id;

	public static $alias_one = "booking";
	public static $alias_many = "bookings";

	// Status constants:
	const STATUS_OPEN = 'ope';
	const STATUS_NOT_TO_INVOICE = 'nti';
	const STATUS_INVOICED = 'inv';
	const STATUS_RESCINDED = 'sto';
	const STATUS_RESCINDED_OPEN = 'sop';
	const STATUS_LATECANCELLED = 'cao';
	const STATUS_CANCELLED = 'can';
	const STATUS_CANCELLED_INVOICED = 'cai';
	const STATUS_CANCELLED_RESCINDED = 'cas';
	const STATUS_CANCELLED_RESCINDED_OPEN = 'cso';

	const CANCEL_REASONS = array(
		'OTHER'						=> 1,
		'SELF'						=> 2,
		'CUSTOMER'					=> 3,
		'CUSTOMER_NOT_SHOW'			=> 4,
		'RESERVATION_CANCELLATION'	=> 5,
	);

	public static $attrs = array(
		"id" => "int",
		"bookedAt" => "timestamp",
		"type" => "int",
		"employee_id" => "int",
		"product_id" => "int",
		"customer_id" => "int",
		"room_id" => "int",
		"location_title" => "string",
		"price" => "decimal",
		"discount" => "int",
		"date" => "date",
		"startTime" => "time",
		"endTime" => "time",
		"duration" => "int",
		"breakAfter" => "int",
		"title" => "string",
		"comment" => "string",
		"invoice_id" => "int",
		"cancelledAt" => "timestamp",
		"cancel_reason" => "int",
		"case_id" => "int",
		"report_id" => "int",
		"status" => "string",
		"client_id" => "int",
	);

	public static $relations = array(
		"employee" => [
			"type" => "belongsTo",
			"alias" => "employee",
			"relationModel" => "Models\V1\Org\Employees",
			"relationIdAlias" => "employee_id"
		],
		"product" => [
			"type" => "belongsTo",
			"alias" => "product",
			"relationModel" => "Models\V1\Prod\Products",
			"relationIdAlias" => "product_id"
		],
		"customer" => [
			"type" => "belongsTo",
			"alias" => "customer",
			"relationModel" => "Models\V1\Cust\Customers",
			"relationIdAlias" => "customer_id"
		],
		"invoice" => [
			"type" => "belongsTo",
			"alias" => "invoice",
			"relationModel" => "Models\V1\Invo\Invoices",
			"relationIdAlias" => "invoice_id"
		],
		"case" => [
			"type" => "belongsTo",
			"alias" => "case",
			"relationModel" => "Models\V1\Cust\Cases",
			"relationIdAlias" => "case_id"
		],
		"report" => [
			"type" => "belongsTo",
			"alias" => "report",
			"relationModel" => "Models\V1\Cust\Reports",
			"relationIdAlias" => "report_id"
		],
		"room" => [
			"type" => "belongsTo",
			"alias" => "room",
			"relationModel" => "Models\V1\Loc\Rooms",
			"relationIdAlias" => "room_id"
		],
		"bookingparts" => [
			"type" => "hasMany",
			"alias" => "bookingparts",
			"relationModel" => "Models\V1\Booking\Bookingparts",
			"relationIdAlias" => "booking_id"
		],
		"onlinebooking" => [
			"type" => "hasOne",
			"alias" => "onlinebooking",
			"relationModel" => "Models\V1\Onlinebooking\Bookings",
			"relationIdAlias" => "booking_id"
		],
		"invoices" => [
			"type" => "manyToMany",
			"alias" => "invoices",
			"aliasOneObj" => "invoice",
			"throughAlias" => "bookingsInvoices",
			"throughModel" => "Models\V1\Relations\BookingsInvoices",
			"throughIdAlias" => "booking_id",
			"throughRelIdAlias" => "invoice_id",
			"relationModel" => "Models\V1\Invo\Invoices"
		],
	);

	/**
	 * Independent Column Mapping.
	 */
	public function columnMap()
	{
		return array(
			'id' => 'id',
			'bookedAt' => 'bookedAt',
			'type' => 'type',
			'employee_id' => 'employee_id',
			'product_id' => 'product_id',
			'customer_id' => 'customer_id',
			'room_id' => 'room_id',
			'location_title' => 'location_title',
			'price' => 'price',
			'discount' => 'discount',
			'date' => 'date',
			'startTime' => 'startTime',
			'endTime' => 'endTime',
			'duration' => 'duration',
			'breakAfter' => 'breakAfter',
			'title' => 'title',
			'comment' => 'comment',
			'invoice_id' => 'invoice_id',
			'cancelledAt' => 'cancelledAt',
			'cancel_reason' => 'cancel_reason',
			"case_id" => "case_id",
			"report_id" => "report_id",
			"status" => "status",
			'client_id' => 'client_id',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_booking_bookings");

		$this->belongsTo(
			"product_id",
			"Models\V1\Prod\Products",
			"id",
			[
				"alias" => "product",
			]
		);

		$this->belongsTo(
			"room_id",
			"Models\V1\Loc\Rooms",
			"id",
			[
				"alias" => "room",
			]
		);

		$this->belongsTo(
			"employee_id",
			"Models\V1\Org\Employees",
			"id",
			[
				"alias" => "employee",
			]
		);

		$this->belongsTo(
			"customer_id",
			"Models\V1\Cust\Customers",
			"id",
			[
				"alias" => "customer",
			]
		);

		$this->belongsTo(
			"report_id",
			"Models\V1\Cust\Reports",
			"id",
			[
				"alias" => "report",
			]
		);

		$this->belongsTo(
			"case_id",
			"Models\V1\Cust\Cases",
			"id",
			[
				"alias" => "case",
			]
		);

		$this->belongsTo(
			"invoice_id",
			"Models\V1\Invo\Invoices",
			"id",
			[
				"alias" => "invoice",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Booking\Bookingparts",
			"booking_id",
			[
				"alias" => "bookingparts",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Relations\BookingsInvoices",
			"booking_id",
			[
				"alias" => "bookingsInvoices",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Relations\BookingsInvoices",
			"booking_id",
			"invoice_id",
			"Models\V1\Invo\Invoices",
			"id",
			[
				"alias" => "invoices",
			]
		);

		$this->hasOne(
			"id",
			"Models\V1\Onlinebooking\Bookings",
			"booking_id",
			[
				"alias" => "onlinebooking",
			]
		);

	}

	/* -------------------------------------------------------------------------
	 * Action to open a new booking and set its status
	 */
	public function open()
	{
		if(intval($this->type) == 100) {
			$this->status = self::STATUS_OPEN;
			//$this->update();
			//return true;
			return $this->update();
		} elseif(intval($this->type) == 200) {
			$this->status = self::STATUS_NOT_TO_INVOICE;
			//$this->update();
			//return true;
			return $this->update();
		} else {
			return false;
		}
	}

	/* -------------------------------------------------------------------------
	 * Action to cancel this booking and set status depending on actual status
	 */
	public function cancel($reason, $date = null, $invoiceable = false, $price = 0, $comment = null)
	{
		if($date === null) { $date = time(); }
		if(!in_array((int)$reason, self::CANCEL_REASONS, true)) return false;
		if($invoiceable && $this->status === self::STATUS_OPEN) {
			$this->status = self::STATUS_LATECANCELLED;
			$this->cancel_reason = $reason;
			$this->cancelledAt = date('Y-m-d H:i:s', $date);
			$this->price = $price;
			if(!empty($comment)) {
				$this->comment = $comment;
			}
			return $this->update();	
		}
		else if (!$invoiceable && ($this->status === self::STATUS_OPEN || $this->status === self::STATUS_NOT_TO_INVOICE)) {
			$this->status = self::STATUS_CANCELLED;
			$this->cancel_reason = $reason;
			$this->cancelledAt = date('Y-m-d H:i:s', $date);
			if(!empty($comment)) {
				$this->comment = $comment;
			}
			return $this->update();
		}
		else {
			return false;
		}
	}

	/* -------------------------------------------------------------------------
	 * Action to cancel this booking if cancellation was too late
	 */
	/* DEPRECATED! Use no more! */
	 public function lateCancel()
	{
		if($this->status === self::STATUS_OPEN) {
			$this->status = self::STATUS_LATECANCELLED;
			return $this->update();
		} else {
			return false;
		}
	}

	/* -------------------------------------------------------------------------
	 * Action to cancel/rescind an already invoiced booking and set status depending on actual status
	 */
	public function rescind()
	{
		if($this->status === self::STATUS_INVOICED) {
			$this->status = self::STATUS_RESCINDED;
			return $this->update();
		} elseif ($this->status === self::STATUS_CANCELLED_INVOICED) {
			$this->status = self::STATUS_CANCELLED_RESCINDED;
			return $this->update();
		} else {
			return false;
		}
	}

	/* -------------------------------------------------------------------------
	 * Action to undo an invoicing status of a booking and reopen it
	 */
	public function reopen()
	{
		if($this->status === self::STATUS_INVOICED) {
			$this->status = self::STATUS_RESCINDED_OPEN;
			$this->invoice_id = NULL;
			return $this->update();
		} elseif($this->status === self::STATUS_CANCELLED_INVOICED) {
			$this->status = self::STATUS_CANCELLED_RESCINDED_OPEN;
			$this->invoice_id = NULL;
			return $this->update();
		} else {
			return false;
		}
	}

	/* -------------------------------------------------------------------------
	 * Action to invoice a booking and set its status accordingly
	 */
	public function invoice($id, &$transaction)
	{
		$modelRel = "Models\V1\Relations\BookingsInvoices";
		switch($this->status) {
			case self::STATUS_OPEN:
			case self::STATUS_RESCINDED_OPEN:
				$this->status = self::STATUS_INVOICED;
				$this->invoice_id = $id;
				$rel = new $modelRel();
				$rel->setTransaction($transaction);
				$rel->booking_id = $this->id;
				$rel->invoice_id = $id;
				if(!$rel->save()) {
					return false;
				}
				return $this->update();
			case self::STATUS_LATECANCELLED:
			case self::STATUS_CANCELLED_RESCINDED_OPEN:
				$this->status = self::STATUS_CANCELLED_INVOICED;
				$this->invoice_id = $id;
				$rel = new $modelRel();
				$rel->setTransaction($transaction);
				$rel->booking_id = $this->id;
				$rel->invoice_id = $id;
				if(!$rel->save()) {
					return false;
				}
				return $this->update();
			default:
				return false;
		}
	}

	public function notInvoice()
	{
		switch($this->status) {
			case self::STATUS_OPEN:
				$this->status = self::STATUS_NOT_TO_INVOICE;
				return $this->update();
			case self::STATUS_RESCINDED_OPEN:
				$this->status = self::STATUS_RESCINDED;
				return $this->update();
			case self::STATUS_CANCELLED_RESCINDED_OPEN:
				$this->status = self::STATUS_CANCELLED_RESCINDED;
				return $this->update();
			case self::STATUS_LATECANCELLED:
				$this->status = self::STATUS_CANCELLED;
				return $this->update();
			default:
				return false;
		}
	}

	// Returns the Unix Timestamp out of the Date and StartTime
	public function getTimestamp()
	{
		$date = date('Y-m-d', strtotime($this->date));
		$time = date('H:i:s', strtotime($this->startTime));
		return strtotime($date . " " . $time);
	}

	/* *************************************************************************
	 * Checks if the invoice is invoiceable at all for a specific standard
	 * returns: boolean
	 */
	public function invoiceable($standard = "")
	{
		$modelOrg = "Models\V1\Org\Organization";
		$modelTemplates = "Models\V1\Pdf\Templates";

		if(empty($this->customer)) { return false; }
		if(empty($this->employee)) { return false; }
		if(empty($this->case)) { return false; }

		$client_id = $this->client_id;
		$organization = $modelOrg::findFirstByClient_id($client_id);
		if(empty($organization)) { return false; }

		// Needed Data for all standards:
		if(empty($this->customer->name)) { return false; }
		if(empty($this->customer->firstname)) { return false; }
		if(empty($this->customer->street)) { return false; }
		if(empty($this->customer->zip)) { return false; }
		if(empty($this->customer->city)) { return false; }

		// Minimum required Organization Data
		if(empty($organization->name)) { return false; }
		if(empty($organization->street)) { return false; }
		if(empty($organization->zip)) { return false; }
		if(empty($organization->city)) { return false; }
		if(empty($organization->bankaccount_iban)) { return false; }
		if(!isset($organization->mwst)) { return false; }
		if(!empty($organization->mwst) && empty($organization->mwst_nr)) { return false; }
		if(empty($organization->payment_term)) { return false; }

		if($standard === $modelTemplates::STANDARDS['TARIF590']) {
			// Minimum required Tarif 590 Customer Data
			if(empty($this->customer->birth_day)) { return false; }
			if(empty($this->customer->birth_month)) { return false; }
			if(empty($this->customer->birth_year)) { return false; }

			// Minimum required Tarif 590 Employee Data
			if(empty($this->employee->firstname)) { return false; }
			if(empty($this->employee->name)) { return false; }
			if(empty($this->employee->zsr)) { return false; }
			// if(empty($this->employee->street)) { return false; }
			// if(empty($this->employee->zip)) { return false; }
			// if(empty($this->employee->city)) { return false; }

			// Minimum required Tarif 590 Case Data
			if(empty($this->case->type)) { return false; }
			if(empty($this->case->therapy_type)) { return false; }
			if(empty($this->case->payment_type)) { return false; }
		}

		return true;
	}


	/* *************************************************************************
	 * Returns the missing fields for invoicing this booking with the specified standard
	 * returns: array
	 */
	public function missingFieldsForInvoicing($standard = "")
	{
		$modelOrg = "Models\V1\Org\Organization";
		$modelTemplates = "Models\V1\Pdf\Templates";
		$ret = array();

		if(empty($this->customer)) { $ret[] = "CUSTOMER"; }
		// Needed Data for all standards:
		if(empty($this->customer->name)) { $ret[] = "CUSTOMER.NAME"; }
		if(empty($this->customer->firstname)) { $ret[] = "CUSTOMER.FIRSTNAME"; }
		if(empty($this->customer->street)) { $ret[] = "CUSTOMER.STREET"; }
		if(empty($this->customer->zip)) { $ret[] = "CUSTOMER.ZIP"; }
		if(empty($this->customer->city)) { $ret[] = "CUSTOMER.CITY"; }
		if($standard === $modelTemplates::STANDARDS['TARIF590']) {
			// Minimum required Tarif 590 Customer Data
			if(empty($this->customer->birth_day) || empty($this->customer->birth_month) || empty($this->customer->birth_year)) { $ret[] = "CUSTOMER.BIRTHDAY"; }
		}

		$client_id = $this->client_id;
		$organization = $modelOrg::findFirstByClient_id($client_id);
		if(empty($organization)) { $ret[] = "ORGANIZATION"; }
		// Minimum required Organization Data
		if(empty($organization->name)) { $ret[] = "ORGANIZATION.NAME"; }
		if(empty($organization->street)) { $ret[] = "ORGANIZATION.STREET"; }
		if(empty($organization->zip)) { $ret[] = "ORGANIZATION.ZIP"; }
		if(empty($organization->city)) { $ret[] = "ORGANIZATION.CITY"; }
		if(empty($organization->bankaccount_iban)) { $ret[] = "ORGANIZATION.IBAN"; }
		if(!isset($organization->mwst)) { $ret[] = "ORGANIZATION.MWST"; }
		if(!empty($organization->mwst) && empty($organization->mwst_nr)) { $ret[] = "ORGANIZATION.MWST_NR"; }
		if(empty($organization->payment_term)) { $ret[] = "ORGANIZATION.PAYMENT_TERM"; }

		if(empty($this->employee)) { $ret[] = "EMPLOYEE"; }
		if($standard === $modelTemplates::STANDARDS['TARIF590']) {
			// Minimum required Tarif 590 Employee Data
			if(empty($this->employee->firstname)) { $ret[] = "EMPLOYEE.FIRSTNAME"; }
			if(empty($this->employee->name)) { $ret[] = "EMPLOYEE.NAME"; }
			if(empty($this->employee->zsr)) { $ret[] = "EMPLOYEE.ZSR"; }
			// if(empty($this->employee->street)) { return false; }
			// if(empty($this->employee->zip)) { return false; }
			// if(empty($this->employee->city)) { return false; }
		}

		if(empty($this->case)) { $ret[] = "CASE"; }
		if($standard === $modelTemplates::STANDARDS['TARIF590']) {
			// Minimum required Tarif 590 Case Data
			if(empty($this->case->type)) { $ret[] = "CASE.TYPE"; }
			if(empty($this->case->therapy_type)) { $ret[] = "CASE.THERAPY_TYPE"; }
			if(empty($this->case->payment_type)) { $ret[] = "CASE.PAYMENT_TYPE"; }
		}

		return $ret;
	}

}
