<?php

namespace Models\V1\Insu;

class Hourlyrates extends \Phalcon\Mvc\Model
{

	public $id;
	public $tariffposition_id;
	public $insurance_id;
	public $max_pph;
	public $includeMWST;
	public $client_id;

	public function allPositions()
	{
		return empty($this->tariffposition_id);
	}

	public static $alias_one = "hourlyrate";
	public static $alias_many = "hourlyrates";

	public static $attrs = array(
		'id' => 'int',
		'tariffposition_id' => 'int',
		'insurance_id' => 'int',
		'max_pph' => 'decimal',
		'includeMWST' => 'bool',
		'client_id' => 'int',
	);

	public static $relations = array(
		"employee" => [
			"type" => "belongsTo",
			"alias" => "tariffposition",
			"relationModel" => "Models\V1\Prod\Tariffpositions",
			"relationIdAlias" => "tariffposition_id"
		],
		"product" => [
			"type" => "belongsTo",
			"alias" => "insurance",
			"relationModel" => "Models\V1\Insu\Insurances",
			"relationIdAlias" => "insurance_id"
		],
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
			'id' => 'id',
			'tariffposition_id' => 'tariffposition_id',
			'insurance_id' => 'insurance_id',
			'max_pph' => 'max_pph',
			'includeMWST' => 'includeMWST',
			'client_id' => 'client_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_insu_hourlyrates");

		$this->belongsTo(
			"tariffposition_id",
			"Models\V1\Prod\Tariffpositions",
			"id",
			[
				"alias" => "tariffposition",
			]
		);

		$this->belongsTo(
			"insurance_id",
			"Models\V1\Insu\Insurances",
			"id",
			[
				"alias" => "insurance",
			]
		);
	}

}
