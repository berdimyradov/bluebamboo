<?php

namespace Models\V1\Insu;

class Insurances extends \Phalcon\Mvc\Model
{

	public $id;

	public $name;

	public $client_id;

	public static $attrs = array(
		'id' => 'int',
		'name' => 'string',
		'client_id' => 'int',
	);

	public static $relations = array(
		[
			"type" => "hasMany",
			"alias" => "hourlyrates",
			"relationModel" => "Models\V1\Insu\Hourlyrates",
			"relationIdAlias" => "insurance_id"
		],
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
			'id' => 'id',
			'name' => 'name',
			'client_id' => 'client_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_insu_insurances");

		$this->hasMany(
			"id",
			"Models\V1\Insu\Hourlyrates",
			"insurance_id",
			[
				"alias" => "hourlyrates",
			]
		);
	}

}
