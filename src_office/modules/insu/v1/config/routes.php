<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$api = new RouterGroup(array('module' => 'api_v1_insu'));
$api->setPrefix('/api/v1/insu');

$api->addGet	("/insurances", array('controller' => 'Insurances', 'action' => 'index'));
$api->addGet	("/insurances/:int", array('controller' => 'Insurances', 'action' => 'one', 'id' => 1));
$api->addPost	("/insurances", array('controller' => 'Insurances', 'action' => 'create'));
$api->addPut	("/insurances/:int", array('controller' => 'Insurances', 'action' => 'update', 'id' => 1));
$api->addDelete	("/insurances/:int", array('controller' => 'Insurances', 'action' => 'delete', 'id' => 1));

// $api->addGet	("/hourlyrates", array('controller' => 'Hourlyrates', 'action' => 'index'));
// $api->addGet	("/hourlyrates/:int", array('controller' => 'Hourlyrates', 'action' => 'one', 'id' => 1));
// $api->addPost	("/hourlyrates", array('controller' => 'Hourlyrates', 'action' => 'create'));
// $api->addPut	("/hourlyrates/:int", array('controller' => 'Hourlyrates', 'action' => 'update', 'id' => 1));
// $api->addDelete	("/hourlyrates/:int", array('controller' => 'Hourlyrates', 'action' => 'delete', 'id' => 1));
$api->addGet	("/hourlyrates/insurance/:int", array('controller' => 'Hourlyrates', 'action' => 'indexByInsurance', 'id' => 1));
$api->addPost	("/hourlyrates/insurance/:int", array('controller' => 'Hourlyrates', 'action' => 'updateByInsurance', 'id' => 1));

$router->mount($api);
