<?php

namespace Api\V1\Insu\Logic;

class Logic extends \Phalcon\Mvc\User\Component
{

	private $feedback = null;

	/* ---------------------------------------------------------------------------------
	 * constructor
	 */
	public function __construct($di) {
		$this->setDI($di);
	}

	/* ---------------------------------------------------------------------------------
	 * addError
	 */
	protected function addError ($field, $message) {
		if ($this->feedback == null) $this->feedback = array();
		$this->feedback[$field] = $message;
	}

	/* ---------------------------------------------------------------------------------
	 * applyFeedback
	 */
	protected function applyFeedback ($response) {
		if ($this->feedback == null) $response->fields = array();
		else $response->fields = $this->feedback;
	}

	/* ---------------------------------------------------------------------------------
	 * stop processing
	 */
	protected function stopProcessing ($field, $message, $response, $xmessage = "invalid state") {
		$this->addError($field, $message);
		$this->applyFeedback($response);
		throw new \Exception($xmessage);
	}

	/* ---------------------------------------------------------------------------------
	 * init registration insurances
	 */
	public function onRegistration () {

		if (!$this->session->has('auth-identity-client')) {
			throw new \Exception("missing client id", 403);
		}
		$clientId = $this->session->get('auth-identity-client');

		// try {
			$insurances = array(
				"Aquilana",
				"Artisana",
				"Assura",
				"Atupri",
				"Concordia",
				"CSS",
				"EGK",
				"Group Mutuel",
				"Helsana",
				"Intras/Sanitas",
				"KPT",
				"Sanitas",
				"Swica",
				"Sympany",
				"Visana",
			);

			foreach ($insurances as $name) {
				$insurance = new \Models\V1\Insu\Insurances();
				$insurance->setTransaction($this->transaction);
				$insurance->name = $name;
				$insurance->client_id = $clientId;

				if (!$insurance->save()) {
					// throw new \Exception("Could not save insurance " . $name . " for Client " . $clientId, 500);
					error_log("Could not save insurance " . $name . " for Client " . $clientId);
				}
			}

		// }
		// catch (\Exception $err) {
		//	error_log($err->getMessage());
		//}
	}

}
