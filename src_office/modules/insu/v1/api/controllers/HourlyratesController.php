<?php

namespace Api\V1\Insu\Controllers;

class HourlyratesController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Insu\Hourlyrates";

	/* -----------------------------------------------------------------------------------
	 * index action
	 */
	public function indexAction ()
	{
		// site id or similar filter? just about access rights
		$default_projection     = array("o.id", "o.tariffposition_id", "o.insurance_id", "o.max_pph", "o.includeMWST", "o.client_id");
		$orderby                = array("o.id");
		$attrs_for_projection	= array("o.id", "o.tariffposition_id", "o.insurance_id", "o.max_pph", "o.includeMWST", "o.client_id");
		$attrs_for_where        = array("o.id", "o.tariffposition_id", "o.insurance_id", "o.max_pph", "o.includeMWST", "o.client_id");
		$attrs_for_orderby      = array("o.id", "o.tariffposition_id", "o.insurance_id", "o.max_pph", "o.includeMWST", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * index lookup action
	 */
	public function indexLookupAction ()
	{
		$projection			= array("o.id");
		$lookup_fields			= array("id");
		$orderby			= array("o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->title = "";
			$l->title .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/* -----------------------------------------------------------------------------------
	 * one action
	 */
	public function oneAction($id)
	{
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneAction($model, $id, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * create action
	 */
	public function createAction()
	{
		$model = $this->from;
		return $this->_createAction($model, $model::$attrs);
	}

	/* -----------------------------------------------------------------------------------
	 * update action
	 */
	public function updateAction($id)
	{
		$model = $this->from;
		return $this->_updateAction($model, $model::$attrs, $id);
	}

	/* -----------------------------------------------------------------------------------
	 * delete action
	 */
	public function deleteAction($id)
	{
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

	/* -----------------------------------------------------------------------------------
	 * Index all hourlyrates of an Insurance
	 */
	public function indexByInsuranceAction($id)
	{
		return $this->jsonReadRequest(function(&$transaction) use ($id) {
			
			$model = $this->from;
			$modelInsu = "Models\V1\Insu\Insurances";
			
			$insurance = $this->_oneActionObj($modelInsu, $id);
			$rates = $insurance->hourlyrates;

			/*
			$retArr = $insurance->toArray();
			$retArr["hourlyrates"] = array();
			foreach($rates as $rate) {
				// $retArr["hourlyrates"][] = $rate->toArray();
				$retArr[] = $this->_valueConversionPostload($rate, $model::$attrs)->toArray();
			}
			*/
			$retArr = array();
			foreach($rates as $rate) {
				$retArr[] = $this->_valueConversionPostload($rate, $model::$attrs);
			}

			return $retArr;
		});
	}


	/* -----------------------------------------------------------------------------------
	 * Update all hourlyrates of an Insurance
	 */
	public function updateByInsuranceAction($id)
	{
		return $this->jsonWriteRequest(function(&$transaction) use ($id)
		{
			$model = $this->from;
			$modelInsu = "Models\V1\Insu\Insurances";
			$transaction = $this->transaction;

			$data = $this->request->getJsonRawBody();
			if(!is_array($data)) {
				throw new \Exception("Wrong Data Structure: No Array!", 400);
			}

			// Delete all old Hourlyrates
			$insurance = $this->_oneActionObj($modelInsu, $id);
			$rates = $insurance->hourlyrates;
			foreach($rates as $rate) {
				$this->_deleteActionObj($model, $rate->id);
			}

			// Create new Hourlyrates
			foreach($data as $newrate) {
				$this->_createActionObj($newrate, $model, $model::$attrs);
			}

			$transaction->commit();
			return $data;
		});
	}

}
