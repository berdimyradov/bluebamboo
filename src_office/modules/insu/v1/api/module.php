<?php

namespace Api\V1\Insu;

use Phalcon\Loader,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\View,
    Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface
{
	public function registerAutoloaders(\Phalcon\DiInterface $dependencyInjector = null)
	{
		$loader = new Loader();
		$loader->registerNamespaces(
			array(
                'GSCLibrary'					=> '../library/',

                'Models\V1\Insu'				=> '../modules/insu/v1/models/',
				'Models\V1\Prod'				=> '../modules/prod/v1/models/',
                'Models\V1\Sys'					=> '../modules/sys/v1/models/',

                'Api\V1\Insu\Logic' 			=> '../modules/insu/v1/api/logic/',
                'Api\V1\Insu\Controllers' 		=> '../modules/insu/v1/api/controllers/',
			)
		);
		$loader->register();
	}

    public function registerServices(\Phalcon\DiInterface $di)
    {
        $di->set('dispatcher', function() {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace("Api\V1\Insu\Controllers");
            return $dispatcher;
        });
        /*
        */
        $di->set('view', function() {
            $view = new View();
//            $view->setViewsDir('../app/api/v1/insu/views/');
            return $view;
        });
    }
}
