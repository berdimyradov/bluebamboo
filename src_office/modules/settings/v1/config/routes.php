<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$api = new RouterGroup(array('module' => 'api_v1_settings'));
$api->setPrefix('/api/v1/settings');

$api->addGet	("/general", array('controller' => 'General', 'action' => 'load'));
$api->addPut	("/general", array('controller' => 'General', 'action' => 'update'));

$api->addGet	("/onlinebooking", array('controller' => 'Onlinebooking', 'action' => 'load'));
$api->addPut	("/onlinebooking", array('controller' => 'Onlinebooking', 'action' => 'update'));

$router->mount($api);
