<?php

/**
 * Controller class LocationsProductsController
 */

namespace Api\V1\Settings\Controllers;

class OnlinebookingController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Settings\Onlinebooking";

	/**
	 * returns one entry
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneAction($model, $id, $postLoad);
	}

	/**
	 * returns the settings
	 */
	public function loadAction() {
		return $this->jsonWriteRequest(function(&$transaction)
		{
			$model = $this->from;
			$client_id = $this->grantAccessForClient();

			$settings = $model::findFirstByClient_id($client_id);
			if (!$settings) {
				$settings = $this->createAction();
				$this->transaction->commit();
			} else if(empty($settings->include_code)) {
				$settings->include_code = rtrim($this->crypt->encryptBase64((string)$client_id, $this->crypt->getKey(), true), "=");
				if(!$settings->update()) {
					throw new \Exception("Could not add include code", 500);
				}
			}

			if(property_exists($model, "attrs")) {
				$settings = $this->_valueConversionPostload($settings, $model::$attrs);
			}

			return $settings;
		});
	}

	/**
	 * creates a new entry
	 */
	public function createAction() {
		$model = $this->from;

		$clientId = $this->grantAccessForClient();
		$cryptedID = rtrim($this->crypt->encryptBase64((string)$clientId, $this->crypt->getKey(), true), "=");

		$stdSettings = new \stdClass();
		$stdSettings->include_code = $cryptedID;
		$stdSettings->time_period = 10;
		$stdSettings->min_time_from_now = 48;
		$stdSettings->max_time_from_now = 31;
		$stdSettings->max_cancel_before = 48;
		$stdSettings->auto_confirm = -1;
		$stdSettings->employee_notification = false;
		$stdSettings->must_accept = false;
		$stdSettings->send_reminder_mail = 0;

		return $this->_createActionObj($stdSettings, $model, $model::$attrs);
	}

	/**
	 * updates an existing entry
	 */
	public function updateAction() {
		return $this->jsonWriteRequest(function(&$transaction)
		{
			$data = $this->request->getJsonRawBody();
			$model = $this->from;
			$transaction = $this->transaction;

			$client_id = $this->grantAccessForClient();

			if(!empty($data->client_id) && (int)$data->client_id != (int)$client_id) throw new \Exception("Wrong Client ID", 403);
			$obj = $model::findFirstByClient_id($client_id);

			if (!$obj) {
				$res = $this->_createActionObj($data, $model, $model::$attrs);
			} else {
				if(empty($data->include_code)) {
					$data->include_code = rtrim($this->crypt->encryptBase64((string)$client_id, $this->crypt->getKey(), true), "=");
				}
				$res = $this->_updateActionObj($data, $model, $model::$attrs, $obj->id);
			}

			$transaction->commit();
			return $res;
		});
	}

}
