<?php

/**
 * Controller class LocationsProductsController
 */

namespace Api\V1\Settings\Controllers;

class GeneralController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Settings\General";

	/**
	 * returns one entry
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneAction($model, $id, $postLoad);
	}

	/**
	 * returns the settings
	 */
	public function loadAction() {
		return $this->jsonReadRequest(function(&$transaction)
		{
			$model = $this->from;
			$client_id = $this->grantAccessForClient();

			$modelOrg = "Models\V1\Org\Organization";
			$organization = $modelOrg::getOrganization();

			$searchConditions  = "client_id = " . $client_id; // search for Client ID

			$settings = $model::findFirst(["conditions" => $searchConditions]);
			if ($settings == null) {
				$settings = $this->createAction();
			}

			if(property_exists($model, "attrs")) {
				$settings = $this->_valueConversionPostload($settings, $model::$attrs);
			}

			// Add Organization to Settings
			//$settings->organization = $organization->toArray();
			$set = $settings->toArray();
			$set["organization"] = $organization->toArray();

			return $set;
		});
	}

	/**
	 * creates a new entry
	 */
	public function createAction() {
		$model = $this->from;
		$stdSettings = new \stdClass();
		return $this->_createActionObj($stdSettings, $model, $model::$attrs);
	}

	/**
	 * updates an existing entry
	 */
	public function updateAction() {
		return $this->jsonWriteRequest(function(&$transaction)
		{
			$data = $this->request->getJsonRawBody();
			$model = $this->from;
			$transaction = $this->transaction;

			$client_id = $this->grantAccessForClient();
			$obj = $model::findFirstByClient_id($client_id);

			if (!$obj) {
				$res = $this->_createActionObj($data, $model, $model::$attrs);
			} else {
				$res = $this->_updateActionObj($data, $model, $model::$attrs, $obj->id);
			}

			$transaction->commit();
			return $res;
		});
	}

}
