<?php

namespace Api\V1\Settings\Logic;


class Logic extends \Phalcon\Mvc\User\Component {

	private $feedback = null;

	/* ---------------------------------------------------------------------------------
	 * constructor
	 */
	public function __construct($di) {
		$this->setDI($di);
	}

	/* ---------------------------------------------------------------------------------
	 * addError
	 */
	protected function addError ($field, $message) {
		if ($this->feedback == null) $this->feedback = array();
		$this->feedback[$field] = $message;
	}

	/* ---------------------------------------------------------------------------------
	 * applyFeedback
	 */
	protected function applyFeedback ($response) {
		if ($this->feedback == null) $response->fields = array();
		else $response->fields = $this->feedback;
	}

	/* ---------------------------------------------------------------------------------
	 * stop processing
	 */
	protected function stopProcessing ($field, $message, $response, $xmessage = "invalid state") {
		$this->addError($field, $message);
		$this->applyFeedback($response);
		throw new \Exception($xmessage);
	}

	/* ---------------------------------------------------------------------------------
	 * create Empty Onlinebooking Settings Data on Registration
	 */
	public function onRegistration () {
		if (!$this->session->has('auth-identity-client')) {
			throw new \Exception("missing client id", 403);
		}
		$clientId = $this->session->get('auth-identity-client');
		$cryptedID = rtrim($this->crypt->encryptBase64((string)$clientId, $this->crypt->getKey(), true), "=");

		$model = "\Models\V1\Settings\Onlinebooking";
		
		$stdSettings = new $model();
		$stdSettings->include_code = $cryptedID;
		$stdSettings->time_period = 10;
		$stdSettings->min_time_from_now = 48;
		$stdSettings->max_time_from_now = 31;
		$stdSettings->max_cancel_before = 48;
		$stdSettings->auto_confirm = -1;
		$stdSettings->employee_notification = false;
		$stdSettings->must_accept = false;
		$stdSettings->send_reminder_mail = 0;
		$stdSettings->client_id = $clientId;

		if (!$org->save()) {
			// throw new \Exception("Could not save insurance " . $name . " for Client " . $clientId, 500);
			error_log("Could not save empty Onlinebooking Settings Data for Client " . $clientId . "in registration process");
		}
	}

}
