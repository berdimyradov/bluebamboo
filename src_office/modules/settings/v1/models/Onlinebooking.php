<?php

/**
 * Model for app settings
 */

namespace Models\V1\Settings;

class Onlinebooking extends \Phalcon\Mvc\Model
{

	public $id;
	public $include_code;
	public $time_period;
	public $min_time_from_now;
	public $max_time_from_now;
	public $max_cancel_before;
	public $email_sender;
	public $auto_confirm;
	public $employee_notification;
	public $must_accept;
	public $terms_of_use;
	public $send_reminder_mail;
	public $client_id;

	public static $attrs = array(
		"id" => "int",
		"include_code" => "string",
		"time_period" => "int",
		"min_time_from_now" => "int",
		"max_time_from_now" => "int",
		"max_cancel_before" => "int",
		"email_sender" => "string",
		"auto_confirm" => "int",
		"employee_notification" => "bool",
		"must_accept" => "bool",
		"terms_of_use" => "string",
		"send_reminder_mail" => "int",
		"client_id" => "int"
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
			"id" => "id",
			"include_code" => "include_code",
			"time_period" => "time_period",
			"min_time_from_now" => "min_time_from_now",
			"max_time_from_now" => "max_time_from_now",
			"max_cancel_before" => "max_cancel_before",
			"email_sender" => "email_sender",
			"auto_confirm" => "auto_confirm",
			"employee_notification" => "employee_notification",
			"must_accept" => "must_accept",
			"terms_of_use" => "terms_of_use",
			"send_reminder_mail" => "send_reminder_mail",
			"client_id" => "client_id"
		);
	}

	/**
	 * Organization is like a singleton - get the one for the client:
	 */
	public static function getSettings($client_id = null) {
		$session = \Phalcon\Di::getDefault()->getSession();

		// Check for permission
		$c_id = $session->get("auth-identity-client");
		if(empty($c_id) && empty($client_id)) { throw new \Exception(403); }
		if(!empty($c_id)) {
			if($client_id != null && $client_id != $c_id) { throw new \Exception(403); }
			if($client_id == null) { $client_id = $c_id; } 
		}
		if(empty($client_id)) { throw new \Exception(403); }

		// Search for Settings Entry
		$settings = self::findFirstByClient_id($client_id);
	
		if($settings == null) {
			throw new \Exception("No Settings Data defined", 404);
		} else {
// TODO: Refactoring -> value Conversion in Model not in controller
			foreach(self::$attrs as $attr => $type) {
				if(isset($settings->$attr)) {
					if($settings->$attr === null) continue;
					else {
						switch($type) {
							case "string":
								$settings->$attr = (string)$settings->$attr;
								break;
							case "int":
								$settings->$attr = (int)$settings->$attr;
								break;
							case "decimal":
							case "float":
								$settings->$attr = (float)$settings->$attr;
								break;
							case "bool":
								$settings->$attr = (bool)$settings->$attr;
								break;
							case "date":
							case "datetime":
							case "timestamp":
								$settings->$attr = date('c', strtotime($settings->$attr));
								break;
							case "time":
								$settings->$attr = date('c', strtotime($settings->$attr, 0));
								break;
							case "json":
								$settings->$attr = json_decode($settings->$attr);
								break;
						}
					}
				}
			}
		}

		return $settings;
	}

	public function getSenderMail()
	{
		if(!empty($this->email_sender)) return $this->email_sender;
		$organization = \Models\V1\Org\Organization::findFirstByClient_id($this->client_id);
		if(empty($organization->email)) return false;
		return $organization->email;
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_settings_onlinebooking");
	}

}
