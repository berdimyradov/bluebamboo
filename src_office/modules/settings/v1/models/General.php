<?php

/**
 * Model for app settings
 */

namespace Models\V1\Settings;

class General extends \Phalcon\Mvc\Model
{

	public $id;
	public $client_id;

	public static $attrs = array(
		"id" => "int",
		"client_id" => "int"
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
		'id' => 'id',
		'client_id' => 'client_id'
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_settings_general");
	}

}
