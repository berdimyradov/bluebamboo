<?php

namespace Api\V1\Payment\Controllers;


use Genkgo\Camt\Config;
use Genkgo\Camt\Reader;
use Models\V1\Camt\Payments;
use Exception;

class CamtController extends \GSCLibrary\BaseApiController
{

    public function indexAllAction()
    {
        return $this->jsonReadRequest(function (&$transaction) {

            $payments = Payments::find();

            return $payments;
        });
    }

    public function uploadAction()
    {
        return $this->jsonWriteRequest(function (&$transaction) {
            $transaction = $this->transaction;

            try {
                $reader = new Reader(Config::getDefault());
                $payments = [];

                if (true == $this->request->hasFiles() && $this->request->isPost()) {
                    foreach ($this->request->getUploadedFiles() as $file) {

//                        $this->getFileSwitch()->phalconFileMoveTo($file, './');

                        $message = $reader->readFile($file->getTempName());
                        $statements = $message->getRecords();

                        foreach ($statements as $statement) {
                            $entries = $statement->getEntries();

                            foreach ($entries as $entry) {
                                $payment = new Payments();
                                $payment->creditor_iban = $statement->getAccount()->getIdentification();
                                $payment->amount = $entry->getAmount()->getAmount();
                                $payment->currency = $entry->getAmount()->getCurrency()->getName();
                                $payment->booking_date = $entry->getBookingDate()->format("Y-m-d");
                                $payment->value_date = $entry->getValueDate()->format("Y-m-d");
                                $payment->created_at = date("Y-m-d H:i:s");
                                $payment->updated_at = date("Y-m-d H:i:s");
                                $payment->xml_tree = file_get_contents($file->getTempName());
                                $payments[] = $payment;

                                $payment->save();
                            }
                        }
                    }
                }

                $transaction->commit();

                return $payments;

            } catch (Exception $exception) {
//                $transaction->rollback();
                throw $exception;
            }
        });
    }
}