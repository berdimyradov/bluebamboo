<?php


namespace Api\V1\Payment\Controllers;


use Models\V1\Camt\CsvDataModel;

class CsvController extends \GSCLibrary\BaseApiController {

    public function indexAllAction()
    {
        return $this->jsonReadRequest(function (&$transaction) {

            $csvDataModels = CsvDataModel::find();

            return $csvDataModels;
        });
    }
}