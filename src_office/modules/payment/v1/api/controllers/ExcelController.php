<?php


namespace Api\V1\Payment\Controllers;


use Models\V1\Camt\ExcelDataModel;
use Exception;
use PHPExcel_IOFactory;

class ExcelController extends \GSCLibrary\BaseApiController {

    public function indexAllAction()
    {
        return $this->jsonReadRequest(function (&$transaction) {

            $excelDataModels = ExcelDataModel::find();

            return $excelDataModels;
        });
    }

    public function uploadAction()
    {
        return $this->jsonWriteRequest(function (&$transaction) {
            $transaction = $this->transaction;
            $insertedData = [];

            try {

                if (true == $this->request->hasFiles() && $this->request->isPost()) {
                    foreach ($this->request->getUploadedFiles() as $file) {

                        $inputFileName = $file->getTempName();

                        //  Read your Excel workbook
                        try {
                            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                            $objPHPExcel = $objReader->load($inputFileName);
                        } catch (Exception $e) {
                            throw $e;
                        }

                        //  Get worksheet dimensions
                        $sheet = $objPHPExcel->getSheet(0);
                        $highestRow = $sheet->getHighestRow();
                        $highestColumn = $sheet->getHighestColumn();

                        //  Loop through each row of the worksheet in turn
                        for ($row = 1; $row <= $highestRow; $row++) {
                            //  Read a row of data into an array
                            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                NULL,
                                TRUE,
                                FALSE)[0];

                            if (empty($rowData) || !is_numeric($rowData[0])) {
                                continue;
                            }

                            //  Insert row data array into your database of choice here
                            $excelDataModel = new ExcelDataModel();
//                            $excelDataModel->id = $rowData[0];
                            $excelDataModel->first_name = $rowData[1];
                            $excelDataModel->last_name = $rowData[2];
                            $excelDataModel->email = $rowData[3];
                            $excelDataModel->gender = $rowData[4];
                            $excelDataModel->ip_address = $rowData[5];
                            $insertedData[] = $excelDataModel;

                            $excelDataModel->save();

                        }
                    }
                }

                $transaction->commit();

                return $insertedData;

            } catch (Exception $exception) {
//                $transaction->rollback();
                throw $exception;
            }

        });
    }

}