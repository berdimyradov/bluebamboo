<?php

/**
 * module-config for V1/API/Payments API
 */

namespace Api\V1\Payment;

use Phalcon\Loader,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\View,
    Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface {
    public function registerAutoloaders(\Phalcon\DiInterface $dependencyInjector = null)
    {
        $loader = new Loader();
        $loader->registerNamespaces(
            array(
                'GSCLibrary' => '../library/',
                'DocumentGenerator' => '../library/',

                'Models\V1\Camt' => '../modules/payment/v1/models/',

                'Api\V1\Payment\Logic' => '../modules/payment/v1/api/logic/',
                'Api\V1\Payment\Controllers' => '../modules/payment/v1/api/controllers/',

                'Genkgo\Camt' => '../vendor/genkgo/camt/src/',
                'IBAN' => '../vendor/jschaedl/iban/library/IBAN/',
                'Money' => '../vendor/moneyphp/money/lib/Money/',
            )
        );

//        $loader->registerFiles([
//        ]);

        $loader->register();
    }

    public function registerServices(\Phalcon\DiInterface $di)
    {
        $di->set('dispatcher', function () {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace("Api\V1\Payment\Controllers");
            return $dispatcher;
        });

        $di->set('view', function () {
            $view = new View();
//            $view->setViewsDir('../app/api/v1/payment/views/');
            return $view;
        });
    }
}
