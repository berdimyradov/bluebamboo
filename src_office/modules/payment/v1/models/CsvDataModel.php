<?php


namespace Models\V1\Camt;


class CsvDataModel extends \Phalcon\Mvc\Model {
    public $id;
    public $first_name;
    public $last_name;
    public $email;
    public $ip_address;
    public $gender;
    public $street;
    public $car;
    public $city;
    public $hair_color;
    public $zip;
    public $birthday;
    public $website;
    public $avatar;
    public $geo;
    public $married;
    public $state;
    public $country;
    public $user_agent;
    public $work;
    public $language;
    public $created_at;
    public $updated_at;

    public static $alias_one = "csv-datum";
    public static $alias_many = "csv-data";

    public static $attrs = array(
        'id' => 'int',
        'first_name' => 'string',
        'last_name' => 'string',
        'email' => 'string',
        'ip_address' => 'string',
        'gender' => 'string',
        'street' => 'string',
        'car' => 'string',
        'city' => 'string',
        'hair_color' => 'string',
        'zip' => 'string',
        'birthday' => 'data',
        'website' => 'string',
        'avatar' => 'string',
        'geo' => 'string',
        'married' => 'boolean',
        'state' => 'string',
        'country' => 'string',
        'user_agent' => 'string',
        'work' => 'string',
        'language' => 'string',
        'created_at' => 'data',
        'updated_at' => 'data'
    );

    public function columnMap()
    {
        return array(
            'id' => 'id',
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'email' => 'email',
            'ip_address' => 'ip_address',
            'gender' => 'gender',
            'street' => 'street',
            'car' => 'car',
            'city' => 'city',
            'hair_color' => 'hair_color',
            'zip' => 'zip',
            'birthday' => 'birthday',
            'website' => 'website',
            'avatar' => 'avatar',
            'geo' => 'geo',
            'married' => 'married',
            'state' => 'state',
            'country' => 'country',
            'user_agent' => 'user_agent',
            'work' => 'work',
            'language' => 'language',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        );
    }

    /**
     * Initializes relationships in the model
     */
    public function initialize()
    {
        $this->setSource("gsc_pay_csv_data");
    }

}