<?php

/**
 * Model for app "romi", class "Recieved"
 */

namespace Models\V1\Camt;

class Payments extends \Phalcon\Mvc\Model {
    public $id;
    public $creditor_iban;
    public $amount;
    public $currency;
    public $booking_date;
    public $value_date;
    public $created_at;
    public $updated_at;

    public static $alias_one = "payment";
    public static $alias_many = "payments";

    public static $attrs = array(
        'id' => 'int',
        'creditor_iban' => 'string',
        'amount' => 'int',
        'currency' => 'date',
        'booking_date' => 'date',
        'value_date' => 'date',
        'xml_tree' => 'string',
        'created_at' => 'date',
        'updated_at' => 'date'
    );

    public function columnMap()
    {
        return array(
            'id' => 'id',
            'creditor_iban' => 'creditor_iban',
            'amount' => 'amount',
            'currency' => 'currency',
            'booking_date' => 'booking_date',
            'value_date' => 'value_date',
            'xml_tree' => 'xml_tree',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        );
    }


    /**
     * Initializes relationships in the model
     */
    public function initialize() {
        $this->setSource("gsc_pay_received");
    }
}