<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$api = new RouterGroup(array('module' => 'api_v1_pay'));
$api->setPrefix('/api/v1/pay');

$api->addGet("/camt/all", array('controller' => 'Camt', 'action' => 'indexAll'));
$api->addPost("/camt/upload", array('controller' => 'Camt', 'action' => 'upload'));

$api->addGet("/excel/all", array('controller' => 'Excel', 'action' => 'indexAll'));
$api->addPost("/excel/upload", array('controller' => 'Excel', 'action' => 'upload'));

$router->mount($api);
