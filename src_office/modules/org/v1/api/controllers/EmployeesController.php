<?php

namespace Api\V1\Org\Controllers;

class EmployeesController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Org\Employees";

	/* -----------------------------------------------------------------------------------
	 * index action
	 */
	public function indexAction() {
		// site id or similar filter? just about access rights
		$default_projection     = array("o.id", "o.firstname", "o.name", "o.address", "o.gender", "o.foto", "o.address_line", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.birth_day", "o.birth_month", "o.birth_year", "o.phone_home", "o.phone_mobile", "o.phone_work", "o.email", "o.email_2", "o.email_primary", "o.ahv", "o.zsr", "o.egk", "o.experience", "o.specialty", "o.career", "o.hasTariff590", "o.active", "o.active_from", "o.active_to", "o.role_id", "o.client_id");
		$orderby                = array("o.name", "o.firstname", "o.id");
		$attrs_for_projection	= array("o.id", "o.firstname", "o.name", "o.address", "o.gender", "o.foto", "o.address_line", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.birth_day", "o.birth_month", "o.birth_year", "o.phone_home", "o.phone_mobile", "o.phone_work", "o.email", "o.email_2", "o.email_primary", "o.ahv", "o.zsr", "o.egk", "o.experience", "o.specialty", "o.career", "o.hasTariff590", "o.active", "o.active_from", "o.active_to", "o.role_id", "o.client_id");
		$attrs_for_where        = array("o.id", "o.firstname", "o.name", "o.address", "o.gender", "o.foto", "o.address_line", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.birth_day", "o.birth_month", "o.birth_year", "o.phone_home", "o.phone_mobile", "o.phone_work", "o.email", "o.email_2", "o.ahv", "o.zsr", "o.egk", "o.specialty", "o.career", "o.hasTariff590", "o.active", "o.active_from", "o.active_to", "o.role_id", "o.client_id");
		$attrs_for_orderby      = array("o.id", "o.firstname", "o.name", "o.address", "o.gender", "o.foto", "o.address_line", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.birth_day", "o.birth_month", "o.birth_year", "o.phone_home", "o.phone_mobile", "o.phone_work", "o.email", "o.email_2", "o.ahv", "o.zsr", "o.egk", "o.role_id", "o.hasTariff590", "o.active", "o.active_from", "o.active_to", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * index lookup action
	 */
	public function indexLookupAction () {
		$projection			= array("o.id", "o.name", "o.firstname");
		$lookup_fields			= array("name", "firstname");
		$orderby			= array("o.name", "o.firstname", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->title = "";
			$l->title .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/* -----------------------------------------------------------------------------------
	 * index active action
	 */
	public function indexActiveAction () {
		// site id or similar filter? just about access rights
		$default_projection     = array("o.id", "o.firstname", "o.name", "o.address", "o.gender", "o.foto", "o.address_line", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.birth_day", "o.birth_month", "o.birth_year", "o.phone_home", "o.phone_mobile", "o.phone_work", "o.email", "o.email_2", "o.email_primary", "o.ahv", "o.zsr", "o.egk", "o.experience", "o.specialty", "o.career", "o.hasTariff590", "o.active", "o.active_from", "o.active_to", "o.role_id", "o.client_id");
		$orderby                = array("o.name", "o.firstname", "o.id");
		$attrs_for_projection	= array("o.id", "o.firstname", "o.name", "o.address", "o.gender", "o.foto", "o.address_line", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.birth_day", "o.birth_month", "o.birth_year", "o.phone_home", "o.phone_mobile", "o.phone_work", "o.email", "o.email_2", "o.email_primary", "o.ahv", "o.zsr", "o.egk", "o.experience", "o.specialty", "o.career", "o.hasTariff590", "o.active", "o.active_from", "o.active_to", "o.role_id", "o.client_id");
		$attrs_for_where        = array("o.id", "o.firstname", "o.name", "o.address", "o.gender", "o.foto", "o.address_line", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.birth_day", "o.birth_month", "o.birth_year", "o.phone_home", "o.phone_mobile", "o.phone_work", "o.email", "o.email_2", "o.ahv", "o.zsr", "o.egk", "o.specialty", "o.career", "o.hasTariff590", "o.active", "o.active_from", "o.active_to", "o.role_id", "o.client_id");
		$attrs_for_orderby      = array("o.id", "o.firstname", "o.name", "o.address", "o.gender", "o.foto", "o.address_line", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.birth_day", "o.birth_month", "o.birth_year", "o.phone_home", "o.phone_mobile", "o.phone_work", "o.email", "o.email_2", "o.ahv", "o.zsr", "o.egk", "o.role_id", "o.hasTariff590", "o.active", "o.active_from", "o.active_to", "o.client_id");

		$filter = array(
			"active" => "1"
		);

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, $filter, $postLoad);
	
	}

	/* -----------------------------------------------------------------------------------
	 * action to list all locations for one item
	 */
	public function locationsAction($id) {
		$model = $this->from;
		return $this->_relationsAction($model, $id, "locations");
	}

	/* -----------------------------------------------------------------------------------
	 * action to list all products for one item
	 */
	public function productsAction($id) {
		$model = $this->from;
		return $this->_relationsAction($model, $id, "products");
	}

	/* -----------------------------------------------------------------------------------
	 * one action
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneActionRelations($model, $id, $model::$relations, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * create action
	 */
	public function createAction() {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);
		$model = $this->from;
		return $this->_createActionRelations($model, $model::$attrs, $model::$relations);
	}

	/* -----------------------------------------------------------------------------------
	 * update action
	 */
	public function updateAction($id) {
		$model = $this->from;
		return $this->_updateActionRelations($model, $model::$attrs, $id, $model::$relations);
	}

	/* -----------------------------------------------------------------------------------
	 * delete action
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteActionRelations($model, $id, $model::$relations);
	}

}
