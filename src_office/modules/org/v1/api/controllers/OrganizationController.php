<?php

namespace Api\V1\Org\Controllers;

class OrganizationController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Org\Organization";

	/* -----------------------------------------------------------------------------------
	 * index action
	 */
	public function indexAction () {
		// site id or similar filter? just about access rights
		$default_projection     = array("o.id", "o.name", "o.logo", "o.address_line", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.phone", "o.email", "o.bankaccount_iban", "o.bankaccount_bic", "o.bankaccount_bank", "o.bankaccount_owner", "o.mwst", "o.mwst_nr", "o.payment_term", "o.cost_reminder_first", "o.payment_term_reminder_first", "o.cost_reminder_second", "o.payment_term_reminder_second", "o.client_id");
		$orderby                = array("o.id");
		$attrs_for_projection	= array("o.id", "o.name", "o.logo", "o.address_line", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.phone", "o.email", "o.bankaccount_iban", "o.bankaccount_bic", "o.bankaccount_bank", "o.bankaccount_owner", "o.mwst", "o.mwst_nr", "o.payment_term", "o.cost_reminder_first", "o.payment_term_reminder_first", "o.cost_reminder_second", "o.payment_term_reminder_second", "o.client_id");
		$attrs_for_where        = array("o.id", "o.name", "o.logo", "o.address_line", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.phone", "o.email", "o.bankaccount_iban", "o.bankaccount_bic", "o.bankaccount_bank", "o.bankaccount_owner", "o.mwst", "o.mwst_nr", "o.payment_term", "o.cost_reminder_first", "o.payment_term_reminder_first", "o.cost_reminder_second", "o.payment_term_reminder_second", "o.client_id");
		$attrs_for_orderby      = array("o.id", "o.name", "o.logo", "o.address_line", "o.street", "o.zip", "o.city", "o.state", "o.country", "o.phone", "o.email", "o.bankaccount_iban", "o.bankaccount_bic", "o.bankaccount_bank", "o.bankaccount_owner", "o.mwst", "o.mwst_nr", "o.payment_term", "o.cost_reminder_first", "o.payment_term_reminder_first", "o.cost_reminder_second", "o.payment_term_reminder_second", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * index lookup action
	 */
	public function indexLookupAction () {
		$projection			= array("o.id", "o.name");
		$lookup_fields			= array("name");
		$orderby			= array("o.name", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->title = "";
			$l->title .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/* -----------------------------------------------------------------------------------
	 * one action
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneAction($model, $id, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * create action
	 */
	public function createAction() {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);
		$model = $this->from;
		$res = $this->_createAction($model, $model::$attrs);

		return $res;
	}

	/* -----------------------------------------------------------------------------------
	 * update action
	 */
	public function updateAction($id) {
		$model = $this->from;
		return $this->_updateAction($model, $model::$attrs, $id);
	}

	/* -----------------------------------------------------------------------------------
	 * delete action
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

	/* -----------------------------------------------------------------------------------
	 * get Organization Data or create empty new Data
	 */
	public function getAction() {
		return $this->jsonWriteRequest(function(&$transaction) {

			$model = $this->from;
			$transaction = $this->transaction;

			$clientId = $this->grantAccessForClient();

			$obj = $model::findFirstByClient_id($clientId);
			if(!$obj) {
				$org = new \stdClass();
				$org->name = "Your Organization Name";
				$org->mwst = false;
				$org->payment_term = 30;
				$org->cost_reminder_first = 0;
				$org->payment_term_reminder_first = 30;
				$org->cost_reminder_second = 0;
				$org->payment_term_reminder_second = 30;
				$org->client_id = $clientId;
				$obj = $this->_createActionObj($org, $model, $model::$attrs);
				$transaction->commit();
			}
			
			return $this->_valueConversionPostload($obj, $model::$attrs);
		});

	}


}
