<?php

namespace Api\V1\Org\Logic;

class Logic extends \Phalcon\Mvc\User\Component
{	

	private $feedback = null;

	/* ---------------------------------------------------------------------------------
	 * constructor
	 */
	public function __construct($di) {
		$this->setDI($di);
	}

	/* ---------------------------------------------------------------------------------
	 * addError
	 */
	protected function addError ($field, $message) {
		if ($this->feedback == null) $this->feedback = array();
		$this->feedback[$field] = $message;
	}

	/* ---------------------------------------------------------------------------------
	 * applyFeedback
	 */
	protected function applyFeedback ($response) {
		if ($this->feedback == null) $response->fields = array();
		else $response->fields = $this->feedback;
	}

	/* ---------------------------------------------------------------------------------
	 * stop processing
	 */
	protected function stopProcessing ($field, $message, $response, $xmessage = "invalid state") {
		$this->addError($field, $message);
		$this->applyFeedback($response);
		throw new \Exception($xmessage);
	}

	/* ---------------------------------------------------------------------------------
	 * create Empty Organization Data on Registration
	 */
	public function onRegistration () {
		if (!$this->session->has('auth-identity-client')) {
			throw new \Exception("missing client id", 403);
		}
		$clientId = $this->session->get('auth-identity-client');

		$modelOrg = "\Models\V1\Org\Organization";
		
		$org = new $modelOrg();
		$org->name = "Your Organization Name";
		$org->mwst = false;
		$org->payment_term = 30;
		$org->cost_reminder_first = 0;
		$org->payment_term_reminder_first = 30;
		$org->cost_reminder_second = 0;
		$org->payment_term_reminder_second = 30;
		$org->client_id = $clientId;

		if (!$org->save()) {
			// throw new \Exception("Could not save insurance " . $name . " for Client " . $clientId, 500);
			error_log("Could not save empty Organization Data for Client " . $clientId . "in registration process");
		}
	}

}
