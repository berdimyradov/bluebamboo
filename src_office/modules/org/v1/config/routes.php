<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$api = new RouterGroup(array('module' => 'api_v1_org'));
$api->setPrefix('/api/v1/org');

$api->addGet	("/organization", array('controller' => 'Organization', 'action' => 'get'));
$api->addGet	("/organization/:int", array('controller' => 'Organization', 'action' => 'one', 'id' => 1));
$api->addPost	("/organization", array('controller' => 'Organization', 'action' => 'create'));
$api->addPut	("/organization/:int", array('controller' => 'Organization', 'action' => 'update', 'id' => 1));
$api->addDelete	("/organization/:int", array('controller' => 'Organization', 'action' => 'delete', 'id' => 1));

$api->addGet	("/employees", array('controller' => 'Employees', 'action' => 'index'));
$api->addGet	("/employees/active", array('controller' => 'Employees', 'action' => 'indexActive'));
$api->addGet	("/employees/:int", array('controller' => 'Employees', 'action' => 'one', 'id' => 1));
$api->addGet	("/employees/:int/locations", array('controller' => 'Employees', 'action' => 'locations', 'id' => 1));
$api->addGet	("/employees/:int/products", array('controller' => 'Employees', 'action' => 'products', 'id' => 1));
$api->addPost	("/employees", array('controller' => 'Employees', 'action' => 'create'));
$api->addPut	("/employees/:int", array('controller' => 'Employees', 'action' => 'update', 'id' => 1));
$api->addDelete	("/employees/:int", array('controller' => 'Employees', 'action' => 'delete', 'id' => 1));

$router->mount($api);
