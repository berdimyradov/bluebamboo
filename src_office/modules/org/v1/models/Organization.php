<?php

namespace Models\V1\Org;

class Organization extends \Phalcon\Mvc\Model
{

	public $id;
	public $name;
	public $logo;
	public $address_line;
	public $street;
	public $zip;
	public $city;
	public $state;
	public $country;
	public $phone;
	public $email;
	public $bankaccount_iban;
	public $bankaccount_bic;
	public $bankaccount_bank;
	public $bankaccount_owner;
	public $mwst;
	public $mwst_nr;
	public $payment_term;
	public $cost_reminder_first;
	public $payment_term_reminder_first;
	public $cost_reminder_second;
	public $payment_term_reminder_second;
	public $client_id;

	public static $attrs = array(
		'id' => 'int',
		'name' => 'string',
		'logo' => 'string',
		'address_line' => 'string',
		'street' => 'string',
		'zip' => 'string',
		'city' => 'string',
		'state' => 'string',
		'country' => 'string',
		'phone' => 'string',
		'email' => 'string',
		'bankaccount_iban' => 'string',
		'bankaccount_bic' => 'string',
		'bankaccount_bank' => 'string',
		'bankaccount_owner' => 'string',
		'mwst' => 'bool',
		'mwst_nr' => 'string',
		'payment_term' => 'int',
		'cost_reminder_first' => 'float',
		'payment_term_reminder_first' => 'int',
		'cost_reminder_second' => 'float',
		'payment_term_reminder_second' => 'int',
		'client_id' => 'int',
	);

	public static $fileattrs = array(
		"logo" => ["image", "one"],
	);

	public static $relations = array(
		[
			"type" => "hasMany",
			"alias" => "invoices",
			"relationModel" => "Models\V1\Invo\Invoices",
			"relationIdAlias" => "organization_id"
		]
	);

	/**
	 * Independent Column Mapping.
	 */
	public function columnMap()
	{
		return array(
		'id' => 'id',
		'name' => 'name',
		'logo' => 'logo',
		'address_line' => 'address_line',
		'street' => 'street',
		'zip' => 'zip',
		'city' => 'city',
		'state' => 'state',
		'country' => 'country',
		'phone' => 'phone',
		'email' => 'email',
		'bankaccount_iban' => 'bankaccount_iban',
		'bankaccount_bic' => 'bankaccount_bic',
		'bankaccount_bank' => 'bankaccount_bank',
		'bankaccount_owner' => 'bankaccount_owner',
		'mwst' => 'mwst',
		'mwst_nr' => 'mwst_nr',
		'payment_term' => 'payment_term',
		'cost_reminder_first' => 'cost_reminder_first',
		'payment_term_reminder_first' => 'payment_term_reminder_first',
		'cost_reminder_second' => 'cost_reminder_second',
		'payment_term_reminder_second' => 'payment_term_reminder_second',
		'client_id' => 'client_id',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_org_organization");

		$this->hasMany(
			"id",
			"Models\V1\Invo\Invoices",
			"organization_id",
			[
				"alias" => "invoices",
			]
		);
	}

	/**
	 * Organization is like a singleton - get the one for the client:
	 */
	public static function getOrganization() {
		$session = \Phalcon\Di::getDefault()->getSession();
		$client_id = $session->get("auth-identity-client");

		// Search for Organization Entry
		$organization = self::findFirstByClient_id($client_id);
		if(!$organization) {
			throw new \Exception("No Organization Data defined", 404);
		} else {
// TODO: Refactoring -> value Conversion in Model not in controller
			foreach(self::$attrs as $attr => $type) {
				if(isset($organization->$attr)) {
					if($organization->$attr === null) continue;
					else {
						switch($type) {
							case "string":
								$organization->$attr = (string)$organization->$attr;
								break;
							case "int":
								$organization->$attr = (int)$organization->$attr;
								break;
							case "decimal":
							case "float":
								$organization->$attr = (float)$organization->$attr;
								break;
							case "bool":
								$organization->$attr = (bool)$organization->$attr;
								break;
							case "date":
							case "datetime":
							case "timestamp":
								$organization->$attr = date('c', strtotime($organization->$attr));
								break;
							case "time":
								$organization->$attr = date('c', strtotime($organization->$attr, 0));
								break;
							case "json":
								$organization->$attr = json_decode($organization->$attr);
								break;
						}
					}
				}
			}
		}

		return $organization;
	}
}
