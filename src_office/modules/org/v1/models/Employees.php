<?php

namespace Models\V1\Org;

class Employees extends \Phalcon\Mvc\Model
{

	public $id;
	public $firstname;
	public $name;
	public $address;
	public $gender;
	public $foto;
	public $address_line;
	public $street;
	public $zip;
	public $city;
	public $state;
	public $country;
	public $birth_day;
	public $birth_month;
	public $birth_year;
	public $phone_home;
	public $phone_mobile;
	public $phone_work;
	public $email;
	public $email_2;
	public $email_primary;
	public $ahv;
	public $zsr;
	public $egk;
	public $experience;
	public $specialty;
	public $career;
	public $hasTariff590;
	public $active;
	public $active_from;
	public $active_to;
	public $role_id;
	public $client_id;

	public static $attrs = array(
		"id" => "int",
		"firstname" => "string",
		"name" => "string",
		"address" => "string",
		"gender" => "bool",
		"foto" => "string",
		"address_line" => "string",
		"street" => "string",
		"zip" => "string",
		"city" => "string",
		"state" => "string",
		"country" => "string",
		"birth_day" => "int",
		"birth_month" => "int",
		"birth_year" => "int",
		"phone_home" => "string",
		"phone_mobile" => "string",
		"phone_work" => "string",
		"email" => "string",
		"email_2" => "string",
		"email_primary" => "int",
		"ahv" => "string",
		"zsr" => "string",
		"egk" => "string",
		"experience" => "string",
		"specialty" => "string",
		"career" => "string",
		"hasTariff590" => "bool",
		"active" => "bool",
		"active_from" => "date",
		"active_to" => "date",
		"role_id" => "int",
		"client_id" => "int",
	);

	public static $fileattrs = array(
		"foto" => ["image", "one"],
	);

	public static $relations = array(
		[
			"type" => "manyToMany",
			"alias" => "locations",
			"aliasOneObj" => "location",
			"throughAlias" => "locationsEmployees",
			"throughModel" => "Models\V1\Relations\LocationsEmployees",
			"throughIdAlias" => "employee_id",
			"throughRelIdAlias" => "location_id",
			"relationModel" => "Models\V1\Loc\Locations"
		],
		[
			"type" => "manyToMany",
			"alias" => "products",
			"aliasOneObj" => "product",
			"throughAlias" => "employeesProducts",
			"throughModel" => "Models\V1\Relations\EmployeesProducts",
			"throughIdAlias" => "employee_id",
			"throughRelIdAlias" => "product_id",
			"relationModel" => "Models\V1\Prod\Products"
		],
		[
			"type" => "manyToMany",
			"alias" => "groups",
			"aliasOneObj" => "group",
			"throughAlias" => "groupsEmployees",
			"throughModel" => "Models\V1\Relations\GroupsEmployees",
			"throughIdAlias" => "employee_id",
			"throughRelIdAlias" => "group_id",
			"relationModel" => "Models\V1\Cust\Groups"
		],
		/*
		 * Not needed by now, commented out to save space and time
		[
			"type" => "hasMany",
			"alias" => "bookings",
			"relationModel" => "Models\V1\Booking\Bookings"
		]
		*/
	);

	/**
	* Independent Column Mapping.
	*/
	 public function columnMap()
	{
		return array(
		'id' => 'id',
		'firstname' => 'firstname',
		'name' => 'name',
		'address' => 'address',
		'gender' => 'gender',
		'foto' => 'foto',
		'address_line' => 'address_line',
		'street' => 'street',
		'zip' => 'zip',
		'city' => 'city',
		'state' => 'state',
		'country' => 'country',
		'birth_day' => 'birth_day',
		'birth_month' => 'birth_month',
		'birth_year' => 'birth_year',
		'phone_home' => 'phone_home',
		'phone_mobile' => 'phone_mobile',
		'phone_work' => 'phone_work',
		'email' => 'email',
		'email_2' => 'email_2',
		'email_primary' => 'email_primary',
		'ahv' => 'ahv',
		'zsr' => 'zsr',
		'egk' => 'egk',
		'experience' => 'experience',
		'specialty' => 'specialty',
		'career' => 'career',
		'hasTariff590' => 'hasTariff590',
		'active' => 'active',
		'active_from' => 'active_from',
		'active_to' => 'active_to',
		'role_id' => 'role_id',
		'client_id' => 'client_id',
		);
	}

	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_org_employees");

		$this->hasMany(
			"id",
			"Models\V1\Relations\LocationsEmployees",
			"employee_id",
			[
				"alias" => "locationsEmployees",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Relations\LocationsEmployees",
			"employee_id",
			"location_id",
			"Models\V1\Loc\Locations",
			"id",
			[
				"alias" => "locations",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Relations\EmployeesProducts",
			"employee_id",
			[
				"alias" => "employeesProducts",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Relations\EmployeesProducts",
			"employee_id",
			"product_id",
			"Models\V1\Prod\Products",
			"id",
			[
				"alias" => "products",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Relations\GroupsEmployees",
			"employee_id",
			[
				"alias" => "groupsEmployees",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Relations\GroupsEmployees",
			"employee_id",
			"group_id",
			"Models\V1\Cust\Groups",
			"id",
			[
				"alias" => "groups",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Booking\Bookings",
			"employee_id",
			[
				"alias" => "bookings",
			]
		);

	}

	public function getPrimaryMail() {
		if(empty($this->email) && empty($this->email_second)) return "";
		if(empty($this->email)) return $this->email_second;
		if(empty($this->email_second)) return $this->email;
		if(empty($this->email_primary)) return $this->email;

		switch((int)$this->email_primary) {
			case 1:
				return $this->email;
			case 2:
				return $this->email_second;
			default:
				return "";
		}

		return "";
	}

	/* ----------------------------------------------------------------------------------
	 * Does the Employee work at a specific location?
	 * Returns: boolean
	 */
	public function worksAtLocation(int $location_id)
	{
		foreach($this->locations as $loc) {
			if((int)$loc->id == $location_id) {
				return true;
			}
		}
		return false;
	}

}
