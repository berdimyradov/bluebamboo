<?php

namespace Api\V1\Cust\Controllers;

class ReportsController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Cust\Reports";

	/* -----------------------------------------------------------------------------------
	 * index action
	 */
	public function indexAction () {
		// site id or similar filter? just about access rights
		$default_projection     = array("o.id", "o.text", "o.comment", "o.image", "o.client_id");
		$orderby                = array("o.name", "o.id");
		$attrs_for_projection	= array("o.id", "o.text", "o.comment", "o.image", "o.client_id");
		$attrs_for_where        = array("o.id", "o.client_id");
		$attrs_for_orderby      = array("o.id", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * index lookup action
	 */
	public function indexLookupAction () {
		$projection			= array("o.id");
		$lookup_fields			= array("id");
		$orderby			= array("o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->title = "";
			$l->title .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/* -----------------------------------------------------------------------------------
	 * one action
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneActionRelations($model, $id, $model::$relations);
	}

	/* -----------------------------------------------------------------------------------
	 * create action
	 */
	public function createAction() {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);
		$model = $this->from;
		return $this->_createActionRelations($model, $model::$attrs, $model::$relations);
	}

	/* -----------------------------------------------------------------------------------
	 * update action
	 */
	public function updateAction($id) {
		$model = $this->from;
		// $o = $this->request->getJsonRawBody();
		// $this->logger->log(json_encode($o,JSON_PRETTY_PRINT));
		return $this->_updateAction($model, $model::$attrs, $id);
	}

	/* -----------------------------------------------------------------------------------
	 * delete action
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

}
