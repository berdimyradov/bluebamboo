<?php

namespace Api\V1\Cust\Controllers;

class CustomersController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Cust\Customers";

	/* -----------------------------------------------------------------------------------
	 * index action
	 */
	public function indexAction () {
		// site id or similar filter? just about access rights
		$default_projection     = array("o.id", "o.nr", "o.type", "o.gender", "o.firstname", "o.name", "o.address", "o.address_you", "o.foto", "o.street", "o.zip", "o.city", "o.phone_home", "o.phone_mobile", "o.phone_work", "o.email", "o.email_second", "o.email_primary", "o.birth_day", "o.birth_month", "o.birth_year", "o.known_from", "o.comment", "o.insurance_nr", "o.insurance_id", "o.guardian_id", "o.caregiver_id", "o.dossier_id", "o.client_id");
		$orderby                = array("o.name", "o.firstname", "o.nr");
		$attrs_for_projection	= array("o.id", "o.nr", "o.type", "o.gender", "o.firstname", "o.name", "o.address", "o.address_you", "o.foto", "o.street", "o.zip", "o.city", "o.phone_home", "o.phone_mobile", "o.phone_work", "o.email", "o.email_second", "o.email_primary", "o.birth_day", "o.birth_month", "o.birth_year", "o.known_from", "o.comment", "o.insurance_nr", "o.insurance_id", "o.guardian_id", "o.caregiver_id", "o.dossier_id", "o.client_id");
		$attrs_for_where        = array("o.id", "o.nr", "o.type", "o.gender", "o.firstname", "o.name", "o.address", "o.address_you", "o.street", "o.zip", "o.city", "o.phone_home", "o.phone_mobile", "o.phone_work", "o.email", "o.email_second", "o.birth_day", "o.birth_month", "o.birth_year", "o.insurance_nr", "o.insurance_id", "o.guardian_id", "o.caregiver_id", "o.dossier_id", "o.client_id");
		$attrs_for_orderby      = array("o.id", "o.nr", "o.type", "o.gender", "o.firstname", "o.name", "o.address", "o.address_you", "o.street", "o.zip", "o.city", "o.phone_home", "o.phone_mobile", "o.phone_work", "o.email", "o.email_second", "o.birth_day", "o.birth_month", "o.birth_year", "o.insurance_nr", "o.insurance_id", "o.guardian_id", "o.caregiver_id", "o.dossier_id", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * index lookup action
	 */
	public function indexLookupAction () {
		$projection			= array("o.id", "o.nr", "o.name", "o.firstname");
		$lookup_fields			= array("name", "firstname");
		$orderby			= array("o.name", "o.firstname", "o.nr", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->title = "";
			$l->title .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/* -----------------------------------------------------------------------------------
	 * one action
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->jsonWriteRequest(function(&$transaction) use ($model, $id, $postLoad) {
			$obj = $this->_oneActionObj($model, $id, $postLoad);
			$relations = array(
				$model::$relations["guardian"],
				$model::$relations["caregiver"],
				// $model::$relations["groups"],
			);
			if(!isset($obj->nr) || $obj->nr == 0) {
				$obj = $this->addCustNr($obj->id);
				$transaction = $this->transaction;
				$transaction->commit();
			}
			return $this->_getActionAddRelations($obj, $relations);
		});
	}

	/* -----------------------------------------------------------------------------------
	 * create action
	 */
	public function createAction() {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);

		return $this->jsonWriteRequest(function(&$transaction)
		{
			$transaction = $this->transaction;
			$data = $this->request->getJsonRawBody();
			$model = $this->from;
			$values = array();
			$values['nr'] = $this->createCustNr();

			/*
			if(isset($data->foto)) {
				if (!$this->_moveFileFromTemp($data->foto)) {
					throw new \Exception("Unable to move image file");
				}
			}
			*/

			$res = $this->_createActionObj($data, $model, $model::$attrs, $values);
			// Create standard case for new customer
			$this->createStandardCase($res->id);

			$transaction->commit();
			return $this->_getActionAddRelations(
				$this->_valueConversionPostload($res, $model::$attrs), 
				[
					$model::$relations["caregiver"],
					$model::$relations["guardian"]
				]
			);
		});
	}

	/* -----------------------------------------------------------------------------------
	 * import action
	 */
	public function importAction() {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);
		if ($this->session->get('auth-identity-user') != "14") throw new \Exception(403);

		return $this->jsonWriteRequest(function(&$transaction) {


			$transaction = $this->transaction;

			error_log("imported");


			$transaction->commit();
		});
	}

	/* -----------------------------------------------------------------------------------
	 * update action
	 */
	public function updateAction($id) {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);

		return $this->jsonWriteRequest(function(&$transaction) use ($id) {
			$transaction = $this->transaction;
			$model = $this->from;
			$attrs = $model::$attrs;
			$data = $this->request->getJsonRawBody();
			/*
			$cust = $this->_oneActionObj($model, $pk);
			if(isset($data->foto) && $data->foto != "") {
				if(isset($cust->foto) && $cust->foto != "") {
					if($cust->foto != $data->foto) {
						$this->_updateImage($cust->foto, $data->foto);
					}
				} else {
					$this->_moveFileFromTemp($data->foto);
				}
			} else if(isset($cust->foto) && $cust->foto != "") {
				$this->_deleteImage($cust->foto);
			}
			*/
			$res = $this->_updateActionObj($data, $model, $attrs, $id);
			// create customer nr if customer does not have one
			if(empty($res->nr)) {
				$res = $this->addCustNr($res->id);
			}
			// Create standard case if customer does not have one
			if(empty($res->cases) || count($res->cases) == 0) {
				$this->createStandardCase($res->id);
			}

			$transaction->commit();
			return $this->_getActionAddRelations(
				$this->_valueConversionPostload($res, $model::$attrs), 
				[
					$model::$relations["caregiver"],
					$model::$relations["guardian"]
				]
			);
			// return $this->_valueConversionPostload($res, $model::$attrs);
		});
	}

	/* -----------------------------------------------------------------------------------
	 * delete action
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

	/* -----------------------------------------------------------------------------------
	 * Create a new Client related Customer Nr.
	 */
	private function createCustNr() {
		$model = $this->from;
		return $model::createNewCustNr();
	}

	/* -----------------------------------------------------------------------------------
	 * Add a Customer Number to a Customer entry
	 */
	private function addCustNr($id) {
		$nummer = $this->createCustNr();
		$model = $this->from;

		$valObj = new \stdClass();
		$valObj->nr = $nummer;

		$obj = $this->_updateActionObj($valObj, $model, $model::$attrs, $id);

		return $obj;

		/*
		$res = $entryObj;
		if(!isset($res->nr)) {
			$number = $this->createCustNr();
			$res->nr = $number;
			if ($res->update()) {
				$this->obj = $res;
				return $res;
			}
			else {
				$ret = new \stdClass();
				$ret->status = "error";
				$ret->errors = array();
				foreach ($res->getMessages() as $msg) {
					$m = new \stdClass();
					$m->type = $msg->getType();
					$m->message = $msg->getMessage();
					$m->field = $msg->getField();
					$ret->errors[] = $m;
				}
				$this->rollback("update failed", $ret);
			}
		}
		elseif ($res->nr == 0) {
			$number = $this->createCustNr();
			$res->nr = $number;
			if ($res->update()) {
				$this->obj = $res;
				return $res;
			}
			else {
				$ret = new \stdClass();
				$ret->status = "error";
				$ret->errors = array();
				foreach ($res->getMessages() as $msg) {
					$m = new \stdClass();
					$m->type = $msg->getType();
					$m->message = $msg->getMessage();
					$m->field = $msg->getField();
					$ret->errors[] = $m;
				}
				$this->rollback("update failed", $ret);
			}
		}
		return $res;
		*/
	}

	/* -----------------------------------------------------------------------------------
	 * Create one standard case on Customer creation / update without a case
	 */
	private function createStandardCase($customer_id)
	{
		$modelCases = "Models\V1\Cust\Cases";

		$case = new \stdClass();
		$case->title = "Basisfall - bitte anpassen!";
		$case->therapy_type = 1;
		$case->type = 6;
		$case->payment_type = 1;
		$case->comment = "Automatisch bei Kundenerstellung erzeugter Fall. Bitte passen Sie die Falldaten an!";
		$case->isOpen = 1;
		$case->isCurrent = 1;
		$case->customer_id = $customer_id;

		$res = $this->_createActionObj($case, $modelCases, $modelCases::$attrs);

		return $res;
	}
}
