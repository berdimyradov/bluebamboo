<?php

namespace Api\V1\Cust\Controllers;

class DossiersController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Cust\Dossiers";

	/* -----------------------------------------------------------------------------------
	 * index action
	 */
	public function indexAction () {
		// site id or similar filter? just about access rights
		$default_projection     = array("o.id", "o.medical_history", "o.medicines", "o.allergies", "o.image", "o.client_id");
		$orderby                = array("o.id");
		$attrs_for_projection	= array("o.id", "o.medical_history", "o.medicines", "o.allergies", "o.image", "o.client_id");
		$attrs_for_where        = array("o.id", "o.client_id");
		$attrs_for_orderby      = array("o.id", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * index lookup action
	 */
	public function indexLookupAction () {
		$projection			= array("o.id");
		$lookup_fields			= array("id");
		$orderby			= array("o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->title = "";
			$l->title .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/* -----------------------------------------------------------------------------------
	 * one action
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneAction($model, $id, $postLoad);
	}


	/* -----------------------------------------------------------------------------------
	 * one action by Customer
	 */
	public function oneByCustomerAction($id) {
		return $this->jsonReadRequest(function(&$transaction) use ($id) {
			$model = $this->from;
			$postLoad = function ($obj) {
				$model = $this->from;
				return $this->_valueConversionPostload($obj, $model::$attrs);
			};
			$modelCustomers = "Models\V1\Cust\Customers";
			$cust = $this->_oneActionObj($modelCustomers, $id);

			if(!isset($cust->dossier) || $cust->dossier == null) {

				$values = new \stdClass();
				$dossier = $this->_createActionObj($values, $model, $model::$attrs);
				$cust->dossier_id = $dossier->id;

				if (!$cust->update()) {
					$res = new \stdClass();
					$res->status = "error";
					$res->errors = array();
					foreach ($obj->getMessages() as $msg) {
						$m = new \stdClass();
						$m->type = $msg->getType();
						$m->message = $msg->getMessage();
						$m->field = $msg->getField();
						$res->errors[] = $m;
					}
					$this->rollback("update failed", $res);
				}
			}

			// load Dossier
			$d_id = $cust->dossier_id;
			$obj = $this->_oneActionObj($model, $d_id, $postLoad);
			$retArr = $obj->toArray();

			// Add customer to returnArray
			$retArr["customer"] = $cust;

			// load and add Cases
			$retArr["cases"] = $this->loadCustomerCases($id);

			// load and add unassigned bookings
			$addBookings = array();
			$bookings = $this->loadUnassignedCustomerBookings($id);
			foreach($bookings as $booking) {
				$addBookings[] = $booking->toArray();
			}
			$retArr["bookings"] = $addBookings;

			$transaction = $this->transaction;
			$transaction->commit();
			return $retArr;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * create action

	public function createAction() {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);
		$model = $this->from;
		return $this->_createAction($model, $model::$attrs, $values);
	}
	 */

	/* -----------------------------------------------------------------------------------
	 * update action
	 */
	public function updateAction($id) {
		$model = $this->from;
		// $o = $this->request->getJsonRawBody();
		// $this->logger->log(json_encode($o,JSON_PRETTY_PRINT));
		return $this->_updateAction($model, $model::$attrs, $id);
	}

	/* -----------------------------------------------------------------------------------
	 * delete action
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

	/* -----------------------------------------------------------------------------------
	 * load Cases for Customer action
	 */
	public function loadCustomerCases($customer_id) {
		$model = "Models\V1\Cust\Cases";
		$objs = $model::findByCustomer_id($customer_id);

		if(count($objs) > 0) {
			return $objs;
		} else {
			return array();
		}
	}

	/* -----------------------------------------------------------------------------------
	 * load Unassigned Bookings for Customer
	 */
	public function loadUnassignedCustomerBookings($customer_id) {
		$client_id = $this->grantAccessForClient();
		$model = "Models\V1\Booking\Bookings";

		// Build Search and order query
		$search = array();
		$searchConditions  = "client_id = " . $client_id; // only bookings for this client
		$searchConditions .= " AND type = 100"; // is the booking a business booking?
		$searchConditions .= " AND customer_id = " . $customer_id; // is a customer choosen?
		$searchConditions .= " AND (date < CURDATE() OR (date = CURDATE() AND endTime < CURTIME()))"; // Is the Booking in the past?
		$searchConditions .= " AND (case_id IS NULL OR case_id = 0)"; // is the Booking assigned
		$search["conditions"] = $searchConditions;
		$search["order"] = "date  DESC, startTime  DESC";

		// Get bookings
		$objs = $model::find($search);

		if(count($objs) > 0) {
			return $objs;
		} else {
			return array();
		}
	}

}
