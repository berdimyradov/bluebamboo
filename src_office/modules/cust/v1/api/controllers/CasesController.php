<?php

namespace Api\V1\Cust\Controllers;

class CasesController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Cust\Cases";

	/* -----------------------------------------------------------------------------------
	 * index action
	 */
	public function indexAction() {
		// site id or similar filter? just about access rights
		$default_projection     = array("o.id", "o.title", "o.type", "o.therapy_type", "o.payment_type", "o.law", "o.diagnosis_type", "o.diagnosis_text", "o.comment", "o.comment_invoice", "o.referral_type", "o.referral_name", "o.referral_gln", "o.referral_zsr", "o.accident_date", "o.image", "o.isOpen", "o.isCurrent", "o.customer_id", "o.client_id");
		$orderby                = array("o.id");
		$attrs_for_projection	= array("o.id", "o.title", "o.type", "o.therapy_type", "o.payment_type", "o.law", "o.diagnosis_type", "o.diagnosis_text", "o.comment", "o.comment_invoice", "o.referral_type", "o.referral_name", "o.referral_gln", "o.referral_zsr", "o.accident_date", "o.image", "o.isOpen", "o.isCurrent", "o.customer_id", "o.client_id");
		$attrs_for_where        = array("o.id", "o.title", "o.type", "o.therapy_type", "o.payment_type", "o.law", "o.diagnosis_type", "o.referral_type", "o.referral_name", "o.referral_gln", "o.referral_zsr", "o.accident_date", "o.image", "o.isOpen", "o.isCurrent", "o.customer_id", "o.client_id");
		$attrs_for_orderby      = array("o.id", "o.title", "o.type", "o.therapy_type", "o.payment_type", "o.law", "o.diagnosis_type", "o.referral_type", "o.referral_name", "o.referral_gln", "o.referral_zsr", "o.accident_date", "o.image", "o.isOpen", "o.isCurrent", "o.customer_id", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/* -----------------------------------------------------------------------------------
	 * index lookup action
	 */
	public function indexLookupAction() {
		$projection			= array("o.id", "o.title");
		$lookup_fields			= array("title");
		$orderby			= array("o.title", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->title = "";
			$l->title .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/* -----------------------------------------------------------------------------------
	 * one action
	 */
	public function oneAction($id) {
		return $this->jsonReadRequest(function(&$transaction) use ($id) {
			$model = $this->from;
			$transaction = $this->transaction;
			$postLoad = function ($obj) {
				$model = $this->from;
				return $this->_valueConversionPostload($obj, $model::$attrs);
			};
			$relations = $model::$relations;
			$obj = $this->_oneActionObj($model, $id, $postLoad);
			$bookings = $obj->getBookings(["order" => "date  DESC, startTime  DESC"])->toArray();
			$relobj = $this->_getActionAddRelations($obj, $relations);
			$relobj["bookings"] = $bookings;
			return $relobj;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * one action
	 */
	public function oneCurrentByCustomerAction($id) {
		return $this->jsonReadRequest(function(&$transaction) use ($id) {
			$model = $this->from;
			$postLoad = function ($obj) {
				$model = $this->from;
				return $this->_valueConversionPostload($obj, $model::$attrs);
			};
			$client_id = $this->grantAccessForClient();

			// Conditions for searching the template
			$searchConditions  = "client_id = " . $client_id; // search for Client ID
			$searchConditions .= " AND isCurrent = 1"; // only the default Template
			$searchConditions .= " AND customer_id = " . $id;

			$obj = $model::findFirst(["conditions" => $searchConditions]);

			if($obj == null) {
				$obj = new \stdClass();
			}
			else {
				$obj = $postLoad($obj);
			}

			$relobj = $this->_getActionAddRelations($obj, $model::$relations);
			return $relobj;
		});

	}

	/* -----------------------------------------------------------------------------------
	 * create action
	 */
	public function createAction() {
		return $this->jsonWriteRequest(function(&$transaction)
		{
			$this->grantAccessForClient();
			$model = $this->from;
			$data = $this->request->getJsonRawBody();
			if(isset($data->isCurrent) && $data->isCurrent) {
				// Wenn dieser aktuell -> alle anderen auf unaktuell setzen!
				if(!empty($data->id)) {
					$this->unsetOtherCurrentExcept($data->id, $data->customer_id);
				}
			}
			$res = $this->_createActionObj($data, $model, $model::$attrs);
			$transaction = $this->transaction;
			$transaction->commit();
			return $res;
		});
	}

	/* -----------------------------------------------------------------------------------
	 * update action
	 */
	public function updateAction($id) {
		return $this->jsonWriteRequest(function(&$transaction) use ($id) {
			$this->grantAccessForClient();
			$model = $this->from;
			$data = $this->request->getJsonRawBody();
			if(isset($data->isCurrent) && $data->isCurrent) {
				// Wenn dieser aktuell -> alle anderen auf unaktuell setzen!
				$this->unsetOtherCurrentExcept($data->id, $data->customer_id);
			}
			$res = $this->_updateActionObj($data, $model, $model::$attrs, $id);
			$transaction = $this->transaction;
			$transaction->commit();
			$res = $this->_valueConversionPostload($res, $model::$attrs);
			return $this->_getActionAddRelations($res, $model::$relations);
		});
	}

	/* -----------------------------------------------------------------------------------
	 * delete action
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

	private function unsetOtherCurrentExcept($id, $customer_id) {
		$model = $this->from;
		$client_id = $this->grantAccessForClient();

		// Conditions for searching current cases
		$searchConditions  = "client_id = " . $client_id; // search for Client ID
		$searchConditions .= " AND isCurrent = 1"; // only the default Template
		$searchConditions .= " AND customer_id = " . $customer_id;

		$objs = $model::find(["conditions" => $searchConditions]);

		foreach($objs as $obj) {
			if ($obj->id == $id) continue;
			$obj->isCurrent = 0;
			if(!$obj->update()) {
				$res = new \stdClass();
				$res->status = "error";
				$res->errors = array();
				foreach ($obj->getMessages() as $msg) {
					$m = new \stdClass();
					$m->type = $msg->getType();
					$m->message = $msg->getMessage();
					$m->field = $msg->getField();
					$res->errors[] = $m;
				}
				$this->rollback("Unset Current Case Failed", $res);
			}
		}
	}

}
