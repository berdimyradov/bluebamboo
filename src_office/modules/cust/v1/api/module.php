<?php

namespace Api\V1\Cust;

use Phalcon\Loader,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\View,
    Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface
{
	public function registerAutoloaders(\Phalcon\DiInterface $dependencyInjector = null)
	{
		$loader = new Loader();
		$loader->registerNamespaces(
			array(
                'GSCLibrary'					=> '../library/',

                'Models\V1\Booking'				=> '../modules/booking/v1/models/',
                'Models\V1\Cust'				=> '../modules/cust/v1/models/',
                'Models\V1\Invo'				=> '../modules/invo/v1/models/',
                'Models\V1\Relations'			=> '../modules/relations/v1/models/',
                'Models\V1\Sys'					=> '../modules/sys/v1/models/',
				'Models\V1\Org'					=> '../modules/org/v1/models/',
				'Models\V1\Prod'				=> '../modules/prod/v1/models/',

                'Api\V1\Cust\Logic' 			=> '../modules/cust/v1/api/logic/',
                'Api\V1\Cust\Controllers' 		=> '../modules/cust/v1/api/controllers/',
			)
		);
		$loader->register();
	}

    public function registerServices(\Phalcon\DiInterface $di)
    {
        $di->set('dispatcher', function() {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace("Api\V1\Cust\Controllers");
            return $dispatcher;
        });
        /*
        */
        $di->set('view', function() {
            $view = new View();
            $view->setViewsDir('../app/api/v1/cust/views/');
            return $view;
        });
    }
}
