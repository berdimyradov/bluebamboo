<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$api = new RouterGroup(array('module' => 'api_v1_cust'));
$api->setPrefix('/api/v1/cust');

$api->addGet	("/customers", array('controller' => 'Customers', 'action' => 'index'));
$api->addGet	("/customers/:int", array('controller' => 'Customers', 'action' => 'one', 'id' => 1));
$api->addPost	("/customers", array('controller' => 'Customers', 'action' => 'create'));
$api->addPut	("/customers/:int", array('controller' => 'Customers', 'action' => 'update', 'id' => 1));
$api->addDelete	("/customers/:int", array('controller' => 'Customers', 'action' => 'delete', 'id' => 1));

$api->addGet	("/groups", array('controller' => 'Groups', 'action' => 'index'));
$api->addGet	("/groups/:int", array('controller' => 'Groups', 'action' => 'one', 'id' => 1));
$api->addPost	("/groups", array('controller' => 'Groups', 'action' => 'create'));
$api->addPut	("/groups/:int", array('controller' => 'Groups', 'action' => 'update', 'id' => 1));
$api->addDelete	("/groups/:int", array('controller' => 'Groups', 'action' => 'delete', 'id' => 1));

$api->addGet	("/reports", array('controller' => 'Reports', 'action' => 'index'));
$api->addGet	("/reports/:int", array('controller' => 'Reports', 'action' => 'one', 'id' => 1));
$api->addPost	("/reports", array('controller' => 'Reports', 'action' => 'create'));
$api->addPut	("/reports/:int", array('controller' => 'Reports', 'action' => 'update', 'id' => 1));
$api->addDelete	("/reports/:int", array('controller' => 'Reports', 'action' => 'delete', 'id' => 1));

$api->addGet	("/cases", array('controller' => 'Cases', 'action' => 'index'));
//$api->addGet	("/cases/customer/:int", array('controller' => 'Cases', 'action' => 'indexByCustomer', 'id' => 1));
$api->addGet	("/cases/:int", array('controller' => 'Cases', 'action' => 'one', 'id' => 1));
$api->addGet	("/cases/current/:int", array('controller' => 'Cases', 'action' => 'oneCurrentByCustomer', 'id' => 1));
$api->addPost	("/cases", array('controller' => 'Cases', 'action' => 'create'));
$api->addPut	("/cases/:int", array('controller' => 'Cases', 'action' => 'update', 'id' => 1));
$api->addDelete	("/cases/:int", array('controller' => 'Cases', 'action' => 'delete', 'id' => 1));

$api->addGet	("/dossiers", array('controller' => 'Dossiers', 'action' => 'index'));
$api->addGet	("/dossiers/:int", array('controller' => 'Dossiers', 'action' => 'one', 'id' => 1));
$api->addGet	("/dossiers/customer/:int", array('controller' => 'Dossiers', 'action' => 'oneByCustomer', 'id' => 1));
//$api->addPost	("/dossiers", array('controller' => 'Dossiers', 'action' => 'create'));
$api->addPut	("/dossiers/:int", array('controller' => 'Dossiers', 'action' => 'update', 'id' => 1));
$api->addDelete	("/dossiers/:int", array('controller' => 'Dossiers', 'action' => 'delete', 'id' => 1));

$api->addGet	("/import", array('controller' => 'Customers', 'action' => 'import'));

$router->mount($api);
