<?php

namespace Models\V1\Cust;

class Cases extends \Phalcon\Mvc\Model
{

	public $id;
	public $title;
	public $type;
	public $therapy_type;
	public $payment_type;
	public $law;
	public $diagnosis_type;
	public $diagnosis_text;
	public $comment;
	public $comment_invoice;
	public $referral_type;
	public $referral_name;
	public $referral_gln;
	public $referral_zsr;
	public $accident_date;
	public $image;
	public $isOpen;
	public $isCurrent;
	public $customer_id;
	public $client_id;

	const DIAGNOSIS_TYPES = array(
		'TEXT'			=> 1,
		'ICD-10'		=> 2,
		'ICPC'			=> 3,
		'TESSINER_CODE'	=> 4,
	);

	const REFERRAL_TYPES = array(
		'DOCTOR' => 1,
		'THERAPIST' => 2,
		'PSYCHOLOGIST' => 3,
		'PSYCHIATRIST' => 4,
		'OTHER' => 5,
	);

	const LAWS = array(
		'KVG' => 1,
		'UVG' => 2,
		'IVG' => 3,
		'MVG' => 4,
		'VVG' => 5,
		'ORG' => 6,
	);

	public static $attrs = array(
		"id" => "int",
		"title" => "string",
		"type" => "int",
		"therapy_type" => "int",
		"payment_type" => "int",
		"law" => "int",
		"diagnosis_type" => "int",
		"diagnosis_text" => "string",
		"comment" => "string",
		"comment_invoice" => "string",
		"referral_type" => "int",
		"referral_name" => "string",
		"referral_gln" => "string",
		"referral_zsr" => "string",
		"accident_date" => "date",
		"image" => "string",
		"isOpen" => "bool",
		"isCurrent" => "bool",
		"customer_id" => "int",
		"client_id" => "int",
	);

	public static $fileattrs = array(
		"image" => ["image", "one"],
	);

	public static $relations = array(
		[
			"type" => "belongsTo",
			"alias" => "customer",
			"relationModel" => "Models\V1\Cust\Customers",
			"relationIdAlias" => "customer_id"
		],
		[
			"type" => "hasMany",
			"alias" => "bookings",
			"relationModel" => "Models\V1\Booking\Bookings",
			"relationIdAlias" => "case_id"
		],
		[
			"type" => "manyToMany",
			"alias" => "reports",
			"aliasOneObj" => "report",
			"throughAlias" => "bookings",
			"throughModel" => "Models\V1\Booking\Bookings",
			"throughIdAlias" => "case_id",
			"throughRelIdAlias" => "report_id",
			"relationModel" => "Models\V1\Cust\Reports"
		],
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
			"id" => "id",
			"title" => "title",
			"type" => "type",
			"therapy_type" => "therapy_type",
			"payment_type" => "payment_type",
			"law" => "law",
			"diagnosis_type" => "diagnosis_type",
			"diagnosis_text" => "diagnosis_text",
			"comment" => "comment",
			"comment_invoice" => "comment_invoice",
			"referral_type" => "referral_type",
			"referral_name" => "referral_name",
			"referral_gln" => "referral_gln",
			"referral_zsr" => "referral_zsr",
			"accident_date" => "accident_date",
			"image" => "image",
			"isOpen" => "isOpen",
			"isCurrent" => "isCurrent",
			"customer_id" => "customer_id",
			"client_id" => "client_id",
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_cust_cases");

		$this->belongsTo(
			"customer_id",
			"Models\V1\Cust\Customers",
			"id",
			[
				"alias" => "customer",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Booking\Bookings",
			"case_id",
			[
				"alias" => "bookings",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Booking\Bookings",
			"case_id",
			"report_id",
			"Models\V1\Cust\Reports",
			"id",
			[
				"alias" => "reports",
			]
		);

	}

}
