<?php

namespace Models\V1\Cust;

class Reports extends \Phalcon\Mvc\Model
{

	public $id;
	public $text;
	public $comment;
	public $image;
	public $client_id;

	public static $attrs = array(
		"id" => "int",
		"text" => "string",
		"comment" => "string",
		"image" => "string",
		"client_id" => "int",
	);

	public static $fileattrs = array(
		"image" => ["image", "one"],
	);

	public static $relations = array(
		[
			"type" => "hasOne",
			"alias" => "booking",
			"relationModel" => "Models\V1\Booking\Bookings",
			"relationIdAlias" => "report_id"
		],
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
			'id' => 'id',
			'text' => 'text',
			'comment' => 'comment',
			'image' => 'image',
			'client_id' => 'client_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_cust_reports");

		$this->hasOne(
			"id",
			"Models\V1\Booking\Bookings",
			"report_id",
			[
				"alias" => "booking",
			]
		);
	}

}
