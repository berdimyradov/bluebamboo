<?php

namespace Models\V1\Cust;

class Groups extends \Phalcon\Mvc\Model
{

	/**
	 * @Primary
	 * @Identity
	 * @Column(type="integer", nullable=false)
	 */
	public $id;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $name;

	/**
	 * @Column(type="integer", nullable=false)
	 */
	public $client_id;

	public static $attrs = array(
		"id" => "int",
		"name" => "string",
		"client_id" => "int",
	);

	public static $relations = array(
		[
			"type" => "manyToMany",
			"alias" => "customers",
			"aliasOneObj" => "customer",
			"throughAlias" => "groupsCustomers",
			"throughModel" => "Models\V1\Relations\GroupsCustomers",
			"throughIdAlias" => "group_id",
			"throughRelIdAlias" => "customer_id",
			"relationModel" => "Models\V1\Cust\Customers"
		],
		[
			"type" => "manyToMany",
			"alias" => "employees",
			"aliasOneObj" => "employee",
			"throughAlias" => "groupsEmployees",
			"throughModel" => "Models\V1\Relations\GroupsEmployees",
			"throughIdAlias" => "group_id",
			"throughRelIdAlias" => "employee_id",
			"relationModel" => "Models\V1\Org\Employees"
		],
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
			'id' => 'id',
			'name' => 'name',
			'client_id' => 'client_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_cust_groups");

		$this->hasMany(
			"id",
			"Models\V1\Relations\GroupsCustomers",
			"group_id",
			[
				"alias" => "groupsCustomers",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Relations\GroupsEmployees",
			"group_id",
			[
				"alias" => "groupsEmployees",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Relations\GroupsCustomers",
			"group_id",
			"customer_id",
			"Models\V1\Cust\Customers",
			"id",
			[
				"alias" => "customers",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Relations\GroupsEmployees",
			"group_id",
			"employee_id",
			"Models\V1\Org\Employees",
			"id",
			[
				"alias" => "employees",
			]
		);
	}

}
