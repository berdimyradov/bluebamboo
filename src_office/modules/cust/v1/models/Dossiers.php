<?php

namespace Models\V1\Cust;

class Dossiers extends \Phalcon\Mvc\Model
{

	/**
	 * @Primary
	 * @Identity
	 * @Column(type="integer", nullable=false)
	 */
	public $id;

	public $medical_history;

	public $medicines;

	public $allergies;

	public $image;

	/**
	 * @Column(type="integer", nullable=false)
	 */
	public $client_id;

	public static $attrs = array(
		"id" => "int",
		"medical_history" => "string",
		"medicines" => "string",
		"allergies" => "string",
		"image" => "string",
		"client_id" => "int",
	);

	public static $fileattrs = array(
		"image" => ["image", "one"],
	);

	public static $relations = array(
		[
			"type" => "hasOne",
			"alias" => "customer",
			"relationModel" => "Models\V1\Cust\Customers",
			"relationIdAlias" => "dossier_id"
		],
	);

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
			'id' => 'id',
			'medical_history' => 'medical_history',
			'medicines' => 'medicines',
			'allergies' => 'allergies',
			'image' => 'image',
			'client_id' => 'client_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_cust_dossiers");

		$this->hasOne(
			"id",
			"Models\V1\Cust\Customers",
			"dossier_id",
			[
				"alias" => "customer",
			]
		);
	}

}
