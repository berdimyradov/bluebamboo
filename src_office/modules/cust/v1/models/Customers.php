<?php

namespace Models\V1\Cust;

class Customers extends \Phalcon\Mvc\Model
{

	/**
	 * @Primary
	 * @Identity
	 * @Column(type="integer", nullable=false)
	 */
	public $id;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	public $nr;

	/**
	 * @Column(type="string", nullable=false)
	 */
	public $type;

	/**
	 * @Column(type="boolean", nullable=true)
	 */
	public $gender;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $firstname;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $name;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $address;

	/**
	 * @Column(type="boolean", nullable=false)
	 */
	public $address_you;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $foto;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $address_line;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $street;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $zip;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $city;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $state;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $country;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $phone_home;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $phone_mobile;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $phone_work;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $email;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $email_second;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	public $email_primary;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	public $birth_day;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	public $birth_month;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	public $birth_year;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $known_from;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $comment;

	/**
	 * @Column(type="string", nullable=true)
	 */
	public $insurance_nr;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	public $insurance_id;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	public $guardian_id;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	public $caregiver_id;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	public $dossier_id;

	/**
	 * @Column(type="integer", nullable=false)
	 */
	public $client_id;

	public static $attrs = array(
		"id" => "int",
		"nr" => "int",
		"type" => "int",
		"gender" => "bool",
		"firstname" => "string",
		"name" => "string",
		"address" => "string",
		"address_you" => "bool",
		"foto" => "string",
		"address_line" => "string",
		"street" => "string",
		"zip" => "string",
		"city" => "string",
		"state" => "string",
		"country" => "string",
		"phone_home" => "string",
		"phone_mobile" => "string",
		"phone_work" => "string",
		"email" => "string",
		"email_second" => "string",
		"email_primary" => "int",
		"birth_day" => "int",
		"birth_month" => "int",
		"birth_year" => "int",
		"known_from" => "string",
		"comment" => "string",
		"insurance_nr" => "string",
		"insurance_id" => "int",
		"guardian_id" => "int",
		"caregiver_id" => "int",
		"dossier_id" => "int",
		"client_id" => "int",
	);

	public static $fileattrs = array(
		"foto" => ["image", "one"],
	);

	public static $relations = array(
		"invoices" => [
			"type" => "hasMany",
			"alias" => "invoices",
			"relationModel" => "Models\V1\Invo\Invoices",
			"relationIdAlias" => "customer_id"
		],
		"bookings" => [
			"type" => "hasMany",
			"alias" => "bookings",
			"relationModel" => "Models\V1\Booking\Bookings",
			"relationIdAlias" => "customer_id"
		],
		"guardian" => [
			"type" => "belongsTo",
			"alias" => "guardian",
			"relationModel" => "Models\V1\Cust\Customers",
			"relationIdAlias" => "guardian_id"
		],
		"caregiver" => [
			"type" => "belongsTo",
			"alias" => "caregiver",
			"relationModel" => "Models\V1\Cust\Customers",
			"relationIdAlias" => "caregiver_id"
		],
		"dossier" => [
			"type" => "belongsTo",
			"alias" => "dossier",
			"relationModel" => "Models\V1\Cust\Dossiers",
			"relationIdAlias" => "dossier_id"
		],
		"cases" => [
			"type" => "hasMany",
			"alias" => "cases",
			"relationModel" => "Models\V1\Cust\Cases",
			"relationIdAlias" => "customer_id"
		],
		"groups" => [
			"type" => "manyToMany",
			"alias" => "groups",
			"aliasOneObj" => "group",
			"throughAlias" => "groupsCustomers",
			"throughModel" => "Models\V1\Relations\GroupsCustomers",
			"throughIdAlias" => "customer_id",
			"throughRelIdAlias" => "group_id",
			"relationModel" => "Models\V1\Cust\Groups"
		],
	);

	/*
	 * Create a new Client related Customer Nr.
	 */
	public static function createNewCustNr() {
		// $number = 1;

		$session = \Phalcon\Di::getDefault()->getSession();
		$client_id = $session->get("auth-identity-client");

		$search  = "client_id = " . $client_id;
		$search .= " AND nr IS NOT NULL";

		$number = self::maximum(
			[
				"column"     => "nr",
				"conditions" => $search,
			]
		);
		$number++;

		/*
		$customers = self::find(
			[
				$search,
				"order" => "nr",
			]
		);
		$custCount = count($customers);
		if($custCount > 0) {
			$custCount--;
			$lastCust = $customers[$custCount];
			$number = $lastCust->nr++;
		}
		*/

		return $number;
	 }

	/**
	* Independent Column Mapping.
	*/
	public function columnMap()
	{
		return array(
			'id' => 'id',
			"nr" => "nr",
			'type' => 'type',
			'gender' => 'gender',
			'firstname' => 'firstname',
			'name' => 'name',
			'address' => 'address',
			'address_you' => 'address_you',
			'foto' => 'foto',
			'address_line' => 'address_line',
			'street' => 'street',
			'zip' => 'zip',
			'city' => 'city',
			'state' => 'state',
			'country' => 'country',
			'phone_home' => 'phone_home',
			'phone_mobile' => 'phone_mobile',
			'phone_work' => 'phone_work',
			'email' => 'email',
			'email_second' => 'email_second',
			'email_primary' => 'email_primary',
			'birth_day' => 'birth_day',
			'birth_month' => 'birth_month',
			'birth_year' => 'birth_year',
			'known_from' => 'known_from',
			'comment' => 'comment',
			'insurance_nr' => 'insurance_nr',
			'insurance_id' => 'insurance_id',
			'guardian_id' => "guardian_id",
			"caregiver_id" => "caregiver_id",
			"dossier_id" => "dossier_id",
			'client_id' => 'client_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
	{
		$this->setSource("gsc_cust_customers");

		$this->hasMany(
			"id",
			"Models\V1\Invo\Invoices",
			"customer_id",
			[
				"alias" => "invoices",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Booking\Bookings",
			"customer_id",
			[
				"alias" => "bookings",
			]
		);

		$this->belongsTo(
			"caregiver_id",
			"Models\V1\Cust\Customers",
			"id",
			[
				"alias" => "caregiver",
			]
		);

		$this->belongsTo(
			"guardian_id",
			"Models\V1\Cust\Customers",
			"id",
			[
				"alias" => "guardian",
			]
		);

		$this->belongsTo(
			"dossier_id",
			"Models\V1\Cust\Dossiers",
			"id",
			[
				"alias" => "dossier",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Cust\Cases",
			"customer_id",
			[
				"alias" => "cases",
			]
		);

		$this->hasMany(
			"id",
			"Models\V1\Relations\GroupsCustomers",
			"customer_id",
			[
				"alias" => "groupsCustomers",
			]
		);

		$this->hasManyToMany(
			"id",
			"Models\V1\Relations\GroupsCustomers",
			"customer_id",
			"group_id",
			"Models\V1\Cust\Groups",
			"id",
			[
				"alias" => "groups",
			]
		);
	}

	public function getPrimaryMail() {
		/*
		if(empty($this->email) && empty($this->email_second)) return "";
		if(empty($this->email)) return $this->email_second;
		if(empty($this->email_second)) return $this->email;
		if(empty($this->email_primary)) return $this->email;

		switch((int)$this->email_primary) {
			case 1:
				return $this->email;
			case 2:
				return $this->email_second;
			default:
				return "";
		}

		return "";
		*/
		return self::getCustomerPrimaryMail($this);
	}

	public static function getCustomerPrimaryMail($customer) {
		if(empty($customer->email) && empty($customer->email_second)) return "";
		if(empty($customer->email)) return $customer->email_second;
		if(empty($customer->email_second)) return $customer->email;
		if(empty($customer->email_primary)) return $customer->email;

		if(!isset($customer->email_primary)) return "";
		switch((int)$customer->email_primary) {
			case 1:
				return $customer->email;
			case 2:
				return $customer->email_second;
			default:
				return "";
		}

		return "";
	}

}
