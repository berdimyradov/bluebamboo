<?php

/**
 * Controller class TemplatebasesController
 */

namespace Api\V1\Pdf\Controllers;

class PreviewContentController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Pdf\PreviewContent";

	/**
	 * returns data with the option to select fields, filter results and sort result.
	 * authentication required
	 */
	public function indexAction () {

		// site id or similar filter? just about access rights

		$default_projection		= array("o.id", "o.title", "o.content_json", "o.group_id", "o.role_id");
		$orderby				= array("o.title", "o.id");
		$attrs_for_projection	= array("o.id", "o.title", "o.content_json", "o.group_id", "o.role_id");
		$attrs_for_where		= array("o.id", "o.title", "o.group_id", "o.role_id");
		$attrs_for_orderby		= array("o.id", "o.title", "o.group_id", "o.role_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/**
	 * lookup for index
	 */
	public function indexLookupAction () {
		$projection				= array("o.id", "o.title");
		$lookup_fields			= array("title");
		$orderby				= array("o.title", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->name = "";
			$l->name .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/**
	 * returns one invoice
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		$res = $this->_oneAction($model, $id, $postLoad);
		return $res;
	}

	/**
	 * returns the preview content json
	 */
	public function getAction($id = 0) {
		/*
		return $this->jsonReadRequest(function(&$transaction) {
			$model = $this->from;
			$obj = $model::findFirst();

			if ($obj == null) {
				throw new \Exception("No Preview Content Data in Database", 500);
			}

			if (property_exists($model, "attrs")) {
				$obj = $this->_valueConversionPostload($obj, $model::$attrs);
			}

			return $obj;

		});
		*/
		return $this->jsonReadRequest(function(&$transaction) use ($id) {

			$model = $this->from;
			$modelGroups = "Models\V1\Pdf\Groups";
			$modelRoles = "Models\V1\Pdf\Roles";

			if($id == 0) {
				$rows = $this->modelsManager->createBuilder()
					->columns('content.*, g.*, r.*')
					->from(['content' => $model])
					->leftJoin($modelGroups, "g.id = content.group_id", "g")
					->leftJoin($modelRoles, "r.id = content.role_id", "r")
					->orderBy('content.group_id, content.role_id, content.id')
					->getQuery()
					->execute();
			} else {
				$rows = $this->modelsManager->createBuilder()
					->columns('content.*, g.*, r.*')
					->from(['content' => $model])
					->leftJoin($modelGroups, "g.id = content.group_id", "g")
					->leftJoin($modelRoles, "r.id = content.role_id", "r")
					->where("content.role_id = :rid:", ["rid" => $id])
					->andWhere("content.group_id = r.group_id")
					->orderBy('content.group_id, content.role_id, content.id')
					->getQuery()
					->execute();
			}

			// Create custom structure with bookings associated to customers
			$retArr = array();

			foreach($rows as $row) {
				// Value Conversions and relation additions
				$content = $this->_valueConversionPostload($row->content, $model::$attrs)->toArray();
				$content['group'] = $this->_valueConversionPostload($row->g, $modelGroups::$attrs);
				unset($content['g']);
				$content['role'] = $this->_valueConversionPostload($row->r, $modelRoles::$attrs);
				unset($content['r']);
				$retArr[] = $content;
			}

			if(count($retArr) == 0) {
				return new \stdClass();
			} else {
				return $retArr[0];
			}
		});
	}

	/**
	 * creates a test invoice with the preview content in the temp folder
	 */
	public function createPreviewInvoiceAction() {
		return $this->jsonWriteRequest(function(&$transaction)
		{
			$transaction = $this->transaction;

			$data = $this->request->getJsonRawBody();
			if(empty($data->template)) {
				throw new \Exception("No Template provided", 400);
			}

			$template = $data->template;
			if(is_object($template)) {
				$template = json_encode($template, JSON_PRETTY_PRINT);
			}
			if(empty($template) || !is_string($template)) {
				throw new \Exception("Wrong Template Structure", 400);
			}

			if(empty($data->role_id)) {
				throw new \Exception("No role id provided", 400);
			}

			$id = (int)$data->role_id;

			$model = $this->from;
			$modelGroups = "Models\V1\Pdf\Groups";
			$modelRoles = "Models\V1\Pdf\Roles";
			$rows = $this->modelsManager->createBuilder()
				->columns('content.*')
				->from(['content' => $model])
				->leftJoin($modelRoles, "r.id = " . $id, "r")
				->where("content.role_id = :rid:", ["rid" => $id])
				->andWhere("content.group_id = r.group_id")
				->orderBy('content.group_id, content.role_id, content.id')
				->getQuery()
				->execute();

			if (count($rows) == 0) {
				throw new \Exception("No Preview Content Data in Database", 501);
			}

			$obj = $rows[0];

			if(property_exists($model, "attrs")) {
				$obj = $this->_valueConversionPostload($obj, $model::$attrs);
			}

			// $sysInfo = "\GSCLibrary\SystemInfo";
			// $fn = "TestPDF_" . $sysInfo::uniqueFilename("pdf");
			// $tempPath = $this->getTempPath();

			$fn = "TestPDF_" . \GSCLibrary\SystemInfo::uniqueFilename("pdf");
			$file = $this->getTempPath() . $fn;
			$docGen = new \DocumentGenerator\DocumentGenerator();
			$contentJSON = json_encode($obj->content_json, JSON_PRETTY_PRINT);

			// $docGen->setContent($contentJSON);
			// $docGen->setLayout($template);

			if(!$docGen->createPDF($file, $contentJSON, $template)) {
				throw new \Exception("Preview Invoice could not be created!", 500);
			}

			$res = new \stdClass();
			$res->path = "/api/v1/pdf/download/" . $fn;
			$res->filename = $fn;

			$transaction->commit();
			return $res;
		});
	}

	/* Just hide that - Request Body in Get Action is no good idea
	public function getPreviewPDFAction() {
		try {
			$this->view->disable();
			$data = $this->request->getJsonRawBody();
			if(empty($data->template)) {
				throw new \Exception("No Template provided", 400);
			}

			if(empty($data->role_id)) {
				throw new \Exception("No role id provided", 400);
			}
			$id = (int)$data->role_id;

			$template = $data->template;
			if(is_object($template)) {
				$template = json_encode($template, JSON_PRETTY_PRINT);
			}
			if(empty($template) || !is_string($template)) {
				throw new \Exception("Wrong Template Structure", 400);
			}

			$model = $this->from;
			$rows = $this->modelsManager->createBuilder()
				->columns('content.*')
				->from(['content' => $model])
				->where("content.role_id = :rid:", ["rid" => $id])
				->andWhere("r.id = :id:", ["id" => $id])
				->andWhere("content.group_id = r.group_id")
				->orderBy('content.group_id, content.role_id, content.id')
				->getQuery()
				->execute();

			if (count($rows) == 0) {
				throw new \Exception("No Preview Content Data in Database", 501);
			}

			$obj = $rows[0];
			if (property_exists($model, "attrs")) {
				$obj = $this->_valueConversionPostload($obj, $model::$attrs);
			}
			$contentJSON = json_encode($obj->content_json, JSON_PRETTY_PRINT);

			$file = $this->getTempPath() . "TestPDF_" . \GSCLibrary\SystemInfo::uniqueFilename("pdf");
			$docGen = new \DocumentGenerator\DocumentGenerator();

			if(!$docGen->createPDF($file, $contentJSON, $template)) {
				throw new \Exception("Preview Invoice could not be created!", 500);
			}

			$response = $this->response;
			$response->setHeader('Content-Length', filesize($file));
			$response->setContentType('application/pdf');
			$response->setFileToSend($file);
			!$response->isSent() && $response->send();
		}
		catch (\Exception $err) {
			if ($err->getCode() == 403) {
				$this->response->setStatusCode(403, "Forbidden");
			}
			else {
				$this->response->setStatusCode($err->getCode(), "Bad Request: " . $err->getMessage());
			}
			return $this->response->send();
		}
	}
	*/

	/**
	 * download of a preview pdf
	 */
	public function previewDownloadAction($filename)
	{
		try {
			$this->view->disable();
			//$this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);

			$pdfPath = $this->getTempPath();
			$file = $pdfPath.$filename;

			if(!is_file($file)) {
				throw new \Exception("File not found!", 404);
			}

			$response = $this->response;
			$response->setHeader('Content-Length', filesize($file));
			$response->setContentType('application/pdf');
			$response->setFileToSend($file, null, false);
			$response->setHeader('Content-Disposition', "inline; filename=" . $filename);
			!$response->isSent() && $response->send();
			// Directly delete the pdf file after sending of response
			unlink($file);
		}
		catch (\Exception $err) {
			if ($err->getCode() == 403) {
				$this->response->setStatusCode(403, "forbidden");
			}
			else {
				$this->response->setStatusCode(400, "Bad Request: " . $err->getMessage());
//				$this->logger->log("error: " . json_encode(debug_backtrace(), JSON_PRETTY_PRINT));
//				$this->logger->log("error: " . json_encode($err->getTrace(), JSON_PRETTY_PRINT));
			}
			return $this->response->send();
		}
	}

	/**
	 * creates a new invoice
	 */
	public function createAction() {
		$model = $this->from;
		return $this->_createAction($model, $model::$attrs);
	}

	/**
	 * updates an existing invoice
	 */
	public function updateAction($id) {
		$model = $this->from;
		return $this->_updateAction($model, $model::$attrs, $id);
	}

	/**
	 * deletes an existing invoice
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

}
