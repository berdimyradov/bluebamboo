<?php

/**
 * Controller class RolesController
 */

namespace Api\V1\Pdf\Controllers;

class RolesController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Pdf\Roles";

	/**
	 * returns data with the option to select fields, filter results and sort result.
	 * authentication required
	 */
	public function indexAction () {

		// site id or similar filter? just about access rights

		$default_projection		= array("o.id", "o.title", "o.group_id");
		$orderby				= array("o.title", "o.id");
		$attrs_for_projection	= array("o.id", "o.title", "o.group_id");
		$attrs_for_where		= array("o.id", "o.title", "o.group_id");
		$attrs_for_orderby		= array("o.id", "o.title", "o.group_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/**
	 * lookup for index
	 */
	public function indexLookupAction () {
		$projection				= array("o.id", "o.title");
		$lookup_fields			= array("title");
		$orderby				= array("o.title", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->name = "";
			$l->name .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/**
	 * returns one invoice
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		$res = $this->_oneAction($model, $id, $postLoad);
		return $res;
	}

	/**
	 * creates a new invoice
	 */
	public function createAction() {
		$model = $this->from;
		return $this->_createAction($model, $model::$attrs);
	}

	/**
	 * updates an existing invoice
	 */
	public function updateAction($id) {
		$model = $this->from;
		return $this->_updateAction($model, $model::$attrs, $id);
	}

	/**
	 * deletes an existing invoice
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

}
