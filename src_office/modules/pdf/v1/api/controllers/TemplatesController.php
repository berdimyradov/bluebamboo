<?php

/**
 * Controller class TemplatesController
 */

namespace Api\V1\Pdf\Controllers;

class TemplatesController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Pdf\Templates";

	/**
	 * returns data with the option to select fields, filter results and sort result.
	 * authentication required
	 */
	public function indexAction () {

		// site id or similar filter? just about access rights

		$default_projection		= array("o.id", "o.title", "o.template", "o.active", "o.editable", "o.standard", "o.group_id", "o.role_id", "o.client_id");
		$orderby				= array("o.title", "o.id");
		$attrs_for_projection	= array("o.id", "o.title", "o.template", "o.active", "o.editable", "o.standard", "o.group_id", "o.role_id", "o.client_id");
		$attrs_for_where		= array("o.id", "o.title", "o.template", "o.active", "o.editable", "o.standard", "o.group_id", "o.role_id", "o.client_id");
		$attrs_for_orderby		= array("o.id", "o.title", "o.template", "o.active", "o.editable", "o.standard", "o.group_id", "o.role_id", "o.client_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/**
	 * lookup for index
	 */
	public function indexLookupAction () {
		$projection				= array("o.id", "o.title");
		$lookup_fields			= array("title");
		$orderby				= array("o.title", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->name = "";
			$l->name .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/* -----------------------------------------------------------------------------------
	 * get Action for open invoiceable Bookings in desired structure
	 */
	public function indexAllAction() {
		return $this->jsonReadRequest(function(&$transaction) {

			$model = $this->from;
			$modelGroups = "Models\V1\Pdf\Groups";
			$modelRoles = "Models\V1\Pdf\Roles";

			$client_id = $this->session->get("auth-identity-client");

			$rows = $this->modelsManager->createBuilder()
				->columns('t.*, g.*, r.*')
				->from(['t' => $model])
				->leftJoin($modelGroups, "g.id = t.group_id", "g")
				->leftJoin($modelRoles, "r.id = t.role_id", "r")
				->where("t.client_id = :cid:", ["cid" => $client_id])
				->orderBy('t.group_id, t.role_id, t.id')
				->getQuery()
				->execute();

			// Create custom structure with bookings associated to customers
			$retArr = array();

			foreach($rows as $row) {
				// Value Conversions and relation additions
				$t = $this->_valueConversionPostload($row->t, $model::$attrs)->toArray();
				$t['group'] = $this->_valueConversionPostload($row->g, $modelGroups::$attrs);
				$t['role'] = $this->_valueConversionPostload($row->r, $modelRoles::$attrs);
				$retArr[] = $t;
			}

			return $retArr;
		});
	}

	/**
	 * returns one invoice
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		return $this->_oneActionRelations($model, $id, $model::$relations, $postLoad);
	}

	/**
	 * creates a new invoice
	 */
	public function createAction() {
		if (!$this->session->has('auth-identity-user')) throw new \Exception(403);
		$model = $this->from;
		return $this->_createActionRelations($model, $model::$attrs, $model::$relations);
	}

	/**
	 * updates an existing invoice
	 */
	public function updateAction($id) {
		return $this->jsonWriteRequest(function(&$transaction) use ($id) {

			$modelClass = $this->from;
			$attrs = $modelClass::$attrs;
			$pk = $id;
			$relations = $modelClass::$relations;
			$values = null;
			$valueConverter = null;

			$o = $this->request->getJsonRawBody();

			$testObj = $modelClass::findFirstByid($id);
			if((int)$testObj->editable == 0) {
				// Set group, role and template from old entry if not editable
				$testObj = $this->_valueConversionPostload($testObj, $modelClass::$attrs);
				$o->template = !empty($testObj->template) ? $testObj->template : null;
				$o->group_id = !empty($testObj->group_id) ? $testObj->group_id : null;
				$o->role_id = !empty($testObj->role_id) ? $testObj->role_id : null;
				$o->standard = !empty($testObj->standard) ? $testObj->standard : null;
				$o->editable = 0;
			}

			$obj = $this->_updateActionObj($o, $modelClass, $attrs, $pk, $values, $valueConverter);

			$transaction = $this->transaction;
			$this->transaction->commit();
			return $this->_jsonResponse($this->_getActionAddRelations($obj, $relations));

		});
	}

	/**
	 * deletes an existing invoice
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

}
