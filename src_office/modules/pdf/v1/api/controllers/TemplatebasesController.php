<?php

/**
 * Controller class TemplatebasesController
 */

namespace Api\V1\Pdf\Controllers;

class TemplatebasesController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Pdf\Templatebases";

	/**
	 * returns data with the option to select fields, filter results and sort result.
	 * authentication required
	 */
	public function indexAction () {

		// site id or similar filter? just about access rights

		$default_projection		= array("o.id", "o.title", "o.thumbnail", "o.description", "o.template", "o.editable", "o.standard", "o.group_id", "o.role_id");
		$orderby				= array("o.title", "o.id");
		$attrs_for_projection	= array("o.id", "o.title", "o.thumbnail", "o.description", "o.template", "o.editable", "o.standard", "o.group_id", "o.role_id");
		$attrs_for_where		= array("o.id", "o.title", "o.thumbnail", "o.description", "o.template", "o.editable", "o.standard", "o.group_id", "o.role_id");
		$attrs_for_orderby		= array("o.id", "o.title", "o.thumbnail", "o.description", "o.template", "o.editable", "o.standard", "o.group_id", "o.role_id");

		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};

		return $this->_indexAction($default_projection, $attrs_for_projection, $this->from, $attrs_for_where, $orderby, $attrs_for_orderby, null /*filter*/, $postLoad);
	}

	/**
	 * lookup for index
	 */
	public function indexLookupAction () {
		$projection				= array("o.id", "o.title");
		$lookup_fields			= array("title");
		$orderby				= array("o.title", "o.id");
		$label = function ($obj) {
			$l = new \stdClass();
			$l->id = $obj->id;
			$l->name = "";
			$l->name .= $obj->title;
			return $l;
		};
		return $this->_indexLookupAction($projection, $this->from, $lookup_fields, $orderby, $label);
	}

	/* -----------------------------------------------------------------------------------
	 * get Action for open invoiceable Bookings in desired structure
	 */
	public function indexAllAction() {
		return $this->jsonReadRequest(function(&$transaction) {

			$model = $this->from;
			$modelGroups = "Models\V1\Pdf\Groups";
			$modelRoles = "Models\V1\Pdf\Roles";

			$rows = $this->modelsManager->createBuilder()
				->columns('tbase.id, tbase.title, tbase.group_id, tbase.role_id, tbase.thumbnail, tbase.editable, tbase.description, tbase.standard, g.*, r.*')
				->from(['tbase' => $model])
				->leftJoin($modelGroups, "g.id = tbase.group_id", "g")
				->leftJoin($modelRoles, "r.id = tbase.role_id", "r")
				->orderBy('tbase.group_id, tbase.role_id, tbase.standard, tbase.id')
				->getQuery()
				->execute();

			// Create custom structure with bookings associated to customers
			$retArr = array();

			foreach($rows as $row) {
				// Value Conversions and relation additions
				$tbase = $this->_valueConversionPostload($row, $model::$attrs)->toArray();
				$tbase['group'] = $this->_valueConversionPostload($row->g, $modelGroups::$attrs);
				unset($tbase['g']);
				$tbase['role'] = $this->_valueConversionPostload($row->r, $modelRoles::$attrs);
				unset($tbase['r']);
				$retArr[] = $tbase;
			}

			return $retArr;
		});
	}

	/**
	 * returns one invoice
	 */
	public function oneAction($id) {
		$model = $this->from;
		$postLoad = function ($obj) {
			$model = $this->from;
			return $this->_valueConversionPostload($obj, $model::$attrs);
		};
		$res = $this->_oneActionRelations($model, $id, $model::$relations, $postLoad);
		return $res;
	}

	/**
	 * creates a new invoice
	 */
	public function createAction() {
		$model = $this->from;
		return $this->_createAction($model, $model::$attrs);
	}

	/**
	 * updates an existing invoice
	 */
	public function updateAction($id) {
		$model = $this->from;
		return $this->_updateAction($model, $model::$attrs, $id);
	}

	/**
	 * deletes an existing invoice
	 */
	public function deleteAction($id) {
		$model = $this->from;
		return $this->_deleteAction($model, $id);
	}

}
