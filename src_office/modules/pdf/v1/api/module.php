<?php

/**
 * module-config for V1/API/Pdf API
 */

namespace Api\V1\Pdf;

use Phalcon\Loader,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\View,
    Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface
{
    public function registerAutoloaders(\Phalcon\DiInterface $dependencyInjector = null)
    {
        $loader = new Loader();
        $loader->registerNamespaces(
            array(
                'GSCLibrary'					=> '../library/',
				'DocumentGenerator'				=> '../library/',

                'Models\V1\Pdf'					=> '../modules/pdf/v1/models/',
                'Models\V1\Sys'					=> '../modules/sys/v1/models/',

                'Api\V1\Pdf\Logic' 			=> '../modules/pdf/v1/api/logic/',
                'Api\V1\Pdf\Controllers' 		=> '../modules/pdf/v1/api/controllers/',
            )
        );
        $loader->register();
    }

    public function registerServices(\Phalcon\DiInterface $di)
    {
        $di->set('dispatcher', function() {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace("Api\V1\Pdf\Controllers");
            return $dispatcher;
        });

        $di->set('view', function() {
            $view = new View();
//            $view->setViewsDir('../app/api/v1/pdf/views/');
            return $view;
        });
    }
}
