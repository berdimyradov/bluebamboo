<?php

namespace Api\V1\Pdf\Logic;


class Logic extends \Phalcon\Mvc\User\Component
{


	private $feedback = null;

	/* ---------------------------------------------------------------------------------
	 * constructor
	 */
	public function __construct($di) {
		$this->setDI($di);
	}

	/* ---------------------------------------------------------------------------------
	 * addError
	 */
	protected function addError ($field, $message) {
		if ($this->feedback == null) $this->feedback = array();
		$this->feedback[$field] = $message;
	}

	/* ---------------------------------------------------------------------------------
	 * applyFeedback
	 */
	protected function applyFeedback ($response) {
		if ($this->feedback == null) $response->fields = array();
		else $response->fields = $this->feedback;
	}

	/* ---------------------------------------------------------------------------------
	 * stop processing
	 */
	protected function stopProcessing ($field, $message, $response, $xmessage = "invalid state") {
		$this->addError($field, $message);
		$this->applyFeedback($response);
		throw new \Exception($xmessage);
	}

	/* ---------------------------------------------------------------------------------
	 * init registration insurances
	 */
	public function onRegistration () {
		
		if (!$this->session->has('auth-identity-client')) {
			throw new \Exception("missing client id", 403);
		}
		$clientId = $this->session->get('auth-identity-client');

		$modelbases = "\Models\V1\Pdf\Templatebases";
		$bases = $modelbases::findByEditable(0);

		foreach ($bases as $base) {
			$template = new \Models\V1\Pdf\Templates();
			$template->setTransaction($this->transaction);
			$template->title = $base->title;
			$template->template = $base->template;
			$template->active = 1;
			$template->editable = $base->editable;
			$template->standard = $base->standard;
			$template->group_id = $base->group_id;
			$template->role_id = $base->role_id;
			$template->client_id = $clientId;

			if (!$template->save()) {
				// throw new \Exception("Could not save insurance " . $name . " for Client " . $clientId, 500);
				error_log("Could not save template " . $template->title . " for Client " . $clientId);
			}
		}

	}

}
