<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$api = new RouterGroup(array('module' => 'api_v1_pdf'));
$api->setPrefix('/api/v1/pdf');

$api->addGet	("/templates",					array('controller' => 'Templates',			'action' => 'indexAll'));
$api->addGet	("/templates/:int",				array('controller' => 'Templates',			'action' => 'one',		'id' => 1));
$api->addPost	("/templates",					array('controller' => 'Templates',			'action' => 'create'));
$api->addPut	("/templates/:int",				array('controller' => 'Templates',			'action' => 'update',	'id' => 1));
$api->addDelete	("/templates/:int",				array('controller' => 'Templates',			'action' => 'delete',	'id' => 1));

$api->addGet	("/templates/bases",					array('controller' => 'Templatebases',		'action' => 'indexAll'));
$api->addGet	("/templates/bases/:int",				array('controller' => 'Templatebases',		'action' => 'one', 		'id' => 1));
$api->addGet	("/templates/previewcontent",			array('controller' => 'Previewcontent',		'action' => 'get'));
$api->addGet	("/templates/previewcontent/r/:int",	array('controller' => 'Previewcontent',		'action' => 'get', 		'id' => 1));

$api->addGet	("/templates/groups/:int",	array('controller' => 'Groups',			'action' => 'one',		'id' => 1));
//$api->addGet	("/templates/previewpdf",				array('controller' => 'Previewcontent',		'action' => 'getPreviewPDF'));//

// Download Route for Preview PDFs
$api->addPost("/templates/previewpdf", array('controller' => 'Previewcontent', 'action' => 'createPreviewInvoice'));
$api->addGet("/download/([a-zA-ZöäüÖÄÜ0-9_/\.\-]+)", array('controller'	=> 'Previewcontent', 'action' => 'previewDownload', 'file' => 1));

$router->mount($api);
