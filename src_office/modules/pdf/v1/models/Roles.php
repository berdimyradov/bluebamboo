<?php

namespace Models\V1\Pdf;

class Roles extends \Phalcon\Mvc\Model
{

	public $id;

	public $title;

	public $group_id;

	const ROLES = array(
		"INVOICE" => 1,
		"REMINDER_1" => 2,
		"REMINDER_2" => 3,
		"RECLAIM_SLIP" => 4,
	);

	public static $relations = array(
		"role" => [
			"type" => "belongsTo",
			"alias" => "group",
			"relationModel" => "Models\V1\Pdf\Groups",
			"relationIdAlias" => "group_id"
		],
	);

	public static $attrs = array(
		'id' => 'int',
		'title' => 'string',
		'group_id' => 'int',
	);

	public function columnMap()
	{
		return array(
			'id' => 'id',
			'title' => 'title',
			'group_id' => 'group_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
    {
		$this->setSource("gsc_templates_pdf_roles");

		$this->belongsTo(
			"group_id",
			"Models\V1\Pdf\Groups",
			"id",
			[
				"alias" => "group",
			]
		);
	}

}
