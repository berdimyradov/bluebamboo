<?php

namespace Models\V1\Pdf;

class Groups extends \Phalcon\Mvc\Model
{

	public $id;

	public $title;

	const GROUPS = array(
		"INVOICES" => 1,
	);

	public static $attrs = array(
		'id' => 'int',
		'title' => 'string',
	);

	public static $relations = array(
		"roles" => [
			"type" => "hasMany",
			"alias" => "roles",
			"relationModel" => "Models\V1\Pdf\Roles",
			"relationIdAlias" => "group_id"
		],
	);

	public function columnMap()
	{
		return array(
			'id' => 'id',
			'title' => 'title',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
    {
		$this->setSource("gsc_templates_pdf_groups");

		$this->hasMany(
            "id",
            "Models\V1\Pdf\Roles",
			"group_id",
			[
				"alias" => "roles",
			]
		);
	}

}
