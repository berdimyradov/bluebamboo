<?php

namespace Models\V1\Pdf;

class Previewcontent extends \Phalcon\Mvc\Model
{

	public $id;

	public $title;

	public $content_json;

	public $group_id;

	public $role_id;

	public static $attrs = array(
		'id' => 'int',
		'title' => 'string',
		'content_json' => 'json',
		'group_id' => 'int',
		'role_id' => 'int',
	);

	public static $relations = array();

	public function columnMap()
	{
		return array(
			'id' => 'id',
			'title' => 'title',
			'content_json' => 'content_json',
			'group_id' => 'group_id',
			'role_id' => 'role_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
    {
		$this->setSource("gsc_templates_pdf_previewcontent");
	}

}
