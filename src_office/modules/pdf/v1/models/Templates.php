<?php

/**
 * Model for app "pdf", class "Templates"
 */

namespace Models\V1\Pdf;

class Templates extends \Phalcon\Mvc\Model
{

	public $id;
	public $title;
	public $template;
	public $active;
	public $editable;
	public $standard;
	public $group_id;
	public $role_id;
	public $client_id;

	const STANDARDS = array(
		'TARIF590' => 'Tarif 590',
	);

	public static $attrs = array(
		'id' => 'int',
		'title' => 'string',
		'template' => 'json',
		'active' => 'bool',
		'editable' => 'bool',
		'standard' => 'string',
		'group_id' => 'int',
		'role_id' => 'int',
		'client_id' => 'int',
	);

	public static $relations = array(
		"group" => [
			"type" => "belongsTo",
			"alias" => "group",
			"relationModel" => "Models\V1\Pdf\Groups",
			"relationIdAlias" => "group_id"
		],
		"role" => [
			"type" => "belongsTo",
			"alias" => "role",
			"relationModel" => "Models\V1\Pdf\Roles",
			"relationIdAlias" => "role_id"
		],
	);

	public function columnMap()
	{
		return array(
			'id' => 'id',
			'title' => 'title',
			'template' => 'template',
			'active' => 'active',
			'editable' => 'editable',
			'standard' => 'standard',
			'group_id' => 'group_id',
			'role_id' => 'role_id',
			'client_id' => 'client_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
    {
		$this->setSource("gsc_templates_pdf");

		$this->belongsTo(
			"group_id",
			"Models\V1\Pdf\Groups",
			"id",
			[
				"alias" => "group",
			]
		);

		$this->belongsTo(
			"role_id",
			"Models\V1\Pdf\Roles",
			"id",
			[
				"alias" => "role",
			]
		);
	}

}
