<?php

namespace Models\V1\Pdf;

class Templatebases extends \Phalcon\Mvc\Model
{

	public $id;

	public $title;

	public $thumbnail;

	public $description;

	public $template;

	public $editable;

	public $standard;

	public $group_id;

	public $role_id;

	public static $attrs = array(
		'id' => 'int',
		'title' => 'string',
		'thumbnail' => 'string',
		'description' => 'string',
		'template' => 'json',
		'editable' => 'bool',
		'standard' => 'string',
		'group_id' => 'int',
		'role_id' => 'int',
	);

	public static $relations = array(
		"group" => [
			"type" => "belongsTo",
			"alias" => "group",
			"relationModel" => "Models\V1\Pdf\Groups",
			"relationIdAlias" => "group_id"
		],
		"role" => [
			"type" => "belongsTo",
			"alias" => "role",
			"relationModel" => "Models\V1\Pdf\Roles",
			"relationIdAlias" => "role_id"
		],
	);

	public function columnMap()
	{
		return array(
			'id' => 'id',
			'title' => 'title',
			'thumbnail' => 'thumbnail',
			'description' => 'description',
			'template' => 'template',
			'editable' => 'editable',
			'standard' => 'standard',
			'group_id' => 'group_id',
			'role_id' => 'role_id',
		);
	}
	/**
	 * Initializes relationships in the model
	 */
	public function initialize()
    {
		$this->setSource("gsc_templates_pdf_bases");

		$this->belongsTo(
			"group_id",
			"Models\V1\Pdf\Groups",
			"id",
			[
				"alias" => "group",
			]
		);

		$this->belongsTo(
			"role_id",
			"Models\V1\Pdf\Roles",
			"id",
			[
				"alias" => "role",
			]
		);
	}

}
