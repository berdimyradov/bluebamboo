<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$api = new RouterGroup(array('module' => 'api_v1_testing'));
$api->setPrefix('/api/v1/testing');

$api->addGet	("/test", array('controller' => 'Testing', 'action' => 'test'));
$api->addPut	("/test", array('controller' => 'Testing', 'action' => 'testupdate'));

$router->mount($api);
