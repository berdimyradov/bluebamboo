<?php

/**
 * Controller class LocationsProductsController
 */

namespace Api\V1\Testing\Controllers;

class TestingController extends \GSCLibrary\BaseApiController {

	private $from = "Models\V1\Testing\Testmodel";

	/**
	 * returns one entry
	 */
	public function testAction($id = "") {

		$_blobStorage = new \GSCLibrary\GSCAzureBlobStorage();

		$container = "data";
		$file = $this->getClientImagePath() . "9_58f686328a796.jpg";
		$dataPath = \GSCLibrary\SystemInfo::getDataPath();
		$content = $dataPath . $file;

		$exists = $_blobStorage->createBlob($container, $file, $content);

		error_log("Container existiert: ");
		error_log($exists ? "true" : "false");

		return $this->_jsonResponse($exists ? "Existiert!" : "Existiert nicht!");
	}

	/**
	 * updates an existing entry
	 */
	public function testupdateAction($id) {
		$model = $this->from;
		return $this->_updateAction($model, $model::$attrs, $id);
	}

}
