<?php 

namespace FrontAdmin;

use Phalcon\Loader,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\View,
    Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface {

    /**
     * Register a specific autoloader for the module
     */
    public function registerAutoloaders(\Phalcon\DiInterface $dependencyInjector = null) {

        $loader = new Loader();

        $loader->registerNamespaces(
            array(
                'GSCLibrary'      			=> '../library/',

                'Models\V1\CMS'				=> '../modules/cms/v1/models/',
                'Models\V1\Sys'				=> '../modules/sys/v1/models/',

                'FrontAdmin\Controllers' 	=> '../front_office/',
                'FrontAdmin\Logic'   		=> '../front_office/',
            )
        );

        $loader->register();
    }

    /**
     * Register specific services for the module
     */
    public function registerServices(\Phalcon\DiInterface $di) {

        //Registering a dispatcher
        $di->set('dispatcher', function() {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace("FrontAdmin\Controllers");
            return $dispatcher;
        });

        //Registering the view component
        $di->set('view', function() {
            $view = new View();
            $view->setViewsDir('../front_office/');
			$view->registerEngines(array(
				".phtml" => 'Phalcon\Mvc\View\Engine\Php',
				".volt" => 'Phalcon\Mvc\View\Engine\Volt',
			));
            return $view;
        });
    }

}
