app.controller("CustomersController", 
	["$scope", "Restangular", "$window", function ($scope, Restangular, $window) {
			var ctrl = this;

			setInherited(ctrl);

			ctrl.fields = {};
			ctrl.fields.name = "";
			ctrl.fields.firstname = "";
			ctrl.fields.street = "";
			ctrl.fields.zip = "";
			ctrl.fields.city = "";
			ctrl.fields.phone = "";
			ctrl.fields.mobile = "";
			ctrl.fields.email = "";
			
			ctrl.waiting = false;

			ctrl.bookings = 14;

			ctrl.customers = [];
			
			ctrl.customers.push({"name":"Ammann","firstname":"Michael"});
			ctrl.customers.push({"name":"Ammann","firstname":"Catherine"});

			ctrl.step = 1;

			ctrl.cta = function(label) {
				if (ctrl.waiting) return "bitte warten...";
				return label;
			}

			ctrl.addCustomer = function () {
				$window.location.href = "customers/add_customer";
			};

			ctrl.isReady = function () {
				if (this.waiting) return false;
				if (this.fields.name == "") return false;
				if (this.fields.firstname == "") return false;
				return true;
			};

			ctrl.nextStep = function () {
				ctrl.step++;
			};

			ctrl.login = function () {
				ctrl.resetFeedback();
				ctrl.waiting = true;
				var ok_cb = function () {
					ctrl.waiting = false;
					$window.location.href = "index";
				};
				var nok_cb = function (res) {
					ctrl.waiting = false;
					ctrl.setFeedback(res.data);
				};
				Restangular.all('api/v1/auth/login').customPOST(ctrl.fields).then(ok_cb, nok_cb);
			};			
		}
	]
);
