#out: $1.js, bare: true

app.controller "TemplatesController", ["$scope", "Restangular", "$window", ($scope, Restangular, $window) ->

	ctrl = this
	setInherited ctrl
	gsc_dialog_confirm ctrl

	ctrl.templates = {}
	ctrl.model = {}
	ctrl.model.fields = {}

	ctrl.fontstylenames = []
	ctrl.borderstylenames = []
	ctrl.elementTypes = [
			id: "string"
			label: "Text"
		,
			id: "table"
			label: "Tabelle"
		,
			id: "dynamictable"
			label: "Dynamische Tabelle"
		,
			id: "image"
			label: "Bild"
	]

	ctrl.page = 0
	ctrl.pagetitle = ""
	ctrl.waiting = false

	ctrl.loadPage = (page) ->
		ctrl.page = page
		switch ctrl.page
			when 1
				ctrl.reset()
				ctrl.ctaLoadTemplates()
				ctrl.pagetitle = "Vorlagen verwalten"
			when 2
				ctrl.pagetitle = if ctrl.model._newObj then "Neue Vorlage erstellen" else "Vorlage bearbeiten"
				ctrl.updateFontstyleNames()
				ctrl.updateBorderstyleNames()
			when 3
				ctrl.pagetitle = if ctrl.model._newObj then "Schriftstil erstellen" else "Schriftstil bearbeiten"
			when 4
				ctrl.pagetitle = if ctrl.model._newObj then "Rahmenstil erstellen" else "Rahmenstil bearbeiten"
			when 5
				ctrl.pagetitle = "Art des Elementes wählen"
			when 6
				ctrl.pagetitle = if ctrl.model._newObj then "Textblock hinzufügen" else "Textblock bearbeiten"
			when 7
				if ctrl.model.fields.type == "table"
					ctrl.pagetitle = if ctrl.model._newObj then "Tabelle hinzufügen" else "Tabelle bearbeiten"
				else
					ctrl.pagetitle = if ctrl.model._newObj then "Dynamische Tabelle hinzufügen" else "Dynamische Tabelle bearbeiten"
			when 8
				ctrl.pagetitle = if ctrl.model._newObj then "Bild hinzufügen" else "Bild bearbeiten"
			when 9
				ctrl.pagetitle = "Tabellenheader /-footer bearbeiten"
			when 10
				ctrl.pagetitle = "Tabelleninhalt bearbeiten"
		return

	ctrl.cta = ->
		switch ctrl.page
			when 2 then ctrl.ctaSaveTemplate()
			when 3,4,6,7,8,9,10 then ctrl.saveSubobject()
			when 5 then ctrl.addElement()
		return

	ctrl.cancel = ->
		switch ctrl.page
			when 1,2 then ctrl.pageBack()
			when 3,4,5,6,7,8,9,10 then ctrl.cancelSubobject()

	ctrl.ctaLoadTemplates = ->
		ctrl.templates = {}
		ctrl.waiting = true
		ok_cb = (res) ->
			ctrl.waiting = false
			ctrl.templates = res
		nok_cb = (res) ->
			ctrl.waiting = false
		Restangular.all("api/v1/invo/templates?_fields=id,title").customGET().then ok_cb, nok_cb

	ctrl.ctaSaveTemplate = ->
		if ctrl.model._newObj then ctrl.ctaSaveNewTemplate() else ctrl.ctaUpdateTemplate()
		return

	ctrl.ctaSaveNewTemplate = ->
		ctrl.resetFeedback()
		ctrl.waiting = true
		ok_cb = (res) ->
			ctrl.waiting = false
			ctrl.loadPage 1
		nok_cb = (res) ->
			ctrl.waiting = false
			ctrl.setFeedback res.data
			ctrl.model.fields.template = JSON.parse ctrl.model.fields.template
		ctrl.model.fields.template = JSON.stringify ctrl.model.fields.template
		Restangular.all("api/v1/invo/templates").customPOST(ctrl.model.fields).then ok_cb, nok_cb
		return

	ctrl.ctaUpdateTemplate = ->
		ctrl.resetFeedback()
		ctrl.waiting = true
		ok_cb = (res) ->
			ctrl.waiting = false
			ctrl.loadPage 1
		nok_cb = (res) ->
			ctrl.waiting = false
			ctrl.setFeedback res.data
			ctrl.model.fields.template = JSON.parse ctrl.model.fields.template
		id = ctrl.model.fields.id
		ctrl.model.fields.template = JSON.stringify ctrl.model.fields.template
		Restangular.all("api/v1/invo/templates/#{id}").customPUT(ctrl.model.fields).then ok_cb, nok_cb
		return

	ctrl.addTemplate = (id) ->
		if id?
			ctrl.addTemplateById id
		else
			ctrl.model = {}
			ctrl.model.fields = {}
			ctrl.model._newObj = true
			ctrl.model.fields.id = 0
			ctrl.model.fields.title = ""
			ctrl.model.fields.template = {}
			ctrl.model.fields.template.fontstyles = []
			ctrl.model.fields.template.borderstyles = []
			ctrl.model.fields.template.elements = []
			ctrl.model.fields.template.type = "layout"
			ctrl.loadPage 2
		return

	ctrl.addTemplateById = (id) ->
		ctrl.resetFeedback()
		ctrl.waiting = true
		ok_cb = (res) ->
			ctrl.waiting = false
			ctrl.model = {}
			ctrl.model.fields = res
			ctrl.model.fields.id = 0
			ctrl.model.fields.title = ""
			ctrl.model._newObj = true
			ctrl.loadPage 2
		nok_cb = (res) ->
			ctrl.waiting = false
			ctrl.setFeedback res
		Restangular.all("api/v1/invo/templates/#{id}").customGET().then ok_cb, nok_cb
		return

	ctrl.editTemplate = (id) ->
		ctrl.resetFeedback()
		ctrl.waiting = true
		ok_cb = (res) ->
			ctrl.waiting = false
			ctrl.model = {}
			ctrl.model.fields = res
			ctrl.model._newObj = false
			ctrl.loadPage 2, 1
		nok_cb = (res) ->
			ctrl.waiting = false
			ctrl.setFeedback res
		Restangular.all("api/v1/invo/templates/#{id}").customGET().then ok_cb, nok_cb
		return

	ctrl.ctaDeleteTemplate = (id) ->
		ctrl.resetFeedback()
		ctrl.waiting = true
		ok_cb = (res) ->
			ctrl.waiting = false
			ctrl.ctaLoadTemplates()
		nok_cb = (res) ->
			ctrl.waiting = false
			ctrl.setFeedback res.data
		remove = ->
			Restangular.all("api/v1/invo/templates/#{id}").customDELETE().then ok_cb, nok_cb
		ctrl.confirmDialog("Vorlage löschen?", "Die Vorlage wirklich löschen?", "Abbrechen", "Löschen", remove)
		return

	ctrl.previewTemplate = (template_json) ->
		return

	ctrl.ctaLabel = (label) ->
		if ctrl.waiting then return "bitte warten..."
		label

	ctrl.isReady = ->
		switch ctrl.page
			when 1 then return true
			when 2 then return ctrl.isReadyTemplate()
			when 3 then return ctrl.isReadyFontstyle()
			when 4 then return ctrl.isReadyBorderstyle()
			when 5 then return ctrl.isReadyElement()
			when 6 then return ctrl.isReadyString()
			when 7 then return ctrl.isReadyTable()
			when 8 then return ctrl.isReadyImage()
			when 9 then return ctrl.isReadyTableHeader()
			when 10 then return ctrl.isReadyTableContent()
		true

	ctrl.isReadyTemplate = ->
		if ctrl.waiting then return false
		if ctrl.isEmpty ctrl.model.fields, "title" then return false
		true

	ctrl.isReadyFontstyle = ->
		if ctrl.waiting then return false
		if ctrl.isEmpty ctrl.model.fields, "name" then return false
		true

	ctrl.isReadyBorderstyle = ->
		if ctrl.waiting then return false
		if ctrl.isEmpty ctrl.model.fields, "name" then return false
		true

	ctrl.isReadyElement = ->
		if ctrl.waiting then return false
		if ctrl.isEmpty ctrl.model.fields, "type" then return false
		true

	ctrl.isReadyString = ->
		if ctrl.waiting then return false
		if ctrl.isEmpty ctrl.model.fields, "name" then return false
		if ctrl.isEmpty ctrl.model.fields, "content" then return false
		if ctrl.isEmpty ctrl.model.fields.location, "x" then return false
		if ctrl.isEmpty ctrl.model.fields.location, "y" then return false
		true

	ctrl.isReadyTable = ->
		if ctrl.waiting then return false
		if ctrl.isEmpty ctrl.model.fields, "name" then return false
		if ctrl.isEmpty ctrl.model.fields.location, "x" then return false
		if ctrl.isEmpty ctrl.model.fields.location, "y" then return false
		if ctrl.isEmpty ctrl.model.fields, "content" then return false
		if ctrl.isEmpty ctrl.model.fields.content, "rows" then return false
		true

	ctrl.isReadyTableHeader = ->
		if ctrl.waiting then return false
		if ctrl.isEmpty ctrl.model.fields, "cells" then return false
		true

	ctrl.isReadyTableContent = ->
		if ctrl.waiting then return false
		true

	ctrl.isReadyImage = ->
		true

	ctrl.isEmpty = (fields, field) ->
		ctrl.isVarEmpty fields[field]

	ctrl.isVarEmpty = (variable) ->
		if variable == undefined then return true
		if variable == null then return true
		if variable.trim() == "" then return true
		if variable == "" then return true
		false

	ctrl.reset = ->
		ctrl.model = {}
		ctrl.model.fields = {}
		return

	ctrl.pageBack = ->
		switch ctrl.page
			when 1
				ctrl.reset
				$window.location.href = "../invo"
			when 2 then ctrl.loadPage 1
			when 3, 4, 5 then ctrl.loadPage 2
			when 6, 7, 8
				if ctrl.model._newObj && ctrl.isEmpty ctrl.model.fields, "name" then ctrl.loadPage 5 else ctrl.loadPage 2
			else ctrl.loadPage 1
		return

	ctrl.fontStyle = (fontstyle) ->
		style = {}
		fontsize = if fontstyle.size? then fontstyle.size*100 else 0
		if fontstyle.face? then style['font-family'] = "#{fontstyle.face}"
		if fontstyle.size? then style['font-size'] = "#{fontsize}pt;"
		if fontstyle.color? then style['color'] = "#{fontstyle.color}"
		if fontstyle.bold?
			if fontstyle.bold then style['font-weight'] = "bold"
		if fontstyle.italic?
			if fontstyle.italic then style['font-style'] = "italic"
		if fontstyle.underlined?
			if fontstyle.underlined then style['text-decoration'] = "underline"
		style

	ctrl.borderStyle = (borderstyle) ->
		style = {}
		if borderstyle.width?
			width = borderstyle.width * 8
			style['border-width'] = "#{width}px"
			style['border-style'] = "solid"
		if borderstyle.color? then style['border-color'] = "#{borderstyle.color}"
		if borderstyle.top?
			if borderstyle.top.width
				topwidth = borderstyle.top.width * 8
				style['border-top-width'] = "#{topwidth}px"
				style['border-top-style'] = "solid"
		if borderstyle.left
			if borderstyle.left.width
				leftwidth = borderstyle.left.width * 8
				style['border-left-width'] = "#{leftwidth}px"
				style['border-left-style'] = "solid"
		if borderstyle.right?
			if borderstyle.right.width
				rightwidth = borderstyle.right.width * 8
				style['border-right-width'] = "#{rightwidth}px"
				style['border-right-style'] = "solid"
		if borderstyle.bottom?
			if borderstyle.bottom.width
				bottomwidth = borderstyle.bottom.width * 8
				style['border-bottom-width'] = "#{bottomwidth}px"
				style['border-bottom-style'] = "solid"
		style

	ctrl.addFontstyleFromElement = (index) ->
		ctrl.editSubobject('template.fontstyles', 3, index)
		ctrl.model._newObj = true
		ctrl.model._index = -1
		ctrl.model.fields.name = ""
		ctrl.loadPage 3
		return

	ctrl.addBorderstyleFromElement = (index) ->
		ctrl.editSubobject('template.borderstyles', 4, index)
		ctrl.model._newObj = true
		ctrl.model._index = -1
		ctrl.model.fields.name = ""
		ctrl.loadPage 4
		return

	ctrl.addElement = ->
		switch ctrl.model.fields.type
			when "string" then return ctrl.loadPage 6
			when "table"
				ctrl.model.fields.colcount = 1
				ctrl.loadPage 7
			when "dynamictable"
				ctrl.model.fields.colcount = 1
				ctrl.loadPage 7
			when "image" then return ctrl.loadPage 8
		return

	ctrl.editElement = (index) ->
		switch ctrl.model.fields.template.elements[index].type
			when "string" then ctrl.editSubobject 'template.elements', 6, index
			when "table" then ctrl.editSubobject 'template.elements', 7, index
			when "dynamictable" then ctrl.editSubobject 'template.elements', 7, index
			when "image" then ctrl.editSubobject 'template.elements', 8, index
		return

	ctrl.addElementFromElement = (index) ->
		ctrl.editElement index
		ctrl.model._newObj = true
		ctrl.model._index = -1
		ctrl.model.fields.name = ""
		switch ctrl.model.fields.type
			when "string" then ctrl.loadPage 6
			when "table" then ctrl.loadPage 7
			when "dynamictable" then ctrl.loadPage 9
			when "image" then ctrl.loadPage 8
		return

	ctrl.addSubobject = (attr, page) ->
		currentModel = ctrl.cloneObject ctrl.model
		split = attr.split "."
		attribute = ctrl.model.fields
		for attrname in split
			attribute = attribute[attrname]
		isArray = if attribute.constructor is Array then true else false
		ctrl.model = {}
		ctrl.model.fields = {}
		ctrl.model._attr = attr
		ctrl.model._attrIsArray = isArray
		ctrl.model._index = -1
		ctrl.model._previousPage = ctrl.page
		ctrl.model._newObj = true
		ctrl.model._previous = currentModel
		ctrl.loadPage page
		return

	ctrl.editSubobject = (attr, page, index) ->
		currentModel = ctrl.cloneObject ctrl.model
		split = attr.split "."
		attribute = ctrl.model.fields
		for attrname in split
			attribute = attribute[attrname]
		isArray = if attribute.constructor is Array then true else false
		if isArray
			if index >= 0
				ctrl.model.fields = ctrl.cloneObject attribute[index]
			else
				ctrl.model.fields = {}
		else
			ctrl.model.fields = ctrl.cloneObject attribute
			index = -2
		ctrl.model._attr = attr
		ctrl.model._attrIsArray = isArray
		ctrl.model._index = index
		ctrl.model._previous = currentModel
		ctrl.model._previousPage = ctrl.page
		ctrl.model._newObj = false
		ctrl.loadPage page
		return

	ctrl.deleteSubobject = (attr, index) ->
		split = attr.split "."
		length = split.length - 1
		last = split[length]
		attribute = ctrl.model.fields
		i = 0
		while i < length
			attribute = attribute[split[i]]
			i++
		isArray = if attribute[last].constructor is Array then true else false
		if isArray
			if index >= 0
				attribute[last].splice index,1
		else
			delete attribute[last]
		ctrl

	ctrl.cancelSubobject = ->
		page = ctrl.model._previousPage
		ctrl.model = ctrl.model._previous
		ctrl.loadPage page

	ctrl.saveSubobject = ->
		attr = ctrl.model._attr
		isArray = ctrl.model._attrIsArray
		page = ctrl.model._previousPage
		previousModel = ctrl.model._previous
		index = ctrl.model._index

		split = attr.split "."
		length = split.length - 1
		last = split[length]
		attribute = previousModel.fields
		i = 0
		while i < length
			attribute = attribute[split[i]]
			i++

		if isArray
			if index < 0 then attribute[last].push ctrl.model.fields else attribute[last][index] = ctrl.model.fields
		else
			attribute[last] = ctrl.model.fields

		ctrl.model = previousModel
		ctrl.loadPage page

	ctrl.cloneObject = (obj) ->
		JSON.parse(JSON.stringify(obj))

	ctrl.updateFontstyleNames = (fontstyles) ->
		ctrl.fontstylenames = []
		ctrl.fontstylenames.push {id: styles.name} for styles in ctrl.model.fields.template.fontstyles
		return

	ctrl.updateBorderstyleNames = ->
		ctrl.borderstylenames = []
		ctrl.borderstylenames.push {id: styles.name} for styles in ctrl.model.fields.template.borderstyles
		return

	ctrl.editTableContent = (content) ->
		switch content
			when 1
				cellcount = parseInt ctrl.model.fields.colcount
				if ctrl.model.fields.header?
					ctrl.editSubobject "header", 9
					if ctrl.model.fields.cells?
						if ctrl.model.fields.cells.length < cellcount
							i=ctrl.model.fields.cells.length
							while i < cellcount
								ctrl.model.fields.cells.push {value: ""}
								i++
					else
						ctrl.model.fields.cells = []
						i=0
						while i < cellcount
							ctrl.model.fields.cells.push {value: ""}
							i++
				else
					ctrl.model.fields.header = {}
					ctrl.addSubobject "header", 9
					ctrl.model.fields.cells = []
					ctrl.model.fields.show = false
					i = 0
					while i < cellcount
						ctrl.model.fields.cells.push {value: ""}
						i++

			when 2
				cellcount = parseInt ctrl.model.fields.colcount
				if ctrl.model.fields.footer?
					ctrl.editSubobject "footer", 9
					if ctrl.model.fields.cells?
						if ctrl.model.fields.cells.length < cellcount
							i=ctrl.model.fields.cells.length
							while i < cellcount
								ctrl.model.fields.cells.push {value: ""}
								i++
					else
						ctrl.model.fields.cells = []
						i=0
						while i < cellcount
							ctrl.model.fields.cells.push {value: ""}
							i++
				else
					ctrl.model.fields.footer = {}
					ctrl.addSubobject "footer", 9
					ctrl.model.fields.cells = []
					ctrl.model.fields.show = false
					i = 0
					while i < cellcount
						ctrl.model.fields.cells.push {value: ""}
						i++
			when 3
				cellcount = parseInt ctrl.model.fields.colcount

				if ctrl.model.fields.content?
					ctrl.editSubobject "content", 10
					if !ctrl.model.fields.rows?
						ctrl.model.fields.rows = []
						ctrl.addTablerow cellcount
					else if !ctrl.model.fields.rows[0]?
						ctrl.addTablerow cellcount

				else
					ctrl.model.fields.content = {}
					ctrl.addSubobject "content", 10
					ctrl.model.fields = {}
					ctrl.model.fields.rows = []
					ctrl.addTablerow cellcount
		return


	ctrl.addTablerow = (cellcount, index) ->
		if !cellcount? or cellcount < 0
			cellcount = if ctrl.model.fields.rows[0]? then ctrl.model.fields.rows[0].cells.length else 0
		row = {}
		row.cells = []
		i = 0
		while i < cellcount
			row.cells.push {value: ""}
			i++

		if index? && index >= 0
			ctrl.model.fields.rows.splice index, 0, row
		else
			ctrl.model.fields.rows.push row
		return

	ctrl.addsubTablecell = (sum) ->
		ctrl.model.fields.colcount = parseInt(ctrl.model.fields.colcount) + sum
		return

	ctrl.loadPage(1)
	ctrl
]
