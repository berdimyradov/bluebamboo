###
	Invoices Controller Class
###
class InvoController extends BambooController

	fromInvo: "api/v1/invo/invoices"
	fromReq: "api/v1/invo/invoicerequests"
	searchOrder: ["status", "number", "name", "value", "id"]

	constructor: (ctrl) ->
		super ctrl
		@addModule("invoices")
		@mainModule = @modules.invoices
		@loadIndex = ->
			@mainModule.loadMenu()
		@start()
		return


	start: ->
		@loadIndex()
		return

	cta: (data = "") ->
		if @isReady()
			switch @page
				when 340
					@mainModule.createInvoices_2()
				when 341
					@mainModule.makeInvoices()
				when 360
					@mainModule.ctaStornoInvoice().then((res) ->
						return
					, (res) ->
						return
					)
				else console.log "cta page #{@page} not handled"
		else
			# if @isModelEmpty "birth_day" then @model.fields.birth_day = ""
			# TODO - Fehlerfenster öffnen
		super()
		return

	back: ->
		@pageBack()
		return

	cancel: ->
# TODO
		switch @page
			when 300 then @gotoDesktop()
			when 310,320,340,371,372 then @pageBack()
			when 341,342 then @gotoIndex()
			else
				@gotoIndex()
				console.log "cancel page #{@page} not handled"
		return

app.controller "InvoicesController", ["$scope", "$window", "$document", "localStorageService", "Restangular", ($scope, $window, $document, localStorageService, Restangular) ->

	ng = @
	ng.$scope = $scope
	ng.$window = $window
	ng.$document = $document
	ng.localStorageService = localStorageService
	ng.Restangular = Restangular

	ng.ctrl = new InvoController @

	return
]
