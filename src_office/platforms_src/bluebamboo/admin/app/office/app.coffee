﻿### ========================================================================================================================================
# AngularJS: d
#  - Downloads: https://code.angularjs.org/1.3.1/
#  - Dependency Injection: https://github.com/angular/angular.js/wiki/Understanding-Dependency-Injection
###

root = exports ? this

app_context = '/api'
root.app = angular.module('project', [
  'ngAnimate'
  'xeditable'
  'ngMessages'
  'restangular'
  'ngCookies'
  'dndLists'
  'LocalStorageModule'
  'ngSanitize'
  'froala',
  'ngFileUpload'
  'ui.select'
  'ui.calendar',
  'ui.bootstrap'
]).value('froalaConfig',
  toolbarButtons: [
    'bold'
    'italic'
    'underline'
    'strikeThrough'
    'subscript'
    'superscript'
    '|'
    'emoticons'
    '|'
    'paragraphFormat'
    'align'
    'formatOL'
    'formatUL'
    'outdent'
    'indent'
    'quote'
    'insertLink'
    'insertTable'
    'undo'
    'redo'
    'clearFormatting'
    'html'
  ]
  tableResizerOffset: 10
  tableResizingLimit: 50
  heightMin: 250
  heightMax: 250
  language: 'de'
  charCounterCount: false
  toolbarInline: false)
master_controller = null
app.config((RestangularProvider) ->
  RestangularProvider.setDefaultHeaders 'Content-Type': 'application/json'
  #		var API_ENDPOINT = app_context;
  #		RestangularProvider.setBaseUrl(API_ENDPOINT);
  RestangularProvider.setRequestInterceptor (elem, operation, what) ->
    if operation == 'put'
      elem._id = undefined
      return elem
    elem
  RestangularProvider.setErrorInterceptor (response, deferred, responseHandler) ->
    if response.status == 403
      window.top.location.href = '/de/admin/auto_logout'
      return false
      ###
      	refreshAccesstoken().then(function() {
      		// Repeat the request and then call the handlers the usual way.
      		$http(response.config).then(responseHandler, deferred.reject);
      		// Be aware that no request interceptors are called this way.
      	});

      	return false; // error handled
      ###
    true
    # error not handled
  # RestangularProvider.setDefaultHttpFields withCredentials: true
  #		localStorageServiceProvider.setPrefix('gsc_app');
  return
).run ($rootScope, Restangular, editableOptions, editableThemes) ->
  editableThemes.bs3.inputClass = 'input-sm'
  editableThemes.bs3.buttonsClass = 'btn-sm btn-xyz'
  editableOptions.theme = 'bs3'

  String::endsWith = (suffix) ->
    @indexOf(suffix, @length - (suffix.length)) != -1

  ###
  Restangular.configuration.setIdToElem = function(elem, id) {
  	console.log("[1]elem.route: " + elem.route + "/" + id);
  	console.log(JSON.stringify(elem));
  	var x = _.initial(elem.route).join('');
  	if (route.endsWith("/api/v1/continent")) {
  		elem["code"] = id;
  	}
  	else {
  		elem["ID"] = id;
  	}
  	return this;
  };
  ###

  Restangular.configuration.useCannonicalId = true

  ###
  Restangular.configuration.getIdFromElem = function(elem) {
  //			console.log("[2]elem.route: " + elem.route);
  	var route = _.initial(elem.route).join('');
  //			if (route == "msc/api/v1/continent") {
  	if (route.endsWith("api/v1/continent")) {
  		return elem["code"];
  	}
  	return elem["ID"];
  };
  ###

  #		routesFix(Restangular);
  Restangular.setErrorInterceptor (response, deferred, responseHandler) ->
    if response.status == 403
      #				User.setLoggedOut();
      #				window.location = app_context + "/admin";
    else
    true
    # error not handled
  return
app.directive 'autoFocus', ($timeout) ->
  {
    restrict: 'A'
    require: '?ngModel'
    link: (_scope, _element, _attrs, _ctrl) ->
      if !_ctrl
        return
      _scope.$watch _attrs['autoFocus'], (val) ->
        if val
          $timeout (->
            _element[0].focus()
            _element[0].select()
            return
          ), 0
        return
      return

  }
app.controller 'MasterController', [
  '$scope'
  'Restangular'
  '$window'
  '$cookies'
  ($scope, Restangular, $window, $cookies) ->
    ctrl = undefined
    ctrl = this
    master_controller = this
    ctrl.defaultStyle = true

    ctrl.logout = ->
      nok_cb = undefined
      ok_cb = undefined

      ok_cb = (res) ->
        $window.location.href = res.url
        return

      nok_cb = (res) ->
        true

      Restangular.all('api/v1/auth/logout').customGET().then ok_cb, nok_cb

    return
]

###
http://stackoverflow.com/questions/14115701/angularjs-create-a-directive-that-uses-ng-model
###

###
app.directive('ffError', function() {
  return {
    restrict: 'E',
    scope: {
      ctrl: '='
    },
    templateUrl: "/de/admin/ff/error.tpl"
  };
});

app.directive('ffCta', function() {
  return {
    restrict: 'E',
    scope: {
      ctrl: '=',
      label: '@'
    },
    templateUrl: "/de/admin/ff/cta.tpl"
  };
});

app.directive('ffString', function() {
  return {
    restrict: 'E',
    scope: {
      ctrl: '=',
      required: '@',
      label: '@',
      name: '@',
      autofocus: '@'
    },
    templateUrl: "/de/admin/ff/string.tpl"
  };
});
###

###
app.directive('bindHeightToWidth', function () {
//  var directive: ng.IDirective;

	return {
		restrict: 'A',
		link: function (scope, instanceElement, instanceAttributes, controller, transclude) {
			var heightFactor = 1;

			if (instanceAttributes['bindHeightToWidth']) {
				heightFactor = instanceAttributes['bindHeightToWidth'];
			}

			var updateHeight = function () {
//				console.log("update height, heightFactor: " + heightFactor);
//				console.log("width: " + instanceElement.width());
				if (heightFactor == 0.5) {
					instanceElement.height((instanceElement.width() - 10) * heightFactor);
				}
				else {
					instanceElement.height(instanceElement.width() * heightFactor);
				}
			}

			scope.$watch(instanceAttributes['bindHeightToWidth'], function (value) {
				heightFactor = value;
			});

			$(window).resize(updateHeight);
			updateHeight();

			scope.$on('$destroy', function () {
				$(window).unbind('resize', updateHeight);
			});
		}
	};

})
###

