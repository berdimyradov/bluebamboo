###
#	Locations Controller Class
###
class LocController extends BambooController

	constructor: (ctrl) ->
		super ctrl
		@addModule "locations"
		@addModule "rooms"
		@loadIndex = =>
			@modules.locations.loadIndex()
		@start()
		return

	start: ->
		if !@loadData()
			@loadIndex()
		else
			if @loadData().pageLoad
				switch @loadData().action
					when 1 then @modules.locations.showLocation @loadData().elementId
					when 2 then @modules.locations.addLocation()
					when 3 then @modules.locations.editLocation @loadData().elementId
					when 4 then @modules.locations.deleteLocation @loadData().elementId
					else
						@loadIndex()
			else
				@loadIndex()
		return

	cta: (data="") ->
		super()
		if @isReady()
			switch @page
				when 740 then @modules.locations.saveLocation()
				when 750 then @modules.locations.saveLocation()
				else console.log "cta page #{@page} not handled"
		else
			# TODO - Fehlerfenster öffnen
		return

	cancel: ->
		switch @page
			when 700, 720 then @gotoDesktop()
			when 710,730,740 then @gotoIndex()
			when 750 then @pageBack()
			else
				@gotoIndex()
				console.log "cancel page #{@page} not handled"
		return

	back: ->
		@pageBack()
		return

###
#	Controller
###
app.controller "LocationsController", ["$scope", "$window", "$document", "localStorageService", "Restangular", ($scope, $window, $document, localStorageService, Restangular) ->

	ng = @
	ng.$scope = $scope
	ng.$window = $window
	ng.$document = $document
	ng.localStorageService = localStorageService
	ng.Restangular = Restangular

	ng.ctrl = new LocController @

	return
]
