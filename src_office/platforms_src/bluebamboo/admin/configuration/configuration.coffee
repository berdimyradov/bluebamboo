###
	Configuration Controller Class
###
class ConfigController extends BambooController

	constructor: (ctrl) ->
		super ctrl
		@addModule "configuration"
		@start()
		return

	start: ->
		@loadPage 1100
		return

	cta: ->
		super()
		###
		if @isReady()
			switch @page
				when 1100 then @loadPage 1100
				else console.log "cta page #{@page} not handled"
		else
			# if @isModelEmpty "birth_day" then @model.fields.birth_day = ""
			# TODO - Fehlerfenster öffnen
		###
		return

	cancel: ->
		switch @page
			when 1100 then @gotoDesktop()
			when 1110 then @gotoIndex()
			when 1150 then @pageBack()
			else
				@gotoIndex()
				console.log "cancel page #{@page} not handled"
		return

	back: ->
		@pageBack()
		return

app.controller "ConfigurationController", ["$scope", "$window", "$document", "localStorageService", "Restangular", ($scope, $window, $document, localStorageService, Restangular) ->

	ng = @
	ng.$scope = $scope
	ng.$window = $window
	ng.$document = $document
	ng.localStorageService = localStorageService
	ng.Restangular = Restangular

	ng.ctrl = new ConfigController @

	return
]
