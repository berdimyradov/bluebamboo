###
	Organization Controller Class
###
class OrgaController extends BambooController

	from: "api/v1/org/organization"
	_required = ["name"]
	_autofocus = "name"

	constructor: (ctrl) ->
		super ctrl
		@start()
		return

	start: ->
		@editOrganization()
		return

	cta: ->
		super()
		if @isReady()
			switch @page
				when 1 then @saveOrganization()
				else console.log "cta page #{@page} not handled"
		else
			# TODO - Fehlerfenster öffnen
		return

	cancel: ->
		switch @page
			when 1 then @pageBack()
			else console.log "cancel page #{@page} not handled"
		return

	pageBack: ->
		switch @page
			when 1 then @ng.$window.location.href = "configuration"
		return

	back: ->
		@pageBack()
		return

	editOrganization: ->
		ok_add = (res) =>
			if res.length is 0
				@model._newObj = true
				@resetModel()
			else
				@model.fields = @dateConverter res[0]
				@model._newObj = false
		@_editAction "", @from, 1, ok_add
		return

	saveOrganization: ->
		file = "logo"
		filesrc = "/api/v1/cms/fileupload/image"

		save = =>
			cb = (res) =>
				@stopWaiting()
				@loadPage(-1)
				return
			@_saveAction @from, -1, cb
			return

		if @hasFile file
			@_imageUploadAction(file, filesrc,
				(resp) =>
					@model.fields[file] = resp.filename
					save()
					true
				(resp) =>
					# TODO: Error Handling
					return
			)
		else save()

		return

app.controller "OrganizationController", ["$scope", "$window", "$document", "Upload", "localStorageService", "Restangular", ($scope, $window, $document, Upload, localStorageService, Restangular) ->

	ng = @
	ng.$scope = $scope
	ng.$window = $window
	ng.$document = $document
	ng.Upload = Upload
	ng.localStorageService = localStorageService
	ng.Restangular = Restangular

	ng.ctrl = new OrgaController(@)

	return
]
