#out: $1.js, bare: true

###
	Customers Controller Class
###
class InsuController extends BambooController

	from: "api/v1/insu/insurances"
	_required = ["name"]
	_autofocus = "name"

	constructor: (ctrl) ->
		super ctrl
		@start()
		return

	start: ->
		if !@loadData()
			@loadPage 1
		else
			if @loadData().pageLoad
				switch @loadData().action
					when 2 then @addInsurance()
					when 3 then @editInsurance @loadData().elementId
					when 4 then @deleteInsurance @loadData().elementId
					else
						@loadPage 1
			else
				@loadPage 1
		return

	isReady: ->
		if @waiting() then return false
		switch @page
			when 1 then return true
			when 2
				if @forms.form.$invalid then return false
				for prop in @_required
					if @_isModelEmpty prop then return false
				return true
			else console.log "isReady page #{@page} not handled"
		true

	cta: ->
		super()
		if @isReady()
			switch @page
				when 2 then @saveInsurance()
				else console.log "cta page #{@page} not handled"
		else
			# TODO - Fehlerfenster öffnen
		return

	cancel: ->
		switch @page
			when 1,2 then @pageBack()
			else console.log "cancel page #{@page} not handled"
		return

	loadPage: (page) ->
		super page
		switch page
			when 1
				@loadIndex()
				@pagetitle = "Krankenkassen verwalten"
			when 2
				@pagetitle = if @model._newObj then "Krankenkasse hinzufügen" else "Krankenkasse bearbeiten"
			else console.log "loadPage page #{@page} not handled"
		return

	pageBack: ->
		switch @page
			when 1 then @ng.$window.location.href = "desktop"
			when 2 then @loadPage 1
		return

	back: ->
		@pageBack()
		return

	addInsurance: ->
		@_addAction 2
		return

	editInsurance: (id) ->
		@_editAction id, @from, 2
		return

	deleteInsurance: (id) ->
		@_deleteAction id, @from, 1, "Krankenkasse löschen", "Die Krankenkasse wirklich löschen?"
		return

	saveInsurance: ->
		@_saveAction @from, 1
		return

	loadIndex: ->
		@_indexAction @from
		return

app.controller "InsurancesController", ["$scope", "$window", "$document", "localStorageService", "Restangular", ($scope, $window, $document, localStorageService, Restangular) ->

	ng = @
	ng.$scope = $scope
	ng.$window = $window
	ng.$document = $document
	ng.localStorageService = localStorageService
	ng.Restangular = Restangular

	ng.ctrl = new InsuController @

	return
]
