#out: $1.js, bare: true

###
#	Products Controller Class
###
class ProdController extends BambooController

	from: "api/v1/prod/products"

	pageSwitch:
		[
			label: "Leistung"
			value: 10
		,
			label: "Produkt"
			value: 20
		]

	tariffpositions: []

	emptyModel:
		type: 10
		active: true
		employees: []
		rooms: []
		productparts: [
			tariffposition_id: 0
			tariff: 590
		]

	_required: ["type", "title", "price"]
	_requiredPerType:
		10: ["type", "title"]
		20: ["type", "title", "price"]

	_autofocus: "title"

	constructor: (ctrl) ->
		super ctrl
		@cblProducts = new ContentBoxList()
		@cblServices = new ContentBoxList()
		@employeesCheckbox = new ContentBoxListCheckboxExtensions()
		@roomsCheckbox = new ContentBoxListCheckboxExtensions()
		@loadTariffpositions()
		@start()
		return

	activeLabel: ->
		if @model.fields.active?
			if @model.fields.active then return "Die Leistung ist aktiv."
		else return "Die Leistung ist aktuell inaktiv."

	tariffPostionLabel: (tariffPosition) ->
		if @tariffpositions.length > 0
			for t in @tariffpositions
				if t.id == tariffPosition then return "#{t.nr} - #{t.title}"
		return ""

	isReady: ->
		if @waiting() then return false
		switch @page
			when 800, 810, 820, 830 then return true
			when 840
				if @forms.form.$invalid then return false
				for prop in @_required
					if @isModelEmpty prop then return false
				return true
			else console.log "isReady page #{@page} not handled"
		true

	cta: ->
		super()
		if @isReady()
			switch @page
				when 840 then @saveProduct()
				else console.log "cta page #{@page} not handled"
		else
			console.log "Form nicht ready?!"
			# TODO - Fehlerfenster öffnen
		return

	cancel: ->
		switch @page
			when 800,820,830,840 then @pageBack()
			when 810 then @loadPage(820)
			else console.log "cancel page #{@page} not handled"
		return

	loadPage: (page) ->
		super page
		switch page
			when 800
				@pagetitle = "Leistungen und Produkte"
			when 820
				@loadIndex()
				@pagetitle = "Leistungen und Produkte"
			when 810
				if @model.fields.type?
					if @model.fields.type is "10" or @model.fields.type is 10
						@pagetitle = "Leistung"
					if @model.fields.type is "20" or @model.fields.type is 20
						@pagetitle = "Produkt"
				else
					@pagetitle = "Leistung"
			when 840
				if @model._newObj
					if @model.fields.type is "10" or @model.fields.type is 10
						@pagetitle = "Leistung erfassen"
					if @model.fields.type is "20" or @model.fields.type is 20
						@pagetitle = "Produkt erfassen"
				else
					if @model.fields.type is "10" or @model.fields.type is 10
						@pagetitle = "Leistung bearbeiten"
					if @model.fields.type is "20" or @model.fields.type is 20
						@pagetitle = "Produkt bearbeiten"
			else console.log "loadPage page #{@page} not handled"
		return

	pageBack: ->
		switch @page
			when 800 then @ng.$window.location.href = "desktop"
			when 820,830,840 then @loadPage 800
			when 810 then @loadPage 820
			else console.log "pageBack page #{@page} not handled"
		return

	back: ->
		@pageBack()
		return

	loadIndex: ->
		addLabel = (model) ->
			"#{model.title}"
		addInfoLabelProd = (model) ->
			"CHF #{model.price}"
		addInfoLabelServ = (model) ->
			dur = BambooController.hourStringByMin model.duration
			"#{dur} - #{model.breakAfter}' Pause - CHF #{model.price}"
		ok_cb = (res) =>
			@stopWaiting()
			@resetModel()
			@models = res
			listProd = []
			listServ = []
			for e in @models
				if e.type is "10" or e.type is 10 then listServ.push e
				if e.type is "20" or e.type is 20 then listProd.push e
			@cblServices.setContent listServ, addLabel, false, false, addInfoLabelServ
			@cblProducts.setContent listProd, addLabel, false, false, addInfoLabelProd
			return
		@_indexAction @from, "", ok_cb
		return

	start: ->
		if !@loadData()
			@loadPage 800
		else
			if @loadData().pageLoad
				switch @loadData().action
					when 1 then @showProduct @loadData().elementId
					when 2 then @addProduct()
					when 3 then @editProduct @loadData().elementId
					when 4 then @deleteProduct @loadData().elementId
					else
						@loadPage 800
			else
				@loadPage 800
		return

	showProduct: (id) ->
		@_showAction id, @from, 810
		return

	addProduct: ->
		@createChecklists()
		@updateRequired()
		@_addAction 840
		return

	editProduct: (id) ->
		@createChecklists()
		@updateRequired()
		@_editAction id, @from, 840
		return

	saveProduct: ->
		@updateModel()
		@_saveAction @from, 800
		return

	deleteProduct: (id) ->
		confirmHeader = "Leistung löschen"
		confirmQuestion = "Die Leistung wirklich löschen?"
		@_deleteAction(id, @from, 800, confirmHeader, confirmQuestion)
		return

	deleteService: (id) ->
		confirmHeader = "Leistung löschen"
		confirmQuestion = "Die Leistung wirklich löschen?"
		@_deleteAction(id, @from, 1, confirmHeader, confirmQuestion)
		return

	updateModel: ->
		@model.fields.employees = []
		@model.fields.rooms = []
		if @model.fields.type is "10" or @model.fields.type is 10
			checkedEmployees = @employeesCheckbox.checkedExtensionContent()[0]
			checkedRooms = @roomsCheckbox.checkedExtensionContent()[0]
			i = 0
			while i < checkedEmployees.length
				employee = {}
				if !@model._newObj
					employee.product_id = @model.fields.id
				employee.employee_id = checkedEmployees[i].id
				@model.fields.employees.push employee
				i++
			i = 0
			while i < checkedRooms.length
				room = {}
				if !@model._newObj
					room.product_id = @model.fields.id
				room.room_id = checkedRooms[i].id
				@model.fields.rooms.push room
				i++
			price = 0
			duration = 0
			i = 0
			while i < @model.fields.productparts.length
				price += @model.fields.productparts[i].price
				duration += @model.fields.productparts[i].duration
				i++
			@model.fields.price = price
			@model.fields.duration = duration
		else if @model.fields.type is "20" or @model.fields.type is 20
			@model.fields.productparts = []
		return

	updateRequired: ->
		type = @model.fields.type
		unless @isVarEmpty type
			type = String type
			if type is "10"
				@_required = @cloneObject @_requiredPerType[type]
			if type is "20"
				@_required = @cloneObject @_requiredPerType[type]
		return

	onSubpageChange: ->
		@updateRequired()
		if @model._newObj
			if @model.fields.type is "10" or @model.fields.type is 10
				@pagetitle = "Leistung erfassen"
			if @model.fields.type is "20" or @model.fields.type is 20
				@pagetitle = "Produkt erfassen"
		else
			if @model.fields.type is "10" or @model.fields.type is 10
				@pagetitle = "Leistung bearbeiten"
			if @model.fields.type is "20" or @model.fields.type is 20
				@pagetitle = "Produkt bearbeiten"
		@refreshFormData()
		return

	createChecklists: ->
		@createEmployeesChecklist()
		@createRoomsChecklist()
		return

	createEmployeesChecklist: ->
		relName = "Mitarbeiter"
		relProp = "employee"
		from = "api/v1/org/employees"
		query = "?_fields=id,name,firstname"
		labelFunc = (model) ->
			"Alle"
		extLabelFunc = (model) ->
			"#{model.firstname} #{model.name}"
		@_createRelationChecklist @employeesCheckbox, relName, @model.fields.employees, relProp, from, query, labelFunc, extLabelFunc
		return

	createRoomsChecklist: ->
		relName = "Räume"
		relProp = "room"
		from = "api/v1/loc/rooms"
		query = "?_fields=id,title"
		labelFunc = (model) ->
			"Alle"
		extLabelFunc = (model) ->
			"#{model.title}"
		@_createRelationChecklist @roomsCheckbox, relName, @model.fields.rooms, relProp, from, query, labelFunc, extLabelFunc
		return

	loadTariffpositions: ->
		from = "api/v1/prod/tariffpositions"
		ok_cb = (res) =>
			@stopWaiting()
			i = 0
			for pos in res
				pos.label = "#{pos.nr} - #{pos.title}"
			###
			addPos =
				id: 0
				label: "Freie Eingabe"
			res.push addPos
			###
			@tariffpositions = @dateConverter res
			return
		@_loadAction from,"","",ok_cb
		return

	addProductpart: (ind = "") ->
		newProd =
			tariffposition_id: 0
			tariff: 590
		if ind is ""
			@model.fields.productparts.push newProd
		else
			@model.fields.productparts.splice ind, 0, newProd
		@refreshFormData()
		return

	removeProductpart: (ind) ->
		@model.fields.productparts.splice ind, 1
		@refreshFormData()
		return

	pricePerFiveMin: (price, duration) ->
		p = parseFloat(price)
		d = parseFloat(duration)
		ppfm = p / d * 5
		return ppfm.toFixed(2)

###
#	Controller
###
app.controller "ProductsController", ["$scope", "$window", "$document", "Upload", "localStorageService", "Restangular", ($scope, $window, $document, Upload, localStorageService, Restangular) ->

	ng = @
	ng.$scope = $scope
	ng.$window = $window
	ng.$document = $document
	ng.Upload = Upload
	ng.localStorageService = localStorageService
	ng.Restangular = Restangular

	ng.ctrl = new ProdController @

	return
]
