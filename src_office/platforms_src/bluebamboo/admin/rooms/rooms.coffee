###
#	Rooms Controller Class
###
class RoomController extends BambooController

	constructor: (ctrl) ->
		super ctrl
		@addModule "locations"
		@addModule "rooms"
		@loadIndex = =>
			@modules.rooms.loadIndex()
		@start()
		return

	start: ->
		if !@loadData()
			@loadIndex()
		else
			if @loadData().pageLoad
				switch @loadData().action
					when 1 then @modules.rooms.showRoom @loadData().elementId
					when 2 then @modules.rooms.addRoom()
					when 3 then @modules.rooms.editRoom @loadData().elementId
					when 4 then @modules.rooms.deleteRoom @loadData().elementId
					else
						@loadIndex()
			else
				@loadIndex()
		return

	cta: (data="") ->
		super()
		if @isReady()
			switch @page
				when 730
					@pageBack()
					@model.fields.location = data
					@model.fields.location_id = data.id
					console.log @model.fields
				when 1040
					@modules.rooms.saveRoom()
				when 1050
					@modules.rooms.saveRoom()
				else console.log "cta page #{@page} not handled"
		else
			# TODO - Fehlerfenster öffnen
		return

	cancel: ->
		switch @page
			when 1000, 1020 then @gotoDesktop()
			when 1010,1030,1040 then @gotoIndex()
			when 1050 then @pageBack()
			else
				@gotoIndex()
				console.log "cancel page #{@page} not handled"
		return

	back: ->
		@pageBack()
		return

###
#	Controller
###
app.controller "RoomsController", ["$scope", "$window", "$document", "localStorageService", "Restangular", ($scope, $window, $document, localStorageService, Restangular) ->

	ng = @
	ng.$scope = $scope
	ng.$window = $window
	ng.$document = $document
	ng.localStorageService = localStorageService
	ng.Restangular = Restangular

	ng.ctrl = new RoomController @

	return
]
