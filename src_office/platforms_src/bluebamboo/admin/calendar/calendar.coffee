###
#	Bookings Controller Class
###
class BookController extends BambooController

	constructor: (ctrl) ->
		super ctrl
		@startWaiting()
		@addModule("bookings")
		@addModule("dossiers")
		@addModule("customers")
		@addModule("employees")
		@addModule("rooms")
		@addModule("products")
		@loadIndex = ->
			@modules.bookings.loadCalendar()
		@start()
		@stopWaiting()
		return

	start: ->

		if !@loadData()
			@modules.bookings.loadCalendar()
		else
			ctrl = @
			data = @loadData()
			if data.pageLoad
				switch data.action
					when 1
						@modules.bookings.showBooking(data.elementId)
					when 2
						if data.pageData? then @modules.bookings.addBooking(data.pageData) else @modules.bookings.addBooking()
					when 3 then @modules.bookings.editBooking data.elementId
					when 4 then @modules.bookings.deleteBooking data.elementId
					when 5
						#ctrl.startWaiting()
						ctrl.modules.bookings.loadCalendar()
						#	.then (res) ->
						#		ctrl.stopWaiting()
						#		if data.pageData? then ctrl.model._chooseProps = data.pageData
						#		return
						#	, (res) ->
						#		ctrl.stopWaiting()
						#		return
						if data.pageData? then ctrl.model._chooseProps = data.pageData
					else
						@modules.bookings.loadCalendar()
			else
				@modules.bookings.loadCalendar()
		return

	cta: (data = "") ->
		ctrl = @
		if ctrl.isReady()
			switch ctrl.page
				when 140,150 then ctrl.modules.bookings.saveBooking(0)
				when 160
					ctrl.modules.bookings.cancelBooking(ctrl.model.fields, 0)
						.then (res) ->
							# ctrl.modules.bookings.loadBookings(0, 0)
							return
				when 230
					ctrl.pageBack()
					ctrl.modules.bookings.addCustomer(data)
						.then (res) ->
							ctrl.refreshPage()
							ctrl.refreshFormData()
							return
				when 240
					ctrl.modules.customers.saveCustomer(0, (res) =>
						ctrl.stopWaiting()
						ctrl.pageBack(2)
						ctrl.modules.bookings.addCustomer(res)
						return
					)
				when 473
					ctrl.pageBack()
					ctrl.model.fields.case = data
					ctrl.model.fields.case_id = data.id
				when 474
					ctrl.modules.dossiers.saveCase(0, (res) =>
						ctrl.stopWaiting()
						loop
							ctrl.pageBack(1)
							break if ctrl.page > 99 and ctrl.page < 200
						ctrl.model.fields.case_id = res.id
						ctrl.model.fields.case = res
						return
					)
				when 484,485
					ctrl.modules.dossiers.saveReport(0, (res) =>
						ctrl.stopWaiting()
						ctrl.pageBack()
						ctrl.model.fields.report_id = res.id
						ctrl.model.fields.report = res
						return
					)
				when 630
					ctrl.pageBack()
					ctrl.modules.bookings.addEmployee(data)
					return
				when 830
					ctrl.pageBack()
					ctrl.modules.bookings.addProduct(data)
						.then (res) ->
							ctrl.refreshPage()
							ctrl.refreshFormData()
							return
					return
				when 1030
					ctrl.pageBack()
					ctrl.modules.bookings.addRoom(data)
					return
				else console.log "cta page #{@page} not handled"
		else
			console.log "Form nicht ready?!"
			# TODO - Fehlerfenster öffnen
		super()
		return

	back: ->
		switch @page
			when 170 then @gotoDesktop()
			else
				@pageBack()
				console.log "back page #{@page} not handled"
		return

	cancel: ->
		switch @page
			when 170 then @gotoDesktop()
			else @pageBack()
		return

###
#	Controller
###
app.controller "BookingsController", ["$scope", "$window", "$document", "localStorageService", "Restangular", "uiCalendarConfig", ($scope, $window, $document, localStorageService, Restangular, uiCalendarConfig) ->

	ng = @
	ng.$scope = $scope
	ng.$window = $window
	ng.$document = $document
	ng.localStorageService = localStorageService
	ng.Restangular = Restangular
	ng.uiCalendarConfig = uiCalendarConfig

	ng.ctrl = new BookController @

	return
]
