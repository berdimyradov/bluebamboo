###
	Customers Controller Class
###
class CustController extends BambooController

	constructor: (ctrl) ->
		super ctrl
		c = @
		@addModule "customers"
		@addModule "dossiers"
		@indexPage = 220
		stayAwakePages = [410,450,471,473,474,475,481,484,485,487,488,210]
		stayAwakePages.forEach( (e, i, a) ->
			c._stayAwake[e] = true
			return
		)
		@loadIndex = ->
			@modules.customers.loadIndex()
		@start()
		return

	start: ->
		if !@loadData()
			@loadIndex()
		else
			if @loadData().pageLoad
				switch @loadData().action
					when 1 then @modules.customers.showCustomer @loadData().elementId
					when 2 then @modules.customers.addCustomer()
					when 3 then @modules.customers.editCustomer @loadData().elementId
					when 4 then @modules.customers.deleteCustomer @loadData().elementId
					else
						@loadIndex()
			else
				@loadIndex()
		return

	cta: (data = "") ->
		if @isReady()
			switch @page
				when 230
					ctrl = @
					module = @modules.customers
					if ctrl.model._choosing? and ctrl.model._choosing
						choosing = true
						chooseFor = ctrl.model._chooseFor
					else
						choosing = false
					ctrl.pageBack()
					if choosing
						switch chooseFor
							when "guardian"
								module.addGuardian(data)
							when "caregiver"
								module.addCaregiver(data)
					ctrl.refreshFormData()
					return
				when 240
					ctrl = @
					if ctrl.model._choosing? and ctrl.model._choosing
						choosing = ctrl.model._choosing
						chooseFor = ctrl.model._chooseFor
						ctrl.modules.customers.saveCustomer(0, (res) =>
							ctrl.stopWaiting()
							ctrl.pageBack(2)
							switch chooseFor
								when "guardian"
									module.addGuardian(res)
								when "caregiver"
									module.addCaregiver(res)
							return
						)
					else
						@modules.customers.saveCustomer(270, "", (res) =>
							@modules.customers.loadTherapies()
							return
						)
				when 241 then @modules.customers.searchCustomers()
				when 242 then @modules.customers.addCustomer(@model.fields)
				when 250
					# console.log @forms
					if @isFormValid("form_customer_edit")
						@modules.customers.saveCustomer(-1, "", (res) =>
							@setModel(res)
							return
						)
				when 450
					@modules.dossiers.saveDossier(-1, "", (res) =>
						res.customer = @model.fields.customer
						@setModel res
						return
					)
				when 473
					@pageBack()
					@model.fields.booking.case = data
					@model.fields.booking.case_id = data.id
				when 474
					@modules.dossiers.saveCase(-1, "", (res) =>
						@model.fields.cases.push res
						@modules.dossiers.setContentCblCases @model.fields.cases
						return
					)
				when 475
					@modules.dossiers.saveCase(-1, "", (res) =>
						#@modules.dossiers.showCase res.id
						@setModel res
						return
					)
				when 484
					@modules.dossiers.saveReport(-1, "", (res) =>
						id = @model.fields.id
						@pageBack()
						@modules.dossiers.showCase id
						return
					)
				when 485
					@modules.dossiers.saveReport(-1, "", (res) =>
						@pageBack()
						@modules.dossiers.showReport res.id
						return
					)
				when 487, 488
					console.log @model
					if !@isVarEmpty @model.fields.booking.case_id
						hasCase = true
						booking = @model.fields.booking
					else
						hasCase = false
					ret = =>
						id = @model.fields.customer.id
						@pageBack()
						@modules.dossiers.showDossier(id)
						return
					@modules.dossiers.saveReport(-1, "", (res) =>
						if hasCase
							from = "api/v1/booking/bookings"
							@startWaiting()
							@ng.Restangular.all("#{from}/#{booking.id}").customPUT(booking).then(
								(res) =>
									@stopWaiting()
									ret()
									return
								(res) =>
									@stopWaiting()
									@setFeedback res.data
									return
							)
						else
							ret()
						return
					)
				else console.log "cta page #{@page} not handled"
		else
			# TODO - Fehlerfenster öffnen
		super()
		return

	back: ->
		@pageBack()
		return

	cancel: ->
		switch @page
			when 200, 220 then @gotoDesktop()
			when 210,230,240,241,242,270 then @gotoIndex()
			when 250, 450, 474, 475, 481, 484, 485 then @pageBack()
			else
				@gotoIndex()
				console.log "cancel page #{@page} not handled"
		return

app.controller "CustomersController", ["$scope", "$window", "$document", "Upload", "localStorageService", "Restangular", ($scope, $window, $document, Upload, localStorageService, Restangular) ->

	ng = @
	ng.$scope = $scope
	ng.$window = $window
	ng.$document = $document
	ng.Upload = Upload
	ng.localStorageService = localStorageService
	ng.Restangular = Restangular

	ng.ctrl = new CustController @
	return
]
