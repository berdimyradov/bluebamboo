#out: $1.js, bare: true

###
	Employees Controller Class
###
class EmpController extends BambooController

	from: "api/v1/org/employees"
	_required: ["name", "firstname"]
	_autofocus: "name"
	genders: [
			id: true
			label: "männlich"
		,
			id: false
			label: "weiblich"
	]

	emptyModel:
		locations: []
		products: []

	constructor: (ctrl) ->
		super ctrl
		@contentBoxList = new ContentBoxList()
		@locationsCheckbox = new ContentBoxListCheckboxExtensions()
		@productsCheckbox = new ContentBoxListCheckboxExtensions()
		@start()
		return

	isReady: ->
		if @waiting() then return false
		switch @page
			when 1 then return true
			when 2 then return true
			when 3 then return true
			when 4
				if @forms.form.$invalid then return false
				for prop in @_required
					if @isModelEmpty prop then return false
				return true
			else console.log "isReady page #{@page} not handled"
		true

	cta: ->
		super()
		if @isReady()
			switch @page
				when 4 then @saveEmployee()
				else console.log "cta page #{@page} not handled"
		else
			# if @isModelEmpty "birth_day" then @model.fields.birth_day = ""
			# TODO - Fehlerfenster öffnen
		return

	cancel: ->
		switch @page
			when 1,2,4 then @pageBack()
			when 3 then @loadPage 2
			else console.log "cancel page #{@page} not handled"
		return

	loadPage: (page) ->
		super page
		switch page
			when 1
				@pagetitle = "Mitarbeiter"
			when 2
				@loadIndex()
				@pagetitle = "Mitarbeiter"
			when 3
				@pagetitle = "Mitarbeiter"
			when 4
				@createChecklists()
				if @model._newObj
					@pagetitle = "Mitarbeiter erfassen"
				else
					@pagetitle = "Mitarbeiter bearbeiten"
			else console.log "loadPage page #{@page} not handled"
		return

	pageBack: ->
		switch @page
			when 1 then @ng.$window.location.href = "desktop"
			when 2 then @loadPage 1
			when 3 then @loadPage 2
			when 4 then @loadPage 1
			else console.log "pageBack page #{@page} not handled"
		return

	back: ->
		@pageBack()
		return

	loadIndex: ->
		query = "?_fields=id,name,firstname"
		addLabel = (model) ->
			"#{model.firstname} #{model.name}"
		@_indexActionCBL @contentBoxList, @from, addLabel, query
		return

	start: ->
		if !@loadData()
			@loadPage 1
		else
			if @loadData().pageLoad
				switch @loadData().action
					when 1 then @showEmployee @loadData().elementId
					when 2 then @addEmployee()
					when 3 then @editEmployee @loadData().elementId
					when 4 then @deleteEmployee @loadData().elementId
					else
						@loadPage 1
			else
				@loadPage 1
		return

	showEmployee: (id) ->
		@_showAction id, @from, 3
		return

	addEmployee: ->
		@_addAction 4
		return

	editEmployee: (id) ->
		@_editAction id, @from, 4
		return

	saveEmployee: ->
		@updateModel()
		@_saveAction @from, 1
		return

	deleteEmployee: (id) ->
		confirmHeader = "Mitarbeiter löschen"
		confirmQuestion = "Den Mitarbeiter wirklich löschen?"
		@_deleteAction(id, @from, 1, confirmHeader, confirmQuestion)
		return

	updateModel: ->
		@model.fields.locations = []
		@model.fields.products = []
		checkedLocations = @locationsCheckbox.checkedExtensionContent()[0]
		checkedProducts = @productsCheckbox.checkedExtensionContent()[0]
		i = 0
		while i < checkedLocations.length
			location = {}
			if !@model._newObj
				location.employee_id = @model.fields.id
			location.location_id = checkedLocations[i].id
			@model.fields.locations.push location
			i++
		i = 0
		while i < checkedProducts.length
			product = {}
			if !@model._newObj
				product.employee_id = @model.fields.id
			product.product_id = checkedProducts[i].id
			@model.fields.products.push product
			i++
		return

	showEvents: (id) ->
		###
			TODO
		###
		@pageBack()
		return

	createChecklists: ->
		@createLocationsChecklist()
		@createProductsChecklist()
		return

	createLocationsChecklist: ->
		relName = "Lokationen"
		relProp = "location"
		from = "api/v1/loc/locations"
		query = "?_fields=id,title"
		labelFunc = (model) ->
			"Alle"
		extLabelFunc = (model) ->
			"#{model.title}"
		@_createRelationChecklist @locationsCheckbox, relName, @model.fields.locations, relProp, from, query, labelFunc, extLabelFunc
		return

	createProductsChecklist: ->
		relName = "Leistungen"
		relProp = "product"
		from = "api/v1/prod/products"
		query = "?_fields=id,title&type=10"
		labelFunc = (model) ->
			"Alle"
		extLabelFunc = (model) ->
			"#{model.title}"
		@_createRelationChecklist @productsCheckbox, relName, @model.fields.products, relProp, from, query, labelFunc, extLabelFunc
		return

app.controller "EmployeesController", ["$scope", "$window", "$document", "Upload", "localStorageService", "Restangular", ($scope, $window, $document, Upload, localStorageService, Restangular) ->

	ng = @
	ng.$scope = $scope
	ng.$window = $window
	ng.$document = $document
	ng.Upload = Upload
	ng.localStorageService = localStorageService
	ng.Restangular = Restangular

	ng.ctrl = new EmpController @

	return
]
